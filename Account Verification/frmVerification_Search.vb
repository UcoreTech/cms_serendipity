﻿Imports System
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmVerification_Search
    Private mycon As New Clsappconfiguration

    Private Sub frmVerification_Search_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadClient(txtSearch.Text)
    End Sub

    Private Sub LoadClient(ByVal search As String)
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(mycon.cnstring, "_Select_ClientInfo_Verification",
                                           New SqlParameter("@Search", search))
            dgvList.DataSource = ds.Tables(0)
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        LoadClient(txtSearch.Text)
    End Sub

    Private Sub dgvList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.DoubleClick
        With dgvList
            frmVerification.lblIDno.Text = .SelectedRows(0).Cells(0).Value.ToString
            frmVerification.lblClientname.Text = .SelectedRows(0).Cells(1).Value.ToString
        End With
        Me.Close()
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        With dgvList
            frmVerification.lblIDno.Text = .SelectedRows(0).Cells(0).Value.ToString
            frmVerification.lblClientname.Text = .SelectedRows(0).Cells(1).Value.ToString
        End With
        Me.Close()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

End Class