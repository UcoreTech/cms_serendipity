﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVerification
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblClientname = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblIDno = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.tabVerification = New System.Windows.Forms.TabControl()
        Me.CurrentLoan = New System.Windows.Forms.TabPage()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtBalPrincipal = New System.Windows.Forms.TextBox()
        Me.txtBalInterest = New System.Windows.Forms.TextBox()
        Me.txtBalServicefee = New System.Windows.Forms.TextBox()
        Me.txtBalCreditAcct = New System.Windows.Forms.TextBox()
        Me.txtTotalPayment = New System.Windows.Forms.TextBox()
        Me.dgvSubsidiaryCurrent = New System.Windows.Forms.DataGridView()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dgvLoans = New System.Windows.Forms.DataGridView()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.LoanHistory = New System.Windows.Forms.TabPage()
        Me.dgvLoanHistorySubsidiary = New System.Windows.Forms.DataGridView()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dgvLoanHistory = New System.Windows.Forms.DataGridView()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Debit = New System.Windows.Forms.TabPage()
        Me.TBCDebitAccounts = New System.Windows.Forms.TabControl()
        Me.TBListAccounts = New System.Windows.Forms.TabPage()
        Me.dgvDebitList = New System.Windows.Forms.DataGridView()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TBSoaAccounts = New System.Windows.Forms.TabPage()
        Me.dgvSoaList = New System.Windows.Forms.DataGridView()
        Me.dgvDebitDetails = New System.Windows.Forms.DataGridView()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Credit = New System.Windows.Forms.TabPage()
        Me.dgvCreditDetails = New System.Windows.Forms.DataGridView()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.dgvCredit = New System.Windows.Forms.DataGridView()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Amortization = New System.Windows.Forms.TabPage()
        Me.btnSelectedLoanPrint = New System.Windows.Forms.Button()
        Me.txtServiceFee = New System.Windows.Forms.TextBox()
        Me.txtPrincipal = New System.Windows.Forms.TextBox()
        Me.txtInterest = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.lblLoanType = New System.Windows.Forms.Label()
        Me.lblLoanRef = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.dgvAmortizationBalances = New System.Windows.Forms.DataGridView()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.dgvAmortizationSchedule = New System.Windows.Forms.DataGridView()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.SOA = New System.Windows.Forms.TabPage()
        Me.CrvRpt = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.RBHistory = New System.Windows.Forms.RadioButton()
        Me.RBBalance = New System.Windows.Forms.RadioButton()
        Me.btnBrowse = New System.Windows.Forms.Button()
        Me.txtSoa = New System.Windows.Forms.TextBox()
        Me.RBPerSoa = New System.Windows.Forms.RadioButton()
        Me.RBAll = New System.Windows.Forms.RadioButton()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.dtDate = New System.Windows.Forms.DateTimePicker()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.cboFilterSoa = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.btnPrintStatement = New System.Windows.Forms.Button()
        Me.btnPrintLoanLedger = New System.Windows.Forms.Button()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.PanePanel5 = New WindowsApplication2.PanePanel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.tabVerification.SuspendLayout()
        Me.CurrentLoan.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.dgvSubsidiaryCurrent, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.dgvLoans, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.LoanHistory.SuspendLayout()
        CType(Me.dgvLoanHistorySubsidiary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvLoanHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Debit.SuspendLayout()
        Me.TBCDebitAccounts.SuspendLayout()
        Me.TBListAccounts.SuspendLayout()
        CType(Me.dgvDebitList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TBSoaAccounts.SuspendLayout()
        CType(Me.dgvSoaList, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDebitDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Credit.SuspendLayout()
        CType(Me.dgvCreditDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCredit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Amortization.SuspendLayout()
        CType(Me.dgvAmortizationBalances, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvAmortizationSchedule, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SOA.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.PanePanel1.SuspendLayout()
        Me.PanePanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 22)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1020, 44)
        Me.Panel1.TabIndex = 57
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblClientname)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.lblIDno)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1020, 42)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'lblClientname
        '
        Me.lblClientname.AutoSize = True
        Me.lblClientname.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClientname.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblClientname.Location = New System.Drawing.Point(433, 17)
        Me.lblClientname.Name = "lblClientname"
        Me.lblClientname.Size = New System.Drawing.Size(138, 16)
        Me.lblClientname.TabIndex = 1
        Me.lblClientname.Text = "CLIENT, CLIENT C."
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(344, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(92, 14)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Client Name :"
        '
        'lblIDno
        '
        Me.lblIDno.AutoSize = True
        Me.lblIDno.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIDno.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblIDno.Location = New System.Drawing.Point(57, 18)
        Me.lblIDno.Name = "lblIDno"
        Me.lblIDno.Size = New System.Drawing.Size(94, 14)
        Me.lblIDno.TabIndex = 0
        Me.lblIDno.Text = "SAMPLE-123"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 14)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "ID No. :"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.White
        Me.Panel2.Controls.Add(Me.tabVerification)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 66)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1020, 492)
        Me.Panel2.TabIndex = 58
        '
        'tabVerification
        '
        Me.tabVerification.Controls.Add(Me.CurrentLoan)
        Me.tabVerification.Controls.Add(Me.LoanHistory)
        Me.tabVerification.Controls.Add(Me.Debit)
        Me.tabVerification.Controls.Add(Me.Credit)
        Me.tabVerification.Controls.Add(Me.Amortization)
        Me.tabVerification.Controls.Add(Me.SOA)
        Me.tabVerification.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabVerification.Location = New System.Drawing.Point(0, 0)
        Me.tabVerification.Name = "tabVerification"
        Me.tabVerification.SelectedIndex = 0
        Me.tabVerification.Size = New System.Drawing.Size(1020, 492)
        Me.tabVerification.TabIndex = 0
        '
        'CurrentLoan
        '
        Me.CurrentLoan.BackColor = System.Drawing.Color.White
        Me.CurrentLoan.Controls.Add(Me.Panel5)
        Me.CurrentLoan.Controls.Add(Me.dgvSubsidiaryCurrent)
        Me.CurrentLoan.Controls.Add(Me.Panel3)
        Me.CurrentLoan.Controls.Add(Me.dgvLoans)
        Me.CurrentLoan.Controls.Add(Me.Panel4)
        Me.CurrentLoan.Location = New System.Drawing.Point(4, 23)
        Me.CurrentLoan.Name = "CurrentLoan"
        Me.CurrentLoan.Padding = New System.Windows.Forms.Padding(3)
        Me.CurrentLoan.Size = New System.Drawing.Size(1012, 465)
        Me.CurrentLoan.TabIndex = 1
        Me.CurrentLoan.Text = "Active Loan"
        '
        'Panel5
        '
        Me.Panel5.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.Panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel5.Controls.Add(Me.Label17)
        Me.Panel5.Controls.Add(Me.txtBalPrincipal)
        Me.Panel5.Controls.Add(Me.txtBalInterest)
        Me.Panel5.Controls.Add(Me.txtBalServicefee)
        Me.Panel5.Controls.Add(Me.txtBalCreditAcct)
        Me.Panel5.Controls.Add(Me.txtTotalPayment)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel5.Location = New System.Drawing.Point(3, 365)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(1006, 47)
        Me.Panel5.TabIndex = 24
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Dock = System.Windows.Forms.DockStyle.Right
        Me.Label17.Location = New System.Drawing.Point(434, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(72, 14)
        Me.Label17.TabIndex = 22
        Me.Label17.Text = "Balances :"
        '
        'txtBalPrincipal
        '
        Me.txtBalPrincipal.BackColor = System.Drawing.SystemColors.Info
        Me.txtBalPrincipal.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBalPrincipal.Dock = System.Windows.Forms.DockStyle.Right
        Me.txtBalPrincipal.Location = New System.Drawing.Point(506, 0)
        Me.txtBalPrincipal.Name = "txtBalPrincipal"
        Me.txtBalPrincipal.ReadOnly = True
        Me.txtBalPrincipal.Size = New System.Drawing.Size(100, 15)
        Me.txtBalPrincipal.TabIndex = 24
        Me.txtBalPrincipal.Text = "0.00"
        Me.txtBalPrincipal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBalInterest
        '
        Me.txtBalInterest.BackColor = System.Drawing.SystemColors.Info
        Me.txtBalInterest.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBalInterest.Dock = System.Windows.Forms.DockStyle.Right
        Me.txtBalInterest.Location = New System.Drawing.Point(606, 0)
        Me.txtBalInterest.Name = "txtBalInterest"
        Me.txtBalInterest.ReadOnly = True
        Me.txtBalInterest.Size = New System.Drawing.Size(100, 15)
        Me.txtBalInterest.TabIndex = 23
        Me.txtBalInterest.Text = "0.00"
        Me.txtBalInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBalServicefee
        '
        Me.txtBalServicefee.BackColor = System.Drawing.SystemColors.Info
        Me.txtBalServicefee.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBalServicefee.Dock = System.Windows.Forms.DockStyle.Right
        Me.txtBalServicefee.Location = New System.Drawing.Point(706, 0)
        Me.txtBalServicefee.Name = "txtBalServicefee"
        Me.txtBalServicefee.ReadOnly = True
        Me.txtBalServicefee.Size = New System.Drawing.Size(100, 15)
        Me.txtBalServicefee.TabIndex = 25
        Me.txtBalServicefee.Text = "0.00"
        Me.txtBalServicefee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBalCreditAcct
        '
        Me.txtBalCreditAcct.BackColor = System.Drawing.SystemColors.Info
        Me.txtBalCreditAcct.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBalCreditAcct.Dock = System.Windows.Forms.DockStyle.Right
        Me.txtBalCreditAcct.Location = New System.Drawing.Point(806, 0)
        Me.txtBalCreditAcct.Name = "txtBalCreditAcct"
        Me.txtBalCreditAcct.ReadOnly = True
        Me.txtBalCreditAcct.Size = New System.Drawing.Size(100, 15)
        Me.txtBalCreditAcct.TabIndex = 27
        Me.txtBalCreditAcct.Text = "0.00"
        Me.txtBalCreditAcct.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalPayment
        '
        Me.txtTotalPayment.BackColor = System.Drawing.SystemColors.Info
        Me.txtTotalPayment.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTotalPayment.Dock = System.Windows.Forms.DockStyle.Right
        Me.txtTotalPayment.Location = New System.Drawing.Point(906, 0)
        Me.txtTotalPayment.Name = "txtTotalPayment"
        Me.txtTotalPayment.ReadOnly = True
        Me.txtTotalPayment.Size = New System.Drawing.Size(100, 15)
        Me.txtTotalPayment.TabIndex = 26
        Me.txtTotalPayment.Text = "0.00"
        Me.txtTotalPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dgvSubsidiaryCurrent
        '
        Me.dgvSubsidiaryCurrent.AllowUserToAddRows = False
        Me.dgvSubsidiaryCurrent.AllowUserToDeleteRows = False
        Me.dgvSubsidiaryCurrent.AllowUserToResizeColumns = False
        Me.dgvSubsidiaryCurrent.AllowUserToResizeRows = False
        Me.dgvSubsidiaryCurrent.BackgroundColor = System.Drawing.Color.White
        Me.dgvSubsidiaryCurrent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSubsidiaryCurrent.Dock = System.Windows.Forms.DockStyle.Top
        Me.dgvSubsidiaryCurrent.Location = New System.Drawing.Point(3, 208)
        Me.dgvSubsidiaryCurrent.Name = "dgvSubsidiaryCurrent"
        Me.dgvSubsidiaryCurrent.ReadOnly = True
        Me.dgvSubsidiaryCurrent.RowHeadersVisible = False
        Me.dgvSubsidiaryCurrent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSubsidiaryCurrent.Size = New System.Drawing.Size(1006, 157)
        Me.dgvSubsidiaryCurrent.TabIndex = 3
        '
        'Panel3
        '
        Me.Panel3.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.Panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel3.Location = New System.Drawing.Point(3, 171)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1006, 37)
        Me.Panel3.TabIndex = 22
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(5, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(124, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Subsidiary Ledger"
        '
        'dgvLoans
        '
        Me.dgvLoans.AllowUserToAddRows = False
        Me.dgvLoans.AllowUserToDeleteRows = False
        Me.dgvLoans.AllowUserToResizeColumns = False
        Me.dgvLoans.AllowUserToResizeRows = False
        Me.dgvLoans.BackgroundColor = System.Drawing.Color.White
        Me.dgvLoans.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLoans.Dock = System.Windows.Forms.DockStyle.Top
        Me.dgvLoans.Location = New System.Drawing.Point(3, 40)
        Me.dgvLoans.Name = "dgvLoans"
        Me.dgvLoans.ReadOnly = True
        Me.dgvLoans.RowHeadersVisible = False
        Me.dgvLoans.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLoans.Size = New System.Drawing.Size(1006, 131)
        Me.dgvLoans.TabIndex = 1
        '
        'Panel4
        '
        Me.Panel4.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.Panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel4.Controls.Add(Me.Label16)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(3, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(1006, 37)
        Me.Panel4.TabIndex = 23
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(5, 15)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(139, 16)
        Me.Label16.TabIndex = 0
        Me.Label16.Text = "List of Active Loans"
        '
        'LoanHistory
        '
        Me.LoanHistory.BackColor = System.Drawing.Color.White
        Me.LoanHistory.Controls.Add(Me.dgvLoanHistorySubsidiary)
        Me.LoanHistory.Controls.Add(Me.Label9)
        Me.LoanHistory.Controls.Add(Me.dgvLoanHistory)
        Me.LoanHistory.Controls.Add(Me.Label12)
        Me.LoanHistory.Location = New System.Drawing.Point(4, 23)
        Me.LoanHistory.Name = "LoanHistory"
        Me.LoanHistory.Size = New System.Drawing.Size(1012, 465)
        Me.LoanHistory.TabIndex = 2
        Me.LoanHistory.Text = "Loan History"
        '
        'dgvLoanHistorySubsidiary
        '
        Me.dgvLoanHistorySubsidiary.AllowUserToAddRows = False
        Me.dgvLoanHistorySubsidiary.AllowUserToDeleteRows = False
        Me.dgvLoanHistorySubsidiary.AllowUserToResizeColumns = False
        Me.dgvLoanHistorySubsidiary.AllowUserToResizeRows = False
        Me.dgvLoanHistorySubsidiary.BackgroundColor = System.Drawing.Color.White
        Me.dgvLoanHistorySubsidiary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLoanHistorySubsidiary.Location = New System.Drawing.Point(17, 172)
        Me.dgvLoanHistorySubsidiary.Name = "dgvLoanHistorySubsidiary"
        Me.dgvLoanHistorySubsidiary.ReadOnly = True
        Me.dgvLoanHistorySubsidiary.RowHeadersVisible = False
        Me.dgvLoanHistorySubsidiary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLoanHistorySubsidiary.Size = New System.Drawing.Size(982, 220)
        Me.dgvLoanHistorySubsidiary.TabIndex = 7
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(14, 155)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(129, 14)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Subsidiary Ledger :"
        '
        'dgvLoanHistory
        '
        Me.dgvLoanHistory.AllowUserToAddRows = False
        Me.dgvLoanHistory.AllowUserToDeleteRows = False
        Me.dgvLoanHistory.AllowUserToResizeColumns = False
        Me.dgvLoanHistory.AllowUserToResizeRows = False
        Me.dgvLoanHistory.BackgroundColor = System.Drawing.Color.White
        Me.dgvLoanHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLoanHistory.Location = New System.Drawing.Point(17, 28)
        Me.dgvLoanHistory.Name = "dgvLoanHistory"
        Me.dgvLoanHistory.ReadOnly = True
        Me.dgvLoanHistory.RowHeadersVisible = False
        Me.dgvLoanHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLoanHistory.Size = New System.Drawing.Size(982, 124)
        Me.dgvLoanHistory.TabIndex = 5
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(14, 11)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(127, 14)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "List of Paid Loans :"
        '
        'Debit
        '
        Me.Debit.BackColor = System.Drawing.Color.White
        Me.Debit.Controls.Add(Me.TBCDebitAccounts)
        Me.Debit.Controls.Add(Me.dgvDebitDetails)
        Me.Debit.Controls.Add(Me.Label6)
        Me.Debit.Location = New System.Drawing.Point(4, 23)
        Me.Debit.Name = "Debit"
        Me.Debit.Size = New System.Drawing.Size(1012, 465)
        Me.Debit.TabIndex = 3
        Me.Debit.Text = "Debit Accounts"
        '
        'TBCDebitAccounts
        '
        Me.TBCDebitAccounts.Controls.Add(Me.TBListAccounts)
        Me.TBCDebitAccounts.Controls.Add(Me.TBSoaAccounts)
        Me.TBCDebitAccounts.Location = New System.Drawing.Point(25, 14)
        Me.TBCDebitAccounts.Name = "TBCDebitAccounts"
        Me.TBCDebitAccounts.SelectedIndex = 0
        Me.TBCDebitAccounts.Size = New System.Drawing.Size(974, 238)
        Me.TBCDebitAccounts.TabIndex = 4
        '
        'TBListAccounts
        '
        Me.TBListAccounts.Controls.Add(Me.dgvDebitList)
        Me.TBListAccounts.Controls.Add(Me.Label5)
        Me.TBListAccounts.Location = New System.Drawing.Point(4, 23)
        Me.TBListAccounts.Name = "TBListAccounts"
        Me.TBListAccounts.Padding = New System.Windows.Forms.Padding(3)
        Me.TBListAccounts.Size = New System.Drawing.Size(966, 211)
        Me.TBListAccounts.TabIndex = 0
        Me.TBListAccounts.Text = "Debit Accounts"
        Me.TBListAccounts.UseVisualStyleBackColor = True
        '
        'dgvDebitList
        '
        Me.dgvDebitList.AllowUserToAddRows = False
        Me.dgvDebitList.AllowUserToDeleteRows = False
        Me.dgvDebitList.AllowUserToResizeColumns = False
        Me.dgvDebitList.AllowUserToResizeRows = False
        Me.dgvDebitList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvDebitList.BackgroundColor = System.Drawing.Color.White
        Me.dgvDebitList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDebitList.Location = New System.Drawing.Point(6, 17)
        Me.dgvDebitList.Name = "dgvDebitList"
        Me.dgvDebitList.ReadOnly = True
        Me.dgvDebitList.RowHeadersVisible = False
        Me.dgvDebitList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDebitList.Size = New System.Drawing.Size(954, 188)
        Me.dgvDebitList.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(151, 14)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "List of Debit Accounts :"
        Me.Label5.Visible = False
        '
        'TBSoaAccounts
        '
        Me.TBSoaAccounts.Controls.Add(Me.dgvSoaList)
        Me.TBSoaAccounts.Location = New System.Drawing.Point(4, 23)
        Me.TBSoaAccounts.Name = "TBSoaAccounts"
        Me.TBSoaAccounts.Padding = New System.Windows.Forms.Padding(3)
        Me.TBSoaAccounts.Size = New System.Drawing.Size(966, 211)
        Me.TBSoaAccounts.TabIndex = 1
        Me.TBSoaAccounts.Text = "Soa Accounts"
        Me.TBSoaAccounts.UseVisualStyleBackColor = True
        '
        'dgvSoaList
        '
        Me.dgvSoaList.AllowUserToAddRows = False
        Me.dgvSoaList.AllowUserToDeleteRows = False
        Me.dgvSoaList.AllowUserToResizeColumns = False
        Me.dgvSoaList.AllowUserToResizeRows = False
        Me.dgvSoaList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvSoaList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvSoaList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.dgvSoaList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSoaList.Location = New System.Drawing.Point(6, 17)
        Me.dgvSoaList.Name = "dgvSoaList"
        Me.dgvSoaList.ReadOnly = True
        Me.dgvSoaList.RowHeadersVisible = False
        Me.dgvSoaList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSoaList.Size = New System.Drawing.Size(954, 188)
        Me.dgvSoaList.TabIndex = 0
        '
        'dgvDebitDetails
        '
        Me.dgvDebitDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvDebitDetails.BackgroundColor = System.Drawing.Color.White
        Me.dgvDebitDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDebitDetails.Location = New System.Drawing.Point(25, 272)
        Me.dgvDebitDetails.Name = "dgvDebitDetails"
        Me.dgvDebitDetails.ReadOnly = True
        Me.dgvDebitDetails.RowHeadersVisible = False
        Me.dgvDebitDetails.Size = New System.Drawing.Size(974, 166)
        Me.dgvDebitDetails.TabIndex = 3
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(25, 255)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(59, 14)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Details :"
        '
        'Credit
        '
        Me.Credit.BackColor = System.Drawing.Color.White
        Me.Credit.Controls.Add(Me.dgvCreditDetails)
        Me.Credit.Controls.Add(Me.Label8)
        Me.Credit.Controls.Add(Me.dgvCredit)
        Me.Credit.Controls.Add(Me.Label7)
        Me.Credit.Location = New System.Drawing.Point(4, 23)
        Me.Credit.Name = "Credit"
        Me.Credit.Size = New System.Drawing.Size(1012, 465)
        Me.Credit.TabIndex = 4
        Me.Credit.Text = "Credit Accounts"
        '
        'dgvCreditDetails
        '
        Me.dgvCreditDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvCreditDetails.BackgroundColor = System.Drawing.Color.White
        Me.dgvCreditDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCreditDetails.Location = New System.Drawing.Point(31, 217)
        Me.dgvCreditDetails.Name = "dgvCreditDetails"
        Me.dgvCreditDetails.ReadOnly = True
        Me.dgvCreditDetails.RowHeadersVisible = False
        Me.dgvCreditDetails.Size = New System.Drawing.Size(968, 160)
        Me.dgvCreditDetails.TabIndex = 3
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(28, 200)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(59, 14)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Details :"
        '
        'dgvCredit
        '
        Me.dgvCredit.AllowUserToAddRows = False
        Me.dgvCredit.AllowUserToDeleteRows = False
        Me.dgvCredit.AllowUserToResizeColumns = False
        Me.dgvCredit.AllowUserToResizeRows = False
        Me.dgvCredit.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvCredit.BackgroundColor = System.Drawing.Color.White
        Me.dgvCredit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCredit.Location = New System.Drawing.Point(28, 26)
        Me.dgvCredit.Name = "dgvCredit"
        Me.dgvCredit.ReadOnly = True
        Me.dgvCredit.RowHeadersVisible = False
        Me.dgvCredit.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCredit.Size = New System.Drawing.Size(971, 171)
        Me.dgvCredit.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(25, 9)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(156, 14)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "List of Credit Accounts :"
        '
        'Amortization
        '
        Me.Amortization.Controls.Add(Me.btnSelectedLoanPrint)
        Me.Amortization.Controls.Add(Me.txtServiceFee)
        Me.Amortization.Controls.Add(Me.txtPrincipal)
        Me.Amortization.Controls.Add(Me.txtInterest)
        Me.Amortization.Controls.Add(Me.Label15)
        Me.Amortization.Controls.Add(Me.lblLoanType)
        Me.Amortization.Controls.Add(Me.lblLoanRef)
        Me.Amortization.Controls.Add(Me.Label14)
        Me.Amortization.Controls.Add(Me.Label13)
        Me.Amortization.Controls.Add(Me.dgvAmortizationBalances)
        Me.Amortization.Controls.Add(Me.Label10)
        Me.Amortization.Controls.Add(Me.dgvAmortizationSchedule)
        Me.Amortization.Controls.Add(Me.Label4)
        Me.Amortization.Location = New System.Drawing.Point(4, 23)
        Me.Amortization.Name = "Amortization"
        Me.Amortization.Size = New System.Drawing.Size(1012, 465)
        Me.Amortization.TabIndex = 5
        Me.Amortization.Text = "Amortization"
        Me.Amortization.UseVisualStyleBackColor = True
        '
        'btnSelectedLoanPrint
        '
        Me.btnSelectedLoanPrint.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnSelectedLoanPrint.Image = Global.WindowsApplication2.My.Resources.Resources.paperbook
        Me.btnSelectedLoanPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSelectedLoanPrint.Location = New System.Drawing.Point(11, 379)
        Me.btnSelectedLoanPrint.Name = "btnSelectedLoanPrint"
        Me.btnSelectedLoanPrint.Size = New System.Drawing.Size(229, 35)
        Me.btnSelectedLoanPrint.TabIndex = 22
        Me.btnSelectedLoanPrint.Text = "Preview Loan Amortization"
        Me.btnSelectedLoanPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSelectedLoanPrint.UseVisualStyleBackColor = True
        '
        'txtServiceFee
        '
        Me.txtServiceFee.BackColor = System.Drawing.SystemColors.Info
        Me.txtServiceFee.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtServiceFee.Location = New System.Drawing.Point(641, 201)
        Me.txtServiceFee.Name = "txtServiceFee"
        Me.txtServiceFee.ReadOnly = True
        Me.txtServiceFee.Size = New System.Drawing.Size(100, 15)
        Me.txtServiceFee.TabIndex = 15
        Me.txtServiceFee.Text = "0.00"
        Me.txtServiceFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPrincipal
        '
        Me.txtPrincipal.BackColor = System.Drawing.SystemColors.Info
        Me.txtPrincipal.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPrincipal.Location = New System.Drawing.Point(328, 202)
        Me.txtPrincipal.Name = "txtPrincipal"
        Me.txtPrincipal.ReadOnly = True
        Me.txtPrincipal.Size = New System.Drawing.Size(100, 15)
        Me.txtPrincipal.TabIndex = 14
        Me.txtPrincipal.Text = "0.00"
        Me.txtPrincipal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtInterest
        '
        Me.txtInterest.BackColor = System.Drawing.SystemColors.Info
        Me.txtInterest.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtInterest.Location = New System.Drawing.Point(492, 201)
        Me.txtInterest.Name = "txtInterest"
        Me.txtInterest.ReadOnly = True
        Me.txtInterest.Size = New System.Drawing.Size(100, 15)
        Me.txtInterest.TabIndex = 13
        Me.txtInterest.Text = "0.00"
        Me.txtInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(268, 203)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(54, 14)
        Me.Label15.TabIndex = 12
        Me.Label15.Text = "Totals: "
        '
        'lblLoanType
        '
        Me.lblLoanType.AutoSize = True
        Me.lblLoanType.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanType.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblLoanType.Location = New System.Drawing.Point(521, 10)
        Me.lblLoanType.Name = "lblLoanType"
        Me.lblLoanType.Size = New System.Drawing.Size(13, 14)
        Me.lblLoanType.TabIndex = 11
        Me.lblLoanType.Text = "-"
        '
        'lblLoanRef
        '
        Me.lblLoanRef.AutoSize = True
        Me.lblLoanRef.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanRef.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblLoanRef.Location = New System.Drawing.Point(268, 10)
        Me.lblLoanRef.Name = "lblLoanRef"
        Me.lblLoanRef.Size = New System.Drawing.Size(13, 14)
        Me.lblLoanRef.TabIndex = 10
        Me.lblLoanRef.Text = "-"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(434, 10)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(81, 14)
        Me.Label14.TabIndex = 9
        Me.Label14.Text = "Loan Type :"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(195, 10)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(67, 14)
        Me.Label13.TabIndex = 8
        Me.Label13.Text = "Loan Ref:"
        '
        'dgvAmortizationBalances
        '
        Me.dgvAmortizationBalances.AllowUserToAddRows = False
        Me.dgvAmortizationBalances.AllowUserToDeleteRows = False
        Me.dgvAmortizationBalances.AllowUserToResizeColumns = False
        Me.dgvAmortizationBalances.AllowUserToResizeRows = False
        Me.dgvAmortizationBalances.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvAmortizationBalances.BackgroundColor = System.Drawing.Color.White
        Me.dgvAmortizationBalances.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAmortizationBalances.Location = New System.Drawing.Point(11, 238)
        Me.dgvAmortizationBalances.Name = "dgvAmortizationBalances"
        Me.dgvAmortizationBalances.ReadOnly = True
        Me.dgvAmortizationBalances.RowHeadersVisible = False
        Me.dgvAmortizationBalances.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAmortizationBalances.Size = New System.Drawing.Size(982, 139)
        Me.dgvAmortizationBalances.TabIndex = 7
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(8, 221)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(60, 14)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Balance "
        '
        'dgvAmortizationSchedule
        '
        Me.dgvAmortizationSchedule.AllowUserToAddRows = False
        Me.dgvAmortizationSchedule.AllowUserToDeleteRows = False
        Me.dgvAmortizationSchedule.AllowUserToResizeColumns = False
        Me.dgvAmortizationSchedule.AllowUserToResizeRows = False
        Me.dgvAmortizationSchedule.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvAmortizationSchedule.BackgroundColor = System.Drawing.Color.White
        Me.dgvAmortizationSchedule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAmortizationSchedule.Location = New System.Drawing.Point(11, 53)
        Me.dgvAmortizationSchedule.Name = "dgvAmortizationSchedule"
        Me.dgvAmortizationSchedule.ReadOnly = True
        Me.dgvAmortizationSchedule.RowHeadersVisible = False
        Me.dgvAmortizationSchedule.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAmortizationSchedule.Size = New System.Drawing.Size(982, 136)
        Me.dgvAmortizationSchedule.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 36)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(151, 14)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Amortization Schedule "
        '
        'SOA
        '
        Me.SOA.Controls.Add(Me.CrvRpt)
        Me.SOA.Controls.Add(Me.Panel6)
        Me.SOA.Location = New System.Drawing.Point(4, 23)
        Me.SOA.Name = "SOA"
        Me.SOA.Size = New System.Drawing.Size(1012, 465)
        Me.SOA.TabIndex = 6
        Me.SOA.Text = "SOA"
        Me.SOA.UseVisualStyleBackColor = True
        '
        'CrvRpt
        '
        Me.CrvRpt.ActiveViewIndex = -1
        Me.CrvRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrvRpt.CachedPageNumberPerDoc = 10
        Me.CrvRpt.Cursor = System.Windows.Forms.Cursors.Default
        Me.CrvRpt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrvRpt.EnableDrillDown = False
        Me.CrvRpt.Location = New System.Drawing.Point(0, 41)
        Me.CrvRpt.Name = "CrvRpt"
        Me.CrvRpt.SelectionFormula = ""
        Me.CrvRpt.ShowGroupTreeButton = False
        Me.CrvRpt.ShowRefreshButton = False
        Me.CrvRpt.Size = New System.Drawing.Size(1012, 424)
        Me.CrvRpt.TabIndex = 60
        Me.CrvRpt.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.CrvRpt.ViewTimeSelectionFormula = ""
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.White
        Me.Panel6.Controls.Add(Me.RBHistory)
        Me.Panel6.Controls.Add(Me.RBBalance)
        Me.Panel6.Controls.Add(Me.btnBrowse)
        Me.Panel6.Controls.Add(Me.txtSoa)
        Me.Panel6.Controls.Add(Me.RBPerSoa)
        Me.Panel6.Controls.Add(Me.RBAll)
        Me.Panel6.Controls.Add(Me.Label18)
        Me.Panel6.Controls.Add(Me.dtDate)
        Me.Panel6.Controls.Add(Me.btnPrint)
        Me.Panel6.Controls.Add(Me.btnPreview)
        Me.Panel6.Controls.Add(Me.cboFilterSoa)
        Me.Panel6.Controls.Add(Me.Label11)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel6.Location = New System.Drawing.Point(0, 0)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(1012, 41)
        Me.Panel6.TabIndex = 0
        '
        'RBHistory
        '
        Me.RBHistory.AutoSize = True
        Me.RBHistory.Location = New System.Drawing.Point(525, 12)
        Me.RBHistory.Name = "RBHistory"
        Me.RBHistory.Size = New System.Drawing.Size(69, 18)
        Me.RBHistory.TabIndex = 11
        Me.RBHistory.TabStop = True
        Me.RBHistory.Text = "History"
        Me.RBHistory.UseVisualStyleBackColor = True
        Me.RBHistory.Visible = False
        '
        'RBBalance
        '
        Me.RBBalance.AutoSize = True
        Me.RBBalance.Location = New System.Drawing.Point(414, 11)
        Me.RBBalance.Name = "RBBalance"
        Me.RBBalance.Size = New System.Drawing.Size(103, 18)
        Me.RBBalance.TabIndex = 10
        Me.RBBalance.TabStop = True
        Me.RBBalance.Text = "Outstanding"
        Me.RBBalance.UseVisualStyleBackColor = True
        Me.RBBalance.Visible = False
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(792, 8)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(31, 24)
        Me.btnBrowse.TabIndex = 9
        Me.btnBrowse.Text = "....."
        Me.btnBrowse.UseVisualStyleBackColor = True
        Me.btnBrowse.Visible = False
        '
        'txtSoa
        '
        Me.txtSoa.Location = New System.Drawing.Point(679, 9)
        Me.txtSoa.Name = "txtSoa"
        Me.txtSoa.Size = New System.Drawing.Size(107, 22)
        Me.txtSoa.TabIndex = 8
        Me.txtSoa.Visible = False
        '
        'RBPerSoa
        '
        Me.RBPerSoa.AutoSize = True
        Me.RBPerSoa.Location = New System.Drawing.Point(600, 12)
        Me.RBPerSoa.Name = "RBPerSoa"
        Me.RBPerSoa.Size = New System.Drawing.Size(76, 18)
        Me.RBPerSoa.TabIndex = 7
        Me.RBPerSoa.TabStop = True
        Me.RBPerSoa.Text = "Per SOA"
        Me.RBPerSoa.UseVisualStyleBackColor = True
        Me.RBPerSoa.Visible = False
        '
        'RBAll
        '
        Me.RBAll.AutoSize = True
        Me.RBAll.Location = New System.Drawing.Point(357, 11)
        Me.RBAll.Name = "RBAll"
        Me.RBAll.Size = New System.Drawing.Size(39, 18)
        Me.RBAll.TabIndex = 6
        Me.RBAll.TabStop = True
        Me.RBAll.Text = "All"
        Me.RBAll.UseVisualStyleBackColor = True
        Me.RBAll.Visible = False
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(405, 12)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(88, 14)
        Me.Label18.TabIndex = 5
        Me.Label18.Text = "Select Date :"
        '
        'dtDate
        '
        Me.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtDate.Location = New System.Drawing.Point(499, 7)
        Me.dtDate.Name = "dtDate"
        Me.dtDate.Size = New System.Drawing.Size(169, 22)
        Me.dtDate.TabIndex = 4
        '
        'btnPrint
        '
        Me.btnPrint.Location = New System.Drawing.Point(929, 9)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(75, 23)
        Me.btnPrint.TabIndex = 3
        Me.btnPrint.Text = "Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnPreview
        '
        Me.btnPreview.Location = New System.Drawing.Point(848, 9)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(75, 23)
        Me.btnPreview.TabIndex = 2
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'cboFilterSoa
        '
        Me.cboFilterSoa.AutoCompleteCustomSource.AddRange(New String() {"Statement Of Accounts - Loan", "Statement Of Accounts - Detailed"})
        Me.cboFilterSoa.FormattingEnabled = True
        Me.cboFilterSoa.Items.AddRange(New Object() {"SOA  - Loans Receivable", "SOA  - Detailed", "SOA - Accounts Receivable - Trade"})
        Me.cboFilterSoa.Location = New System.Drawing.Point(86, 8)
        Me.cboFilterSoa.Name = "cboFilterSoa"
        Me.cboFilterSoa.Size = New System.Drawing.Size(254, 22)
        Me.cboFilterSoa.TabIndex = 1
        Me.cboFilterSoa.Text = "Select"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(9, 13)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(77, 14)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Filter SOA :"
        '
        'PanePanel1
        '
        Me.PanePanel1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.btnRefresh)
        Me.PanePanel1.Controls.Add(Me.btnPrintStatement)
        Me.PanePanel1.Controls.Add(Me.btnPrintLoanLedger)
        Me.PanePanel1.Controls.Add(Me.btnSearch)
        Me.PanePanel1.Controls.Add(Me.btnClose)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel1.Location = New System.Drawing.Point(0, 558)
        Me.PanePanel1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(1020, 30)
        Me.PanePanel1.TabIndex = 56
        '
        'btnRefresh
        '
        Me.btnRefresh.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnRefresh.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefresh.Image = Global.WindowsApplication2.My.Resources.Resources.reload
        Me.btnRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRefresh.Location = New System.Drawing.Point(739, 0)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(98, 28)
        Me.btnRefresh.TabIndex = 24
        Me.btnRefresh.Text = "Refresh"
        Me.btnRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'btnPrintStatement
        '
        Me.btnPrintStatement.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnPrintStatement.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrintStatement.Image = Global.WindowsApplication2.My.Resources.Resources.paperbook
        Me.btnPrintStatement.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPrintStatement.Location = New System.Drawing.Point(179, 0)
        Me.btnPrintStatement.Name = "btnPrintStatement"
        Me.btnPrintStatement.Size = New System.Drawing.Size(252, 28)
        Me.btnPrintStatement.TabIndex = 23
        Me.btnPrintStatement.Text = "Print Statement of Accounts"
        Me.btnPrintStatement.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPrintStatement.UseVisualStyleBackColor = True
        '
        'btnPrintLoanLedger
        '
        Me.btnPrintLoanLedger.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnPrintLoanLedger.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrintLoanLedger.Image = Global.WindowsApplication2.My.Resources.Resources.paperbook
        Me.btnPrintLoanLedger.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPrintLoanLedger.Location = New System.Drawing.Point(0, 0)
        Me.btnPrintLoanLedger.Name = "btnPrintLoanLedger"
        Me.btnPrintLoanLedger.Size = New System.Drawing.Size(179, 28)
        Me.btnPrintLoanLedger.TabIndex = 22
        Me.btnPrintLoanLedger.Text = "Print Loan Ledger"
        Me.btnPrintLoanLedger.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPrintLoanLedger.UseVisualStyleBackColor = True
        '
        'btnSearch
        '
        Me.btnSearch.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnSearch.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearch.Image = Global.WindowsApplication2.My.Resources.Resources.find1
        Me.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSearch.Location = New System.Drawing.Point(837, 0)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(106, 28)
        Me.btnSearch.TabIndex = 2
        Me.btnSearch.Text = "Search"
        Me.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClose.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = Global.WindowsApplication2.My.Resources.Resources.button_cancel
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(943, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 28)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'PanePanel5
        '
        Me.PanePanel5.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel5.Controls.Add(Me.Label19)
        Me.PanePanel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel5.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel5.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel5.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel5.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel5.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel5.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.PanePanel5.Name = "PanePanel5"
        Me.PanePanel5.Size = New System.Drawing.Size(1020, 22)
        Me.PanePanel5.TabIndex = 55
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(-1, -1)
        Me.Label19.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(142, 19)
        Me.Label19.TabIndex = 14
        Me.Label19.Text = "Account Inquiry "
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Date"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 109
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Doc No."
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 109
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Particulars"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 108
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Debit"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 109
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Credit"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Width = 109
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Principal"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Width = 109
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Interest"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Width = 108
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Service Fee"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Width = 109
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Check"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Width = 109
        '
        'frmVerification
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1020, 588)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.PanePanel5)
        Me.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Name = "frmVerification"
        Me.Text = "..."
        Me.Panel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.tabVerification.ResumeLayout(False)
        Me.CurrentLoan.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.dgvSubsidiaryCurrent, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.dgvLoans, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.LoanHistory.ResumeLayout(False)
        Me.LoanHistory.PerformLayout()
        CType(Me.dgvLoanHistorySubsidiary, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvLoanHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Debit.ResumeLayout(False)
        Me.Debit.PerformLayout()
        Me.TBCDebitAccounts.ResumeLayout(False)
        Me.TBListAccounts.ResumeLayout(False)
        Me.TBListAccounts.PerformLayout()
        CType(Me.dgvDebitList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TBSoaAccounts.ResumeLayout(False)
        CType(Me.dgvSoaList, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDebitDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Credit.ResumeLayout(False)
        Me.Credit.PerformLayout()
        CType(Me.dgvCreditDetails, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCredit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Amortization.ResumeLayout(False)
        Me.Amortization.PerformLayout()
        CType(Me.dgvAmortizationBalances, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvAmortizationSchedule, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SOA.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel5.ResumeLayout(False)
        Me.PanePanel5.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanePanel5 As WindowsApplication2.PanePanel
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblClientname As System.Windows.Forms.Label
    Friend WithEvents lblIDno As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents tabVerification As System.Windows.Forms.TabControl
    Friend WithEvents CurrentLoan As System.Windows.Forms.TabPage
    Friend WithEvents LoanHistory As System.Windows.Forms.TabPage
    Friend WithEvents Debit As System.Windows.Forms.TabPage
    Friend WithEvents Credit As System.Windows.Forms.TabPage
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents dgvDebitDetails As System.Windows.Forms.DataGridView
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dgvDebitList As System.Windows.Forms.DataGridView
    Friend WithEvents dgvCreditDetails As System.Windows.Forms.DataGridView
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents dgvCredit As System.Windows.Forms.DataGridView
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents dgvLoans As System.Windows.Forms.DataGridView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Amortization As System.Windows.Forms.TabPage
    Friend WithEvents dgvAmortizationBalances As System.Windows.Forms.DataGridView
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dgvAmortizationSchedule As System.Windows.Forms.DataGridView
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dgvSubsidiaryCurrent As System.Windows.Forms.DataGridView
    Friend WithEvents dgvLoanHistorySubsidiary As System.Windows.Forms.DataGridView
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents dgvLoanHistory As System.Windows.Forms.DataGridView
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents SOA As System.Windows.Forms.TabPage
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lblLoanType As System.Windows.Forms.Label
    Friend WithEvents lblLoanRef As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtInterest As System.Windows.Forms.TextBox
    Friend WithEvents txtServiceFee As System.Windows.Forms.TextBox
    Friend WithEvents txtPrincipal As System.Windows.Forms.TextBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnPrintLoanLedger As System.Windows.Forms.Button
    Friend WithEvents btnSelectedLoanPrint As System.Windows.Forms.Button
    Friend WithEvents btnPrintStatement As System.Windows.Forms.Button
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtBalCreditAcct As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalPayment As System.Windows.Forms.TextBox
    Friend WithEvents txtBalServicefee As System.Windows.Forms.TextBox
    Friend WithEvents txtBalPrincipal As System.Windows.Forms.TextBox
    Friend WithEvents txtBalInterest As System.Windows.Forms.TextBox
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents cboFilterSoa As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Private WithEvents CrvRpt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents dtDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents TBCDebitAccounts As System.Windows.Forms.TabControl
    Friend WithEvents TBListAccounts As System.Windows.Forms.TabPage
    Friend WithEvents TBSoaAccounts As System.Windows.Forms.TabPage
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dgvSoaList As System.Windows.Forms.DataGridView
    Friend WithEvents RBAll As System.Windows.Forms.RadioButton
    Friend WithEvents RBPerSoa As System.Windows.Forms.RadioButton
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents txtSoa As System.Windows.Forms.TextBox
    Friend WithEvents RBBalance As System.Windows.Forms.RadioButton
    Friend WithEvents RBHistory As System.Windows.Forms.RadioButton
End Class
