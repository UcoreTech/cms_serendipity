﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmPrint

    Private rptsummary As New ReportDocument
    Private gcon As New Clsappconfiguration

    Private Sub frmPrint_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        CrvRpt.PrintReport()
    End Sub

    Public Sub LoadREport(ByVal empno As String)
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\StatementofAccounts.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@EmployeeNo", empno)
            rptsummary.SetParameterValue("@IDNo", empno, "SOA_Debit.rpt")
            rptsummary.SetParameterValue("@IDNo", empno, "SOA_Credit.rpt")
            rptsummary.SetParameterValue("@IDNo", empno, "SOA_General.rpt")
            rptsummary.SetParameterValue("@IDNo", empno, "SOA_Comakers.rpt")
            rptsummary.SetParameterValue("@EmployeeNo", empno, "LoanLedger_LoanHistory.rpt")
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try

        CrvRpt.ReportSource = rptsummary
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Public Sub LoadLoanAmortization(ByVal empno As String, ByVal loanno As String)
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\LoanAmortization.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@EmployeeNo", empno)
            rptsummary.SetParameterValue("@LoanNo", loanno)
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
        CrvRpt.ReportSource = rptsummary
    End Sub

    Public Sub LoadLoanLedger(ByVal loanno As String)
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\LoanLedgers\LoanLedger1.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@LoanNo", loanno)
            rptsummary.SetParameterValue("@LoanNo", loanno, "SubReport1.rpt")
            'rptsummary.SetParameterValue("@EmployeeNo", empno)
            'rptsummary.SetParameterValue("@EmployeeNo", empno, "LoanLedger_SummaryAmountsDue2.rpt")
            'rptsummary.SetParameterValue("@EmployeeNo", empno, "LoanLedger_LoanHistory.rpt")
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
        CrvRpt.ReportSource = rptsummary
    End Sub
    Public Sub LoadLoanLedger2(ByVal loanno As String)
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\LoanLedgers\LoanLedger2.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@LoanNo", loanno)
            rptsummary.SetParameterValue("@LoanNo", loanno, "SubReport2.rpt")
            'rptsummary.SetParameterValue("@EmployeeNo", empno)
            'rptsummary.SetParameterValue("@EmployeeNo", empno, "LoanLedger_SummaryAmountsDue2.rpt")
            'rptsummary.SetParameterValue("@EmployeeNo", empno, "LoanLedger_LoanHistory.rpt")
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
        CrvRpt.ReportSource = rptsummary
    End Sub

    Public Sub LoadLoanSoa(ByVal empno As String, ByVal dt As Date)
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\LoanLedger_SummaryAmountsDue.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@EmployeeNo", empno)
            rptsummary.SetParameterValue("@dtDate", dt)
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
        CrvRpt.ReportSource = rptsummary
    End Sub
End Class