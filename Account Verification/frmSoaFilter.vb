﻿Imports System
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmSoaFilter
    Private mycon As New Clsappconfiguration
    Public SoaNo As String

    Private Sub frmSoaFilter_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadSoaNo(cboClient.Text)
        Load_Client()
        dgvSoaNo.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        'dgvSoaNo.Rows(0).Cells(1).Style.Font = New Font("Verdana", 12, FontStyle.Regular)
    End Sub
    Private Sub LoadSoaNo(ByVal co_name As String)
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(mycon.cnstring, "SOA_No_USED",
                                          New SqlParameter("@co_name", co_name),
                                          New SqlParameter("@Emp_ID", frmVerification.lblIDno.Text))

            dgvSoaNo.DataSource = ds.Tables(0)
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error :" + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub
    Private Sub Load_Client()
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("SOA_No_Client", mycon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "Document")
            With cboClient
                .DisplayMember = "co_name"
                .DataSource = ds.Tables(0)
                .Text = "Select"
            End With
            mycon.sqlconn.Close()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub CancelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub OKToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OKToolStripMenuItem.Click
        Me.DialogResult = DialogResult.Yes
        Me.Close()
    End Sub

    Private Sub cboClient_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboClient.TextChanged
        Call LoadSoaNo(cboClient.Text)
    End Sub

    Private Sub dgvSoaNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSoaNo.Click
        Try
            SoaNo = dgvSoaNo.SelectedRows(0).Cells(0).Value.ToString()
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Oops, Something went wrong!"
            frmMsgBox.ShowDialog()
        End Try
    End Sub
End Class