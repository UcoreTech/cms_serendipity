Public Class frmAgingOfLoans

#Region "Functions"
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next

        Return True
    End Function
#End Region

#Region "Load Report"
    Private Sub PreviewReport()
        
        Try
            Dim childform As Integer = 0
            Dim childforms(4) As CooperativeReports
            childform = 1
            childforms(childform) = New CooperativeReports()
            childforms(childform).Text = "Aging of Loans"
            Dim con As New clsPersonnel
            Dim appRdr As New System.Configuration.AppSettingsReader
            Dim myconnection As New Clsappconfiguration
            Dim objreport As New CrystalDecisions.CrystalReports.Engine.ReportDocument
            objreport.Load(Application.StartupPath & "\LoanReport\AgingOfLoans.rpt")
            Logon(objreport, con.servername, con.databasename, con.username1, con.password1)
            objreport.Refresh()
            objreport.SetParameterValue("@asOfDate", dteAgingOfLoans.Value.Date)
            childforms(childform).appreports.ReportSource = objreport
            childforms(childform).Show()

            Me.Close()
        Catch ex As Exception
            MessageBox.Show(MessageBox.Show(ex.Message), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
        
    End Sub
#End Region

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        PreviewReport()
    End Sub
End Class