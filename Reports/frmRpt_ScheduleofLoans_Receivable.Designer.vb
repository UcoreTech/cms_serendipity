﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRpt_ScheduleofLoans_Receivable
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CrvRpt = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.PanePanel5 = New WindowsApplication2.PanePanel()
        Me.cboLoanTypes = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.PanePanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'CrvRpt
        '
        Me.CrvRpt.ActiveViewIndex = -1
        Me.CrvRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        'Me.CrvRpt.CachedPageNumberPerDoc = 10
        Me.CrvRpt.Cursor = System.Windows.Forms.Cursors.Default
        Me.CrvRpt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrvRpt.Location = New System.Drawing.Point(0, 32)
        Me.CrvRpt.Name = "CrvRpt"
        Me.CrvRpt.SelectionFormula = ""
        Me.CrvRpt.ShowGroupTreeButton = False
        Me.CrvRpt.ShowRefreshButton = False
        Me.CrvRpt.Size = New System.Drawing.Size(975, 324)
        Me.CrvRpt.TabIndex = 64
        Me.CrvRpt.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.CrvRpt.ViewTimeSelectionFormula = ""
        '
        'PanePanel5
        '
        Me.PanePanel5.BackColor = System.Drawing.Color.White
        Me.PanePanel5.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PanePanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel5.Controls.Add(Me.cboLoanTypes)
        Me.PanePanel5.Controls.Add(Me.Label2)
        Me.PanePanel5.Controls.Add(Me.btnClose)
        Me.PanePanel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel5.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel5.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel5.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel5.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel5.Name = "PanePanel5"
        Me.PanePanel5.Size = New System.Drawing.Size(975, 32)
        Me.PanePanel5.TabIndex = 65
        '
        'cboLoanTypes
        '
        Me.cboLoanTypes.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanTypes.FormattingEnabled = True
        Me.cboLoanTypes.Location = New System.Drawing.Point(85, 6)
        Me.cboLoanTypes.Name = "cboLoanTypes"
        Me.cboLoanTypes.Size = New System.Drawing.Size(236, 21)
        Me.cboLoanTypes.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(11, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(77, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Loan Types:"
        '
        'btnClose
        '
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClose.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Location = New System.Drawing.Point(886, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(87, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "CLOSE"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmRpt_ScheduleofLoans_Receivable
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(975, 356)
        Me.Controls.Add(Me.CrvRpt)
        Me.Controls.Add(Me.PanePanel5)
        Me.Name = "frmRpt_ScheduleofLoans_Receivable"
        Me.Text = "Schedule of Loans Receivable - All Types"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.PanePanel5.ResumeLayout(False)
        Me.PanePanel5.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CrvRpt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents PanePanel5 As WindowsApplication2.PanePanel
    Friend WithEvents cboLoanTypes As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button
End Class
