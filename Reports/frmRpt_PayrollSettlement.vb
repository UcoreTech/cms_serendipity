Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmRpt_PayrollSettlement

    Private gcon As New Clsappconfiguration
    Private rptsummary As New ReportDocument

    Private payrollCode As String
    Private paymentDate As Date
    Private mode As String

    Public Property GetPayrollCode() As String
        Get
            Return payrollCode
        End Get
        Set(ByVal value As String)
            payrollCode = value
        End Set
    End Property
    Public Property GetPaymentDate() As Date
        Get
            Return paymentDate
        End Get
        Set(ByVal value As Date)
            paymentDate = value
        End Set
    End Property
    Public Property GetMode() As String
        Get
            Return mode
        End Get
        Set(ByVal value As String)
            mode = value
        End Set
    End Property

    Private Sub LoadReport()
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\PayrollSettlement_v2.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@payrollCode", GetPayrollCode())
            rptsummary.SetParameterValue("@date", Microsoft.VisualBasic.FormatDateTime(GetPaymentDate(), DateFormat.ShortDate))
        Catch ex As Exception
        End Try
    End Sub
    Private Sub LoadParameters()
        GetPayrollCode() = cboMultiPayrollCode.SelectedValue
        GetPaymentDate() = dtePaydate.Value.Date()
    End Sub
    Private Sub LoadPayrollCodes()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet

        cboMultiPayrollCode.Items.Clear()

        Try
            ds = SqlHelper.ExecuteDataset(mycon.cnstring, CommandType.StoredProcedure, "CIMS_m_Member_PayrollCode_Select")
            With cboMultiPayrollCode
                .LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
                .SourceDataString = New String(1) {"pk_PRollCode", "PRollDesc"}
                .SourceDataTable = ds.Tables(0)
            End With
        Catch ex As Exception
            MessageBox.Show("PayrollUploadtab", "Payroll Upload", MessageBoxButtons.OK)
        End Try
    End Sub

#Region "Form Events"
    Private Sub rmPayrollSettlement_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call LoadPayrollCodes()
        If GetMode() = "Module" Then
            picLoading.Visible = True
            cboMultiPayrollCode.SelectedValue = GetPayrollCode()
            dtePaydate.Value = GetPaymentDate()
            bgwLoadReport.RunWorkerAsync()
        End If
    End Sub

    Private Sub frmPayrollSettlement_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        'Prevents Memory leak for Crystal Reports
        rptsummary.Close()
    End Sub

    Private Sub btnLoadReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoadReport.Click
        Call LoadParameters()
        picLoading.Visible = True
        bgwLoadReport.RunWorkerAsync()
    End Sub
#End Region

#Region "Standard Report Functions"
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function
#End Region

#Region "Background Tasks"
    Private Sub bgwLoadReport_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwLoadReport.DoWork
        LoadReport()
    End Sub

    Private Sub bgwLoadReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwLoadReport.RunWorkerCompleted
        Me.crvPayrollSettlement.Visible = True
        Me.crvPayrollSettlement.ReportSource = rptsummary

        picLoading.Visible = False
    End Sub
#End Region

End Class