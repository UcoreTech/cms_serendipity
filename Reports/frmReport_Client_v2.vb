﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmReport_Client_v2


    Private rptsummary As New ReportDocument
    Private gcon As New Clsappconfiguration
    Public ReportMode As String = ""

#Region "Functions"
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next

        Return True
    End Function

    Private Sub LoadReport()
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\MemberList_Sortbyname.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            CrvRpt.ReportSource = rptsummary
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
#End Region

    Private Sub frmReport_Client_v2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadGroup()
        LoadSubgroup()
        LoadCategory()
        LoadType()
        LoadRank()
        LoadMemberStatus()
        'LoadReport()
        LoadYear()
        LoadMonth()
        LoadDate()
        Refresh_SortByname()
        rdbName.Checked = True
    End Sub

    Private Sub LoadSubgroup()
        cboSubGroup.Items.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_ClientMigration_Select_Subgroup")
        While rd.Read
            With cboSubGroup
                .Items.Add(rd(0))
            End With
        End While
        cboSubGroup.Text = "Select"
    End Sub

    Private Sub LoadDate()
        cboDate.Items.Clear()
        With cboDate
            For i As Integer = 1 To 31
                .Items.Add(i)
            Next
        End With
    End Sub

    Private Sub LoadMonth()
        cboMonth.Items.Clear()
        With cboMonth.Items
            .Add("January")
            .Add("February")
            .Add("March")
            .Add("April")
            .Add("May")
            .Add("June")
            .Add("July")
            .Add("August")
            .Add("September")
            .Add("October")
            .Add("November")
            .Add("December")
        End With
    End Sub

    Private Sub LoadGroup()
        cboGroup.Items.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_Group")
        While rd.Read
            With cboGroup
                .Items.Add(rd("Fc_GroupDesc"))
            End With
        End While
        cboGroup.Items.Add("All")
        cboGroup.Text = "Select"
    End Sub

    Private Sub LoadCategory()
        cboCategory.Items.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_Category_GroupAll")
        While rd.Read
            With cboCategory
                .Items.Add(rd("Fc_Category"))
            End With
        End While
        'cboCategory.Text = "Select"
    End Sub

    Private Sub LoadType()
        cboType.Items.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_Type_ALL")
        While rd.Read
            With cboType
                .Items.Add(rd("Fc_Type"))
            End With
        End While
        'cboType.Text = "Select"
    End Sub

    Private Sub LoadRank()
        cboRank.Items.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_Rank_Master")
        While rd.Read
            With cboRank
                .Items.Add(rd("Description"))
            End With
        End While
    End Sub

    Private Sub LoadMemberStatus()
        cboMemberStatus.Items.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "CIMS_m_Membership_Status_view")
        While rd.Read
            With cboMemberStatus
                .Items.Add(rd("Status"))
            End With
        End While
        cboMemberStatus.Text = "Select"
    End Sub

    Private Sub LoadYear()
        cboYear.Items.Clear()
        With cboYear
            For i As Integer = 0 To 150
                .Items.Add(Date.Now.Year - i)
            Next
        End With
    End Sub

    Private Sub chkInclude_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkInclude.CheckedChanged
        Select Case chkInclude.Checked
            Case True
                GroupBox4.Enabled = True
            Case False
                GroupBox4.Enabled = False
        End Select
    End Sub

    Public Sub LoadReport_Groups()
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\ClientReport_AllGroups_v2.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@xGroup", cboGroup.Text)
            rptsummary.SetParameterValue("@Category", cboCategory.Text)
            rptsummary.SetParameterValue("@Type", cboType.Text)
            rptsummary.SetParameterValue("@Rank", cboRank.Text)
            rptsummary.SetParameterValue("@Status", cboMemberStatus.Text)
            rptsummary.SetParameterValue("@Subgroup", cboSubgroup.Text)
            CrvRpt.ReportSource = rptsummary
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub cboGroup_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGroup.SelectedValueChanged
        LoadReport_Groups()
    End Sub

    Private Sub cboCategory_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCategory.SelectedValueChanged
        LoadReport_Groups()
    End Sub

    Private Sub cboType_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboType.SelectedValueChanged
        LoadReport_Groups()
    End Sub

    Private Sub cboMemberStatus_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMemberStatus.SelectedValueChanged
        LoadReport_Groups()
    End Sub

    Private Sub cboRank_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRank.SelectedValueChanged
        LoadReport_Groups()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        CrvRpt.PrintReport()
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        CrvRpt.ExportReport()
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        LoadGroup()
        LoadSubgroup()
        LoadCategory()
        LoadType()
        LoadRank()
        LoadMemberStatus()
        'LoadReport()
        LoadYear()
        LoadMonth()
        LoadDate()
        rdbName.Checked = True
        Refresh_SortByname()
    End Sub

    Private Sub Refresh_SortByname()
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\ClientReport_AllGroups_v2.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@xGroup", "All")
            rptsummary.SetParameterValue("@Category", cboCategory.Text)
            rptsummary.SetParameterValue("@Type", cboType.Text)
            rptsummary.SetParameterValue("@Rank", cboRank.Text)
            rptsummary.SetParameterValue("@Status", cboMemberStatus.Text)
            rptsummary.SetParameterValue("@Subgroup", cboSubgroup.Text)
            CrvRpt.ReportSource = rptsummary
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub rdbCode_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbCode.Click
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\ClientReport_Sortbycode_v2.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@xGroup", "All")
            rptsummary.SetParameterValue("@Category", cboCategory.Text)
            rptsummary.SetParameterValue("@Type", cboType.Text)
            rptsummary.SetParameterValue("@Rank", cboRank.Text)
            rptsummary.SetParameterValue("@Status", cboMemberStatus.Text)
            CrvRpt.ReportSource = rptsummary
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub rdbName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbName.Click
        Refresh_SortByname()
    End Sub

    Private Sub FilterBirthdate()
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\ClientReport_FilterBirthdate_v2.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@xGroup", cboGroup.Text)
            rptsummary.SetParameterValue("@Category", cboCategory.Text)
            rptsummary.SetParameterValue("@Type", cboType.Text)
            rptsummary.SetParameterValue("@Rank", cboRank.Text)
            rptsummary.SetParameterValue("@Status", cboMemberStatus.Text)
            rptsummary.SetParameterValue("@Year", cboYear.Text)
            rptsummary.SetParameterValue("@Month", cboMonth.Text)
            rptsummary.SetParameterValue("@Date", cboDate.Text)
            rptsummary.SetParameterValue("@Subgroup", cboSubgroup.Text)
            CrvRpt.ReportSource = rptsummary
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub cboYear_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboYear.SelectedValueChanged
        FilterBirthdate()
    End Sub

    Private Sub cboMonth_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMonth.SelectedValueChanged
        FilterBirthdate()
    End Sub

    Private Sub cboDate_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDate.SelectedValueChanged
        FilterBirthdate()
    End Sub

    Private Sub Filterby_DateRanged()
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\ClientReport_DateRanged_v2.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@from", dtpfrom.Value)
            rptsummary.SetParameterValue("@To", dtpTo.Value)
            CrvRpt.ReportSource = rptsummary
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub btnGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        Filterby_DateRanged()
    End Sub

    Private Sub cboSubgroup_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSubgroup.SelectedValueChanged
        LoadReport_Groups()
    End Sub

End Class