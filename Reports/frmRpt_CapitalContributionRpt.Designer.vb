<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRpt_CapitalContributionRpt
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRpt_CapitalContributionRpt))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.btnGenerate = New System.Windows.Forms.Button
        Me.DtDate = New System.Windows.Forms.DateTimePicker
        Me.Label1 = New System.Windows.Forms.Label
        Me.CrvRpt = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.BGWorker = New System.ComponentModel.BackgroundWorker
        Me.Timer = New System.Windows.Forms.Timer(Me.components)
        Me.lblLoading = New System.Windows.Forms.Label
        Me.MtcboMembers = New MTGCComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.MtcboMembers)
        Me.Panel1.Controls.Add(Me.btnGenerate)
        Me.Panel1.Controls.Add(Me.DtDate)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(649, 32)
        Me.Panel1.TabIndex = 0
        '
        'btnGenerate
        '
        Me.btnGenerate.Image = Global.WindowsApplication2.My.Resources.Resources.apply
        Me.btnGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGenerate.Location = New System.Drawing.Point(404, 3)
        Me.btnGenerate.Name = "btnGenerate"
        Me.btnGenerate.Size = New System.Drawing.Size(87, 27)
        Me.btnGenerate.TabIndex = 2
        Me.btnGenerate.Text = "Generate"
        Me.btnGenerate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnGenerate.UseVisualStyleBackColor = True
        '
        'DtDate
        '
        Me.DtDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DtDate.Location = New System.Drawing.Point(283, 5)
        Me.DtDate.Name = "DtDate"
        Me.DtDate.Size = New System.Drawing.Size(115, 23)
        Me.DtDate.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(242, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Date:"
        '
        'CrvRpt
        '
        Me.CrvRpt.ActiveViewIndex = -1
        Me.CrvRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrvRpt.DisplayGroupTree = False
        Me.CrvRpt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrvRpt.Location = New System.Drawing.Point(0, 32)
        Me.CrvRpt.Name = "CrvRpt"
        Me.CrvRpt.SelectionFormula = ""
        Me.CrvRpt.ShowCloseButton = False
        Me.CrvRpt.ShowGroupTreeButton = False
        Me.CrvRpt.ShowRefreshButton = False
        Me.CrvRpt.Size = New System.Drawing.Size(649, 370)
        Me.CrvRpt.TabIndex = 1
        Me.CrvRpt.ViewTimeSelectionFormula = ""
        '
        'BGWorker
        '
        '
        'Timer
        '
        Me.Timer.Interval = 1
        '
        'lblLoading
        '
        Me.lblLoading.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblLoading.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoading.Image = Global.WindowsApplication2.My.Resources.Resources.LoadingData
        Me.lblLoading.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblLoading.Location = New System.Drawing.Point(270, 154)
        Me.lblLoading.Name = "lblLoading"
        Me.lblLoading.Size = New System.Drawing.Size(101, 113)
        Me.lblLoading.TabIndex = 2
        Me.lblLoading.Text = "Loading . . ."
        Me.lblLoading.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.lblLoading.Visible = False
        '
        'MtcboMembers
        '
        Me.MtcboMembers.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.MtcboMembers.ArrowColor = System.Drawing.Color.Black
        Me.MtcboMembers.BindedControl = CType(resources.GetObject("MtcboMembers.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.MtcboMembers.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.MtcboMembers.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.MtcboMembers.ColumnNum = 3
        Me.MtcboMembers.ColumnWidth = "200; 75; 0"
        Me.MtcboMembers.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.MtcboMembers.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.MtcboMembers.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.MtcboMembers.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.MtcboMembers.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.MtcboMembers.DisplayMember = "Text"
        Me.MtcboMembers.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.MtcboMembers.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.MtcboMembers.DropDownForeColor = System.Drawing.Color.Black
        Me.MtcboMembers.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDown
        Me.MtcboMembers.DropDownWidth = 295
        Me.MtcboMembers.GridLineColor = System.Drawing.Color.LightGray
        Me.MtcboMembers.GridLineHorizontal = False
        Me.MtcboMembers.GridLineVertical = False
        Me.MtcboMembers.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.MtcboMembers.Location = New System.Drawing.Point(73, 4)
        Me.MtcboMembers.ManagingFastMouseMoving = True
        Me.MtcboMembers.ManagingFastMouseMovingInterval = 30
        Me.MtcboMembers.Name = "MtcboMembers"
        Me.MtcboMembers.SelectedItem = Nothing
        Me.MtcboMembers.SelectedValue = Nothing
        Me.MtcboMembers.Size = New System.Drawing.Size(163, 24)
        Me.MtcboMembers.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 15)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Member:"
        '
        'frmCapitalContributionRpt
        '
        Me.AcceptButton = Me.btnGenerate
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(649, 402)
        Me.Controls.Add(Me.lblLoading)
        Me.Controls.Add(Me.CrvRpt)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmCapitalContributionRpt"
        Me.ShowIcon = False
        Me.Text = "Capital Contribution Report"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnGenerate As System.Windows.Forms.Button
    Friend WithEvents DtDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CrvRpt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents BGWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents Timer As System.Windows.Forms.Timer
    Friend WithEvents lblLoading As System.Windows.Forms.Label
    Friend WithEvents MtcboMembers As MTGCComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
