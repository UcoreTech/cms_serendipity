<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CooperativeReports
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CooperativeReports))
        Me.appreports = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.SuspendLayout()
        '
        'appreports
        '
        Me.appreports.ActiveViewIndex = -1
        Me.appreports.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.appreports.Dock = System.Windows.Forms.DockStyle.Fill
        Me.appreports.Location = New System.Drawing.Point(0, 0)
        Me.appreports.Name = "appreports"
        Me.appreports.SelectionFormula = ""
        Me.appreports.ShowCloseButton = False
        Me.appreports.ShowGroupTreeButton = False
        Me.appreports.ShowParameterPanelButton = False
        Me.appreports.ShowRefreshButton = False
        Me.appreports.Size = New System.Drawing.Size(683, 497)
        Me.appreports.TabIndex = 0
        Me.appreports.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.appreports.ViewTimeSelectionFormula = ""
        '
        'CooperativeReports
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(683, 497)
        Me.Controls.Add(Me.appreports)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "CooperativeReports"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reports"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents appreports As CrystalDecisions.Windows.Forms.CrystalReportViewer
End Class
