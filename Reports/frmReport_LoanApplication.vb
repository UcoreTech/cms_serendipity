﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmReport_LoanApplication

    Private rptsummary As New ReportDocument
    Private gcon As New Clsappconfiguration

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frmReport_LoanApplication_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Public Sub LoadREport(ByVal loanno As String)
        Try
            Me.Text = "Loan Agreement"
            rptsummary.Load(Application.StartupPath & "\LoanReport\LoanAgreement.rpt")
            'rptsummary.Load(Application.StartupPath & "\LoanReport\BIRReport\PromissoryNote.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@LoanNo", loanno)
            CrvRpt.ReportSource = rptsummary
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Public Sub LoadREport2(ByVal loanno As String)
        Try
            Me.Text = "Loan Application Form"
            rptsummary.Load(Application.StartupPath & "\LoanReport\LoanApplication.rpt")
            'rptsummary.Load(Application.StartupPath & "\LoanReport\BIRReport\CreditSlip_Application.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@LoanNo", loanno)
            rptsummary.SetParameterValue("@Company", frmMain.lblCoopName.Text)
            CrvRpt.ReportSource = rptsummary
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Public Sub LoadREport_Cash(ByVal loanno As String, ByVal CompanyName As String)
        Try
            Me.Text = "Loan Application Form"
            rptsummary.Load(Application.StartupPath & "\LoanReport\LoanApplication.rpt")
            'rptsummary.Load(Application.StartupPath & "\LoanReport\BIRReport\CashLoan_Application.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@LoanNo", loanno)
            rptsummary.SetParameterValue("@Company", CompanyName)
            CrvRpt.ReportSource = rptsummary
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub btnSignatories_Click(sender As System.Object, e As System.EventArgs) Handles btnSignatories.Click
        frmVoucherSignatories.ShowDialog()
    End Sub
End Class