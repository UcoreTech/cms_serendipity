<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmContributionBalanceMaster
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.crvContributionBalance = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.picLoading = New System.Windows.Forms.PictureBox
        Me.bgwContributionBalance = New System.ComponentModel.BackgroundWorker
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'crvContributionBalance
        '
        Me.crvContributionBalance.ActiveViewIndex = -1
        Me.crvContributionBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvContributionBalance.DisplayGroupTree = False
        Me.crvContributionBalance.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvContributionBalance.Location = New System.Drawing.Point(0, 0)
        Me.crvContributionBalance.Name = "crvContributionBalance"
        Me.crvContributionBalance.SelectionFormula = ""
        Me.crvContributionBalance.ShowGroupTreeButton = False
        Me.crvContributionBalance.Size = New System.Drawing.Size(659, 468)
        Me.crvContributionBalance.TabIndex = 0
        Me.crvContributionBalance.ViewTimeSelectionFormula = ""
        '
        'picLoading
        '
        Me.picLoading.Image = Global.WindowsApplication2.My.Resources.Resources.LoadingData
        Me.picLoading.Location = New System.Drawing.Point(12, 38)
        Me.picLoading.Name = "picLoading"
        Me.picLoading.Size = New System.Drawing.Size(47, 45)
        Me.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picLoading.TabIndex = 3
        Me.picLoading.TabStop = False
        '
        'bgwContributionBalance
        '
        '
        'frmContributionBalanceMaster
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(659, 468)
        Me.Controls.Add(Me.picLoading)
        Me.Controls.Add(Me.crvContributionBalance)
        Me.Name = "frmContributionBalanceMaster"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Contribution Balance Master"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents crvContributionBalance As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents picLoading As System.Windows.Forms.PictureBox
    Friend WithEvents bgwContributionBalance As System.ComponentModel.BackgroundWorker
End Class
