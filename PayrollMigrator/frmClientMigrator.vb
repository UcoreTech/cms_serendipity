﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmClientMigrator
    'Module : Client Payroll Migrator
    'Programmer : Vincent Nacar
    'Date Start : 8/9/14

    Private con As New Clsappconfiguration

    Private Sub frmClientMigrator_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadAllComboxes()
        PrepareClients("", "", "", "")
    End Sub

    Private Sub AddSelectButton()
        Dim btnselect As New DataGridViewCheckBoxColumn
        With btnselect
            .Name = "btnSelect"
            .HeaderText = "Select"
        End With
        dgvList.Columns.Add(btnselect)
    End Sub

    Private Sub PrepareClients(ByVal group As String,
                               ByVal subgroup As String,
                               ByVal category As String,
                               ByVal type As String)
        dgvList.Columns.Clear()
        AddSelectButton()
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_Prepare_Clients_List_For_PAyroll",
                                          New SqlParameter("@xGroup", group),
                                          New SqlParameter("@Subgroup", subgroup),
                                          New SqlParameter("@Category", category),
                                          New SqlParameter("@Type", type))
            dgvList.DataSource = ds.Tables(0)
            dgvList.Columns(0).Width = 50
            dgvList.Columns(1).Width = 200
        Catch ex As Exception

        End Try
    End Sub

    Private Sub PrepareClients_byLocation(ByVal id As String)
        dgvList.Columns.Clear()
        AddSelectButton()
        'Try
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(con.cnstring, "_Prepare_Clients_List_For_PAyroll_byLocation",
                                      New SqlParameter("@Location", id))
        dgvList.DataSource = ds.Tables(0)
        dgvList.Columns(0).Width = 50
        dgvList.Columns(1).Width = 200
        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub TransferClient_ToPayroll()
        Dim ctr As Integer = 0
        'Try
        For i As Integer = 0 To dgvList.RowCount - 1
            If dgvList.Rows(i).Cells(0).Value = True Then
                'code here
                With dgvList
                    Dim idno As String = .Rows(i).Cells(2).Value.ToString
                    Dim lastname As String = .Rows(i).Cells(3).Value.ToString
                    Dim firstname As String = .Rows(i).Cells(4).Value.ToString
                    Dim middlename As String = .Rows(i).Cells(5).Value.ToString
                    Dim bdate As Date = .Rows(i).Cells(6).Value
                    Dim datehired As Date = .Rows(i).Cells(7).Value
                    Dim basicpay As String = .Rows(i).Cells(8).Value.ToString
                    Dim ecola As String = .Rows(i).Cells(9).Value.ToString
                    Dim contractprice As String = .Rows(i).Cells(10).Value.ToString
                    Dim sss As String = .Rows(i).Cells(11).Value.ToString
                    Dim tin As String = .Rows(i).Cells(12).Value.ToString
                    Dim hdmf As String = .Rows(i).Cells(13).Value.ToString
                    Dim phic As String = .Rows(i).Cells(14).Value.ToString
                    Dim gender As String = .Rows(i).Cells(15).Value.ToString
                    Dim officeno As String = .Rows(i).Cells(16).Value.ToString
                    Dim mobileno As String = .Rows(i).Cells(17).Value.ToString
                    Dim email As String = .Rows(i).Cells(18).Value.ToString
                    Dim Status As String = .Rows(i).Cells(19).Value.ToString
                    Dim SalaryRate As String = .Rows(i).Cells(20).Value.ToString
                    Dim Position As String = .Rows(i).Cells(21).Value.ToString
                    Dim PositionLevel As String = .Rows(i).Cells(22).Value.ToString
                    Dim ContractRate As String = .Rows(i).Cells(23).Value.ToString
                    Dim Supervisor As String = .Rows(i).Cells(23).Value.ToString

                    SqlHelper.ExecuteNonQuery(con.cnstring, "_Integrate_Client_To_Payroll",
                                               New SqlParameter("@IDNo", idno),
                                               New SqlParameter("@LastName", lastname),
                                               New SqlParameter("@FirstName", firstname),
                                               New SqlParameter("@MiddleName", middlename),
                                               New SqlParameter("@Birthdate", bdate),
                                               New SqlParameter("@DateHired", datehired),
                                               New SqlParameter("@BasicPAy", CDec(basicpay)),
                                               New SqlParameter("@ECOLA", CDec(ecola)),
                                               New SqlParameter("@ContractPrice", CDec(contractprice)),
                                               New SqlParameter("@SSS", sss),
                                               New SqlParameter("@TIN", tin),
                                               New SqlParameter("@HDMF", hdmf),
                                               New SqlParameter("@PHIC", phic),
                                               New SqlParameter("@Gender", gender),
                                               New SqlParameter("@OfficeNo", officeno),
                                               New SqlParameter("@MobileNo", mobileno),
                                               New SqlParameter("@Email", email),
                                               New SqlParameter("@orgID", txtLocation.Tag),
                                               New SqlParameter("@Status", Status),
                                               New SqlParameter("@SalaryCategoryDesc", SalaryRate),
                                               New SqlParameter("@Position", Position),
                                               New SqlParameter("@PositionType", PositionLevel),
                                               New SqlParameter("@fcContractCode", ContractRate),
                                               New SqlParameter("@fcSupervisor", Supervisor))
                    ' Init_Destination(idno)
                End With

                ctr += 1
            End If
        Next

        frmMsgBox.Text = "Done!"
        frmMsgBox.txtMessage.Text = "Total No. of Items Transferred: " + ctr.ToString
        frmMsgBox.ShowDialog()
        'Catch ex As Exception
        '    frmMsgBox.txtMessage.Text = "Oops, Something went wrong!  - " + ex.ToString
        '    frmMsgBox.ShowDialog()
        'End Try
    End Sub

    Private Sub Init_Destination(ByVal id As String)
        Dim pos As Integer = 0
        If cboSubGroup.Text = "" And cboCategory.Text = "" And cboType.Text = "" Then
            pos = 1
        End If
        If cboCategory.Text = "" And cboType.Text = "" Then
            pos = 2
        End If
        If cboType.Text = "" Then
            pos = 3
        End If
        If cboGroup.Text <> "" And cboSubGroup.Text <> "" And cboCategory.Text <> "" And cboType.Text <> "" Then
            pos = 4
        End If
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Generate_PAyroll_Destination",
                                      New SqlParameter("@xGroup", cboGroup.Text),
                                      New SqlParameter("@Subgroup", cboSubGroup.Text),
                                      New SqlParameter("@Category", cboCategory.Text),
                                      New SqlParameter("@Type", cboType.Text),
                                      New SqlParameter("@ID", id),
                                      New SqlParameter("@pos", pos))
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnCLose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCLose.Click
        Me.Close()
    End Sub

    Private Sub dgvList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.Click
        Try
            If dgvList.SelectedRows(0).Cells(0).Value = True Then
                dgvList.SelectedRows(0).Cells(0).Value = False
                dgvList.SelectedRows(0).DefaultCellStyle.BackColor = Color.White
            Else
                dgvList.SelectedRows(0).Cells(0).Value = True
                dgvList.SelectedRows(0).DefaultCellStyle.BackColor = Color.SkyBlue
            End If
        Catch
        End Try
    End Sub

    Private Sub chkSelectAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelectAll.Click
        If chkSelectAll.Checked = True Then
            For i As Integer = 0 To dgvList.RowCount - 1
                dgvList.Rows(i).Cells(0).Value = True
                dgvList.Rows(i).DefaultCellStyle.BackColor = Color.SkyBlue
            Next
        Else
            For i As Integer = 0 To dgvList.RowCount - 1
                dgvList.Rows(i).Cells(0).Value = False
                dgvList.Rows(i).DefaultCellStyle.BackColor = Color.White
            Next
        End If
    End Sub

    Private Sub LoadAllComboxes()
        Try
            LoadGroup()
            LoadSubgroup()
            LoadCategory()
            LoadType()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub LoadGroup()
        cboGroup.Items.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(con.cnstring, "_Select_Group")
        While rd.Read
            With cboGroup
                .Items.Add(rd("Fc_GroupDesc"))
            End With
        End While
        cboGroup.Text = "Select"
    End Sub

    Private Sub LoadCategory()
        cboCategory.Items.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(con.cnstring, "_Select_Category_GroupAll")
        While rd.Read
            With cboCategory
                .Items.Add(rd("Fc_Category"))
            End With
        End While
        cboCategory.Text = "Select"
    End Sub

    Private Sub LoadType()
        cboType.Items.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(con.cnstring, "_Select_Type_ALL")
        While rd.Read
            With cboType
                .Items.Add(rd("Fc_Type"))
            End With
        End While
        cboType.Text = "Select"
    End Sub

    Private Sub LoadSubgroup()
        cboSubGroup.Items.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(con.cnstring, "_ClientMigration_Select_Subgroup")
        While rd.Read
            With cboSubGroup
                .Items.Add(rd(0))
            End With
        End While
        cboSubGroup.Text = "Select"
    End Sub

    Private Function LocateClients()
        Try
            Return cboGroup.Text + "\" + cboSubGroup.Text + "\" + cboCategory.Text + "\" + cboType.Text
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Private Sub cboGroup_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGroup.SelectedValueChanged
        PrepareClients(cboGroup.Text, cboSubGroup.Text, cboCategory.Text, cboType.Text)
    End Sub

    Private Sub cboSubGroup_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSubGroup.SelectedValueChanged
        PrepareClients(cboGroup.Text, cboSubGroup.Text, cboCategory.Text, cboType.Text)
    End Sub

    Private Sub cboCategory_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCategory.SelectedValueChanged
        PrepareClients(cboGroup.Text, cboSubGroup.Text, cboCategory.Text, cboType.Text)
    End Sub

    Private Sub cboType_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboType.SelectedValueChanged
        PrepareClients(cboGroup.Text, cboSubGroup.Text, cboCategory.Text, cboType.Text)
    End Sub

    Private Sub txtLocation_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLocation.TextChanged
        PrepareClients(cboGroup.Text, cboSubGroup.Text, cboCategory.Text, cboType.Text)
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        LoadAllComboxes()
        PrepareClients("", "", "", "")
    End Sub

    Private Sub btnTransfer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTransfer.Click
        If cboGroup.Text = "" And txtLocation.Text = "" Then
            frmMsgBox.txtMessage.Text = "Unable to proceed!, Please Select Location"
            frmMsgBox.ShowDialog()
        Else
            TransferClient_ToPayroll()
        End If
    End Sub

    Private Sub btnBrowseLocation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseLocation.Click
        Dim orgchart As New frmOrgChart_Master(orgchart.selection.Org_Emp)
        orgchart.Width = 257
        orgchart.btnClose.Location = New System.Drawing.Point(179, 6)
        orgchart.btnAdd.Enabled = False
        orgchart.btnPrint.Visible = False
        orgchart.btnPrint.Visible = False
        If orgchart.ShowDialog = Windows.Forms.DialogResult.Yes Then
            Me.txtLocation.Text = orgchart.org_parent_desc
            Me.txtLocation.Tag = orgchart.orgid
            'Me.txtDepartment2.Text = orgchart.department
            Me.txtLocation.Text = Mid(Me.txtLocation.Text, 21)
            Call proposeddeptdivision(Me.txtLocation.Tag)
        End If
        PrepareClients_byLocation(txtLocation.Tag)
    End Sub

    Public Sub proposeddeptdivision(ByVal m As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_orgchart_getid", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            myconnection.sqlconn.Open()
            With cmd.Parameters
                .Add("@getid", SqlDbType.VarChar, 10).Value = m
            End With
            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Dim x As String

            Try
                While myreader.Read
                    x = myreader.Item(0)
                End While
                myreader.Close()

                Dim arr As String() = x.Split("\".ToCharArray())
                For i As Integer = 0 To (arr.Length - 1)
                    'MsgBox(arr(2))
                    'MsgBox(arr(3))
                    'MsgBox(arr(4))
                    'MsgBox(arr(5))
                Next

            Finally
                myconnection.sqlconn.Close()
            End Try
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class