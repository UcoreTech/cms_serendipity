﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAmort_ColSetup
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAmort_ColSetup))
        Me.dgvList = New System.Windows.Forms.DataGridView()
        Me.btnAddCol = New System.Windows.Forms.Button()
        Me.btnRemoveCol = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvList
        '
        Me.dgvList.AllowUserToAddRows = False
        Me.dgvList.AllowUserToDeleteRows = False
        Me.dgvList.AllowUserToResizeColumns = False
        Me.dgvList.AllowUserToResizeRows = False
        Me.dgvList.BackgroundColor = System.Drawing.Color.White
        Me.dgvList.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvList.Location = New System.Drawing.Point(11, 12)
        Me.dgvList.Name = "dgvList"
        Me.dgvList.Size = New System.Drawing.Size(744, 238)
        Me.dgvList.TabIndex = 0
        '
        'btnAddCol
        '
        Me.btnAddCol.Image = Global.WindowsApplication2.My.Resources.Resources.edit_add
        Me.btnAddCol.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAddCol.Location = New System.Drawing.Point(11, 256)
        Me.btnAddCol.Name = "btnAddCol"
        Me.btnAddCol.Size = New System.Drawing.Size(120, 23)
        Me.btnAddCol.TabIndex = 1
        Me.btnAddCol.Text = "Add Column"
        Me.btnAddCol.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAddCol.UseVisualStyleBackColor = True
        '
        'btnRemoveCol
        '
        Me.btnRemoveCol.Image = CType(resources.GetObject("btnRemoveCol.Image"), System.Drawing.Image)
        Me.btnRemoveCol.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRemoveCol.Location = New System.Drawing.Point(137, 256)
        Me.btnRemoveCol.Name = "btnRemoveCol"
        Me.btnRemoveCol.Size = New System.Drawing.Size(133, 23)
        Me.btnRemoveCol.TabIndex = 2
        Me.btnRemoveCol.Text = "Remove Column"
        Me.btnRemoveCol.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRemoveCol.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Image = CType(resources.GetObject("Button3.Image"), System.Drawing.Image)
        Me.Button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button3.Location = New System.Drawing.Point(680, 256)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 3
        Me.Button3.Text = "Close"
        Me.Button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button3.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Image = Global.WindowsApplication2.My.Resources.Resources.save2
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(276, 256)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 4
        Me.btnSave.Text = "Save"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'frmAmort_ColSetup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(767, 286)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.btnRemoveCol)
        Me.Controls.Add(Me.btnAddCol)
        Me.Controls.Add(Me.dgvList)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmAmort_ColSetup"
        Me.Text = "Amortization Columns Setup"
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvList As System.Windows.Forms.DataGridView
    Friend WithEvents btnAddCol As System.Windows.Forms.Button
    Friend WithEvents btnRemoveCol As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
End Class
