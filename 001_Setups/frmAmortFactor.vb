﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmAmortFactor

    Private con As New Clsappconfiguration

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub frmAmortFactor_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        _LoadData()
    End Sub
    Private Sub _LoadCol()
        dgvList.Rows.Clear()
        dgvList.Columns.Clear()
        Dim terms As New DataGridViewTextBoxColumn
        Dim factor As New DataGridViewTextBoxColumn
        With terms
            .Name = "terms"
            .HeaderText = "Terms in Months"
        End With
        With factor
            .Name = "factor"
            .HeaderText = "Factor"
        End With
        With dgvList
            .Columns.Add(terms)
            .Columns.Add(factor)
        End With
    End Sub

    Private Sub _InsertData(ByVal terms As Integer, ByVal rate As Decimal)
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_TblFactor_Insert",
                                       New SqlParameter("@fnTermsinMonths", terms),
                                       New SqlParameter("@fdFactorRate", rate))
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnAddRows_Click(sender As System.Object, e As System.EventArgs) Handles btnAddRows.Click
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_TblFactor_Delete")
            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList.Rows(i)
                    _InsertData(CInt(.Cells(0).Value), CDec(.Cells(1).Value))
                End With
            Next
            MsgBox("Changes Saved!", vbInformation, "...")
            _LoadData()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub _LoadData()
        Try
            _LoadCol()
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_TblFactor_Load")
            If rd.HasRows Then
                While rd.Read
                    dgvList.Rows.Add(rd(0), rd(1))
                End While
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnRemove_Click(sender As System.Object, e As System.EventArgs) Handles btnRemove.Click
        dgvList.Rows.Remove(dgvList.CurrentRow)
    End Sub

End Class