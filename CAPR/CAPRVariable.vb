Imports system.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Module CAPRVariable
    Private gCon As New Clsappconfiguration
    Public xSectionSwitch As Boolean
    Public xFormItro As FrmCAPRIntro
    Public xFormSec1 As FrmCAPRSecI
    Public xFormSec2 As frmCAPRSecII
    Public xFormSec3 As frmCAPRSecIII
    Public xFormSec3b As frmCAPRSecIIIb
    Public xFormSec4 As FrmCAPRSecIV

    Public ReceivedBy, ValidateBy, EncodedBy, Verifiedby As String
    Public DateReceived, DateValidated, DateEncoded, DateVerified As Date
    Public AsOfDateYear As Integer
    Public CINumber, CoopNameLatestAmendment, RegConfirmNo, PresentCoopAddress As String
    Public CoopType, BussActEngaged, Box3OtherSPecified As String
    Public Box3FinancialInterm, Box3Mining, Box3Construction As Boolean
    Public Box3TransStorageComm, Box3RealEstateRentBussAct, Box3HealthSocialWork As Boolean
    Public Box3Fishing, Box3ElecGasWater, Box3Education As Boolean
    Public Box3AgriHuntForst, Box3Manufctring, Box3HotelResto As Boolean
    Public Box3WholeSaleRetail, Box3Others As Boolean
    Public ContactPerson, PhoneNo, Email, FAX As String

    Public IHTargetMale, IHTargeFemale As Integer
    Public Ii1a, Ii1b, Ii1c, Ii1d As String
    Public Ii2a, Ii2b, Ii2c, Ii2d As String
    Public Ii3a, I1bif As String
    Public Ii4a, Ii4c, Ii4d As String
    Public Ii4bi, Ii4bii, Ii4biii As String
    Public Ii5a, Ii5b, Ii5c, Ii5d, Ii5e As String
    Public Ii6a, Ii6b As String
    Public Ii7a, Ii7aIf As String
    Public Ii7b, Ii7c, Ii7d, Ii7e As String
    Public Ii8a, Ii8b, Ii8c, Ii8d As String
    Public Ii8ei, Ii8eii, Ii8eiii As String
    Public Ii8fi, Ii8fii, Ii8fiii, Ii8fiv, Ii8fv As String
    Public Ii9a, Ii9b, Ii9c As String
    Public Ii9d, Ii9e, Ii9f As String
    Public Ii9g, Ii9h, Ii9i As String
    Public Ii9j, Ii9k As String
    Public Ii10a, Ii10b, Ii10c As String
    Public Ii11 As String
    Public Ij1a As Integer
    Public Ij1b, Ij2a As Date
    Public Ij1cName, Ij1cDesignation As String
    Public Ij2b, Ij2c, Ij3 As String

    Public IIFinancialCondition, IIAssets, IILoanReceivable, IIAReceivable As Decimal
    Public IILiabilities, IIPaidUpShreCap, IIDonationsGrants As Decimal
    Public IIReserves, IIGenReserve, IIOptnalReserve, IICETF As Decimal
    Public IIVolTransction, IILoanReleases, IISalesGrossReceipt As Decimal
    Public IIOperation, IIRevenues, IIExpenses As Decimal
    Public IIindividualNetSurplus, IIAllocGenResrveFnd, IIAllocOptnlRsrveFnd, IIAllocCETF As Decimal
    Public IIAmntDistribution, IIinterestShreCap, IIPatrongeRefnd As Decimal

    Public VLongTerm1, VLongTerm2, VLongTerm3 As Decimal
    Public VInvst1, VInvst2, VInvst3 As Decimal
    Public VGovernSec1, VGovernSec2, VGovernSec3 As Decimal
    Public VBColType, VBAmnt1, VBAmnt2, VBAmnt3 As String
    Public VCForeignAmnt, VCConvRte, VCLocCurAmt As Decimal
    Public VCGovFinInst, VCPrivFinIns As Decimal
    Public VCCreditSource, VCAmnt As String

    Public VDWithdwble1, VDWithdwble2, VDWithdwble4 As Integer
    Public VDWithdwble5 As Integer
    Public VDWithdwble3, VDWithdwble6 As Decimal
    Public VD30days1, VD30days2, VD30days4 As Integer
    Public VD30days5 As Integer
    Public VD30days3, VD30days6 As Decimal
    Public VD30to90days1, VD30to90days2, VD30to90days4 As Integer
    Public VD30to90days5 As Integer
    Public VD30to90days3, VD30to90days6 As Decimal
    Public VD90to1yr1, VD90to1yr2, VD90to1yr4 As Integer
    Public VD90to1yr5 As Integer
    Public VD90to1yr3, VD90to1yr6 As Decimal
    Public VD1to5yr1, VD1to5yr2, VD1to5yr4 As Integer
    Public VD1to5yr5 As Integer
    Public VD1to5yr3, VD1to5yr6 As Decimal
    Public VD5years1, VD5years2, VD5years4 As Integer
    Public VD5years5 As Integer
    Public VD5years3, VD5years6 As Decimal
    Public VDNonWitdrwble1, VDNonWitdrwble2, VDNonWitdrwble4 As Integer
    Public VDNonWitdrwble5 As Integer
    Public VDNonWitdrwble3, VDNonWitdrwble6 As Decimal
    Public VDDepositType, VDRegNoMemWthDepAcnt, VDRegNoAcnt As String
    Public VDRegTotal, VDAssNoMemWthDepAcnt, VDAssNoAcnt As String
    Public VDAssTotal As String

    Public VI41No, VI42No, VI43No As Integer
    Public VI44No, VI45No As Integer
    Public VI41Amt, VI42Amt, VI43Amt As Decimal
    Public VI44Amt, VI45Amt As Decimal
    Public VI51No, VI52No As Integer
    Public VI51Amt, VI52Amt As Decimal
    Public VI611No, VI612No, VI613No As Integer
    Public VI611Amt, VI612Amt, VI613Amt As Decimal
    Public VI621No, VI622No As Integer
    Public VI621Amt, VI622Amt As Decimal
    Public VII1, VII2, VII3 As Decimal
    Public VII5, VII6 As Decimal
    Public VIII1Per, VIII2Per, VIII3Per As Decimal
    Public VIII4Per, VIII5Per As Decimal
    Public VIII1Amt, VIII2Amt, VIII3Amt As Decimal
    Public VIII4Amt, VIII5Amt As Decimal
    Public VIII1Alloc, VIII2Alloc, VIII3Alloc As Decimal
    Public VIII4Alloc, VIII5Alloc As Decimal
    Public VIII1Distrb, VIII2Distrb, VIII3Distrb As Decimal
    Public VIII4Distrb, VIII5Distrb As Decimal
    Public VIII1Discrp, VIII2Discrp, VIII3Discrp As Decimal
    Public VIII4Discrp, VIII5Discrp As Decimal

    Public IV11, IV12, IV13 As String
    Public IV14, IV15, IV16 As String
    Public IV17, IV18, IV19 As String
    Public IV110, IV111, IV112 As String
    Public IV113, IV114, IV115 As String
    Public IV116, IV117, IV118 As String
    Public IV119, IV120, IV121 As String

    Public IV21, IV22, IV23 As Decimal
    Public IV24, IV25, IV26 As Decimal
    Public IV27, IV28, IV29 As Decimal
    Public IV210, IV211, IV212 As Decimal
    Public IV213, IV214, IV215 As Decimal
    Public IV216, IV217, IV218 As Decimal
    Public IV219, IV220, IV221 As Decimal

    Public IV31, IV32, IV33 As Decimal
    Public IV34, IV35, IV36 As Decimal
    Public IV37, IV38, IV39 As Decimal
    Public IV310, IV311, IV312 As Decimal
    Public IV313, IV314, IV315 As Decimal
    Public IV316, IV317, IV318 As Decimal
    Public IV319, IV320, IV321 As Decimal
    Public PreparedBy, CertifiedTrue As String
    Public Accoountant, BOD As Boolean

    Public Sub NewCAPRReport()
        xFormItro = New FrmCAPRIntro
        xFormSec1 = New FrmCAPRSecI
        xFormSec2 = New frmCAPRSecII
        xFormSec3 = New frmCAPRSecIII
        xFormSec3b = New frmCAPRSecIIIb
        xFormSec4 = New FrmCAPRSecIV
    End Sub
    Public Sub LoadGeneralInfo()
        Dim sSqlCmd As String = "select TIN, CompanyName, CompanyCode, CompanyAdd1 from dbo.Per_Company_Information"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    CINumber = rd.Item("TIN").ToString
                    CoopNameLatestAmendment = rd.Item("CompanyName").ToString
                    PresentCoopAddress = rd.Item("CompanyAdd1").ToString
                    RegConfirmNo = rd.Item("CompanyCode").ToString
                End While
            End Using
        Catch ex As Exception
            ShowError("LoadGeneralInfo", "CAPRVariable", ex.Message)
        End Try
    End Sub

    Private Sub ShowError(ByVal FunctionOrSubName As String, ByVal ModuleName As String, ByVal ErrorMsg As String)
        MsgBox("Error at " & FunctionOrSubName & " in " & ModuleName & "." & vbCr & vbCr & ErrorMsg, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
    End Sub
End Module