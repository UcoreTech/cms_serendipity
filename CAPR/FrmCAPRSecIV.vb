Public Class FrmCAPRSecIV

    Private Sub txt21_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt21.TextChanged, txt24.TextChanged, txt23.TextChanged, txt22.TextChanged
        If txt21.Text = "" Then
            txt21.Text = "0.00"
        End If
        If txt22.Text = "" Then
            txt22.Text = "0.00"
        End If
        If txt23.Text = "" Then
            txt23.Text = "0.00"
        End If
        If txt24.Text = "" Then
            txt24.Text = "0.00"
        End If
        txtTotalCoopPoints.Text = Format(CDec(txt21.Text) + CDec(txt22.Text) + CDec(txt23.Text) + CDec(txt24.Text), "##,##0.00")
    End Sub

    Private Sub txt31_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt31.TextChanged, txt32.TextChanged, txt33.TextChanged, txt34.TextChanged
        If txt31.Text = "" Then
            txt31.Text = "0.00"
        End If
        If txt32.Text = "" Then
            txt32.Text = "0.00"
        End If
        If txt33.Text = "" Then
            txt33.Text = "0.00"
        End If
        If txt34.Text = "" Then
            txt34.Text = "0.00"
        End If
        txtCoopRating.Text = Format(CDec(txt31.Text) + CDec(txt32.Text) + CDec(txt33.Text) + CDec(txt34.Text), "##,##0.00")
    End Sub

    Private Sub txt25_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt25.TextChanged, txt26.TextChanged, txt27.TextChanged, _
                                                                                                    txt28.TextChanged, txt29.TextChanged, txt210.TextChanged, _
                                                                                                    txt211.TextChanged, txt212.TextChanged, txt213.TextChanged, _
                                                                                                    txt214.TextChanged, txt215.TextChanged, txt216.TextChanged, _
                                                                                                    txt217.TextChanged, txt218.TextChanged, txt219.TextChanged, _
                                                                                                    txt220.TextChanged, txt221.TextChanged
        If txt25.Text = "" Then
            txt25.Text = "0.00"
        End If
        If txt26.Text = "" Then
            txt26.Text = "0.00"
        End If
        If txt27.Text = "" Then
            txt27.Text = "0.00"
        End If
        If txt28.Text = "" Then
            txt28.Text = "0.00"
        End If
        If txt29.Text = "" Then
            txt29.Text = "0.00"
        End If
        If txt210.Text = "" Then
            txt210.Text = "0.00"
        End If
        If txt211.Text = "" Then
            txt211.Text = "0.00"
        End If
        If txt212.Text = "" Then
            txt212.Text = "0.00"
        End If
        If txt213.Text = "" Then
            txt213.Text = "0.00"
        End If
        If txt214.Text = "" Then
            txt214.Text = "0.00"
        End If
        If txt215.Text = "" Then
            txt215.Text = "0.00"
        End If
        If txt216.Text = "" Then
            txt216.Text = "0.00"
        End If
        If txt217.Text = "" Then
            txt217.Text = "0.00"
        End If
        If txt218.Text = "" Then
            txt218.Text = "0.00"
        End If
        If txt219.Text = "" Then
            txt219.Text = "0.00"
        End If
        If txt220.Text = "" Then
            txt220.Text = "0.00"
        End If
        If txt221.Text = "" Then
            txt221.Text = "0.00"
        End If
        txtTotalPesosPt.Text = Format(CDec(txt25.Text) + CDec(txt26.Text) + CDec(txt27.Text) + _
                                        CDec(txt28.Text) + CDec(txt29.Text) + CDec(txt210.Text) + _
                                        CDec(txt211.Text) + CDec(txt212.Text) + CDec(txt213.Text) + _
                                        CDec(txt214.Text) + CDec(txt215.Text) + CDec(txt216.Text) + _
                                        CDec(txt217.Text) + CDec(txt218.Text) + CDec(txt219.Text) + _
                                        CDec(txt220.Text) + CDec(txt221.Text), "##,##0.00")
    End Sub

    Private Sub txt35_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt35.TextChanged, txt36.TextChanged, txt37.TextChanged, txt38.TextChanged, _
                                                                                                    txt39.TextChanged, txt310.TextChanged, txt311.TextChanged, txt312.TextChanged, _
                                                                                                    txt313.TextChanged, txt314.TextChanged, txt315.TextChanged, txt316.TextChanged, _
                                                                                                    txt317.TextChanged, txt318.TextChanged, txt319.TextChanged, txt320.TextChanged, _
                                                                                                    txt321.TextChanged
        If txt35.Text = "" Then
            txt35.Text = "0.00"
        End If
        If txt36.Text = "" Then
            txt36.Text = "0.00"
        End If
        If txt37.Text = "" Then
            txt37.Text = "0.00"
        End If
        If txt38.Text = "" Then
            txt38.Text = "0.00"
        End If
        If txt39.Text = "" Then
            txt39.Text = "0.00"
        End If
        If txt310.Text = "" Then
            txt310.Text = "0.00"
        End If
        If txt311.Text = "" Then
            txt311.Text = "0.00"
        End If
        If txt312.Text = "" Then
            txt312.Text = "0.00"
        End If
        If txt313.Text = "" Then
            txt313.Text = "0.00"
        End If
        If txt314.Text = "" Then
            txt314.Text = "0.00"
        End If
        If txt315.Text = "" Then
            txt315.Text = "0.00"
        End If
        If txt316.Text = "" Then
            txt316.Text = "0.00"
        End If
        If txt317.Text = "" Then
            txt317.Text = "0.00"
        End If
        If txt318.Text = "" Then
            txt318.Text = "0.00"
        End If
        If txt319.Text = "" Then
            txt319.Text = "0.00"
        End If
        If txt320.Text = "" Then
            txt320.Text = "0.00"
        End If
        If txt321.Text = "" Then
            txt321.Text = "0.00"
        End If
        txtPesosRating.Text = Format(CDec(txt35.Text) + CDec(txt36.Text) + CDec(txt37.Text) + _
                                    CDec(txt38.Text) + CDec(txt39.Text) + CDec(txt310.Text) + _
                                    CDec(txt311.Text) + CDec(txt312.Text) + CDec(txt313.Text) + _
                                    CDec(txt314.Text) + CDec(txt315.Text) + CDec(txt316.Text) + _
                                    CDec(txt317.Text) + CDec(txt318.Text) + CDec(txt319.Text) + _
                                    CDec(txt320.Text) + CDec(txt321.Text), "##,##0.00")
    End Sub

    Private Sub txtPesosRating_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPesosRating.TextChanged, txtCoopRating.TextChanged
        If txtPesosRating.Text = "" Then
            txtPesosRating.Text = "0.00"
        End If
        If txtCoopRating.Text = "" Then
            txtCoopRating.Text = "0.00"
        End If
        txtCoopPesosRating.Text = Format(CDec(txtPesosRating.Text) + CDec(txtCoopRating.Text), "##,##0.00")
    End Sub

    Private Sub btnPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrevious.Click
        Select Case TabControl1.SelectedIndex
            Case 2
                TabControl1.SelectTab(1)
            Case 1
                TabControl1.SelectTab(0)
            Case 0
                Me.Hide()
                If xSectionSwitch = False Then
                    xFormSec2.Show()
                Else
                    xFormSec3b.Show()
                End If
        End Select
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Select Case TabControl1.SelectedIndex
            Case 0
                TabControl1.SelectTab(1)
            Case 1
                TabControl1.SelectTab(2)
            Case 2
                IV11 = txt11.Text
                IV12 = txt12.Text
                IV13 = txt13.Text
                IV14 = txt14.Text
                IV15 = txt15.Text
                IV16 = txt16.Text
                IV17 = txt17.Text
                IV18 = txt18.Text
                IV19 = txt19.Text
                IV110 = txt110.Text
                IV111 = txt111.Text
                IV112 = txt112.Text
                IV113 = txt113.Text
                IV114 = txt114.Text
                IV115 = txt115.Text
                IV116 = txt116.Text
                IV117 = txt117.Text
                IV118 = txt118.Text
                IV119 = txt119.Text
                IV120 = txt120.Text
                IV121 = txt121.Text
                IV21 = txt21.Text
                IV22 = txt22.Text
                IV23 = txt23.Text
                IV24 = txt24.Text
                IV25 = txt25.Text
                IV26 = txt26.Text
                IV27 = txt27.Text
                IV28 = txt28.Text
                IV29 = txt29.Text
                IV210 = txt210.Text
                IV211 = txt211.Text
                IV212 = txt212.Text
                IV213 = txt213.Text
                IV214 = txt214.Text
                IV215 = txt215.Text
                IV216 = txt216.Text
                IV217 = txt217.Text
                IV218 = txt218.Text
                IV219 = txt219.Text
                IV220 = txt220.Text
                IV221 = txt221.Text

                IV31 = txt31.Text
                IV32 = txt32.Text
                IV33 = txt33.Text
                IV34 = txt34.Text
                IV35 = txt35.Text
                IV36 = txt36.Text
                IV37 = txt37.Text
                IV38 = txt38.Text
                IV39 = txt39.Text
                IV310 = txt310.Text
                IV311 = txt311.Text
                IV312 = txt312.Text
                IV313 = txt313.Text
                IV314 = txt314.Text
                IV315 = txt315.Text
                IV316 = txt316.Text
                IV317 = txt317.Text
                IV318 = txt318.Text
                IV319 = txt319.Text
                IV320 = txt320.Text
                IV321 = txt321.Text
                PreparedBy = txtPreparedBy.Text
                CertifiedTrue = txtCertifiedTrue.Text
                Accoountant = RdoAccountant.Checked
                BOD = RdoBOD.Checked
                Me.Hide()
                frmCAPRViewer.Show()
        End Select
    End Sub

    Private Sub TabControl1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabControl1.SelectedIndexChanged
        If TabControl1.SelectedIndex = 2 Then
            btnNext.Text = "View Report"
        Else
            btnNext.Text = "Next"
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        frmCAPRViewer.Close()
        xFormSec4.Close()
        xFormSec3b.Close()
        xFormSec3.Close()
        xFormSec2.Close()
        xFormItro.Close()
        Me.Close()
    End Sub
End Class