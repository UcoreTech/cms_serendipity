<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStartup
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.processWait = New System.Windows.Forms.ProgressBar
        Me.lblProgress = New System.Windows.Forms.Label
        Me.lblProcessStatus = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.picLoadingSmall = New System.Windows.Forms.PictureBox
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        CType(Me.picLoadingSmall, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'processWait
        '
        Me.processWait.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.processWait.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.processWait.Location = New System.Drawing.Point(12, 24)
        Me.processWait.Name = "processWait"
        Me.processWait.Size = New System.Drawing.Size(484, 23)
        Me.processWait.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.processWait.TabIndex = 0
        '
        'lblProgress
        '
        Me.lblProgress.AutoSize = True
        Me.lblProgress.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProgress.Location = New System.Drawing.Point(34, 50)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(56, 18)
        Me.lblProgress.TabIndex = 1
        Me.lblProgress.Text = "Status 1"
        '
        'lblProcessStatus
        '
        Me.lblProcessStatus.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProcessStatus.Location = New System.Drawing.Point(237, 50)
        Me.lblProcessStatus.Name = "lblProcessStatus"
        Me.lblProcessStatus.Size = New System.Drawing.Size(306, 18)
        Me.lblProcessStatus.TabIndex = 3
        Me.lblProcessStatus.Text = "Status 2"
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.ForeColor = System.Drawing.Color.Black
        Me.lblTitle.Location = New System.Drawing.Point(9, 3)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(86, 18)
        Me.lblTitle.TabIndex = 4
        Me.lblTitle.Text = "Process Title"
        '
        'picLoadingSmall
        '
        Me.picLoadingSmall.Image = Global.WindowsApplication2.My.Resources.Resources.loading2
        Me.picLoadingSmall.Location = New System.Drawing.Point(12, 50)
        Me.picLoadingSmall.Name = "picLoadingSmall"
        Me.picLoadingSmall.Size = New System.Drawing.Size(21, 18)
        Me.picLoadingSmall.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picLoadingSmall.TabIndex = 5
        Me.picLoadingSmall.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.WindowsApplication2.My.Resources.Resources.LoadingData
        Me.PictureBox1.Location = New System.Drawing.Point(432, -22)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(111, 111)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'frmStartup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(520, 75)
        Me.ControlBox = False
        Me.Controls.Add(Me.picLoadingSmall)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.lblProgress)
        Me.Controls.Add(Me.processWait)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.lblProcessStatus)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmStartup"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.TopMost = True
        CType(Me.picLoadingSmall, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents processWait As System.Windows.Forms.ProgressBar
    Friend WithEvents lblProgress As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblProcessStatus As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents picLoadingSmall As System.Windows.Forms.PictureBox
End Class
