﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmInvestmentTerms

    Private con As New Clsappconfiguration
    Dim ds As DataSet
    Private pkterm As String

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        dgvList.DataSource = Nothing
        dgvList.Columns.Clear()
        txtInvestment.Text = ""
        txtTerms.Text = "0.0"
        Me.Close()
    End Sub

    Private Sub frmInvestmentTerms_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If isloaded() Then
            Me.txtInvestment.Text = frmInvestmentSetup.lblInvestmentType.Text
        End If
    End Sub

    Private Function isloaded() As Boolean
        Try
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_Investment_Terms_Select",
                                           New SqlParameter("@fkInvestment", GetID()))
            dgvList.DataSource = ds.Tables(0)
            dgvList.Columns(0).Visible = False
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function GetID() As String
        Try
            Return frmInvestmentSetup.pkkey
        Catch ex As Exception
            Return "0"
        End Try
    End Function

    Private Sub txtTerms_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtTerms.KeyDown
        If e.KeyCode = Keys.Enter Then
            If isInserted() Then
                If isloaded() Then
                    txtTerms.Text = "0.0"
                End If
            End If
        End If
    End Sub

    Private Function isInserted() As Boolean
        Try
            Dim pkid As New Guid(GetID())
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Investment_Terms_Insert",
                                       New SqlParameter("@fkInvestment", pkid),
                                       New SqlParameter("@fnTerms", txtTerms.Text))
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub dgvList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.Click
        With dgvList
            pkterm = dgvList.SelectedRows(0).Cells(0).Value.ToString
            txtTerms.Text = dgvList.SelectedRows(0).Cells(1).Value.ToString
        End With
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If pkterm <> "" Then
                SqlHelper.ExecuteNonQuery(con.cnstring, "_Investment_Terms_Delete",
                           New SqlParameter("@pkInvestmentTerms", pkterm))
                If isloaded() Then
                    txtTerms.Text = "0.0"
                    pkterm = ""
                    MsgBox("Delete Success!", vbInformation, "Success...")
                End If
            Else
                MsgBox("No selected item to delete!", vbInformation, "Delete")
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

End Class