﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInvestmentSetup
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtInvestmentType = New System.Windows.Forms.TextBox()
        Me.txtDesc = New System.Windows.Forms.TextBox()
        Me.btnBrowseAcct = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboInterestType = New System.Windows.Forms.ComboBox()
        Me.txtInterestRate = New System.Windows.Forms.TextBox()
        Me.btnTerms = New System.Windows.Forms.Button()
        Me.btnApprover = New System.Windows.Forms.Button()
        Me.btnRequirements = New System.Windows.Forms.Button()
        Me.dgvInvestment = New System.Windows.Forms.DataGridView()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtMaximumAmtLimit = New System.Windows.Forms.TextBox()
        Me.rdApplyMaximum = New System.Windows.Forms.RadioButton()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtCode = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.btnNew = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblInvestmentType = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        CType(Me.dgvInvestment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(21, 74)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(109, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Investment Type:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(406, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Description:"
        '
        'txtInvestmentType
        '
        Me.txtInvestmentType.Location = New System.Drawing.Point(136, 66)
        Me.txtInvestmentType.Name = "txtInvestmentType"
        Me.txtInvestmentType.ReadOnly = True
        Me.txtInvestmentType.Size = New System.Drawing.Size(219, 21)
        Me.txtInvestmentType.TabIndex = 4
        '
        'txtDesc
        '
        Me.txtDesc.Location = New System.Drawing.Point(481, 38)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDesc.Size = New System.Drawing.Size(385, 48)
        Me.txtDesc.TabIndex = 5
        '
        'btnBrowseAcct
        '
        Me.btnBrowseAcct.Location = New System.Drawing.Point(361, 65)
        Me.btnBrowseAcct.Name = "btnBrowseAcct"
        Me.btnBrowseAcct.Size = New System.Drawing.Size(36, 21)
        Me.btnBrowseAcct.TabIndex = 6
        Me.btnBrowseAcct.Text = "..."
        Me.btnBrowseAcct.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(41, 116)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(89, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Interest Type:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(41, 144)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(87, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Interest Rate:"
        '
        'cboInterestType
        '
        Me.cboInterestType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInterestType.FormattingEnabled = True
        Me.cboInterestType.Items.AddRange(New Object() {"Per Month", "Per Year"})
        Me.cboInterestType.Location = New System.Drawing.Point(136, 108)
        Me.cboInterestType.Name = "cboInterestType"
        Me.cboInterestType.Size = New System.Drawing.Size(219, 21)
        Me.cboInterestType.TabIndex = 9
        '
        'txtInterestRate
        '
        Me.txtInterestRate.Location = New System.Drawing.Point(136, 135)
        Me.txtInterestRate.Name = "txtInterestRate"
        Me.txtInterestRate.Size = New System.Drawing.Size(98, 21)
        Me.txtInterestRate.TabIndex = 10
        Me.txtInterestRate.Text = "0"
        '
        'btnTerms
        '
        Me.btnTerms.Location = New System.Drawing.Point(543, 181)
        Me.btnTerms.Name = "btnTerms"
        Me.btnTerms.Size = New System.Drawing.Size(75, 23)
        Me.btnTerms.TabIndex = 11
        Me.btnTerms.Text = "Terms"
        Me.btnTerms.UseVisualStyleBackColor = True
        '
        'btnApprover
        '
        Me.btnApprover.Location = New System.Drawing.Point(624, 181)
        Me.btnApprover.Name = "btnApprover"
        Me.btnApprover.Size = New System.Drawing.Size(75, 23)
        Me.btnApprover.TabIndex = 12
        Me.btnApprover.Text = "Approver"
        Me.btnApprover.UseVisualStyleBackColor = True
        '
        'btnRequirements
        '
        Me.btnRequirements.Location = New System.Drawing.Point(705, 181)
        Me.btnRequirements.Name = "btnRequirements"
        Me.btnRequirements.Size = New System.Drawing.Size(158, 23)
        Me.btnRequirements.TabIndex = 13
        Me.btnRequirements.Text = "Requirements"
        Me.btnRequirements.UseVisualStyleBackColor = True
        '
        'dgvInvestment
        '
        Me.dgvInvestment.AllowUserToAddRows = False
        Me.dgvInvestment.AllowUserToDeleteRows = False
        Me.dgvInvestment.AllowUserToResizeColumns = False
        Me.dgvInvestment.AllowUserToResizeRows = False
        Me.dgvInvestment.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvInvestment.BackgroundColor = System.Drawing.Color.White
        Me.dgvInvestment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvInvestment.Location = New System.Drawing.Point(24, 210)
        Me.dgvInvestment.Name = "dgvInvestment"
        Me.dgvInvestment.ReadOnly = True
        Me.dgvInvestment.RowHeadersVisible = False
        Me.dgvInvestment.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvInvestment.Size = New System.Drawing.Size(842, 152)
        Me.dgvInvestment.TabIndex = 14
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(21, 194)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(125, 13)
        Me.Label5.TabIndex = 15
        Me.Label5.Text = "Investment Types"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(406, 143)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(184, 13)
        Me.Label6.TabIndex = 16
        Me.Label6.Text = "Maximum Investment Amount:"
        '
        'txtMaximumAmtLimit
        '
        Me.txtMaximumAmtLimit.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaximumAmtLimit.Location = New System.Drawing.Point(596, 135)
        Me.txtMaximumAmtLimit.Name = "txtMaximumAmtLimit"
        Me.txtMaximumAmtLimit.Size = New System.Drawing.Size(267, 21)
        Me.txtMaximumAmtLimit.TabIndex = 17
        Me.txtMaximumAmtLimit.Text = "0.00"
        Me.txtMaximumAmtLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'rdApplyMaximum
        '
        Me.rdApplyMaximum.AutoSize = True
        Me.rdApplyMaximum.Location = New System.Drawing.Point(596, 112)
        Me.rdApplyMaximum.Name = "rdApplyMaximum"
        Me.rdApplyMaximum.Size = New System.Drawing.Size(264, 17)
        Me.rdApplyMaximum.TabIndex = 18
        Me.rdApplyMaximum.TabStop = True
        Me.rdApplyMaximum.Text = "Apply Maximum Investment Amount Limit"
        Me.rdApplyMaximum.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(37, 46)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 13)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "Account Code:"
        '
        'txtCode
        '
        Me.txtCode.Location = New System.Drawing.Point(136, 38)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.ReadOnly = True
        Me.txtCode.Size = New System.Drawing.Size(100, 21)
        Me.txtCode.TabIndex = 20
        '
        'Panel3
        '
        Me.Panel3.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.Panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel3.Controls.Add(Me.btnRefresh)
        Me.Panel3.Controls.Add(Me.btnDelete)
        Me.Panel3.Controls.Add(Me.btnEdit)
        Me.Panel3.Controls.Add(Me.btnNew)
        Me.Panel3.Controls.Add(Me.btnClose)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(0, 369)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(890, 27)
        Me.Panel3.TabIndex = 1
        '
        'btnRefresh
        '
        Me.btnRefresh.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnRefresh.Location = New System.Drawing.Point(312, 0)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(75, 27)
        Me.btnRefresh.TabIndex = 4
        Me.btnRefresh.Text = "Refresh"
        Me.btnRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnDelete.Image = Global.WindowsApplication2.My.Resources.Resources.delete3
        Me.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDelete.Location = New System.Drawing.Point(237, 0)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 27)
        Me.btnDelete.TabIndex = 3
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnEdit.Image = Global.WindowsApplication2.My.Resources.Resources.edit
        Me.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEdit.Location = New System.Drawing.Point(162, 0)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 27)
        Me.btnEdit.TabIndex = 2
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnNew.Image = Global.WindowsApplication2.My.Resources.Resources.new3
        Me.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNew.Location = New System.Drawing.Point(87, 0)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(75, 27)
        Me.btnNew.TabIndex = 1
        Me.btnNew.Text = "New"
        Me.btnNew.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnClose.Image = Global.WindowsApplication2.My.Resources.Resources.button_cancel
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(0, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(87, 27)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.lblInvestmentType)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(890, 32)
        Me.Panel1.TabIndex = 0
        '
        'lblInvestmentType
        '
        Me.lblInvestmentType.AutoSize = True
        Me.lblInvestmentType.BackColor = System.Drawing.Color.Transparent
        Me.lblInvestmentType.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInvestmentType.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblInvestmentType.Location = New System.Drawing.Point(12, 9)
        Me.lblInvestmentType.Name = "lblInvestmentType"
        Me.lblInvestmentType.Size = New System.Drawing.Size(19, 14)
        Me.lblInvestmentType.TabIndex = 1
        Me.lblInvestmentType.Text = "..."
        Me.lblInvestmentType.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(744, 9)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(124, 14)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Investment Setup"
        '
        'frmInvestmentSetup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(890, 396)
        Me.Controls.Add(Me.txtCode)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.rdApplyMaximum)
        Me.Controls.Add(Me.txtMaximumAmtLimit)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.dgvInvestment)
        Me.Controls.Add(Me.btnRequirements)
        Me.Controls.Add(Me.btnApprover)
        Me.Controls.Add(Me.btnTerms)
        Me.Controls.Add(Me.txtInterestRate)
        Me.Controls.Add(Me.cboInterestType)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnBrowseAcct)
        Me.Controls.Add(Me.txtDesc)
        Me.Controls.Add(Me.txtInvestmentType)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmInvestmentSetup"
        Me.Text = "Investment Setup"
        CType(Me.dgvInvestment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtInvestmentType As System.Windows.Forms.TextBox
    Friend WithEvents txtDesc As System.Windows.Forms.TextBox
    Friend WithEvents btnBrowseAcct As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboInterestType As System.Windows.Forms.ComboBox
    Friend WithEvents txtInterestRate As System.Windows.Forms.TextBox
    Friend WithEvents btnTerms As System.Windows.Forms.Button
    Friend WithEvents btnApprover As System.Windows.Forms.Button
    Friend WithEvents btnRequirements As System.Windows.Forms.Button
    Friend WithEvents dgvInvestment As System.Windows.Forms.DataGridView
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtMaximumAmtLimit As System.Windows.Forms.TextBox
    Friend WithEvents rdApplyMaximum As System.Windows.Forms.RadioButton
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtCode As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblInvestmentType As System.Windows.Forms.Label
End Class
