﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInvestmentApplication
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rdRestructure = New System.Windows.Forms.RadioButton()
        Me.rdNew = New System.Windows.Forms.RadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtIDNo = New System.Windows.Forms.TextBox()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboInvestmentType = New System.Windows.Forms.ComboBox()
        Me.cboTerms = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtDocNo = New System.Windows.Forms.TextBox()
        Me.btnBrowseDocNumber = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtTotAmortization = New System.Windows.Forms.TextBox()
        Me.txtTotServiceFee = New System.Windows.Forms.TextBox()
        Me.txtTotInterest = New System.Windows.Forms.TextBox()
        Me.txtTotPrincipal = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.dgvAmortization = New System.Windows.Forms.DataGridView()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.txtOrigInterest = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtInterestRate = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.rdCompounded = New System.Windows.Forms.RadioButton()
        Me.rdStraight = New System.Windows.Forms.RadioButton()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtAmount = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.dtMaturity = New System.Windows.Forms.DateTimePicker()
        Me.dtFirstpayment = New System.Windows.Forms.DateTimePicker()
        Me.dtGranted = New System.Windows.Forms.DateTimePicker()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnCompute = New System.Windows.Forms.Button()
        Me.cboModeofPayment = New System.Windows.Forms.ComboBox()
        Me.txtnoofpayment = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.rdShares = New System.Windows.Forms.RadioButton()
        Me.rdDirectInvestment = New System.Windows.Forms.RadioButton()
        Me.rdAdditional = New System.Windows.Forms.RadioButton()
        Me.rdInitial = New System.Windows.Forms.RadioButton()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.rdCheck = New System.Windows.Forms.RadioButton()
        Me.rdDirectDeposit = New System.Windows.Forms.RadioButton()
        Me.rdCash = New System.Windows.Forms.RadioButton()
        Me.txtCheckNo = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtBankBranch = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtClient = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnNew = New System.Windows.Forms.Button()
        Me.btnSearchExisting = New System.Windows.Forms.Button()
        Me.btnPrintForm = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.rdFixedMonthly = New System.Windows.Forms.RadioButton()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvAmortization, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rdRestructure)
        Me.GroupBox1.Controls.Add(Me.rdNew)
        Me.GroupBox1.Location = New System.Drawing.Point(11, 90)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(200, 73)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Classification"
        '
        'rdRestructure
        '
        Me.rdRestructure.AutoSize = True
        Me.rdRestructure.Location = New System.Drawing.Point(58, 43)
        Me.rdRestructure.Name = "rdRestructure"
        Me.rdRestructure.Size = New System.Drawing.Size(91, 17)
        Me.rdRestructure.TabIndex = 1
        Me.rdRestructure.TabStop = True
        Me.rdRestructure.Text = "Restructure"
        Me.rdRestructure.UseVisualStyleBackColor = True
        '
        'rdNew
        '
        Me.rdNew.AutoSize = True
        Me.rdNew.Location = New System.Drawing.Point(58, 20)
        Me.rdNew.Name = "rdNew"
        Me.rdNew.Size = New System.Drawing.Size(49, 17)
        Me.rdNew.TabIndex = 0
        Me.rdNew.TabStop = True
        Me.rdNew.Text = "New"
        Me.rdNew.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(50, 46)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "ID No:"
        '
        'txtIDNo
        '
        Me.txtIDNo.Location = New System.Drawing.Point(102, 40)
        Me.txtIDNo.Name = "txtIDNo"
        Me.txtIDNo.ReadOnly = True
        Me.txtIDNo.Size = New System.Drawing.Size(184, 21)
        Me.txtIDNo.TabIndex = 4
        '
        'btnSearch
        '
        Me.btnSearch.ForeColor = System.Drawing.SystemColors.Highlight
        Me.btnSearch.Location = New System.Drawing.Point(292, 36)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(99, 23)
        Me.btnSearch.TabIndex = 5
        Me.btnSearch.Text = "Select Client"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(215, 99)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(148, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Select Investment Type:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(315, 129)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Terms:"
        '
        'cboInvestmentType
        '
        Me.cboInvestmentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInvestmentType.FormattingEnabled = True
        Me.cboInvestmentType.Location = New System.Drawing.Point(369, 91)
        Me.cboInvestmentType.Name = "cboInvestmentType"
        Me.cboInvestmentType.Size = New System.Drawing.Size(231, 21)
        Me.cboInvestmentType.TabIndex = 8
        '
        'cboTerms
        '
        Me.cboTerms.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTerms.FormattingEnabled = True
        Me.cboTerms.Location = New System.Drawing.Point(369, 121)
        Me.cboTerms.Name = "cboTerms"
        Me.cboTerms.Size = New System.Drawing.Size(149, 21)
        Me.cboTerms.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(448, 48)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(126, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Select Doc. Number:"
        '
        'txtDocNo
        '
        Me.txtDocNo.Location = New System.Drawing.Point(580, 43)
        Me.txtDocNo.Name = "txtDocNo"
        Me.txtDocNo.ReadOnly = True
        Me.txtDocNo.Size = New System.Drawing.Size(121, 21)
        Me.txtDocNo.TabIndex = 11
        '
        'btnBrowseDocNumber
        '
        Me.btnBrowseDocNumber.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnBrowseDocNumber.Location = New System.Drawing.Point(707, 43)
        Me.btnBrowseDocNumber.Name = "btnBrowseDocNumber"
        Me.btnBrowseDocNumber.Size = New System.Drawing.Size(32, 21)
        Me.btnBrowseDocNumber.TabIndex = 12
        Me.btnBrowseDocNumber.Text = "..."
        Me.btnBrowseDocNumber.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtTotAmortization)
        Me.GroupBox2.Controls.Add(Me.txtTotServiceFee)
        Me.GroupBox2.Controls.Add(Me.txtTotInterest)
        Me.GroupBox2.Controls.Add(Me.txtTotPrincipal)
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Controls.Add(Me.dgvAmortization)
        Me.GroupBox2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(11, 340)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(939, 175)
        Me.GroupBox2.TabIndex = 17
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Amortization"
        '
        'txtTotAmortization
        '
        Me.txtTotAmortization.Location = New System.Drawing.Point(687, 148)
        Me.txtTotAmortization.Name = "txtTotAmortization"
        Me.txtTotAmortization.ReadOnly = True
        Me.txtTotAmortization.Size = New System.Drawing.Size(132, 21)
        Me.txtTotAmortization.TabIndex = 5
        Me.txtTotAmortization.Text = "0.00"
        Me.txtTotAmortization.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotServiceFee
        '
        Me.txtTotServiceFee.Location = New System.Drawing.Point(549, 148)
        Me.txtTotServiceFee.Name = "txtTotServiceFee"
        Me.txtTotServiceFee.ReadOnly = True
        Me.txtTotServiceFee.Size = New System.Drawing.Size(132, 21)
        Me.txtTotServiceFee.TabIndex = 4
        Me.txtTotServiceFee.Text = "0.00"
        Me.txtTotServiceFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotInterest
        '
        Me.txtTotInterest.Location = New System.Drawing.Point(411, 148)
        Me.txtTotInterest.Name = "txtTotInterest"
        Me.txtTotInterest.ReadOnly = True
        Me.txtTotInterest.Size = New System.Drawing.Size(132, 21)
        Me.txtTotInterest.TabIndex = 3
        Me.txtTotInterest.Text = "0.00"
        Me.txtTotInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotPrincipal
        '
        Me.txtTotPrincipal.Location = New System.Drawing.Point(273, 148)
        Me.txtTotPrincipal.Name = "txtTotPrincipal"
        Me.txtTotPrincipal.ReadOnly = True
        Me.txtTotPrincipal.Size = New System.Drawing.Size(132, 21)
        Me.txtTotPrincipal.TabIndex = 2
        Me.txtTotPrincipal.Text = "0.00"
        Me.txtTotPrincipal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(221, 153)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(46, 13)
        Me.Label21.TabIndex = 1
        Me.Label21.Text = "Totals:"
        '
        'dgvAmortization
        '
        Me.dgvAmortization.AllowUserToAddRows = False
        Me.dgvAmortization.AllowUserToDeleteRows = False
        Me.dgvAmortization.AllowUserToResizeColumns = False
        Me.dgvAmortization.AllowUserToResizeRows = False
        Me.dgvAmortization.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvAmortization.BackgroundColor = System.Drawing.Color.White
        Me.dgvAmortization.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvAmortization.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAmortization.Location = New System.Drawing.Point(14, 20)
        Me.dgvAmortization.Name = "dgvAmortization"
        Me.dgvAmortization.ReadOnly = True
        Me.dgvAmortization.RowHeadersVisible = False
        Me.dgvAmortization.Size = New System.Drawing.Size(912, 123)
        Me.dgvAmortization.TabIndex = 0
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.txtOrigInterest)
        Me.Panel3.Controls.Add(Me.Label20)
        Me.Panel3.Controls.Add(Me.txtInterestRate)
        Me.Panel3.Controls.Add(Me.Label12)
        Me.Panel3.Controls.Add(Me.GroupBox4)
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Controls.Add(Me.txtAmount)
        Me.Panel3.Controls.Add(Me.Label10)
        Me.Panel3.Controls.Add(Me.dtMaturity)
        Me.Panel3.Controls.Add(Me.dtFirstpayment)
        Me.Panel3.Controls.Add(Me.dtGranted)
        Me.Panel3.Controls.Add(Me.Label9)
        Me.Panel3.Controls.Add(Me.Label8)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.btnCompute)
        Me.Panel3.Controls.Add(Me.cboModeofPayment)
        Me.Panel3.Controls.Add(Me.txtnoofpayment)
        Me.Panel3.Controls.Add(Me.Label6)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Location = New System.Drawing.Point(11, 169)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(929, 165)
        Me.Panel3.TabIndex = 18
        '
        'txtOrigInterest
        '
        Me.txtOrigInterest.Location = New System.Drawing.Point(466, 103)
        Me.txtOrigInterest.Name = "txtOrigInterest"
        Me.txtOrigInterest.ReadOnly = True
        Me.txtOrigInterest.Size = New System.Drawing.Size(100, 21)
        Me.txtOrigInterest.TabIndex = 62
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(358, 138)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(103, 13)
        Me.Label20.TabIndex = 61
        Me.Label20.Text = "Annuall Interest:"
        '
        'txtInterestRate
        '
        Me.txtInterestRate.Location = New System.Drawing.Point(466, 132)
        Me.txtInterestRate.Name = "txtInterestRate"
        Me.txtInterestRate.ReadOnly = True
        Me.txtInterestRate.Size = New System.Drawing.Size(100, 21)
        Me.txtInterestRate.TabIndex = 60
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(373, 109)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(87, 13)
        Me.Label12.TabIndex = 59
        Me.Label12.Text = "Interest Rate:"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.rdFixedMonthly)
        Me.GroupBox4.Controls.Add(Me.rdCompounded)
        Me.GroupBox4.Controls.Add(Me.rdStraight)
        Me.GroupBox4.Location = New System.Drawing.Point(13, 34)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(276, 90)
        Me.GroupBox4.TabIndex = 58
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Interest Method"
        '
        'rdCompounded
        '
        Me.rdCompounded.AutoSize = True
        Me.rdCompounded.Location = New System.Drawing.Point(146, 28)
        Me.rdCompounded.Name = "rdCompounded"
        Me.rdCompounded.Size = New System.Drawing.Size(101, 17)
        Me.rdCompounded.TabIndex = 3
        Me.rdCompounded.TabStop = True
        Me.rdCompounded.Text = "Compounded"
        Me.rdCompounded.UseVisualStyleBackColor = True
        '
        'rdStraight
        '
        Me.rdStraight.AutoSize = True
        Me.rdStraight.Location = New System.Drawing.Point(22, 28)
        Me.rdStraight.Name = "rdStraight"
        Me.rdStraight.Size = New System.Drawing.Size(74, 17)
        Me.rdStraight.TabIndex = 0
        Me.rdStraight.TabStop = True
        Me.rdStraight.Text = "Straight "
        Me.rdStraight.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.Panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel4.Controls.Add(Me.Label14)
        Me.Panel4.Controls.Add(Me.Label13)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(929, 28)
        Me.Panel4.TabIndex = 57
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(10, 9)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(160, 13)
        Me.Label14.TabIndex = 1
        Me.Label14.Text = "Details for computation"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(11, 9)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(0, 13)
        Me.Label13.TabIndex = 0
        '
        'txtAmount
        '
        Me.txtAmount.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmount.Location = New System.Drawing.Point(135, 130)
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Size = New System.Drawing.Size(217, 23)
        Me.txtAmount.TabIndex = 56
        Me.txtAmount.Text = "0.00"
        Me.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(10, 140)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(125, 13)
        Me.Label10.TabIndex = 55
        Me.Label10.Text = "Investment Amount:"
        '
        'dtMaturity
        '
        Me.dtMaturity.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtMaturity.Location = New System.Drawing.Point(742, 109)
        Me.dtMaturity.Name = "dtMaturity"
        Me.dtMaturity.Size = New System.Drawing.Size(168, 21)
        Me.dtMaturity.TabIndex = 54
        '
        'dtFirstpayment
        '
        Me.dtFirstpayment.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFirstpayment.Location = New System.Drawing.Point(743, 77)
        Me.dtFirstpayment.Name = "dtFirstpayment"
        Me.dtFirstpayment.Size = New System.Drawing.Size(168, 21)
        Me.dtFirstpayment.TabIndex = 53
        '
        'dtGranted
        '
        Me.dtGranted.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtGranted.Location = New System.Drawing.Point(743, 46)
        Me.dtGranted.Name = "dtGranted"
        Me.dtGranted.Size = New System.Drawing.Size(168, 21)
        Me.dtGranted.TabIndex = 52
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(678, 115)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(58, 13)
        Me.Label9.TabIndex = 51
        Me.Label9.Text = "Maturity:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(646, 83)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(90, 13)
        Me.Label8.TabIndex = 50
        Me.Label8.Text = "First Payment:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(627, 49)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(109, 13)
        Me.Label7.TabIndex = 49
        Me.Label7.Text = "Transaction Date:"
        '
        'btnCompute
        '
        Me.btnCompute.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCompute.ForeColor = System.Drawing.Color.DarkBlue
        Me.btnCompute.Location = New System.Drawing.Point(755, 136)
        Me.btnCompute.Name = "btnCompute"
        Me.btnCompute.Size = New System.Drawing.Size(140, 23)
        Me.btnCompute.TabIndex = 48
        Me.btnCompute.Text = "Compute"
        Me.btnCompute.UseVisualStyleBackColor = True
        '
        'cboModeofPayment
        '
        Me.cboModeofPayment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboModeofPayment.FormattingEnabled = True
        Me.cboModeofPayment.Items.AddRange(New Object() {"SEMI-MONTHLY", "MONTHLY", "QUARTERLY", "SEMI-ANNUALLY", "ANNUALLY"})
        Me.cboModeofPayment.Location = New System.Drawing.Point(430, 43)
        Me.cboModeofPayment.Name = "cboModeofPayment"
        Me.cboModeofPayment.Size = New System.Drawing.Size(162, 21)
        Me.cboModeofPayment.TabIndex = 47
        '
        'txtnoofpayment
        '
        Me.txtnoofpayment.Location = New System.Drawing.Point(466, 73)
        Me.txtnoofpayment.Name = "txtnoofpayment"
        Me.txtnoofpayment.ReadOnly = True
        Me.txtnoofpayment.Size = New System.Drawing.Size(100, 21)
        Me.txtnoofpayment.TabIndex = 46
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(360, 78)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(100, 13)
        Me.Label6.TabIndex = 45
        Me.Label6.Text = "No. of Payment:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(316, 46)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(111, 13)
        Me.Label5.TabIndex = 44
        Me.Label5.Text = "Mode of Payment:"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.rdShares)
        Me.GroupBox3.Controls.Add(Me.rdDirectInvestment)
        Me.GroupBox3.Controls.Add(Me.rdAdditional)
        Me.GroupBox3.Controls.Add(Me.rdInitial)
        Me.GroupBox3.Location = New System.Drawing.Point(621, 88)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(316, 75)
        Me.GroupBox3.TabIndex = 19
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Investment Details"
        '
        'rdShares
        '
        Me.rdShares.AutoSize = True
        Me.rdShares.Location = New System.Drawing.Point(158, 49)
        Me.rdShares.Name = "rdShares"
        Me.rdShares.Size = New System.Drawing.Size(65, 17)
        Me.rdShares.TabIndex = 3
        Me.rdShares.TabStop = True
        Me.rdShares.Text = "Shares"
        Me.rdShares.UseVisualStyleBackColor = True
        '
        'rdDirectInvestment
        '
        Me.rdDirectInvestment.AutoSize = True
        Me.rdDirectInvestment.Location = New System.Drawing.Point(158, 23)
        Me.rdDirectInvestment.Name = "rdDirectInvestment"
        Me.rdDirectInvestment.Size = New System.Drawing.Size(128, 17)
        Me.rdDirectInvestment.TabIndex = 2
        Me.rdDirectInvestment.TabStop = True
        Me.rdDirectInvestment.Text = "Direct Investment"
        Me.rdDirectInvestment.UseVisualStyleBackColor = True
        '
        'rdAdditional
        '
        Me.rdAdditional.AutoSize = True
        Me.rdAdditional.Location = New System.Drawing.Point(19, 49)
        Me.rdAdditional.Name = "rdAdditional"
        Me.rdAdditional.Size = New System.Drawing.Size(81, 17)
        Me.rdAdditional.TabIndex = 1
        Me.rdAdditional.TabStop = True
        Me.rdAdditional.Text = "Additional" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.rdAdditional.UseVisualStyleBackColor = True
        '
        'rdInitial
        '
        Me.rdInitial.AutoSize = True
        Me.rdInitial.Location = New System.Drawing.Point(20, 23)
        Me.rdInitial.Name = "rdInitial"
        Me.rdInitial.Size = New System.Drawing.Size(61, 17)
        Me.rdInitial.TabIndex = 0
        Me.rdInitial.TabStop = True
        Me.rdInitial.Text = "Initial "
        Me.rdInitial.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.GroupBox5)
        Me.Panel5.Controls.Add(Me.txtCheckNo)
        Me.Panel5.Controls.Add(Me.Label18)
        Me.Panel5.Controls.Add(Me.txtBankBranch)
        Me.Panel5.Controls.Add(Me.Label17)
        Me.Panel5.Controls.Add(Me.Panel6)
        Me.Panel5.Location = New System.Drawing.Point(11, 521)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(925, 125)
        Me.Panel5.TabIndex = 20
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.rdCheck)
        Me.GroupBox5.Controls.Add(Me.rdDirectDeposit)
        Me.GroupBox5.Controls.Add(Me.rdCash)
        Me.GroupBox5.Location = New System.Drawing.Point(10, 35)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(353, 74)
        Me.GroupBox5.TabIndex = 67
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Payment Type"
        '
        'rdCheck
        '
        Me.rdCheck.AutoSize = True
        Me.rdCheck.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdCheck.Location = New System.Drawing.Point(256, 29)
        Me.rdCheck.Name = "rdCheck"
        Me.rdCheck.Size = New System.Drawing.Size(64, 17)
        Me.rdCheck.TabIndex = 65
        Me.rdCheck.TabStop = True
        Me.rdCheck.Text = "Check"
        Me.rdCheck.UseVisualStyleBackColor = True
        '
        'rdDirectDeposit
        '
        Me.rdDirectDeposit.AutoSize = True
        Me.rdDirectDeposit.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdDirectDeposit.Location = New System.Drawing.Point(125, 29)
        Me.rdDirectDeposit.Name = "rdDirectDeposit"
        Me.rdDirectDeposit.Size = New System.Drawing.Size(117, 17)
        Me.rdDirectDeposit.TabIndex = 64
        Me.rdDirectDeposit.TabStop = True
        Me.rdDirectDeposit.Text = "Direct Deposit"
        Me.rdDirectDeposit.UseVisualStyleBackColor = True
        '
        'rdCash
        '
        Me.rdCash.AutoSize = True
        Me.rdCash.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdCash.Location = New System.Drawing.Point(32, 29)
        Me.rdCash.Name = "rdCash"
        Me.rdCash.Size = New System.Drawing.Size(56, 17)
        Me.rdCash.TabIndex = 63
        Me.rdCash.TabStop = True
        Me.rdCash.Text = "Cash"
        Me.rdCash.UseVisualStyleBackColor = True
        '
        'txtCheckNo
        '
        Me.txtCheckNo.Location = New System.Drawing.Point(462, 71)
        Me.txtCheckNo.Name = "txtCheckNo"
        Me.txtCheckNo.Size = New System.Drawing.Size(339, 21)
        Me.txtCheckNo.TabIndex = 66
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(380, 74)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(75, 13)
        Me.Label18.TabIndex = 65
        Me.Label18.Text = "Check No. :"
        '
        'txtBankBranch
        '
        Me.txtBankBranch.Location = New System.Drawing.Point(462, 47)
        Me.txtBankBranch.Name = "txtBankBranch"
        Me.txtBankBranch.Size = New System.Drawing.Size(339, 21)
        Me.txtBankBranch.TabIndex = 64
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(369, 50)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(86, 13)
        Me.Label17.TabIndex = 63
        Me.Label17.Text = "Bank/Branch:"
        '
        'Panel6
        '
        Me.Panel6.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.Panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel6.Controls.Add(Me.Label15)
        Me.Panel6.Controls.Add(Me.Label16)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel6.Location = New System.Drawing.Point(0, 0)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(925, 28)
        Me.Panel6.TabIndex = 59
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(10, 9)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(113, 13)
        Me.Label15.TabIndex = 1
        Me.Label15.Text = "Payment Details"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(11, 9)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(0, 13)
        Me.Label16.TabIndex = 0
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(14, 71)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(82, 13)
        Me.Label19.TabIndex = 21
        Me.Label19.Text = "Client Name:"
        '
        'txtClient
        '
        Me.txtClient.Location = New System.Drawing.Point(102, 63)
        Me.txtClient.Name = "txtClient"
        Me.txtClient.ReadOnly = True
        Me.txtClient.Size = New System.Drawing.Size(346, 21)
        Me.txtClient.TabIndex = 22
        '
        'Panel2
        '
        Me.Panel2.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.Panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel2.Controls.Add(Me.btnDelete)
        Me.Panel2.Controls.Add(Me.btnNew)
        Me.Panel2.Controls.Add(Me.btnSearchExisting)
        Me.Panel2.Controls.Add(Me.btnPrintForm)
        Me.Panel2.Controls.Add(Me.btnSave)
        Me.Panel2.Controls.Add(Me.btnClose)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 653)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(952, 24)
        Me.Panel2.TabIndex = 1
        '
        'btnDelete
        '
        Me.btnDelete.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnDelete.Image = Global.WindowsApplication2.My.Resources.Resources.delete3
        Me.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDelete.Location = New System.Drawing.Point(640, 0)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 24)
        Me.btnDelete.TabIndex = 5
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnNew.Image = Global.WindowsApplication2.My.Resources.Resources.new3
        Me.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNew.Location = New System.Drawing.Point(715, 0)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(75, 24)
        Me.btnNew.TabIndex = 4
        Me.btnNew.Text = "New"
        Me.btnNew.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnSearchExisting
        '
        Me.btnSearchExisting.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnSearchExisting.Image = Global.WindowsApplication2.My.Resources.Resources.Search
        Me.btnSearchExisting.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSearchExisting.Location = New System.Drawing.Point(170, 0)
        Me.btnSearchExisting.Name = "btnSearchExisting"
        Me.btnSearchExisting.Size = New System.Drawing.Size(75, 24)
        Me.btnSearchExisting.TabIndex = 3
        Me.btnSearchExisting.Text = "Search "
        Me.btnSearchExisting.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSearchExisting.UseVisualStyleBackColor = True
        '
        'btnPrintForm
        '
        Me.btnPrintForm.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnPrintForm.Image = Global.WindowsApplication2.My.Resources.Resources.printer
        Me.btnPrintForm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPrintForm.Location = New System.Drawing.Point(0, 0)
        Me.btnPrintForm.Name = "btnPrintForm"
        Me.btnPrintForm.Size = New System.Drawing.Size(170, 24)
        Me.btnPrintForm.TabIndex = 2
        Me.btnPrintForm.Text = "Print Investment Form"
        Me.btnPrintForm.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPrintForm.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnSave.Image = Global.WindowsApplication2.My.Resources.Resources.apply1
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(790, 0)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 24)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "Submit"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClose.Image = Global.WindowsApplication2.My.Resources.Resources.button_cancel
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(865, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(87, 24)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(952, 32)
        Me.Panel1.TabIndex = 0
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(426, 9)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(159, 13)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Investment Application"
        '
        'rdFixedMonthly
        '
        Me.rdFixedMonthly.AutoSize = True
        Me.rdFixedMonthly.Location = New System.Drawing.Point(19, 67)
        Me.rdFixedMonthly.Name = "rdFixedMonthly"
        Me.rdFixedMonthly.Size = New System.Drawing.Size(103, 17)
        Me.rdFixedMonthly.TabIndex = 4
        Me.rdFixedMonthly.TabStop = True
        Me.rdFixedMonthly.Text = "Fixed Monthly"
        Me.rdFixedMonthly.UseVisualStyleBackColor = True
        '
        'frmInvestmentApplication
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(952, 677)
        Me.Controls.Add(Me.txtClient)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnBrowseDocNumber)
        Me.Controls.Add(Me.txtDocNo)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cboTerms)
        Me.Controls.Add(Me.cboInvestmentType)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.txtIDNo)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmInvestmentApplication"
        Me.Text = "Investment Application"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvAmortization, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtIDNo As System.Windows.Forms.TextBox
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents rdRestructure As System.Windows.Forms.RadioButton
    Friend WithEvents rdNew As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboInvestmentType As System.Windows.Forms.ComboBox
    Friend WithEvents cboTerms As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtDocNo As System.Windows.Forms.TextBox
    Friend WithEvents btnBrowseDocNumber As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents btnPrintForm As System.Windows.Forms.Button
    Friend WithEvents dgvAmortization As System.Windows.Forms.DataGridView
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents rdCompounded As System.Windows.Forms.RadioButton
    Friend WithEvents rdStraight As System.Windows.Forms.RadioButton
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents txtAmount As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dtMaturity As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtFirstpayment As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtGranted As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnCompute As System.Windows.Forms.Button
    Friend WithEvents cboModeofPayment As System.Windows.Forms.ComboBox
    Friend WithEvents txtnoofpayment As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtInterestRate As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents rdShares As System.Windows.Forms.RadioButton
    Friend WithEvents rdDirectInvestment As System.Windows.Forms.RadioButton
    Friend WithEvents rdAdditional As System.Windows.Forms.RadioButton
    Friend WithEvents rdInitial As System.Windows.Forms.RadioButton
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtCheckNo As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtBankBranch As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtClient As System.Windows.Forms.TextBox
    Friend WithEvents txtOrigInterest As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtTotAmortization As System.Windows.Forms.TextBox
    Friend WithEvents txtTotServiceFee As System.Windows.Forms.TextBox
    Friend WithEvents txtTotInterest As System.Windows.Forms.TextBox
    Friend WithEvents txtTotPrincipal As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents rdCheck As System.Windows.Forms.RadioButton
    Friend WithEvents rdDirectDeposit As System.Windows.Forms.RadioButton
    Friend WithEvents rdCash As System.Windows.Forms.RadioButton
    Friend WithEvents btnSearchExisting As System.Windows.Forms.Button
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents rdFixedMonthly As System.Windows.Forms.RadioButton
End Class
