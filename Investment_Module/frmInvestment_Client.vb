﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmInvestment_Client

    Private con As New Clsappconfiguration
    Dim ds As DataSet
    Public mode As String

    Private Sub frmInvestment_Client_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If isloaded("") Then
            txtSearch.Text = ""
            ActiveControl = txtSearch
        End If
    End Sub

    Private Function isloaded(ByVal key As String) As Boolean
        Try
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_Investment_Select_Client",
                                           New SqlParameter("@Search", key))
            dgvList.DataSource = ds.Tables(0)
            dgvList.Columns(0).Visible = False
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        If e.KeyCode = Keys.Enter Then
            If isloaded(txtSearch.Text) Then
                If isSelected() Then
                    Me.Close()
                End If
            End If
        End If
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        If isloaded(txtSearch.Text) Then
            'other stuff here haha :D
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        mode = ""
        dgvList.DataSource = Nothing
        dgvList.Columns.Clear()
        Me.Close()
    End Sub

    Private Function isSelected() As Boolean
        Try
            If mode = "App" Then
                frmInvestmentApplication.fkemployee = dgvList.SelectedRows(0).Cells(0).Value.ToString
                frmInvestmentApplication.txtIDNo.Text = dgvList.SelectedRows(0).Cells(1).Value.ToString
                frmInvestmentApplication.txtClient.Text = dgvList.SelectedRows(0).Cells(2).Value.ToString
            Else
                frmInvestmentApprover.fkemployee = dgvList.SelectedRows(0).Cells(0).Value.ToString
                frmInvestmentApprover.txtIDNo.Text = dgvList.SelectedRows(0).Cells(1).Value.ToString
                frmInvestmentApprover.txtName.Text = dgvList.SelectedRows(0).Cells(2).Value.ToString
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        If isSelected() Then
            mode = ""
            Me.Close()
        End If
    End Sub

    Private Sub dgvList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.DoubleClick
        If isSelected() Then
            mode = ""
            Me.Close()
        End If
    End Sub

End Class