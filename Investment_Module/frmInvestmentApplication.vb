﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmInvestmentApplication

    Private con As New Clsappconfiguration
    Public fkemployee As String
    Public investmentNo As String
    Dim ds As DataSet

    Private Sub frmInvestmentApplication_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If isInvestmentLoad() Then
            DisableControls()
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If MsgBox("Are you sure?", vbYesNo + MsgBoxStyle.Question, "Exit Investment Application") = vbYes Then
            Me.Close()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        frmInvestment_Client.mode = "App"
        frmInvestment_Client.StartPosition = FormStartPosition.CenterScreen
        frmInvestment_Client.ShowDialog()
    End Sub

    Private Function isInvestmentLoad() As Boolean
        Try
            cboInvestmentType.Items.Clear()
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(con.cnstring, "_InvestmentApplication_Select_Investment")
            While rd.Read
                cboInvestmentType.Items.Add(rd(0).ToString)
            End While
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function isTermsLoad(ByVal investment As String) As Boolean
        Try
            cboTerms.Items.Clear()
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(con.cnstring, "_Investment_Terms_Select_2",
                                          New SqlParameter("@fcInvestment", investment))
            While rd.Read
                cboTerms.Items.Add(rd(1).ToString)
            End While
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub cboInvestmentType_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboInvestmentType.SelectedValueChanged
        If isTermsLoad(cboInvestmentType.Text) Then
            txtOrigInterest.Text = GetInterestValue()
        End If
    End Sub

    Private Sub btnCompute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCompute.Click
        If Not txtAmount.Text = "0.00" Then
            GenerateAmortDate()
            If isComputing() Then
                GetAmortizationTotals()
                btnCompute.Text = "Compute"
            End If
        Else
            MsgBox("Investment amount is zero!", MsgBoxStyle.Information, "Oops...")
        End If
    End Sub

    Private Function isComputing() As Boolean
        Try
            If rdStraight.Checked = True Then
                ds = SqlHelper.ExecuteDataset(con.cnstring, "Calculate_Amortization_Schedule_Straight_AddOn",
                                               New SqlParameter("@PrincipalAmount", CDec(txtAmount.Text)),
                                               New SqlParameter("@InterestRate", GetInterest()),
                                               New SqlParameter("@Terms", txtnoofpayment.Text),
                                               New SqlParameter("@DateGranted", dtGranted.Value),
                                               New SqlParameter("@FirstPayment", dtFirstpayment.Value),
                                               New SqlParameter("@xmode", cboModeofPayment.Text))
                dgvAmortization.DataSource = ds.Tables(0)
            End If
            If rdFixedMonthly.Checked = True Then
                ds = SqlHelper.ExecuteDataset(con.cnstring, "Calculate_Amortization_Schedule_FixedMonthly",
                               New SqlParameter("@PrincipalAmount", CDec(txtAmount.Text)),
                               New SqlParameter("@InterestRate", GetInterest()),
                               New SqlParameter("@Terms", txtnoofpayment.Text),
                               New SqlParameter("@DateGranted", dtGranted.Value),
                               New SqlParameter("@FirstPayment", dtFirstpayment.Value),
                               New SqlParameter("@xmode", cboModeofPayment.Text))
                dgvAmortization.DataSource = ds.Tables(0)
            End If
            If rdCompounded.Checked = True Then
                ds = SqlHelper.ExecuteDataset(con.cnstring, "Calculate_Amortization_Schedule_Compounded",
                               New SqlParameter("@PrincipalAmount", CDec(txtAmount.Text)),
                               New SqlParameter("@InterestRate", GetInterest()),
                               New SqlParameter("@Terms", txtnoofpayment.Text),
                               New SqlParameter("@DateGranted", dtGranted.Value),
                               New SqlParameter("@FirstPayment", dtFirstpayment.Value),
                               New SqlParameter("@xmode", cboModeofPayment.Text))
                dgvAmortization.DataSource = ds.Tables(0)
            End If
            btnCompute.Text = "Computing..."
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function GetInterest() As Decimal
        Try
            Dim interestrate As Decimal
            Select Case cboModeofPayment.Text
                Case "DAILY"
                    interestrate = CDec(txtInterestRate.Text / 360)
                Case "WEEKLY"
                    interestrate = CDec(txtInterestRate.Text / 48)
                Case "SEMI-MONTHLY"
                    interestrate = CDec(txtInterestRate.Text / 24)
                Case "MONTHLY"
                    interestrate = CDec(txtInterestRate.Text / 12)
                Case "QUARTERLY"
                    interestrate = CDec(txtInterestRate.Text / 3)
                Case "ANNUALLY"
                    interestrate = CDec(txtInterestRate.Text / 1)
                Case "LUMP SUM"
                    interestrate = CDec(txtInterestRate.Text)
            End Select
            Return interestrate
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Private Sub cboModeofPayment_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboModeofPayment.SelectedValueChanged
        Try
            Select Case cboModeofPayment.Text
                Case "DAILY"
                    txtnoofpayment.Text = cboTerms.Text * 30
                Case "WEEKLY"
                    txtnoofpayment.Text = cboTerms.Text * 4
                Case "SEMI-MONTHLY"
                    txtnoofpayment.Text = cboTerms.Text * 2
                Case "MONTHLY"
                    txtnoofpayment.Text = cboTerms.Text
                Case "QUARTERLY"
                    txtnoofpayment.Text = cboTerms.Text / 4
                Case "SEMI-ANNUALLY"
                    txtnoofpayment.Text = cboTerms.Text / 6
                Case "ANNUALLY"
                    txtnoofpayment.Text = cboTerms.Text / 12
                Case "LUMP SUM"
                    txtnoofpayment.Text = 1
            End Select
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Please Select Terms!"
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub txtAmount_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtAmount.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtAmount.Text = Format(CDec(txtAmount.Text), "##,##0.00")
        End If
    End Sub

    Private Sub GenerateAmortDate()
        'Generate Date Granted,First Payment , Maturity
        'Added by: Vincent Nacar 8/5/2014
        '****************************************************************************
        Dim mode As String = cboModeofPayment.Text
        Dim t As Integer = txtnoofpayment.Text
        Dim fpay As Date = dtGranted.Value
        Dim weekctr As Integer = 7
        Dim weekdays As Date
        'updated 8/15/14
        Select Case mode
            Case "DAILY"
                dtFirstpayment.Value = DateAdd(DateInterval.Day, 1, dtGranted.Value)
                dtMaturity.Value = fpay.AddDays(t)
            Case "WEEKLY"
                dtFirstpayment.Value = DateAdd(DateInterval.Day, 7, dtGranted.Value)
                For i As Integer = 0 To t - 1
                    dtMaturity.Value = DateAdd(DateInterval.Day, 7, dtMaturity.Value)
                Next
            Case "SEMI-MONTHLY"
                dtFirstpayment.Value = DateAdd(DateInterval.Day, 15, dtGranted.Value)
                For i As Integer = 0 To t - 1
                    dtMaturity.Value = DateAdd(DateInterval.Day, 15, dtMaturity.Value)
                Next
            Case "MONTHLY"
                dtFirstpayment.Value = DateAdd(DateInterval.Month, 1, dtGranted.Value)
                dtMaturity.Value = fpay.AddMonths(t)
            Case "QUARTERLY"
                dtFirstpayment.Value = DateAdd(DateInterval.Month, 3, dtGranted.Value)
                For i As Integer = 0 To t - 1
                    dtMaturity.Value = DateAdd(DateInterval.Month, 3, dtMaturity.Value)
                Next
            Case "ANNUALLY"
                dtFirstpayment.Value = DateAdd(DateInterval.Year, 1, dtGranted.Value)
                dtMaturity.Value = fpay.AddYears(t)
            Case "LUMP SUM"
                dtFirstpayment.Value = DateAdd(DateInterval.Year, 1, dtGranted.Value)
                dtMaturity.Value = dtFirstpayment.Value
        End Select
        '****************************************************************************
    End Sub

    Private Function GetInterestValue()
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(con.cnstring, "_Select_Investment_ByLoanType",
                                         New SqlParameter("@InvestmentType", cboInvestmentType.Text))
            While rd.Read
                If rd(1) = "Per Year" Then
                    txtInterestRate.Text = rd(0)
                    Return rd(0) / 12
                Else
                    txtInterestRate.Text = rd(0) * 12
                    Return rd(0)
                End If
            End While
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Private Sub GetAmortizationTotals()
        Try
            ' align number grid textboxes
            With dgvAmortization
                .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With

            Dim totprincipal As Decimal
            Dim totinterest As Decimal
            Dim totservicefee As Decimal
            Dim totamortization As Decimal

            For i As Integer = 0 To dgvAmortization.RowCount - 1
                With dgvAmortization
                    totprincipal = totprincipal + CDec(.Rows(i).Cells(2).Value.ToString)
                    totinterest = totinterest + CDec(.Rows(i).Cells(3).Value.ToString)
                    totservicefee = totservicefee + CDec(.Rows(i).Cells(4).Value.ToString)
                    totamortization = totamortization + CDec(.Rows(i).Cells(5).Value.ToString)
                End With
            Next

            txtTotPrincipal.Text = Format(totprincipal, "##,##0.00")
            txtTotInterest.Text = Format(totinterest, "##,##0.00")
            txtTotServiceFee.Text = Format(totservicefee, "##,##0.00")
            txtTotAmortization.Text = Format(totamortization, "##,##0.00")
        Catch ex As Exception
            frmMsgBox.Text = "Error - Calculating Amortization"
            frmMsgBox.txtMessage.Text = ex.Message + "  - Select 'Mode of Payment' and 'Interest Method' then click Compute."
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtDocNo.Text <> "" Then
            If MsgBox("Are you sure you want to submit this investment application to approver?", vbYesNo, "Confirmation") = vbYes Then
                If isSubmitted() Then
                    Call UpdateDocNumber()
                    Call SaveAmortSchedule()
                    frmMsgBox.Text = "Investment Application"
                    frmMsgBox.txtMessage.Text = "Success!"
                    frmMsgBox.ShowDialog()

                    DisableControls()
                Else
                    MsgBox("Unable to proceed please fill up the required field!", vbInformation, "Ooops.")
                End If
            Else
                Exit Sub
            End If
        Else
            MsgBox("Please select document number to continue.", vbInformation, "Ooops.")
        End If
    End Sub

    Private Function isSubmitted() As Boolean
        Try
            Dim classification As String
            Dim intmethod As String
            Dim paymenttype As String

            If rdNew.Checked = True Then
                classification = "New"
            End If
            If rdRestructure.Checked = True Then
                classification = "Restructure"
            End If

            If rdStraight.Checked = True Then
                intmethod = "Straight Add-On"
            End If
            If rdCompounded.Checked = True Then
                intmethod = "Compounded"
            End If
            If rdFixedMonthly.Checked = True Then
                intmethod = "Fixed Monthly"
            End If

            If rdCash.Checked = True Then
                paymenttype = "Cash"
            End If
            If rdCheck.Checked = True Then
                paymenttype = "Check"
            End If
            If rdDirectDeposit.Checked Then
                paymenttype = "Direct Deposit"
            End If

            xmode = intmethod
            Dim empkey As New Guid(fkemployee)
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Investment_Application_Submit",
                                       New SqlParameter("@fkemployee", empkey),
                                       New SqlParameter("@fcDocRef", txtDocNo.Text),
                                       New SqlParameter("@fcInvestmentType", cboInvestmentType.Text),
                                       New SqlParameter("@fcClassification", classification),
                                       New SqlParameter("@fcInterestMethod", intmethod),
                                       New SqlParameter("@fnTerms", cboTerms.Text),
                                       New SqlParameter("@fcModeOfPayment", cboModeofPayment.Text),
                                       New SqlParameter("@fnNoPayment", CType(txtnoofpayment.Text, Integer)),
                                       New SqlParameter("@dtTransactionDate", dtGranted.Text),
                                       New SqlParameter("@dtFirstPayment", dtFirstpayment.Text),
                                       New SqlParameter("@dtMaturity", dtMaturity.Text),
                                       New SqlParameter("@fcPaymentType", paymenttype),
                                       New SqlParameter("@fcBank", txtBankBranch.Text),
                                       New SqlParameter("@fcCheckNo", txtCheckNo.Text),
                                       New SqlParameter("@fdAmount", CDec(txtAmount.Text)))

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub btnBrowseDocNumber_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseDocNumber.Click
        frmInvestmentNo.StartPosition = FormStartPosition.CenterScreen
        frmInvestmentNo.ShowDialog()
    End Sub

    Private Sub UpdateDocNumber()
        SqlHelper.ExecuteNonQuery(con.cnstring, "_Update_DocNumber_Used",
                           New SqlParameter("@DocNumber", txtDocNo.Text),
                           New SqlParameter("@DateUsed", dtGranted.Value))
    End Sub

    Dim xmode As String
    Private Sub SaveAmortSchedule() 'by Vincent Nacar
        For i As Integer = 0 To dgvAmortization.RowCount - 1
            With dgvAmortization
                Dim loanNo As String = txtDocNo.Text
                Dim payNo As String = .Item(0, i).Value
                Dim fcDate As String = .Item(1, i).Value
                Dim xprincipal As String = .Item(2, i).Value
                Dim xinterest As String = .Item(3, i).Value
                Dim xservicefee As String = .Item(4, i).Value
                Dim xamortization As String = .Item(5, i).Value
                Dim balance As String = .Item(6, i).Value
                Dim method As String = xmode
                Dim status As String
                status = "Unpaid"

                SqlHelper.ExecuteNonQuery(con.cnstring, "_Insert_AmortizationSchedule",
                                           New SqlParameter("@pk_KeyID", System.Guid.NewGuid),
                                           New SqlParameter("@fcLoanNo", loanNo),
                                           New SqlParameter("@fdPayNo", payNo),
                                           New SqlParameter("@fcDate", fcDate),
                                           New SqlParameter("@fdPrincipal", xprincipal),
                                           New SqlParameter("@fdInterest", xinterest),
                                           New SqlParameter("@fdServiceFee", xservicefee),
                                           New SqlParameter("@fdAmortization", xamortization),
                                           New SqlParameter("@fdBalance", balance),
                                           New SqlParameter("@fcMethod", method),
                                           New SqlParameter("@fcStatus", status))
            End With
        Next
    End Sub

    Private Sub ResetValues()
        txtIDNo.Text = ""
        txtClient.Text = ""
        txtDocNo.Text = ""
        rdNew.Checked = False
        rdAdditional.Checked = False
        rdCash.Checked = False
        rdCheck.Checked = False
        rdCompounded.Checked = False
        rdDirectDeposit.Checked = False
        rdDirectInvestment.Checked = False
        rdInitial.Checked = False
        cboInvestmentType.Text = ""
        cboTerms.Text = ""
        rdRestructure.Checked = False
        rdShares.Checked = False
        rdStraight.Checked = False
        txtAmount.Text = "0.00"
        cboModeofPayment.Text = ""
        txtnoofpayment.Text = ""
        txtOrigInterest.Text = ""
        txtInterestRate.Text = ""
        txtTotPrincipal.Text = "0.00"
        txtTotInterest.Text = "0.00"
        txtTotServiceFee.Text = "0.00"
        txtTotAmortization.Text = "0.00"
        txtBankBranch.Text = ""
        txtCheckNo.Text = ""
        dgvAmortization.Columns.Clear()
        fkemployee = ""
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        ResetValues()
        EnableControls()
    End Sub

    Private Sub DisableControls()
        Panel3.Enabled = False
        GroupBox2.Enabled = False
        Panel5.Enabled = False
        GroupBox1.Enabled = False
        GroupBox3.Enabled = False
        btnSearch.Enabled = False
        btnBrowseDocNumber.Enabled = False
        cboInvestmentType.Enabled = False
        cboTerms.Enabled = False
        btnSave.Enabled = False
    End Sub

    Private Sub EnableControls()
        Panel3.Enabled = True
        GroupBox2.Enabled = True
        Panel5.Enabled = True
        GroupBox1.Enabled = True
        GroupBox3.Enabled = True
        btnSearch.Enabled = True
        btnBrowseDocNumber.Enabled = True
        cboInvestmentType.Enabled = True
        cboTerms.Enabled = True
        btnSave.Enabled = True
    End Sub

    Private Sub btnSearchExisting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchExisting.Click
        frmExistingInvestment.StartPosition = FormStartPosition.CenterScreen
        frmExistingInvestment.ShowDialog()
    End Sub

#Region "Search EXisting"

    Public Function isLoad_InvestmentDetails() As Boolean
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(con.cnstring, "_InvestmentApplication_Select_InvestDetails",
                                          New SqlParameter("@investmentNo", investmentNo))
            While rd.Read
                fkemployee = rd(0).ToString
                txtIDNo.Text = rd(1).ToString
                txtClient.Text = rd(2).ToString
                cboInvestmentType.Text = rd(3).ToString
                If rd(4) = "New" Then
                    rdNew.Checked = True
                Else
                    rdRestructure.Checked = True
                End If
                If rd(5) = "Straight Add-On" Then
                    rdStraight.Checked = True
                End If
                If rd(5) = "Compounded" Then
                    rdCompounded.Checked = True
                End If
                If rd(5) = "Fixed Monthly" Then
                    rdFixedMonthly.Checked = True
                End If
                cboTerms.Text = rd(6).ToString
                cboModeofPayment.Text = rd(7).ToString
                txtnoofpayment.Text = rd(8).ToString
                dtGranted.Value = rd(9)
                dtFirstpayment.Value = rd(10)
                dtMaturity.Value = rd(11)
                If rd(12) = "Cash" Then
                    rdCash.Checked = True
                End If
                If rd(12) = "Direct Deposit" Then
                    rdDirectDeposit.Checked = True
                End If
                If rd(12) = "Check" Then
                    rdCheck.Checked = True
                End If
                txtBankBranch.Text = rd(13).ToString
                txtCheckNo.Text = rd(14).ToString
                txtAmount.Text = rd(15).ToString
                txtDocNo.Text = rd(16).ToString
            End While
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function isLoad_Amortization() As Boolean
        Try
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_InvestmentApplication_Select_Amortization",
                                           New SqlParameter("@investmentNo", investmentNo))
            dgvAmortization.DataSource = ds.Tables(0)
            GetAmortizationTotals()
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

#End Region

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If txtDocNo.Text <> "" Then
            If MsgBox("Make sure that this investment application currently dont have a transaction." + vbNewLine + "Are you sure you want to delete this investment application?", vbYesNo, "Warning...") = vbYes Then
                If isDeleted() Then
                    MsgBox("Delete Success!", vbInformation, "Done.")
                    ResetValues()
                    DisableControls()
                End If
            Else
                Exit Sub
            End If
        Else
            MsgBox("No selected item to be deleted!", vbInformation, "Oops...")
        End If
    End Sub

    Private Function isDeleted() As Boolean
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_InvestmentApplication_Delete",
                                       New SqlParameter("@investmentNo", investmentNo))
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

End Class