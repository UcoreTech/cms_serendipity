<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLogin
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub
    Friend WithEvents LogoPictureBox As System.Windows.Forms.PictureBox

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLogin))
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbousertype = New System.Windows.Forms.ComboBox()
        Me.LogoPictureBox = New System.Windows.Forms.PictureBox()
        Me.Cancel = New System.Windows.Forms.Button()
        Me.OK = New System.Windows.Forms.Button()
        Me.Password = New System.Windows.Forms.TextBox()
        Me.Username = New System.Windows.Forms.TextBox()
        Me.btnconnection = New System.Windows.Forms.Button()
        Me.txtDatabase = New System.Windows.Forms.TextBox()
        Me.txtServer = New System.Windows.Forms.TextBox()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanePanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(200, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "User Type"
        Me.Label3.Visible = False
        '
        'cbousertype
        '
        Me.cbousertype.FormattingEnabled = True
        Me.cbousertype.Location = New System.Drawing.Point(167, 67)
        Me.cbousertype.Name = "cbousertype"
        Me.cbousertype.Size = New System.Drawing.Size(107, 21)
        Me.cbousertype.TabIndex = 9
        Me.cbousertype.Visible = False
        '
        'LogoPictureBox
        '
        Me.LogoPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LogoPictureBox.Image = CType(resources.GetObject("LogoPictureBox.Image"), System.Drawing.Image)
        Me.LogoPictureBox.Location = New System.Drawing.Point(0, -1)
        Me.LogoPictureBox.Name = "LogoPictureBox"
        Me.LogoPictureBox.Size = New System.Drawing.Size(484, 79)
        Me.LogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.LogoPictureBox.TabIndex = 0
        Me.LogoPictureBox.TabStop = False
        '
        'Cancel
        '
        Me.Cancel.BackColor = System.Drawing.Color.White
        Me.Cancel.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.exit3
        Me.Cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Cancel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Cancel.Location = New System.Drawing.Point(283, 256)
        Me.Cancel.Name = "Cancel"
        Me.Cancel.Size = New System.Drawing.Size(119, 35)
        Me.Cancel.TabIndex = 5
        Me.Cancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Cancel.UseVisualStyleBackColor = False
        '
        'OK
        '
        Me.OK.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.OK.BackColor = System.Drawing.Color.White
        Me.OK.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.btnLogin
        Me.OK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.OK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.OK.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.OK.Location = New System.Drawing.Point(167, 255)
        Me.OK.Name = "OK"
        Me.OK.Size = New System.Drawing.Size(111, 35)
        Me.OK.TabIndex = 4
        Me.OK.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.OK.UseVisualStyleBackColor = False
        '
        'Password
        '
        Me.Password.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Password.ForeColor = System.Drawing.Color.Gray
        Me.Password.Location = New System.Drawing.Point(167, 224)
        Me.Password.Name = "Password"
        Me.Password.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.Password.Size = New System.Drawing.Size(235, 22)
        Me.Password.TabIndex = 3
        '
        'Username
        '
        Me.Username.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Username.ForeColor = System.Drawing.Color.Gray
        Me.Username.Location = New System.Drawing.Point(167, 192)
        Me.Username.Name = "Username"
        Me.Username.Size = New System.Drawing.Size(235, 22)
        Me.Username.TabIndex = 1
        '
        'btnconnection
        '
        Me.btnconnection.BackColor = System.Drawing.Color.White
        Me.btnconnection.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.cc
        Me.btnconnection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnconnection.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnconnection.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnconnection.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnconnection.Location = New System.Drawing.Point(411, 131)
        Me.btnconnection.Name = "btnconnection"
        Me.btnconnection.Size = New System.Drawing.Size(73, 51)
        Me.btnconnection.TabIndex = 12
        Me.btnconnection.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnconnection.UseVisualStyleBackColor = False
        '
        'txtDatabase
        '
        Me.txtDatabase.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDatabase.ForeColor = System.Drawing.Color.Gray
        Me.txtDatabase.Location = New System.Drawing.Point(167, 160)
        Me.txtDatabase.Name = "txtDatabase"
        Me.txtDatabase.Size = New System.Drawing.Size(235, 22)
        Me.txtDatabase.TabIndex = 14
        '
        'txtServer
        '
        Me.txtServer.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtServer.ForeColor = System.Drawing.Color.Gray
        Me.txtServer.Location = New System.Drawing.Point(167, 128)
        Me.txtServer.Name = "txtServer"
        Me.txtServer.Size = New System.Drawing.Size(235, 22)
        Me.txtServer.TabIndex = 15
        '
        'PanePanel1
        '
        Me.PanePanel1.ActiveGradientHighColor = System.Drawing.Color.Black
        Me.PanePanel1.ActiveGradientLowColor = System.Drawing.Color.Chocolate
        Me.PanePanel1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.CMS_Interface2
        Me.PanePanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PanePanel1.Controls.Add(Me.txtServer)
        Me.PanePanel1.Controls.Add(Me.txtDatabase)
        Me.PanePanel1.Controls.Add(Me.btnconnection)
        Me.PanePanel1.Controls.Add(Me.Username)
        Me.PanePanel1.Controls.Add(Me.Password)
        Me.PanePanel1.Controls.Add(Me.OK)
        Me.PanePanel1.Controls.Add(Me.Cancel)
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel1.Location = New System.Drawing.Point(0, -1)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(533, 294)
        Me.PanePanel1.TabIndex = 13
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(173, 296)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(214, 13)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Copyright 2014 | Intellitech CLC"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(173, 309)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(217, 13)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "UCore Technologies Corporation"
        '
        'frmLogin
        '
        Me.AccessibleRole = System.Windows.Forms.AccessibleRole.Animation
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(526, 329)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.LogoPictureBox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cbousertype)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLogin"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "login"
        Me.TransparencyKey = System.Drawing.Color.Silver
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cbousertype As System.Windows.Forms.ComboBox
    Friend WithEvents Cancel As System.Windows.Forms.Button
    Friend WithEvents OK As System.Windows.Forms.Button
    Friend WithEvents Password As System.Windows.Forms.TextBox
    Friend WithEvents Username As System.Windows.Forms.TextBox
    Friend WithEvents btnconnection As System.Windows.Forms.Button
    Friend WithEvents txtDatabase As System.Windows.Forms.TextBox
    Friend WithEvents txtServer As System.Windows.Forms.TextBox
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label

End Class
