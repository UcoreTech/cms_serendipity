﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.IO

Public Class frmDownloader

#Region "VARIABLE"
    Private gCon As New Clsappconfiguration
    Dim EmpId As String
    Dim eName As String
    Dim nName As String
    Dim position As String
    Dim address As String
    Dim sss As String
    Dim picData As Byte()
    Dim sigData As Byte()
    Dim img As Image
#End Region

    Private Sub btnPreview_Click(sender As System.Object, e As System.EventArgs) Handles btnPreview.Click
        Try
            dgvInfoList.Columns.Clear()
            If rbIndividual.Checked Then
                Call ListbyEmployee()
            ElseIf rbBatch.Checked Then
                Call ListbyDate()
            End If
            Call Add_IDExpirationaDate()
            For i As Integer = 0 To dgvInfoList.RowCount - 1
                dgvInfoList.Rows(i).Cells(11).Value = dtExpDate.Text
            Next
            btnDownload.Enabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ListbyEmployee()
        Try
            Dim ds As New DataSet

            If txtEmpNo.Text <> "" Then
                ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "Select_DataforID_byEmployee",
                                  New SqlParameter("@Employee", txtEmpNo.Text))

                With dgvInfoList
                    .DataSource = ds.Tables(0)
                    .Columns(0).HeaderText = "ID"
                    .Columns(1).HeaderText = "Lastame"
                    .Columns(2).HeaderText = "Firstname"
                    .Columns(3).HeaderText = "Middle Initial"
                    .Columns(4).HeaderText = "Nickname"
                    .Columns(5).HeaderText = "Address"
                    .Columns(6).Visible = False
                    .Columns(7).HeaderText = "Contact Person"
                    .Columns(8).HeaderText = "Contact No"
                    .Columns(9).HeaderText = "Contact Address"
                    .Columns(10).HeaderText = "SSS No"
                    .Columns(11).HeaderText = "Position"
                    .Columns(12).HeaderText = "Project"

                    .Columns(0).Width = 100
                    .Columns(1).Width = 90
                    .Columns(2).Width = 90
                    .Columns(3).Width = 90
                    .Columns(4).Width = 100
                    .Columns(5).Width = 200
                    .Columns(7).Width = 150
                    .Columns(8).Width = 80
                    .Columns(9).Width = 200
                    .Columns(10).Width = 100
                    .Columns(11).Width = 150
                    .Columns(12).Width = 100
                End With
            Else
                MessageBox.Show("Input ID first", "Get Data", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ListbyDate()
        Try
            Dim ds As New DataSet

            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "Select_DataforID_byDate",
                              New SqlParameter("@from", dtFrom.Text),
                              New SqlParameter("@to", dtTo.Text))

            With dgvInfoList
                .DataSource = ds.Tables(0)
                .Columns(0).HeaderText = "ID"
                .Columns(1).HeaderText = "Lastame"
                .Columns(2).HeaderText = "Firstname"
                .Columns(3).HeaderText = "Middle Initial"
                .Columns(4).HeaderText = "Nickname"
                .Columns(5).HeaderText = "Address"
                .Columns(6).Visible = False
                .Columns(7).HeaderText = "Contact Person"
                .Columns(8).HeaderText = "Contact No"
                .Columns(9).HeaderText = "Contact Address"
                .Columns(10).HeaderText = "SSS No"
                .Columns(11).HeaderText = "Position"
                .Columns(12).HeaderText = "Project"

                .Columns(0).Width = 100
                .Columns(1).Width = 90
                .Columns(2).Width = 90
                .Columns(3).Width = 90
                .Columns(4).Width = 100
                .Columns(5).Width = 200
                .Columns(7).Width = 150
                .Columns(8).Width = 80
                .Columns(9).Width = 200
                .Columns(10).Width = 100
                .Columns(11).Width = 150
                .Columns(12).Width = 100
            End With

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub frmDownloader_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        dgvInfoList.Columns.Clear()
    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnDownload_Click(sender As System.Object, e As System.EventArgs) Handles btnDownload.Click
        Try

        '    Dim xlapp As Excel.Application
        '    Dim xlworkbook As Excel.Workbook
        '    Dim xlworksheet As Excel.Worksheet
        '    Dim misvalue As Object = System.Reflection.Missing.Value
        '    Dim i As Integer
        '    Dim j As Integer
        '    xlapp = New Excel.Application
        '    xlworkbook = xlapp.Workbooks.Add(misvalue)
        '    xlworksheet = xlworkbook.Sheets("Sheet1")
        '    For i = 0 To dgvInfoList.RowCount - 1
        '        For j = 0 To dgvInfoList.ColumnCount - 1
        '            xlworksheet.Cells(i + 1, j + 1) = _
        '                dgvInfoList(j, i).Value.ToString()
        '        Next
        '    Next

        '    Dim saveFileDialog1 As New SaveFileDialog
        '    saveFileDialog1.Filter = "Excel Worksheets (*.xlsx)|*.xlsx|Excel Worksheets 93-2003 (*.xls)|*.xls"
        '    saveFileDialog1.Title = "Save an Excel File"
        '    saveFileDialog1.ShowDialog()
        '    If saveFileDialog1.FileName <> "" Then
        '        xlworksheet.SaveAs(saveFileDialog1.FileName)
        '        MessageBox.Show("File Successfully Downloaded!", "Downloaded", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    Else
        '        MessageBox.Show("Nothing to Download!", "Downloaded", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    End If

        '    xlworkbook.Close()
        '    xlapp.Quit()

        '    releaseObject(xlapp)
        '    releaseObject(xlworkbook)
        '    releaseObject(xlworksheet)

        'Dim StrExport As String = ""
        'For Each C As DataGridViewColumn In dgvInfoList.Columns
        '    StrExport &= """" & C.HeaderText & ""","
        'Next
        'StrExport = StrExport.Substring(0, StrExport.Length - 1)
        'StrExport &= Environment.NewLine

        'For Each R As DataGridViewRow In dgvInfoList.Rows
        '    For Each C As DataGridViewCell In R.Cells
        '        If Not C.Value Is Nothing Then
        '            StrExport &= "" & C.Value.ToString & ","
        '        Else
        '            StrExport &= "" & "" & ""","
        '        End If
        '    Next
        '    StrExport = StrExport.Substring(0, StrExport.Length - 1)
        '    StrExport &= Environment.NewLine
        'Next

        'Dim tw As IO.TextWriter = New IO.StreamWriter("c:\ID(database).txt")
        'MessageBox.Show("File Saved", "Download", MessageBoxButtons.OK, MessageBoxIcon.Information)
        'tw.Write(StrExport)
        'tw.Close()

        Dim filename As String = String.Empty
        Dim sfd1 As New SaveFileDialog()

            sfd1.Filter = "txt files (*.txt)|*.txt"
        sfd1.FilterIndex = 2
        sfd1.RestoreDirectory = True
        sfd1.Title = "Save Text File"

            If sfd1.ShowDialog() = DialogResult.OK Then
                If sfd1.FileName = String.Empty Then
                    MsgBox("Please input filename")
                Else
                    filename = sfd1.FileName.ToString
                    Saveto_TextFile(dgvInfoList, filename)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Saveto_TextFile(ByVal dvList As DataGridView, ByVal filename As String)
        Dim strDestinationFile As String = "" & filename & ".txt"
        Dim tw As TextWriter = New StreamWriter(strDestinationFile)
        Dim LineToWrite As String = String.Empty

        Try
            For _Row As Integer = 0 To dgvInfoList.Rows.Count - 1
                LineToWrite = String.Empty
                For _Column As Integer = 0 To dgvInfoList.Columns.Count - 1
                    If dgvInfoList.Rows(_Row).Cells(_Column).Value IsNot Nothing Then

                        LineToWrite &= vbTab & dgvInfoList.Rows(_Row).Cells(_Column).Value.ToString
                    Else
                        LineToWrite &= vbTab
                    End If
                Next
                LineToWrite = LineToWrite.Remove(0, 1)
                tw.WriteLine(LineToWrite)
            Next
            tw.Flush()
            tw.Close()
            MessageBox.Show("Data Export Successfully", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
            btnDownload.Enabled = False
        Catch ex As Exception
            MessageBox.Show("An error occurred")
            btnDownload.Enabled = False
        End Try
    End Sub

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub rbIndividual_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rbIndividual.CheckedChanged
        If rbIndividual.Checked = True Then
            txtEmpNo.Enabled = True
            Label2.Enabled = True
            btnPreview.Enabled = True
        Else
            txtEmpNo.Enabled = False
            Label2.Enabled = False
            btnPreview.Enabled = False
        End If
    End Sub

    Private Sub rbBatch_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rbBatch.CheckedChanged
        If rbBatch.Checked = True Then
            Label1.Enabled = True
            Label3.Enabled = True
            dtFrom.Enabled = True
            dtTo.Enabled = True
            btnPreview.Enabled = True
        Else
            Label1.Enabled = False
            Label3.Enabled = False
            dtFrom.Enabled = False
            dtTo.Enabled = False
            btnPreview.Enabled = False
        End If
    End Sub

    Private Sub Add_IDExpirationaDate()
        Dim IDExpirationDate As New DataGridViewColumn

        IDExpirationDate.Name = "ExpireDate"
        IDExpirationDate.ValueType = GetType(Date)
        IDExpirationDate.HeaderText = "Expiration Date"
        IDExpirationDate.CellTemplate = New DataGridViewTextBoxCell

        With dgvInfoList
            .Columns.Add(IDExpirationDate)
        End With
    End Sub

    Public Sub LoadPicture_byEmployee()
        Try
            Dim rd As SqlDataReader
            Dim myconnection As New Clsappconfiguration

            rd = SqlHelper.ExecuteReader(myconnection.cnstring, "Select_Photo_byEmployee",
                               New SqlParameter("@employeeno", txtEmpNo.Text))
            While rd.Read
                picData = rd.Item("Fb_Picture")
                sigData = rd.Item("Fb_Signature")
            End While

            picData = picData
            sigData = sigData

            'load picture
            Dim ms As New MemoryStream(picData, 0, picData.Length)
            ms.Write(picData, 0, picData.Length)
            img = Image.FromStream(ms, True)
            'picEmpPhoto.Image = img
            'picEmpPhoto.SizeMode = PictureBoxSizeMode.StretchImage

            'load signature
            Dim ms0 As New MemoryStream(sigData, 0, sigData.Length)
            ms0.Write(sigData, 0, sigData.Length)
            img = Image.FromStream(ms0, True)
            'picEmpSignature.Image = img
            'picEmpSignature.SizeMode = PictureBoxSizeMode.StretchImage
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub LoadPicture_byDate()
        Try
            Dim rd As SqlDataReader
            Dim myconnection As New Clsappconfiguration

            rd = SqlHelper.ExecuteReader(myconnection.cnstring, "Select_Photo_byDate",
                              New SqlParameter("@from", dtFrom.Text),
                              New SqlParameter("@to", dtTo.Text))
            While rd.Read
                picData = rd.Item("Fb_Picture")
                sigData = rd.Item("Fb_Signature")
            End While

            picData = picData
            sigData = sigData

            'load picture
            Dim ms As New MemoryStream(picData, 0, picData.Length)
            ms.Write(picData, 0, picData.Length)
            img = Image.FromStream(ms, True)
            'picEmpPhoto.Image = img
            'picEmpPhoto.SizeMode = PictureBoxSizeMode.StretchImage

            'load signature
            Dim ms0 As New MemoryStream(sigData, 0, sigData.Length)
            ms0.Write(sigData, 0, sigData.Length)
            img = Image.FromStream(ms0, True)
            'picEmpSignature.Image = img
            'picEmpSignature.SizeMode = PictureBoxSizeMode.StretchImage
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class