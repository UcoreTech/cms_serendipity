Imports System.Data.SqlClient.SqlConnection
Imports System.Data.SqlClient
Imports System.Drawing
Imports Microsoft.VisualBasic.FileSystem
Imports System.Data
Imports System.IO
Imports Microsoft.VisualBasic
Imports System.Security
Imports System.Security.Principal.WindowsIdentity
Imports Microsoft.ApplicationBlocks.Data
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Web.Security

Public Class frmMember_Master
    '=====================================================================
    ' Project:          ClickSoftware Personnel System
    ' Client:           Mindshare,Thunderbird
    ' Module:           Form6(Employee Masterfile)
    ' Author:           Jolan P.Mahinay
    ' Description:      Employee 201 file
    ' Date:             February 28,2006
    ' Date Revised :    May 30,2007
    '=====================================================================
#Region "Variables"
    Public orgid As String
    Public xorgid As String
    Public position As CurrencyManager
    Dim mydataset As DataSet
    Dim mycommand As New SqlCommand
    Dim myadapter As New SqlDataAdapter
    Dim chrmodeTemp As String
    Private y As Integer
    Public filename As String = ""

    Public sfirstname(15) As String
    Public slastname(15) As String
    Public smiddlename(15) As String
    Public semployeetype(15) As String
    Public dhired(15) As String
    Public snumber(15) As String
    Public sGender(15) As String
    Public sdepartment(15) As String
    Public smobile_phone(15) As String
    Public saddress_1(15) As String
    Public m_sOldIDNumber As String
    Public oldid(100) As String
    Public resigned As String
    Private getevalid As String
    Public keyemployee As String
    Public employeeID As String
    Public emergencykey As String
    'Public admin As String
    Public keyresigned As Integer = 0
    Public suspendesc As String
    Public bereavement As Integer
    Public fbemployed As Integer
    Public fbMembership As Integer
    Private isFirstLoad As Boolean = False

    Private formMode As String = "Edit"

    Private gCon As New Clsappconfiguration()
#End Region
#Region "Get Property"
    Public Property oldidnumber() As String
        Get
            Return m_sOldIDNumber
        End Get
        Set(ByVal value As String)
            m_sOldIDNumber = value
        End Set
    End Property
    Public Property resigned_you() As String
        Get
            Return resigned
        End Get
        Set(ByVal value As String)
            resigned = value
        End Set
    End Property

    Public Property _getevalid() As String
        Get
            Return getevalid
        End Get
        Set(ByVal value As String)
            getevalid = value
        End Set
    End Property
    Public Property getfxkeyemployee() As String
        Get
            Return keyemployee
        End Get
        Set(ByVal value As String)
            keyemployee = value
        End Set
    End Property
    Public Property getemployeeID() As String
        Get
            Return employeeID
        End Get
        Set(ByVal value As String)
            employeeID = value
        End Set
    End Property
    Public Property getEmergencykey() As String
        Get
            Return emergencykey
        End Get
        Set(ByVal value As String)
            emergencykey = value
        End Set
    End Property
    Public Property getkeyresigned() As Integer
        Get
            Return keyresigned
        End Get
        Set(ByVal value As Integer)
            keyresigned = value
        End Set
    End Property
    Public Property getxorgid() As String
        Get
            Return xorgid
        End Get
        Set(ByVal value As String)
            xorgid = value
        End Set
    End Property
    Public Property getsupendesc() As String
        Get
            Return suspendesc
        End Get
        Set(ByVal value As String)
            suspendesc = value
        End Set
    End Property
    'Public Property getbereavement() As Integer
    '    Get
    '        Return bereavement
    '    End Get
    '    Set(ByVal value As Integer)
    '        bereavement = value
    '    End Set
    'End Property
    Public Property getbereavement() As Boolean
        Get
            Return bereavement
        End Get
        Set(ByVal value As Boolean)
            bereavement = value
        End Set
    End Property
    Public Property getfbemployed() As Boolean
        Get
            Return fbemployed
        End Get
        Set(ByVal value As Boolean)
            fbemployed = value
        End Set
    End Property
    Public Property getfbMembership() As Integer
        Get
            Return fbMembership
        End Get
        Set(ByVal value As Integer)
            fbMembership = value
        End Set
    End Property

    Private empKeyID As String
    Public Property GetEmployeeKeyID() As String
        Get
            Return empKeyID
        End Get
        Set(ByVal value As String)
            empKeyID = value
        End Set
    End Property

#End Region

    Private Sub btnadd_dep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnadd_dep.Click
        frmMember_Dependents.Label1.Text = "Add New Dependents"
        frmMember_Dependents.Show()
        frmMember_Dependents.btnupdate.Visible = False
    End Sub

    Private Sub btnemp_close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_close.Click
        If btnemp_close.Text = "Cancel" Then
            'Call Timekeeping_leaves_ledger_get()
            Call disabledbutton()
            btnemp_add.Text = "New"
            btnemp_add.Image = My.Resources.new3
            Call DisableControls()

            Me.btnemp_next.Enabled = True
            Me.btnemp_previous.Enabled = True
            Me.BTNSEARCH.Enabled = True

            btnrestricted.Visible = True

            btnemp_close.Text = "Close"
            'btnemp_close.Location = New System.Drawing.Point(498, 3)
            btnemp_edit.Visible = True
            btnemp_edit.Text = "Edit"
            btnemp_edit.Image = My.Resources.edit1
            btnemp_delete.Visible = True
            btnemp_add.Visible = True
        ElseIf btnemp_close.Text = "Close" Then
            Me.Close()
        End If
    End Sub


#Region "User rights"
    Public Sub LoaduserRights()
        If frmLogin.admin = "Admin" Then

        ElseIf frmLogin.admin = "User" Then
            'Me.btnresigned.Enabled = False
            Me.btnrestricted.Enabled = False

        End If
    End Sub
#End Region
#Region "Button disabled"
    Public Sub disabledbutton()
        btnadd_dep.Enabled = False
        btnemp_editdepdts.Enabled = False
        btndelete_dep.Enabled = False
    End Sub
#End Region
#Region "Button Enabled"
    Public Sub enabledbutton()
        btnadd_dep.Enabled = True
        btnemp_editdepdts.Enabled = True
        btndelete_dep.Enabled = True
    End Sub
#End Region

#Region "Load Civil Status"
    Private Sub LoadCivilStatus()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_Civil_Status", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                Me.cboemp_civil.Items.Add(myreader.Item(0))
            End While

        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Validate Email add"

    Public Sub validateEmail(ByVal email As String)
        'Dim pattern As String = "^[-a-zA-Z0-9][-.a-zA-Z0-9]*@[-.a-zA-Z0-9]+(\.[-.a-zA-Z0-9]+)*\." & _
        '       "(com|edu|info|gov|int|mil|net|org|biz|name|museum|coop|aero|pro|tv|[a-zA-Z]{2})$"
        'Dim check As New System.Text.RegularExpressions.Regex(pattern, RegexOptions.IgnorePatternWhitespace)
        Dim Expression As New System.Text.RegularExpressions.Regex("\S+@\S+\.\S+")
        'make sure an email address was provided
        If Expression.IsMatch(email) Then

        Else
            MessageBox.Show("The email address is NOT valid.")
        End If
    End Sub
#End Region

    Public Sub Form6_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        isFirstLoad = True


        Call LoaduserRights()
        Call GetAge(EMP_DATEbirth.Value)
        Me.txtfullname.Text = Me.temp_lname.Text + "," + Me.temp_fname.Text + " " + Me.temp_midname.Text

        txtdateregular.Text = ""
        txtremarks.Visible = False

        Call load_configurationfile()
        Call load_salarygrade_1()
        Call Employee_Type()
        Call EmployeeStatus()
        Call Load_RecordId_Masterfile()
        Call LoadCivilStatus()
        Call GetFirstRecord()
        Call PositionTitle()
        Call LoadPayrollCode()
        Call LoadSalaryRate()
        Call PayrollDescription()
        Call LoadTaxCode()
        Call LoadEloadingRegistrationDetails(txtEmployeeNo.Text)


        '======================================
        Dateresigned.Value = Microsoft.VisualBasic.FormatDateTime(Now, DateFormat.ShortDate)
        'emp_pic.ImageLocation = Application.StartupPath & "\SplashScreen v3.jpg"
        'emp_pic.Load()
        '======================================
        btnemp_next.Visible = True
        btnemp_previous.Visible = True
        btnemp_next.Visible = True
        btnemp_previous.Visible = True
        BTNSEARCH.Enabled = True
        '======================================
        Call Me.DisableControls()
        '======================================
    End Sub
#Region "Position Title"
    Private Sub PositionTitle()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_m_Employment_Positions")
        Try
            While rd.Read
                Me.cbotitledesignation.Items.Add(rd.Item(0))
            End While
            rd.Close()
        Catch ex As Exception

        End Try
    End Sub
#End Region
#Region "Tax Code"
    Private Sub LoadTaxCode()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_Member_LoadTaxCode")
        Try
            While rd.Read
                Me.cbotaxcode.Items.Add(rd.Item(0))
            End While
            rd.Close()
        Catch ex As Exception
        End Try
    End Sub
#End Region
#Region "Payroll Description"
    Private Sub PayrollDescription()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_Member_LoadPayrollDesc")
        Try
            While rd.Read
                Me.cbopayroll.Items.Add(rd.Item(1))
            End While
            rd.Close()
        Catch ex As Exception

        End Try
    End Sub
#End Region
#Region "Salary Rate"
    Private Sub LoadSalaryRate()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_Member_LoadSalaryRate")
        Try
            While rd.Read
                Me.cborate.Items.Add(rd.Item(1))
            End While
            rd.Close()
        Catch ex As Exception

        End Try
    End Sub
#End Region
#Region "Load Payroll Code"
    Private Sub LoadPayrollCode()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_Member_LoadPayCode")
        Try
            While rd.Read
                Me.cboPaycode.Items.Add(rd.Item(1))
            End While
            rd.Close()
        Catch ex As Exception

        End Try
    End Sub
#End Region
#Region "Check Employee IDnumber if Duplicate"
    Private Sub CheckDuplicateIDnumber()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("CIMS_m_Member_checkduplicateID", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@fcEmployeeNo", SqlDbType.VarChar, 50).Value = Me.txtEmployeeNo.Text
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.HasRows Then
                MessageBox.Show("IDnumber already exist please try another one", "IDnumber Exists..", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                Call AddEditnewExistingmember(fbemployed, Me.txtEmployeeNo.Text.Trim, "", Me.temp_fname.Text, Me.temp_midname.Text, Me.temp_lname.Text, _
                                              Me.cboemp_gender.Text, Me.cboemp_civil.Text, Me.EMP_DATEbirth.Text, Me.temp_placebirth.Text, Me.txtResidenceadd.Text, _
                                              Me.txtprovincialadd.Text, Me.txtresidencephone.Text, Me.txtMobileNo.Text, Me.emp_company.Text, Me.txtofficeadd.Text, _
                                              Me.emp_Datehired.Value, Me.txtlocalofficenumber.Text, Me.txtofficenumber.Text, Me.txtbasicpay.Text, Me.mem_MemberDate.Value, _
                                              bereavement, Me.txtwithdrawal.Text, Me.txtEmailAddress.Text, Me.txtpayrollcontriamount.Text, Me.mem_PaycontDate.Value, Me.txtorgchart.Tag, _
                                              Me.cbotaxcode.Text, "", Me.Cbomem_Status.Text, Me.cbotitledesignation.Text, Me.cborate.Text, Me.cboPaycode.Text, Me.cbopayroll.Text)


            End If
            myreader.Close()
        Finally
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Add new existing member"
    Private Sub AddEditnewExistingmember(ByVal fbemployed As Boolean, ByVal EmpID As String, ByVal MembershipID As String, _
                                     ByVal firstname As String, ByVal middlename As String, ByVal lastname As String, ByVal gender As String, ByVal civilstatus As String, _
                                     ByVal birthday As Date, ByVal birthplace As String, ByVal residenceaddress As String, ByVal provincialaddress As String, _
                                     ByVal residencetelno As String, ByVal mobileno As String, ByVal company As String, ByVal companyaddress As String, ByVal datehired As Date, _
                                     ByVal localno As String, ByVal officeno As String, ByVal salary As Decimal, ByVal membershipdate As Date, ByVal bereavement As Boolean, _
                                     ByVal withdrawdate As String, ByVal emailaddress As String, ByVal contribution As Decimal, ByVal contributiondate As Date, ByVal orgid As String, _
                                     ByVal taxcode As String, ByVal coopname As String, ByVal membershipstatus As String, ByVal position As String, ByVal salarycategory As String, _
                                     ByVal paycodedesc As String, ByVal prolldesc As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Master_AddEdit", _
                                      New SqlParameter("@fbEmployed", fbemployed), _
                                      New SqlParameter("@fcEmployeeNo", EmpID), _
                                      New SqlParameter("@fcMembershipNo", MembershipID), _
                                      New SqlParameter("@fcFirstName", firstname), _
                                      New SqlParameter("@fcMiddleName", middlename), _
                                      New SqlParameter("@fcLastName", lastname), _
                                      New SqlParameter("@fcGender", gender), _
                                      New SqlParameter("@fcStatus", civilstatus), _
                                      New SqlParameter("@fcBirthday", birthday), _
                                      New SqlParameter("@fcBirthplace", birthplace), _
                                      New SqlParameter("@fcResidenceAddress", residenceaddress), _
                                      New SqlParameter("@fcProvincialAddress", provincialaddress), _
                                      New SqlParameter("@fcResidenceTelNo", residencetelno), _
                                      New SqlParameter("@fcMobileNo", mobileno), _
                                      New SqlParameter("@fcCompany", company), _
                                      New SqlParameter("@fcCompanyAddress", companyaddress), _
                                      New SqlParameter("@dtDateHired", datehired), _
                                      New SqlParameter("@fcLocalNo", localno), _
                                      New SqlParameter("@fcOfficeNo", officeno), _
                                      New SqlParameter("@fdSalary", salary), _
                                      New SqlParameter("@dtMembershipDate", membershipdate), _
                                      New SqlParameter("@fbBereavementMemberStatus", bereavement), _
                                      New SqlParameter("@fbWithrawDate", withdrawdate), _
                                      New SqlParameter("@fcEmailAddress", emailaddress), _
                                      New SqlParameter("@fdContribution", contribution), _
                                      New SqlParameter("@fdContributionDate", contributiondate), _
                                      New SqlParameter("@orgID", orgid), _
                                      New SqlParameter("@fk_TaxCode", taxcode), _
                                      New SqlParameter("@coopName", coopname), _
                                      New SqlParameter("@membershipStatus_Desc", membershipstatus), _
                                      New SqlParameter("@position", position), _
                                      New SqlParameter("@SalaryCategoryDesc", salarycategory), _
                                      New SqlParameter("@PayCodeDesc", paycodedesc), _
                                      New SqlParameter("@PRollDesc", prolldesc), _
                                      New SqlParameter("@isExempt", rdoExempt.Checked))

            trans.Commit()

            Call Integrate_CIMSToAccounting()

            Dim x As New DialogResult
            x = MessageBox.Show("Record successfully save!", "Saving Record...", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "CIMS_Member_Master_AddEdit", MessageBoxButtons.OK)
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Edit new existing member"
    Private Sub EditnewExistingmember(ByVal fbemployed As Boolean, ByVal EmpID As String, ByVal MembershipID As String, _
                                         ByVal firstname As String, ByVal middlename As String, ByVal lastname As String, ByVal gender As String, ByVal civilstatus As String, _
                                         ByVal birthday As Date, ByVal birthplace As String, ByVal residenceaddress As String, ByVal provincialaddress As String, _
                                         ByVal residencetelno As String, ByVal mobileno As String, ByVal company As String, ByVal companyaddress As String, ByVal datehired As Date, _
                                         ByVal localno As String, ByVal officeno As String, ByVal salary As Decimal, ByVal membershipdate As Date, ByVal bereavement As Boolean, _
                                         ByVal withdrawdate As String, ByVal emailaddress As String, ByVal contribution As Decimal, ByVal contributiondate As Date, ByVal orgid As String, _
                                         ByVal taxcode As String, ByVal coopname As String, ByVal membershipstatus As String, ByVal position As String, ByVal salarycategory As String, _
                                         ByVal paycodedesc As String, ByVal prolldesc As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Master_Edit", _
                                      New SqlParameter("@fbEmployed", fbemployed), _
                                      New SqlParameter("@fcEmployeeNo", EmpID), _
                                      New SqlParameter("@fcMembershipNo", MembershipID), _
                                      New SqlParameter("@fcFirstName", firstname), _
                                      New SqlParameter("@fcMiddleName", middlename), _
                                      New SqlParameter("@fcLastName", lastname), _
                                      New SqlParameter("@fcGender", gender), _
                                      New SqlParameter("@fcStatus", civilstatus), _
                                      New SqlParameter("@fcBirthday", birthday), _
                                      New SqlParameter("@fcBirthplace", birthplace), _
                                      New SqlParameter("@fcResidenceAddress", residenceaddress), _
                                      New SqlParameter("@fcProvincialAddress", provincialaddress), _
                                      New SqlParameter("@fcResidenceTelNo", residencetelno), _
                                      New SqlParameter("@fcMobileNo", mobileno), _
                                      New SqlParameter("@fcCompany", company), _
                                      New SqlParameter("@fcCompanyAddress", companyaddress), _
                                      New SqlParameter("@dtDateHired", datehired), _
                                      New SqlParameter("@fcLocalNo", localno), _
                                      New SqlParameter("@fcOfficeNo", officeno), _
                                      New SqlParameter("@fdSalary", salary), _
                                      New SqlParameter("@dtMembershipDate", membershipdate), _
                                      New SqlParameter("@fbBereavementMemberStatus", bereavement), _
                                      New SqlParameter("@fbWithrawDate", withdrawdate), _
                                      New SqlParameter("@fcEmailAddress", emailaddress), _
                                      New SqlParameter("@fdContribution", contribution), _
                                      New SqlParameter("@fdContributionDate", contributiondate), _
                                      New SqlParameter("@orgID", orgid), _
                                      New SqlParameter("@fk_TaxCode", taxcode), _
                                      New SqlParameter("@coopName", coopname), _
                                      New SqlParameter("@membershipStatus_Desc", membershipstatus), _
                                      New SqlParameter("@position", position), _
                                      New SqlParameter("@SalaryCategoryDesc", salarycategory), _
                                      New SqlParameter("@PayCodeDesc", paycodedesc), _
                                      New SqlParameter("@PRollDesc", prolldesc), _
                                      New SqlParameter("@isExempt", rdoExempt.Checked))



            Call Integrate_CIMSToAccounting()

            Dim x As New DialogResult
            x = MessageBox.Show("Record successfully updated!", "Updating Record...", MessageBoxButtons.OK, MessageBoxIcon.Information)


            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            Throw ex
            MessageBox.Show(ex.Message, "CIMS_Member_Master_AddEdit", MessageBoxButtons.OK)
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region

#Region "Generate Membership ID"
    Private Sub GenerateMembershipID()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_GENERATE_MEMBERSHIPID")
        Try
            While rd.Read
                Me.txtMemberID.Text = rd.Item(0)
            End While
            rd.Close()
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Get first Member Record ascending Order"
    Private Sub GetFirstRecord()
        Dim gcon As New Clsappconfiguration
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Member_GetfirstRecord")
                While rd.Read
                    Me.txtpk_Employee.Text = rd.Item(0).ToString.Trim
                End While
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message, "GetFirstRecord")
        End Try
    End Sub
#End Region

    '''''''''''''''''''''''''''''''
    ''clear txt for emp-masterfile'
    '''''''''''''''''''''''''''''''

    Public Sub cleartxt()
        Me.txtMemberID.Text = ""
        Me.txtkeycompany.Text = ""
        Me.txtdescdivision.Text = ""
        Me.txtdescdepartment.Text = ""
        Me.txtdescsection.Text = ""
        Me.txtpk_Employee.Text = ""
        Me.txtEmployeeNo.Text = ""

        Me.lblemployee_name.Text = ""


        emp_Datehired.Text = ""
        EMP_DATEbirth.Text = ""

        Me.txtType_employee.Text = ""
        Me.txtfxkeypositionlevel.Text = ""
        Me.TXTKEYEMPLOYEEID.Text = ""
        Me.txtDepartment2.Text = ""
        Me.txtResidenceadd.Text = ""

        Me.Text = "" 'for fullname
        ' Me.Label39.Text = "" 'tenureyrs
        Me.txtfullname.Text = ""
        Me.txtitemno.Text = ""
        Me.emp_pic.ImageLocation = ""

        Me.txtweight.Text = ""
        Me.temp_fname.Text = ""
        Me.temp_lname.Text = ""
        Me.temp_midname.Text = ""
        Me.cboEmp_type.Text = ""
        Me.cboemp_gender.Text = ""
        Me.cboemp_civil.Text = ""
        Me.cboemp_status.Text = ""
        Me.txtorgchart.Text = ""
        Me.txtorgchart.Tag = ""
        Me.txtresidencephone.Text = ""
        Me.emp_extphone.Text = ""
        Me.txtMobileNo.Text = ""
        Me.txtEmailAddress.Text = ""
        Me.temp_designation.Text = ""
        Me.txtprovincialadd.Text = ""
        Me.txtEmployeeNo.Text = ""
        Me.temp_placebirth.Text = ""
        Me.txtofficeadd.Text = ""
        Me.temp_citizen.Text = ""
        Me.temp_height.Text = ""
        Me.txtofficenumber.Text = ""
        Me.txtlocalofficenumber.Text = ""
        Me.temp_add3.Text = ""
        Me.temp_marks.Text = ""


        'Me.tlocation.Text = ""
        'Me.txtlocation1.Text = ""

        Me.cbotitledesignation.Text = ""
        Me.cbosalarygrade.Text = ""
        Me.txtdateregular.Text = ""

        'addtional

        'Me.Cbomem_Bereavement.Text = ""
        'Me.mem_amountpaid.Text = "0.00"
        Me.txtpayrollcontriamount.Text = "0.00"
        Me.mem_PaycontDate.Text = ""
        Me.mem_WithdrawDate.Text = ""
        Me.mem_MemberDate.Text = ""
        Me.Cbomem_Status.Text = ""

        Me.cboPaycode.Text = ""
        Me.cbopayroll.Text = ""
        Me.cborate.Text = ""
        Me.cbotaxcode.Text = ""
        Me.emp_Ytenures.Text = ""
        Me.chkyes.Checked = False
        Me.chkNo.Checked = False

    End Sub

#Region "Clear listview"
    Public Sub clearlistview()
        Me.LvlEmployeeDependent.Clear()
    End Sub

#End Region

    Private Sub btnemp_add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_add.Click

        formMode = "New"

        If btnemp_add.Text = "New" Then

            'emp_pic.ImageLocation = Application.StartupPath & "\SplashScreen v3.jpg"  'form6.txtpicture.Text = form6.txtpicture.Text Then
            'emp_pic.Load()
            If isEloadingMemberValid() Then
                emp_Datehired.Value = Microsoft.VisualBasic.FormatDateTime(Now, DateFormat.ShortDate)
                EMP_DATEbirth.Value = Microsoft.VisualBasic.FormatDateTime(Now, DateFormat.ShortDate)

                Call enabledbutton()
                Call clearlistview()
                Me.TXTRECID.Text = ""
                Me.txtage1.Text = "0"
                Me.txtbasicpay.Text = "0.00"
                Call EnableControls()
                Call cleartxt()
                Call GenerateMembershipID()
                btnemp_delete.Visible = False
                btnemp_edit.Visible = False
                btnemp_close.Text = "Cancel"
                'btnemp_close.Location = New System.Drawing.Point(771, 4)

                btnemp_add.Text = "Save"
                btnemp_add.Image = My.Resources.save2
            End If

        ElseIf btnemp_add.Text = "Save" Then



            If isEloadingMemberValid() Then
                If Me.txtorgchart.Text <> "" Then
                    Call CheckDuplicateIDnumber()
                    tabMember.Enabled = True
                    btnemp_add.Text = "New"
                    btnemp_add.Image = My.Resources.new3
                    btnemp_close.Text = "Close"
                    btnemp_close.Location = New System.Drawing.Point(498, 3)
                    btnemp_edit.Visible = True
                    btnemp_delete.Visible = True

                    Call DisableControls()
                    Me.lblemployee_name.Text = Me.temp_fname.Text + " " + Me.temp_lname.Text + " " + Me.temp_midname.Text
                Else
                    MessageBox.Show("Supply the required fields", "Cannot Save", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If
            End If

        End If

    End Sub

#Region "Load Fxkeyemployee in form6"
    Public Sub Loadfxkeyemployee()
        Dim mycon As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_fxkeyemployee_load", mycon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        mycon.sqlconn.Open()
        Dim reader As SqlDataReader = cmd.ExecuteReader
        Try
            While reader.Read
                Me.TXTKEYEMPLOYEEID.Text = reader.Item(0)
            End While
            reader.Close()
            mycon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Load Fxkeyemployee", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region

    Private Sub BTNSEARCH_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTNSEARCH.Click
        Employee_Search.Show()
    End Sub



#Region "Disable text"
    Private Sub DisableControls()

        Me.grpPayrollStatus.Enabled = False
        Me.chkNo.Enabled = False
        Me.chkyes.Enabled = False
        Me.txtMemberID.ReadOnly = True
        Me.PictureBox2.Enabled = False

        Me.txtResidenceadd.ReadOnly = True
        Me.txtDepartment2.ReadOnly = True
        Me.txtorgchart.ReadOnly = True

        Me.txtfullname.Enabled = False
        Me.txtitemno.Enabled = False


        Me.txtweight.ReadOnly = True
        Me.temp_fname.ReadOnly = True
        Me.temp_lname.ReadOnly = True
        Me.temp_midname.ReadOnly = True

        Me.txtorgchart.ReadOnly = True
        Me.txtresidencephone.ReadOnly = True
        Me.emp_extphone.ReadOnly = True
        Me.txtMobileNo.ReadOnly = True
        Me.txtEmailAddress.ReadOnly = True
        Me.temp_designation.Enabled = False
        Me.txtprovincialadd.ReadOnly = True
        Me.txtEmployeeNo.ReadOnly = True
        Me.temp_placebirth.ReadOnly = True
        Me.txtofficeadd.ReadOnly = True
        Me.temp_citizen.ReadOnly = True
        Me.temp_height.ReadOnly = True
        Me.txtofficenumber.ReadOnly = True
        Me.txtlocalofficenumber.ReadOnly = True
        Me.temp_add3.ReadOnly = True
        Me.temp_marks.ReadOnly = True

        'ADDITIONAL DISABLED TEXTBOX

        'Me.Cbomem_Bereavement.ReadOnly = True
        'Me.mem_amountpaid.ReadOnly = True
        Me.txtpayrollcontriamount.ReadOnly = True
        Me.emp_Ytenures.ReadOnly = True
        Me.emp_company.ReadOnly = True

        Me.txtdateregular.ReadOnly = True
        Me.txtorgchart.ReadOnly = True
        With Me
            .txtweight.ReadOnly = True
        End With
        'Date and combo
        cboemp_status.Enabled = False
        cboemp_gender.Enabled = False
        cboemp_civil.Enabled = False
        EMP_DATEbirth.Enabled = False
        'Cbomem_Bereavement.Enabled = False
        Cbomem_Status.Enabled = False
        mem_MemberDate.Enabled = False
        mem_WithdrawDate.Enabled = False
        mem_PaycontDate.Enabled = False
        emp_Datehired.Enabled = False

        cboEmp_type.Enabled = False
        cbotitledesignation.Enabled = False
        cbopayroll.Enabled = False
        cborate.Enabled = False
        cbotaxcode.Enabled = False
        cboPositionLevel.Enabled = False
        Me.cboPaycode.Enabled = False
        Me.txtwithdrawal.Enabled = False

        btnAddMobileNo.Enabled = False
        chkIsEloader.Enabled = False
        txtEloaderPIN.Enabled = False

        gridMobileNos.Enabled = False
        btnEditMobile.Enabled = False

    End Sub
#End Region
#Region "Enable text"
    Public Sub EnableControls()
        Me.grpPayrollStatus.Enabled = True
        Me.chkyes.Enabled = True
        Me.chkNo.Enabled = True
        Me.txtMemberID.ReadOnly = False
        Me.PictureBox2.Enabled = True


        Me.emp_Datehired.Enabled = True
        Me.EMP_DATEbirth.Enabled = True


        Me.txtResidenceadd.ReadOnly = False
        Me.txtDepartment2.ReadOnly = False
        Me.txtorgchart.ReadOnly = False
        'Me.Text = "" 'for fullname

        Me.txtfullname.ReadOnly = False
        Me.txtitemno.ReadOnly = False
        'Me.emp_pic.ImageLocation = ""

        Me.txtweight.ReadOnly = False
        Me.temp_fname.ReadOnly = False
        Me.temp_lname.ReadOnly = False
        Me.temp_midname.ReadOnly = False
        Me.cboEmp_type.Enabled = True
        Me.cboemp_gender.Enabled = True
        Me.cboemp_civil.Enabled = True
        Me.cboemp_status.Enabled = True
        Me.txtorgchart.ReadOnly = False
        'Me.txtorgchart.Tag = ""
        Me.txtresidencephone.ReadOnly = False
        Me.emp_extphone.ReadOnly = False
        Me.txtMobileNo.ReadOnly = False
        Me.txtEmailAddress.ReadOnly = False
        Me.temp_designation.ReadOnly = False
        Me.txtprovincialadd.ReadOnly = False
        Me.txtEmployeeNo.ReadOnly = False
        Me.temp_placebirth.ReadOnly = False
        Me.txtofficeadd.ReadOnly = False
        Me.temp_citizen.ReadOnly = False
        Me.temp_height.ReadOnly = False
        Me.txtofficenumber.ReadOnly = False
        Me.txtlocalofficenumber.ReadOnly = False
        Me.temp_add3.ReadOnly = False
        Me.temp_marks.ReadOnly = False

        'ADDITIONAL ENABLED TEXTBOX    

        'Me.Cbomem_Bereavement.ReadOnly = False
        'Me.mem_amountpaid.ReadOnly = False
        Me.txtpayrollcontriamount.ReadOnly = False
        Me.emp_Ytenures.ReadOnly = False
        Me.emp_company.ReadOnly = False

        'Me.tlocation.ReadOnly = False
        'Me.txtlocation1.ReadOnly = False

        Me.cbotitledesignation.Enabled = True
        Me.cbosalarygrade.Enabled = True
        Me.txtdateregular.ReadOnly = False
        With Me
            .txtweight.ReadOnly = False

            .cborate.Enabled = True
            .txtbasicpay.ReadOnly = False

        End With
        'Date and Combo
        cboemp_status.Enabled = True
        cboemp_gender.Enabled = True
        cboemp_civil.Enabled = True
        EMP_DATEbirth.Enabled = True
        'Cbomem_Bereavement.Enabled = True
        Cbomem_Status.Enabled = True
        mem_MemberDate.Enabled = True
        mem_WithdrawDate.Enabled = True
        mem_PaycontDate.Enabled = True
        emp_Datehired.Enabled = True

        cboEmp_type.Enabled = True
        cbotitledesignation.Enabled = True
        cbopayroll.Enabled = True
        cborate.Enabled = True
        cbotaxcode.Enabled = True
        cboPositionLevel.Enabled = True
        Me.cboPaycode.Enabled = True

        Me.txtwithdrawal.Enabled = True

        btnAddMobileNo.Enabled = True
        chkIsEloader.Enabled = True
        txtEloaderPIN.Enabled = True

        gridMobileNos.Enabled = True
        btnEditMobile.Enabled = True
    End Sub
#End Region

    Private Sub btnemp_edit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_edit.Click
        formMode = "Edit"

        If btnemp_edit.Text = "Edit" Then

            Call EnableControls()
            Call enabledbutton()

            btnemp_edit.Text = "Update"
            btnemp_edit.Image = My.Resources.save2
            btnemp_close.Text = "Cancel"
            'btnemp_close.Location = New System.Drawing.Point(429, 3)
            btnemp_add.Visible = False
            btnemp_delete.Visible = False

            Me.btnemp_next.Enabled = False
            Me.btnemp_previous.Enabled = False
            Me.BTNSEARCH.Enabled = False

        ElseIf btnemp_edit.Text = "Update" Then
            Try
                If isEloadingMemberValid() Then
                    If Me.txtorgchart.Text <> "" Then

                        Call EditnewExistingmember(fbemployed, Me.txtEmployeeNo.Text.Trim, Me.txtMemberID.Text.Trim, Me.temp_fname.Text, Me.temp_midname.Text, Me.temp_lname.Text, _
                                                                     Me.cboemp_gender.Text, Me.cboemp_civil.Text.Trim, Me.EMP_DATEbirth.Value.Date, Me.temp_placebirth.Text, Me.txtResidenceadd.Text, _
                                                                     Me.txtprovincialadd.Text, Me.txtresidencephone.Text, Me.txtMobileNo.Text, Me.emp_company.Text, Me.txtofficeadd.Text, _
                                                                     Me.emp_Datehired.Value.Date, Me.txtlocalofficenumber.Text, Me.txtofficenumber.Text, Me.txtbasicpay.Text, Me.mem_MemberDate.Value.Date, _
                                                                     bereavement, Me.txtwithdrawal.Text, Me.txtEmailAddress.Text, Me.txtpayrollcontriamount.Text, Me.mem_PaycontDate.Value.Date, Me.txtorgchart.Tag, _
                                                                     Me.cbotaxcode.Text, "", Me.Cbomem_Status.Text, Me.cbotitledesignation.Text, Me.cborate.Text, Me.cboPaycode.Text, Me.cbopayroll.Text)



                        'eloading Service Registration
                        If chkIsEloader.Checked = True Then
                            Call RegisterMemberToEloadingApps(txtEmployeeNo.Text.Trim, txtEloaderPIN.Text, txtEloadingLimit.Text)
                        Else
                            Call UnRegisterMemberToEloadingApps(txtEmployeeNo.Text.Trim)
                        End If

                        btnrestricted.Visible = True
                        DisableControls()
                        btnemp_edit.Text = "Edit"
                        btnemp_edit.Image = My.Resources.edit1
                        btnemp_close.Text = "Close"
                        btnemp_add.Visible = True
                        btnemp_delete.Visible = True
                        btnrestricted.Visible = True
                        btnemp_next.Enabled = True
                        btnemp_previous.Enabled = True
                        BTNSEARCH.Enabled = True
                    Else
                        MessageBox.Show("Supply the required fields", "Cannot Update", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    End If
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    '=========================================
    '''''''''''''''''''''''' 
    ''NAVIGATING RECORDS  ''
    ''created aug 25,2006 ''
    '==========================================    

    Private Sub Navigate_Record(ByVal pkemployee As String, ByVal chardirection As String)


        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_Member_RecordNavigation", _
                                     New SqlParameter("@fxkeyemployee", pkemployee), _
                                     New SqlParameter("@chrMode", chardirection))
        Try
            While rd.Read
                Me.txtpk_Employee.Text = rd.Item(0)
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Navigation", MessageBoxButtons.OK)
        End Try


    End Sub

    'Private Sub btnaddpic_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnaddpic.Click
    '    Me.OpenFileDialog1.FileName = ""
    '    Me.OpenFileDialog1.Filter = "jpg (*.jpg)|*.* |All Files (*.*)|*.*"
    '    Me.OpenFileDialog1.FilterIndex = 2
    '    Try
    '        If Me.OpenFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
    '            Try
    '                txtpicture.Text = Me.OpenFileDialog1.FileName
    '                emp_pic.Image = New Bitmap(OpenFileDialog1.FileName)
    '            Catch ex As Exception
    '                MessageBox.Show("File Format is not valid", "Invalid Format", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '            End Try
    '        End If
    '    Catch ex As Exception
    '    End Try
    'End Sub
    'Public Sub getin_picture()
    '    Dim appRdr As New System.Configuration.AppSettingsReader
    '    Dim myconnection As New Clsappconfiguration
    '    Dim cmd As New SqlCommand("INSERT INTO Temployee_picture (Picturedata,idnumber) " & _
    '        "VALUES (@picturedata,idnumber)", myconnection.sqlconn)
    '    Dim strBLOBFilePath As String = txtpicture.Text
    '    '"C:\Documents and Settings\All Users\Documents" & _
    '    '"\My Pictures\Sample Pictures\winter.jpg"
    '    Dim fsBLOBFile As New FileStream(strBLOBFilePath, _
    '        FileMode.Open, FileAccess.Read)
    '    Dim bytBLOBData(fsBLOBFile.Length() - 1) As Byte
    '    fsBLOBFile.Read(bytBLOBData, 0, bytBLOBData.Length)
    '    fsBLOBFile.Close()
    '    Dim prm As New SqlParameter("@picturedata", SqlDbType.VarBinary, _
    '        bytBLOBData.Length, ParameterDirection.Input, False, _
    '        0, 0, Nothing, DataRowVersion.Current, bytBLOBData)
    '    cmd.Parameters.Add(prm)
    '    myconnection.sqlconn.Open()
    '    cmd.ExecuteNonQuery()
    '    myconnection.sqlconn.Close()
    'End Sub

    Public Sub LOAD_PICTURE_FROM_DBASE(ByVal i As String)
        'Dim myconnection As New SqlConnection(My.Settings.CNString)
        'Dim cmd As New SqlCommand("SELECT BLOBID, " & _
        '"BLOBData FROM BLOBTest WHERE BLOBID=", DataGridView1.CurrentRow.Cells(0).Value)
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        'Dim myconnection As New SqlConnection(My.Settings.CNString)

        Dim cmd As New SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet

        cmd.CommandText = "select * from Temployee_picture where idnumber=" & "'" & i & "'" 'i & "'"
        cmd.CommandType = CommandType.Text
        cmd.Connection = myconnection.sqlconn
        myconnection.sqlconn.Open()

        da.SelectCommand = cmd
        da.Fill(ds, "Temployee_picture")

        Dim c As Integer = ds.Tables("Temployee_picture").Rows.Count
        If c > 0 Then
            Dim bytBLOBData() As Byte = _
                ds.Tables("Temployee_picture").Rows(c - 1)("picturedata")
            Dim stmBLOBData As New MemoryStream(bytBLOBData)
            emp_pic.Image = Image.FromStream(stmBLOBData)


        Else
            emp_pic.ImageLocation = Application.StartupPath & "\SplashScreen v3.jpg"
            emp_pic.Load()
        End If
        myconnection.sqlconn.Close()
    End Sub

    Private Sub Temp_educ_attainmentBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


    End Sub

#Region "Delete Second Company"
    Private Sub deleteSecondCompany()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_secondcompany_delete", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        With cmd.Parameters
            .Add("@fxkeyemployee", SqlDbType.Char, 10).Value = Me.TXTRECID.Text
        End With
        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region

    Private Sub btnemp_delete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_delete.Click
        Dim x As New DialogResult
        x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        If x = System.Windows.Forms.DialogResult.OK Then
            Call TaggedDeleted(Me.txtEmployeeNo.Text.Trim, frmLogin.admin)
            Call cleartxt()
        ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
        End If
    End Sub

#Region "Tagging Deleted in Member Records"
    Private Sub TaggedDeleted(ByVal empID As String, ByVal Users As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Master_Delete", _
                                      New SqlParameter("@employeeNo", empID), _
                                      New SqlParameter("@user", Users))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "CIMS_Member_Master_Delete")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Delete masterfile permanently"
    Private Sub deletemasterfilepermanent(ByVal fxkey As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_delete_permanently", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            cmd.Parameters.Add("@fxkeyemployee", SqlDbType.VarChar, 10).Value = fxkey
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show("There is no file delete", "eHRMS", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub
#End Region
    Private Sub Delete_all_employee_records()

        Dim myconnection As New Clsappconfiguration
        'Dim cmd As New SqlCommand("usp_per_employee_master_delete_update", myconnection.sqlconn)
        Dim cmd As New SqlCommand("usp_per_employee_master_delete_update_PERSTK", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            cmd.Parameters.Add("@fxkeyemployee", SqlDbType.VarChar, 10).Value = Me.TXTRECID.Text
            'cmd.Parameters.Add("@Emp_Deleted", SqlDbType.Bit).Value = 1
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Delete Record", MessageBoxButtons.OK)
        End Try
    End Sub

    Private Sub btndelete_dep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete_dep.Click
        Try

            Dim x As New DialogResult
            Dim pksid As String = Me.LvlEmployeeDependent.SelectedItems(0).SubItems(4).Text 'key_id
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            If x = System.Windows.Forms.DialogResult.OK Then
                If pksid <> "" Then
                    Call DeleteDependents(pksid)
                    Call GetmemberDependents(Me.txtEmployeeNo.Text.Trim)
                End If
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception
        End Try
    End Sub
#Region "Delete Dependents"
    Private Sub DeleteDependents(ByVal pks As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Dependents_Delete", _
                                     New SqlParameter("@employeeNo", pks))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete dependents")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region

#Region "Employee Disciplinary Case Delete/stored proc"
    Private Sub setdefaultvaluedatesuspend()
        Dim mycon As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_update_suspensiondate", mycon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            With cmd.Parameters
                .Add("@OPTION", SqlDbType.Int).Value = 2
                .Add("@fxKeyEmployee", SqlDbType.Char, 6).Value = Me.TXTKEYEMPLOYEEID.Text
                .Add("@fdDateSuspendedFrom", SqlDbType.SmallDateTime).Value = "1/1/1900"
                .Add("@fdDateSuspendedTo", SqlDbType.SmallDateTime).Value = "1/1/1900"
            End With
            mycon.sqlconn.Open()
            cmd.ExecuteNonQuery()
            mycon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Suspension", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region

    Private empIDValue As String
    Public Property empIDs() As String
        Get
            Return empIDValue
        End Get
        Set(ByVal value As String)
            empIDValue = value
        End Set
    End Property

    Private Sub txtbasicpay_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtbasicpay.KeyPress
        e.Handled = Not CurrencyInput_Validate(e.KeyChar)

    End Sub
    Private Sub txtpermonth_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not CurrencyInput_Validate(e.KeyChar)
    End Sub
    Private Sub txtecola_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not CurrencyInput_Validate(e.KeyChar)
    End Sub

    Public Function CurrencyInput_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = ".") And Not (C = "-") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Numeric only")
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub emp_Datehired_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles emp_Datehired.TextChanged

    End Sub


    Private Sub emp_Datehired_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles emp_Datehired.ValueChanged


    End Sub
    Public Sub computedatetenures()
        'Try
        '    Dim iMonthHired As Long = 0
        '    iMonthHired = DateDiff(DateInterval.Month, Me.emp_Datehired.Value, Now.Date)
        '    If iMonthHired < 12 Then
        '        Label39.Text = "0"
        '    Else
        '        Label39.Text = CInt(DateDiff(DateInterval.Month, Me.emp_Datehired.Value, Now) / 12)
        '    End If
        '    Me.lblmonth.Text = iMonthHired Mod 12
        'Catch ex As Exception
        'End Try
    End Sub


    Private Sub EMP_DATEbirth_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles EMP_DATEbirth.TextChanged
        'Call compute_age()
        Call GetAge(EMP_DATEbirth.Value)
    End Sub
    Private Sub EMP_DATEbirth_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EMP_DATEbirth.ValueChanged
        'Call compute_age()
        Call GetAge(EMP_DATEbirth.Value)
    End Sub
    Public Sub compute_age()
        Dim month1 As Integer = EMP_DATEbirth.Value.Month
        Dim month2 As Integer = Now.Month
        Dim oldday As Integer = EMP_DATEbirth.Value.Day
        Dim presentday As Integer = Now.Day
        Dim xBday As New Date

        Dim x, y, a, b As Integer

        x = EMP_DATEbirth.Value.Month
        y = Now.Month
        a = EMP_DATEbirth.Value.Day
        b = Now.Day

        If x >= y And a > b Then

            'Label41.Text = DateDiff(DateInterval.Year, EMP_DATEbirth.Value, Now) - 1 '& " yr's" & " " & "old"
            Me.txtage1.Text = DateDiff(DateInterval.Year, EMP_DATEbirth.Value, Now) - 1
        ElseIf x <= y And a < b Then
            'Label41.Text = DateDiff(DateInterval.Year, EMP_DATEbirth.Value, Now)
            Me.txtage1.Text = DateDiff(DateInterval.Year, EMP_DATEbirth.Value, Now)
        ElseIf x <= y And a <= b Then
            'Label41.Text = DateDiff(DateInterval.Year, EMP_DATEbirth.Value, Now) '& '" yr's" & " " & "old"
            Me.txtage1.Text = DateDiff(DateInterval.Year, EMP_DATEbirth.Value, Now)
        ElseIf x <= y And a >= b Then
            'Label41.Text = DateDiff(DateInterval.Year, EMP_DATEbirth.Value, Now) '& '" yr's" & " " & "old"
            Me.txtage1.Text = DateDiff(DateInterval.Year, EMP_DATEbirth.Value, Now)
        End If
    End Sub
#Region "function to compute exact age"
    Public Function GetAge(ByVal Birthdate As System.DateTime, _
    Optional ByVal AsOf As System.DateTime = #1/1/1700#) _
    As String

        'Don't set second parameter if you want Age as of today

        'Demo 1: get age of person born 2/11/1954
        'Dim objDate As New System.DateTime(1954, 2, 11)
        'Debug.WriteLine(GetAge(objDate))

        'Demo 1: get same person's age 10 years from now
        'Dim objDate As New System.DateTime(1954, 2, 11)
        'Dim objdate2 As System.DateTime
        'objdate2 = Now.AddYears(10)
        'Debug.WriteLine(GetAge(objDate, objdate2))

        Dim iMonths As Integer
        Dim iYears As Integer
        Dim dYears As Decimal
        Dim lDayOfBirth As Long
        Dim lAsOf As Long
        Dim iBirthMonth As Integer
        Dim iAsOFMonth As Integer

        If AsOf = "#1/1/1700#" Then
            AsOf = DateTime.Now
        End If
        lDayOfBirth = DatePart(DateInterval.Day, Birthdate)
        lAsOf = DatePart(DateInterval.Day, AsOf)

        iBirthMonth = DatePart(DateInterval.Month, Birthdate)
        iAsOFMonth = DatePart(DateInterval.Month, AsOf)

        iMonths = DateDiff(DateInterval.Month, Birthdate, AsOf)

        dYears = iMonths / 12

        iYears = Math.Floor(dYears)

        If iBirthMonth = iAsOFMonth Then
            If lAsOf < lDayOfBirth Then
                iYears = iYears - 1
            End If
        End If
        Me.txtage1.Text = iYears
        Return iYears

    End Function

#End Region
    Private Sub cboEmp_type_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmp_type.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub
    Private Sub cboEmp_type_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmp_type.SelectedIndexChanged
        Call employee_type_selected_parameters()

    End Sub
#Region "Emplyee type convert from key to description"
    Private Sub employeeType_convert(ByVal typecode As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_type_code_description_select", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@fxkeyemptype", SqlDbType.Char, 3).Value = typecode
        End With
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                Me.cboEmp_type.Text = myreader.Item("Typedescription")
            End While
            myreader.Read()
        Finally

        End Try
        myconnection.sqlconn.Close()
    End Sub
#End Region

#Region "Employee status convert key to description"
    Private Sub employeeStatusdescription()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_status_description_select", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@Statuskey", SqlDbType.Char, 3).Value = Me.cboemp_status.Text
        End With
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                Me.cboemp_status.Text = myreader.Item(0)
            End While
            myreader.Close()
        Finally

        End Try
        myconnection.sqlconn.Close()
    End Sub
#End Region
#Region "Employee Type selected parameters"
    Private Sub employee_type_selected_parameters()
        Dim myconnection As New Clsappconfiguration

        Dim cmd As New SqlCommand("usp_per_employee_type_select_get", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@Typedescription", SqlDbType.VarChar, 50).Value = Me.cboEmp_type.Text

        Dim mydatareader As SqlDataReader = cmd.ExecuteReader()
        Try
            While mydatareader.Read()

                Me.txtType_employee.Text = mydatareader.Item(0)
            End While
        Finally
            mydatareader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Employee Position in Combo"
    Public Sub load_employeeposition_combo()
        Dim myconnection As New Clsappconfiguration

        Dim cmdjol As String = "Select Description from dbo.Per_Employee_Position order by description asc"
        Dim cmd As New SqlCommand(cmdjol, myconnection.sqlconn)
        myconnection.sqlconn.Open()

        Dim mydatareader As SqlDataReader = cmd.ExecuteReader()

        Try
            While mydatareader.Read()

                cbotitledesignation.Items.Add(mydatareader(0))
            End While
        Finally
            mydatareader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Employee Positionlevelcode"
    Public Sub Employee_position_fxkeyposition()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_position_fxkeyposition", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@description", SqlDbType.VarChar, 80).Value = Me.cbotitledesignation.Text
        Dim mydatareader As SqlDataReader = cmd.ExecuteReader()
        Try
            While mydatareader.Read()
                Me.txtfxkeypositionlevel.Text = mydatareader.Item(0)
            End While
        Finally
            mydatareader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Employee Status"
    Public Sub EmployeeStatus()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("CIMS_m_Membership_Status_view", myconnection.sqlconn)
        myconnection.sqlconn.Open()
        Dim mydatareader As SqlDataReader = cmd.ExecuteReader()
        Try
            While mydatareader.Read()
                Me.Cbomem_Status.Items.Add(mydatareader(0))
            End While
        Finally
            mydatareader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Employee Type in Combo"
    Public Sub Employee_Type()
        Dim myconnection As New Clsappconfiguration

        Dim cmdjol As String = "Select typeDescription from dbo.Per_Employee_type order by typedescription asc"
        Dim cmd As New SqlCommand(cmdjol, myconnection.sqlconn)
        myconnection.sqlconn.Open()
        Dim mydatareader As SqlDataReader = cmd.ExecuteReader()
        Try
            While mydatareader.Read()
                cboEmp_type.Items.Add(mydatareader(0))
            End While
        Finally
            mydatareader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region

#Region "Get Employee Nearest Relatives"
    Public Sub ViewNearestRelatives(ByVal empinfo As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetRelatives", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
        With Me.lvlNearestRelatives
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Relative Name", 150, HorizontalAlignment.Left)
            .Columns.Add("Relationship", 100, HorizontalAlignment.Left)
            .Columns.Add("Contact No.", 100, HorizontalAlignment.Left)
            .Columns.Add("Address", 300, HorizontalAlignment.Left)

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .SubItems.Add(myreader.Item(1))
                        .SubItems.Add(myreader.Item(2))
                        .SubItems.Add(myreader.Item(3))
                        .SubItems.Add(myreader.Item(4))
                    End With

                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
#End Region
#Region "Get Member Dependents"
    Public Sub GetmemberDependents(ByVal empinfo As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetDependents", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
        With Me.LvlEmployeeDependent
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Dependent Name", 150, HorizontalAlignment.Left)
            .Columns.Add("Relationship", 100, HorizontalAlignment.Left)
            .Columns.Add("Contact No.", 100, HorizontalAlignment.Left)
            .Columns.Add("Address", 300, HorizontalAlignment.Left)

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .SubItems.Add(myreader.Item(1))
                        .SubItems.Add(myreader.Item(2))
                        .SubItems.Add(myreader.Item(3))
                        .SubItems.Add(myreader.Item(4))
                    End With

                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
#End Region
#Region "Get member bank information"
    Public Sub GetmemberBankInfo(ByVal empinfo As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetBankAccounts", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
        With Me.lvlBankInfo
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Bank Account", 250, HorizontalAlignment.Left)
            .Columns.Add("Bank Name", 300, HorizontalAlignment.Left)
            .Columns.Add("Account Type", 80, HorizontalAlignment.Left)

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .SubItems.Add(myreader.Item(1))
                        .subItems.Add(myreader.Item(2)) 'pk_bankAccounts
                        .subItems.Add(myreader.Item(3))
                    End With

                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
#End Region
#Region "get source of income"
    Public Sub GetsourceofIncomce(ByVal empinfo As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetSourceOfIncome", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
        With Me.lvlSourceIncome
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Source of Income", 250, HorizontalAlignment.Left)
            .Columns.Add("Annual Income", 300, HorizontalAlignment.Left)


            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .SubItems.Add(myreader.Item(1))
                        .subitems.add(myreader.Item(2))

                    End With

                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
#End Region

    'Private Sub txtpermonth_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
    '    If txtpermonth.Text = "" Then
    '        Me.txtpermonth.Text = "0.00"

    '    Else

    '        If Not IsNumeric(txtpermonth.Text) Then
    '            ErrorProvider1.SetError(txtpermonth, "Please write correct number format")
    '            txtpermonth.Focus()
    '            ErrorProvider1.SetError(txtbasicpay, "")
    '        Else
    '            txtpermonth.Text = Format(Decimal.Parse(txtpermonth.Text), "###,###,###.000")

    '        End If

    '    End If
    'End Sub

    Private Sub btnemp_editdepdts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_editdepdts.Click
        Try
            With frmMember_Dependents
                .Label1.Text = "Edit Dependents"
                .btnsave_dep.Visible = False
                .btnupdate.Location = New System.Drawing.Point(5, 5)
                .txtname.Text = Me.LvlEmployeeDependent.SelectedItems(0).Text
                .txtrelation.Text = Me.LvlEmployeeDependent.SelectedItems(0).SubItems(1).Text
                .txtphone.Text = Me.LvlEmployeeDependent.SelectedItems(0).SubItems(2).Text
                .txtaddress.Text = Me.LvlEmployeeDependent.SelectedItems(0).SubItems(3).Text
                .getempdepID() = Me.LvlEmployeeDependent.SelectedItems(0).SubItems(4).Text 'key_id
                .Show()
            End With
        Catch ex As Exception

        End Try
    End Sub

    Public Sub resigned_employee2()
        Dim myreader As SqlDataReader
        Dim ctr As Integer = 0
        Dim counter As Integer = 0
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        'Dim myconnection As New SqlConnection(My.Settings.CNString)
        Dim cmd As New SqlCommand
        cmd.CommandText = "select * from dbo.temp_masterfile where idnumber=" & "'" & txtEmployeeNo.Text & "'"
        myconnection.sqlconn.Open()
        cmd.CommandType = CommandType.Text
        cmd.Connection = myconnection.sqlconn

        myreader = cmd.ExecuteReader()
        With myreader
            If .HasRows Then
                While .Read
                    snumber(ctr) = .Item("idnumber")
                    sfirstname(ctr) = .Item("firstname")
                    slastname(ctr) = .Item("lastname")
                    smiddlename(ctr) = .Item("middlename")
                    semployeetype(ctr) = .Item("employee_type")
                    sGender(ctr) = .Item("gender")
                    sdepartment(ctr) = .Item("department")
                    dhired(ctr) = .Item("date_hired")
                    smobile_phone(ctr) = .Item("mobile_phone")
                    saddress_1(ctr) = .Item("address_1")
                    ctr += 1
                    counter += 1
                End While
            End If
        End With
        myreader.Close()


        cmd.CommandText = "USP_EMPLOYEE_RESIGNED_INSERT"
        cmd.CommandType = CommandType.StoredProcedure

        For ctr = 0 To counter - 1
            With cmd.Parameters
                .Clear()
                .Add("@idnumber", SqlDbType.VarChar, 50).Value = snumber(ctr)
                .Add("@firstname", SqlDbType.VarChar, 50).Value = sfirstname(ctr)
                .Add("@lastname", SqlDbType.VarChar, 50).Value = slastname(ctr)
                .Add("@middlename", SqlDbType.VarChar, 50).Value = smiddlename(ctr)
                .Add("@employee_type", SqlDbType.VarChar, 50).Value = semployeetype(ctr)
                .Add("@Gender", SqlDbType.VarChar, 50).Value = sGender(ctr)
                .Add("@department", SqlDbType.VarChar, 50).Value = sdepartment(ctr)
                .Add("@date_hired", SqlDbType.DateTime, 8).Value = dhired(ctr)
                .Add("@mobile_phone", SqlDbType.VarChar, 50).Value = smobile_phone(ctr)
                .Add("@address_1", SqlDbType.VarChar, 50).Value = saddress_1(ctr)
            End With
            myreader = cmd.ExecuteReader
            myreader.Close()
        Next
        myconnection.sqlconn.Close()
    End Sub

    Private Sub cboregularization_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not cboregularization_Validate(e.KeyChar)
    End Sub
    Public Function cboregularization_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")
            txtitemno.Focus()
            Return False
        Else
            Return True
        End If
    End Function
    Public Sub load_configurationfile()

        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        'Dim myconnection As New SqlConnection(My.Settings.CNString)

        Dim months As String = "select * from dbo.tconfigurationfile"
        Dim cmd As New SqlCommand(months, myconnection.sqlconn)
        cmd.Connection = myconnection.sqlconn
        myconnection.sqlconn.Open()

        Dim myreader As SqlDataReader = cmd.ExecuteReader()
        Try
            While myreader.Read()
                'cboregularization.Items.Add(myreader(1))
            End While
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub

    Private Sub cboemp_status_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboemp_status.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub


#Region "Update idnumber for newly hire employee"
    Public Sub updateid_newhiredemployee()
        Dim count As Integer = 0
        Dim rowcount As Integer = 0
        Dim dr As SqlDataReader
        Dim cm As SqlCommand
        Dim myconnection As New Clsappconfiguration


        Try
            cm = New SqlCommand
            cm.Connection = myconnection.sqlconn
            myconnection.sqlconn.Open()
            cm.CommandText = "USP_IDNUMBER_UPDATE"
            cm.CommandType = CommandType.StoredProcedure

            cm.Parameters.Add("@IDnumber", SqlDbType.VarChar, 50).Value = txtEmployeeNo.Text
            cm.Parameters.Add("@OldIDNumber", SqlDbType.VarChar, 50).Value = "" 'm_sOldIDNumber

            dr = cm.ExecuteReader

            dr.Close()
        Catch ex As Exception

        End Try
    End Sub
#End Region

    Private Sub txtsss_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not txtsss_Validate(e.KeyChar)
    End Sub
    Public Function txtsss_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = "-") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub txtpagibig_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not txtpagibig_Validate(e.KeyChar)
    End Sub

    Public Function txtpagibig_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = "-") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")

            Return False
        Else
            Return True
        End If
    End Function

    Private Sub txthealth_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not txthealth_Validate(e.KeyChar)
    End Sub
    Public Function txthealth_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = "-") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")

            Return False
        Else
            Return True
        End If
    End Function

    Private Sub emp_phone_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtresidencephone.KeyPress
        e.Handled = Not emp_phone_Validate(e.KeyChar)
    End Sub

    Public Function emp_phone_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = "-") And Not (C = "(") And Not (C = ")") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) _
        And Not (Microsoft.VisualBasic.AscW(C) = 22) And Not (Microsoft.VisualBasic.AscW(C) = 3) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")
            txtresidencephone.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub emp_extphone_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles emp_extphone.KeyPress
        e.Handled = Not emp_extphone_Validate(e.KeyChar)
    End Sub
    Public Function emp_extphone_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = "-") And Not (C = "(") And Not (C = ")") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) _
        And Not (Microsoft.VisualBasic.AscW(C) = 22) And Not (Microsoft.VisualBasic.AscW(C) = 3) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")
            emp_extphone.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Public Function emp_mobile_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = "-") And Not (C = "(") And Not (C = ")") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) _
        And Not (Microsoft.VisualBasic.AscW(C) = 22) And Not (Microsoft.VisualBasic.AscW(C) = 3) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")
            txtMobileNo.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub cbopayroll_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cbopayroll.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cbopayroll_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbopayroll.SelectedIndexChanged

    End Sub

    Private Sub cbotax_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cbotax_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub cboshift_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cboshift_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'load_shiftschedule()
    End Sub

    Private Sub cborate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cborate.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cboemp_gender_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboemp_gender.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cboemp_civil_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboemp_civil.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cbodept_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click

        Dim orgchart As New frmOrgChart_Master(orgchart.selection.Org_Emp)
        orgchart.Width = 257
        orgchart.btnClose.Location = New System.Drawing.Point(170, 12)
        orgchart.btnAdd.Enabled = False
        orgchart.btnPrint.Visible = False
        orgchart.btnPrint.Visible = False
        If orgchart.ShowDialog = Windows.Forms.DialogResult.Yes Then
            Me.txtorgchart.Text = orgchart.org_parent_desc
            Me.txtorgchart.Tag = orgchart.orgid
            Me.txtDepartment2.Text = orgchart.department
            Me.txtorgchart.Text = Mid(Me.txtorgchart.Text, 21)
            Call proposeddeptdivision(Me.txtorgchart.Tag)
        End If
    End Sub

    Private Sub cborate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cborate.SelectedIndexChanged


    End Sub
#Region "Rate typecode convert to description"
    Private Sub rateToDescription()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_emloyee_rate_codedecscription_select", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@Rate_code", SqlDbType.Char, 4).Value = Me.cborate.Text
        End With
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                Me.cborate.Text = myreader.Item(0)
            End While
            myreader.Close()
        Finally
        End Try
        myconnection.sqlconn.Close()
    End Sub

#End Region

    Public Sub delete_picture()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        'Dim myconnection As New SqlConnection(My.Settings.CNString)

        Dim cmd As New SqlCommand("USP_PICTURE_DELETE", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@idnumber", SqlDbType.VarChar, 50).Value = txtEmployeeNo.Text

        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub

    Private Sub cbosalarygrade_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cbosalarygrade.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Public Sub load_salarygrade_1()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        'Dim myconnection As New SqlConnection(My.Settings.CNString)

        Dim myreader As SqlDataReader

        Dim mydataset As New DataSet
        Dim cmdjol As String = "Select Salarycode,salary1,salary2 from dbo.Per_Employee_SalaryGrade order by salarycode asc"
        Dim cmd As New SqlCommand(cmdjol, myconnection.sqlconn)
        cmd.CommandType = CommandType.Text
        myconnection.sqlconn.Open()

        myreader = cmd.ExecuteReader()

        Try
            While myreader.Read()

                Me.cbosalarygrade.Items.Add(myreader(0))

            End While
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub

#Region "Increment key employee id/stored proc"
    Public Sub INCREMENT_KEYEMPLOYEEID()
        Dim myconnection As New Clsappconfiguration
        Dim CMD As New SqlCommand("USP_INCREMENT_KEYEMPLOYEEID_NUMBER_SELECT", myconnection.sqlconn)
        CMD.CommandType = CommandType.StoredProcedure
        'CMD.Parameters.Add("@fxkeyemployee", SqlDbType.VarChar, 10).Value = TXTKEYEMPLOYEEID.Text
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = CMD.ExecuteReader
        Try
            While myreader.Read()
                TXTKEYEMPLOYEEID.Text = myreader.Item(0)
            End While
        Finally
            myconnection.sqlconn.Close()
            myreader.Close()
        End Try
    End Sub
#End Region

    Private Sub btnemp_next_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_next.Click

        Call Navigate_Record(Me.txtpk_Employee.Text.Trim, "N")
        Call LoadEloadingRegistrationDetails(txtEmployeeNo.Text)

        'Call compute_age()
        Call GetAge(EMP_DATEbirth.Value)
        'Call computedatetenures()
        Me.lblemployee_name.Text = Me.temp_fname.Text + " " + Me.temp_lname.Text + " " + Me.temp_midname.Text

    End Sub

    Private Sub btnemp_previous_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_previous.Click

        Call Navigate_Record(Me.txtpk_Employee.Text.Trim, "P")
        Call LoadEloadingRegistrationDetails(txtEmployeeNo.Text)

        'Call computedatetenures()
        'Call compute_age()
        Call GetAge(EMP_DATEbirth.Value)
        Me.lblemployee_name.Text = Me.temp_fname.Text + " " + Me.temp_lname.Text + " " + Me.temp_midname.Text

    End Sub

#Region "Get fxkeyemployee"
    Private Sub getfexkeyemployee()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_masterfile_selectkey_get", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@IDnumber", SqlDbType.VarChar, 50).Value = Me.txtEmployeeNo.Text
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                Me.TXTKEYEMPLOYEEID.Text = myreader.Item(0)
            End While
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
    Public Sub load_masterfile_in_txtchangedID()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        Dim x As Integer = 0
        'Dim myconnection As New SqlConnection(My.Settings.CNString)

        Dim myreader As SqlDataReader
        Dim cmd As New SqlCommand
        cmd.CommandText = "USP_NAVIGATE_EMPLOYEE_RECORDS_SELECT"
        myconnection.sqlconn.Open()
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = myconnection.sqlconn

        cmd.Parameters.Add("@FXKEYEMPLOYEE", SqlDbType.VarChar, 10).Value = Me.TXTRECID.Text

        myreader = cmd.ExecuteReader
        Try
            While myreader.Read()

                'Me.txtreplicateIDnumber.Text = myreader.Item(0)
                'txtEmpID.Text = myreader.Item(0)
                temp_fname.Text = myreader.Item(1)
                temp_lname.Text = myreader.Item(2)
                temp_midname.Text = myreader.Item(3)
                cboEmp_type.Text = myreader.Item(4)
                cboemp_gender.Text = myreader.Item(5)
                cboemp_civil.Text = myreader.Item(6)
                cboemp_status.Text = myreader.Item(7)
                Me.txtorgchart.Text = myreader.Item(8)
                emp_Datehired.Text = myreader.Item(9)
                txtresidencephone.Text = myreader.Item(10) '10 COLUMNS
                emp_extphone.Text = myreader.Item(11)
                txtMobileNo.Text = myreader.Item(12)
                txtEmailAddress.Text = myreader.Item(13)
                Me.cbotitledesignation.Text = myreader.Item(14)
                txtprovincialadd.Text = myreader.Item(15)
                EMP_DATEbirth.Text = myreader.Item(16)
                temp_placebirth.Text = myreader.Item(17)
                txtofficeadd.Text = myreader.Item(18)
                temp_citizen.Text = myreader.Item(19)
                '20 COLUMNS
                temp_height.Text = myreader.Item(20)
                txtofficenumber.Text = myreader.Item(21)
                txtlocalofficenumber.Text = myreader.Item(22)
                temp_add3.Text = myreader.Item(23)
                temp_marks.Text = myreader.Item(24)

                '30 COLUMNS

                cborate.Text = myreader.Item(31)
                txtbasicpay.Text = myreader.Item(32)


                Me.txtmonthreqular.Text = myreader.Item(42)
                txtweight.Text = myreader.Item(43)
                txtremarks.Text = myreader.Item(45) '50 COLUMNS

                cbosalarygrade.Text = myreader.Item(47)

                Me.txtage1.Text = myreader.Item(50)
                Me.TXTKEYEMPLOYEEID.Text = myreader.Item(51)
                TXTRECID.Text = myreader.Item(51) 'fxkeyemployee
                Me.txtorgchart.Tag = myreader.Item(52)


                Me.txtResidenceadd.Text = myreader.Item(55)
                Me.txtDepartment2.Text = myreader.Item(56)

                'Me.TxtSeconded.Text = myreader.Item(58)

                Me.txtMemberID.Text = myreader.Item(62)


                'ADDITIONAL 

                'Me.Cbomem_Bereavement.Text = myreader.Item(71)
                'Me.mem_amountpaid.Text = myreader.Item(72)
                Me.txtpayrollcontriamount.Text = myreader.Item(73)
                Me.mem_PaycontDate.Text = myreader.Item(74)
                Me.mem_WithdrawDate.Text = myreader.Item(75)
                Me.mem_MemberDate.Text = myreader.Item(76)
                Me.Cbomem_Status.Text = myreader.Item(77)
                Me.emp_company.Text = myreader.Item(78)


            End While
            'If Me.txtpicture.Text = "" Then
            '    emp_pic.ImageLocation = Application.StartupPath & "\SplashScreen v3.jpg"
            '    emp_pic.Load()
            'End If

            Call getorgchartvalue(Me.txtorgchart.Tag)
            'Call getorgchartvalue(Me.TxtSeconded.Tag)
            Call getorgchartIDtodepartment2(Me.txtorgchart.Tag)
            Call getorgcode(Me.txtorgchart.Tag)
            Call getorgparentid(Me.txtorgchart.Tag)
            Call proposeddeptdivision(Me.txtorgchart.Tag)
            'oldidnumber = txtEmpID.Text

        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#Region "Toggle Resigned"
    Public Sub ToggleNavigateResigned()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        'Dim myconnection As New SqlConnection(My.Settings.CNString)

        Dim myreader As SqlDataReader
        Dim cmd As New SqlCommand
        cmd.CommandText = "USP_NAVIGATE_EMPLOYEE_RECORDS_SELECT_Resigned"
        myconnection.sqlconn.Open()
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = myconnection.sqlconn

        cmd.Parameters.Add("@FXKEYEMPLOYEE", SqlDbType.VarChar, 10).Value = Me.TXTRECID.Text

        myreader = cmd.ExecuteReader
        Try
            While myreader.Read()

                'Me.txtreplicateIDnumber.Text = myreader.Item(0)
                txtEmployeeNo.Text = myreader.Item(0)
                temp_fname.Text = myreader.Item(1)
                temp_lname.Text = myreader.Item(2)
                temp_midname.Text = myreader.Item(3)
                cboEmp_type.Text = myreader.Item(4)
                cboemp_gender.Text = myreader.Item(5)
                cboemp_civil.Text = myreader.Item(6)
                cboemp_status.Text = myreader.Item(7)
                Me.txtDepartment2.Text = myreader.Item(8)
                emp_Datehired.Text = myreader.Item(9) '10 COLUMNS
                txtresidencephone.Text = myreader.Item(10)
                emp_extphone.Text = myreader.Item(11)
                txtMobileNo.Text = myreader.Item(12)
                txtEmailAddress.Text = myreader.Item(13)
                Me.cbotitledesignation.Text = myreader.Item(14)
                txtprovincialadd.Text = myreader.Item(15)
                EMP_DATEbirth.Text = myreader.Item(16)
                temp_placebirth.Text = myreader.Item(17)
                txtofficeadd.Text = myreader.Item(18)
                temp_citizen.Text = myreader.Item(19)
                '20 COLUMNS
                temp_height.Text = myreader.Item(20)
                txtofficenumber.Text = myreader.Item(21)
                txtlocalofficenumber.Text = myreader.Item(22)
                temp_add3.Text = myreader.Item(23)
                temp_marks.Text = myreader.Item(24)

                '30 COLUMNS

                cborate.Text = myreader.Item(31)
                txtbasicpay.Text = myreader.Item(32)


                cbopayroll.Text = myreader.Item(35)


                'txtlocation1.Text = myreader.Item(40)

                Me.txtmonthreqular.Text = myreader.Item(42)
                txtweight.Text = myreader.Item(43)
                txtremarks.Text = myreader.Item(45) '50 COLUMNS

                cbosalarygrade.Text = myreader.Item(47)

                'Label39.Text = myreader.Item(49)tenure
                ' tenure
                Me.txtage1.Text = myreader.Item(50)

                Me.TXTKEYEMPLOYEEID.Text = myreader.Item(51)
                TXTRECID.Text = myreader.Item(51) 'fxkeyemployee

                'Me.txtorgchart.Tag = myreader.Item(52)

                'Me.lblmonth.Text = myreader.Item(53)'tenure
                Me.txtResidenceadd.Text = myreader.Item(53)

                Me.txtDepartment2.Text = myreader.Item(55)


            End While
            'If Me.txtpicture.Text = "" Then
            '    emp_pic.ImageLocation = Application.StartupPath & "\SplashScreen v3.jpg"
            '    emp_pic.Load()
            'End If

            Call getorgchartvalue(Me.txtorgchart.Tag)
            'Call getorgchartvalue(Me.TxtSeconded.Tag)
            Call getorgchartIDtodepartment2(Me.txtorgchart.Tag)
            Call getorgcode(Me.txtorgchart.Tag)
            Call getorgparentid(Me.txtorgchart.Tag)
            Call proposeddeptdivision(Me.txtorgchart.Tag)
            'oldidnumber = tempid_no.Text
            If myreader.Item("Resigned") = "1" Then
                'Me.btnresigned.Enabled = False
            End If
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
    Private Sub Load_RecordId_Masterfile()
        'Dim x As String
        Dim myconnection As New Clsappconfiguration
        Dim myreader As SqlDataReader

        Dim cmd As New SqlCommand("usp_per_employee_keyemployee", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        'cmd.Parameters.Add("@key", SqlDbType.VarChar, 10).Value = Me.TXTRECID.Text
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                Me.TXTRECID.Text = myreader.Item(0)
                'Call LOAD_PICTURE_FROM_DBASE(tempid_no.Text)
            End While
            'Me.TXTRECID.Text = x
            myreader.Close()
            myconnection.sqlconn.Close()
        Catch ex As Exception

        End Try

    End Sub

#Region "Employee payroll delete"
    Public Sub Employee_payroll_tk_delete()
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("spu_EMPLOYEEFile_MARKDelete", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@chrKeyEmployee", SqlDbType.Char, 6).Value = Me.TXTKEYEMPLOYEEID.Text
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Check Stored Delete", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
    Private Sub TXTRECID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TXTRECID.TextChanged
        getfxkeyemployee = Me.TXTRECID.Text

        If TXTRECID.Text = TXTRECID.Text Then
            Call load_masterfile_in_txtchangedID()
        End If

    End Sub

    Private Sub txtitemno_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtitemno.KeyPress
        e.Handled = Not txtitemno_Validate(e.KeyChar)
    End Sub

    Public Function txtitemno_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")
            txtitemno.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub Dateregular_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Dateregular.ValueChanged
        txtdateregular.Text = Dateregular.Value
    End Sub

    Private Sub cboemp_status_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboemp_status.TextChanged
        Call employeeStatusdescription()

    End Sub

    Public Sub RESIGNED_EMPLOYEE1()
        Try
            Dim myconnection As New Clsappconfiguration
            'Dim cmd As New SqlCommand("USP_UPDATE_EMPLOYEE_RESIGNED", myconnection.sqlconn)
            Dim cmd As New SqlCommand("USP_UPDATE_EMPLOYEE_RESIGNED_PERSTK", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@dateResigned", SqlDbType.DateTime).Value = Format(Now, "MM/dd/yyyy")
            cmd.Parameters.Add("@fxkeyemployee", SqlDbType.VarChar, 10).Value = RTrim(Me.TXTKEYEMPLOYEEID.Text)
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Resigned", MessageBoxButtons.OK)
        End Try
    End Sub

#Region "Transfer Deleted Employee"
    Private Sub TransferDeletedEmployee(ByVal idnumber As String, ByVal firstname As String, ByVal lastname As String, _
                                        ByVal middlename As String, ByVal emptype As String, ByVal gender As String, _
                                        ByVal civilstatus As String, ByVal empstatus As String, ByVal department As String, _
                                        ByVal datehired As String, ByVal position As String, ByVal SG As String, ByVal datebirth As String, _
                                        ByVal placebirth As String, ByVal religion As String, ByVal citizenship As String, _
                                        ByVal height As String, ByVal add1 As String, ByVal add2 As String, ByVal add3 As String, _
                                        ByVal atm As String, ByVal gsis As String, ByVal pagibig As String, ByVal tin As String, _
                                        ByVal philhealth As String, ByVal nbi As String, ByVal sss As String, ByVal rate As String, _
                                        ByVal basicpay As String, ByVal permonth As String, ByVal ecola As String, ByVal payschedule As String, _
                                        ByVal tax As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_employee_transferDeleted_insert", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("@idnumber", SqlDbType.VarChar, 50).Value = idnumber
            cmd.Parameters.Add("@fxkeyemployee", SqlDbType.VarChar, 9).Value = ""
            cmd.Parameters.Add("@Firstname", SqlDbType.VarChar, 50).Value = firstname
            cmd.Parameters.Add("@Lastname", SqlDbType.VarChar, 50).Value = lastname
            cmd.Parameters.Add("@Middlename", SqlDbType.VarChar, 50).Value = middlename
            cmd.Parameters.Add("@Employee_Type", SqlDbType.VarChar, 50).Value = emptype
            cmd.Parameters.Add("@Gender", SqlDbType.VarChar, 50).Value = gender
            cmd.Parameters.Add("@Civil_status", SqlDbType.VarChar, 50).Value = civilstatus
            cmd.Parameters.Add("@Status", SqlDbType.VarChar, 50).Value = empstatus
            cmd.Parameters.Add("@Department", SqlDbType.VarChar, 50).Value = department
            cmd.Parameters.Add("@Date_Hired", SqlDbType.DateTime).Value = datehired
            cmd.Parameters.Add("@Positions", SqlDbType.VarChar, 50).Value = position
            cmd.Parameters.Add("@Salary_Grade", SqlDbType.VarChar, 50).Value = SG
            cmd.Parameters.Add("@Date_of_Birth", SqlDbType.DateTime).Value = datebirth
            cmd.Parameters.Add("@Place_of_Birth", SqlDbType.VarChar, 255).Value = placebirth
            cmd.Parameters.Add("@Religion", SqlDbType.VarChar, 50).Value = religion
            cmd.Parameters.Add("@Citizenship", SqlDbType.VarChar, 50).Value = citizenship
            cmd.Parameters.Add("@Height", SqlDbType.VarChar, 50).Value = height
            cmd.Parameters.Add("@Address_1", SqlDbType.VarChar, 255).Value = add1
            cmd.Parameters.Add("@Address_2", SqlDbType.VarChar, 255).Value = add2
            cmd.Parameters.Add("@Address_3", SqlDbType.VarChar, 255).Value = add3
            cmd.Parameters.Add("@ATM_No", SqlDbType.VarChar, 50).Value = atm
            cmd.Parameters.Add("@GSIS_No", SqlDbType.VarChar, 50).Value = gsis
            cmd.Parameters.Add("@Pag_ibig_No", SqlDbType.VarChar, 50).Value = pagibig
            cmd.Parameters.Add("@TIN_No", SqlDbType.VarChar, 50).Value = tin
            cmd.Parameters.Add("@Phil_Health_No", SqlDbType.VarChar, 50).Value = philhealth
            cmd.Parameters.Add("@NBI_No", SqlDbType.VarChar, 50).Value = nbi
            cmd.Parameters.Add("@SSS", SqlDbType.VarChar, 50).Value = sss
            cmd.Parameters.Add("@Rate", SqlDbType.VarChar, 50).Value = rate
            cmd.Parameters.Add("@Basic_Pay", SqlDbType.VarChar, 50).Value = basicpay
            cmd.Parameters.Add("@Per_Month", SqlDbType.VarChar, 50).Value = permonth
            cmd.Parameters.Add("@ECOLA", SqlDbType.VarChar, 50).Value = ecola
            cmd.Parameters.Add("@Payroll", SqlDbType.VarChar, 50).Value = payschedule
            cmd.Parameters.Add("@Tax", SqlDbType.VarChar, 50).Value = tax
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show("Check your SP for Delete", "Clicksoftware", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
#End Region
#Region "Employee Personnel Resigned/stored proc"
    Public Sub Resigned_employee()

        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_masterfile_resigned_update", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            cmd.Parameters.Add("@recid", SqlDbType.Char, 30).Value = Me.TXTRECID.Text
            cmd.Parameters.Add("@Resigned", SqlDbType.Bit).Value = 1
            'Label46.Text = "Resigned"
            cmd.Parameters.Add("@date_resigned", SqlDbType.DateTime).Value = Date.Now 'Microsoft.VisualBasic.FormatDateTime(Dateresigned.Value, DateFormat.ShortDate) 'form6.Label48.Text

            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show("There is no employee record's to be resigned", "eHRMS", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub
#End Region
#Region "Employee Personne in Payroll $ TK Resigned"
    Public Sub Employee_resigned_payroll_tk()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_payroll_employee_masterfile_resigned_update", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            cmd.Parameters.Add("@fxKeyEmployee", SqlDbType.Char, 6, ParameterDirection.Input).Value = TXTKEYEMPLOYEEID.Text
            cmd.Parameters.Add("@fbActive", SqlDbType.Bit).Value = 0
            cmd.Parameters.Add("@fdDateSeparated", SqlDbType.DateTime).Value = Date.Now

            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Resigned..")
        End Try
    End Sub
#End Region

    Private Sub temp_fname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles temp_fname.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub temp_lname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles temp_lname.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub temp_midname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles temp_midname.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub tnotes_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtprovincialadd.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub temp_placebirth_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles temp_placebirth.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub temp_religion_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtofficeadd.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub temp_add1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtofficenumber.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub empadd2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtlocalofficenumber.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub temp_add3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles temp_add3.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub temp_citizen_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles temp_citizen.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub temp_marks_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles temp_marks.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub temp_fname_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles temp_fname.Validating
        If temp_fname.Text = "" Then
            MessageBox.Show("Empty field First Name is required", "System Message", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            temp_fname.Focus()
        Else
        End If
    End Sub

    Private Sub temp_lname_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles temp_lname.Validating
        If temp_lname.Text = "" Then
            MessageBox.Show("Empty field Last Name is required", "System Message", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            temp_lname.Focus()
        Else
        End If
    End Sub

    Private Sub temp_midname_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles temp_midname.Validating
        If temp_midname.Text = "" Then
            MessageBox.Show("Empty field Middle Name is required", "System Message", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            temp_midname.Focus()
        Else
        End If
    End Sub

    Private Sub temp_fname_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles temp_fname.TextChanged
        'Me.txtfullname.Text = temp_fname.Text + " " + Me.temp_lname.Text + " " + Me.temp_midname.Text
        Me.txtfullname.Text = Me.temp_lname.Text + "," + temp_fname.Text + " " + Me.temp_midname.Text
        Me.lblemployee_name.Text = Me.temp_fname.Text + " " + Me.temp_lname.Text + " " + Me.temp_midname.Text
    End Sub

    Private Sub temp_lname_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles temp_lname.TextChanged
        'Me.txtfullname.Text = temp_fname.Text + " " + Me.temp_lname.Text + " " + Me.temp_midname.Text
        Me.txtfullname.Text = Me.temp_lname.Text + "," + temp_fname.Text + " " + Me.temp_midname.Text
        Me.lblemployee_name.Text = Me.temp_fname.Text + " " + Me.temp_lname.Text + " " + Me.temp_midname.Text
    End Sub

    Private Sub temp_midname_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles temp_midname.TextChanged
        'Me.txtfullname.Text = temp_fname.Text + " " + Me.temp_lname.Text + " " + Me.temp_midname.Text
        Me.txtfullname.Text = Me.temp_lname.Text + "," + temp_fname.Text + " " + Me.temp_midname.Text
        Me.lblemployee_name.Text = Me.temp_fname.Text + " " + Me.temp_lname.Text + " " + Me.temp_midname.Text
    End Sub

    Private Sub Label41_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Call compute_age()
    End Sub

    Private Sub cbotitledesignation_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cbotitledesignation.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cbotitledesignation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbotitledesignation.SelectedIndexChanged
        Call Employee_position_fxkeyposition()
    End Sub

    Private Sub txtmonthreqular_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtmonthreqular.KeyPress
        e.Handled = Not txtcode_Validate(e.KeyChar)
    End Sub

    Public Function txtcode_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")
            'txtcode.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub cbohours_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not numeric_Validate(e.KeyChar)
    End Sub

    'Private Sub LvlPerformanceEvaluation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Me.tlocation.Text = Me.LvlPerformanceEvaluation.SelectedItems(0).SubItems(4).Text
    '    Catch ex As Exception
    '    End Try
    'End Sub

    'Private Sub LvlDisciplineCase_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Me.txtDiscFileShow.Text = Me.LvlDisciplineCase.SelectedItems(0).SubItems(8).Text
    '    Catch ex As Exception
    '    End Try
    'End Sub

    'Private Sub RdAllTraining_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Me.RdAllTraining.Checked = True Then
    '        Me.RdCurrentTraining.Checked = False
    '        Me.RdPreviousTraining.Checked = False
    '        Call load_employee_training_with_selection()
    '    End If
    'End Sub
    'Private Sub RdCurrentTraining_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Me.RdCurrentTraining.Checked = True Then
    '        Me.RdPreviousTraining.Checked = False
    '        Me.RdAllTraining.Checked = False
    '        Call load_employee_training_with_selection1()
    '    End If
    ''End Sub
    'Private Sub RdPreviousTraining_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Me.RdPreviousTraining.Checked = True Then
    '        Me.RdAllTraining.Checked = False
    '        Me.RdCurrentTraining.Checked = False
    '        Call load_employee_training_with_selection2()
    '    End If
    'End Sub

    'Private Sub lvlTraining_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Me.txtFileShow.Text = Me.lvlTraining.SelectedItems(0).SubItems(7).Text
    '    Catch ex As Exception
    '    End Try
    'End Sub



    'Private Sub BtnDel_jobdesc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        frmEmployee_JobDescription.getempJobDescID() = Me.LvlJobDescription.SelectedItems(0).SubItems(4).Text
    '        Dim x As New DialogResult
    '        x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
    '        If x = System.Windows.Forms.DialogResult.OK Then
    '            Call Job_Description_Delele()
    '            Call frmEmployee_JobDescription.Job_Description_Get()
    '        ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
    '        End If
    '    Catch ex As Exception
    '    End Try
    'End Sub
#Region "Employee Job Description Delete/stored proc"
    Private Sub Job_Description_Delele()
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_employee_jobdescription_delete", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@key_id", SqlDbType.VarChar, 10).Value = frmEmployee_JobDescription.getjobdesc_id
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
        End Try
    End Sub
#End Region

    'Private Sub BtnEdit_jobDesc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        With frmEmployee_JobDescription
    '            .Label1.Text = "Edit Job Description"
    '            .txtReportingto.Text = Me.LvlJobDescription.SelectedItems(0).Text
    '            .Show()
    '            .btnsave.Visible = False
    '            .btnupdate.Visible = True
    '            .btnupdate.Location = New System.Drawing.Point(5, 4)
    '            .txtLocation.Text = Me.LvlJobDescription.SelectedItems(0).SubItems(1).Text
    '            .txtJobDescription.Text = Me.LvlJobDescription.SelectedItems(0).SubItems(2).Text
    '            .getempJobDescID() = Me.LvlJobDescription.SelectedItems(0).SubItems(4).Text
    '        End With
    '    Catch ex As Exception
    '    End Try
    'End Sub

    Private Sub BtnAdd_Skills_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmEmployee_Skills.Label1.Text = "Add Skills"
        frmEmployee_Skills.btnupdate.Visible = False
        frmEmployee_Skills.Show()
    End Sub
    'Private Sub BtnEdit_Skills_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        With frmEmployee_Skills
    '            .Label1.Text = "Edit Skills"
    '            .txtSkills.Text = Me.LvlSkills.SelectedItems(0).Text
    '            .Show()
    '            .btnsave.Visible = False
    '            .btnupdate.Visible = True
    '            .btnupdate.Location = New System.Drawing.Point(5, 5)
    '            .txtRemark.Text = Me.LvlSkills.SelectedItems(0).SubItems(1).Text
    '            .getempSkillsID = Me.LvlSkills.SelectedItems(0).SubItems(3).Text 'key_id
    '        End With
    '    Catch ex As Exception
    '    End Try
    'End Sub

    'Private Sub BtnDel_Skills_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        frmEmployee_Skills.getempSkillsID = Me.LvlSkills.SelectedItems(0).SubItems(3).Text
    '        Dim x As New DialogResult
    '        x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
    '        If x = System.Windows.Forms.DialogResult.OK Then
    '            Call Employee_Skills_Delete()
    '            Call frmEmployee_Skills.Employee_Skills_Get()
    '        ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
    '        End If
    '    Catch ex As Exception
    '    End Try
    'End Sub
#Region "Employee Skills Delete/stored proc"
    Private Sub Employee_Skills_Delete()
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_employee_skills_delete", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@key_id", SqlDbType.VarChar, 10).Value = frmEmployee_Skills.getskills_id
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
        End Try
    End Sub
#End Region

    Private Sub BtnAdd_Awards_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmEmployee_Awards.Label1.Text = "Add Awards"
        frmEmployee_Awards.btnupdate.Visible = False
        frmEmployee_Awards.Show()
    End Sub

#Region "Employee Awards Delete/stored proc"
    Private Sub Employee_award_delete()
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_employee_award_delete", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@key_id", SqlDbType.VarChar, 10).Value = frmEmployee_Awards.getaward_ID
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
        End Try
    End Sub
#End Region

    Private Sub BtnAdd_Medical_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmEmployee_Medical.Label1.Text = "Add Medical"
        frmEmployee_Medical.btnupdate.Visible = False
        frmEmployee_Medical.Show()
    End Sub

#Region "Employee Medical Delete/stored proc"
    Private Sub Employee_medical_delete()
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_employee_medical_delete", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@key_id", SqlDbType.VarChar, 10).Value = frmEmployee_Medical.getmedical_ID
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
        End Try
    End Sub
#End Region
#Region "Payroll Loan History get/stored proc"
    Public Sub Payroll_loan_history_get()
        'Try
        '    Dim myconnection As New Clsappconfiguration
        '    Dim cmd As New SqlCommand("usp_payroll_loan_history_get", myconnection.sqlconn)
        '    cmd.CommandType = CommandType.StoredProcedure
        '    myconnection.sqlconn.Open()
        '    cmd.Parameters.Add("@fxKeyEmployee", SqlDbType.VarChar, 7).Value = Me.TXTKEYEMPLOYEEID.Text
        '    With Me.LvlLoanHistory
        '        .Clear()
        '        .View = View.Details
        '        .GridLines = True
        '        .FullRowSelect = True
        '        .Columns.Add("Loan Reference", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Date of Loan", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Starting Loan", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Ending Loan", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Approved By", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Remarks", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Payment", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Loan Balance", 100, HorizontalAlignment.Left)
        '        Dim myreader As SqlDataReader
        '        myreader = cmd.ExecuteReader
        '        Try
        '            While myreader.Read
        '                With .Items.Add(myreader.Item(0))
        '                    .Subitems.add(myreader.Item(1))
        '                    .Subitems.add(myreader.Item(2))
        '                    .Subitems.add(myreader.Item(3))
        '                    .Subitems.add(myreader.Item(4))
        '                    .Subitems.add(myreader.Item(5))
        '                    .Subitems.add(myreader.Item(6))
        '                    .Subitems.add(myreader.Item(7))
        '                    .Subitems.add(myreader.Item(8))
        '                    .Subitems.add(myreader.Item(9))
        '                End With
        '            End While

        '        Finally
        '            myreader.Close()
        '            myconnection.sqlconn.Close()
        '        End Try
        '    End With
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, "Payroll loan history", MessageBoxButtons.OK)
        'End Try
    End Sub
#End Region
#Region "Payroll History get/stored proc"
    Public Sub Payroll_history_get()
        'Try
        '    Dim myconnection As New Clsappconfiguration
        '    Dim cmd As New SqlCommand("spu_EMPLOYEEFile_PayrollHistory", myconnection.sqlconn)
        '    cmd.CommandType = CommandType.StoredProcedure
        '    myconnection.sqlconn.Open()
        '    cmd.Parameters.Add("@fxKeyEmployee", SqlDbType.VarChar, 7).Value = Me.TXTKEYEMPLOYEEID.Text
        '    With Me.LvlPayrollHistory
        '        .Clear()
        '        .View = View.Details
        '        .GridLines = True
        '        .FullRowSelect = True
        '        .Columns.Add("Cut Off Date", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Basic Pay", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Gross Pay", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Deduction", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Net Pay", 100, HorizontalAlignment.Left)
        '        Dim myreader As SqlDataReader
        '        myreader = cmd.ExecuteReader
        '        Try
        '            While myreader.Read
        '                With .Items.Add(myreader.Item(0))
        '                    .Subitems.add(myreader.Item(1))
        '                    .Subitems.add(myreader.Item(2))
        '                    .Subitems.add(myreader.Item(3))
        '                    .Subitems.add(myreader.Item(4))
        '                    .Subitems.add(myreader.Item(5))
        '                End With
        '            End While

        '        Finally
        '            myreader.Close()
        '            myconnection.sqlconn.Close()
        '        End Try
        '    End With
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, "Payroll history", MessageBoxButtons.OK)
        'End Try
    End Sub
#End Region
    Private Sub TXTKEYEMPLOYEEID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TXTKEYEMPLOYEEID.TextChanged
        If Me.TXTKEYEMPLOYEEID.Text = Me.TXTKEYEMPLOYEEID.Text Then
            'Call Timekeeping_leaves_ledger_get()
            Call Payroll_loan_history_get()
            Call Payroll_history_get()
        End If
    End Sub

    Private Sub PcSeconded_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim orgchart As New frmOrgChart_Master(orgchart.selection.Org_Emp)
        orgchart.Width = 257
        orgchart.btnClose.Location = New System.Drawing.Point(170, 12)
        orgchart.btnAdd.Enabled = False
        orgchart.btnPrint.Visible = False
        orgchart.btnPrint.Visible = False
    End Sub


    Private Sub txtpagibigcontri_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not CurrencyInput_Validate(e.KeyChar)
    End Sub

#Region "Get Orgchart from the root to node"
    Public Sub getorgchartvalue(ByVal m As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_orgchart_get", myconnection.sqlconn)
        With cmd.Parameters
            .Add("@getid", SqlDbType.VarChar, 10).Value = m
        End With
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Dim x As String
        Try
            While myreader.Read
                Me.txtorgchart.Text = myreader.Item(0)
                x = myreader.Item(0)
            End While
            myreader.Close()
            'Me.txtdescdivision.Text = Mid(x, 2, 32)
            'Me.txtdescdepartment.Text = Mid(x, 35, 24)
            'Me.txtdescsection.Text = Mid(x, 60, 5)
        Finally
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "GetdepartmentID"
    Public Sub getorgchartIDtodepartment2(ByVal orgid As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_department2_select", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@department", SqlDbType.VarChar, 20).Value = orgid
        End With
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            Me.txtDepartment2.Text = ""
            While myreader.Read
                Me.txtDepartment2.Text = myreader.Item(0)
            End While
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Get orgcode"
    Public Sub getorgcode(ByVal m As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_orgchart_get_code", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@getid", SqlDbType.VarChar, 10).Value = m
        End With
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader

        Dim x1 As String

        Dim split As New Collection
        Try
            While myreader.Read
                x1 = myreader.Item(0)

            End While
            myreader.Close()
            Me.txtkeycompany.Text = Mid(x1, 2, 2)
            Me.txtkeydivision.Text = Mid(x1, 5, 2)
            Me.txtkeydepartment.Text = Mid(x1, 8, 2)
            Me.txtkeysection.Text = Mid(x1, 11, 2)
        Finally
            'myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Get parent id from orgchart"
    Public Sub getorgparentid(ByVal m As String)

        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_orgchart_get_parentid", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@getid", SqlDbType.VarChar, 10).Value = m
        End With
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader

        Dim x1 As String

        Dim split As New Collection
        Try
            While myreader.Read
                x1 = myreader.Item(0)
            End While
            myreader.Close()

            Me.txtdescdivision.Text = Mid(x1, 5, 2)

        Finally
            'myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Proposed dept/division"
    Public Sub proposeddeptdivision(ByVal m As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_orgchart_getid", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@getid", SqlDbType.VarChar, 10).Value = m
        End With
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Dim x As String

        Try
            While myreader.Read
                x = myreader.Item(0)
            End While
            myreader.Close()

            Dim arr As String() = x.Split("\".ToCharArray())
            For i As Integer = 0 To (arr.Length - 1)
                'Me.txtkeycompany.Text = arr(2)
                'Me.txtdescdivision.Text = arr(3)
                'Me.txtdescdepartment.Text = arr(4)
                'Me.txtdescsection.Text = arr(5)
            Next

        Finally
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "SpacedelimeterFunction"
    Public Function SplitDelimitedLine(ByVal CurrentLine As String, ByVal Delimiter As String, ByVal Qualifier As String) As Collection

        Dim i As Integer
        Dim SplitString As New Collection
        Dim CountDelimiter As Boolean
        Dim Total As Integer
        Dim Ch As Char
        Dim Section As String

        ' We want to count the delimiter unless it is within the text qualifier
        CountDelimiter = True
        Total = 0
        Section = ""

        For i = 1 To Len(CurrentLine)

            Ch = Mid(CurrentLine, i, 1)
            Select Case Ch

                Case Qualifier
                    If CountDelimiter Then
                        CountDelimiter = False
                    Else
                        CountDelimiter = True
                    End If

                Case Delimiter
                    If CountDelimiter Then

                        ' Add current section to collection
                        SplitString.Add(Section)

                        Section = ""
                        Total = Total + 1

                    End If

                Case Else

                    Section = Section & Ch

            End Select


        Next

        ' Get the last field - as most files will not have an ending delimiter
        If CountDelimiter Then

            ' Add current section to collection
            SplitString.Add(Section)

        End If


        SplitDelimitedLine = SplitString


    End Function


#End Region

#Region "Update Education for Change IDnumber"
    Public Sub updateeducationEmployee(ByVal idnumber As String)

        Dim count As Integer = 0
        Dim counter As Integer = 0
        Dim myconnection As New Clsappconfiguration
        Dim myreader As SqlDataReader
        Dim cmd1 As New SqlCommand
        cmd1.CommandText = "select * from Per_Employee_Education where idnumber=" & "'" & idnumber & "'"
        cmd1.CommandType = CommandType.Text
        cmd1.Connection = myconnection.sqlconn
        myconnection.sqlconn.Open()

        myreader = cmd1.ExecuteReader
        With myreader
            If .HasRows Then
                While .Read
                    oldid(count) = .Item("idnumber")
                    count += 1
                    counter += 1
                End While
            End If
        End With
        myreader.Close()

        cmd1.Connection = myconnection.sqlconn
        cmd1.CommandText = "usp_per_education_new_id_update"
        cmd1.CommandType = CommandType.StoredProcedure

        For count = 0 To counter - 1
            With cmd1.Parameters
                .Clear()
                .Add("@idnumber", SqlDbType.VarChar, 50).Value = oldid(count)

            End With
            myreader = cmd1.ExecuteReader
            myreader.Close()
        Next
        myconnection.sqlconn.Close()

        'Dim cmd As New SqlCommand("usp_per_education_new_id_update", myconnection.sqlconn)
        'cmd.CommandType = CommandType.StoredProcedure
        'myconnection.sqlconn.Open()
        'cmd.Parameters.Add("@idnumber", SqlDbType.VarChar, 50).Value = idnumber



        'Try
        '    While myreader.Read
        '        idnumber = myreader.Item(0)
        '    End While
        '    myreader.Close()
        'Finally
        '    myconnection.sqlconn.Close()
        'End Try




    End Sub
#End Region

    Private Sub cborate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cborate.TextChanged
        Call rateToDescription()
    End Sub

    Private Sub txtorgchart_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtorgchart.TextChanged

    End Sub

    Private Sub btnaddContact_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnaddContact.Click
        frmMember_Relatives.Label1.Text = "Add New Relatives"
        frmMember_Relatives.Show()
        frmMember_Relatives.btnupdate.Visible = False
    End Sub

    Private Sub BtnEditContact_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnEditContact.Click
        Try
            With frmMember_Relatives
                .Label1.Text = "Edit Nearest Relatives"
                .btnsave.Visible = False
                .btnupdate.Visible = True
                .btnupdate.Location = New System.Drawing.Point(5, 6)
                .txtname.Text = Me.lvlNearestRelatives.SelectedItems(0).Text
                .txtrelationship.Text = Me.lvlNearestRelatives.SelectedItems(0).SubItems(1).Text
                .txtcontact.Text = Me.lvlNearestRelatives.SelectedItems(0).SubItems(2).Text
                .txtaddress.Text = Me.lvlNearestRelatives.SelectedItems(0).SubItems(3).Text
                .getRelativepk() = Me.lvlNearestRelatives.SelectedItems(0).SubItems(4).Text
                .Show()
            End With
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BtnDeleteContact_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnDeleteContact.Click
        Try

            Dim x As New DialogResult
            Dim pkSID As String = Me.lvlNearestRelatives.SelectedItems(0).SubItems(4).Text
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            If x = System.Windows.Forms.DialogResult.OK Then
                If pkSID <> "" Then
                    Call DeleteRelatives(pkSID)
                    Call ViewNearestRelatives(Me.txtEmployeeNo.Text.Trim)
                End If
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception
        End Try
    End Sub
#Region "Delete Relatives"
    Private Sub DeleteRelatives(ByVal pk As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Relatives_Delete", _
                                     New SqlParameter("@employeeNo", pk))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete Relatives")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Delete Emergency Contact"
    Private Sub deleteEmergencyContact()
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_employee_EmergencyContact_Delete", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@fxkeyemployee", SqlDbType.Char, 10).Value = emergencykey
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show("usp_per_employee_EmergencyContact_Delete", "Check this stored procedure", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
#End Region

    Private Sub txtfullname_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtfullname.TextChanged
        Me.txtfullname.Text = Me.temp_lname.Text + "," + Me.temp_fname.Text + " " + Me.temp_midname.Text
    End Sub

    Private Sub mem_amountpaid_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not CurrencyInput_Validate(e.KeyChar)
    End Sub

    Private Sub txtpayrollcontriamount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtpayrollcontriamount.KeyPress
        e.Handled = Not CurrencyInput_Validate(e.KeyChar)
    End Sub

    Private Sub mem_baccount1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not CurrencyInput_Validate(e.KeyChar)
    End Sub

    Private Sub mem_baccount2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not CurrencyInput_Validate(e.KeyChar)
    End Sub

    Private Sub mem_baccount3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not CurrencyInput_Validate(e.KeyChar)
    End Sub

    Private Sub mem_WithdrawDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mem_WithdrawDate.ValueChanged
        Me.txtwithdrawal.Text = Me.mem_WithdrawDate.Text
    End Sub

    Private Sub txtEmpID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEmployeeNo.TextChanged
        If Me.txtEmployeeNo.Text = "" Then
            Call ControlDisabled()
        Else
            Call ViewNearestRelatives(Me.txtEmployeeNo.Text)
            Call GetmemberDependents(Me.txtEmployeeNo.Text)
            Call GetmemberBankInfo(Me.txtEmployeeNo.Text)
            Call GetsourceofIncomce(Me.txtEmployeeNo.Text)
            Call ControlEnabled()
        End If

    End Sub
#Region "Tab Button Disabled/enabled"
    Private Sub ControlDisabled()
        Me.btnNewBA.Enabled = False
        Me.btnUpdateBA.Enabled = False
        Me.btnDeleteBA.Enabled = False
        '==============
        Me.btnsaveSInc.Enabled = False
        Me.btnUpdateSInc.Enabled = False
        Me.btnDeleteSInc.Enabled = False
        '==============
        Me.btnaddContact.Enabled = False
        Me.BtnEditContact.Enabled = False
        Me.BtnDeleteContact.Enabled = False
        '==============
        Me.btnadd_dep.Enabled = False
        Me.btnemp_editdepdts.Enabled = False
        Me.btndelete_dep.Enabled = False
    End Sub
    Private Sub ControlEnabled()
        Me.btnNewBA.Enabled = True
        Me.btnUpdateBA.Enabled = True
        Me.btnDeleteBA.Enabled = True
        '==============
        Me.btnsaveSInc.Enabled = True
        Me.btnUpdateSInc.Enabled = True
        Me.btnDeleteSInc.Enabled = True
        '==============
        Me.btnaddContact.Enabled = True
        Me.BtnEditContact.Enabled = True
        Me.BtnDeleteContact.Enabled = True
        '==============
        Me.btnadd_dep.Enabled = True
        Me.btnemp_editdepdts.Enabled = True
        Me.btndelete_dep.Enabled = True
    End Sub
#End Region

    Private Sub txtpk_Employee_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtpk_Employee.TextChanged
        If Me.txtpk_Employee.Text <> "" Then
            Call PlotRecord(Me.txtpk_Employee.Text.Trim)
        End If
    End Sub
#Region "Plot record base on pk_employee"
    Public Sub PlotRecord(ByVal pkemployee As String)
        Dim gcon As New Clsappconfiguration

        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "MSS_MembersInfo_GetMasterInfo_Navigation", _
                                     New SqlParameter("@employeeNo", pkemployee))
            Try

                While rd.Read

                    Select Case (rd.Item(0))
                        Case True
                            Me.chkyes.Checked = True
                        Case False
                            Me.chkNo.Checked = True
                        Case Else

                    End Select

                    Me.txtEmployeeNo.Text = rd.Item(1).ToString.Trim
                    Me.txtMemberID.Text = rd.Item(2)
                    Me.temp_lname.Text = rd.Item(4)
                    Me.temp_fname.Text = rd.Item(5)
                    Me.temp_midname.Text = rd.Item(6)
                    Me.cboemp_gender.Text = rd.Item(7)
                    Me.cboemp_civil.Text = rd.Item(8)
                    Me.EMP_DATEbirth.Text = rd.Item(9)
                    Me.temp_placebirth.Text = rd.Item(10)
                    Me.txtResidenceadd.Text = rd.Item(11)
                    Me.txtprovincialadd.Text = rd.Item(12)
                    Me.txtresidencephone.Text = rd.Item(13)
                    Me.txtMobileNo.Text = rd.Item(14)
                    Me.txtEmailAddress.Text = rd.Item(15)
                    Me.cbotitledesignation.Text = rd.Item(16)
                    Me.cborate.Text = rd.Item(17)
                    Me.txtofficenumber.Text = rd.Item(18)
                    Me.txtlocalofficenumber.Text = rd.Item(19)
                    Me.emp_Datehired.Text = rd.Item(20)
                    Me.txtbasicpay.Text = rd.Item(21)
                    Me.mem_MemberDate.Text = rd.Item(22)
                    Me.txtwithdrawal.Text = rd.Item(23)
                    'Select Case (rd.Item(24))
                    '    Case True
                    '        Cbomem_Bereavement.Text = "True"
                    '    Case False
                    '        Cbomem_Bereavement.Text = "False"
                    '    Case Else
                    'End Select
                    Me.cboemp_status.Text = rd.Item(27)
                    Me.Cbomem_Status.Text = rd.Item(27)
                    Dim x As Double = rd.Item(29)
                    Dim y As Double = rd.Item(30)
                    Me.emp_Ytenures.Text = CStr(x) + " yr's" + " and " + CStr(y) + " m"
                    Me.cbotaxcode.Text = rd.Item(31)
                    Me.emp_company.Text = rd.Item(32)
                    Me.txtofficeadd.Text = rd.Item(33)
                    Me.txtpayrollcontriamount.Text = rd.Item(34)
                    Me.txtorgchart.Tag = rd.Item(35)
                    Me.cboPaycode.Text = rd.Item(36)
                    Me.cbopayroll.Text = rd.Item(37)
                    Me.mem_PaycontDate.Text = rd.Item(38)

                    If rd.Item(39) = True Then
                        rdoExempt.Checked = True
                        rdoNonExempt.Checked = False
                    Else
                        rdoExempt.Checked = False
                        rdoNonExempt.Checked = True
                    End If

                End While
                Call getorgchartvalue(Me.txtorgchart.Tag)
                Call getorgchartIDtodepartment2(Me.txtorgchart.Tag)
                rd.Close()
            Catch ex As Exception
                'REDO
                'MessageBox.Show(ex.Message, "PlotRecord")
            End Try
        End Using
    End Sub
#End Region

    Private Sub txtpk_Employee_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtpk_Employee.Validating
        Call PlotRecord(Me.txtpk_Employee.Text.Trim)
    End Sub

    Private Sub temp_email_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmailAddress.MouseLeave

    End Sub


    Private Sub temp_email_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEmailAddress.TextChanged

    End Sub

    Private Sub temp_email_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtEmailAddress.Validating
        Call validateEmail(Me.txtEmailAddress.Text)
    End Sub

    Private Sub btnNewBA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewBA.Click
        frmMember_BankAccount.lblBankAcct.Text = "Add New Bank Account"
        frmMember_BankAccount.btnupdate.Visible = False
        frmMember_BankAccount.Show()
    End Sub

    Private Sub btnUpdateBA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateBA.Click
        Try
            With frmMember_BankAccount
                .lblBankAcct.Text = "Edit Bank Account"
                .btnSave.Visible = False
                .btnupdate.Location = New System.Drawing.Point(5, 5)
                .txtBankAccount.Text = Me.lvlBankInfo.SelectedItems(0).Text
                .txtBankName.Text = Me.lvlBankInfo.SelectedItems(0).SubItems(1).Text
                .txtAccountType.Text = Me.lvlBankInfo.SelectedItems(0).SubItems(2).Text
                .getpkBA() = Me.lvlBankInfo.SelectedItems(0).SubItems(3).Text

                .Show()
            End With
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnDeleteBA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteBA.Click
        Try
            Dim x As New DialogResult
            Dim pkId As String = Me.lvlBankInfo.SelectedItems(0).SubItems(3).Text
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            If x = System.Windows.Forms.DialogResult.OK Then
                If pkId <> "" Then
                    Call DeleteBankAccount(pkId)
                    Call GetmemberBankInfo(Me.txtEmployeeNo.Text)
                End If
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception

        End Try
    End Sub
#Region "Delete Bank account"
    Private Sub DeleteBankAccount(ByVal pkBA As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_BankAccounts_Delete", _
                                      New SqlParameter("@employeeNo", pkBA))
            trans.Commit()

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete Bank Account")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region

    Private Sub btnsaveSInc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsaveSInc.Click
        frmMember_SourceofIncome.lblsourcInc.Text = "Add New Source of Income"
        frmMember_SourceofIncome.btnupdate.Visible = False
        frmMember_SourceofIncome.Show()
    End Sub

    Private Sub btnUpdateSInc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateSInc.Click
        Try
            With frmMember_SourceofIncome
                .lblsourcInc.Text = "Update Source of Income"
                .btnSave.Visible = False
                .btnupdate.Location = New System.Drawing.Point(5, 5)
                .txtSourceInc.Text = Me.lvlSourceIncome.SelectedItems(0).Text
                .txtAnnuallInc.Text = Me.lvlSourceIncome.SelectedItems(0).SubItems(1).Text
                .getpksource() = Me.lvlSourceIncome.SelectedItems(0).SubItems(2).Text
                .Show()
            End With
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnDeleteSInc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteSInc.Click
        Try
            Dim x As New DialogResult
            Dim pkId As String = Me.lvlSourceIncome.SelectedItems(0).SubItems(2).Text
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            If x = System.Windows.Forms.DialogResult.OK Then
                If pkId <> "" Then
                    Call DeleteSourceIncome(pkId)
                    Call GetsourceofIncomce(Me.txtEmployeeNo.Text)
                End If
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception

        End Try
    End Sub
#Region "Delete Source of Income"
    Private Sub DeleteSourceIncome(ByVal pkid As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_OtherIncomeSource_Delete", _
                                      New SqlParameter("@employeeNo", pkid))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete Source of Income")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region

    'Private Sub Cbomem_Bereavement_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Me.Cbomem_Bereavement.Text = "True" Then
    '        getbereavement() = 1
    '    Else
    '        getbereavement() = 0
    '    End If
    'End Sub

    Private Sub chkyes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkyes.CheckedChanged
        If Me.chkyes.Checked = True Then
            Me.chkNo.Enabled = False
            Me.chkNo.Checked = False
            getfbemployed() = True
        Else
            Me.chkNo.Enabled = True
        End If
    End Sub

    Private Sub chkNo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNo.CheckedChanged
        If Me.chkNo.Checked = True Then
            Me.chkyes.Enabled = False
            Me.chkyes.Checked = False
            getfbemployed() = False
        Else
            Me.chkyes.Enabled = True

        End If
    End Sub

    Private Sub Cbomem_Status_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cbomem_Status.SelectedIndexChanged
        If Me.Cbomem_Status.Text = "Active" Then
            getfbMembership() = 1
        ElseIf Me.Cbomem_Status.Text = "Inactive" Then
            getfbMembership() = 0
        End If
    End Sub

    Public Sub Integrate_CIMSToAccounting()
        Try
            Dim employeeNo As String = txtEmployeeNo.Text
            Dim memberName As String = lblemployee_name.Text
            Dim dateHired As Date = emp_Datehired.Value.Date
            Dim companyName As String = memberName
            Dim lastName As String = temp_lname.Text
            Dim firstName As String = temp_fname.Text
            Dim middleName As String = temp_midname.Text
            Dim telNo As String = txtresidencephone.Text
            Dim mobileNo As String = txtMobileNo.Text
            Dim officeNo As String = txtofficenumber.Text
            Dim emailAddress As String = txtEmailAddress.Text
            Dim isActive As String = "1"
            Dim balance As Decimal = 0
            Dim companyID As String = Integrate_CIMSGetCompanyID()
            Dim isDelete As String = "0"
            Dim user As String = frmLogin.Username.Text
            Dim dataCreated As Date = Date.Now.Date
            Dim address As String = txtResidenceadd.Text

            Dim BankName As String
            Dim BankAccountNo As String
            Dim BankAccountType As String

            If formMode = "Edit" Then
                BankName = IIf(Me.lvlBankInfo.Items(0).SubItems(0).Text <> "N/A", lvlBankInfo.Items(0).SubItems(1).Text, "")
                BankAccountNo = IIf(Me.lvlBankInfo.Items(0).SubItems(0).Text <> "N/A", lvlBankInfo.Items(0).SubItems(0).Text, "")
                BankAccountType = IIf(Me.lvlBankInfo.Items(0).SubItems(0).Text <> "N/A", lvlBankInfo.Items(0).SubItems(2).Text, "")
            End If

            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Member_Integration_Accounting", _
                        New SqlParameter("@employeeNo", employeeNo), _
                        New SqlParameter("@customerName", memberName), _
                        New SqlParameter("@dateHired", dateHired), _
                        New SqlParameter("@companyName", companyName), _
                        New SqlParameter("@lastName", lastName), _
                        New SqlParameter("@firstName", firstName), _
                        New SqlParameter("@middleName", middleName), _
                        New SqlParameter("@telNo", telNo), _
                        New SqlParameter("@mobileNo", mobileNo), _
                        New SqlParameter("@officeNo", officeNo), _
                        New SqlParameter("@emailAddress", emailAddress), _
                        New SqlParameter("@active", isActive), _
                        New SqlParameter("@fxKeyCompany", companyID), _
                        New SqlParameter("@balance", balance), _
                        New SqlParameter("@deleted", isDelete), _
                        New SqlParameter("@user", user), _
                        New SqlParameter("@dateCreated", dataCreated), _
                        New SqlParameter("@address", address), _
                        New SqlParameter("@bankAccount", BankAccountNo), _
                        New SqlParameter("@bankName", BankName), _
                        New SqlParameter("@accountType", BankAccountType))

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Function Integrate_CIMSGetCompanyID() As String
        Dim companyID As String

        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Integration_GetCompanyID")
            If rd.Read() Then
                companyID = rd.Item("accountingID").ToString()
            Else
                MessageBox.Show("Company ID Error: Please contact System Administrator,", _
                            "Accounting Integration", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End Using

        Return companyID
    End Function

    'Public Sub LoadWebRegistrationDetails()
    '    Try
    '        Using rd As SqlDataReader = (SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Admin_CheckIfMemberIsWebRegistered", _
    '                New SqlParameter("@employeeNo", txtEmployeeNo.Text)))
    '            If rd.Read() Then
    '                chkIsWebRegistered.Checked = True
    '                txtDefaultPassword.Text = rd.Item("Default Password").ToString()
    '            End If
    '        End Using
    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Private Sub chkShowPassword_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If chkShowPassword.Checked = True Then
    '        txtDefaultPassword.UseSystemPasswordChar = False
    '    Else
    '        txtDefaultPassword.UseSystemPasswordChar = True
    '    End If
    'End Sub

    'Private Sub RegisterSelectedMemberToWeb()
    '    Try
    '        Dim status As MembershipCreateStatus
    '        Dim username As String = txtEmployeeNo.Text
    '        Dim emailAddress As String = txtEmailAddress.Text
    '        Dim password As String

    '        Using rd As SqlDataReader = (SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "MSS_Admin_EmployeeNo_GetPassword", _
    '                New SqlParameter("@fcEmployeeNo", username)))
    '            If rd.Read Then
    '                password = rd.Item("fcPassword").ToString()
    '            End If
    '        End Using

    '        Membership.CreateUser(username, password, emailAddress, "Question", "Answer", True, status)

    '        If Not Roles.IsUserInRole(username, "Member") Then
    '            Roles.AddUserToRole(username, "Member")
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub UnRegisterSelectedMemberToWeb()
    '    Try
    '        Dim isDeleted As Boolean
    '        Dim username As String = txtEmployeeNo.Text
    '        isDeleted = Membership.DeleteUser(username, True)
    '        MessageBox.Show(isDeleted)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub



#Region "Eloading"
    Private Sub btnAddMobileNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddMobileNo.Click
        frmMember_AddMobile.txtMobileNo.Text = ""
        frmMember_AddMobile.GetEmpNo() = txtEmployeeNo.Text
        frmMember_AddMobile.GetMode() = "Add"
        frmMember_AddMobile.ShowDialog()
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditMobile.Click
        Try
            With frmMember_AddMobile
                .txtMobileNo.Text = gridMobileNos.Item("Mobile No", gridMobileNos.CurrentRow.Index).Value.ToString()
                .GetOldMobileNo() = gridMobileNos.Item("Mobile No", gridMobileNos.CurrentRow.Index).Value.ToString()
                .GetEmpNo() = txtEmployeeNo.Text
                .GetMode() = "Edit"
                .ShowDialog()
            End With

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub chkShowPINChar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowPINChar.CheckedChanged
        If chkShowPINChar.Checked = True Then
            txtEloaderPIN.UseSystemPasswordChar = False
        Else
            txtEloaderPIN.UseSystemPasswordChar = True
        End If
    End Sub

    Private Function isEloadingMemberValid() As Boolean
        Dim isValid As Boolean = True
        If chkIsEloader.Checked = True Then
            'If txtMobileNo.Text = "" Then
            '    MessageBox.Show("You are trying to register this member to e-loading Service," + vbNewLine + "but have not entered a valid mobile No." _
            '            + vbNewLine + vbNewLine + "Please Enter a mobile No. First.", "E-loading Service", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '    isValid = False

            'Else
            If txtEloaderPIN.Text = "" Or txtEloaderPIN.Text.Length <> 4 Then
                MessageBox.Show("You are trying to register this member to e-loading Service," + vbNewLine + "but have not entered a valid PIN No." _
                        + vbNewLine + vbNewLine + "Please enter a valid PIN No. (Must be 4 Chars only).", "E-loading Service", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                isValid = False
            ElseIf Cbomem_Status.Text.Trim <> "Active" And Cbomem_Status.Text.Trim <> "Associate" Then
                MessageBox.Show("You are trying to register this member to e-loading Service," + vbNewLine + "but the member is not active." _
                       + vbNewLine + vbNewLine + "Please Activate His membership first.", "E-loading Service", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                isValid = False
            End If
        End If
        Return isValid
    End Function

    Private Sub RegisterMemberToEloadingApps(ByVal empNo As String, ByVal PIN As String, ByVal limit As Decimal)

        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Member_Eloading_RegisterMember", _
                    New SqlParameter("@coopID", empNo), _
                    New SqlParameter("@PIN", PIN), _
                    New SqlParameter("@limit", limit))
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub UnRegisterMemberToEloadingApps(ByVal empNo As String)

        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Member_Eloading_UnRegisterMember", _
                    New SqlParameter("@coopID", empNo))
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub LoadEloadingRegistrationDetails(ByVal empNo As String)
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Member_Eloading_Status_PerMember", _
                    New SqlParameter("@fcEmployeeNo", empNo))
                If rd.Read() Then

                    'Validate Is Loader
                    Dim isLoader As String
                    isLoader = rd.Item("Is Eloader").ToString()
                    If isLoader <> "0" Or isLoader <> "1" Then
                        chkIsEloader.Checked = False
                    Else
                        chkIsEloader.Checked = rd.Item("Is Eloader")

                    End If

                    txtEloaderPIN.Text = rd.Item("PIN").ToString()
                    txtEloadingLimit.Text = rd.Item("E-Loading Limit").ToString()
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try


        Call LoadMemberMobileNumbers(empNo)
    End Sub

    Public Sub LoadMemberMobileNumbers(ByVal empNo As String)
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Member_Eloading_ViewMobileNos", _
                New SqlParameter("@employeeNo", empNo))

        With gridMobileNos
            .DataSource = ds.Tables(0)

            .Columns("pk_Employee").Visible = False
        End With
    End Sub

    Private Sub gridMobileNos_UserDeletingRow(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles gridMobileNos.UserDeletingRow
        Dim mobileNo As String = e.Row.Cells("Mobile No").Value.ToString()

        If MessageBox.Show("Are you sure you want to delete this number from member's record?", "Delete Mobile Nos.", MessageBoxButtons.YesNo, MessageBoxIcon.Stop) = Windows.Forms.DialogResult.Yes Then
            DeleteMemberMobileNo(mobileNo)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub DeleteMemberMobileNo(ByVal mobileNo As String)
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Member_Eloading_DeleteMobileNo", _
            New SqlParameter("@mobileNo", mobileNo))
    End Sub

#End Region

    Private Sub chkBereaveYes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBereaveYes.CheckedChanged

        If Me.chkBereaveYes.Checked = True Then
            Me.chkBereaveNo.Enabled = False
            Me.chkBereaveNo.Checked = False
            getbereavement() = True
        Else
            Me.chkBereaveNo.Enabled = True
        End If
    End Sub

    Private Sub chkBereaveNo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBereaveNo.CheckedChanged
        If Me.chkBereaveNo.Checked = True Then
            Me.chkBereaveYes.Enabled = False
            Me.chkBereaveYes.Checked = False
            getbereavement() = False
        Else
            Me.chkBereaveYes.Enabled = True

        End If
    End Sub

    'Private Sub tabMember_Selected(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TabControlEventArgs) Handles tabMember.Selected
    '    If TabPage2.Click = True Then
    '        If btnemp_add.Text = "Save" Then
    '            MessageBox.Show("Please save first the member information to add the bank account information", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information)

    '            'MessageBox.Show("Supply the required fields", "Cannot Save", MessageBoxButtons.OK, MessageBoxIcon.Warning)

    '        End If
    '    End If

    'End Sub

    Private Sub TabPage2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabPage2.Click

        If btnemp_add.Text = "Save" Then
            MessageBox.Show("Please save first the member information to add the bank account information", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information)

            'MessageBox.Show("Supply the required fields", "Cannot Save", MessageBoxButtons.OK, MessageBoxIcon.Warning)

        End If

    End Sub
End Class