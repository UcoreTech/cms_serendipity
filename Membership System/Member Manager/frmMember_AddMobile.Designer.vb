<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMember_AddMobile
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMember_AddMobile))
        Me.txtMobileNo = New System.Windows.Forms.TextBox
        Me.btnAddEdit = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'txtMobileNo
        '
        Me.txtMobileNo.AccessibleDescription = ""
        Me.txtMobileNo.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMobileNo.Location = New System.Drawing.Point(12, 12)
        Me.txtMobileNo.MaxLength = 11
        Me.txtMobileNo.Name = "txtMobileNo"
        Me.txtMobileNo.Size = New System.Drawing.Size(260, 27)
        Me.txtMobileNo.TabIndex = 0
        Me.txtMobileNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnAddEdit
        '
        Me.btnAddEdit.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddEdit.Image = Global.WindowsApplication2.My.Resources.Resources.edit_add
        Me.btnAddEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAddEdit.Location = New System.Drawing.Point(60, 45)
        Me.btnAddEdit.Name = "btnAddEdit"
        Me.btnAddEdit.Size = New System.Drawing.Size(153, 39)
        Me.btnAddEdit.TabIndex = 2
        Me.btnAddEdit.Text = "Add Mobile No."
        Me.btnAddEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAddEdit.UseVisualStyleBackColor = True
        '
        'frmMember_AddMobile
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 87)
        Me.Controls.Add(Me.btnAddEdit)
        Me.Controls.Add(Me.txtMobileNo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMember_AddMobile"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add Mobile"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtMobileNo As System.Windows.Forms.TextBox
    Friend WithEvents btnAddEdit As System.Windows.Forms.Button
End Class
