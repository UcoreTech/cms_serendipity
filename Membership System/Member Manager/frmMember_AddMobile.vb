Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmMember_AddMobile
    Private gCon As New Clsappconfiguration()


    Private mode As String
    Public Property GetMode() As String
        Get
            Return mode
        End Get
        Set(ByVal value As String)
            mode = value
        End Set
    End Property


    Private empNo As String
    Public Property GetEmpNo() As String
        Get
            Return empNo
        End Get
        Set(ByVal value As String)
            empNo = value
        End Set
    End Property

    Private oldMobileNo As String
    Public Property GetOldMobileNo() As String
        Get
            Return oldMobileNo
        End Get
        Set(ByVal value As String)
            oldMobileNo = value
        End Set
    End Property



    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddEdit.Click
        Dim employeeNo As String = GetEmpNo()

        Select Case GetMode()
            Case "Add"
                If IsMobileNoValid() Then
                    Call AddMobileNo(employeeNo)
                    Call frmMember_Master.LoadMemberMobileNumbers(employeeNo)
                End If

            Case "Edit"
                If IsMobileNoValid() Then
                    Call EditMobileNo()
                    Call frmMember_Master.LoadMemberMobileNumbers(employeeNo)

                    'TODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODO
                    'Must pass and validate old number
                    'TODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODOTODO
                End If
        End Select

        Close()
    End Sub

    Private Function IsMobileNoValid() As Boolean
        If Me.txtMobileNo.Text.Length <> 11 Then
            MessageBox.Show("Invalid Entry! Must contain 11 Chars e.g. 09176217129. ", "Invalid Mobile Number", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        ElseIf IsNumeric(txtMobileNo.Text) = False Then
            MessageBox.Show("Invalid Entry! Must be numerical only.", "Invalid Entry!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub AddMobileNo(ByVal empNo As String)
        Dim employeeNo As String = GetEmpNo()
        Dim mobileNo As String = txtMobileNo.Text

        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Member_Eloading_AddMobileNos", _
                New SqlParameter("@employeeNo", employeeNo), _
                New SqlParameter("@mobileNo", mobileNo))
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub EditMobileNo()
        Dim employeeNo As String = GetEmpNo()
        Dim oldMobileNo As String = GetOldMobileNo()
        Dim newMobileNo As String = txtMobileNo.Text

        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Member_Eloading_EditMobileNos", _
                New SqlParameter("@mobileNo", oldMobileNo), _
                New SqlParameter("@newMobileNo", newMobileNo))
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub frmMember_AddMobile_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnAddEdit.Text = GetMode() + " Mobile No."
    End Sub
End Class