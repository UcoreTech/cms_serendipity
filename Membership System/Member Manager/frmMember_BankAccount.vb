Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMember_BankAccount
#Region "Variables"
    Public pkBA As String

#End Region
#Region "Property"
    Public Property getpkBA() As String
        Get
            Return pkBA
        End Get
        Set(ByVal value As String)
            pkBA = value
        End Set
    End Property
#End Region
#Region "Add/Edit Bank account"
    Private Sub AddEditbankAccount(ByVal empID As String, ByVal bankacct As String, ByVal bankname As String, ByVal accountype As String, _
                                  ByVal pkbankaccount As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_BankAccounts_AddEdit", _
                                     New SqlParameter("@employeeNo", empID), _
                                     New SqlParameter("@bankAccount", bankacct), _
                                     New SqlParameter("@bankName", bankname), _
                                     New SqlParameter("@accountType", accountype), _
                                     New SqlParameter("@pk_BankAccount", pkbankaccount))
            trans.Commit()

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Save Bank Account")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region
    Public Function BankAccount(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = "-") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Call AddEditbankAccount(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtBankAccount.Text.Trim, Me.txtBankName.Text.Trim, txtAccountType.Text, "")
        Call frmMember_Master.GetmemberBankInfo(frmMember_Master.txtEmployeeNo.Text)
        Me.Close()
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Call AddEditbankAccount(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtBankAccount.Text.Trim, Me.txtBankName.Text.Trim, txtAccountType.Text, pkBA)
        Call frmMember_Master.GetmemberBankInfo(frmMember_Master.txtEmployeeNo.Text)
        Me.Close()
    End Sub

    Private Sub txtBankAccount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBankAccount.KeyPress
        e.Handled = Not BankAccount(e.KeyChar)
    End Sub

    Private Sub txtBankAccount_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBankAccount.TextChanged

    End Sub
End Class