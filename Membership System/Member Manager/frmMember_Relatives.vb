Imports System.Data.SqlClient
Imports System.Data.Sql
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMember_Relatives
#Region "variable"
    Public pkId As String
#End Region
#Region "property"
    Public Property getRelativepk() As String
        Get
            Return pkId
        End Get
        Set(ByVal value As String)
            pkId = value
        End Set
    End Property
#End Region
#Region "Add/Edit Relatives"
    Private Sub AddEditRelatives(ByVal empid As String, ByVal relativename As String, ByVal relationship As String, ByVal contact As String, _
                                 ByVal address As String) ', ByVal pkid As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Relatives_AddEdit", _
                                      New SqlParameter("@employeeNo", empid), _
                                      New SqlParameter("@fcRelativeName", relativename), _
                                      New SqlParameter("@fcRelationship", relationship), _
                                      New SqlParameter("@fcContactNo", contact), _
                                      New SqlParameter("@fcAddress", address))
            'New SqlParameter("@pk_Relative", pkid))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Add New Relatives")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region
    Public Function Contact(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = "-") And Not (C = "(") And Not (C = ")") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub frmEmergencyContact_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub


    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Call AddEditRelatives(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtname.Text, Me.txtrelationship.Text, Me.txtcontact.Text, Me.txtaddress.Text)
        Call frmMember_Master.ViewNearestRelatives(frmMember_Master.txtEmployeeNo.Text.Trim)
        Me.Close()
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Call AddEditRelatives(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtname.Text, Me.txtrelationship.Text, Me.txtcontact.Text, Me.txtaddress.Text) ', pkId)
        Call frmMember_Master.ViewNearestRelatives(frmMember_Master.txtEmployeeNo.Text.Trim)
        Me.Close()
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub txtcontact_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtcontact.KeyPress
        e.Handled = Not Contact(e.KeyChar)
    End Sub

    Private Sub txtcontact_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtcontact.TextChanged

    End Sub
End Class