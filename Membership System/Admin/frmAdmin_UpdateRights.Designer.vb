﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdmin_UpdateRights
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GrdRights = New System.Windows.Forms.DataGridView()
        Me.BtnClose = New System.Windows.Forms.Button()
        Me.BtnUpdate = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PanePanel2 = New WindowsApplication2.PanePanel()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.Txtusername = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnSearch = New System.Windows.Forms.Button()
        CType(Me.GrdRights, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanePanel2.SuspendLayout()
        Me.PanePanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GrdRights
        '
        Me.GrdRights.AllowUserToAddRows = False
        Me.GrdRights.AllowUserToDeleteRows = False
        Me.GrdRights.AllowUserToResizeRows = False
        Me.GrdRights.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GrdRights.BackgroundColor = System.Drawing.SystemColors.Window
        Me.GrdRights.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GrdRights.GridColor = System.Drawing.Color.Black
        Me.GrdRights.Location = New System.Drawing.Point(2, 78)
        Me.GrdRights.MultiSelect = False
        Me.GrdRights.Name = "GrdRights"
        Me.GrdRights.RowHeadersVisible = False
        Me.GrdRights.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GrdRights.Size = New System.Drawing.Size(321, 389)
        Me.GrdRights.TabIndex = 96
        '
        'BtnClose
        '
        Me.BtnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnClose.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnClose.Location = New System.Drawing.Point(229, 6)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Size = New System.Drawing.Size(87, 25)
        Me.BtnClose.TabIndex = 3
        Me.BtnClose.Text = "Close"
        Me.BtnClose.UseVisualStyleBackColor = True
        '
        'BtnUpdate
        '
        Me.BtnUpdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnUpdate.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnUpdate.Location = New System.Drawing.Point(136, 6)
        Me.BtnUpdate.Name = "BtnUpdate"
        Me.BtnUpdate.Size = New System.Drawing.Size(87, 25)
        Me.BtnUpdate.TabIndex = 2
        Me.BtnUpdate.Text = "Update"
        Me.BtnUpdate.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(11, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "User Rights"
        '
        'PanePanel2
        '
        Me.PanePanel2.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel2.Controls.Add(Me.BtnClose)
        Me.PanePanel2.Controls.Add(Me.BtnUpdate)
        Me.PanePanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel2.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel2.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel2.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel2.Location = New System.Drawing.Point(0, 470)
        Me.PanePanel2.Name = "PanePanel2"
        Me.PanePanel2.Size = New System.Drawing.Size(325, 36)
        Me.PanePanel2.TabIndex = 89
        '
        'PanePanel1
        '
        Me.PanePanel1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.Label1)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel1.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(325, 36)
        Me.PanePanel1.TabIndex = 88
        '
        'Txtusername
        '
        Me.Txtusername.Location = New System.Drawing.Point(63, 49)
        Me.Txtusername.Name = "Txtusername"
        Me.Txtusername.Size = New System.Drawing.Size(167, 23)
        Me.Txtusername.TabIndex = 98
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(7, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 15)
        Me.Label2.TabIndex = 97
        Me.Label2.Text = "User ID:"
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearch.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearch.Location = New System.Drawing.Point(236, 47)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(87, 25)
        Me.btnSearch.TabIndex = 99
        Me.btnSearch.Text = "Search"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'frmAdmin_UpdateRights
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(325, 506)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.Txtusername)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.GrdRights)
        Me.Controls.Add(Me.PanePanel2)
        Me.Controls.Add(Me.PanePanel1)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAdmin_UpdateRights"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "User Rights"
        CType(Me.GrdRights, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanePanel2.ResumeLayout(False)
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GrdRights As System.Windows.Forms.DataGridView
    Friend WithEvents BtnClose As System.Windows.Forms.Button
    Friend WithEvents BtnUpdate As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PanePanel2 As WindowsApplication2.PanePanel
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents Txtusername As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnSearch As System.Windows.Forms.Button
End Class
