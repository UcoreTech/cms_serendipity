<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAdmin_FileUploader
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmAdmin_FileUploader))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.txtfile = New System.Windows.Forms.TextBox()
        Me.btngetfile = New System.Windows.Forms.Button()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkbasicpay = New System.Windows.Forms.CheckBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtfileATM = New System.Windows.Forms.TextBox()
        Me.btngetfileATM = New System.Windows.Forms.Button()
        Me.BtnUploadATM = New System.Windows.Forms.Button()
        Me.chkATM = New System.Windows.Forms.CheckBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnuploadtaxcode = New System.Windows.Forms.Button()
        Me.btngetfiletaxcode = New System.Windows.Forms.Button()
        Me.txtfiletaxcode = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.chktaxcode = New System.Windows.Forms.CheckBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.btnUploadPosition = New System.Windows.Forms.Button()
        Me.btngetPositionfile = New System.Windows.Forms.Button()
        Me.txtfilePositionUpload = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ChkPosition = New System.Windows.Forms.CheckBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.btnUploadDepartment = New System.Windows.Forms.Button()
        Me.btngetDepartment = New System.Windows.Forms.Button()
        Me.txtfiledepartmentUpload = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Chkdepartment = New System.Windows.Forms.CheckBox()
        Me.btn7column = New System.Windows.Forms.Button()
        Me.txtpath7column = New System.Windows.Forms.TextBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.btnUpload7Column = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.chk7column = New System.Windows.Forms.CheckBox()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.BtnUploadTKClientID = New System.Windows.Forms.Button()
        Me.btngetTKClientID = New System.Windows.Forms.Button()
        Me.txtTKClientID = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.chkTKClientID = New System.Windows.Forms.CheckBox()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.btnUploadResignedActive = New System.Windows.Forms.Button()
        Me.btngetResignedActive = New System.Windows.Forms.Button()
        Me.txtResignedActive = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.chkResignedActive = New System.Windows.Forms.CheckBox()
        Me.Btnupdate = New System.Windows.Forms.Button()
        Me.btnclose = New System.Windows.Forms.Button()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.PanePanel3 = New WindowsApplication2.PanePanel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.PanePanel2 = New WindowsApplication2.PanePanel()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.PanePanel1.SuspendLayout()
        Me.PanePanel3.SuspendLayout()
        Me.PanePanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(28, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Filename"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(497, 15)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(58, 23)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "Upload"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(575, 43)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(351, 345)
        Me.DataGridView1.TabIndex = 8
        '
        'txtfile
        '
        Me.txtfile.Location = New System.Drawing.Point(80, 17)
        Me.txtfile.Name = "txtfile"
        Me.txtfile.Size = New System.Drawing.Size(365, 20)
        Me.txtfile.TabIndex = 7
        '
        'btngetfile
        '
        Me.btngetfile.Location = New System.Drawing.Point(449, 15)
        Me.btngetfile.Name = "btngetfile"
        Me.btngetfile.Size = New System.Drawing.Size(45, 23)
        Me.btngetfile.TabIndex = 6
        Me.btngetfile.Text = "..."
        Me.btngetfile.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(575, 395)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(351, 23)
        Me.ProgressBar1.TabIndex = 13
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkbasicpay)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtfile)
        Me.GroupBox1.Controls.Add(Me.btngetfile)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 43)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(560, 47)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Basic Pay"
        '
        'chkbasicpay
        '
        Me.chkbasicpay.AutoSize = True
        Me.chkbasicpay.Location = New System.Drawing.Point(12, 20)
        Me.chkbasicpay.Name = "chkbasicpay"
        Me.chkbasicpay.Size = New System.Drawing.Size(15, 14)
        Me.chkbasicpay.TabIndex = 15
        Me.chkbasicpay.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.txtfileATM)
        Me.GroupBox2.Controls.Add(Me.btngetfileATM)
        Me.GroupBox2.Controls.Add(Me.BtnUploadATM)
        Me.GroupBox2.Controls.Add(Me.chkATM)
        Me.GroupBox2.Location = New System.Drawing.Point(9, 91)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(560, 45)
        Me.GroupBox2.TabIndex = 15
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "ATM"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(28, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(49, 13)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "Filename"
        '
        'txtfileATM
        '
        Me.txtfileATM.Location = New System.Drawing.Point(80, 17)
        Me.txtfileATM.Name = "txtfileATM"
        Me.txtfileATM.Size = New System.Drawing.Size(365, 20)
        Me.txtfileATM.TabIndex = 18
        '
        'btngetfileATM
        '
        Me.btngetfileATM.Location = New System.Drawing.Point(449, 15)
        Me.btngetfileATM.Name = "btngetfileATM"
        Me.btngetfileATM.Size = New System.Drawing.Size(45, 23)
        Me.btngetfileATM.TabIndex = 17
        Me.btngetfileATM.Text = "..."
        Me.btngetfileATM.UseVisualStyleBackColor = True
        '
        'BtnUploadATM
        '
        Me.BtnUploadATM.Location = New System.Drawing.Point(495, 15)
        Me.BtnUploadATM.Name = "BtnUploadATM"
        Me.BtnUploadATM.Size = New System.Drawing.Size(58, 23)
        Me.BtnUploadATM.TabIndex = 19
        Me.BtnUploadATM.Text = "Upload"
        Me.BtnUploadATM.UseVisualStyleBackColor = True
        '
        'chkATM
        '
        Me.chkATM.AutoSize = True
        Me.chkATM.Location = New System.Drawing.Point(12, 20)
        Me.chkATM.Name = "chkATM"
        Me.chkATM.Size = New System.Drawing.Size(15, 14)
        Me.chkATM.TabIndex = 16
        Me.chkATM.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnuploadtaxcode)
        Me.GroupBox3.Controls.Add(Me.btngetfiletaxcode)
        Me.GroupBox3.Controls.Add(Me.txtfiletaxcode)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.chktaxcode)
        Me.GroupBox3.Location = New System.Drawing.Point(10, 138)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(560, 45)
        Me.GroupBox3.TabIndex = 18
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Tax Code"
        '
        'btnuploadtaxcode
        '
        Me.btnuploadtaxcode.Location = New System.Drawing.Point(494, 11)
        Me.btnuploadtaxcode.Name = "btnuploadtaxcode"
        Me.btnuploadtaxcode.Size = New System.Drawing.Size(58, 23)
        Me.btnuploadtaxcode.TabIndex = 20
        Me.btnuploadtaxcode.Text = "Upload"
        Me.btnuploadtaxcode.UseVisualStyleBackColor = True
        '
        'btngetfiletaxcode
        '
        Me.btngetfiletaxcode.Location = New System.Drawing.Point(448, 11)
        Me.btngetfiletaxcode.Name = "btngetfiletaxcode"
        Me.btngetfiletaxcode.Size = New System.Drawing.Size(45, 23)
        Me.btngetfiletaxcode.TabIndex = 19
        Me.btngetfiletaxcode.Text = "..."
        Me.btngetfiletaxcode.UseVisualStyleBackColor = True
        '
        'txtfiletaxcode
        '
        Me.txtfiletaxcode.Location = New System.Drawing.Point(79, 13)
        Me.txtfiletaxcode.Name = "txtfiletaxcode"
        Me.txtfiletaxcode.Size = New System.Drawing.Size(365, 20)
        Me.txtfiletaxcode.TabIndex = 19
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(27, 20)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 13)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "Filename"
        '
        'chktaxcode
        '
        Me.chktaxcode.AutoSize = True
        Me.chktaxcode.Location = New System.Drawing.Point(12, 19)
        Me.chktaxcode.Name = "chktaxcode"
        Me.chktaxcode.Size = New System.Drawing.Size(15, 14)
        Me.chktaxcode.TabIndex = 19
        Me.chktaxcode.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.btnUploadPosition)
        Me.GroupBox4.Controls.Add(Me.btngetPositionfile)
        Me.GroupBox4.Controls.Add(Me.txtfilePositionUpload)
        Me.GroupBox4.Controls.Add(Me.Label5)
        Me.GroupBox4.Controls.Add(Me.ChkPosition)
        Me.GroupBox4.Location = New System.Drawing.Point(10, 186)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(560, 45)
        Me.GroupBox4.TabIndex = 19
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Position"
        '
        'btnUploadPosition
        '
        Me.btnUploadPosition.Location = New System.Drawing.Point(494, 11)
        Me.btnUploadPosition.Name = "btnUploadPosition"
        Me.btnUploadPosition.Size = New System.Drawing.Size(58, 23)
        Me.btnUploadPosition.TabIndex = 20
        Me.btnUploadPosition.Text = "Upload"
        Me.btnUploadPosition.UseVisualStyleBackColor = True
        '
        'btngetPositionfile
        '
        Me.btngetPositionfile.Location = New System.Drawing.Point(448, 11)
        Me.btngetPositionfile.Name = "btngetPositionfile"
        Me.btngetPositionfile.Size = New System.Drawing.Size(45, 23)
        Me.btngetPositionfile.TabIndex = 19
        Me.btngetPositionfile.Text = "..."
        Me.btngetPositionfile.UseVisualStyleBackColor = True
        '
        'txtfilePositionUpload
        '
        Me.txtfilePositionUpload.Location = New System.Drawing.Point(79, 13)
        Me.txtfilePositionUpload.Name = "txtfilePositionUpload"
        Me.txtfilePositionUpload.Size = New System.Drawing.Size(365, 20)
        Me.txtfilePositionUpload.TabIndex = 19
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(27, 20)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(49, 13)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "Filename"
        '
        'ChkPosition
        '
        Me.ChkPosition.AutoSize = True
        Me.ChkPosition.Location = New System.Drawing.Point(12, 19)
        Me.ChkPosition.Name = "ChkPosition"
        Me.ChkPosition.Size = New System.Drawing.Size(15, 14)
        Me.ChkPosition.TabIndex = 19
        Me.ChkPosition.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.btnUploadDepartment)
        Me.GroupBox5.Controls.Add(Me.btngetDepartment)
        Me.GroupBox5.Controls.Add(Me.txtfiledepartmentUpload)
        Me.GroupBox5.Controls.Add(Me.Label6)
        Me.GroupBox5.Controls.Add(Me.Chkdepartment)
        Me.GroupBox5.Location = New System.Drawing.Point(10, 233)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(560, 45)
        Me.GroupBox5.TabIndex = 20
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Department"
        '
        'btnUploadDepartment
        '
        Me.btnUploadDepartment.Location = New System.Drawing.Point(494, 11)
        Me.btnUploadDepartment.Name = "btnUploadDepartment"
        Me.btnUploadDepartment.Size = New System.Drawing.Size(58, 23)
        Me.btnUploadDepartment.TabIndex = 20
        Me.btnUploadDepartment.Text = "Upload"
        Me.btnUploadDepartment.UseVisualStyleBackColor = True
        '
        'btngetDepartment
        '
        Me.btngetDepartment.Location = New System.Drawing.Point(448, 11)
        Me.btngetDepartment.Name = "btngetDepartment"
        Me.btngetDepartment.Size = New System.Drawing.Size(45, 23)
        Me.btngetDepartment.TabIndex = 19
        Me.btngetDepartment.Text = "..."
        Me.btngetDepartment.UseVisualStyleBackColor = True
        '
        'txtfiledepartmentUpload
        '
        Me.txtfiledepartmentUpload.Location = New System.Drawing.Point(79, 13)
        Me.txtfiledepartmentUpload.Name = "txtfiledepartmentUpload"
        Me.txtfiledepartmentUpload.Size = New System.Drawing.Size(365, 20)
        Me.txtfiledepartmentUpload.TabIndex = 19
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(27, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(49, 13)
        Me.Label6.TabIndex = 21
        Me.Label6.Text = "Filename"
        '
        'Chkdepartment
        '
        Me.Chkdepartment.AutoSize = True
        Me.Chkdepartment.Location = New System.Drawing.Point(12, 19)
        Me.Chkdepartment.Name = "Chkdepartment"
        Me.Chkdepartment.Size = New System.Drawing.Size(15, 14)
        Me.Chkdepartment.TabIndex = 19
        Me.Chkdepartment.UseVisualStyleBackColor = True
        '
        'btn7column
        '
        Me.btn7column.Location = New System.Drawing.Point(448, 11)
        Me.btn7column.Name = "btn7column"
        Me.btn7column.Size = New System.Drawing.Size(45, 23)
        Me.btn7column.TabIndex = 19
        Me.btn7column.Text = "..."
        Me.btn7column.UseVisualStyleBackColor = True
        '
        'txtpath7column
        '
        Me.txtpath7column.Location = New System.Drawing.Point(79, 13)
        Me.txtpath7column.Name = "txtpath7column"
        Me.txtpath7column.Size = New System.Drawing.Size(365, 20)
        Me.txtpath7column.TabIndex = 19
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.btnUpload7Column)
        Me.GroupBox6.Controls.Add(Me.btn7column)
        Me.GroupBox6.Controls.Add(Me.txtpath7column)
        Me.GroupBox6.Controls.Add(Me.Label7)
        Me.GroupBox6.Controls.Add(Me.chk7column)
        Me.GroupBox6.Location = New System.Drawing.Point(8, 280)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(560, 45)
        Me.GroupBox6.TabIndex = 21
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Job Grade, Hiring Date, Regularization Date, Position, Resignation Date, Gender"
        '
        'btnUpload7Column
        '
        Me.btnUpload7Column.Location = New System.Drawing.Point(494, 11)
        Me.btnUpload7Column.Name = "btnUpload7Column"
        Me.btnUpload7Column.Size = New System.Drawing.Size(58, 23)
        Me.btnUpload7Column.TabIndex = 20
        Me.btnUpload7Column.Text = "Upload"
        Me.btnUpload7Column.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(27, 20)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(49, 13)
        Me.Label7.TabIndex = 21
        Me.Label7.Text = "Filename"
        '
        'chk7column
        '
        Me.chk7column.AutoSize = True
        Me.chk7column.Location = New System.Drawing.Point(12, 19)
        Me.chk7column.Name = "chk7column"
        Me.chk7column.Size = New System.Drawing.Size(15, 14)
        Me.chk7column.TabIndex = 19
        Me.chk7column.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.BtnUploadTKClientID)
        Me.GroupBox7.Controls.Add(Me.btngetTKClientID)
        Me.GroupBox7.Controls.Add(Me.txtTKClientID)
        Me.GroupBox7.Controls.Add(Me.Label8)
        Me.GroupBox7.Controls.Add(Me.chkTKClientID)
        Me.GroupBox7.Location = New System.Drawing.Point(7, 326)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(560, 45)
        Me.GroupBox7.TabIndex = 24
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "TK ID, Client ID"
        '
        'BtnUploadTKClientID
        '
        Me.BtnUploadTKClientID.Location = New System.Drawing.Point(494, 11)
        Me.BtnUploadTKClientID.Name = "BtnUploadTKClientID"
        Me.BtnUploadTKClientID.Size = New System.Drawing.Size(58, 23)
        Me.BtnUploadTKClientID.TabIndex = 20
        Me.BtnUploadTKClientID.Text = "Upload"
        Me.BtnUploadTKClientID.UseVisualStyleBackColor = True
        '
        'btngetTKClientID
        '
        Me.btngetTKClientID.Location = New System.Drawing.Point(448, 11)
        Me.btngetTKClientID.Name = "btngetTKClientID"
        Me.btngetTKClientID.Size = New System.Drawing.Size(45, 23)
        Me.btngetTKClientID.TabIndex = 19
        Me.btngetTKClientID.Text = "..."
        Me.btngetTKClientID.UseVisualStyleBackColor = True
        '
        'txtTKClientID
        '
        Me.txtTKClientID.Location = New System.Drawing.Point(79, 13)
        Me.txtTKClientID.Name = "txtTKClientID"
        Me.txtTKClientID.Size = New System.Drawing.Size(365, 20)
        Me.txtTKClientID.TabIndex = 19
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(27, 17)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(49, 13)
        Me.Label8.TabIndex = 21
        Me.Label8.Text = "Filename"
        '
        'chkTKClientID
        '
        Me.chkTKClientID.AutoSize = True
        Me.chkTKClientID.Location = New System.Drawing.Point(12, 17)
        Me.chkTKClientID.Name = "chkTKClientID"
        Me.chkTKClientID.Size = New System.Drawing.Size(15, 14)
        Me.chkTKClientID.TabIndex = 19
        Me.chkTKClientID.UseVisualStyleBackColor = True
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.btnUploadResignedActive)
        Me.GroupBox8.Controls.Add(Me.btngetResignedActive)
        Me.GroupBox8.Controls.Add(Me.txtResignedActive)
        Me.GroupBox8.Controls.Add(Me.Label9)
        Me.GroupBox8.Controls.Add(Me.chkResignedActive)
        Me.GroupBox8.Location = New System.Drawing.Point(7, 373)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(560, 45)
        Me.GroupBox8.TabIndex = 25
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Resigned to Active"
        '
        'btnUploadResignedActive
        '
        Me.btnUploadResignedActive.Location = New System.Drawing.Point(494, 11)
        Me.btnUploadResignedActive.Name = "btnUploadResignedActive"
        Me.btnUploadResignedActive.Size = New System.Drawing.Size(58, 23)
        Me.btnUploadResignedActive.TabIndex = 20
        Me.btnUploadResignedActive.Text = "Upload"
        Me.btnUploadResignedActive.UseVisualStyleBackColor = True
        '
        'btngetResignedActive
        '
        Me.btngetResignedActive.Location = New System.Drawing.Point(448, 11)
        Me.btngetResignedActive.Name = "btngetResignedActive"
        Me.btngetResignedActive.Size = New System.Drawing.Size(45, 23)
        Me.btngetResignedActive.TabIndex = 19
        Me.btngetResignedActive.Text = "..."
        Me.btngetResignedActive.UseVisualStyleBackColor = True
        '
        'txtResignedActive
        '
        Me.txtResignedActive.Location = New System.Drawing.Point(79, 13)
        Me.txtResignedActive.Name = "txtResignedActive"
        Me.txtResignedActive.Size = New System.Drawing.Size(365, 20)
        Me.txtResignedActive.TabIndex = 19
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(27, 17)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(49, 13)
        Me.Label9.TabIndex = 21
        Me.Label9.Text = "Filename"
        '
        'chkResignedActive
        '
        Me.chkResignedActive.AutoSize = True
        Me.chkResignedActive.Location = New System.Drawing.Point(12, 17)
        Me.chkResignedActive.Name = "chkResignedActive"
        Me.chkResignedActive.Size = New System.Drawing.Size(15, 14)
        Me.chkResignedActive.TabIndex = 19
        Me.chkResignedActive.UseVisualStyleBackColor = True
        '
        'Btnupdate
        '
        Me.Btnupdate.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btnupdate.Location = New System.Drawing.Point(753, 6)
        Me.Btnupdate.Name = "Btnupdate"
        Me.Btnupdate.Size = New System.Drawing.Size(87, 25)
        Me.Btnupdate.TabIndex = 11
        Me.Btnupdate.Text = "Update"
        Me.Btnupdate.UseVisualStyleBackColor = True
        '
        'btnclose
        '
        Me.btnclose.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnclose.Location = New System.Drawing.Point(846, 6)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.Size = New System.Drawing.Size(87, 25)
        Me.btnclose.TabIndex = 12
        Me.btnclose.Text = "Close"
        Me.btnclose.UseVisualStyleBackColor = True
        '
        'PanePanel1
        '
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.PanePanel3)
        Me.PanePanel1.Controls.Add(Me.Label3)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.White
        Me.PanePanel1.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(938, 37)
        Me.PanePanel1.TabIndex = 22
        '
        'PanePanel3
        '
        Me.PanePanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel3.Controls.Add(Me.Label10)
        Me.PanePanel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel3.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel3.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel3.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel3.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel3.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel3.Name = "PanePanel3"
        Me.PanePanel3.Size = New System.Drawing.Size(936, 36)
        Me.PanePanel3.TabIndex = 89
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(10, 9)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(89, 16)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "File Uploader"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(16, 8)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(122, 18)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "File Uploader"
        '
        'PanePanel2
        '
        Me.PanePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel2.Controls.Add(Me.Btnupdate)
        Me.PanePanel2.Controls.Add(Me.btnclose)
        Me.PanePanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel2.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel2.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel2.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel2.Location = New System.Drawing.Point(0, 426)
        Me.PanePanel2.Name = "PanePanel2"
        Me.PanePanel2.Size = New System.Drawing.Size(938, 36)
        Me.PanePanel2.TabIndex = 90
        '
        'FrmAdmin_FileUploader
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(938, 462)
        Me.Controls.Add(Me.PanePanel2)
        Me.Controls.Add(Me.GroupBox8)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmAdmin_FileUploader"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ClickSoftware File Uploader"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.PanePanel3.ResumeLayout(False)
        Me.PanePanel3.PerformLayout()
        Me.PanePanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents txtfile As System.Windows.Forms.TextBox
    Friend WithEvents btngetfile As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents btnclose As System.Windows.Forms.Button
    Friend WithEvents Btnupdate As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkbasicpay As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtfileATM As System.Windows.Forms.TextBox
    Friend WithEvents btngetfileATM As System.Windows.Forms.Button
    Friend WithEvents BtnUploadATM As System.Windows.Forms.Button
    Friend WithEvents chkATM As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btngetfiletaxcode As System.Windows.Forms.Button
    Friend WithEvents txtfiletaxcode As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents chktaxcode As System.Windows.Forms.CheckBox
    Friend WithEvents btnuploadtaxcode As System.Windows.Forms.Button
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents btnUploadPosition As System.Windows.Forms.Button
    Friend WithEvents btngetPositionfile As System.Windows.Forms.Button
    Friend WithEvents txtfilePositionUpload As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ChkPosition As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents btnUploadDepartment As System.Windows.Forms.Button
    Friend WithEvents btngetDepartment As System.Windows.Forms.Button
    Friend WithEvents txtfiledepartmentUpload As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Chkdepartment As System.Windows.Forms.CheckBox
    Friend WithEvents btn7column As System.Windows.Forms.Button
    Friend WithEvents txtpath7column As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents btnUpload7Column As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents chk7column As System.Windows.Forms.CheckBox
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents BtnUploadTKClientID As System.Windows.Forms.Button
    Friend WithEvents btngetTKClientID As System.Windows.Forms.Button
    Friend WithEvents txtTKClientID As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents chkTKClientID As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents btnUploadResignedActive As System.Windows.Forms.Button
    Friend WithEvents btngetResignedActive As System.Windows.Forms.Button
    Friend WithEvents txtResignedActive As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents chkResignedActive As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents PanePanel3 As WindowsApplication2.PanePanel
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents PanePanel2 As WindowsApplication2.PanePanel

End Class
