Imports System.Data.SqlClient
Public Class frmAdmin_ChangePassword
    Inherits System.Windows.Forms.Form


    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        If Me.txtnewpassword.Text = Me.txtconfirmpassword.Text Then
            Call getoldpass()
        Else
            MessageBox.Show("Password Mismatch", "Mismatch", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)

        End If
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click

        Me.Close()

    End Sub
    Private Sub changepassword()

        Dim myadapter As New SqlDataAdapter
        Dim mydataset As New DataSet
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("USP_UPDATECHANGE_PASSWORD", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Clear()
        cmd.Parameters.Add("@EmployeeID", SqlDbType.Int).Value = employeeid
        cmd.Parameters.Add("@password", SqlDbType.VarChar, 50).Value = Me.txtconfirmpassword.Text
        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
    Private Sub cleartxt()
        txtoldpassword.Text = ""
        txtnewpassword.Text = ""
    End Sub
    Public Sub incrpyte_password()
        Dim PassInt As Integer
        Dim i As Integer


        PassInt = GetIntegerFromPassword(txtnewpassword.Text)
        lblUsername.Text = ""

        For i = 1 To Len(txtnewpassword.Text)

            lblUsername.Text = String.Concat(lblUsername.Text, CStr(Chr(CInt(Asc(Mid(txtnewpassword.Text, i, 1)) * PassInt / 2 / 3 / 4 / 5 / 6))))

        Next i
    End Sub
    Function GetIntegerFromPassword(ByVal Password As String) As Integer
        Dim i As Integer
        Dim Acumm As Integer
        'I'm going to pass across the entire String for to check wath chars it have
        For i = 1 To Len(Password)
            Acumm = Acumm + Asc(Mid(Password, i, 1))
        Next

        'So Acumm now is an Integer, equal to the password.
        'return it
        Return Acumm

    End Function

    Private Sub getoldpass()

        With Me

            '-----check if useraccount is valid-----'

            Dim myreader As SqlDataReader
            Dim myadapter As New SqlDataAdapter
            Dim mydataset As New DataSet
            Dim appRdr As New System.Configuration.AppSettingsReader
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("USP_GET_EMPLOYEEID", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Clear()
            cmd.Parameters.Add("@SelectedOption", SqlDbType.Int, ParameterDirection.Input).Value = "1"
            cmd.Parameters.Add("@EmployeeID", SqlDbType.Int, ParameterDirection.Input).Value = employeeid

            myconnection.sqlconn.Open()
            myreader = cmd.ExecuteReader()

            Try

                While myreader.Read()

                    dbpword = myreader.Item(0)

                End While

            Finally

                myreader.Close()
                myconnection.sqlconn.Close()

            End Try

            If .txtoldpassword.Text = dbpword Then
                Call changepassword()
                Me.Close()
            Else
                MessageBox.Show("Invalid Password", "Invalid Password", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            End If

        End With

    End Sub

    Private Sub LoginForm2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.lblname.Text = frmMain.username
        Me.lbldescription.Text = frmLogin.admin
    End Sub
End Class
