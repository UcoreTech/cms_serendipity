Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmAccountConfiguration

    Private gCon As New Clsappconfiguration()
    Private isWarned As Boolean = False
    Private cb As ComboBox

    Private Sub grdAccountConfig_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdAccountConfig.CellContentClick
        If grdAccountConfig.Columns(e.ColumnIndex).GetType().Name = "DataGridViewCheckBoxColumn" Then
            If e.RowIndex <> -1 Then
                If grdAccountConfig.Item(e.ColumnIndex, e.RowIndex).Value = True Then
                    For Each xRow As DataGridViewRow In grdAccountConfig.Rows
                        If grdAccountConfig.Item(e.ColumnIndex, xRow.Index).RowIndex <> e.RowIndex Then
                            grdAccountConfig.Item(e.ColumnIndex, xRow.Index).Value = False
                        End If
                    Next
                End If
            End If
        End If
    End Sub

    Private Sub grdAccountConfig_CellLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdAccountConfig.CellLeave
        If Not cb Is Nothing Then
            cb.SelectedIndex = cb.FindStringExact(cb.Text)
        End If
    End Sub

    Private Sub grdAccountConfig_CurrentCellDirtyStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grdAccountConfig.CurrentCellDirtyStateChanged
        If grdAccountConfig.IsCurrentCellDirty Then
            grdAccountConfig.CommitEdit(DataGridViewDataErrorContexts.Commit)
        End If
    End Sub

    Private Sub frmAccountConfiguration_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call LoadAccountConfiguration()
        'Me.TabControl1.DefaultBackColor = Color.White
    End Sub

    Private Sub LoadAccountConfiguration()
        'Add The ComboBox Column
        Dim colAccounts As New DataGridViewComboBoxColumn

        With colAccounts
            .Items.Clear()
            .FlatStyle = FlatStyle.Flat
            .HeaderText = "Account"
            .DataPropertyName = "acnt_id"
            .Name = "acnt_id"
            .DataSource = GetActiveAccounts().Tables(0)
            .DisplayMember = "Full Account Name"
            .ValueMember = "Account ID"
            .DropDownWidth = 300
            .Width = 300
            .AutoComplete = True
            .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            .AutoComplete = True
            .MaxDropDownItems = 10
            .Resizable = DataGridViewTriState.True
        End With
        grdAccountConfig.Columns.Add(colAccounts)

        'Load the Accounts Configurations
        With grdAccountConfig
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Admin_Accounts_Configuration_Load")
            .DataSource = ds.Tables(0)
            .Columns("pk_Account_Configuration").Visible = False
            .Columns("acnt_code").Visible = False
            .Columns("acnt_name").Visible = False
        End With
    End Sub

    Private Function GetActiveAccounts() As DataSet
        Dim ds As New DataSet

        Try
            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_Load")
        Catch ex As Exception
            Throw ex
        End Try

        Return ds
    End Function

    Private Sub grdAccountConfig_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles grdAccountConfig.DataError
        If isWarned = False Then
            MessageBox.Show("User Error! Please Configure the accounts properly." + vbNewLine + "The Account must have been moved, deleted or made inactive.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

        isWarned = True
        e.Cancel = True
    End Sub

    Private Sub grdAccountConfig_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles grdAccountConfig.EditingControlShowing
        If TypeOf e.Control Is ComboBox Then
            cb = e.Control
            cb.DropDownStyle = ComboBoxStyle.DropDown
            cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cb.AutoCompleteSource = AutoCompleteSource.ListItems
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Close()
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        If MessageBox.Show("Are you sure you want to change the settings?", "User Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Try
                Call UpdateAccountConfiguration()
                MessageBox.Show("The Accounts have been successfully configured.", "Account Configuration", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error! Account Configuration", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End If
    End Sub

    Private Sub UpdateAccountConfiguration()
        Try
            For Each xRow As DataGridViewRow In grdAccountConfig.Rows
                Dim configID As String = xRow.Cells("pk_Account_Configuration").Value.ToString()
                Dim accountID As String = xRow.Cells("acnt_id").Value.ToString()
                Dim isLoanOverpayment As Boolean = xRow.Cells("Loan Overpayment").Value.ToString()
                Dim isCapitalShare As Boolean = xRow.Cells("Capital Share").Value.ToString()
                Dim isBereavement As Boolean = xRow.Cells("Bereavement").Value.ToString()
                Dim isMembershipFee As Boolean = xRow.Cells("Membership Fee").Value.ToString()
                Dim isSavingsAccount As Boolean = xRow.Cells("Savings").Value.ToString()
                Dim isTimeDeposit As Boolean = xRow.Cells("Time Deposit").Value.ToString()
                Dim isOtherReceivables As Boolean = xRow.Cells("Other Receivables").Value.ToString()
                Dim isPayables As Boolean = xRow.Cells("Payable").Value.ToString()

                SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Admin_Accounts_Configuration_Update", _
                        New SqlParameter("@pk_Account_Configuration", configID), _
                        New SqlParameter("@fk_mAccount", accountID), _
                        New SqlParameter("@IsLoanOverpayment", isLoanOverpayment), _
                        New SqlParameter("@IsCapitalShare", isCapitalShare), _
                        New SqlParameter("@IsBereavement", isBereavement), _
                        New SqlParameter("@IsMembershipFee", isMembershipFee), _
                        New SqlParameter("@IsSavingsAccount", isSavingsAccount), _
                        New SqlParameter("@IsTimeDeposit", isTimeDeposit), _
                        New SqlParameter("@IsOtherReceivables", isOtherReceivables), _
                        New SqlParameter("@IsPayable", isPayables))
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class