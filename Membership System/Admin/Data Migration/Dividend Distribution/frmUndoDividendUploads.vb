Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmUndoDividendUploads
    Private gCon As New Clsappconfiguration()

    Private Sub frmUndoDividendUploads_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call LoadDividendHistory()
    End Sub

    Private Sub DeleteUploadToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteUploadToolStripMenuItem.Click
        Try
            Dim isARowSelected As Boolean = DeleteSelectedUploadHistory()
            Call LoadDividendHistory()

            If isARowSelected = True Then
                MessageBox.Show("Successfully deleted", "Undo Upload History", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub LoadDividendHistory()
        Try
            Dim ds As New DataSet

            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Admin_DividendUploadHistory")
            gridDividendHistory.DataSource = ds.Tables(0)
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Function DeleteSelectedUploadHistory() As Boolean
        Dim isARowSelected As Boolean = False

        Try
            If MessageBox.Show("Are you sure you want to delete all selected records?", "Delete History", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Dim isSelected As Boolean = False
                Dim uploadIDKey As String
                Dim distributionDate As Date
                Dim year As String

                For Each xRow As DataGridViewRow In gridDividendHistory.Rows
                    isSelected = xRow.Cells("Select").Value
                    distributionDate = xRow.Cells("Distribution Date").Value
                    year = xRow.Cells("Year").Value.ToString()

                    If isSelected = True Then
                        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Admin_DividendUndoUploadHistory", _
                                New SqlParameter("@pk_DividendDistribution", uploadIDKey))
                        isARowSelected = True
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try

        If isARowSelected = False Then
            MessageBox.Show("There are no rows selected. Please select one to delete", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

        Return isARowSelected
    End Function


    Private Sub DeleteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteToolStripMenuItem.Click
        Try
            Dim isARowSelected As Boolean = DeleteSelectedUploadHistory()
            Call LoadDividendHistory()

            If isARowSelected = True Then
                MessageBox.Show("Successfully deleted", "Undo Upload History", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ExitToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem1.Click
        Close()
    End Sub
End Class