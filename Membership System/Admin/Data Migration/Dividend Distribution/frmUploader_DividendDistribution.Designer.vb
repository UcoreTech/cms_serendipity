<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUploader_DividendDistribution
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUploader_DividendDistribution))
        Me.toolStripMain = New System.Windows.Forms.ToolStrip
        Me.grdDividendDistribution = New System.Windows.Forms.DataGridView
        Me.SaveFileDialog_Dividend = New System.Windows.Forms.SaveFileDialog
        Me.OpenFileDialog_Dividend = New System.Windows.Forms.OpenFileDialog
        Me.bgwDividend = New System.ComponentModel.BackgroundWorker
        Me.OpenFileDialog_ExportToExcel = New System.Windows.Forms.OpenFileDialog
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.toolBrowse = New System.Windows.Forms.ToolStripButton
        Me.toolUpload = New System.Windows.Forms.ToolStripButton
        Me.toolGenerateTemplate = New System.Windows.Forms.ToolStripButton
        Me.toolClearData = New System.Windows.Forms.ToolStripButton
        Me.ToolStripUndoUpload = New System.Windows.Forms.ToolStripButton
        Me.toolStripMain.SuspendLayout()
        CType(Me.grdDividendDistribution, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'toolStripMain
        '
        Me.toolStripMain.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.toolStripMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolBrowse, Me.toolUpload, Me.toolGenerateTemplate, Me.ToolStripSeparator1, Me.toolClearData, Me.ToolStripUndoUpload})
        Me.toolStripMain.Location = New System.Drawing.Point(0, 0)
        Me.toolStripMain.Name = "toolStripMain"
        Me.toolStripMain.Size = New System.Drawing.Size(849, 55)
        Me.toolStripMain.TabIndex = 0
        Me.toolStripMain.Text = "ToolStrip1"
        '
        'grdDividendDistribution
        '
        Me.grdDividendDistribution.AllowUserToAddRows = False
        Me.grdDividendDistribution.AllowUserToDeleteRows = False
        Me.grdDividendDistribution.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdDividendDistribution.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.grdDividendDistribution.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdDividendDistribution.DefaultCellStyle = DataGridViewCellStyle5
        Me.grdDividendDistribution.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdDividendDistribution.Location = New System.Drawing.Point(0, 55)
        Me.grdDividendDistribution.Name = "grdDividendDistribution"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdDividendDistribution.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.grdDividendDistribution.Size = New System.Drawing.Size(849, 440)
        Me.grdDividendDistribution.TabIndex = 1
        '
        'SaveFileDialog_Dividend
        '
        Me.SaveFileDialog_Dividend.Filter = "Excel 2007 file|*.xlsx|Excel 97-2003 file|*.xls"
        Me.SaveFileDialog_Dividend.ShowHelp = True
        Me.SaveFileDialog_Dividend.Title = "Save Uploader Template"
        '
        'OpenFileDialog_Dividend
        '
        '
        'bgwDividend
        '
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 55)
        '
        'toolBrowse
        '
        Me.toolBrowse.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolBrowse.Image = Global.WindowsApplication2.My.Resources.Resources.viewmag_
        Me.toolBrowse.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.toolBrowse.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolBrowse.Name = "toolBrowse"
        Me.toolBrowse.Size = New System.Drawing.Size(52, 52)
        Me.toolBrowse.Text = "Browse"
        '
        'toolUpload
        '
        Me.toolUpload.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolUpload.Image = Global.WindowsApplication2.My.Resources.Resources.db_comit
        Me.toolUpload.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.toolUpload.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolUpload.Name = "toolUpload"
        Me.toolUpload.Size = New System.Drawing.Size(52, 52)
        Me.toolUpload.Text = "Upload"
        '
        'toolGenerateTemplate
        '
        Me.toolGenerateTemplate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolGenerateTemplate.Image = Global.WindowsApplication2.My.Resources.Resources.lists
        Me.toolGenerateTemplate.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.toolGenerateTemplate.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolGenerateTemplate.Name = "toolGenerateTemplate"
        Me.toolGenerateTemplate.Size = New System.Drawing.Size(52, 52)
        Me.toolGenerateTemplate.Text = "Generate Template"
        '
        'toolClearData
        '
        Me.toolClearData.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolClearData.Image = Global.WindowsApplication2.My.Resources.Resources.trashcan_empty
        Me.toolClearData.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.toolClearData.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolClearData.Name = "toolClearData"
        Me.toolClearData.Size = New System.Drawing.Size(52, 52)
        Me.toolClearData.Text = "Clear Data"
        '
        'ToolStripUndoUpload
        '
        Me.ToolStripUndoUpload.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripUndoUpload.Image = Global.WindowsApplication2.My.Resources.Resources.undo_green
        Me.ToolStripUndoUpload.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripUndoUpload.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripUndoUpload.Name = "ToolStripUndoUpload"
        Me.ToolStripUndoUpload.Size = New System.Drawing.Size(52, 52)
        Me.ToolStripUndoUpload.Text = "Undo Uploads"
        '
        'frmUploader_DividendDistribution
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(849, 495)
        Me.Controls.Add(Me.grdDividendDistribution)
        Me.Controls.Add(Me.toolStripMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmUploader_DividendDistribution"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Dividend Distribution Uploader"
        Me.toolStripMain.ResumeLayout(False)
        Me.toolStripMain.PerformLayout()
        CType(Me.grdDividendDistribution, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents toolStripMain As System.Windows.Forms.ToolStrip
    Friend WithEvents toolUpload As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolGenerateTemplate As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolBrowse As System.Windows.Forms.ToolStripButton
    Friend WithEvents grdDividendDistribution As System.Windows.Forms.DataGridView
    Friend WithEvents SaveFileDialog_Dividend As System.Windows.Forms.SaveFileDialog
    Friend WithEvents OpenFileDialog_Dividend As System.Windows.Forms.OpenFileDialog
    Friend WithEvents toolClearData As System.Windows.Forms.ToolStripButton
    Friend WithEvents bgwDividend As System.ComponentModel.BackgroundWorker
    Friend WithEvents OpenFileDialog_ExportToExcel As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripUndoUpload As System.Windows.Forms.ToolStripButton
End Class
