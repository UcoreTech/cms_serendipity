Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMasterfile_PayrollCode
#Region "variable"
    Public PayID As String
#End Region
#Region "Propert"
    Public Property getpayid() As String
        Get
            Return PayID
        End Get
        Set(ByVal value As String)
            PayID = value
        End Set
    End Property
#End Region
    Private Sub frmPayrollCode_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call PayrollCode()

    End Sub
#Region "Payroll Code"
    Private Sub PayrollCode()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("CIMS_m_Member_PayrollCode_Select", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With Me.lvlPayrollcode
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            '.TopItem.Selected = True
            .Columns.Add("Payroll Code", 100, HorizontalAlignment.Left)
            .Columns.Add("Payroll Description", 300, HorizontalAlignment.Left)
            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .subitems.add(myreader.Item(1))
                        .subitems.add(myreader.Item(2)) 'jobid
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
            .TopItem.Focused = True
            .TopItem.Selected = True
        End With
    End Sub
#End Region
#Region "Clear text"
    Private Sub Cleartext()
        Me.txtcode.Text = ""
        Me.txtdescription.Text = ""
    End Sub
#End Region
#Region "Insert Payroll Code"
    Private Sub AddNewPayrollCode(ByVal code As String, ByVal desc As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_m_Member_PayrollCode_Insert", _
                                      New SqlParameter("@pk_PRollCode", code), _
                                      New SqlParameter("@PRollDesc", desc))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "AddNewPayrollCode")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Update Payroll Code"
    Private Sub EditPayrollCode(ByVal code As String, ByVal desc As String, ByVal payid As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_m_Member_PayrollCode_Update", _
                                      New SqlParameter("@pk_PRollCode", code), _
                                      New SqlParameter("@PRollDesc", desc), _
                                      New SqlParameter("@pk_PRollID", payid))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "AddNewPayrollCode")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Delete Payroll Code"
    Private Sub DeletePayrollCode(ByVal payid As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_m_Member_PayrollCode_Delete", _
                                      New SqlParameter("@pk_PRollID", payid))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show("Cannot delete Payroll Code. This Payroll Code is currently used by a member.", "DeletePayrollCode", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Payroll Validation"
    Private Sub PayrollValidation(ByVal code As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("CIMS_m_Member_PayrollCode_Validate", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@pk_PRollCode", SqlDbType.VarChar, 15).Value = code
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.HasRows Then
                MessageBox.Show("Code already exist, please try another one", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else
                Call AddNewPayrollCode(Me.txtcode.Text, Me.txtdescription.Text)
                Call PayrollCode()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Statusvalidation")
        End Try
    End Sub
#End Region
#Region "Update Validation"
    Private Sub ValidationUpdate(ByVal code As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("CIMS_m_Member_PayrollCode_Validate", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@pk_PRollCode", SqlDbType.VarChar, 15).Value = code
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            'If myreader.HasRows Then
            '    MessageBox.Show("Code already exist, please try another one", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            'Else
            '    Call EditPayrollCode(Me.txtcode.Text, Me.txtdescription.Text, PayID)
            '    Call PayrollCode()
            'End If

            Call EditPayrollCode(Me.txtcode.Text, Me.txtdescription.Text, PayID)
            Call PayrollCode()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Statusvalidation")
        End Try
    End Sub
#End Region

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            If btnsave.Text = "New" Then
                Me.txtcode.Focus()
                Label1.Text = "Add New Payroll Code"
                Me.txtcode.ReadOnly = False
                btndelete.Visible = False
                btnupdate.Visible = False
                btnsave.Text = "Save"
                btnsave.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(84, 6)

            ElseIf btnsave.Text = "Save" Then
                If Me.txtcode.Text <> "" And Me.txtdescription.Text <> "" Then
                    Label1.Text = "Payroll Code"
                    Call PayrollValidation(Me.txtcode.Text)
                    Call Cleartext()
                    Me.txtcode.Focus()
                    btnsave.Text = "New"
                    btnsave.Image = My.Resources.new3
                    btnclose.Text = "Close"
                    btnclose.Location = New System.Drawing.Point(248, 6)
                    btndelete.Visible = True
                    btnupdate.Visible = True
                Else
                    MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Cancel" Then
            Label1.Text = "Payroll Code"
            btnsave.Text = "New"
            btnsave.Image = My.Resources.new3
            btnupdate.Text = "Edit"
            btnupdate.Image = My.Resources.edit1
            btnclose.Text = "Close"
            btnclose.Location = New System.Drawing.Point(248, 6)
            btndelete.Visible = True
            btnupdate.Visible = True
            btnsave.Enabled = True
        ElseIf btnclose.Text = "Close" Then
            Me.Close()
        End If
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If btnupdate.Text = "Edit" Then
                Me.txtcode.Focus()
                Label1.Text = "Edit Payroll Code"
                Me.btnsave.Enabled = False
                'Me.txtcode.ReadOnly = True
                Me.txtcode.Text = Me.lvlPayrollcode.SelectedItems(0).Text
                Me.txtcode.Tag = Me.lvlPayrollcode.SelectedItems(0).Text
                Me.txtdescription.Text = Me.lvlPayrollcode.SelectedItems(0).SubItems(1).Text
                Me.txtdescription.Tag = Me.lvlPayrollcode.SelectedItems(0).SubItems(1).Text
                getpayid() = Me.lvlPayrollcode.SelectedItems(0).SubItems(2).Text
                btnupdate.Text = "Update"
                btnupdate.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(168, 6)
                btndelete.Visible = False

            ElseIf btnupdate.Text = "Update" Then
                If Me.txtcode.Text <> "" And Me.txtdescription.Text <> "" Then
                    Label1.Text = "Payroll Code"
                    Call ValidationUpdate(Me.txtcode.Text)
                    Call Cleartext()
                    Me.txtcode.Focus()
                    btndelete.Visible = True
                    btnupdate.Text = "Edit"
                    btnupdate.Image = My.Resources.edit1
                    btnclose.Text = "Cancel"
                    btnclose.Location = New System.Drawing.Point(230, 6)
                    btnclose.Text = "Close"
                    Me.btnsave.Enabled = True
                    Me.btndelete.Visible = True

                End If
            Else
                MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

        Catch ex As Exception
        End Try
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        getpayid() = Me.lvlPayrollcode.SelectedItems(0).SubItems(2).Text
        Dim x As New DialogResult
        x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
        If x = System.Windows.Forms.DialogResult.OK Then
            Call DeletePayrollCode(PayID)
            Call PayrollCode()
        ElseIf x = System.Windows.Forms.DialogResult.Cancel Then

        End If
    End Sub
End Class