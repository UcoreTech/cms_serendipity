﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmMasterfile_DebitCredit
    Private fxKeyAccount As String
    Dim mycon As New Clsappconfiguration
    Public fxid As String

    Private Sub frmMasterfile_DebitCredit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadComboAcctType()
        ListDebitCredit()
        GroupBox1.Enabled = False
    End Sub

    Private Sub LoadComboAcctType()
        cbotype.Items.Add("Debit")
        cbotype.Items.Add("Credit")
    End Sub

    Private Sub ControlSetup(ByVal mode As String)
        If mode = "New" Then
            btnupdate.Enabled = False
            btndelete.Enabled = False
            btnclose.Text = "Cancel"
            btnsave.Text = "Save"
            GroupBox1.Enabled = True
            txtaccountcode.Clear()
            txtaccountname.Clear()
            cbotype.Text = ""
            Exit Sub
        End If
        If mode = "Save" Then
            AddNewDebitCreditAccount(fxid, txtaccountcode.Text, txtaccountname.Text, cbotype.Text)
            btnupdate.Enabled = True
            btndelete.Enabled = True
            btnclose.Text = "Close"
            btnsave.Text = "New"
            GroupBox1.Enabled = False
            txtaccountcode.Clear()
            txtaccountname.Clear()
            cbotype.Text = ""
            ListDebitCredit()
            Exit Sub
        End If
        If mode = "Close" Then
            Me.Close()
        End If
        If mode = "Cancel" Then
            btnupdate.Enabled = True
            btndelete.Enabled = True
            btnclose.Text = "Close"
            btnsave.Text = "New"
            btnupdate.Text = "Edit"
            btnsave.Enabled = True
            GroupBox1.Enabled = False
            txtaccountcode.Clear()
            txtaccountname.Clear()
            cbotype.Text = ""
            Exit Sub
        End If
        If mode = "Edit" Then
            btnupdate.Text = "Update"
            btndelete.Enabled = False
            btnclose.Text = "Cancel"
            btnsave.Enabled = False
            GroupBox1.Enabled = True
            Exit Sub
        End If
        If mode = "Update" Then
            UpdateDebitCreditAccount(fxid, txtaccountcode.Text, txtaccountname.Text, cbotype.Text)
            btnsave.Enabled = True
            btndelete.Enabled = True
            btnclose.Text = "Close"
            btnsave.Text = "New"
            btnupdate.Text = "Edit"
            GroupBox1.Enabled = False
            txtaccountcode.Clear()
            txtaccountname.Clear()
            cbotype.Text = ""
            ListDebitCredit()
            Exit Sub
        End If
        If mode = "Delete" Then
            If MsgBox("Are you sure you want to permanently delete this record?", vbYesNo, "Confirmation") = vbYes Then
                Try
                    DeleteDebitCreditAccount(fxid)
                    MessageBox.Show("Record Succesfully Deleted!")
                Catch ex As Exception
                    MessageBox.Show("Error !" + ex.ToString)
                End Try
                DeleteDebitCreditAccount(fxid)
                txtaccountcode.Clear()
                txtaccountname.Clear()
                cbotype.Text = ""
                ListDebitCredit()
            Else
                Exit Sub
            End If
        End If
    End Sub
    Private Sub AddNewDebitCreditAccount(ByVal Key_Account As String,
                                         ByVal code As String,
                                         ByVal name As String,
                                         ByVal type As String)
        Dim MyGuid As New Guid(Key_Account)
        If txtaccountcode.Text <> "" And txtaccountname.Text <> "" Then
            Try
                SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_DebitCredit_Addnew",
                                          New SqlParameter("@fxKeyAccountID", MyGuid),
                                          New SqlParameter("@fcAccountCode", code),
                                          New SqlParameter("@fcAccountName", name),
                                          New SqlParameter("@fcAccountType", type))
                MessageBox.Show("Record Succesfully Added!")
            Catch ex As Exception
                MessageBox.Show("Error! " + ex.ToString)
            End Try
        End If
    End Sub

    Private Sub UpdateDebitCreditAccount(ByVal Key_Account As String,
                                         ByVal code As String,
                                         ByVal name As String,
                                         ByVal type As String)
        Dim MyGuid As New Guid(Key_Account)
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_DebitCredit_Edit",
                                      New SqlParameter("@fxKeyAccountID", MyGuid),
                                      New SqlParameter("@fcAccountCode", code),
                                      New SqlParameter("@fcAccountName", name),
                                      New SqlParameter("@fcAccountType", type))
            MessageBox.Show("Record Succesfully Updated!")
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString)
        End Try
    End Sub

    Private Sub DeleteDebitCreditAccount(ByVal Key_Account As String)
        Dim MyGuid As New Guid(Key_Account)
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_DebitCredit_Delete",
                                      New SqlParameter("@fxKeyAccountID", MyGuid))
            MessageBox.Show("Record Succesfully Deleted!")
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString)
        End Try
    End Sub

    Private Sub ListDebitCredit()
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(mycon.cnstring, "CIMS_DebitCredit_SelectAll")

        dgvlist.DataSource = ds.Tables(0)
        dgvlist.Columns(0).Visible = False
    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        If btnsave.Text = "New" Then
            ControlSetup("New")
        Else
            ControlSetup("Save")
        End If
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Close" Then
            ControlSetup("Close")
        Else
            ControlSetup("Cancel")
        End If
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        If btnupdate.Text = "Edit" Then
            fxid = dgvlist.SelectedRows(0).Cells(0).Value.ToString
            txtaccountcode.Text = dgvlist.SelectedRows(0).Cells(1).Value.ToString
            txtaccountname.Text = dgvlist.SelectedRows(0).Cells(2).Value.ToString
            cbotype.Text = dgvlist.SelectedRows(0).Cells(3).Value.ToString
            ControlSetup("Edit")
            Exit Sub
        End If
        If btnupdate.Text = "Update" Then
            ControlSetup("Update")
            Exit Sub
        End If
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        frmMasterfile_Accounts.GetActive = 1
        frmMasterfile_Accounts.ShowDialog()
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        ControlSetup("Delete")
    End Sub

End Class