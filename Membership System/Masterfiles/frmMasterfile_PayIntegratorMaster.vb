Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMasterfile_PayIntegratorMaster
#Region "Var"
    Private Loantypeid As String
    Private payrollid As String
    Private isloan As Boolean
    Private iscontribution As Boolean
    Private isbereavement As Boolean
#End Region
#Region "Property Var"
    Private Property getLoantypeid() As String
        Get
            Return Loantypeid
        End Get
        Set(ByVal value As String)
            Loantypeid = value
        End Set
    End Property
    Private Property getpayrollid() As String
        Get
            Return payrollid
        End Get
        Set(ByVal value As String)
            payrollid = value
        End Set
    End Property
    Private Property getisloan() As Boolean
        Get
            Return isloan
        End Get
        Set(ByVal value As Boolean)
            isloan = value
        End Set
    End Property
    Private Property getiscontribution() As Boolean
        Get
            Return iscontribution
        End Get
        Set(ByVal value As Boolean)
            iscontribution = value
        End Set
    End Property
    Private Property getisbereavement() As Boolean
        Get
            Return isbereavement
        End Get
        Set(ByVal value As Boolean)
            isbereavement = value
        End Set
    End Property
#End Region
#Region "Load LoanType"
    Private Sub LoanType()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_m_Loans_LoanType_PayrollIntegrator")
        Try
            While rd.Read
                Me.cboloantype.Items.Add(rd.Item(1))
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "LoanType")
        End Try

    End Sub
#End Region
#Region "AddEdit Payroll Integrator Master"
    Private Sub AddEditPayrollIntegMaster(ByVal loanid As String, ByVal transactionName As String, ByVal transactioncode As String, _
                                          ByVal returncode As String, ByVal isloan As Boolean, ByVal iscontribution As Boolean, _
                                          ByVal bereavement As Boolean, ByVal pk_PayrollMapping_ID As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_m_PayrollMappingz_AddEdit", _
                                      New SqlParameter("@fk_loanType", loanid), _
                                      New SqlParameter("@PayrollMappingName", transactionName), _
                                      New SqlParameter("@PayrollMappingID", transactioncode), _
                                      New SqlParameter("@fcReturnCode", returncode), _
                                      New SqlParameter("@fbIsLoan", isloan), _
                                      New SqlParameter("@fbIsContribution", iscontribution), _
                                      New SqlParameter("@fbIsBereavement", bereavement), _
                                      New SqlParameter("@pk_PayrollMapping_ID", pk_PayrollMapping_ID))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "AddEditPayrollIntegMaster")
        Finally
            getLoantypeid() = Nothing
        End Try
    End Sub
#End Region
#Region "Delete Payroll Integrator Master"
    Private Sub DeletePayrollIntegratorMaster(ByVal mappingid As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_m_PayrollMapping_Delete", _
                                      New SqlParameter("@pk_PayrollMapping_ID", mappingid))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "DeletePayrollIntegratorMaster")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "View Payroll Integrator Master"
    Private Sub ViewIntegratorMaster()
        Dim mycon As New Clsappconfiguration
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, CommandType.StoredProcedure, "CIMS_m_PayrollMapping_Select")

            With Me.listPayrollIntegratorMaster
                .Clear()
                .View = View.Details
                .GridLines = True
                .FullRowSelect = True
                .Columns.Add("Loan Type", 80, HorizontalAlignment.Left)
                .Columns.Add("Name", 120, HorizontalAlignment.Left)
                .Columns.Add("Transaction Code", 60, HorizontalAlignment.Left)
                .Columns.Add("Return Code", 60, HorizontalAlignment.Left)
                .Columns.Add("Loan", 40, HorizontalAlignment.Left)
                .Columns.Add("Contribution", 40, HorizontalAlignment.Left)
                .Columns.Add("Bereavement", 40, HorizontalAlignment.Left)
                Try
                    While rd.Read
                        With .Items.Add(rd.Item(0))
                            .SubItems.Add(rd.Item(1))
                            .SubItems.Add(rd.Item(2))
                            .SubItems.Add(rd.Item(3))
                            .SubItems.Add(rd.Item(4))
                            .SubItems.Add(rd.Item(5))
                            .SubItems.Add(rd.Item(6))
                            .SubItems.Add(rd.Item(7)) 'mapping id
                            .SubItems.Add(rd.Item(8)) 'loan id
                        End With
                    End While

                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Payroll Integrator Setup")
                End Try
            End With
        End Using

    End Sub
#End Region
#Region "getloan type id"
    Private Sub GetloanID(ByVal loanid As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_m_Loans_LoanType_getid", _
                                  New SqlParameter("@fcLoanTypeName", loanid))
        Try
            While rd.Read
                getLoantypeid() = rd.Item(0)
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "GetloanID")
        End Try
    End Sub
#End Region
#Region "Enabled/Disabled control"
    Private Sub Enabledcontrol()
        Me.cboloantype.Enabled = True
        Me.txtTransname.Enabled = True
        Me.txtTranscode.Enabled = True
        Me.txtReturncode.Enabled = True
        Me.chkloan.Enabled = True
        Me.chkcontribution.Enabled = True
        Me.chkbereavement.Enabled = True
    End Sub
    Private Sub DisabledControl()
        Me.cboloantype.Enabled = False
        Me.txtTransname.Enabled = False
        Me.txtTranscode.Enabled = False
        Me.txtReturncode.Enabled = False
        Me.chkloan.Enabled = False
        Me.chkcontribution.Enabled = False
        Me.chkbereavement.Enabled = False
    End Sub
#End Region
#Region "Cleartext"
    Private Sub Cleartext()
        Me.cboloantype.Text = ""
        Me.txtTransname.Text = ""
        Me.txtTranscode.Text = ""
        Me.txtReturncode.Text = ""
        Me.chkloan.Checked = False
        Me.chkcontribution.Checked = False
        Me.chkbereavement.Checked = False
    End Sub
#End Region
    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            If btnsave.Text = "New" Then
                Call Enabledcontrol()
                Call Cleartext()
                btndelete.Visible = False
                btnupdate.Visible = False
                btnsave.Text = "Save"
                btnsave.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(74, 6)

            ElseIf btnsave.Text = "Save" Then
                If Me.txtTransname.Text <> "" And Me.txtTranscode.Text <> "" And Me.txtReturncode.Text <> "" Then
                    Call AddEditPayrollIntegMaster(Loantypeid, Me.txtTransname.Text, Me.txtTranscode.Text, Me.txtReturncode.Text, isloan, iscontribution, isbereavement, "")
                    Call ViewIntegratorMaster()
                    Call DisabledControl()
                    Call Cleartext()
                    btnsave.Text = "New"
                    btnsave.Image = My.Resources.new3
                    btnclose.Text = "Close"
                    btnclose.Location = New System.Drawing.Point(218, 6)
                    btndelete.Visible = True
                    btnupdate.Visible = True
                Else
                    MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If btnupdate.Text = "Edit" Then
                Call Enabledcontrol()
                Me.cboloantype.Text = Me.listPayrollIntegratorMaster.SelectedItems(0).Text
                Me.txtTransname.Text = Me.listPayrollIntegratorMaster.SelectedItems(0).SubItems(1).Text
                Me.txtTranscode.Text = Me.listPayrollIntegratorMaster.SelectedItems(0).SubItems(2).Text
                Me.txtReturncode.Text = Me.listPayrollIntegratorMaster.SelectedItems(0).SubItems(3).Text
                Me.chkloan.Checked = Me.listPayrollIntegratorMaster.SelectedItems(0).SubItems(4).Text
                Me.chkcontribution.Checked = Me.listPayrollIntegratorMaster.SelectedItems(0).SubItems(5).Text
                Me.chkbereavement.Checked = Me.listPayrollIntegratorMaster.SelectedItems(0).SubItems(6).Text
                getpayrollid() = Me.listPayrollIntegratorMaster.SelectedItems(0).SubItems(7).Text
                getLoantypeid() = Me.listPayrollIntegratorMaster.SelectedItems(0).SubItems(8).Text
                Me.btnsave.Enabled = False
                btnupdate.Text = "Update"
                btnupdate.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(141, 6)
                btndelete.Visible = False

            ElseIf btnupdate.Text = "Update" Then
                If Me.cboloantype.Text <> "" And Me.txtTransname.Text <> "" And Me.txtTranscode.Text <> "" And Me.txtReturncode.Text <> "" Then
                    Call AddEditPayrollIntegMaster(Loantypeid, Me.txtTransname.Text, Me.txtTranscode.Text, Me.txtReturncode.Text, isloan, iscontribution, isbereavement, payrollid)
                    Call ViewIntegratorMaster()
                    Call DisabledControl()
                    Call Cleartext()
                    btndelete.Visible = True
                    btnupdate.Text = "Edit"
                    btnupdate.Image = My.Resources.edit1
                    btnclose.Text = "Cancel"
                    btnclose.Location = New System.Drawing.Point(218, 6)
                    btnclose.Text = "Close"
                    Me.btnsave.Enabled = True
                    Me.btndelete.Visible = True

                Else
                    MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If

        Catch ex As Exception
        End Try
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Try
            getpayrollid() = Me.listPayrollIntegratorMaster.SelectedItems(0).SubItems(7).Text
            Dim x As New DialogResult
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
            If x = System.Windows.Forms.DialogResult.OK Then
                Call DeletePayrollIntegratorMaster(payrollid)
                Call ViewIntegratorMaster()
                Call DisabledControl()
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Cancel" Then
            Call DisabledControl()
            Call Cleartext()
            btnsave.Text = "New"
            btnsave.Image = My.Resources.new3
            btnupdate.Text = "Edit"
            btnupdate.Image = My.Resources.edit1
            btnclose.Text = "Close"
            btnclose.Location = New System.Drawing.Point(218, 6)
            btndelete.Visible = True
            btnupdate.Visible = True
            btnsave.Enabled = True
        ElseIf btnclose.Text = "Close" Then
            Me.Close()
        End If
    End Sub

    Private Sub frmPayIntegratorMaster_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call LoanType()
        Call ViewIntegratorMaster()
        Call DisabledControl()
    End Sub

    Private Sub cboloantype_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboloantype.TextChanged
        Call GetloanID(Me.cboloantype.Text)
    End Sub

    Private Sub chkloan_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkloan.CheckedChanged
        If Me.chkloan.Checked = True Then
            getisloan() = True
            Me.chkcontribution.Checked = False
            Me.chkbereavement.Checked = False

        Else
            getisloan() = False
        End If
    End Sub

    Private Sub chkcontribution_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkcontribution.CheckedChanged
        If Me.chkcontribution.Checked = True Then
            getiscontribution() = True
            Me.chkloan.Checked = False
            Me.chkbereavement.Checked = False
        Else
            getiscontribution() = False
        End If
    End Sub

    Private Sub chkbereavement_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkbereavement.CheckedChanged
        If Me.chkbereavement.Checked = True Then
            getisbereavement() = True
            Me.chkloan.Checked = False
            Me.chkcontribution.Checked = False
        Else
            getisbereavement() = False
        End If
    End Sub


End Class