<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOrgChart_Edit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOrgChart_Edit))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtDesc = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCode = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmbParentDesc = New MTGCComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmbLevel = New MTGCComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PanePanel2 = New WindowsApplication2.PanePanel()
        Me.btnupdate = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.lblLAbel = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.PanePanel2.SuspendLayout()
        Me.PanePanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtDesc)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtCode)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.cmbParentDesc)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.cmbLevel)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(11, 52)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(366, 121)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'txtDesc
        '
        Me.txtDesc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDesc.Location = New System.Drawing.Point(123, 94)
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.Size = New System.Drawing.Size(228, 20)
        Me.txtDesc.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(20, 101)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Description:"
        '
        'txtCode
        '
        Me.txtCode.Location = New System.Drawing.Point(123, 67)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(228, 20)
        Me.txtCode.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(19, 75)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Code:"
        '
        'cmbParentDesc
        '
        Me.cmbParentDesc.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cmbParentDesc.ArrowColor = System.Drawing.Color.Black
        Me.cmbParentDesc.BindedControl = CType(resources.GetObject("cmbParentDesc.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.cmbParentDesc.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.cmbParentDesc.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.cmbParentDesc.ColumnNum = 3
        Me.cmbParentDesc.ColumnWidth = "28;60;0"
        Me.cmbParentDesc.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cmbParentDesc.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.cmbParentDesc.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.cmbParentDesc.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.cmbParentDesc.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.cmbParentDesc.DisplayMember = "Text"
        Me.cmbParentDesc.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbParentDesc.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.cmbParentDesc.DropDownForeColor = System.Drawing.Color.Black
        Me.cmbParentDesc.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDownList
        Me.cmbParentDesc.DropDownWidth = 340
        Me.cmbParentDesc.Enabled = False
        Me.cmbParentDesc.GridLineColor = System.Drawing.Color.RoyalBlue
        Me.cmbParentDesc.GridLineHorizontal = True
        Me.cmbParentDesc.GridLineVertical = True
        Me.cmbParentDesc.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.cmbParentDesc.Location = New System.Drawing.Point(123, 40)
        Me.cmbParentDesc.ManagingFastMouseMoving = True
        Me.cmbParentDesc.ManagingFastMouseMovingInterval = 30
        Me.cmbParentDesc.Name = "cmbParentDesc"
        Me.cmbParentDesc.SelectedItem = Nothing
        Me.cmbParentDesc.SelectedValue = Nothing
        Me.cmbParentDesc.Size = New System.Drawing.Size(228, 21)
        Me.cmbParentDesc.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(19, 47)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(97, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Parent Description:"
        '
        'cmbLevel
        '
        Me.cmbLevel.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cmbLevel.ArrowColor = System.Drawing.Color.Black
        Me.cmbLevel.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbLevel.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbLevel.BindedControl = CType(resources.GetObject("cmbLevel.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.cmbLevel.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.cmbLevel.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.cmbLevel.ColumnNum = 3
        Me.cmbLevel.ColumnWidth = "40;80;140"
        Me.cmbLevel.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cmbLevel.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.cmbLevel.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.cmbLevel.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.cmbLevel.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.cmbLevel.DisplayMember = "Text"
        Me.cmbLevel.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbLevel.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.cmbLevel.DropDownForeColor = System.Drawing.Color.Black
        Me.cmbLevel.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDownList
        Me.cmbLevel.DropDownWidth = 340
        Me.cmbLevel.GridLineColor = System.Drawing.Color.RoyalBlue
        Me.cmbLevel.GridLineHorizontal = True
        Me.cmbLevel.GridLineVertical = True
        Me.cmbLevel.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.cmbLevel.Location = New System.Drawing.Point(123, 12)
        Me.cmbLevel.ManagingFastMouseMoving = True
        Me.cmbLevel.ManagingFastMouseMovingInterval = 30
        Me.cmbLevel.Name = "cmbLevel"
        Me.cmbLevel.SelectedItem = Nothing
        Me.cmbLevel.SelectedValue = Nothing
        Me.cmbLevel.Size = New System.Drawing.Size(228, 21)
        Me.cmbLevel.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(19, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(36, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Level:"
        '
        'PanePanel2
        '
        Me.PanePanel2.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel2.Controls.Add(Me.btnupdate)
        Me.PanePanel2.Controls.Add(Me.btnClose)
        Me.PanePanel2.Controls.Add(Me.btnSave)
        Me.PanePanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel2.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel2.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel2.Location = New System.Drawing.Point(0, 181)
        Me.PanePanel2.Name = "PanePanel2"
        Me.PanePanel2.Size = New System.Drawing.Size(389, 42)
        Me.PanePanel2.TabIndex = 8
        '
        'btnupdate
        '
        Me.btnupdate.Location = New System.Drawing.Point(238, 5)
        Me.btnupdate.Name = "btnupdate"
        Me.btnupdate.Size = New System.Drawing.Size(66, 28)
        Me.btnupdate.TabIndex = 7
        Me.btnupdate.Text = "Update"
        Me.btnupdate.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(310, 5)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(66, 28)
        Me.btnClose.TabIndex = 6
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(166, 5)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(66, 28)
        Me.btnSave.TabIndex = 5
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'PanePanel1
        '
        Me.PanePanel1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.lblLAbel)
        Me.PanePanel1.Controls.Add(Me.Label5)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel1.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(389, 40)
        Me.PanePanel1.TabIndex = 7
        '
        'lblLAbel
        '
        Me.lblLAbel.BackColor = System.Drawing.Color.Transparent
        Me.lblLAbel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLAbel.ForeColor = System.Drawing.Color.White
        Me.lblLAbel.Location = New System.Drawing.Point(29, 8)
        Me.lblLAbel.Name = "lblLAbel"
        Me.lblLAbel.Size = New System.Drawing.Size(255, 20)
        Me.lblLAbel.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(11, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(16, 20)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "*"
        Me.Label5.Visible = False
        '
        'frmOrgChart_Edit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(389, 223)
        Me.Controls.Add(Me.PanePanel2)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmOrgChart_Edit"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = " "
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.PanePanel2.ResumeLayout(False)
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblLAbel As System.Windows.Forms.Label
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbLevel As MTGCComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbParentDesc As MTGCComboBox
    Friend WithEvents txtDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtCode As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents PanePanel2 As WindowsApplication2.PanePanel
    Friend WithEvents btnupdate As System.Windows.Forms.Button
End Class
