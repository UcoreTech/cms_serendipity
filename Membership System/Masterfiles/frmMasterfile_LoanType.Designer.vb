<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMasterfile_LoanType
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMasterfile_LoanType))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtcode = New System.Windows.Forms.TextBox()
        Me.txtTotalAllowable = New System.Windows.Forms.TextBox()
        Me.txtdescription = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtRefund = New System.Windows.Forms.TextBox()
        Me.btnPolicies = New System.Windows.Forms.Button()
        Me.btnApprover = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtpenaltyperc = New System.Windows.Forms.TextBox()
        Me.cboPoliciesCode = New System.Windows.Forms.ComboBox()
        Me.txtCreditCommittee = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtboardmem = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.BtnRequirements = New System.Windows.Forms.Button()
        Me.btnloanTerms = New System.Windows.Forms.Button()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.btnComaker = New System.Windows.Forms.Button()
        Me.cboIntType = New System.Windows.Forms.ComboBox()
        Me.cboInterestType = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtServiceFeeAmt = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtServiceFeePer = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtInterest = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.btnPriority = New System.Windows.Forms.Button()
        Me.txtPercentAllowedToReloan = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txtDaysValid = New System.Windows.Forms.TextBox()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnRemove = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.GrdAccounts = New System.Windows.Forms.DataGridView()
        Me.Per = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Debit = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.InPayment = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cboProject = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtLoanCode = New System.Windows.Forms.TextBox()
        Me.txtName = New System.Windows.Forms.ComboBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.dgvLoanTypes = New System.Windows.Forms.DataGridView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnclose = New System.Windows.Forms.Button()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PanePanel2 = New WindowsApplication2.PanePanel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.PanePanel3 = New WindowsApplication2.PanePanel()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.PK_Account = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Account = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Percent = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Amount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PanePanel4 = New WindowsApplication2.PanePanel()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.PanePanel5 = New WindowsApplication2.PanePanel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtAcctContriCode = New System.Windows.Forms.TextBox()
        Me.btnBrowse = New System.Windows.Forms.Button()
        Me.txtAccountContribution = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.btnsave = New System.Windows.Forms.Button()
        Me.btndelete = New System.Windows.Forms.Button()
        Me.btnupdate = New System.Windows.Forms.Button()
        Me.picStep1 = New WindowsApplication2.PanePanel()
        Me.lblStepone = New System.Windows.Forms.Label()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.GrdAccounts, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvLoanTypes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanePanel2.SuspendLayout()
        Me.PanePanel3.SuspendLayout()
        Me.PanePanel4.SuspendLayout()
        Me.PanePanel5.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.PanePanel1.SuspendLayout()
        Me.picStep1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 13)
        Me.Label1.TabIndex = 31
        Me.Label1.Text = "Account Code"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(28, 36)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 13)
        Me.Label2.TabIndex = 32
        Me.Label2.Text = "Loan Type"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(239, 48)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(96, 13)
        Me.Label6.TabIndex = 36
        Me.Label6.Text = "Total Allowable (%)"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(377, 34)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(60, 13)
        Me.Label9.TabIndex = 39
        Me.Label9.Text = "Description"
        '
        'txtcode
        '
        Me.txtcode.Location = New System.Drawing.Point(53, 6)
        Me.txtcode.Name = "txtcode"
        Me.txtcode.ReadOnly = True
        Me.txtcode.Size = New System.Drawing.Size(151, 20)
        Me.txtcode.TabIndex = 40
        Me.txtcode.Visible = False
        '
        'txtTotalAllowable
        '
        Me.txtTotalAllowable.Location = New System.Drawing.Point(341, 45)
        Me.txtTotalAllowable.Name = "txtTotalAllowable"
        Me.txtTotalAllowable.Size = New System.Drawing.Size(95, 20)
        Me.txtTotalAllowable.TabIndex = 45
        Me.txtTotalAllowable.Text = "0"
        '
        'txtdescription
        '
        Me.txtdescription.Location = New System.Drawing.Point(442, 6)
        Me.txtdescription.Multiline = True
        Me.txtdescription.Name = "txtdescription"
        Me.txtdescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtdescription.Size = New System.Drawing.Size(523, 43)
        Me.txtdescription.TabIndex = 47
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(16, 71)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(81, 13)
        Me.Label16.TabIndex = 55
        Me.Label16.Text = "Pat. Refund (%)"
        '
        'txtRefund
        '
        Me.txtRefund.Location = New System.Drawing.Point(103, 68)
        Me.txtRefund.Name = "txtRefund"
        Me.txtRefund.Size = New System.Drawing.Size(95, 20)
        Me.txtRefund.TabIndex = 62
        Me.txtRefund.Text = "0"
        '
        'btnPolicies
        '
        Me.btnPolicies.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPolicies.Location = New System.Drawing.Point(199, 101)
        Me.btnPolicies.Name = "btnPolicies"
        Me.btnPolicies.Size = New System.Drawing.Size(27, 21)
        Me.btnPolicies.TabIndex = 78
        Me.btnPolicies.Text = "..."
        Me.btnPolicies.UseVisualStyleBackColor = True
        '
        'btnApprover
        '
        Me.btnApprover.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApprover.Location = New System.Drawing.Point(136, 223)
        Me.btnApprover.Name = "btnApprover"
        Me.btnApprover.Size = New System.Drawing.Size(113, 25)
        Me.btnApprover.TabIndex = 79
        Me.btnApprover.Text = "Approver"
        Me.btnApprover.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(38, 46)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(59, 13)
        Me.Label7.TabIndex = 80
        Me.Label7.Text = "Penalty (%)"
        '
        'txtpenaltyperc
        '
        Me.txtpenaltyperc.Location = New System.Drawing.Point(103, 43)
        Me.txtpenaltyperc.Name = "txtpenaltyperc"
        Me.txtpenaltyperc.Size = New System.Drawing.Size(95, 20)
        Me.txtpenaltyperc.TabIndex = 81
        Me.txtpenaltyperc.Text = "0"
        '
        'cboPoliciesCode
        '
        Me.cboPoliciesCode.FormattingEnabled = True
        Me.cboPoliciesCode.Location = New System.Drawing.Point(103, 101)
        Me.cboPoliciesCode.Name = "cboPoliciesCode"
        Me.cboPoliciesCode.Size = New System.Drawing.Size(95, 21)
        Me.cboPoliciesCode.TabIndex = 82
        '
        'txtCreditCommittee
        '
        Me.txtCreditCommittee.Location = New System.Drawing.Point(125, 41)
        Me.txtCreditCommittee.Name = "txtCreditCommittee"
        Me.txtCreditCommittee.Size = New System.Drawing.Size(43, 20)
        Me.txtCreditCommittee.TabIndex = 3
        Me.txtCreditCommittee.Text = "0"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(30, 44)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(86, 13)
        Me.Label18.TabIndex = 2
        Me.Label18.Text = "Credit Committee"
        '
        'txtboardmem
        '
        Me.txtboardmem.Location = New System.Drawing.Point(274, 40)
        Me.txtboardmem.MaxLength = 4
        Me.txtboardmem.Name = "txtboardmem"
        Me.txtboardmem.Size = New System.Drawing.Size(43, 20)
        Me.txtboardmem.TabIndex = 1
        Me.txtboardmem.Text = "0"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(192, 44)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(76, 13)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Board Member"
        '
        'BtnRequirements
        '
        Me.BtnRequirements.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnRequirements.Location = New System.Drawing.Point(255, 223)
        Me.BtnRequirements.Name = "BtnRequirements"
        Me.BtnRequirements.Size = New System.Drawing.Size(106, 25)
        Me.BtnRequirements.TabIndex = 93
        Me.BtnRequirements.Text = "Requirements"
        Me.BtnRequirements.UseVisualStyleBackColor = True
        '
        'btnloanTerms
        '
        Me.btnloanTerms.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnloanTerms.Location = New System.Drawing.Point(367, 223)
        Me.btnloanTerms.Name = "btnloanTerms"
        Me.btnloanTerms.Size = New System.Drawing.Size(64, 25)
        Me.btnloanTerms.TabIndex = 92
        Me.btnloanTerms.Text = "Terms"
        Me.btnloanTerms.UseVisualStyleBackColor = True
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(60, 103)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(35, 13)
        Me.Label20.TabIndex = 82
        Me.Label20.Text = "Policy"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 81)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnComaker)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cboIntType)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cboInterestType)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label8)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtServiceFeeAmt)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtServiceFeePer)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtInterest)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label27)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnPriority)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtPercentAllowedToReloan)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label26)
        Me.SplitContainer1.Panel1.Controls.Add(Me.BtnRequirements)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtDaysValid)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnloanTerms)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnPolicies)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label7)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtpenaltyperc)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtTotalAllowable)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cboPoliciesCode)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnApprover)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label6)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label16)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label20)
        Me.SplitContainer1.Panel1.Controls.Add(Me.PanePanel2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtRefund)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnClear)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnRemove)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnAdd)
        Me.SplitContainer1.Panel2.Controls.Add(Me.PanePanel3)
        Me.SplitContainer1.Panel2.Controls.Add(Me.GrdAccounts)
        Me.SplitContainer1.Panel2.Controls.Add(Me.PanePanel4)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtCreditCommittee)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label18)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtboardmem)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label17)
        Me.SplitContainer1.Size = New System.Drawing.Size(969, 307)
        Me.SplitContainer1.SplitterDistance = 453
        Me.SplitContainer1.TabIndex = 92
        '
        'btnComaker
        '
        Me.btnComaker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnComaker.Location = New System.Drawing.Point(248, 254)
        Me.btnComaker.Name = "btnComaker"
        Me.btnComaker.Size = New System.Drawing.Size(183, 23)
        Me.btnComaker.TabIndex = 110
        Me.btnComaker.Text = "Co-Maker Settings"
        Me.btnComaker.UseVisualStyleBackColor = True
        '
        'cboIntType
        '
        Me.cboIntType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIntType.FormattingEnabled = True
        Me.cboIntType.Items.AddRange(New Object() {"PER MONTH", "PER YEAR"})
        Me.cboIntType.Location = New System.Drawing.Point(102, 128)
        Me.cboIntType.Name = "cboIntType"
        Me.cboIntType.Size = New System.Drawing.Size(102, 21)
        Me.cboIntType.TabIndex = 109
        '
        'cboInterestType
        '
        Me.cboInterestType.FormattingEnabled = True
        Me.cboInterestType.Items.AddRange(New Object() {"DAILY", "WEEKLY", "SEMI-MONTHLY", "MONTHLY", "ANNUALLY", "SEMI-ANNUALLY", "QUARTERLY", "LUMP SUM"})
        Me.cboInterestType.Location = New System.Drawing.Point(98, 196)
        Me.cboInterestType.Name = "cboInterestType"
        Me.cboInterestType.Size = New System.Drawing.Size(123, 21)
        Me.cboInterestType.TabIndex = 108
        Me.cboInterestType.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(233, 156)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(109, 13)
        Me.Label8.TabIndex = 106
        Me.Label8.Text = "Service Fee (Amount)"
        '
        'txtServiceFeeAmt
        '
        Me.txtServiceFeeAmt.Location = New System.Drawing.Point(341, 151)
        Me.txtServiceFeeAmt.Name = "txtServiceFeeAmt"
        Me.txtServiceFeeAmt.Size = New System.Drawing.Size(95, 20)
        Me.txtServiceFeeAmt.TabIndex = 107
        Me.txtServiceFeeAmt.Text = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(256, 132)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(81, 13)
        Me.Label5.TabIndex = 104
        Me.Label5.Text = "Service Fee (%)"
        '
        'txtServiceFeePer
        '
        Me.txtServiceFeePer.Location = New System.Drawing.Point(341, 125)
        Me.txtServiceFeePer.Name = "txtServiceFeePer"
        Me.txtServiceFeePer.Size = New System.Drawing.Size(95, 20)
        Me.txtServiceFeePer.TabIndex = 105
        Me.txtServiceFeePer.Text = "0"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(28, 136)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(69, 13)
        Me.Label4.TabIndex = 102
        Me.Label4.Text = "Interest Type"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(38, 163)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 13)
        Me.Label3.TabIndex = 100
        Me.Label3.Text = "Interest (%)"
        '
        'txtInterest
        '
        Me.txtInterest.Location = New System.Drawing.Point(103, 156)
        Me.txtInterest.Name = "txtInterest"
        Me.txtInterest.Size = New System.Drawing.Size(39, 20)
        Me.txtInterest.TabIndex = 101
        Me.txtInterest.Text = "0"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(256, 75)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(79, 13)
        Me.Label27.TabIndex = 99
        Me.Label27.Text = "Restructure (%)"
        '
        'btnPriority
        '
        Me.btnPriority.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.btnPriority.Location = New System.Drawing.Point(36, 223)
        Me.btnPriority.Name = "btnPriority"
        Me.btnPriority.Size = New System.Drawing.Size(94, 25)
        Me.btnPriority.TabIndex = 97
        Me.btnPriority.Text = "Priority"
        Me.btnPriority.UseVisualStyleBackColor = True
        '
        'txtPercentAllowedToReloan
        '
        Me.txtPercentAllowedToReloan.Location = New System.Drawing.Point(341, 72)
        Me.txtPercentAllowedToReloan.Name = "txtPercentAllowedToReloan"
        Me.txtPercentAllowedToReloan.Size = New System.Drawing.Size(95, 20)
        Me.txtPercentAllowedToReloan.TabIndex = 98
        Me.txtPercentAllowedToReloan.Text = "0"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(278, 101)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(57, 13)
        Me.Label26.TabIndex = 96
        Me.Label26.Text = "Days Valid"
        '
        'txtDaysValid
        '
        Me.txtDaysValid.Location = New System.Drawing.Point(342, 98)
        Me.txtDaysValid.Name = "txtDaysValid"
        Me.txtDaysValid.Size = New System.Drawing.Size(95, 20)
        Me.txtDaysValid.TabIndex = 95
        Me.txtDaysValid.Text = "0"
        '
        'btnClear
        '
        Me.btnClear.Enabled = False
        Me.btnClear.Location = New System.Drawing.Point(432, 281)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(75, 23)
        Me.btnClear.TabIndex = 100
        Me.btnClear.Text = "Remove All"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnRemove
        '
        Me.btnRemove.Enabled = False
        Me.btnRemove.Location = New System.Drawing.Point(351, 281)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(75, 23)
        Me.btnRemove.TabIndex = 99
        Me.btnRemove.Text = "Remove"
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Enabled = False
        Me.btnAdd.Location = New System.Drawing.Point(270, 281)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(75, 23)
        Me.btnAdd.TabIndex = 98
        Me.btnAdd.Text = "Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'GrdAccounts
        '
        Me.GrdAccounts.AllowUserToAddRows = False
        Me.GrdAccounts.AllowUserToDeleteRows = False
        Me.GrdAccounts.AllowUserToResizeRows = False
        Me.GrdAccounts.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GrdAccounts.BackgroundColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GrdAccounts.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.GrdAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GrdAccounts.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PK_Account, Me.Account, Me.AC, Me.Per, Me.Percent, Me.Amount, Me.Debit, Me.InPayment})
        Me.GrdAccounts.GridColor = System.Drawing.Color.Black
        Me.GrdAccounts.Location = New System.Drawing.Point(4, 101)
        Me.GrdAccounts.MultiSelect = False
        Me.GrdAccounts.Name = "GrdAccounts"
        Me.GrdAccounts.ReadOnly = True
        Me.GrdAccounts.RowHeadersVisible = False
        Me.GrdAccounts.RowHeadersWidth = 25
        Me.GrdAccounts.RowTemplate.DefaultCellStyle.NullValue = Nothing
        Me.GrdAccounts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GrdAccounts.Size = New System.Drawing.Size(504, 177)
        Me.GrdAccounts.TabIndex = 97
        '
        'Per
        '
        Me.Per.FalseValue = "0"
        Me.Per.HeaderText = "Percentage"
        Me.Per.Name = "Per"
        Me.Per.ReadOnly = True
        Me.Per.TrueValue = "1"
        '
        'Debit
        '
        Me.Debit.FalseValue = "0"
        Me.Debit.HeaderText = "Debit"
        Me.Debit.Name = "Debit"
        Me.Debit.ReadOnly = True
        Me.Debit.TrueValue = "1"
        '
        'InPayment
        '
        Me.InPayment.FalseValue = "0"
        Me.InPayment.HeaderText = "In Payment"
        Me.InPayment.Name = "InPayment"
        Me.InPayment.ReadOnly = True
        Me.InPayment.TrueValue = "1"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cboProject)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.txtLoanCode)
        Me.Panel1.Controls.Add(Me.txtName)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.txtcode)
        Me.Panel1.Controls.Add(Me.txtdescription)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 26)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(969, 55)
        Me.Panel1.TabIndex = 93
        '
        'cboProject
        '
        Me.cboProject.FormattingEnabled = True
        Me.cboProject.Location = New System.Drawing.Point(210, 6)
        Me.cboProject.Name = "cboProject"
        Me.cboProject.Size = New System.Drawing.Size(227, 21)
        Me.cboProject.TabIndex = 51
        Me.cboProject.Text = "Select Project"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(169, 9)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(40, 13)
        Me.Label12.TabIndex = 50
        Me.Label12.Text = "Project"
        '
        'txtLoanCode
        '
        Me.txtLoanCode.Location = New System.Drawing.Point(91, 6)
        Me.txtLoanCode.Name = "txtLoanCode"
        Me.txtLoanCode.ReadOnly = True
        Me.txtLoanCode.Size = New System.Drawing.Size(68, 20)
        Me.txtLoanCode.TabIndex = 49
        '
        'txtName
        '
        Me.txtName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.FormattingEnabled = True
        Me.txtName.Location = New System.Drawing.Point(91, 29)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(280, 23)
        Me.txtName.TabIndex = 48
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.dgvLoanTypes)
        Me.Panel2.Controls.Add(Me.PanePanel5)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 388)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(969, 161)
        Me.Panel2.TabIndex = 94
        '
        'dgvLoanTypes
        '
        Me.dgvLoanTypes.AllowUserToAddRows = False
        Me.dgvLoanTypes.AllowUserToDeleteRows = False
        Me.dgvLoanTypes.AllowUserToResizeColumns = False
        Me.dgvLoanTypes.AllowUserToResizeRows = False
        Me.dgvLoanTypes.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvLoanTypes.BackgroundColor = System.Drawing.Color.White
        Me.dgvLoanTypes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLoanTypes.Location = New System.Drawing.Point(4, 36)
        Me.dgvLoanTypes.Name = "dgvLoanTypes"
        Me.dgvLoanTypes.ReadOnly = True
        Me.dgvLoanTypes.RowHeadersVisible = False
        Me.dgvLoanTypes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLoanTypes.Size = New System.Drawing.Size(960, 119)
        Me.dgvLoanTypes.TabIndex = 92
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.Button1.Location = New System.Drawing.Point(58, 254)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(184, 23)
        Me.Button1.TabIndex = 112
        Me.Button1.Text = "Computation Table Setup"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnclose
        '
        Me.btnclose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnclose.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnclose.Image = CType(resources.GetObject("btnclose.Image"), System.Drawing.Image)
        Me.btnclose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnclose.Location = New System.Drawing.Point(209, 3)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.Size = New System.Drawing.Size(66, 28)
        Me.btnclose.TabIndex = 9
        Me.btnclose.Text = "Close"
        Me.btnclose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnclose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "PK_Account"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Account Name"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Account Code"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Percent"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Amount"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'PanePanel2
        '
        Me.PanePanel2.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel2.Controls.Add(Me.Label19)
        Me.PanePanel2.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel2.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel2.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel2.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel2.Location = New System.Drawing.Point(1, 12)
        Me.PanePanel2.Name = "PanePanel2"
        Me.PanePanel2.Size = New System.Drawing.Size(451, 26)
        Me.PanePanel2.TabIndex = 0
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(3, 3)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(199, 16)
        Me.Label19.TabIndex = 91
        Me.Label19.Text = "Give the Details for this loan ."
        '
        'PanePanel3
        '
        Me.PanePanel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanePanel3.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel3.Controls.Add(Me.Label21)
        Me.PanePanel3.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel3.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel3.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel3.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel3.Location = New System.Drawing.Point(1, 12)
        Me.PanePanel3.Name = "PanePanel3"
        Me.PanePanel3.Size = New System.Drawing.Size(510, 26)
        Me.PanePanel3.TabIndex = 83
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.BackColor = System.Drawing.Color.Transparent
        Me.Label21.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(3, 3)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(279, 16)
        Me.Label21.TabIndex = 92
        Me.Label21.Text = "Enter the required number of Approvers ."
        '
        'PK_Account
        '
        Me.PK_Account.HeaderText = "PK_Account"
        Me.PK_Account.Name = "PK_Account"
        Me.PK_Account.ReadOnly = True
        Me.PK_Account.Visible = False
        '
        'Account
        '
        Me.Account.HeaderText = "Account Name"
        Me.Account.Name = "Account"
        Me.Account.ReadOnly = True
        '
        'AC
        '
        Me.AC.HeaderText = "Account Code"
        Me.AC.Name = "AC"
        Me.AC.ReadOnly = True
        '
        'Percent
        '
        Me.Percent.HeaderText = "Percent"
        Me.Percent.Name = "Percent"
        Me.Percent.ReadOnly = True
        '
        'Amount
        '
        Me.Amount.HeaderText = "Amount"
        Me.Amount.Name = "Amount"
        Me.Amount.ReadOnly = True
        '
        'PanePanel4
        '
        Me.PanePanel4.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanePanel4.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel4.Controls.Add(Me.Label25)
        Me.PanePanel4.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel4.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel4.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel4.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel4.Location = New System.Drawing.Point(1, 72)
        Me.PanePanel4.Name = "PanePanel4"
        Me.PanePanel4.Size = New System.Drawing.Size(510, 26)
        Me.PanePanel4.TabIndex = 89
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.White
        Me.Label25.Location = New System.Drawing.Point(2, 2)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(80, 16)
        Me.Label25.TabIndex = 94
        Me.Label25.Text = "Deductions"
        '
        'PanePanel5
        '
        Me.PanePanel5.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanePanel5.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel5.Controls.Add(Me.GroupBox1)
        Me.PanePanel5.Controls.Add(Me.Label24)
        Me.PanePanel5.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel5.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel5.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel5.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel5.Location = New System.Drawing.Point(1, 5)
        Me.PanePanel5.Name = "PanePanel5"
        Me.PanePanel5.Size = New System.Drawing.Size(968, 26)
        Me.PanePanel5.TabIndex = 91
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txtAcctContriCode)
        Me.GroupBox1.Controls.Add(Me.btnBrowse)
        Me.GroupBox1.Controls.Add(Me.txtAccountContribution)
        Me.GroupBox1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(365, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(388, 74)
        Me.GroupBox1.TabIndex = 109
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Account for Contribution "
        Me.GroupBox1.Visible = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.Color.Gray
        Me.Label11.Location = New System.Drawing.Point(80, 54)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(89, 13)
        Me.Label11.TabIndex = 110
        Me.Label11.Text = "Account Name"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.Color.Gray
        Me.Label10.Location = New System.Drawing.Point(10, 54)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(37, 13)
        Me.Label10.TabIndex = 3
        Me.Label10.Text = "Code"
        '
        'txtAcctContriCode
        '
        Me.txtAcctContriCode.Location = New System.Drawing.Point(6, 30)
        Me.txtAcctContriCode.Name = "txtAcctContriCode"
        Me.txtAcctContriCode.ReadOnly = True
        Me.txtAcctContriCode.Size = New System.Drawing.Size(63, 21)
        Me.txtAcctContriCode.TabIndex = 2
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(345, 28)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(36, 23)
        Me.btnBrowse.TabIndex = 1
        Me.btnBrowse.Text = "..."
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'txtAccountContribution
        '
        Me.txtAccountContribution.Location = New System.Drawing.Point(75, 30)
        Me.txtAccountContribution.Name = "txtAccountContribution"
        Me.txtAccountContribution.ReadOnly = True
        Me.txtAccountContribution.Size = New System.Drawing.Size(264, 21)
        Me.txtAccountContribution.TabIndex = 0
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(3, 2)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(85, 16)
        Me.Label24.TabIndex = 94
        Me.Label24.Text = "Loan Types "
        '
        'PanePanel1
        '
        Me.PanePanel1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.btnRefresh)
        Me.PanePanel1.Controls.Add(Me.btnclose)
        Me.PanePanel1.Controls.Add(Me.btnsave)
        Me.PanePanel1.Controls.Add(Me.btndelete)
        Me.PanePanel1.Controls.Add(Me.btnupdate)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel1.Location = New System.Drawing.Point(0, 549)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(969, 37)
        Me.PanePanel1.TabIndex = 30
        '
        'btnRefresh
        '
        Me.btnRefresh.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.btnRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRefresh.Location = New System.Drawing.Point(281, 3)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(116, 28)
        Me.btnRefresh.TabIndex = 10
        Me.btnRefresh.Text = "Refresh"
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'btnsave
        '
        Me.btnsave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsave.Image = Global.WindowsApplication2.My.Resources.Resources.new3
        Me.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsave.Location = New System.Drawing.Point(8, 3)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(66, 28)
        Me.btnsave.TabIndex = 6
        Me.btnsave.Text = "New"
        Me.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsave.UseVisualStyleBackColor = True
        '
        'btndelete
        '
        Me.btndelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btndelete.Image = CType(resources.GetObject("btndelete.Image"), System.Drawing.Image)
        Me.btndelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btndelete.Location = New System.Drawing.Point(142, 3)
        Me.btndelete.Name = "btndelete"
        Me.btndelete.Size = New System.Drawing.Size(66, 28)
        Me.btndelete.TabIndex = 8
        Me.btndelete.Text = "Delete"
        Me.btndelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btndelete.UseVisualStyleBackColor = True
        '
        'btnupdate
        '
        Me.btnupdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnupdate.Image = CType(resources.GetObject("btnupdate.Image"), System.Drawing.Image)
        Me.btnupdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnupdate.Location = New System.Drawing.Point(75, 3)
        Me.btnupdate.Name = "btnupdate"
        Me.btnupdate.Size = New System.Drawing.Size(66, 28)
        Me.btnupdate.TabIndex = 7
        Me.btnupdate.Text = "Edit"
        Me.btnupdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnupdate.UseVisualStyleBackColor = True
        '
        'picStep1
        '
        Me.picStep1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.picStep1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picStep1.Controls.Add(Me.lblStepone)
        Me.picStep1.Dock = System.Windows.Forms.DockStyle.Top
        Me.picStep1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.picStep1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.picStep1.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.picStep1.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.picStep1.Location = New System.Drawing.Point(0, 0)
        Me.picStep1.Name = "picStep1"
        Me.picStep1.Size = New System.Drawing.Size(969, 26)
        Me.picStep1.TabIndex = 29
        '
        'lblStepone
        '
        Me.lblStepone.AutoSize = True
        Me.lblStepone.BackColor = System.Drawing.Color.Transparent
        Me.lblStepone.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStepone.ForeColor = System.Drawing.Color.White
        Me.lblStepone.Location = New System.Drawing.Point(16, 2)
        Me.lblStepone.Name = "lblStepone"
        Me.lblStepone.Size = New System.Drawing.Size(24, 19)
        Me.lblStepone.TabIndex = 18
        Me.lblStepone.Text = "..."
        '
        'frmMasterfile_LoanType
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btnclose
        Me.ClientSize = New System.Drawing.Size(969, 586)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.picStep1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(812, 613)
        Me.Name = "frmMasterfile_LoanType"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Loan Type Master"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.GrdAccounts, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgvLoanTypes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanePanel2.ResumeLayout(False)
        Me.PanePanel2.PerformLayout()
        Me.PanePanel3.ResumeLayout(False)
        Me.PanePanel3.PerformLayout()
        Me.PanePanel4.ResumeLayout(False)
        Me.PanePanel4.PerformLayout()
        Me.PanePanel5.ResumeLayout(False)
        Me.PanePanel5.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.PanePanel1.ResumeLayout(False)
        Me.picStep1.ResumeLayout(False)
        Me.picStep1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblStepone As System.Windows.Forms.Label
    Friend WithEvents picStep1 As WindowsApplication2.PanePanel
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtcode As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalAllowable As System.Windows.Forms.TextBox
    Friend WithEvents txtdescription As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtRefund As System.Windows.Forms.TextBox
    Friend WithEvents btnclose As System.Windows.Forms.Button
    Friend WithEvents btnsave As System.Windows.Forms.Button
    Friend WithEvents btndelete As System.Windows.Forms.Button
    Friend WithEvents btnupdate As System.Windows.Forms.Button
    Friend WithEvents btnPolicies As System.Windows.Forms.Button
    Friend WithEvents btnApprover As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtpenaltyperc As System.Windows.Forms.TextBox
    Friend WithEvents cboPoliciesCode As System.Windows.Forms.ComboBox
    Friend WithEvents txtboardmem As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtCreditCommittee As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents PanePanel2 As WindowsApplication2.PanePanel
    Friend WithEvents PanePanel3 As WindowsApplication2.PanePanel
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents PanePanel4 As WindowsApplication2.PanePanel
    Friend WithEvents PanePanel5 As WindowsApplication2.PanePanel
    Friend WithEvents BtnRequirements As System.Windows.Forms.Button
    Friend WithEvents btnloanTerms As System.Windows.Forms.Button
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents txtDaysValid As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents btnPriority As System.Windows.Forms.Button
    Friend WithEvents txtPercentAllowedToReloan As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents GrdAccounts As System.Windows.Forms.DataGridView
    Friend WithEvents btnRemove As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtInterest As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtServiceFeeAmt As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtServiceFeePer As System.Windows.Forms.TextBox
    Friend WithEvents cboInterestType As System.Windows.Forms.ComboBox
    Friend WithEvents txtName As System.Windows.Forms.ComboBox
    Friend WithEvents PK_Account As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Account As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Per As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Percent As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Amount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Debit As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents InPayment As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtLoanCode As System.Windows.Forms.TextBox
    Friend WithEvents dgvLoanTypes As System.Windows.Forms.DataGridView
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents txtAccountContribution As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtAcctContriCode As System.Windows.Forms.TextBox
    Friend WithEvents cboIntType As System.Windows.Forms.ComboBox
    Friend WithEvents btnComaker As System.Windows.Forms.Button
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cboProject As System.Windows.Forms.ComboBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
