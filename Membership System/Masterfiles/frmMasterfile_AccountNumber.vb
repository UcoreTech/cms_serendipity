﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmMasterfile_AccountNumber
    Private ID As Integer = 0
    Public Property GetID() As Integer
        Get
            Return ID
        End Get
        Set(ByVal value As Integer)
            ID = value
        End Set
    End Property

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        If GrdAccounts.SelectedCells.Count <> 0 Then
            If ID = 3 Then
                For Each row As DataGridViewRow In GrdAccounts.SelectedRows
                    frmMasterfile_AccountRegister.txtAccountNumber.Text = row.Cells(0).Value.ToString
                Next
                Me.Close()
            ElseIf ID = 2 Then
                For Each row As DataGridViewRow In GrdAccounts.SelectedRows
                    Deposit.txtRefNo.Text = row.Cells(0).Value.ToString
                Next
                Me.Close()
            ElseIf ID = 1 Then
                For Each row As DataGridViewRow In GrdAccounts.SelectedRows
                    frmWithdrawal.txtRefNo.Text = row.Cells(0).Value.ToString
                Next
                Me.Close()
            End If
        End If
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub txtLastName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLastName.TextChanged
        If ID = 3 Then
            Load_AccountbyID(txtLastName.Text)
        ElseIf ID = 2 Then
            Load_AccountNumberBySavingsID(txtLastName.Text)
        ElseIf ID = 1 Then
            Load_AccountNumberWithDrawID(txtLastName.Text)
        End If
    End Sub

    Private Sub GrdAccounts_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GrdAccounts.DoubleClick
        If GrdAccounts.SelectedCells.Count <> 0 Then
            If ID = 3 Then
                For Each row As DataGridViewRow In GrdAccounts.SelectedRows
                    frmMasterfile_AccountRegister.txtAccountNumber.Text = row.Cells(0).Value.ToString
                Next
                Me.Close()
            ElseIf ID = 2 Then
                For Each row As DataGridViewRow In GrdAccounts.SelectedRows
                    Deposit.txtRefNo.Text = row.Cells(0).Value.ToString
                Next
                Me.Close()
            ElseIf ID = 1 Then
                For Each row As DataGridViewRow In GrdAccounts.SelectedRows
                    frmWithdrawal.txtRefNo.Text = row.Cells(0).Value.ToString
                Next
                Me.Close()
            End If
        End If
    End Sub

    Private Sub frmMasterfile_AccountNumber_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If ID = 3 Then
            Load_AccountNumber()
        ElseIf ID = 2 Then
            Load_AccountNumberBySavings()
        ElseIf ID = 1 Then
            Load_AccountNumberByWithDraw()
        End If
    End Sub

    Private Sub Load_AccountNumber()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Masterfile_AccountNumberList")
        Try
            With GrdAccounts
                .DataSource = Nothing
                .ClearSelection()
                .DataSource = ds.Tables(0)
                .Columns(1).Visible = False
                .Columns(2).Visible = False
                .Columns(3).Visible = False
                .Columns(0).HeaderText = "Account Number"
                .Columns(0).Width = 176
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With
            mycon.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "Account Number", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Load_AccountbyID(ByVal ID As String)
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Masterfile_AccountNumberListByID",
                                                     New SqlParameter("@fcDocNumber", ID))
        Try
            With GrdAccounts
                .ClearSelection()
                .DataSource = ds.Tables(0)
                .Columns(1).Visible = False
                .Columns(2).Visible = False
                .Columns(3).Visible = False
                .Columns(0).HeaderText = "Account Number"
                .Columns(0).Width = 176
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With
            mycon.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "Account Number", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Load_AccountNumberBySavings()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Masterfile_AccountNumberListBySavings")
        Try
            With GrdAccounts
                .ClearSelection()
                .DataSource = ds.Tables(0)
                .Columns(1).Visible = False
                .Columns(2).Visible = False
                .Columns(3).Visible = False
                .Columns(0).HeaderText = "Account Number"
                .Columns(0).Width = 176
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With
            mycon.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "Account Number by Savings", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Load_AccountNumberBySavingsID(ByVal ID As String)
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Masterfile_AccountNumberListBySavingsID",
                                                     New SqlParameter("@fcDocNumber", ID))
        Try
            With GrdAccounts
                .ClearSelection()
                .DataSource = ds.Tables(0)
                .Columns(1).Visible = False
                .Columns(2).Visible = False
                .Columns(3).Visible = False
                .Columns(0).HeaderText = "Account Number"
                .Columns(0).Width = 176
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With
            mycon.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "Account Number by Savings ID", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Load_AccountNumberByWithDraw()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, CommandType.StoredProcedure, "[CIMS_Masterfile_AccountNumberListByWithDraw]")
        Try
            With GrdAccounts
                .ClearSelection()
                .DataSource = ds.Tables(0)
                .Columns(1).Visible = False
                .Columns(2).Visible = False
                .Columns(3).Visible = False
                .Columns(0).HeaderText = "Account Number"
                .Columns(0).Width = 176
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With
            mycon.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "Account Number by Savings", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Load_AccountNumberWithDrawID(ByVal ID As String)
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, CommandType.StoredProcedure, "[CIMS_Masterfile_AccountNumberListByWithdrawID]",
                                                     New SqlParameter("@fcDocNumber", ID))
        Try
            With GrdAccounts
                .ClearSelection()
                .DataSource = ds.Tables(0)
                .Columns(1).Visible = False
                .Columns(2).Visible = False
                .Columns(3).Visible = False
                .Columns(0).HeaderText = "Account Number"
                .Columns(0).Width = 176
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With
            mycon.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "Account Number by Savings ID", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class