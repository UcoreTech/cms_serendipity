<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMasterfile_PayrollCalendar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMasterfile_PayrollCalendar))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtyear = New System.Windows.Forms.TextBox()
        Me.cbopaycode = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.chkDeductive = New System.Windows.Forms.CheckBox()
        Me.dtpaydate = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtto = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtfrom = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lvlCalendar = New System.Windows.Forms.ListView()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.btnclose = New System.Windows.Forms.Button()
        Me.btnsave = New System.Windows.Forms.Button()
        Me.btndelete = New System.Windows.Forms.Button()
        Me.btnupdate = New System.Windows.Forms.Button()
        Me.picStep1 = New WindowsApplication2.PanePanel()
        Me.lblStepone = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.PanePanel1.SuspendLayout()
        Me.picStep1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtyear)
        Me.GroupBox1.Controls.Add(Me.cbopaycode)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.chkDeductive)
        Me.GroupBox1.Controls.Add(Me.dtpaydate)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.dtto)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.dtfrom)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 32)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(703, 96)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Calendar"
        '
        'txtyear
        '
        Me.txtyear.Location = New System.Drawing.Point(40, 55)
        Me.txtyear.MaxLength = 4
        Me.txtyear.Name = "txtyear"
        Me.txtyear.Size = New System.Drawing.Size(74, 23)
        Me.txtyear.TabIndex = 11
        '
        'cbopaycode
        '
        Me.cbopaycode.FormattingEnabled = True
        Me.cbopaycode.Location = New System.Drawing.Point(69, 23)
        Me.cbopaycode.Name = "cbopaycode"
        Me.cbopaycode.Size = New System.Drawing.Size(334, 23)
        Me.cbopaycode.TabIndex = 10
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 26)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(57, 15)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Pay Code"
        '
        'chkDeductive
        '
        Me.chkDeductive.AutoSize = True
        Me.chkDeductive.Location = New System.Drawing.Point(608, 58)
        Me.chkDeductive.Name = "chkDeductive"
        Me.chkDeductive.Size = New System.Drawing.Size(75, 17)
        Me.chkDeductive.TabIndex = 8
        Me.chkDeductive.Text = "Deductive"
        Me.chkDeductive.UseVisualStyleBackColor = True
        '
        'dtpaydate
        '
        Me.dtpaydate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpaydate.Location = New System.Drawing.Point(484, 55)
        Me.dtpaydate.Name = "dtpaydate"
        Me.dtpaydate.Size = New System.Drawing.Size(104, 23)
        Me.dtpaydate.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(416, 60)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(55, 15)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Pay Date"
        '
        'dtto
        '
        Me.dtto.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtto.Location = New System.Drawing.Point(300, 55)
        Me.dtto.Name = "dtto"
        Me.dtto.Size = New System.Drawing.Size(109, 23)
        Me.dtto.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(272, 60)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(19, 15)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "To"
        '
        'dtfrom
        '
        Me.dtfrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtfrom.Location = New System.Drawing.Point(162, 55)
        Me.dtfrom.Name = "dtfrom"
        Me.dtfrom.Size = New System.Drawing.Size(102, 23)
        Me.dtfrom.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(120, 60)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "From"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 59)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(30, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Year"
        '
        'lvlCalendar
        '
        Me.lvlCalendar.Location = New System.Drawing.Point(14, 135)
        Me.lvlCalendar.Name = "lvlCalendar"
        Me.lvlCalendar.Size = New System.Drawing.Size(703, 346)
        Me.lvlCalendar.TabIndex = 1
        Me.lvlCalendar.UseCompatibleStateImageBehavior = False
        '
        'PanePanel1
        '
        Me.PanePanel1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.btnclose)
        Me.PanePanel1.Controls.Add(Me.btnsave)
        Me.PanePanel1.Controls.Add(Me.btndelete)
        Me.PanePanel1.Controls.Add(Me.btnupdate)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel1.Location = New System.Drawing.Point(0, 490)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(731, 42)
        Me.PanePanel1.TabIndex = 34
        '
        'btnclose
        '
        Me.btnclose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnclose.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnclose.Image = CType(resources.GetObject("btnclose.Image"), System.Drawing.Image)
        Me.btnclose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnclose.Location = New System.Drawing.Point(245, 5)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.Size = New System.Drawing.Size(77, 32)
        Me.btnclose.TabIndex = 9
        Me.btnclose.Text = "Close"
        Me.btnclose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnclose.UseVisualStyleBackColor = True
        '
        'btnsave
        '
        Me.btnsave.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsave.Image = Global.WindowsApplication2.My.Resources.Resources.new3
        Me.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsave.Location = New System.Drawing.Point(9, 5)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(77, 32)
        Me.btnsave.TabIndex = 6
        Me.btnsave.Text = "New"
        Me.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsave.UseVisualStyleBackColor = True
        '
        'btndelete
        '
        Me.btndelete.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btndelete.Image = CType(resources.GetObject("btndelete.Image"), System.Drawing.Image)
        Me.btndelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btndelete.Location = New System.Drawing.Point(166, 5)
        Me.btndelete.Name = "btndelete"
        Me.btndelete.Size = New System.Drawing.Size(77, 32)
        Me.btndelete.TabIndex = 8
        Me.btndelete.Text = "Delete"
        Me.btndelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btndelete.UseVisualStyleBackColor = True
        '
        'btnupdate
        '
        Me.btnupdate.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnupdate.Image = CType(resources.GetObject("btnupdate.Image"), System.Drawing.Image)
        Me.btnupdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnupdate.Location = New System.Drawing.Point(87, 5)
        Me.btnupdate.Name = "btnupdate"
        Me.btnupdate.Size = New System.Drawing.Size(77, 32)
        Me.btnupdate.TabIndex = 7
        Me.btnupdate.Text = "Edit"
        Me.btnupdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnupdate.UseVisualStyleBackColor = True
        '
        'picStep1
        '
        Me.picStep1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.picStep1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picStep1.Controls.Add(Me.lblStepone)
        Me.picStep1.Dock = System.Windows.Forms.DockStyle.Top
        Me.picStep1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.picStep1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.picStep1.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.picStep1.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.picStep1.Location = New System.Drawing.Point(0, 0)
        Me.picStep1.Name = "picStep1"
        Me.picStep1.Size = New System.Drawing.Size(731, 30)
        Me.picStep1.TabIndex = 33
        '
        'lblStepone
        '
        Me.lblStepone.AutoSize = True
        Me.lblStepone.BackColor = System.Drawing.Color.Transparent
        Me.lblStepone.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStepone.ForeColor = System.Drawing.Color.White
        Me.lblStepone.Location = New System.Drawing.Point(4, 5)
        Me.lblStepone.Name = "lblStepone"
        Me.lblStepone.Size = New System.Drawing.Size(82, 19)
        Me.lblStepone.TabIndex = 18
        Me.lblStepone.Text = "Calendar"
        '
        'frmMasterfile_PayrollCalendar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btnclose
        Me.ClientSize = New System.Drawing.Size(731, 532)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.picStep1)
        Me.Controls.Add(Me.lvlCalendar)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMasterfile_PayrollCalendar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Payroll Calendar"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.PanePanel1.ResumeLayout(False)
        Me.picStep1.ResumeLayout(False)
        Me.picStep1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkDeductive As System.Windows.Forms.CheckBox
    Friend WithEvents dtpaydate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dtto As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtfrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lvlCalendar As System.Windows.Forms.ListView
    Friend WithEvents lblStepone As System.Windows.Forms.Label
    Friend WithEvents picStep1 As WindowsApplication2.PanePanel
    Friend WithEvents btnclose As System.Windows.Forms.Button
    Friend WithEvents btnsave As System.Windows.Forms.Button
    Friend WithEvents btndelete As System.Windows.Forms.Button
    Friend WithEvents btnupdate As System.Windows.Forms.Button
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents cbopaycode As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtyear As System.Windows.Forms.TextBox
End Class
