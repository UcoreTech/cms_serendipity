Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMasterfile_Pagibig
#Region "Var"
    Private autoid As Integer
#End Region
#Region "VarProperty"
    Private Property getautoid() As Integer
        Get
            Return autoid
        End Get
        Set(ByVal value As Integer)
            autoid = value
        End Set
    End Property
#End Region
#Region "Function"
    Public Function Numbers(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = ".") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Invalid")
            Return False
        Else
            Return True
        End If
    End Function
#End Region
#Region "Add/Edit Contribution"
    Private Sub AddEditContribution(ByVal below As Decimal, ByVal upper As Decimal, ByVal empshare As Decimal, ByVal fnauto As Integer)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Contribution_Pagibig_AddEdit", _
                                      New SqlParameter("@fnBelowLimit", below), _
                                      New SqlParameter("@fnUpperLimit", upper), _
                                      New SqlParameter("@fnEmpShare", empshare), _
                                      New SqlParameter("@fnAuto", fnauto))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "AddEditContribution")
        Finally
            mycon.sqlconn.Close()
        End Try

    End Sub
#End Region
#Region "Delete Contribution"
    Private Sub deleteContribution(ByVal fnauto As Integer)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Contribution_Pagibig_Delete", _
                                      New SqlParameter("@fnAuto", fnauto))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "deleteContribution")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "View Contribution"
    Private Sub ViewContribution()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Contribution_Pagibig_Select")
        With Me.listContribution
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Below Limit", 100, HorizontalAlignment.Left)
            .Columns.Add("Upper Limit", 100, HorizontalAlignment.Left)
            .Columns.Add("Employee Share", 300, HorizontalAlignment.Left)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        .SubItems.Add(rd.Item(1))
                        .SubItems.Add(rd.Item(2))
                        .SubItems.Add(rd.Item(3))
                    End With
                End While
                rd.Close()
            Catch ex As Exception

            End Try
        End With
    End Sub
#End Region
#Region "Cleartext"
    Private Sub ClearText()
        Me.txtBL.Text = "0.00"
        Me.txtUL.Text = "0.00"
        Me.txtEmpShare.Text = "0.00"
    End Sub
#End Region
    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            If btnsave.Text = "New" Then
                Call ClearText()
                Label1.Text = "Add Pag-ibig Contribution"
                btndelete.Visible = False
                btnupdate.Visible = False
                btnsave.Text = "Save"
                btnsave.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(86, 7)

            ElseIf btnsave.Text = "Save" Then
                If Me.txtBL.Text <> "" And Me.txtUL.Text <> "" And Me.txtEmpShare.Text <> "" Then
                    Label1.Text = "Pag-ibig Contribution"
                    Call AddEditContribution(Decimal.Parse(Me.txtBL.Text), Decimal.Parse(Me.txtUL.Text), Decimal.Parse(Me.txtEmpShare.Text), 0)
                    Call ViewContribution()
                    Call ClearText()
                    btnsave.Text = "New"
                    btnsave.Image = My.Resources.new3
                    btnclose.Text = "Close"
                    btnclose.Location = New System.Drawing.Point(218, 6)
                    btndelete.Visible = True
                    btnupdate.Visible = True
                Else
                    MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If btnupdate.Text = "Edit" Then
                Label1.Text = "Edit Pag-ibig Contribution"
                Me.txtBL.Text = Me.listContribution.SelectedItems(0).Text
                Me.txtUL.Text = Me.listContribution.SelectedItems(0).SubItems(1).Text
                Me.txtEmpShare.Text = Me.listContribution.SelectedItems(0).SubItems(2).Text
                getautoid() = Me.listContribution.SelectedItems(0).SubItems(3).Text
                Me.btnsave.Enabled = False
                btnupdate.Text = "Update"
                btnupdate.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(164, 7)
                btndelete.Visible = False

            ElseIf btnupdate.Text = "Update" Then
                If Me.txtBL.Text <> "" And Me.txtUL.Text <> "" And Me.txtEmpShare.Text <> "" Then
                    Label1.Text = "Pag-ibig Contribution"
                    Call AddEditContribution(Decimal.Parse(Me.txtBL.Text), Decimal.Parse(Me.txtUL.Text), Decimal.Parse(Me.txtEmpShare.Text), autoid)
                    Call ViewContribution()
                    Call ClearText()
                    btndelete.Visible = True
                    btnupdate.Text = "Edit"
                    btnupdate.Image = My.Resources.edit1
                    btnclose.Text = "Cancel"
                    btnclose.Location = New System.Drawing.Point(218, 6)
                    btnclose.Text = "Close"
                    Me.btnsave.Enabled = True
                    Me.btndelete.Visible = True

                Else
                    MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If

        Catch ex As Exception
        End Try
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Try
            getautoid() = Me.listContribution.SelectedItems(0).SubItems(3).Text
            Dim x As New DialogResult
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
            If x = System.Windows.Forms.DialogResult.OK Then
                Call deleteContribution(autoid)
                Call ViewContribution()
                Call ClearText()
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Cancel" Then
            Label1.Text = "Pag-ibig Contribution"
            btnsave.Text = "New"
            btnsave.Image = My.Resources.new3
            btnupdate.Text = "Edit"
            btnupdate.Image = My.Resources.edit1
            btnclose.Text = "Close"
            btnclose.Location = New System.Drawing.Point(242, 7)
            btndelete.Visible = True
            btnupdate.Visible = True
            btnsave.Enabled = True
        ElseIf btnclose.Text = "Close" Then
            Me.Close()
        End If
    End Sub

    Private Sub frmContribution_Pagibig_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call ViewContribution()
    End Sub

    Private Sub txtBL_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBL.KeyPress
        e.Handled = Not Numbers(e.KeyChar)
    End Sub

    Private Sub txtUL_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUL.KeyPress
        e.Handled = Not Numbers(e.KeyChar)
    End Sub

    Private Sub txtEmpShare_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEmpShare.KeyPress
        e.Handled = Not Numbers(e.KeyChar)
    End Sub

End Class