Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMasterfile_Paycode
#Region "View Paycode"
    Private Sub ViewPaycode()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("CIMS_CIMS_m_Member_PayCode_Select", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = myconnection.sqlconn
        myconnection.sqlconn.Open()
        With Me.lvlpaycode
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Pay Code", 120, HorizontalAlignment.Left)
            .Columns.Add("Pay Description", 300, HorizontalAlignment.Left)

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .subitems.add(myreader.Item(1))
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
#End Region
#Region "ClearText"
    Private Sub cleartext()
        Me.txtcode.Text = ""
        Me.txtdescription.Text = ""
    End Sub

#End Region
#Region "Create Paycode"
    Private Sub CreatePaycode(ByVal desc As String, ByVal code As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_CIMS_m_Member_PayCode_insert", _
                                        New SqlParameter("@PayDesc", desc), _
                                        New SqlParameter("@paycode", code))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "CreatePaycode")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub

#End Region
#Region "Update Paycode"
    Private Sub UpdatePaycode(ByVal code As Integer, ByVal desc As String, ByVal oldPaycode As String)


        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_CIMS_m_Member_PayCode_Update", _
                                      New SqlParameter("@pk_PayCode", code), _
                                      New SqlParameter("@PayDesc", desc), _
                                      New SqlParameter("@oldPaycode", oldPaycode))
            trans.Commit()
        Catch ex As SqlException
            trans.Rollback()
            MessageBox.Show("User Error! This code has been already used. Please check your input.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "UpdatePaycode")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Delete Paycode"
    Private Sub Deletepaycode(ByVal code As Integer)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_CIMS_m_Member_PayCode_Delete", _
                                      New SqlParameter("@pk_PayCode", code))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show("Cannot delete PayCode. This Paycode is currently used by a member.", "Deletepaycode", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            If btnsave.Text = "New" Then
                Call cleartext()
                Me.txtcode.Focus()
                Label1.Text = "Add New Pay Code"
                btndelete.Visible = False
                btnupdate.Visible = False
                btnsave.Text = "Save"
                btnsave.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(84, 7)

            ElseIf btnsave.Text = "Save" Then
                If Me.txtdescription.Text <> "" Then
                    Label1.Text = "Pay Code"
                    Call CreatePaycode(Me.txtdescription.Text, txtcode.Text)
                    Call ViewPaycode()
                    Call cleartext()
                    txtcode.Focus()
                    btnsave.Text = "New"
                    btnsave.Image = My.Resources.new3
                    btnclose.Text = "Close"
                    btnclose.Location = New System.Drawing.Point(240, 7)
                    btndelete.Visible = True
                    btnupdate.Visible = True
                Else
                    MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If btnupdate.Text = "Edit" Then
                Me.txtcode.Focus()
                Label1.Text = "Edit Pay Code"
                Me.txtcode.Text = Me.lvlpaycode.SelectedItems(0).Text
                Me.txtdescription.Text = Me.lvlpaycode.SelectedItems(0).SubItems(1).Text
                Me.btnsave.Enabled = False
                btnupdate.Text = "Update"
                btnupdate.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(163, 7)
                btndelete.Visible = False

            ElseIf btnupdate.Text = "Update" Then
                If Me.txtcode.Text <> "" And Me.txtdescription.Text <> "" Then
                    Label1.Text = "Pay Code"

                    Dim oldPaycode As String
                    If lvlpaycode.Items.Count <> 0 Then
                        oldPaycode = Me.lvlpaycode.SelectedItems(0).SubItems(0).Text
                    End If

                    Call UpdatePaycode(Me.txtcode.Text, Me.txtdescription.Text, oldPaycode)
                    Call ViewPaycode()
                    Call cleartext()
                    btndelete.Visible = True
                    btnupdate.Text = "Edit"
                    btnupdate.Image = My.Resources.edit1
                    btnclose.Text = "Cancel"
                    btnclose.Location = New System.Drawing.Point(218, 6)
                    btnclose.Text = "Close"
                    Me.btnsave.Enabled = True
                    Me.btndelete.Visible = True

                Else
                    MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If

        Catch ex As Exception
        End Try
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Try

            Dim x As New DialogResult
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
            If x = System.Windows.Forms.DialogResult.OK Then
                Call Deletepaycode(Me.txtcode.Text)
                Call ViewPaycode()
                Call cleartext()
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Cancel" Then
            Label1.Text = "Pay Code"
            btnsave.Text = "New"
            btnsave.Image = My.Resources.new3
            btnupdate.Text = "Edit"
            btnupdate.Image = My.Resources.edit1
            btnclose.Text = "Close"
            btnclose.Location = New System.Drawing.Point(240, 7)
            btndelete.Visible = True
            btnupdate.Visible = True
            btnsave.Enabled = True
        ElseIf btnclose.Text = "Close" Then
            Me.Close()
        End If
    End Sub

    Private Sub frmPaycode_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call ViewPaycode()
    End Sub
#Region "Selecting Record to navigate"
    Private Sub NavigateRecords()
        Try
            Me.txtcode.Text = Me.lvlpaycode.SelectedItems(0).Text
            Me.txtdescription.Text = Me.lvlpaycode.SelectedItems(0).SubItems(1).Text
        Catch ex As Exception
            MessageBox.Show(ex.Message, "NavigateRecords")
        End Try
    End Sub
#End Region
    Private Sub lvlpaycode_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvlpaycode.Click
        Call NavigateRecords()
    End Sub

    Private Sub lvlpaycode_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvlpaycode.KeyDown
        Call NavigateRecords()
    End Sub

    Private Sub lvlpaycode_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvlpaycode.KeyUp
        Call NavigateRecords()
    End Sub
End Class