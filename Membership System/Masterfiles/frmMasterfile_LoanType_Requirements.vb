Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmMasterfile_LoanType_Requirements
#Region "Get Loan Type"
    Private Sub GetloanType()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CIMS_getloanType_byProj", _
                                     New SqlParameter("@co_name", txtProject.Text))
        Me.cboLoantype.Items.Clear()
        While rd.Read
            Me.cboLoantype.Items.Add(rd.Item(0))
        End While
        rd.Close()
        gcon.sqlconn.Close()
    End Sub
#End Region
#Region "Get loan Type uniqueidentifier"
    Private Sub GetloantypeUnique(ByVal loanid As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_getloanTypekey", _
                                     New SqlParameter("@fcLoanTypeName", loanid))
        Try
            While rd.Read
                getLoanID() = rd.Item(0)
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "GetloantypeUnique")
        End Try
    End Sub
#End Region
#Region "Variables"
    Private LoanID As String
    Private pkRequire As String
#End Region
#Region "Property"
    Private Property getLoanID() As String
        Get
            Return LoanID
        End Get
        Set(ByVal value As String)
            LoanID = value
        End Set
    End Property
    Private Property getpkRequire() As String
        Get
            Return pkRequire
        End Get
        Set(ByVal value As String)
            pkRequire = value
        End Set
    End Property
#End Region
#Region "Insert Update Loan Requirements"
    Private Sub InsertUpdateLoanRequirements(ByVal loanid As String, ByVal requirement As String, ByVal pkrequire As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Masterfiles_Requirements_AddEdit", _
                                      New SqlParameter("@fk_LoanType", loanid), _
                                      New SqlParameter("@fcRequirement", requirement), _
                                      New SqlParameter("@pk_LoanRequirements", pkrequire), _
                                      New SqlParameter("@co_name", txtProject.Text))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "InsertUpdateLoanRequirements")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Delete Loan Requirements"
    Private Sub DeleteLoanRequirements(ByVal pkrequire As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Masterfiles_Requirements_Delete", _
                                      New SqlParameter("@pk_LoanRequirements", pkrequire))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show("You cannot delete this requirement that is already tag on the loan processing", "DeleteLoanRequirements")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "View Loan Requirements"
    Private Sub ViewLoanRequirement(ByVal loanid As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_CIMS_m_LoanType_Requirements_Select", _
                                    New SqlParameter("@fk_LoanType", loanid))
        With Me.listLoanRequirements
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Loan Requirements", 300, HorizontalAlignment.Left)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        .SubItems.Add(rd.Item(1))
                    End With
                End While
                rd.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "ViewLoanRequirement")
            End Try
        End With
    End Sub
#End Region
    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Cancel" Then
            Label1.Text = "Loan Requirements"
            btnsave.Text = "New"
            btnsave.Image = My.Resources.new3
            btnupdate.Text = "Edit"
            btnupdate.Image = My.Resources.edit1
            btnclose.Text = "Close"
            btnclose.Location = New System.Drawing.Point(218, 6)
            btndelete.Visible = True
            btnupdate.Visible = True
            btnsave.Enabled = True
        ElseIf btnclose.Text = "Close" Then
            Me.Close()
        End If
    End Sub

    Private Sub frmLoanRequirements_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.txtProject.Text = frmMasterfile_LoanType.cboProject.Text
        If Me.txtProject.Text = "" Then
            Call GetloanType()
        Else
            Me.txtProject.Text = frmMasterfile_LoanType.cboProject.Text
            Me.cboLoantype.Text = frmMasterfile_LoanType.txtName.Text
            Call GetloantypeUnique(Me.cboLoantype.Text)
            Call ViewLoanRequirement(LoanID)
            Call GetloanType()
        End If

    End Sub
    Private Sub cboLoantype_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoantype.TextChanged
        Call GetloantypeUnique(Me.cboLoantype.Text)
        Call ViewLoanRequirement(LoanID)
    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            If btnsave.Text = "New" Then

                Label1.Text = "Add Loan Requirements"
                btndelete.Visible = False
                btnupdate.Visible = False
                btnsave.Text = "Save"
                btnsave.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(74, 6)

            ElseIf btnsave.Text = "Save" Then
                If Me.cboLoantype.Text <> "" And Me.txtdescription.Text <> "" Then
                    Label1.Text = "Loan Requirements"
                    Call InsertUpdateLoanRequirements(LoanID, Me.txtdescription.Text, "")
                    Call ViewLoanRequirement(LoanID)
                    btnsave.Text = "New"
                    btnsave.Image = My.Resources.new3
                    btnclose.Text = "Close"
                    btnclose.Location = New System.Drawing.Point(218, 6)
                    btndelete.Visible = True
                    btnupdate.Visible = True
                    txtdescription.Text = ""
                Else
                    MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If btnupdate.Text = "Edit" Then
                Label1.Text = "Edit Loan Requirements"
                Me.txtdescription.Text = Me.listLoanRequirements.SelectedItems(0).Text
                getpkRequire() = Me.listLoanRequirements.SelectedItems(0).SubItems(1).Text
                Me.btnsave.Enabled = False
                btnupdate.Text = "Update"
                btnupdate.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(141, 6)
                btndelete.Visible = False

            ElseIf btnupdate.Text = "Update" Then
                If Me.cboLoantype.Text <> "" And Me.txtdescription.Text <> "" Then
                    Label1.Text = "Loan Requirements"
                    Call InsertUpdateLoanRequirements(LoanID, Me.txtdescription.Text, pkRequire)
                    Call ViewLoanRequirement(LoanID)
                    btndelete.Visible = True
                    btnupdate.Text = "Edit"
                    btnupdate.Image = My.Resources.edit1
                    btnclose.Text = "Cancel"
                    btnclose.Location = New System.Drawing.Point(218, 6)
                    btnclose.Text = "Close"
                    Me.btnsave.Enabled = True
                    Me.btndelete.Visible = True
                    txtdescription.Text = ""
                Else
                    MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If

        Catch ex As Exception
        End Try
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Try
            getpkRequire() = Me.listLoanRequirements.SelectedItems(0).SubItems(1).Text
            Dim x As New DialogResult
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            If x = System.Windows.Forms.DialogResult.OK Then
                Call DeleteLoanRequirements(pkRequire)
                Call ViewLoanRequirement(LoanID)
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub cboLoantype_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLoantype.SelectedIndexChanged

    End Sub

    Private Sub txtdescription_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtdescription.Click
        txtdescription.Clear()
    End Sub

    Private Sub txtdescription_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtdescription.KeyDown
        If e.KeyCode = Keys.Enter Then
            If Me.cboLoantype.Text <> "" And Me.txtdescription.Text <> "" Then
                Label1.Text = "Loan Requirements"
                Call InsertUpdateLoanRequirements(LoanID, Me.txtdescription.Text, "")
                Call ViewLoanRequirement(LoanID)
                btnsave.Text = "New"
                btnsave.Image = My.Resources.new3
                btnclose.Text = "Close"
                btnclose.Location = New System.Drawing.Point(218, 6)
                btndelete.Visible = True
                btnupdate.Visible = True
            Else
                MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
            txtdescription.Clear()
        End If
    End Sub

    Private Sub txtdescription_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtdescription.TextChanged

    End Sub
End Class