﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmLoanPayment
    'Coded by : Vincent Nacar 

    Private gcon As New Clsappconfiguration

    Private Sub frmLoanPayment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadMethod()
        GenerateMemo()
        LoadAccounts()
        LoadORNo()
        'LoadCheckNo()

        txtDate.Text = Format(Date.Now, "MM/dd/yyyy")
    End Sub

    Private Sub LoadMethod()
        With cboMethod
            .Items.Add("Cash")
            .Items.Add("Check")
        End With
    End Sub

    Private Sub GenerateMemo()
        txtMemo.Text = (Me.Text & " - " & txtLoanNo.Text & " - " & Format(Date.Now, "MM/dd/yyyy"))
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub LoadAccounts()
        cboBank.Items.Clear()
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(gcon.cnstring, "_Select_BankAccount")
        With cboBank
            .DataSource = ds.Tables(0)
            .ValueMember = "PK_BankID"
            .DisplayMember = "fcBankName"
        End With
        cboBank.Text = "Select"
    End Sub

    Private Sub LoadORNo()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_ORNo")
        While rd.Read
            With cboOrNo
                .Items.Add(rd("fcDocNumber"))
            End With
        End While
    End Sub

    Private Sub LoadCheckNo()
        cboCheckNo.Items.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_CheckNumber",
                                     New SqlParameter("@BankID", cboBank.SelectedValue))
        While rd.Read
            With cboCheckNo
                .Items.Add(rd("FX_CheckNumber"))
            End With
        End While
    End Sub

    Private Sub cboBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBank.Click
        LoadCheckNo()
    End Sub

    Private Sub cboMethod_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMethod.SelectedValueChanged
        Select Case cboMethod.Text
            Case "Cash"
                cboBank.Enabled = False
                cboCheckNo.Enabled = False
            Case "Check"
                cboBank.Enabled = True
                cboCheckNo.Enabled = True
        End Select
    End Sub

    Private Sub btnPost_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPost.Click

    End Sub
End Class