'                   ########################################################
'                   ### Created by: Charl Magne "Ionflux" Onod San Pedro ###
'                   ### Date Created: August 17, 2010                    ###
'                   ### Website: http://www.ionflux.site50.net           ###
'                   ########################################################

Imports system.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmCashPayment_Main


    Private employeeNo As String
    Public Property GetEmployeeNo() As String
        Get
            Return employeeNo
        End Get
        Set(ByVal value As String)
            employeeNo = value
        End Set
    End Property


    Private interestRefundAdjustment As Decimal
    Public Property GetAdjustedInterestRefund() As Decimal
        Get
            Return interestRefundAdjustment
        End Get
        Set(ByVal value As Decimal)
            interestRefundAdjustment = value
        End Set
    End Property



    Private gCon As New Clsappconfiguration
    Private CIMSfunctions As New ClsCIMSFunctions()
    Private fxKeyNames As String = ""
    Private fcOrigIterestRefund As Decimal = 0


    Private Sub EditMode(ByVal Engaged As Boolean)
        If Engaged = True Then
            txtLoanNo.Enabled = False
            txtPrincipal.Enabled = False
            txtTerms.Enabled = False
            txtInterest.Enabled = False
            txtServiceFee.Enabled = False
            txtCapital.Enabled = False
            txtAmortization.Enabled = False
            txtReceivable.Enabled = False
            txtPenalty.Enabled = False
            txtTotalPayment.Enabled = False
            cboPaymentType.Enabled = False
            FlowLayoutPanel1.Visible = False
            btnSave.Visible = True
            btnCancel.Visible = True
            btnSearch.Enabled = False
            txtSearchBox.Enabled = False
            lstNames.Enabled = False
            grdLoanList.Enabled = False
            If cboPaymentType.Text = "Loan Pretermination" Then
                txtNetDue.Enabled = False
                txtInterestRefund.ReadOnly = False
                txtInterestRefund.Enabled = True
                txtNetDue.ReadOnly = True
                txtInterestRefund.Text = Format(CDec(txtInterestRefund.Text), "####0.00")
                txtInterestRefund.SelectionStart = 0
                txtInterestRefund.SelectionLength = Len(txtInterestRefund.Text)
                txtInterestRefund.Select()
                txtInterestRefund.SelectAll()
            Else
                txtNetDue.Enabled = True
                txtInterestRefund.ReadOnly = True
                txtInterestRefund.Enabled = False
                txtNetDue.ReadOnly = False
                txtNetDue.Text = Format(CDec(txtNetDue.Text), "####0.00")
                txtNetDue.SelectionStart = 0
                txtNetDue.SelectionLength = Len(txtNetDue.Text)
                txtNetDue.Select()
                txtNetDue.SelectAll()
            End If
        Else
            cboPaymentType.Enabled = True
            txtLoanNo.Enabled = True
            txtPrincipal.Enabled = True
            txtTerms.Enabled = True
            txtInterest.Enabled = True
            txtServiceFee.Enabled = True
            txtCapital.Enabled = True
            txtAmortization.Enabled = True
            txtReceivable.Enabled = True
            txtPenalty.Enabled = True
            txtTotalPayment.Enabled = True
            txtNetDue.Enabled = True
            txtInterestRefund.ReadOnly = True
            txtInterestRefund.Enabled = True
            txtNetDue.ReadOnly = True
            FlowLayoutPanel1.Visible = True
            btnSave.Visible = False
            btnCancel.Visible = False
            btnSearch.Enabled = True
            txtSearchBox.Enabled = True
            lstNames.Enabled = True
            grdLoanList.Enabled = True
        End If
    End Sub
    Private Sub LoadLoanComputation(ByVal pkLoanMember As String)
        If pkLoanMember <> "" Then
            Dim sSqlCmd As String
            Select Case cboPaymentType.Text
                Case "Loan Pretermination"
                    sSqlCmd = "CIMS_Payments_LoanPretermination_Computation '" & pkLoanMember & "'"
                    lblReceivable.Text = "Receivable:"

                Case "Amortization Settlement"
                    sSqlCmd = "CIMS_Payments_AmortizationSettlement_Computation '" & pkLoanMember & "'"
                    lblReceivable.Text = "Balance:"
            End Select

            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                    While rd.Read
                        txtReceivable.Text = Format(rd.Item("Receivable"), "##,##0.00")
                        txtPenalty.Text = Format(rd.Item("Penalty"), "##,##0.00")
                        txtTotalPayment.Text = Format(rd.Item("total payment"), "##,##0.00")
                        txtInterestRefund.Text = Format(rd.Item("Interest Refund"), "##,##0.00")
                        fcOrigIterestRefund = rd.Item("Interest Refund")
                        txtNetDue.Text = Format(rd.Item("Net Due"), "##,##0.00")
                    End While
                End Using
            Catch ex As Exception
                MsgBox("Error at LoadLoanComputation in FrmCashPaymentMaster." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
            End Try
        Else
            txtReceivable.Text = "0.00"
            txtPenalty.Text = "0.00"
            txtInterestRefund.Text = "0.00"
            txtNetDue.Text = "0.00"
        End If
    End Sub
    Private Sub loadLoanDetail(ByVal pkLoanMember As String)
        If pkLoanMember <> "" Then
            Dim sSqlCmd As String = "CIMS_Payments_LoadApprovedLoans_PerLoan  '" & pkLoanMember & "'"
            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                    While rd.Read
                        txtLoanAppDate.Text = Format(rd.Item("dtLoanApplicationDate"), "MM/dd/yyyy")
                        txtLoanNo.Text = rd.Item("fnLoanNo").ToString
                        txtPrincipal.Text = Format(rd.Item("fdPrincipal"), "##,##0.00")
                        txtTerms.Text = Format(rd.Item("fnTermsInMonths"), "##,##0")
                        lblInterest.Text = "Interest (" & rd.Item("fnInterestPercent").ToString & "%):"
                        txtInterest.Text = Format(rd.Item("fdInterestAmount"), "##,##0")
                        lblServiceFee.Text = "Service Fee (" & rd.Item("fnServiceFeePercent").ToString & "%):"
                        txtServiceFee.Text = Format(rd.Item("fdServiceFeeAmount"), "##,##0.00")
                        lblCapShare.Text = "Capital Share (" & rd.Item("fnCapitalSharePercent").ToString & "%):"
                        txtCapital.Text = Format(rd.Item("fdCapitalShareAmount"), "##,##0.00")
                        txtAmortization.Text = Format(rd.Item("Amortization"), "##,##0.00")
                    End While
                End Using
                txtStatus.Text = grdLoanList.Item("fcStatusName", grdLoanList.SelectedRows(0).Index).Value.ToString
            Catch ex As Exception
                MsgBox("Error at loadLoanDetail in FrmCashPaymentMaster" & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
            End Try
        Else
            txtLoanAppDate.Text = ""
            txtLoanNo.Text = ""
            txtPrincipal.Text = "0.00"
            txtTerms.Text = "0.00"
            lblInterest.Text = "Interest (0%):"
            txtInterest.Text = "0.00"
            lblServiceFee.Text = "Service Fee (0%):"
            txtServiceFee.Text = "0.00"
            lblCapShare.Text = "Capital Share (0%):"
            txtCapital.Text = "0.00"
            txtAmortization.Text = "0.00"
        End If
    End Sub
    Private Sub SelectMode()
        Select Case cboPaymentType.Text
            Case "Loan Pretermination"
                lblLoanDetailTitle.Text = "Loan Pretermination"
                lblPanel2.Text = "Select a loan to preterminate:"
                SplitContainer2.Panel1Collapsed = False
                btnView.Visible = True
                btnEdit.Visible = True
                Panel2.Visible = False
                LoadApprovedLoans()

            Case "Amortization Settlement"
                lblLoanDetailTitle.Text = "Amortization Settlement"
                lblPanel2.Text = "Select a loan to pay:"
                SplitContainer2.Panel1Collapsed = False
                btnView.Visible = True
                btnEdit.Visible = True
                Panel2.Visible = False
                LoadApprovedLoans()

            Case "Contribution"
                SplitContainer2.Panel1Collapsed = True
                btnView.Visible = False
                btnEdit.Visible = False
                Panel2.Visible = False

            Case "Membership Fee"
                SplitContainer2.Panel1Collapsed = True
                btnView.Visible = False
                btnEdit.Visible = False
                Panel2.Visible = False

            Case "Savings"
                Label19.Text = "Savings"
                SplitContainer2.Panel1Collapsed = False
                btnView.Visible = False
                btnEdit.Visible = False
                Panel2.Visible = True
                ViewSavingsDeposit(employeeNo)

            Case "Time Deposit"
                Label19.Text = "Time Deposit"
                SplitContainer2.Panel1Collapsed = False
                btnView.Visible = False
                btnEdit.Visible = False
                Panel2.Visible = True
                ViewTimeDeposit(employeeNo)
        End Select
    End Sub
    Private Sub LoadApprovedLoans()
        txtLoanAppDate.Text = ""
        txtLoanNo.Text = ""
        txtPrincipal.Text = "0.00"
        txtTerms.Text = "0.00"
        lblInterest.Text = "Interest (0%):"
        txtInterest.Text = "0.00"
        lblServiceFee.Text = "Service Fee (0%):"
        txtServiceFee.Text = "0.00"
        lblCapShare.Text = "Capital Share (0%):"
        txtCapital.Text = "0.00"
        txtAmortization.Text = "0.00"
        txtStatus.Text = ""

        txtReceivable.Text = "0.00"
        txtPenalty.Text = "0.00"
        txtInterestRefund.Text = "0.00"
        txtNetDue.Text = "0.00"
        txtTotalPayment.Text = "0.00"

        If fxKeyNames <> "" Then
            Dim sSqlCmd As String = "CIMS_Payments_LoadApprovedLoans '" & fxKeyNames & "'"
            Try
                grdLoanList.DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSqlCmd).Tables(0)
                With grdLoanList
                    .Columns("pk_Members_Loan").Visible = False
                    .Columns("pk_Employee").Visible = False
                    .Columns("fcStatusName").Visible = False

                    .Columns("fdPrincipal").DefaultCellStyle.Format = "##,##0.00"
                    .Columns("fdInterestAmount").DefaultCellStyle.Format = "##,##0.00"
                    .Columns("fdServiceFeeAmount").DefaultCellStyle.Format = "##,##0.00"
                    .Columns("fdCapitalShareAmount").DefaultCellStyle.Format = "##,##0.00"
                    .Columns("Amortization").DefaultCellStyle.Format = "##,##0.00"
                    .Columns("fnTermsInMonths").DefaultCellStyle.Format = "##,##0"
                    .Columns("fdBalance").DefaultCellStyle.Format = "##,##0.00"
                    .Columns("fdAmountPaid").DefaultCellStyle.Format = "##,##0.00"

                    .Columns("fdPrincipal").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    .Columns("fdInterestAmount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    .Columns("fdServiceFeeAmount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    .Columns("fdCapitalShareAmount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    .Columns("Amortization").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    .Columns("fdBalance").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    .Columns("fdAmountPaid").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

                    .Columns("fnLoanNo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .Columns("fnInterestPercent").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .Columns("fnServiceFeePercent").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .Columns("fnTermsInMonths").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .Columns("fnCapitalSharePercent").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

                    .Columns("dtLoanApplicationDate").HeaderText = "Date"
                    .Columns("fnLoanNo").HeaderText = "Loan No."
                    .Columns("fdPrincipal").HeaderText = "Principal"
                    .Columns("fnTermsInMonths").HeaderText = "Terms"
                    .Columns("fnInterestPercent").HeaderText = "Interest %"
                    .Columns("fdInterestAmount").HeaderText = "Interest Amt"
                    .Columns("fnServiceFeePercent").HeaderText = "Ser. Fee %"
                    .Columns("fdServiceFeeAmount").HeaderText = "Ser. Fee Amt"
                    .Columns("fnCapitalSharePercent").HeaderText = "Capital Share %"
                    .Columns("fdCapitalShareAmount").HeaderText = "Capital Share Amt"
                    .Columns("Amortization").HeaderText = "Amortization"
                    .Columns("fcLoanTypeName").HeaderText = "Loan Type"
                    .Columns("fdBalance").HeaderText = "Balance"
                    .Columns("fdAmountPaid").HeaderText = "Payment"


                End With
            Catch ex As Exception
                MsgBox("Error occur at LoadApprovedLoans in FrmCashPaymentMaster." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur")
            End Try
        Else
            grdLoanList.Rows.Clear()
        End If
    End Sub
    Private Sub SearchMember(ByVal searchname As String)
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList_SearchName", _
                                                         New SqlParameter("@SEARCH", searchname))
        With Me.lstNames
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("IDnumber", 70, HorizontalAlignment.Left)
            .Columns.Add("Member's Fullname", 200, HorizontalAlignment.Left)
            .Columns.Add("fillers", 0, HorizontalAlignment.Left)
            .Columns.Add("fxKey", 0, HorizontalAlignment.Left)
            .Columns.Add("fillers", 0, HorizontalAlignment.Left)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        .SubItems.Add(rd.Item(1))
                        .SubItems.Add("")
                        .SubItems.Add(rd.Item(2).ToString)
                        .SubItems.Add("")
                    End With
                End While
                rd.Close()
                gcon.sqlconn.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Load Member list")
            End Try
        End With
    End Sub
    Private Sub loadNames()
        Dim gCon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList")
        With Me.lstNames
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("IDnumber", 70, HorizontalAlignment.Left)
            .Columns.Add("Member's Fullname", 200, HorizontalAlignment.Left)
            .Columns.Add("fillers", 0, HorizontalAlignment.Left)
            .Columns.Add("fxKey", 0, HorizontalAlignment.Left)
            .Columns.Add("fillers", 0, HorizontalAlignment.Left)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        .SubItems.Add(rd.Item(1))
                        .SubItems.Add("")
                        .SubItems.Add(rd.Item(2).ToString)
                        .SubItems.Add("")
                    End With
                End While
                rd.Close()
                gCon.sqlconn.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Load Member list")
            End Try
            .Columns(2).Text = ""
            .Columns(3).Text = ""
            .Columns(4).Text = ""

        End With
    End Sub
    Private Sub LoadPaymentType(ByVal empNo As String)
        Dim sSqlCmd As String = "CIMS_Payments_Type_Load"
        cboPaymentType.Items.Clear()
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, sSqlCmd, _
                        New SqlParameter("@employeeNo", empNo))
                While rd.Read
                    cboPaymentType.Items.Add(rd.Item("fcPaymentName").ToString)
                End While
            End Using
        Catch ex As Exception
            MsgBox("Error at LoadPaymentType in FrmCashPaymentMaster" & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try
        cboPaymentType.SelectedIndex = 0
    End Sub
    Private Sub FrmCashPaymentMaster_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loadNames()
        LoadPaymentType(GetEmployeeNo())
    End Sub
    Private Sub lstNames_ColumnWidthChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnWidthChangedEventArgs) Handles lstNames.ColumnWidthChanged
        If lstNames.Columns.Count = 5 Then
            If lstNames.Columns(2).Width <> 0 Then
                lstNames.Columns(2).Width = 0
            End If
            If lstNames.Columns(3).Width <> 0 Then
                lstNames.Columns(3).Width = 0
            End If
            If lstNames.Columns(4).Width <> 0 Then
                lstNames.Columns(4).Width = 0
            End If
        End If
    End Sub
    Private Sub cboPaymentType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPaymentType.SelectedIndexChanged
        SelectMode()

    End Sub
    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        SearchMember(txtSearchBox.Text)
    End Sub
    Private Sub lstNames_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstNames.SelectedIndexChanged
        If lstNames.SelectedItems.Count <> 0 Then
            fxKeyNames = lstNames.SelectedItems(0).SubItems(3).Text
            GetEmployeeNo() = lstNames.SelectedItems(0).SubItems(0).Text
            SelectMode()
            LoadPaymentType(GetEmployeeNo())
        End If
    End Sub
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        EditMode(True)
    End Sub
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        EditMode(False)
        LoadLoanComputation(grdLoanList.Item("pk_Members_Loan", grdLoanList.SelectedRows(0).Index).Value.ToString)
        GetAdjustedInterestRefund() = 0
    End Sub
    Private Sub txtInterestRefund_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtInterestRefund.KeyPress
        If e.KeyChar <> Chr(46) Then
            If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
            If e.KeyChar = Chr(8) Then e.Handled = False
            If e.KeyChar = Chr(13) Then txtInterestRefund.Focus()
        End If
    End Sub
    Private Sub txtInterestRefund_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInterestRefund.LostFocus
        If txtInterestRefund.Text <> "" Then
            txtInterestRefund.Text = Format(CDec(txtInterestRefund.Text), "##,##0.00")
        Else
            txtInterestRefund.Text = "0.00"
        End If
    End Sub
    Private Sub txtNetDue_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNetDue.KeyPress
        If e.KeyChar <> Chr(46) Then
            If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
            If e.KeyChar = Chr(8) Then e.Handled = False
            If e.KeyChar = Chr(13) Then txtNetDue.Focus()
        End If
    End Sub
    Private Sub txtNetDue_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNetDue.LostFocus
        If txtNetDue.Text <> "" Then
            txtNetDue.Text = Format(CDec(txtNetDue.Text), "##,##0.00")
        Else
            txtNetDue.Text = "0.00"
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        EditMode(False)
        If cboPaymentType.Text = "Loan Pretermination" Then
            Dim xReceivable As Decimal = 0
            Dim xPenalty As Decimal = 0
            Dim xTotalPayment As Decimal = 0
            Dim xInterestRefund As Decimal = 0
            Dim xNetDue As Decimal = 0
            xReceivable = Format(CDec(txtReceivable.Text), "####0.00")
            xPenalty = Format(CDec(txtPenalty.Text), "####0.00")
            xTotalPayment = Format(CDec(txtTotalPayment.Text), "####0.00")
            xInterestRefund = Format(CDec(txtInterestRefund.Text), "####0.00")
            xNetDue = ((xReceivable + xPenalty) - (xTotalPayment + xInterestRefund))
            txtNetDue.Text = Format(xNetDue, "##,##0.00")

            GetAdjustedInterestRefund() = xInterestRefund
        End If
    End Sub
    Private Sub btnReceivePymnt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReceivePymnt.Click
        If grdLoanList.SelectedRows.Count <> 0 And (cboPaymentType.Text <> "" Or cboPaymentType.Text IsNot Nothing) Then

            Dim xForm As New frmCashPayment_ReceivePayment
            xForm.fcMode = cboPaymentType.Text
            xForm.lblName.Text = lstNames.SelectedItems(0).SubItems(1).Text
            xForm.lblEmpNo.Text = "Employee No.: " & lstNames.SelectedItems(0).Text
            xForm.lblLoanNo.Text = txtLoanNo.Text
            xForm.txtPayment.Text = txtNetDue.Text
            xForm.fKeyEmployee = lstNames.SelectedItems(0).SubItems.Item(3).Text
            xForm.fcInterestRefund = fcOrigIterestRefund
            xForm.fcInterestRefundAdjustment = fcOrigIterestRefund - CDec(txtInterestRefund.Text)
            xForm.ShowDialog()

            Select Case cboPaymentType.Text
                Case "Loan Pretermination"
                    LoadApprovedLoans()
                Case "Amortization Settlement"
                    LoadApprovedLoans()
            End Select


        Else
            If cboPaymentType.Text = "Contribution" Or cboPaymentType.Text = "Membership Fee" Or cboPaymentType.Text = "Savings" Or cboPaymentType.Text = "Time Deposit" Then

                Dim xForm As New frmCashPayment_ReceivePayment
                xForm.fcMode = cboPaymentType.Text
                xForm.lblName.Text = lstNames.SelectedItems(0).SubItems(1).Text
                xForm.lblEmpNo.Text = "Employee No.: " & lstNames.SelectedItems(0).Text
                xForm.lblLoanNo.Text = txtLoanNo.Text
                xForm.txtPayment.Text = txtNetDue.Text
                xForm.fKeyEmployee = lstNames.SelectedItems(0).SubItems.Item(3).Text
                xForm.fcInterestRefund = fcOrigIterestRefund
                xForm.fcInterestRefundAdjustment = fcOrigIterestRefund - CDec(txtInterestRefund.Text)
                xForm.ShowDialog()
                LoadApprovedLoans()

            Else
                MsgBox("Please select a loan to pay.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "No loan selected")
            End If
        End If

    End Sub
    Private Sub grdLoanList_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdLoanList.SelectionChanged
        If grdLoanList.SelectedRows.Count <> 0 Then
            loadLoanDetail(grdLoanList.Item("pk_Members_Loan", grdLoanList.SelectedRows(0).Index).Value.ToString)
            LoadLoanComputation(grdLoanList.Item("pk_Members_Loan", grdLoanList.SelectedRows(0).Index).Value.ToString)
            btnEdit.Enabled = True
        Else
            btnEdit.Enabled = False
        End If

        GetAdjustedInterestRefund() = 0
    End Sub


    Private Sub grdLoanList_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grdLoanList.DoubleClick
        LoadPaymentProperties()
        frmLoan_PaymentHistory.ShowDialog()
    End Sub
    Private Sub LoadPaymentProperties()
        With Me.grdLoanList.CurrentRow
            frmLoan_PaymentHistory.GetSetFkLoanPayment() = .Cells("pk_Members_Loan").Value.ToString
            frmLoan_PaymentHistory.GetSetSupplierName() = .Cells("Member").Value.ToString
            frmLoan_PaymentHistory.GetSetLoanNo() = .Cells("fnLoanNo").Value.ToString
            frmLoan_PaymentHistory.GetSetLoanType() = .Cells("fcLoanTypeName").Value.ToString
            frmLoan_PaymentHistory.GetSetBalance() = .Cells("fdBalance").Value.ToString
            frmLoan_PaymentHistory.GetSetTotalPayment() = .Cells("fdAmountPaid").Value.ToString
        End With
    End Sub

    Private Sub SelectViewReport_BasedOnTransactionType()
        Select Case cboPaymentType.Text
            Case "Loan Pretermination"
                CallLoanPreterminationReport()
            Case "Amortization Settlement"
                CallAmortizationSettlementReport()
        End Select
    End Sub

    Private Sub CallLoanPreterminationReport()
        Dim loanID As String = grdLoanList.Item("pk_Members_Loan", grdLoanList.SelectedRows(0).Index).Value.ToString()

        'Fill In required Report Properties
        With frmRpt_LoanPreterminationSettlement
            .GetEmployeeNo() = Me.GetEmployeeNo()
            .GetDate() = Date.Now.Date()
            .GetUser() = CIMSfunctions.GetLoggedInUserFullName()
            .GetLoanID = loanID
            .GetAdjustedInterestRefund() = Me.GetAdjustedInterestRefund()
        End With

        'Finally Load The Report
        frmRpt_LoanPreterminationSettlement.Show()
    End Sub

    Private Sub CallAmortizationSettlementReport()
        Dim loanID As String = grdLoanList.Item("pk_Members_Loan", grdLoanList.SelectedRows(0).Index).Value.ToString()

        'Fill in required Properties
        With frmRpt_AmortizationSettlementReport
            .GetEmployeeNo() = Me.GetEmployeeNo()
            .GetAsOfDate() = Date.Now.Date()
            .GetUser() = CIMSfunctions.GetLoggedInUserFullName()
            .GetLoanID() = loanID
            .GetAmortization() = txtNetDue.Text
        End With

        'Finally Load The Report
        frmRpt_AmortizationSettlementReport.Show()
    End Sub

    Private Sub btnView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnView.Click
        Try
            Dim loanID As String = grdLoanList.Item("pk_Members_Loan", grdLoanList.SelectedRows(0).Index).Value.ToString()
            SelectViewReport_BasedOnTransactionType()
        Catch ex As Exception
            MessageBox.Show("User Error!: Please Select Loan first before proceeding.", "User Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub txtSearchBox_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSearchBox.KeyPress
        If e.KeyChar = Chr(13) Then
            SearchMember(txtSearchBox.Text)
        End If
    End Sub

#Region "NEW"




#Region "View Saving Deposit"
    Private Sub ViewSavingsDeposit(ByVal Empinfo As String)
        Dim sSqlCmd As String = "MSS_LoanTracking_GetSavingsDeposit '" & Empinfo & "'"
        Try
            grdSavingDeposit.DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSqlCmd).Tables(0).DefaultView
            grdSavingDeposit.Columns(2).DefaultCellStyle.Format = "#,##0.00"
            grdSavingDeposit.Columns(3).DefaultCellStyle.Format = "#,##0.00"
            grdSavingDeposit.Columns(4).DefaultCellStyle.Format = "#,##0.00"
            grdSavingDeposit.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            grdSavingDeposit.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            grdSavingDeposit.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        Catch ex As Exception
            MsgBox("Error at loadofSavings in FrmCashPaymentMaster" & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try

    End Sub
#End Region
#Region "View Time Deposit"
    Private Sub ViewTimeDeposit(ByVal Empinof As String)
        Dim sSqlCmd As String = "MSS_LoanTracking_GetTimeDeposit '" & Empinof & "'"
        Try
            grdSavingDeposit.DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSqlCmd).Tables(0).DefaultView
            grdSavingDeposit.Columns(3).DefaultCellStyle.Format = "#,##0.00"
            grdSavingDeposit.Columns(4).DefaultCellStyle.Format = "#,##0.00"
            grdSavingDeposit.Columns(5).DefaultCellStyle.Format = "#,##0.00"
            grdSavingDeposit.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            grdSavingDeposit.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            grdSavingDeposit.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            grdSavingDeposit.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        Catch ex As Exception
            MsgBox("Error at loadofSavings in FrmCashPaymentMaster" & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try
    End Sub
#End Region

#End Region
End Class