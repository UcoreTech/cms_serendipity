﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmLoanLedger
    'Created by : Vincent Nacar
    'Module : Loan Ledger
    'May 26,2014

    Private gcon As New Clsappconfiguration

    Private Sub frmLoanLedger_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadAmortizationStatus() 'test
        LoadLoans()
    End Sub

    Private Sub LoadAmortizationStatus()
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(gcon.cnstring, "_CIMS_LoanLedger_SelectAmortization",
                                        New SqlParameter("@LoanNo", txtLoanNo.Text))
        dgvList.DataSource = ds.Tables(0)
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Select Case btnSearch.Text
            Case "SEARCH"
                LoadLoans()
                pnlLoan.Visible = True
                btnSearch.Text = "CANCEL"
            Case "CANCEL"
                pnlLoan.Visible = False
                btnSearch.Text = "SEARCH"
        End Select
    End Sub

    Private Sub LoadLoans()
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(gcon.cnstring, "_Select_ApprovedLoans")
        dgvLoanList.DataSource = ds.Tables(0)
        dgvLoanList.Columns(0).Width = 100
    End Sub

    Private Sub LoadOtherInfo()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_Info_LoanLedger",
                                      New SqlParameter("@LoanNo", txtLoanNo.Text))
        While rd.Read
            txtLoanName.Text = rd("Loan Name").ToString
            txtLoanAmount.Text = rd("Loan Amount").ToString
            txtAmortization.Text = rd("Amortization").ToString
            txtTerms.Text = rd("Terms").ToString
            txtInterest.Text = rd("Interest").ToString
            txtMethod.Text = rd("Interest Method").ToString
            txtAmountPaid.Text = rd("Amount Paid").ToString
            txtBalance.Text = rd("Balance").ToString
            txtApplicationDate.Text = Format(rd("Application Date"), "MM/dd/yyyy")
        End While
    End Sub

    Private Sub btnCLose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCLose.Click
        Me.Close()
    End Sub

    Private Sub dgvLoanList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLoanList.Click
        With dgvLoanList
            txtLoanNo.Text = .SelectedRows(0).Cells(0).Value.ToString
            lblCLientName.Text = "Loan Ledger (" + .SelectedRows(0).Cells(1).Value.ToString + ")"
            LoadOtherInfo()
            LoadAmortizationStatus()
        End With
    End Sub

    'Private Sub dgvLoanList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLoanList.DoubleClick

    'End Sub
End Class