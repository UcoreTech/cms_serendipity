<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCashPayment_Main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCashPayment_Main))
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.lstNames = New System.Windows.Forms.ListView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtSearchBox = New System.Windows.Forms.TextBox()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.PanePanel6 = New WindowsApplication2.PanePanel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.grdSavingDeposit = New System.Windows.Forms.DataGridView()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.lblCapShare = New System.Windows.Forms.Label()
        Me.lblServiceFee = New System.Windows.Forms.Label()
        Me.lblInterest = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtAmortization = New System.Windows.Forms.TextBox()
        Me.txtCapital = New System.Windows.Forms.TextBox()
        Me.txtServiceFee = New System.Windows.Forms.TextBox()
        Me.txtInterest = New System.Windows.Forms.TextBox()
        Me.txtTerms = New System.Windows.Forms.TextBox()
        Me.txtPrincipal = New System.Windows.Forms.TextBox()
        Me.txtLoanNo = New System.Windows.Forms.TextBox()
        Me.txtNetDue = New System.Windows.Forms.TextBox()
        Me.txtInterestRefund = New System.Windows.Forms.TextBox()
        Me.txtTotalPayment = New System.Windows.Forms.TextBox()
        Me.txtPenalty = New System.Windows.Forms.TextBox()
        Me.txtReceivable = New System.Windows.Forms.TextBox()
        Me.txtStatus = New System.Windows.Forms.TextBox()
        Me.txtLoanAppDate = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lblReceivable = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.PanePanel5 = New WindowsApplication2.PanePanel()
        Me.lblLoanDetailTitle = New System.Windows.Forms.Label()
        Me.grdLoanList = New System.Windows.Forms.DataGridView()
        Me.PanePanel3 = New WindowsApplication2.PanePanel()
        Me.lblPanel2 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnView = New System.Windows.Forms.Button()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.btnReceivePymnt = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.PanePanel4 = New WindowsApplication2.PanePanel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cboPaymentType = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.PanePanel2 = New WindowsApplication2.PanePanel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.PanePanel1.SuspendLayout()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.PanePanel6.SuspendLayout()
        CType(Me.grdSavingDeposit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.PanePanel5.SuspendLayout()
        CType(Me.grdLoanList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanePanel3.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.PanePanel4.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.PanePanel2.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.TabControl1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.PanePanel1)
        Me.SplitContainer1.Panel1MinSize = 250
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.SplitContainer2)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Size = New System.Drawing.Size(938, 499)
        Me.SplitContainer1.SplitterDistance = 250
        Me.SplitContainer1.SplitterWidth = 5
        Me.SplitContainer1.TabIndex = 0
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 30)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(246, 465)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.lstNames)
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 24)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(238, 437)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Members"
        '
        'lstNames
        '
        Me.lstNames.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstNames.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstNames.HideSelection = False
        Me.lstNames.Location = New System.Drawing.Point(9, 64)
        Me.lstNames.MultiSelect = False
        Me.lstNames.Name = "lstNames"
        Me.lstNames.Size = New System.Drawing.Size(220, 347)
        Me.lstNames.TabIndex = 1
        Me.lstNames.UseCompatibleStateImageBehavior = False
        Me.lstNames.View = System.Windows.Forms.View.Details
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.txtSearchBox)
        Me.GroupBox1.Controls.Add(Me.btnSearch)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 7)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(220, 51)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Search Option"
        '
        'txtSearchBox
        '
        Me.txtSearchBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchBox.Location = New System.Drawing.Point(7, 20)
        Me.txtSearchBox.Name = "txtSearchBox"
        Me.txtSearchBox.Size = New System.Drawing.Size(119, 23)
        Me.txtSearchBox.TabIndex = 1
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearch.Image = Global.WindowsApplication2.My.Resources.Resources.Search
        Me.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSearch.Location = New System.Drawing.Point(134, 17)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(79, 27)
        Me.btnSearch.TabIndex = 0
        Me.btnSearch.Text = "Search"
        Me.btnSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'PanePanel1
        '
        Me.PanePanel1.ActiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel1.ActiveGradientLowColor = System.Drawing.Color.LawnGreen
        Me.PanePanel1.Controls.Add(Me.Label1)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.LawnGreen
        Me.PanePanel1.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(246, 30)
        Me.PanePanel1.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(4, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(87, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Members"
        '
        'SplitContainer2
        '
        Me.SplitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 75)
        Me.SplitContainer2.Name = "SplitContainer2"
        Me.SplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.BackColor = System.Drawing.SystemColors.Window
        Me.SplitContainer2.Panel1.Controls.Add(Me.Panel2)
        Me.SplitContainer2.Panel1.Controls.Add(Me.Label7)
        Me.SplitContainer2.Panel1.Controls.Add(Me.GroupBox2)
        Me.SplitContainer2.Panel1.Controls.Add(Me.txtNetDue)
        Me.SplitContainer2.Panel1.Controls.Add(Me.txtInterestRefund)
        Me.SplitContainer2.Panel1.Controls.Add(Me.txtTotalPayment)
        Me.SplitContainer2.Panel1.Controls.Add(Me.txtPenalty)
        Me.SplitContainer2.Panel1.Controls.Add(Me.txtReceivable)
        Me.SplitContainer2.Panel1.Controls.Add(Me.txtStatus)
        Me.SplitContainer2.Panel1.Controls.Add(Me.txtLoanAppDate)
        Me.SplitContainer2.Panel1.Controls.Add(Me.Label15)
        Me.SplitContainer2.Panel1.Controls.Add(Me.Label14)
        Me.SplitContainer2.Panel1.Controls.Add(Me.Label13)
        Me.SplitContainer2.Panel1.Controls.Add(Me.Label12)
        Me.SplitContainer2.Panel1.Controls.Add(Me.Label11)
        Me.SplitContainer2.Panel1.Controls.Add(Me.lblReceivable)
        Me.SplitContainer2.Panel1.Controls.Add(Me.Label8)
        Me.SplitContainer2.Panel1.Controls.Add(Me.Label9)
        Me.SplitContainer2.Panel1.Controls.Add(Me.Label6)
        Me.SplitContainer2.Panel1.Controls.Add(Me.PanePanel5)
        Me.SplitContainer2.Panel1.Controls.Add(Me.grdLoanList)
        Me.SplitContainer2.Panel1.Controls.Add(Me.PanePanel3)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.BackColor = System.Drawing.SystemColors.Window
        Me.SplitContainer2.Panel2.Controls.Add(Me.FlowLayoutPanel1)
        Me.SplitContainer2.Panel2.Controls.Add(Me.btnSave)
        Me.SplitContainer2.Panel2.Controls.Add(Me.btnCancel)
        Me.SplitContainer2.Panel2.Controls.Add(Me.PanePanel4)
        Me.SplitContainer2.Size = New System.Drawing.Size(683, 424)
        Me.SplitContainer2.SplitterDistance = 299
        Me.SplitContainer2.SplitterWidth = 5
        Me.SplitContainer2.TabIndex = 1
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel2.Controls.Add(Me.PanePanel6)
        Me.Panel2.Controls.Add(Me.grdSavingDeposit)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, -83)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(679, 378)
        Me.Panel2.TabIndex = 8
        Me.Panel2.Visible = False
        '
        'PanePanel6
        '
        Me.PanePanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel6.Controls.Add(Me.Label19)
        Me.PanePanel6.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel6.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel6.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel6.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel6.InactiveGradientLowColor = System.Drawing.Color.White
        Me.PanePanel6.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel6.Name = "PanePanel6"
        Me.PanePanel6.Size = New System.Drawing.Size(675, 48)
        Me.PanePanel6.TabIndex = 53
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(20, 10)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(79, 19)
        Me.Label19.TabIndex = 14
        Me.Label19.Text = "Deposits"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'grdSavingDeposit
        '
        Me.grdSavingDeposit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdSavingDeposit.Location = New System.Drawing.Point(0, 91)
        Me.grdSavingDeposit.Name = "grdSavingDeposit"
        Me.grdSavingDeposit.ReadOnly = True
        Me.grdSavingDeposit.Size = New System.Drawing.Size(850, 215)
        Me.grdSavingDeposit.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(181, 59)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(130, 15)
        Me.Label7.TabIndex = 5
        Me.Label7.Text = "Loan Application Date:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.Label22)
        Me.GroupBox2.Controls.Add(Me.lblCapShare)
        Me.GroupBox2.Controls.Add(Me.lblServiceFee)
        Me.GroupBox2.Controls.Add(Me.lblInterest)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.txtAmortization)
        Me.GroupBox2.Controls.Add(Me.txtCapital)
        Me.GroupBox2.Controls.Add(Me.txtServiceFee)
        Me.GroupBox2.Controls.Add(Me.txtInterest)
        Me.GroupBox2.Controls.Add(Me.txtTerms)
        Me.GroupBox2.Controls.Add(Me.txtPrincipal)
        Me.GroupBox2.Controls.Add(Me.txtLoanNo)
        Me.GroupBox2.Location = New System.Drawing.Point(2, 70)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(313, 222)
        Me.GroupBox2.TabIndex = 7
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Loan Details"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(75, 195)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(79, 15)
        Me.Label22.TabIndex = 5
        Me.Label22.Text = "Amortization:"
        '
        'lblCapShare
        '
        Me.lblCapShare.Location = New System.Drawing.Point(10, 165)
        Me.lblCapShare.Name = "lblCapShare"
        Me.lblCapShare.Size = New System.Drawing.Size(146, 15)
        Me.lblCapShare.TabIndex = 5
        Me.lblCapShare.Text = "Capital Share (0%):"
        Me.lblCapShare.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblServiceFee
        '
        Me.lblServiceFee.Location = New System.Drawing.Point(24, 135)
        Me.lblServiceFee.Name = "lblServiceFee"
        Me.lblServiceFee.Size = New System.Drawing.Size(132, 15)
        Me.lblServiceFee.TabIndex = 5
        Me.lblServiceFee.Text = "Service Fee (0%):"
        Me.lblServiceFee.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblInterest
        '
        Me.lblInterest.Location = New System.Drawing.Point(42, 106)
        Me.lblInterest.Name = "lblInterest"
        Me.lblInterest.Size = New System.Drawing.Size(114, 15)
        Me.lblInterest.TabIndex = 5
        Me.lblInterest.Text = "Interest (0%):"
        Me.lblInterest.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(79, 76)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(76, 15)
        Me.Label18.TabIndex = 5
        Me.Label18.Text = "Terms (mos):"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(95, 46)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(61, 15)
        Me.Label17.TabIndex = 5
        Me.Label17.Text = "Principal:"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(99, 16)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(57, 15)
        Me.Label16.TabIndex = 5
        Me.Label16.Text = "Loan No.:"
        '
        'txtAmortization
        '
        Me.txtAmortization.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAmortization.BackColor = System.Drawing.SystemColors.Window
        Me.txtAmortization.Location = New System.Drawing.Point(160, 192)
        Me.txtAmortization.Name = "txtAmortization"
        Me.txtAmortization.ReadOnly = True
        Me.txtAmortization.Size = New System.Drawing.Size(137, 23)
        Me.txtAmortization.TabIndex = 6
        Me.txtAmortization.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCapital
        '
        Me.txtCapital.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCapital.BackColor = System.Drawing.SystemColors.Window
        Me.txtCapital.Location = New System.Drawing.Point(160, 162)
        Me.txtCapital.Name = "txtCapital"
        Me.txtCapital.ReadOnly = True
        Me.txtCapital.Size = New System.Drawing.Size(137, 23)
        Me.txtCapital.TabIndex = 6
        Me.txtCapital.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtServiceFee
        '
        Me.txtServiceFee.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtServiceFee.BackColor = System.Drawing.SystemColors.Window
        Me.txtServiceFee.Location = New System.Drawing.Point(160, 132)
        Me.txtServiceFee.Name = "txtServiceFee"
        Me.txtServiceFee.ReadOnly = True
        Me.txtServiceFee.Size = New System.Drawing.Size(137, 23)
        Me.txtServiceFee.TabIndex = 6
        Me.txtServiceFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtInterest
        '
        Me.txtInterest.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtInterest.BackColor = System.Drawing.SystemColors.Window
        Me.txtInterest.Location = New System.Drawing.Point(160, 103)
        Me.txtInterest.Name = "txtInterest"
        Me.txtInterest.ReadOnly = True
        Me.txtInterest.Size = New System.Drawing.Size(141, 23)
        Me.txtInterest.TabIndex = 6
        Me.txtInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTerms
        '
        Me.txtTerms.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTerms.BackColor = System.Drawing.SystemColors.Window
        Me.txtTerms.Location = New System.Drawing.Point(160, 73)
        Me.txtTerms.Name = "txtTerms"
        Me.txtTerms.ReadOnly = True
        Me.txtTerms.Size = New System.Drawing.Size(141, 23)
        Me.txtTerms.TabIndex = 6
        Me.txtTerms.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPrincipal
        '
        Me.txtPrincipal.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPrincipal.BackColor = System.Drawing.SystemColors.Window
        Me.txtPrincipal.Location = New System.Drawing.Point(160, 43)
        Me.txtPrincipal.Name = "txtPrincipal"
        Me.txtPrincipal.ReadOnly = True
        Me.txtPrincipal.Size = New System.Drawing.Size(141, 23)
        Me.txtPrincipal.TabIndex = 6
        Me.txtPrincipal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLoanNo
        '
        Me.txtLoanNo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtLoanNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtLoanNo.Location = New System.Drawing.Point(160, 13)
        Me.txtLoanNo.Name = "txtLoanNo"
        Me.txtLoanNo.ReadOnly = True
        Me.txtLoanNo.Size = New System.Drawing.Size(141, 23)
        Me.txtLoanNo.TabIndex = 6
        Me.txtLoanNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNetDue
        '
        Me.txtNetDue.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNetDue.BackColor = System.Drawing.SystemColors.Window
        Me.txtNetDue.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNetDue.Location = New System.Drawing.Point(470, 253)
        Me.txtNetDue.Name = "txtNetDue"
        Me.txtNetDue.ReadOnly = True
        Me.txtNetDue.Size = New System.Drawing.Size(207, 29)
        Me.txtNetDue.TabIndex = 6
        Me.txtNetDue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtInterestRefund
        '
        Me.txtInterestRefund.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtInterestRefund.BackColor = System.Drawing.SystemColors.Window
        Me.txtInterestRefund.Location = New System.Drawing.Point(510, 223)
        Me.txtInterestRefund.Name = "txtInterestRefund"
        Me.txtInterestRefund.ReadOnly = True
        Me.txtInterestRefund.Size = New System.Drawing.Size(167, 23)
        Me.txtInterestRefund.TabIndex = 6
        Me.txtInterestRefund.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalPayment
        '
        Me.txtTotalPayment.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotalPayment.BackColor = System.Drawing.SystemColors.Window
        Me.txtTotalPayment.Location = New System.Drawing.Point(510, 194)
        Me.txtTotalPayment.Name = "txtTotalPayment"
        Me.txtTotalPayment.ReadOnly = True
        Me.txtTotalPayment.Size = New System.Drawing.Size(167, 23)
        Me.txtTotalPayment.TabIndex = 6
        Me.txtTotalPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPenalty
        '
        Me.txtPenalty.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPenalty.BackColor = System.Drawing.SystemColors.Window
        Me.txtPenalty.Location = New System.Drawing.Point(510, 134)
        Me.txtPenalty.Name = "txtPenalty"
        Me.txtPenalty.ReadOnly = True
        Me.txtPenalty.Size = New System.Drawing.Size(167, 23)
        Me.txtPenalty.TabIndex = 6
        Me.txtPenalty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtReceivable
        '
        Me.txtReceivable.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtReceivable.BackColor = System.Drawing.SystemColors.Window
        Me.txtReceivable.Location = New System.Drawing.Point(510, 104)
        Me.txtReceivable.Name = "txtReceivable"
        Me.txtReceivable.ReadOnly = True
        Me.txtReceivable.Size = New System.Drawing.Size(167, 23)
        Me.txtReceivable.TabIndex = 6
        Me.txtReceivable.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtStatus
        '
        Me.txtStatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtStatus.BackColor = System.Drawing.SystemColors.Window
        Me.txtStatus.Location = New System.Drawing.Point(520, 56)
        Me.txtStatus.Name = "txtStatus"
        Me.txtStatus.ReadOnly = True
        Me.txtStatus.Size = New System.Drawing.Size(157, 23)
        Me.txtStatus.TabIndex = 6
        '
        'txtLoanAppDate
        '
        Me.txtLoanAppDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtLoanAppDate.BackColor = System.Drawing.SystemColors.Window
        Me.txtLoanAppDate.Location = New System.Drawing.Point(321, 56)
        Me.txtLoanAppDate.Name = "txtLoanAppDate"
        Me.txtLoanAppDate.ReadOnly = True
        Me.txtLoanAppDate.Size = New System.Drawing.Size(124, 23)
        Me.txtLoanAppDate.TabIndex = 6
        '
        'Label15
        '
        Me.Label15.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(321, 255)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(90, 26)
        Me.Label15.TabIndex = 5
        Me.Label15.Text = "Net Due:"
        '
        'Label14
        '
        Me.Label14.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(331, 226)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(93, 15)
        Me.Label14.TabIndex = 5
        Me.Label14.Text = "Interest Refund:"
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(327, 194)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(87, 15)
        Me.Label13.TabIndex = 5
        Me.Label13.Text = "Total Payment:"
        '
        'Label12
        '
        Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(321, 167)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(30, 15)
        Me.Label12.TabIndex = 5
        Me.Label12.Text = "Less"
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(331, 137)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(51, 15)
        Me.Label11.TabIndex = 5
        Me.Label11.Text = "Penalty:"
        '
        'lblReceivable
        '
        Me.lblReceivable.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblReceivable.AutoSize = True
        Me.lblReceivable.Location = New System.Drawing.Point(331, 107)
        Me.lblReceivable.Name = "lblReceivable"
        Me.lblReceivable.Size = New System.Drawing.Size(69, 15)
        Me.lblReceivable.TabIndex = 5
        Me.lblReceivable.Text = "Receivable:"
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(463, 55)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(44, 15)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "Status:"
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(322, 83)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(108, 19)
        Me.Label9.TabIndex = 5
        Me.Label9.Text = "Computations:"
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(3, 53)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(73, 19)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Extended"
        '
        'PanePanel5
        '
        Me.PanePanel5.ActiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel5.ActiveGradientLowColor = System.Drawing.Color.LawnGreen
        Me.PanePanel5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanePanel5.Controls.Add(Me.lblLoanDetailTitle)
        Me.PanePanel5.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel5.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel5.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel5.InactiveGradientLowColor = System.Drawing.Color.LawnGreen
        Me.PanePanel5.Location = New System.Drawing.Point(0, 26)
        Me.PanePanel5.Name = "PanePanel5"
        Me.PanePanel5.Size = New System.Drawing.Size(678, 24)
        Me.PanePanel5.TabIndex = 3
        '
        'lblLoanDetailTitle
        '
        Me.lblLoanDetailTitle.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblLoanDetailTitle.AutoSize = True
        Me.lblLoanDetailTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblLoanDetailTitle.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanDetailTitle.ForeColor = System.Drawing.Color.Black
        Me.lblLoanDetailTitle.Location = New System.Drawing.Point(219, 0)
        Me.lblLoanDetailTitle.Name = "lblLoanDetailTitle"
        Me.lblLoanDetailTitle.Size = New System.Drawing.Size(231, 23)
        Me.lblLoanDetailTitle.TabIndex = 0
        Me.lblLoanDetailTitle.Text = "Title base on Payment Type:"
        Me.lblLoanDetailTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'grdLoanList
        '
        Me.grdLoanList.AllowUserToAddRows = False
        Me.grdLoanList.AllowUserToDeleteRows = False
        Me.grdLoanList.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdLoanList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.grdLoanList.BackgroundColor = System.Drawing.Color.White
        Me.grdLoanList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdLoanList.GridColor = System.Drawing.Color.Black
        Me.grdLoanList.Location = New System.Drawing.Point(-2, 17)
        Me.grdLoanList.MultiSelect = False
        Me.grdLoanList.Name = "grdLoanList"
        Me.grdLoanList.ReadOnly = True
        Me.grdLoanList.RowHeadersVisible = False
        Me.grdLoanList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdLoanList.Size = New System.Drawing.Size(681, 8)
        Me.grdLoanList.TabIndex = 3
        '
        'PanePanel3
        '
        Me.PanePanel3.ActiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel3.ActiveGradientLowColor = System.Drawing.Color.LawnGreen
        Me.PanePanel3.Controls.Add(Me.lblPanel2)
        Me.PanePanel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel3.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel3.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel3.InactiveGradientLowColor = System.Drawing.Color.LawnGreen
        Me.PanePanel3.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel3.Name = "PanePanel3"
        Me.PanePanel3.Size = New System.Drawing.Size(679, 19)
        Me.PanePanel3.TabIndex = 2
        '
        'lblPanel2
        '
        Me.lblPanel2.AutoSize = True
        Me.lblPanel2.BackColor = System.Drawing.Color.Transparent
        Me.lblPanel2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPanel2.ForeColor = System.Drawing.Color.Black
        Me.lblPanel2.Location = New System.Drawing.Point(7, 2)
        Me.lblPanel2.Name = "lblPanel2"
        Me.lblPanel2.Size = New System.Drawing.Size(148, 15)
        Me.lblPanel2.TabIndex = 0
        Me.lblPanel2.Text = "Select a loan to [optional]"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnView)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnEdit)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnReceivePymnt)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnClose)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(2, 25)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(460, 38)
        Me.FlowLayoutPanel1.TabIndex = 4
        '
        'btnView
        '
        Me.btnView.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnView.Image = Global.WindowsApplication2.My.Resources.Resources.PrintPreview
        Me.btnView.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnView.Location = New System.Drawing.Point(3, 3)
        Me.btnView.Name = "btnView"
        Me.btnView.Size = New System.Drawing.Size(87, 32)
        Me.btnView.TabIndex = 3
        Me.btnView.Text = "View"
        Me.btnView.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnView.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Enabled = False
        Me.btnEdit.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Image = Global.WindowsApplication2.My.Resources.Resources.editEntry
        Me.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEdit.Location = New System.Drawing.Point(96, 3)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(87, 32)
        Me.btnEdit.TabIndex = 3
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnReceivePymnt
        '
        Me.btnReceivePymnt.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReceivePymnt.Image = Global.WindowsApplication2.My.Resources.Resources.Coins
        Me.btnReceivePymnt.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnReceivePymnt.Location = New System.Drawing.Point(189, 3)
        Me.btnReceivePymnt.Name = "btnReceivePymnt"
        Me.btnReceivePymnt.Size = New System.Drawing.Size(173, 32)
        Me.btnReceivePymnt.TabIndex = 3
        Me.btnReceivePymnt.Text = "Receive Payment"
        Me.btnReceivePymnt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnReceivePymnt.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = Global.WindowsApplication2.My.Resources.Resources.exit17
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.Location = New System.Drawing.Point(368, 3)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(87, 32)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "Close"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Image = Global.WindowsApplication2.My.Resources.Resources.Floppy
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.Location = New System.Drawing.Point(482, 28)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(87, 32)
        Me.btnSave.TabIndex = 3
        Me.btnSave.Text = "Save"
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = True
        Me.btnSave.Visible = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = Global.WindowsApplication2.My.Resources.Resources.button_cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.Location = New System.Drawing.Point(575, 28)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(94, 32)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCancel.UseVisualStyleBackColor = True
        Me.btnCancel.Visible = False
        '
        'PanePanel4
        '
        Me.PanePanel4.ActiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel4.ActiveGradientLowColor = System.Drawing.Color.LawnGreen
        Me.PanePanel4.Controls.Add(Me.Label4)
        Me.PanePanel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel4.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel4.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel4.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel4.InactiveGradientLowColor = System.Drawing.Color.LawnGreen
        Me.PanePanel4.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel4.Name = "PanePanel4"
        Me.PanePanel4.Size = New System.Drawing.Size(679, 24)
        Me.PanePanel4.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(5, 2)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(186, 19)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "What do you want to do?"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.Window
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.cboPaymentType)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.PanePanel2)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(683, 75)
        Me.Panel1.TabIndex = 0
        '
        'cboPaymentType
        '
        Me.cboPaymentType.FormattingEnabled = True
        Me.cboPaymentType.Location = New System.Drawing.Point(83, 47)
        Me.cboPaymentType.Name = "cboPaymentType"
        Me.cboPaymentType.Size = New System.Drawing.Size(206, 23)
        Me.cboPaymentType.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(81, 27)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(119, 15)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Select Payment Type:"
        '
        'PanePanel2
        '
        Me.PanePanel2.ActiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel2.ActiveGradientLowColor = System.Drawing.Color.LawnGreen
        Me.PanePanel2.Controls.Add(Me.Label2)
        Me.PanePanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel2.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel2.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel2.InactiveGradientLowColor = System.Drawing.Color.LawnGreen
        Me.PanePanel2.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel2.Name = "PanePanel2"
        Me.PanePanel2.Size = New System.Drawing.Size(679, 24)
        Me.PanePanel2.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(4, 1)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(146, 19)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Payment Processing"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.WindowsApplication2.My.Resources.Resources.cash
        Me.PictureBox1.Location = New System.Drawing.Point(0, 16)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(82, 70)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 3
        Me.PictureBox1.TabStop = False
        '
        'frmCashPayment_Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(938, 499)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmCashPayment_Main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Receive Payment"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.PerformLayout()
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        Me.SplitContainer2.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.PanePanel6.ResumeLayout(False)
        Me.PanePanel6.PerformLayout()
        CType(Me.grdSavingDeposit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.PanePanel5.ResumeLayout(False)
        Me.PanePanel5.PerformLayout()
        CType(Me.grdLoanList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanePanel3.ResumeLayout(False)
        Me.PanePanel3.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.PanePanel4.ResumeLayout(False)
        Me.PanePanel4.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.PanePanel2.ResumeLayout(False)
        Me.PanePanel2.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtSearchBox As System.Windows.Forms.TextBox
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents PanePanel3 As WindowsApplication2.PanePanel
    Friend WithEvents lblPanel2 As System.Windows.Forms.Label
    Friend WithEvents PanePanel4 As WindowsApplication2.PanePanel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents PanePanel2 As WindowsApplication2.PanePanel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents grdLoanList As System.Windows.Forms.DataGridView
    Friend WithEvents cboPaymentType As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents PanePanel5 As WindowsApplication2.PanePanel
    Friend WithEvents lblLoanDetailTitle As System.Windows.Forms.Label
    Friend WithEvents txtStatus As System.Windows.Forms.TextBox
    Friend WithEvents txtLoanAppDate As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtNetDue As System.Windows.Forms.TextBox
    Friend WithEvents txtInterestRefund As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalPayment As System.Windows.Forms.TextBox
    Friend WithEvents txtPenalty As System.Windows.Forms.TextBox
    Friend WithEvents txtReceivable As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lblReceivable As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents lblCapShare As System.Windows.Forms.Label
    Friend WithEvents lblServiceFee As System.Windows.Forms.Label
    Friend WithEvents lblInterest As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtServiceFee As System.Windows.Forms.TextBox
    Friend WithEvents txtInterest As System.Windows.Forms.TextBox
    Friend WithEvents txtTerms As System.Windows.Forms.TextBox
    Friend WithEvents txtPrincipal As System.Windows.Forms.TextBox
    Friend WithEvents txtLoanNo As System.Windows.Forms.TextBox
    Friend WithEvents txtAmortization As System.Windows.Forms.TextBox
    Friend WithEvents txtCapital As System.Windows.Forms.TextBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnReceivePymnt As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnView As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents lstNames As System.Windows.Forms.ListView
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents grdSavingDeposit As System.Windows.Forms.DataGridView
    Friend WithEvents PanePanel6 As WindowsApplication2.PanePanel
    Friend WithEvents Label19 As System.Windows.Forms.Label
End Class
