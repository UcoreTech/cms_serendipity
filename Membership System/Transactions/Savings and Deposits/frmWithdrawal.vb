Imports system.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports WMPs_Money_Figure_Convert_to_Words

Public Class frmWithdrawal
    Private gcon As New Clsappconfiguration
    Public serialNo As Integer

#Region "Property"
    Private idNo As String
    Public Property GetIDNo() As String
        Get
            Return idNo
        End Get
        Set(ByVal value As String)
            idNo = value
        End Set
    End Property

    Private Name As String
    Public Property GetName() As String
        Get
            Return Name
        End Get
        Set(ByVal value As String)
            Name = value
        End Set
    End Property

    Private RefNoMember As String
    Public Property GetRefNoForMember() As String
        Get
            Return RefNoMember
        End Get
        Set(ByVal value As String)
            RefNoMember = value
        End Set
    End Property

    Private SelectSTD As String
    Public Property GetSelectSTD() As String
        Get
            Return SelectSTD
        End Get
        Set(ByVal value As String)
            SelectSTD = value
        End Set
    End Property

    Private RefNo As String
    Public Property GetRefNo() As String
        Get
            Return RefNo
        End Get
        Set(ByVal value As String)
            RefNo = value
        End Set
    End Property

    Private UniqEmp As String
    Public Property GetUniqEmp() As String
        Get
            Return UniqEmp
        End Get
        Set(ByVal value As String)
            UniqEmp = value
        End Set
    End Property
#End Region

    Private Sub Withdrawal_Savings(ByVal wdate As Date, ByVal descr As String, ByVal empID As String, _
                                    ByVal wAmount As Integer, ByVal clear As Boolean, ByVal RefNo As String, ByVal fcDocNo As String, ByVal KeyAccount As String)
        gcon.sqlconn.Open()
        Dim guid As New Guid(KeyAccount)
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "[CIMS_m_Withdrawal_Savings_Deposit]", _
                                    New SqlParameter("@dtDepdate", wdate),
                                    New SqlParameter("@fcDes", descr), _
                                    New SqlParameter("@Emp", empID),
                                    New SqlParameter("@fdWithdrawalAmount", wAmount),
                                    New SqlParameter("@clear", clear), _
                                    New SqlParameter("@fcRefNo", RefNo), _
                                    New SqlParameter("@fcDocNumber", fcDocNo), _
                                    New SqlParameter("@fxKey_Account", guid))

            trans.Commit()
            UpdateAccountInAccounting(fcDocNo, Date.Now)
            gcon.sqlconn.Close()
            MessageBox.Show("Success!")
            Me.Close()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.ToString, "Withdrawal")
        End Try
    End Sub

    Private Sub UpdateAccountInAccounting(ByVal fcDocNumber As String, ByVal fdDateUsed As Date)
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, "CIMS_Member_AccountRegister_UpdateAccount",
                                      New SqlParameter("@fcDocNumber", fcDocNumber),
                                      New SqlParameter("@fdDateUsed", fdDateUsed))
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Update Document Number....", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub Withdrawal_Time_Deposit(ByVal wdate As Date, ByVal descr As String, ByVal empID As String, _
                                    ByVal wAmount As Integer, ByVal clear As Boolean)
                                    
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "[CIMS_m_Withdrawal_Time_Deposit]", _
                                New SqlParameter("@dtWdrawdate", wdate), New SqlParameter("@fcDes", descr), _
                             New SqlParameter("@Emp", empID), New SqlParameter("@fdWithdrawalAmount", wAmount), New SqlParameter("@clear", clear))
            trans.Commit()
            MessageBox.Show("Success!")
        Catch ex As Exception
            trans.Rollback()
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub frmWithdrawal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtID.Text = GetIDNo()
        txtname.Text = GetName()
        'txtRefNo.Text = GetRefNo()
        'txtDescription.Text = GetSelectSTD()
    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Close()
    End Sub
    Private Sub btnwDrawal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnwDrawal.Click
        'If SelectSTD = "Savings" Then
        If txtRefNo.Text <> "" Then
            Withdrawal_Savings(dtDate.Value, txtDescription.Text, GetUniqEmp(), Me.txtAmount.Text, 1, RefNoMember, txtRefNo.Text, txtDescription.Tag.ToString)
            Generate_JournalNo_Serial(Date.Now, "WS")
            AccountingEntry_Deposit(Date.Now, False, GetIDNo, txtDescription.Text, txtAmount.Text, getusername, "WS", serialNo,
                                    txtRefNo.Text, False, RefNoMember)
        Else
            MessageBox.Show("Please select Document Number!", "Withdrawal", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
        'Accounting Integration (Savings Withdrawal)
        'Call AccountingEntry_Deposit(txtID.Text, txtAmount.Text, txtDescription.Text, dtDate.Value, frmMain.username, )
        'Else
        '    Withdrawal_Time_Deposit(dtDate.Value, "Withdrawal_Time_Deposit", GetUniqEmp(), txtAmount.Text, 1)

        '    'Accounting Integration (Time Withdrawal)
        '    Call AccountingEntry_TimeWithdrawal(txtID.Text, txtAmount.Text, "Withdrawal", dtDate.Value, frmMain.username)
        'End If
    End Sub

    'Private Sub AccountingEntry_TimeWithdrawal(ByVal empNo As String, ByVal amount As Decimal, ByVal particulars As String, ByVal effectiveDate As Date, ByVal user As String)
    '    Try
    '        SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_TimeWithdrawal_AccountingEntries_ToGenJournal", _
    '            New SqlParameter("@employeeNo", empNo), _
    '            New SqlParameter("@amountWithdrawn", amount), _
    '            New SqlParameter("@particulars", particulars), _
    '            New SqlParameter("@effectiveDate", effectiveDate), _
    '            New SqlParameter("@user", user))
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Private Sub AccountingEntry_SavingsWithdrawal(ByVal empNo As String, ByVal amount As Decimal, ByVal particulars As String, ByVal effectiveDate As Date, ByVal user As String)
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_SavingsWithdrawal_AccountingEntries_ToGenJournal", _
                New SqlParameter("@employeeNo", empNo), _
                New SqlParameter("@amountWithdrawn", amount), _
                New SqlParameter("@particulars", particulars), _
                New SqlParameter("@effectiveDate", effectiveDate), _
                New SqlParameter("@user", user))
        Catch ex As Exception
            'Throw ex
        End Try
    End Sub

    Private Sub txtAmount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAmount.KeyPress
        If e.KeyChar <> Chr(46) Then
            If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
            If e.KeyChar = Chr(8) Then e.Handled = False
            If e.KeyChar = Chr(13) Then txtAmount.Focus()
        End If
    End Sub

#Region "SAVE ACCOUNTING ENTRY"
    Private Sub AccountingEntry_Deposit(ByVal fdTransDate As Date,
                                        ByVal fbAdjust As Boolean,
                                        ByVal fcEmployeeNo As String,
                                        ByVal fcMemo As String,
                                        ByVal fdTotAmt As Double,
                                        ByVal user As String,
                                        ByVal doctypeInitial As String,
                                        ByVal maxSerialNo As Integer,
                                        ByVal fiEntryNo As String,
                                        ByVal fbIsCancelled As Boolean,
                                        ByVal fcAccRef As String)
        Try
            If GetJVKeyID() = "" Then
                GetJVKeyID() = System.Guid.NewGuid.ToString()
            End If
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "[CAS_t_GeneralJournal_Header_InsertUpdate]", _
                New SqlParameter("@fxKeyJVNo", GetJVKeyID), _
                New SqlParameter("@fdTransDate", fdTransDate), _
                New SqlParameter("@fbAdjust", fbAdjust), _
                New SqlParameter("@fcEmployeeNo", fcEmployeeNo), _
                New SqlParameter("@fcMemo", fcMemo), _
                New SqlParameter("@fdTotAmt", fdTotAmt), _
                New SqlParameter("@user", user), _
                New SqlParameter("@doctypeInitial", doctypeInitial), _
                New SqlParameter("@maxSerialNo", maxSerialNo), _
                New SqlParameter("@fiEntryNo", fiEntryNo), _
                New SqlParameter("@fbIsCancelled", fbIsCancelled), _
                New SqlParameter("@fcAccRef", fcAccRef))
            If Saving_and_Deposit_New.cbosavtd.Text = "Debit" Then
                AccountingEntryItem_Deposit(txtRefNo.Text, txtDescription.Tag.ToString, 0, txtAmount.Text, txtDescription.Text,
                                         " ", " ", GetJVKeyID, Date.Now)
                AccountingEntryItem_Deposit(txtRefNo.Text, txtaccountname.Tag.ToString, txtAmount.Text, 0, txtDescription.Text,
                                         " ", " ", GetJVKeyID, Date.Now)
                'frmCreditDebit.GetAccount = Saving_and_Deposit_New.cbosavtd.Text
                'frmCreditDebit.txtRefNo.Text = Me.txtRefNo.Text
                'frmCreditDebit.txtamount.Text = Me.txtAmount.Text
                'frmCreditDebit.GetjvKeyID = GetJVKeyID
                'frmCreditDebit.GetActive = True
                'frmCreditDebit.Show()
            ElseIf Saving_and_Deposit_New.cbosavtd.Text = "Credit" Then
                AccountingEntryItem_Deposit(txtRefNo.Text, txtDescription.Tag.ToString, txtAmount.Text, 0, txtDescription.Text,
                                         " ", " ", GetJVKeyID, Date.Now)
                AccountingEntryItem_Deposit(txtRefNo.Text, txtaccountname.Tag.ToString, 0, txtAmount.Text, txtDescription.Text,
                                         " ", " ", GetJVKeyID, Date.Now)
                'frmCreditDebit.GetAccount = Saving_and_Deposit_New.cbosavtd.Text
                'frmCreditDebit.txtRefNo.Text = Me.txtRefNo.Text
                'frmCreditDebit.txtamount.Text = Me.txtAmount.Text
                'frmCreditDebit.GetjvKeyID = GetJVKeyID
                'frmCreditDebit.GetActive = True
                'frmCreditDebit.Show()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private jvKeyID As String
    Public Property GetJVKeyID() As String
        Get
            Return jvKeyID
        End Get
        Set(ByVal value As String)
            jvKeyID = value
        End Set
    End Property

    Private Sub AccountingEntryItem_Deposit(ByVal fiEntryNo As String,
                                      ByVal fxKeyAccount As String,
                                      ByVal fdDebit As Double,
                                      ByVal fdCredit As Double,
                                      ByVal fcMemo As String,
                                      ByVal fcNote As String,
                                      ByVal fxKeyNameID As String,
                                      ByVal fk_tJVEntry As String,
                                      ByVal transDate As Date)
        Try
            If GetJVKeyID() = "" Then
                GetJVKeyID() = System.Guid.NewGuid.ToString()
            End If
            Dim guid As New Guid(fxKeyAccount)
            Dim guid1 As New Guid(jvKeyID)
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "[usp_t_tJVEntry_details_save]", _
                New SqlParameter("@fiEntryNo", fiEntryNo), _
                New SqlParameter("@fxKeyAccount", guid), _
                New SqlParameter("@fdDebit", fdDebit), _
                New SqlParameter("@fdCredit", fdCredit), _
                New SqlParameter("@fcMemo", fcMemo), _
                New SqlParameter("@fcNote", fcNote), _
                New SqlParameter("@fk_JVHeader", guid1), _
                New SqlParameter("@transDate", transDate))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function Generate_JournalNo_Serial(ByVal transDate As Date, ByVal docTypeInitial As String) As Integer
        Dim gCon As New Clsappconfiguration
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "usp_m_JournalNo_GenerateSerialPerMonthPerYear", _
                                                            New SqlParameter("@date", transDate), _
                                                            New SqlParameter("@doctypeInitial", docTypeInitial))
            If rd.Read() Then
                serialNo = rd.Item("maxSerialNo")
            Else
                serialNo = 0
            End If
            Return serialNo
        End Using
    End Function
#End Region

    Private Sub btnBrowseAccountNumber_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseAccountNumber.Click
        frmMasterfile_AccountNumber.GetID = 1
        frmMasterfile_AccountNumber.ShowDialog()
    End Sub

    Private Sub btnBrowseAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseAccount.Click
        frmMasterfile_Accounts.GetActive = 4
        frmMasterfile_Accounts.ShowDialog()
    End Sub
End Class