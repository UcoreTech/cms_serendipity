﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports WMPs_Money_Figure_Convert_to_Words

Public Class frmCreditDebit
    Private GetAccounts, jvKeyID, fxKeyAccount As String
    Private active As Boolean
    Private gcon As New Clsappconfiguration

#Region "PROPERTY"
    Public Property GetAccount() As String
        Get
            Return GetAccounts
        End Get
        Set(ByVal value As String)
            GetAccounts = value
        End Set
    End Property

    Public Property GetActive() As Boolean
        Get
            Return active
        End Get
        Set(ByVal value As Boolean)
            active = value
        End Set
    End Property

    Public Property GetjvKeyID() As String
        Get
            Return jvKeyID
        End Get
        Set(ByVal value As String)
            jvKeyID = value
        End Set
    End Property
#End Region

    Private Sub frmCreditDebit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtaccountname.Clear()
        txtaccountname.Tag = ""
        txtD.Text = ""
    End Sub

    Private Sub btnDeposit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeposit.Click
        If active = True Then
            If GetAccounts = "Debit" Then
                AccountingEntryItem_Deposit(txtRefNo.Text, txtaccountname.Tag.ToString, 0, txtamount.Text, txtD.Text,
                                             " ", " ", GetjvKeyID)
            ElseIf GetAccounts = "Credit" Then
                AccountingEntryItem_Deposit(txtRefNo.Text, txtaccountname.Tag.ToString, txtamount.Text, 0, txtD.Text,
                                             " ", " ", GetjvKeyID)
            End If
        Else
            If GetAccounts = "Debit" Then
                AccountingEntryItem_Deposit(txtRefNo.Text, txtaccountname.Tag.ToString, 0, txtamount.Text, txtD.Text,
                                             " ", " ", GetjvKeyID)
            ElseIf GetAccounts = "Credit" Then
                AccountingEntryItem_Deposit(txtRefNo.Text, txtaccountname.Tag.ToString, txtamount.Text, 0, txtD.Text,
                                             " ", " ", GetjvKeyID)
            End If
        End If
    End Sub

    Private Sub AccountingEntryItem_Deposit(ByVal fiEntryNo As String,
                                     ByVal fxKeyAccount As String,
                                     ByVal fdDebit As Double,
                                     ByVal fdCredit As Double,
                                     ByVal fcMemo As String,
                                     ByVal fcNote As String,
                                     ByVal fxKeyNameID As String,
                                     ByVal fk_tJVEntry As String)
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "[usp_t_tJVEntry_details_save]", _
                New SqlParameter("@fiEntryNo", fiEntryNo), _
                New SqlParameter("@fxKeyAccount", fxKeyAccount), _
                New SqlParameter("@fdDebit", fdDebit), _
                New SqlParameter("@fdCredit", fdCredit), _
                New SqlParameter("@fcMemo", fcMemo), _
                New SqlParameter("@fcNote", fcNote), _
                New SqlParameter("@fk_JVHeader", jvKeyID), _
                New SqlParameter("@transDate", Date.Now))
            MessageBox.Show("Record Saved!", "Deposit", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnBrowseAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseAccount.Click
        frmMasterfile_Accounts.GetActive = 3
        frmMasterfile_Accounts.ShowDialog()
    End Sub

    Private Sub txtamount_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtamount.TextChanged

    End Sub
End Class