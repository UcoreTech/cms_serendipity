Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmCheckforClearing
    Private gcon As New Clsappconfiguration()
    Private Sub CheckedList()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "CIMS_SavingandTimeDeposit_CheckList_Load")
        Try
            grdcheck.Columns.Clear()
            With Me.grdcheck
                .DataSource = ds.Tables(0)
                .Columns("Clear").Width = 50
                .Columns("Clear").DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter
                .Columns("Bank").ReadOnly = True
                .Columns("Bank").Width = 120
                .Columns("Bank").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns("Check No.").ReadOnly = True
                .Columns("Check No.").Width = 120
                .Columns("Check No.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns("Description").Width = 170
                .Columns("Description").ReadOnly = True
                .Columns("Description").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                .Columns("Deposit Amount").Width = 100
                .Columns("Deposit Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns("Deposit Amount").ReadOnly = True
                .Columns(5).Visible = False
                .Columns(6).Visible = False
                .Columns(7).Visible = False
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub frmCheckforClearing_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CheckedList()
    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Close()
    End Sub
    Private Sub CheckUpdate(ByVal chckbox As Boolean, ByVal dtclr As Date, _
                            ByVal clrdby As String, ByVal pk As String)
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "[CIMS_SavingandTimeDeposit_CheckList_Update]", _
                                        New SqlParameter("@iscleared", chckbox), _
                                        New SqlParameter("@dtcleared", dtclr), _
                                        New SqlParameter("@clearedby", clrdby), _
                                        New SqlParameter("@pksd", pk))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            Throw ex
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Dim x As New DialogResult
        x = MessageBox.Show("Are you sure you want to Apply Changes?", "Confirm Update", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
        If x = Windows.Forms.DialogResult.OK Then
            CheckUpdate(grdcheck.CurrentRow.Cells(0).Value, _
                         grdcheck.CurrentRow.Cells(5).Value, _
                         grdcheck.CurrentRow.Cells(6).Value.ToString, _
                         grdcheck.CurrentRow.Cells(7).Value.ToString)
            CheckedList()
        End If
    End Sub
End Class