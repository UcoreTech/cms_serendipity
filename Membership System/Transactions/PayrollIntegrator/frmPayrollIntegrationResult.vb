Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Threading

Public Class frmPayrollIntegrationResult
#Region "Variables"
    Private gcon As New Clsappconfiguration
    Private payrollcode As String
    Private paycode As String
    Private paymentDate As Date

    Private dataset_Settle As DataSet
    Private dataset_Recon As DataSet
    Private dataset_3rdPartyPayroll As DataSet

    Private noOfWork As Int16

    Private username As String = frmLogin.Username.Text
    Private IsSuccessful As Boolean

#End Region
#Region "Property"
    Private Property getpayrolcode() As String
        Get
            Return payrollcode
        End Get
        Set(ByVal value As String)
            payrollcode = value
        End Set
    End Property

    Private Property GetPaymentDate() As Date
        Get
            Return paymentDate
        End Get
        Set(ByVal value As Date)
            paymentDate = value
        End Set
    End Property

    Private Property getPaycode() As String
        Get
            Return paycode
        End Get
        Set(ByVal value As String)
            paycode = value
        End Set
    End Property
#End Region
#Region "Payroll Code Upload tab"
    Private Sub PayrollUploadtab()
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_m_Member_PayrollCode_Select")
                While rd.Read
                    Me.cboPaycode.Items.Add(rd.Item("PRollDesc"))
                End While
            End Using
        Catch ex As Exception
            MessageBox.Show("PayrollUploadtab", "Payroll Upload", MessageBoxButtons.OK)
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub PayrollCodeUploadtab(ByVal payrolldesc As String)
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_m_Member_PayrollCode_Param", _
                                     New SqlParameter("PRollDesc", payrolldesc))
        Try
            While rd.Read
                getpayrolcode() = rd.Item("pk_PRollCode")
            End While
            rd.Close()

        Catch ex As Exception
            MessageBox.Show("PayrollCodeUploadtab", "Payroll Upload", MessageBoxButtons.OK)
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Execute Missed Payments"
    Private Sub DisplayPayrollForReconciliation(ByVal paycode As String, ByVal paydate As Date)
        Try
            dataset_Recon = ModifiedSqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Upload_FindPaymentsForReconciliation", _
                New SqlParameter("@payrollCode", paycode), _
                New SqlParameter("@paydate", paydate))

        Catch ex As Exception
            MessageBox.Show(ex.Message, "For Reconciliation", MessageBoxButtons.OK)
        End Try

    End Sub
#End Region
#Region "Execute Loan Settlement"
    Private Sub DisplayMatchPayroll(ByVal paycode As String, ByVal paydate As Date)

        Try
            dataset_Settle = ModifiedSqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Upload_FindMatchPayroll", _
                New SqlParameter("@payrollCode", paycode), _
                New SqlParameter("@paydate", paydate))
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Loan Settlement", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
#Region "Execute Payroll Settlement"
    Private Sub ExecuteMatchedPayrollInDatabase(ByVal paycode As String, ByVal paydate As String, ByVal fk_downloaded As Object, _
    ByVal fcEmployeeNo As String, ByVal fcMember As String, ByVal fcTranscode As String, ByVal fcReference As String, _
    ByVal fdTotal As Decimal, ByVal fcType As String, ByVal dtPaydate As Date)

        gcon.sqlconn.Open()

        Dim transaction As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            Dim user As String = username
            ModifiedSqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "CIMS_Upload_SettleAndPostPayroll_Matched", _
                                     New SqlParameter("@payrollCode", paycode), _
                                     New SqlParameter("@paydate", paydate), _
                                     New SqlParameter("@user", user), _
                                     New SqlParameter("@filename", filename), _
                                     New SqlParameter("@matched_fk_downloaded", fk_downloaded), _
                                     New SqlParameter("@matched_fcEmployeeNo", fcEmployeeNo), _
                                     New SqlParameter("@matched_fcMember", fcMember), _
                                     New SqlParameter("@matched_fcTranscode", fcTranscode), _
                                     New SqlParameter("@matched_fcReference", fcReference), _
                                     New SqlParameter("@matched_fdTotal", fdTotal), _
                                     New SqlParameter("@matched_fcType", fcType), _
                                     New SqlParameter("@matched_dtPaydate", dtPaydate))


            transaction.Commit()
            'MessageBox.Show("Payroll has been successfully settled and posted.", "Payroll Settlement and Posting", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
            'MessageBox.Show(ex.Message, "Payroll Settlement and Posting", MessageBoxButtons.OK)
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
    Private Sub ExecuteUnmatchedPayrollInDatabase(ByVal employeeNo As String, _
    ByVal returnCode As String, ByVal paymentDate As Date, ByVal paymentAmount As Decimal, ByVal payrollcode As String)

        gcon.sqlconn.Open()

        Dim transaction As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            Dim user As String = username
            ModifiedSqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "CIMS_Upload_SettleAndPostPayroll_UNMatched", _
                                     New SqlParameter("@user", user), _
                                     New SqlParameter("@filename", filename), _
                                     New SqlParameter("@Uploaded_EmployeeNo", employeeNo), _
                                     New SqlParameter("@Uploaded_ReturnCode", returnCode), _
                                     New SqlParameter("@Uploaded_fdPaymentDate", paymentDate), _
                                     New SqlParameter("@Uploaded_fdPaymentAmount", paymentAmount), _
                                     New SqlParameter("@Uploaded_fkPayrollCode", payrollcode))


            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw ex
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region


#Region "Bind to Datagridview"
    Private Sub BindDataToGrid_Settle()
        If dataset_Settle Is Nothing Then
            gridMatched.Columns.Add("no data.", "No Data")
        Else
            With gridMatched
                .DataSource = dataset_Settle.Tables(0)

                .Columns(0).Visible = False
                .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                '.Columns(5).DefaultCellStyle.Format = "N2"
                .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(8).Visible = False
            End With
        End If
    End Sub

    Private Sub BindDataToGrid_Recon()
        If dataset_Recon Is Nothing Then
            gridUnmatched.Columns.Add("no data.", "No Data")
        Else
            With gridUnmatched
                .DataSource = dataset_Recon.Tables(0)

                .Columns(0).Visible = False
                .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(5).DefaultCellStyle.Format = "N2"
                .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(6).Visible = False
            End With
        End If
    End Sub

#End Region
#Region "Background Routines"
    Private Sub BackgroundGridLoader_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundGridLoader_Settle.DoWork
        Call DisplayMatchPayroll(payrollcode, Me.Payrolldate.Text)
    End Sub

    Private Sub BackgroundGridLoader_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundGridLoader_Settle.RunWorkerCompleted
        BindDataToGrid_Settle()
        loadingPic1.Visible = False

        If IsPayrollProcessingFinish(1) Then
            btnSettlePost.Enabled = True
        End If

        'Call InititateProgress()
    End Sub

    Private Sub BackgroundGridLoader_Recon_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundGridLoader_Recon.DoWork
        Call DisplayPayrollForReconciliation(payrollcode, Me.Payrolldate.Text)
    End Sub

    Private Sub BackgroundGridLoader_Recon_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundGridLoader_Recon.RunWorkerCompleted
        BindDataToGrid_Recon()
        loadingPic2.Visible = False

        If IsPayrollProcessingFinish(1) Then
            btnSettlePost.Enabled = True
        End If
    End Sub

    Private Sub BackgroundSettlePayments_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundSettlePayments.DoWork
        Try
            '* For Accounting Integration (Preparation)
            Call RecordGeneralJournalHeader(Me.Payrolldate.Text, username, filename)
            '****

            Call SettleMatchedPayroll()

            '* This tags Posted Upload Historical Data as Settled
            Call SettleHistoricalUploadData()

            ' 
            Call Get3rdPartyPayroll(payrollcode, Me.Payrolldate.Text)

            '
            Call SettleUnMatchedPayroll()

            '* For Accounting Integration (Finalization)
            Call FinalizeGeneralJournalSummary(payrollcode)
            '****

            MessageBox.Show("Payroll has been successfully settled and posted.", "Payroll Settlement and Posting", MessageBoxButtons.OK, MessageBoxIcon.Information)
            IsSuccessful = True
            'Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            IsSuccessful = False
        End Try

    End Sub

    Private Sub BackgroundSettlePayments_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundSettlePayments.RunWorkerCompleted
        If IsSuccessful Then
            Me.Close()
            Call LoadPayrollReport()
        End If
    End Sub

#End Region

    Private Function IsPayrollProcessingFinish(ByVal noOfWorkCompleted As Int16) As Boolean
        Const totalNoOfWork As Int16 = 2

        noOfWork = noOfWork + noOfWorkCompleted
        If noOfWork = totalNoOfWork Then
            Return True
            noOfWork = 0
        Else
            Return False
        End If
    End Function
    Private Sub ActivateBackgroundTask()
        BackgroundGridLoader_Settle.RunWorkerAsync()
        BackgroundGridLoader_Recon.RunWorkerAsync()
    End Sub
    Private Sub ClearAllDataGridView()
        gridMatched.Columns.Clear()
        gridUnmatched.Columns.Clear()
    End Sub
    Private Sub InitializeForm()
        loadingPic1.Visible = True
        loadingPic2.Visible = True

        Me.cboPaycode.Text = frmDownloadUploadExcel.cbopaycodeupload.Text
        Me.Payrolldate.Text = frmDownloadUploadExcel.txtpaydate.Text
        getpayrolcode() = frmDownloadUploadExcel.payrollcode
        getPaycode() = Me.cboPaycode.Text
        GetPaymentDate() = Payrolldate.Value.Date
    End Sub
    Private Sub LoadLoadingScreen()
        loadingPic1.Visible = True
        loadingPic2.Visible = True
    End Sub
    Private Sub LoadPayrollReport()
        frmRpt_PayrollSettlement.GetMode() = "Module"
        frmRpt_PayrollSettlement.GetPaymentDate() = Payrolldate.Value.Date
        frmRpt_PayrollSettlement.GetPayrollCode() = getpayrolcode()
        frmRpt_PayrollSettlement.MdiParent = frmMain
        frmRpt_PayrollSettlement.Show()
    End Sub

    Private Sub RecordGeneralJournalHeader(ByVal paydate As String, ByVal user As String, ByVal filename As String)

        Dim storedProc As String = "CIMS_Upload_Accounting_PrepareJournalEntrySummary"

        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, storedProc, _
                New SqlParameter("@paydate", paydate), _
                New SqlParameter("@user", user), _
                New SqlParameter("@filename", filename))

        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    Private Sub FinalizeGeneralJournalSummary(ByVal payrollCode As String)
        Try
            Dim storedProc As String = "CIMS_Upload_FinalizeJournalEntrySummary"
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, storedProc, _
                        New SqlParameter("@payrollCode", payrollCode))
        Catch ex As Exception

            Throw ex
        End Try
    End Sub
    Private Sub SettleMatchedPayroll()
        Dim progressForm As New frmStartup()
        Dim fk_downloaded As System.Guid
        Dim fcEmployeeNo As String = ""
        Dim fcMember As String = ""
        Dim fcTranscode As String = ""
        Dim fcReference As String = ""
        Dim fcType As String = ""
        Dim dtPaydate As Date
        Dim fdTotal As Decimal = 0

        Try
            Dim count As Int16
            count = 1

            With progressForm
                .Show()
                
                .lblTitle.Text = "Matched Payroll Processing"

                Dim Minimum As Integer = 0
                .processWait.Minimum = Minimum

                Dim totalRows As Integer = dataset_Settle.Tables(0).Rows.Count
                .processWait.Maximum = totalRows

                For Each datarow As DataRow In dataset_Settle.Tables(0).Rows
                    count = count + 1
                    .processWait.Increment(1)

                    fk_downloaded = datarow("fk_downloaded")
                    fcEmployeeNo = datarow("Employee No.").ToString
                    fcMember = datarow("Member").ToString
                    fcTranscode = datarow("Transcode").ToString
                    fcReference = datarow("Reference No.").ToString
                    fdTotal = Decimal.Parse(datarow("Total").ToString)
                    fcType = datarow("Type").ToString
                    dtPaydate = Date.Parse(datarow("PayDate").ToString)

                    'Record Payroll to Database
                    Call ExecuteMatchedPayrollInDatabase(payrollcode, Me.Payrolldate.Text, fk_downloaded, fcEmployeeNo, _
                                fcMember, fcTranscode, fcReference, fdTotal, fcType, dtPaydate)

                    .Refresh()

                    'Update Status
                    .lblProcessStatus.Text = fcMember

                    Dim ProgressStatus As String = count.ToString + " of " + .processWait.Maximum.ToString + " data processed..."
                    .lblProgress.Text = ProgressStatus
                Next
            End With

        Catch ex As Exception
            MessageBox.Show(fcEmployeeNo + " " + fcMember + " " + fcTranscode + " " + fcReference + " " + fdTotal)
            Throw ex
        Finally
            progressForm.Close()
        End Try

    End Sub
    Private Sub SettleUnMatchedPayroll()
        Dim progressForm As New frmStartup()
        Dim employeeNo As String = ""
        Dim fcMember As String = ""
        Dim returnCode As String = ""
        Dim payrollCode As String = ""
        Dim paydate As Date
        Dim paymentAmount As Decimal = 0

        Try
            Dim count As Int16
            count = 1

            With progressForm
                .Show()

                .lblTitle.Text = "Un-Matched Payroll Processing"

                Dim Minimum As Integer = 0
                .processWait.Minimum = Minimum

                Dim totalRows As Integer = dataset_3rdPartyPayroll.Tables(0).Rows.Count
                .processWait.Maximum = totalRows


                Dim data3rdPartyPayroll As DataRowCollection = dataset_3rdPartyPayroll.Tables(0).Rows
                For Each datarow As DataRow In data3rdPartyPayroll
                    count = count + 1
                    .processWait.Increment(1)

                    employeeNo = datarow("employeeNo").ToString()
                    fcMember = datarow("Member").ToString()
                    returnCode = datarow("transcode").ToString()
                    paydate = Date.Parse(datarow("fdPaymentDate").ToString())
                    paymentAmount = Decimal.Parse(datarow("fdAmount").ToString())
                    payrollCode = datarow("fk_ProllCode").ToString()

                    'Record Payroll to Database
                    Call ExecuteUnmatchedPayrollInDatabase(employeeNo, returnCode, paydate, paymentAmount, payrollCode)

                    .Refresh()

                    'Update Status
                    .lblProcessStatus.Text = fcMember

                    Dim ProgressStatus As String = count.ToString + " of " + .processWait.Maximum.ToString + " data processed..."
                    .lblProgress.Text = ProgressStatus
                Next
            End With

        Catch ex As Exception
            MessageBox.Show(employeeNo + " " + fcMember + " " + returnCode + " " + paydate + " " + paymentAmount)
            Throw ex
        Finally
            progressForm.Close()
        End Try
    End Sub
    Private Sub SettleHistoricalUploadData()
        Try
            Dim StoredProc As String = "CIMS_Upload_HistoricalUploadData_Settlement"
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, StoredProc)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub Get3rdPartyPayroll(ByVal payrollCode As String, ByVal paydate As String)

        Try
            Dim storedProc As String = "CIMS_Upload_Find3rdPartyPayrollData"
            dataset_3rdPartyPayroll = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, storedProc, _
                    New SqlParameter("@payrollCode", payrollCode), _
                    New SqlParameter("@paymentdate", paydate))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


#Region "Form Events"
    Private Sub frmPayrollIntegrationResult_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call InitializeForm()
        Call PayrollUploadtab()
        Call ActivateBackgroundTask()
    End Sub
    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Call DisplayMatchPayroll(payrollcode, Me.Payrolldate.Text)
        Call DisplayPayrollForReconciliation(payrollcode, Me.Payrolldate.Text)
    End Sub
    Private Sub cbopaycodeupload_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPaycode.TextChanged
        Call PayrollCodeUploadtab(Me.cboPaycode.Text.Trim)
    End Sub

    Private Sub btnSettle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSettlePost.Click
        If gridMatched.RowCount = 0 And gridUnmatched.RowCount = 0 Then
            MessageBox.Show("There are no transactions to settle.", "Payroll Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            If payrollcode <> "" And Me.Payrolldate.Text <> "" Then
                Dim x As DialogResult
                x = MessageBox.Show("This will settle and post these transactions." _
                    + vbNewLine + "Do you want to proceed?" _
                    + vbNewLine + "This is irreversible.", "Settlement", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                If x = Windows.Forms.DialogResult.Yes Then
                    If BackgroundSettlePayments.IsBusy = False Then
                        Call BackgroundSettlePayments.RunWorkerAsync()
                    Else
                        MessageBox.Show("User Error! The system is currently posting your transactions." & _
                                vbNewLine & "Please do not execute any commands to avoid errors in the result.", "User Error!", MessageBoxButtons.OK, _
                                MessageBoxIcon.Error)
                    End If
                End If
            Else
                MessageBox.Show("Select payroll code first and upload your payroll file.", "Payroll Transaction", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If BackgroundGridLoader_Settle.IsBusy Then
            BackgroundGridLoader_Settle.CancelAsync()
        End If

        If BackgroundGridLoader_Recon.IsBusy Then
            BackgroundGridLoader_Recon.CancelAsync()
        End If

        Me.Close()
    End Sub

#End Region

End Class