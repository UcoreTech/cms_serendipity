<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayrollIntegrationResult
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPayrollIntegrationResult))
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cboPaycode = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Payrolldate = New System.Windows.Forms.DateTimePicker()
        Me.gridMatched = New System.Windows.Forms.DataGridView()
        Me.gridUnmatched = New System.Windows.Forms.DataGridView()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnSettlePost = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.BackgroundGridLoader_Settle = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundGridLoader_Recon = New System.ComponentModel.BackgroundWorker()
        Me.loadingPic1 = New System.Windows.Forms.PictureBox()
        Me.loadingPic2 = New System.Windows.Forms.PictureBox()
        Me.BackgroundSettlePayments = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundProgressBar = New System.ComponentModel.BackgroundWorker()
        CType(Me.gridMatched, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridUnmatched, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.loadingPic1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.loadingPic2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(321, 23)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(55, 15)
        Me.Label5.TabIndex = 57
        Me.Label5.Text = "Pay Date"
        '
        'cboPaycode
        '
        Me.cboPaycode.Enabled = False
        Me.cboPaycode.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPaycode.FormattingEnabled = True
        Me.cboPaycode.Location = New System.Drawing.Point(95, 20)
        Me.cboPaycode.Name = "cboPaycode"
        Me.cboPaycode.Size = New System.Drawing.Size(205, 23)
        Me.cboPaycode.TabIndex = 55
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(12, 23)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(77, 15)
        Me.Label6.TabIndex = 56
        Me.Label6.Text = "Payroll Code"
        '
        'Payrolldate
        '
        Me.Payrolldate.Enabled = False
        Me.Payrolldate.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Payrolldate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Payrolldate.Location = New System.Drawing.Point(382, 20)
        Me.Payrolldate.Name = "Payrolldate"
        Me.Payrolldate.Size = New System.Drawing.Size(102, 23)
        Me.Payrolldate.TabIndex = 58
        '
        'gridMatched
        '
        Me.gridMatched.AllowUserToAddRows = False
        Me.gridMatched.AllowUserToDeleteRows = False
        Me.gridMatched.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridMatched.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.gridMatched.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gridMatched.DefaultCellStyle = DataGridViewCellStyle2
        Me.gridMatched.GridColor = System.Drawing.Color.Gray
        Me.gridMatched.Location = New System.Drawing.Point(12, 94)
        Me.gridMatched.Name = "gridMatched"
        Me.gridMatched.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridMatched.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.gridMatched.Size = New System.Drawing.Size(685, 197)
        Me.gridMatched.TabIndex = 60
        '
        'gridUnmatched
        '
        Me.gridUnmatched.AllowUserToAddRows = False
        Me.gridUnmatched.AllowUserToDeleteRows = False
        Me.gridUnmatched.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridUnmatched.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.gridUnmatched.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.LightPink
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gridUnmatched.DefaultCellStyle = DataGridViewCellStyle5
        Me.gridUnmatched.Location = New System.Drawing.Point(12, 317)
        Me.gridUnmatched.Name = "gridUnmatched"
        Me.gridUnmatched.ReadOnly = True
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridUnmatched.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.gridUnmatched.Size = New System.Drawing.Size(685, 166)
        Me.gridUnmatched.TabIndex = 61
        '
        'btnRefresh
        '
        Me.btnRefresh.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefresh.Location = New System.Drawing.Point(490, 20)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(105, 25)
        Me.btnRefresh.TabIndex = 65
        Me.btnRefresh.Text = "Refresh"
        Me.btnRefresh.UseVisualStyleBackColor = True
        Me.btnRefresh.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(12, 294)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(156, 23)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Unmatched Payroll"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnCancel)
        Me.Panel1.Controls.Add(Me.btnSettlePost)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 500)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(703, 46)
        Me.Panel1.TabIndex = 66
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = Global.WindowsApplication2.My.Resources.Resources._exit
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(569, 8)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(128, 30)
        Me.btnCancel.TabIndex = 65
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSettlePost
        '
        Me.btnSettlePost.Enabled = False
        Me.btnSettlePost.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSettlePost.Image = Global.WindowsApplication2.My.Resources.Resources.OK
        Me.btnSettlePost.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSettlePost.Location = New System.Drawing.Point(359, 8)
        Me.btnSettlePost.Name = "btnSettlePost"
        Me.btnSettlePost.Size = New System.Drawing.Size(204, 30)
        Me.btnSettlePost.TabIndex = 63
        Me.btnSettlePost.Text = "Settle && Post Payroll"
        Me.btnSettlePost.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(11, 68)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(135, 23)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Matched Payroll"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.LightGray
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.cboPaycode)
        Me.Panel2.Controls.Add(Me.btnRefresh)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Payrolldate)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(703, 54)
        Me.Panel2.TabIndex = 68
        '
        'BackgroundGridLoader_Settle
        '
        Me.BackgroundGridLoader_Settle.WorkerSupportsCancellation = True
        '
        'BackgroundGridLoader_Recon
        '
        Me.BackgroundGridLoader_Recon.WorkerSupportsCancellation = True
        '
        'loadingPic1
        '
        Me.loadingPic1.AccessibleRole = System.Windows.Forms.AccessibleRole.Animation
        Me.loadingPic1.BackColor = System.Drawing.Color.Transparent
        Me.loadingPic1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.loadingPic1.Image = Global.WindowsApplication2.My.Resources.Resources.LoadingData
        Me.loadingPic1.InitialImage = Global.WindowsApplication2.My.Resources.Resources.LoadingData
        Me.loadingPic1.Location = New System.Drawing.Point(324, 160)
        Me.loadingPic1.Name = "loadingPic1"
        Me.loadingPic1.Size = New System.Drawing.Size(63, 60)
        Me.loadingPic1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.loadingPic1.TabIndex = 69
        Me.loadingPic1.TabStop = False
        Me.loadingPic1.Visible = False
        '
        'loadingPic2
        '
        Me.loadingPic2.AccessibleRole = System.Windows.Forms.AccessibleRole.Animation
        Me.loadingPic2.BackColor = System.Drawing.Color.Transparent
        Me.loadingPic2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.loadingPic2.Image = Global.WindowsApplication2.My.Resources.Resources.LoadingData
        Me.loadingPic2.InitialImage = Global.WindowsApplication2.My.Resources.Resources.LoadingData
        Me.loadingPic2.Location = New System.Drawing.Point(324, 382)
        Me.loadingPic2.Name = "loadingPic2"
        Me.loadingPic2.Size = New System.Drawing.Size(63, 60)
        Me.loadingPic2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.loadingPic2.TabIndex = 70
        Me.loadingPic2.TabStop = False
        Me.loadingPic2.UseWaitCursor = True
        '
        'BackgroundSettlePayments
        '
        Me.BackgroundSettlePayments.WorkerSupportsCancellation = True
        '
        'frmPayrollIntegrationResult
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(703, 546)
        Me.Controls.Add(Me.loadingPic2)
        Me.Controls.Add(Me.loadingPic1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.gridUnmatched)
        Me.Controls.Add(Me.gridMatched)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPayrollIntegrationResult"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Payroll Integration Result"
        Me.TopMost = True
        CType(Me.gridMatched, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridUnmatched, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.loadingPic1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.loadingPic2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cboPaycode As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Payrolldate As System.Windows.Forms.DateTimePicker
    Friend WithEvents gridMatched As System.Windows.Forms.DataGridView
    Friend WithEvents gridUnmatched As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnSettlePost As System.Windows.Forms.Button
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents BackgroundGridLoader_Settle As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundGridLoader_Recon As System.ComponentModel.BackgroundWorker
    Friend WithEvents loadingPic1 As System.Windows.Forms.PictureBox
    Friend WithEvents loadingPic2 As System.Windows.Forms.PictureBox
    Friend WithEvents BackgroundSettlePayments As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundProgressBar As System.ComponentModel.BackgroundWorker
End Class
