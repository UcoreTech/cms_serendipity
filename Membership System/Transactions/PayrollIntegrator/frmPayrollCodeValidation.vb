Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
'Imports Excel = Microsoft.Office.Interop.Excel

Public Class frmPayrollCodeValidation
    Public payrollcode As String

    Public Property getpayrollcode() As String
        Get
            Return payrollcode
        End Get
        Set(ByVal value As String)
            payrollcode = value
        End Set
    End Property

    Private Sub PayrollCodeUploadtab(ByVal payrolldesc As String)
        Dim mycon As New Clsappconfiguration

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_m_Member_PayrollCode_Param", _
                                   New SqlParameter("PRollDesc", payrolldesc))
                While rd.Read
                    getpayrollcode() = rd.Item("pk_PRollCode")
                End While
            End Using

        Catch ex As Exception
            MessageBox.Show("PayrollCodeUploadtab", "Payroll Upload", MessageBoxButtons.OK)
        End Try
    End Sub


#Region "View list of Personnel who does not belong to payroll code"
    Private Sub viewlistpersonnel(ByVal payrollDesc As String, ByVal paydate As Date)
        Dim mycon As New Clsappconfiguration

        PayrollCodeUploadtab(payrollDesc)

        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Upload_PayrollCode_Validate", _
                New SqlParameter("@payrollCode", payrollcode), _
                New SqlParameter("@payDate", paydate))
            With gridPersonnellist
                .DataSource = ds.Tables(0)
                .Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                .Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            End With

        Catch ex As Exception

        End Try

    End Sub
#End Region
#Region "Delete Temp Table"
    Private Sub DeleteTempTable()
        Dim mycon As New Clsappconfiguration

        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Upload_TemporaryTable_Delete")
        Catch ex As Exception
            MessageBox.Show("DeleteTempTable", "frmPayrollCodeValidation", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
    Private Sub frmPayrollCodeValidation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.lblPayrollcode.Text = "Payroll Code:  " + frmDownloadUploadExcel.cbopaycodeupload.Text
        If frmDownloadUploadExcel.cbopaycodeupload.Text <> "" And frmDownloadUploadExcel.txtpaydate.Text <> "" Then
            Call viewlistpersonnel(frmDownloadUploadExcel.cbopaycodeupload.Text, frmDownloadUploadExcel.txtpaydate.Text)
        Else
            MessageBox.Show("Payroll Code or Pay Date is Empty.", "Empty Parameter found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

    End Sub

    Private Sub btnMembermaster_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMembermaster.Click
        frmMember_Master.Show()
        frmDownloadUploadExcel.Close()
        Call createExcelfile()
        Me.Close()
    End Sub
#Region "Create Excel file for the list of Personnel"
    Private Sub createExcelfile()
        Try
            'Dim rowcount As Int32 = Me.gridDownload.SelectedRows.Count - 1
            Dim rowcount As Int32 = Me.gridPersonnellist.Rows.Count - 1
            If rowcount > -1 Then
                Dim fieldname() As String = {"EmployeeNo", "FullName"}
                Dim dataarray(rowcount, 2) As Object
                Dim colcount As Int32 = 0

                For rowcounter As Int32 = 0 To rowcount
                    For Each cell As DataGridViewCell In Me.gridPersonnellist.Rows(rowcount - rowcounter).Cells
                        dataarray(rowcounter, colcount) = cell.Value
                        colcount = colcount + 1
                    Next
                    colcount = 0
                Next

                'var for excel
                Dim xlapp As New Excel.Application
                Dim workbook As Excel.Workbook = xlapp.Workbooks.Add(Excel.XlWBATemplate.xlWBATWorksheet)
                Dim worksheet As Excel.Worksheet = CType(workbook.Worksheets(1), Excel.Worksheet)
                Dim xlcalc As Excel.XlCalculation
                'save setting
                With xlapp
                    xlcalc = .Calculation
                    .Calculation = Excel.XlCalculation.xlCalculationManual
                End With
                'write the field names and data to the targeting worksheet
                With worksheet
                    .Range(.Cells(1, 1), .Cells(1, 2)).Value = fieldname
                    .Range(.Cells(2, 1), .Cells(rowcount + 2, 2)).Value = dataarray

                End With

                With xlapp
                    .Visible = True
                    .UserControl = True
                    .Calculation = xlcalc
                End With
                'tanggal to memory
                worksheet = Nothing
                workbook = Nothing
                xlapp = Nothing
                GC.Collect()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
#End Region
#Region "Clear gridupload"
    Private Sub Cleargridupload()
        Dim rows As Integer = frmDownloadUploadExcel.gridupload.Rows.Count
        Dim i As Integer
        For i = 0 To frmDownloadUploadExcel.gridupload.Rows.Count - 1
            frmDownloadUploadExcel.gridupload.Rows.Remove(frmDownloadUploadExcel.gridupload.CurrentRow)
        Next
    End Sub
#End Region
    Private Sub btnChangePayrollcode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChangePayrollcode.Click
        Dim x As DialogResult
        x = MessageBox.Show("If you want to change payroll code re-upload your excel file", "Want to change Payroll code?", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        If x = Windows.Forms.DialogResult.Yes Then
            Call DeleteTempTable()
            Call Cleargridupload()
            Me.Close()
        ElseIf x = Windows.Forms.DialogResult.No Then
            Me.Close()
        End If
    End Sub
End Class