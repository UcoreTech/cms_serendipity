Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
'Imports Excel = Microsoft.Office.Interop.Excel
Imports System.Data.OleDb, System.IO
Imports System.Threading
Imports System.Security.Principal.WindowsIdentity

Public Class frmUnsettledLoanPayments

    Dim mycon As New Clsappconfiguration
#Region "Property"
    Private user As String
    Public Property GetSetUser() As String
        Get
            Return user
        End Get
        Set(ByVal value As String)
            user = value
        End Set
    End Property
    Private fkPaycode As String
    Public Property GetSetfkPaycode() As String
        Get
            Return fkPaycode
        End Get
        Set(ByVal value As String)
            fkPaycode = value
        End Set
    End Property
    Private isLoan As Boolean
    Public Property GetSetIsLoan() As Boolean
        Get
            Return isLoan
        End Get
        Set(ByVal value As Boolean)
            isLoan = value
        End Set
    End Property
    Private isContribution As Boolean
    Public Property GetSetIsContribution() As Boolean
        Get
            Return isContribution
        End Get
        Set(ByVal value As Boolean)
            isContribution = value
        End Set
    End Property
    Private isBereavement As Boolean
    Public Property GetSetIsBereavement() As Boolean
        Get
            Return isBereavement
        End Get
        Set(ByVal value As Boolean)
            isBereavement = value
        End Set
    End Property
    Private bereavementAmount As Decimal
    Public Property GetSetBereavementAmount() As Decimal
        Get
            Return bereavementAmount
        End Get
        Set(ByVal value As Decimal)
            bereavementAmount = value
        End Set
    End Property
    Private paydate As Date
    Public Property GetSetPaydate() As Date
        Get
            Return paydate
        End Get
        Set(ByVal value As Date)
            paydate = value
        End Set
    End Property
#End Region

    Private unsettledPaymentsDS As New DataSet
    Private xSQLHelper As New NewSQLHelper

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Call PayrollConfirm()
    End Sub

#Region "SEND TO EXCEl and LAUNCH"
    Private Sub CreateExcelOutputFile()
        Try
            Dim rowcount As Int32 = frmDownloadUploadExcel.gridDownload.Rows.Count - 1
            If rowcount > -1 Then
                Dim fieldname() As String = {"Employee ID", "Members", "TranCode", "Total"}
                Dim dataarray(rowcount, 4) As Object
                Dim colcount As Int32 = 0

                For rowcounter As Int32 = 0 To rowcount
                    For Each cell As DataGridViewCell In frmDownloadUploadExcel.gridDownload.Rows(rowcount - rowcounter).Cells
                        dataarray(rowcounter, colcount) = cell.Value
                        colcount = colcount + 1
                    Next
                    colcount = 0
                Next

                'var for excel
                Dim xlapp As New Excel.Application
                Dim workbook As Excel.Workbook = xlapp.Workbooks.Add(Excel.XlWBATemplate.xlWBATWorksheet)
                Dim worksheet As Excel.Worksheet = CType(workbook.Worksheets(1), Excel.Worksheet)
                Dim xlcalc As Excel.XlCalculation
                'save setting
                With xlapp
                    xlcalc = .Calculation
                    .Calculation = Excel.XlCalculation.xlCalculationManual


                End With
                'write the field names and data to the targeting worksheet
                With worksheet
                    .Range(.Cells(1, 1), .Cells(1, 4)).Value = fieldname
                    .Range(.Cells(2, 1), .Cells(rowcount + 2, 4)).Value = dataarray

                End With

                With xlapp
                    .Visible = True
                    .UserControl = True
                    .Calculation = xlcalc
                End With
                'tanggal to memory
                worksheet = Nothing
                workbook = Nothing
                xlapp = Nothing
                GC.Collect()
            End If
        Catch ex As Exception
            Throw ex
            'MessageBox.Show(ex.ToString)
        End Try
    End Sub
#End Region
#Region "Payroll Confirmation"
    'confirm after closing the form for Payroll Include/Exclude
    Private Sub PayrollConfirm()
        Try
            If frmDownloadUploadExcel.cboPaycode.Text <> "" And frmDownloadUploadExcel.cboPaydate.Text <> "" Then
                Dim x As New DialogResult
                x = MessageBox.Show("Are you sure you want to create payroll file?", "Payroll Integrator", MessageBoxButtons.YesNo)
                If x = Windows.Forms.DialogResult.Yes Then
                    Dim rows As Integer = Me.gridloanpayment.Rows.Count
                    Call PostToHistory(frmDownloadUploadExcel.fkpaycode, frmDownloadUploadExcel.isloan, frmDownloadUploadExcel.iscontribution, _
                                       frmDownloadUploadExcel.isbereavement, frmDownloadUploadExcel.isdecimal, frmDownloadUploadExcel.ispaydate)
                    If rows > 0 Then
                        Call CreateExcelOutputFile()
                    End If
                Else
                    Me.Close()
                End If
            Else
                MessageBox.Show("Select Pay Code and Pay Date", "Select", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try


    End Sub
#End Region
#Region "Include/Exclude Loan Payment"
    Private Shared Sub ExcludeLoanPayment(ByVal pk_DownloadHistory As String)
        Dim mycon As New Clsappconfiguration
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Download_ExcludeInPayroll", _
                                      New SqlParameter("@pk_DownloadHistory", pk_DownloadHistory))
        Catch ex As Exception
            MessageBox.Show("CIMS_Download_ExcludeInPayroll", "ExcludeLoanPayment", MessageBoxButtons.OK)
        End Try
    End Sub
    Private Shared Sub IncludeLoanPayment(ByVal pk_DownloadHistory As String)
        Dim mycon As New Clsappconfiguration
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Download_IncludeInPayroll", _
                                      New SqlParameter("@pk_DownloadHistory", pk_DownloadHistory))
        Catch ex As Exception
            MessageBox.Show("CIMS_Download_IncludeInPayroll", "IncludeLoanPayment", MessageBoxButtons.OK)
        End Try
    End Sub
    Private Sub LoadPaymentsIncludeExclude(ByVal paycode As String, ByVal isloadinclude As Boolean, ByVal iscontrinclude As Boolean, _
                                           ByVal isbereavinclude As Boolean, ByVal bereavement As Decimal, ByVal paydate As Date)
        'This will nullify the parameter for the script 'CIMS_Download_UnsettledLoanPayments_Load'
        'Needed for the script
        Dim showExclusion As String
        If chkDisplayExcluded.Checked = False Then
            showExclusion = "1"
        Else
            showExclusion = "0"
        End If

        Try
            Dim ds As DataSet = xSQLHelper.ExecuteDataset("CIMS_Download_UnsettledLoanPayments_Load", _
                New SqlParameter("@paycode", paycode), _
                New SqlParameter("@isLoanIncluded", isloadinclude), _
                New SqlParameter("@isContributionIncluded", iscontrinclude), _
                New SqlParameter("@isBereavementIncluded", isbereavinclude), _
                New SqlParameter("@bereavementAmount", bereavement), _
                New SqlParameter("@payDate", paydate), _
                New SqlParameter("@isDisplayExcluded", showExclusion))

            unsettledPaymentsDS = ds
            If unsettledPaymentsDS.Tables(0).Rows.Count = 0 Then
                frmDownloadUploadExcel.HasUnsettledPayments = False
            Else
                frmDownloadUploadExcel.HasUnsettledPayments = True
            End If

            With gridloanpayment
                .DataSource = ds.Tables(0)
                .Columns("Select").Width = 60
                .Columns("Reference No").Width = 60
                .Columns("Member").Width = 200
                .Columns("Amount").Width = 80
                .Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns("Remarks").Width = 60
                .Columns("Employee No.").Visible = False
                .Columns("pk_DownloadHistory").Visible = False
            End With

            Call ColorGrid()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "LoadPaymentsIncludeExclude", MessageBoxButtons.OK)
        End Try
    End Sub


#End Region
#Region "CIMS_Download_Details_PostToHistory_Analog"
    Private Sub PostToHistory(ByVal paycode As String, ByVal isloanincluded As Boolean, ByVal isContributionIncluded As Boolean, _
                                  ByVal isBereavementIncluded As Boolean, ByVal bereavementAmount As Decimal, ByVal payDate As Date)
        Dim mycon As New Clsappconfiguration

        Try
            ModifiedSqlHelper.ExecuteNonQuery(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Download_Details_PostToHistory_Analog", _
                                      New SqlParameter("@paycode", paycode), _
                                      New SqlParameter("@isLoanIncluded", isloanincluded), _
                                      New SqlParameter("@isContributionIncluded", isContributionIncluded), _
                                      New SqlParameter("@isBereavementIncluded", isBereavementIncluded), _
                                      New SqlParameter("@bereavementAmount", bereavementAmount), _
                                      New SqlParameter("@payDate", payDate), _
                                      New SqlParameter("@user", GetSetUser()))
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Insert failed", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub
#End Region



    Private Sub frmUnsettledLoanPayments_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call InitialializeParameters()

        Call LoadPaymentsIncludeExclude(frmDownloadUploadExcel.fkpaycode, _
                                        frmDownloadUploadExcel.isloan, _
                                        frmDownloadUploadExcel.iscontribution, _
                                        frmDownloadUploadExcel.isbereavement, _
                                        frmDownloadUploadExcel.isdecimal, _
                                        frmDownloadUploadExcel.ispaydate)

        Call LoadGroup()
        Call LoadCategory()
        Call LoadType()
    End Sub

    'load group ,category, type
    Private Sub LoadGroup()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.cnstring, "CIMS_Masterfile_SelectAll")
        While rd.Read
            cbofgroup.Items.Add(rd("Fc_GroupDesc"))
        End While
    End Sub

    Private Sub LoadCategory()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.cnstring, "CIMS_Masterfile_Category_Select")
        While rd.Read
            cbofcategory.Items.Add(rd("Fc_Category"))
        End While
    End Sub

    Private Sub LoadType()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.cnstring, "CIMS_Masterfile_Type_Select")
        While rd.Read
            cboftype.Items.Add(rd("Fc_Type"))
        End While
    End Sub

    Private Sub ColorGrid()
        Dim index As Int16
        For index = 0 To gridloanpayment.Rows.Count - 1
            If gridloanpayment.Item(4, index).Value = "Included" Then
                gridloanpayment.Rows(index).DefaultCellStyle.BackColor = Color.LightBlue
            Else
                gridloanpayment.Rows(index).DefaultCellStyle.BackColor = Color.LightPink
            End If
        Next
    End Sub

    Private Sub InitialializeParameters()
        GetSetUser() = frmLogin.Username.ToString()
        GetSetfkPaycode() = frmDownloadUploadExcel.fkpaycode
        GetSetIsLoan() = frmDownloadUploadExcel.isloan
        GetSetIsContribution() = frmDownloadUploadExcel.iscontribution
        GetSetIsBereavement() = frmDownloadUploadExcel.isbereavement
        GetSetBereavementAmount() = frmDownloadUploadExcel.isdecimal
        GetSetPaydate() = frmDownloadUploadExcel.ispaydate
    End Sub
    Private Shared Sub DisplayMarqueeProgressBar()
        With frmStartup
            .MdiParent = frmMain
            .Show()
            .processWait.Style = ProgressBarStyle.Marquee
            .processWait.MarqueeAnimationSpeed = 5
            .lblTitle.Text = "Posting to Download Details..."
            .lblProcessStatus.Text = ""
            .lblProgress.Text = ""
        End With
    End Sub

    Private Sub chkExcludeAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkExcludeAll.CheckedChanged
        Dim i As Integer
        If chkExcludeAll.Checked = True Then
            For i = 0 To gridloanpayment.Rows.Count - 1
                gridloanpayment.Item(0, i).Value = True
            Next
        Else
            For i = 0 To gridloanpayment.Rows.Count - 1
                gridloanpayment.Item(0, i).Value = False
            Next
        End If
    End Sub
    Private Sub btnInclude_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInclude.Click
        Dim incld As Integer

        For incld = 0 To gridloanpayment.Rows.Count - 1
            If gridloanpayment.Item(0, incld).Value = True Then
                Call IncludeLoanPayment(gridloanpayment.Item(6, incld).Value.ToString)
            End If
        Next

        'Display Data
        Call LoadPaymentsIncludeExclude(frmDownloadUploadExcel.fkpaycode, frmDownloadUploadExcel.isloan, frmDownloadUploadExcel.iscontribution, _
                                           frmDownloadUploadExcel.isbereavement, frmDownloadUploadExcel.isdecimal, frmDownloadUploadExcel.ispaydate)
        frmDownloadUploadExcel.ProcessDownload(frmDownloadUploadExcel.fkpaycode, frmDownloadUploadExcel.isloan, frmDownloadUploadExcel.iscontribution, _
                                               frmDownloadUploadExcel.isbereavement, frmDownloadUploadExcel.isdecimal, frmDownloadUploadExcel.ispaydate)
        frmDownloadUploadExcel.BindProcessedPayrollToGrid()
    End Sub
    Private Sub btnExclude_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExclude.Click
        Dim excld As Integer

        For excld = 0 To gridloanpayment.Rows.Count - 1
            If gridloanpayment.Item(0, excld).Value = True Then
                Call ExcludeLoanPayment(gridloanpayment.Item(6, excld).Value.ToString)
            End If
        Next

        'Display Data
        Call LoadPaymentsIncludeExclude(frmDownloadUploadExcel.fkpaycode, frmDownloadUploadExcel.isloan, frmDownloadUploadExcel.iscontribution, _
                                              frmDownloadUploadExcel.isbereavement, frmDownloadUploadExcel.isdecimal, frmDownloadUploadExcel.ispaydate)
        frmDownloadUploadExcel.ProcessDownload(frmDownloadUploadExcel.fkpaycode, frmDownloadUploadExcel.isloan, frmDownloadUploadExcel.iscontribution, _
                                              frmDownloadUploadExcel.isbereavement, frmDownloadUploadExcel.isdecimal, frmDownloadUploadExcel.ispaydate)
        frmDownloadUploadExcel.BindProcessedPayrollToGrid()
    End Sub
    Private Sub gridloanpayment_Sorted(ByVal sender As Object, ByVal e As System.EventArgs)
        ColorGrid()
    End Sub
    Private Sub chkDisplayExcluded_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDisplayExcluded.CheckedChanged
        Call LoadPaymentsIncludeExclude(frmDownloadUploadExcel.fkpaycode, frmDownloadUploadExcel.isloan, frmDownloadUploadExcel.iscontribution, _
                                              frmDownloadUploadExcel.isbereavement, frmDownloadUploadExcel.isdecimal, frmDownloadUploadExcel.ispaydate)
    End Sub

#Region "Background Tasks"
    Private Sub bgwUnsettled_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwUnsettled.DoWork
        Call PostToHistory(GetSetfkPaycode(), GetSetIsLoan(), GetSetIsContribution(), GetSetIsBereavement(), _
                            GetSetBereavementAmount(), GetSetPaydate())
    End Sub
    Private Sub bgwUnsettled_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwUnsettled.RunWorkerCompleted
        frmStartup.Close()

        Call CreateExcelOutputFile()
    End Sub
#End Region

    Private Sub btnViewDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewDetails.Click
        frmDownloadUploadExcel.DisplayDeductionDetails()
        Close()
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        LoadPaymentsIncludeExclude_Filter(frmDownloadUploadExcel.fkpaycode, frmDownloadUploadExcel.isloan, frmDownloadUploadExcel.iscontribution, _
                                              frmDownloadUploadExcel.isbereavement, frmDownloadUploadExcel.isdecimal, frmDownloadUploadExcel.ispaydate, cbofgroup.Text, cbofcategory.Text, cboftype.Text)
    End Sub

    Private Sub LoadPaymentsIncludeExclude_Filter(ByVal paycode As String, ByVal isloadinclude As Boolean, ByVal iscontrinclude As Boolean, _
                                           ByVal isbereavinclude As Boolean, ByVal bereavement As Decimal, ByVal paydate As Date, ByVal fgroup As String, ByVal fcategory As String, ByVal ftype As String)
        'This will nullify the parameter for the script 'CIMS_Download_UnsettledLoanPayments_Load'
        'Needed for the script
        Dim showExclusion As String
        If chkDisplayExcluded.Checked = False Then
            showExclusion = "1"
        Else
            showExclusion = "0"
        End If

        Try
            Dim ds As DataSet = xSQLHelper.ExecuteDataset("CIMS_Download_UnsettledLoanPayments_Filter", _
                New SqlParameter("@paycode", paycode), _
                New SqlParameter("@isLoanIncluded", isloadinclude), _
                New SqlParameter("@isContributionIncluded", iscontrinclude), _
                New SqlParameter("@isBereavementIncluded", isbereavinclude), _
                New SqlParameter("@bereavementAmount", bereavement), _
                New SqlParameter("@payDate", paydate), _
                New SqlParameter("@isDisplayExcluded", showExclusion), _
                New SqlParameter("@fcGroup", fgroup), _
                New SqlParameter("@fcCategory", fcategory), _
                New SqlParameter("@fcType", ftype))

            unsettledPaymentsDS = ds
            If unsettledPaymentsDS.Tables(0).Rows.Count = 0 Then
                frmDownloadUploadExcel.HasUnsettledPayments = False
            Else
                frmDownloadUploadExcel.HasUnsettledPayments = True
            End If

            With gridloanpayment
                .DataSource = ds.Tables(0)
                .Columns("Select").Width = 60
                .Columns("Reference No").Width = 60
                .Columns("Member").Width = 200
                .Columns("Amount").Width = 80
                .Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns("Remarks").Width = 60
                .Columns("Employee No.").Visible = False
                .Columns("pk_DownloadHistory").Visible = False
            End With

            Call ColorGrid()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "LoadPaymentsIncludeExclude", MessageBoxButtons.OK)
        End Try
    End Sub
End Class