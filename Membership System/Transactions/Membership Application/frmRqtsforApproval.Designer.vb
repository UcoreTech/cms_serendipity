<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRqtsforApproval
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblloans = New System.Windows.Forms.Label()
        Me.cmdCancel_RqtsforApproval = New System.Windows.Forms.Button()
        Me.cmdUpdate_RqtsforApproval = New System.Windows.Forms.Button()
        Me.grdRqts = New System.Windows.Forms.DataGridView()
        Me.PanePanel5 = New WindowsApplication2.PanePanel()
        Me.lblEmpNo = New System.Windows.Forms.Label()
        CType(Me.grdRqts, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanePanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblloans
        '
        Me.lblloans.AutoSize = True
        Me.lblloans.BackColor = System.Drawing.Color.Transparent
        Me.lblloans.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblloans.ForeColor = System.Drawing.Color.White
        Me.lblloans.Location = New System.Drawing.Point(3, 2)
        Me.lblloans.Name = "lblloans"
        Me.lblloans.Size = New System.Drawing.Size(267, 19)
        Me.lblloans.TabIndex = 14
        Me.lblloans.Text = "Submit Requirements to complete"
        '
        'cmdCancel_RqtsforApproval
        '
        Me.cmdCancel_RqtsforApproval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdCancel_RqtsforApproval.Location = New System.Drawing.Point(291, 181)
        Me.cmdCancel_RqtsforApproval.Name = "cmdCancel_RqtsforApproval"
        Me.cmdCancel_RqtsforApproval.Size = New System.Drawing.Size(72, 25)
        Me.cmdCancel_RqtsforApproval.TabIndex = 39
        Me.cmdCancel_RqtsforApproval.Text = "Cancel"
        Me.cmdCancel_RqtsforApproval.UseVisualStyleBackColor = True
        '
        'cmdUpdate_RqtsforApproval
        '
        Me.cmdUpdate_RqtsforApproval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUpdate_RqtsforApproval.Location = New System.Drawing.Point(195, 181)
        Me.cmdUpdate_RqtsforApproval.Name = "cmdUpdate_RqtsforApproval"
        Me.cmdUpdate_RqtsforApproval.Size = New System.Drawing.Size(90, 25)
        Me.cmdUpdate_RqtsforApproval.TabIndex = 38
        Me.cmdUpdate_RqtsforApproval.Text = "Approved "
        Me.cmdUpdate_RqtsforApproval.UseVisualStyleBackColor = True
        '
        'grdRqts
        '
        Me.grdRqts.AllowUserToAddRows = False
        Me.grdRqts.AllowUserToDeleteRows = False
        Me.grdRqts.BackgroundColor = System.Drawing.Color.White
        Me.grdRqts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdRqts.Location = New System.Drawing.Point(0, 29)
        Me.grdRqts.Name = "grdRqts"
        Me.grdRqts.Size = New System.Drawing.Size(363, 147)
        Me.grdRqts.TabIndex = 37
        '
        'PanePanel5
        '
        Me.PanePanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel5.Controls.Add(Me.lblloans)
        Me.PanePanel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel5.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel5.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel5.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel5.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel5.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel5.Name = "PanePanel5"
        Me.PanePanel5.Size = New System.Drawing.Size(363, 26)
        Me.PanePanel5.TabIndex = 36
        '
        'lblEmpNo
        '
        Me.lblEmpNo.AutoSize = True
        Me.lblEmpNo.Location = New System.Drawing.Point(314, 193)
        Me.lblEmpNo.Name = "lblEmpNo"
        Me.lblEmpNo.Size = New System.Drawing.Size(0, 13)
        Me.lblEmpNo.TabIndex = 40
        '
        'frmRqtsforApproval
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(363, 208)
        Me.Controls.Add(Me.lblEmpNo)
        Me.Controls.Add(Me.cmdCancel_RqtsforApproval)
        Me.Controls.Add(Me.cmdUpdate_RqtsforApproval)
        Me.Controls.Add(Me.grdRqts)
        Me.Controls.Add(Me.PanePanel5)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmRqtsforApproval"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Rqts for Approval"
        CType(Me.grdRqts, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanePanel5.ResumeLayout(False)
        Me.PanePanel5.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblloans As System.Windows.Forms.Label
    Friend WithEvents cmdCancel_RqtsforApproval As System.Windows.Forms.Button
    Friend WithEvents cmdUpdate_RqtsforApproval As System.Windows.Forms.Button
    Friend WithEvents grdRqts As System.Windows.Forms.DataGridView
    Friend WithEvents PanePanel5 As WindowsApplication2.PanePanel
    Friend WithEvents lblEmpNo As System.Windows.Forms.Label
End Class
