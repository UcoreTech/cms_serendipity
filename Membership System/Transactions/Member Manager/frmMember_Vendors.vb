﻿Imports System.Data.SqlClient.SqlConnection
Imports System.Data.SqlClient
Imports System.Drawing
Imports Microsoft.VisualBasic.FileSystem
Imports System.Data
Imports System.IO
Imports Microsoft.VisualBasic
Imports System.Security
Imports System.Security.Principal.WindowsIdentity
Imports Microsoft.ApplicationBlocks.Data
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Web.Security
Imports System.Configuration

Public Class frmMember_Vendors

#Region "Variables"
    Dim pk_Vendor As String
    Dim TaxClassification As String
    Dim TaxAgent As Boolean
    Dim TaxCategory As Boolean
    Private gCon As New Clsappconfiguration()
    Dim AttachName As String
    Dim Attach As Byte()
    Dim AttachFile As String
    Dim AttachPath As String
    Dim ex As String
#End Region

    Private Sub GetPk_Vendor(ByVal VendorNo As String)
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gCon.cnstring, "CIMS_Member_Vendor_Select_KeyID",
                                      New SqlParameter("@fcVendorNo", VendorNo))
        While rd.Read
            pk_Vendor = rd("fxkey").ToString
        End While
    End Sub

#Region "Add/Edit Vendors"
    Private Sub AddVendors(ByVal IDno As String, ByVal Name As String, ByVal address As String, ByVal PurchaseOrder As String, ByVal ContactPerson As String, ByVal Terms As String, _
                               ByVal MobNo As String, ByVal TelNo As String, ByVal TINno As String, ByVal FAXno As String, ByVal Email As String, ByVal Postion As String, ByVal VerifiedDate As Date, _
                               ByVal Category As String, ByVal BusinessNature As String, ByVal Instruction As String, ByVal PreparedBy As String, ByVal DatePrepared As Date, ByVal EncodedBy As String,
                               ByVal EncodedDate As Date, ByVal TAXClassification As String, ByVal TAXagent As Boolean, ByVal TAXCategPercent As String, ByVal TAXCategCheck As Boolean, ByVal TAXCategOn As String, _
                               ByVal AttachType As String, ByVal AttachName As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Vendors_Add",
                                      New SqlParameter("@fcVendorNo", IDno), _
                                      New SqlParameter("@fcPurchaseOrder", PurchaseOrder), _
                                      New SqlParameter("@fcName", Name), _
                                      New SqlParameter("@fcAddress", address), _
                                      New SqlParameter("@fcContractPerson", ContactPerson), _
                                      New SqlParameter("@fcTermsOfPayment", Terms), _
                                      New SqlParameter("@fcMobileNo", MobNo), _
                                      New SqlParameter("@fcTelNo", TelNo), _
                                      New SqlParameter("@fcTINNo", TINno), _
                                      New SqlParameter("@fcFAXNo", FAXno), _
                                      New SqlParameter("@fcEmailAddress", Email), _
                                      New SqlParameter("@fcPosition", Postion), _
                                      New SqlParameter("@fcBIRVerifyDate", VerifiedDate), _
                                      New SqlParameter("@fcCategory", Category), _
                                      New SqlParameter("@fcBusinessNature", BusinessNature), _
                                      New SqlParameter("@fcInstruction", Instruction), _
                                      New SqlParameter("@fcPreparedBy", PreparedBy), _
                                      New SqlParameter("@fcDatePrepared", DatePrepared), _
                                      New SqlParameter("@fcEncodeBy", EncodedBy), _
                                      New SqlParameter("@fcDateEncoded", EncodedDate), _
                                      New SqlParameter("@fcTaxClisification", TAXClassification), _
                                      New SqlParameter("@fcTaxAgent", TAXagent), _
                                      New SqlParameter("@fcTaxCategoryPercent", TAXCategPercent), _
                                      New SqlParameter("@fcTaxCategoryCheck", TAXCategCheck), _
                                      New SqlParameter("@fcTaxCategoryOn", TAXCategOn), _
                                      New SqlParameter("@fcAttachType", AttachType), _
                                      New SqlParameter("@fcAttachName", AttachName))
            trans.Commit()
            MessageBox.Show("Records successfully saved!", "Saving Record...", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message)
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub EditVendors(ByVal IDno As String, ByVal Name As String, ByVal address As String, ByVal PurchaseOrder As String, ByVal ContactPerson As String, ByVal Terms As String, _
                           ByVal MobNo As String, ByVal TelNo As String, ByVal TINno As String, ByVal FAXno As String, ByVal Email As String, ByVal Postion As String, ByVal VerifiedDate As Date, _
                           ByVal Category As String, ByVal BusinessNature As String, ByVal Instruction As String, ByVal PreparedBy As String, ByVal DatePrepared As Date, ByVal EncodedBy As String,
                           ByVal EncodedDate As Date, ByVal TAXClassification As String, ByVal TAXagent As Boolean, ByVal TAXCategPercent As String, ByVal TAXCategCheck As Boolean, ByVal TAXCategOn As String, _
                           ByVal AttachType As String, ByVal AttachName As String, ByVal pkVendorID As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            Dim Myguid As Guid = New Guid(pkVendorID)

            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Vendors_Edit",
                                      New SqlParameter("@fcVendorNo", IDno), _
                                      New SqlParameter("@fcPurchaseOrder", PurchaseOrder), _
                                      New SqlParameter("@fcName", Name), _
                                      New SqlParameter("@fcAddress", address), _
                                      New SqlParameter("@fcContractPerson", ContactPerson), _
                                      New SqlParameter("@fcTermsOfPayment", Terms), _
                                      New SqlParameter("@fcMobileNo", MobNo), _
                                      New SqlParameter("@fcTelNo", TelNo), _
                                      New SqlParameter("@fcTINNo", TINno), _
                                      New SqlParameter("@fcFAXNo", FAXno), _
                                      New SqlParameter("@fcEmailAddress", Email), _
                                      New SqlParameter("@fcPosition", Postion), _
                                      New SqlParameter("@fcBIRVerifyDate", VerifiedDate), _
                                      New SqlParameter("@fcCategory", Category), _
                                      New SqlParameter("@fcBusinessNature", BusinessNature), _
                                      New SqlParameter("@fcInstruction", Instruction), _
                                      New SqlParameter("@fcPreparedBy", PreparedBy), _
                                      New SqlParameter("@fcDatePrepared", DatePrepared), _
                                      New SqlParameter("@fcEncodeBy", EncodedBy), _
                                      New SqlParameter("@fcDateEncoded", EncodedDate), _
                                      New SqlParameter("@fcTaxClisification", TAXClassification), _
                                      New SqlParameter("@fcTaxAgent", TAXagent), _
                                      New SqlParameter("@fcTaxCategoryPercent", TAXCategPercent), _
                                      New SqlParameter("@fcTaxCategoryCheck", TAXCategCheck), _
                                      New SqlParameter("@fcTaxCategoryOn", TAXCategOn), _
                                      New SqlParameter("@fcAttachType", AttachType), _
                                      New SqlParameter("@fcAttachName", AttachName), _
                                      New SqlParameter("@pk_VendorsID", pkVendorID))
            trans.Commit()
            MessageBox.Show("Records successfully saved!", "Saving Record...", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message)
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region

#Region "Delete Vendor"
    Private Sub DeleteVendor(ByVal VendorID As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Vendor_Delete", _
                                      New SqlParameter("@fcVendorNo", VendorID))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message)
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region

    Private Sub btnemp_close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_close.Click
        If btnemp_close.Text = "Close" Then
            Me.Close()
        ElseIf btnemp_close.Text = "Cancel" Then
            Call DisableText()
            Call EnableButton()
            If btnemp_add.Text = "Save" Then
                btnemp_delete.Visible = True
                btnemp_edit.Visible = True
                btnemp_close.Text = "Close"
                btnemp_add.Text = "New"
                btnAttach.Enabled = False
                btnemp_add.Image = My.Resources.new3
            End If
            If btnemp_edit.Text = "Update" Then
                btnemp_delete.Visible = True
                btnemp_add.Visible = True
                btnemp_close.Text = "Close"
                btnemp_edit.Text = "Edit"
                btnAttach.Enabled = False
                btnemp_edit.Image = My.Resources.edit1
            End If
            If btnAttach.Text = "Browse" Then
                btnAttach.Text = "Attach"
                btnAttach.Image = My.Resources.images__14_
                cboAttach.Enabled = False
                txtAttach.Enabled = False
                btnAttach.Enabled = False
            End If
        End If
    End Sub

#Region "Enable/Disable/Clear Text & Buttons"
    Private Sub ClearText()
        txtID.Text = ""
        txtName.Text = ""
        txtAddress.Text = ""
        txtBusiness.Text = ""
        txtTIN.Text = ""
        txtCategory.Text = ""
        txtFAX.Text = ""
        txtContactPerson.Text = ""
        txtPosition.Text = ""
        txtTelephone.Text = ""
        txtMobNo.Text = ""
        txtEmail.Text = ""
        txtTerms.Text = ""
        txtPurchase.Text = ""
        txtRemarks.Text = ""
        txtPreparedBy.Text = ""
        txtEncodeby.Text = ""
        rbGovernment.Checked = False
        rbNo.Checked = False
        chkCategory.Checked = False
        rbVatable.Checked = False
        rbYes.Checked = False
        rbZeroRated.Checked = False
        lvlAttachment.Clear()
        txtPercent.Text = ""
        txtTAXon.Text = ""
    End Sub

    Private Sub EnableText()
        txtID.Enabled = True
        txtName.Enabled = True
        txtAddress.Enabled = True
        txtBusiness.Enabled = True
        txtTIN.Enabled = True
        txtCategory.Enabled = True
        txtFAX.Enabled = True
        txtContactPerson.Enabled = True
        txtPosition.Enabled = True
        txtTelephone.Enabled = True
        txtMobNo.Enabled = True
        txtEmail.Enabled = True
        txtTerms.Enabled = True
        txtPurchase.Enabled = True
        txtRemarks.Enabled = True
        txtPreparedBy.Enabled = True
        txtEncodeby.Enabled = True
        rbGovernment.Enabled = True
        rbNo.Enabled = True
        chkCategory.Enabled = True
        rbVatable.Enabled = True
        rbYes.Enabled = True
        rbZeroRated.Enabled = True
        dtBIRVerify.Enabled = True
        dtEncode.Enabled = True
        dtPrepared.Enabled = True
        txtPercent.Enabled = True
        txtTAXon.Enabled = True
    End Sub

    Private Sub DisableText()
        txtID.Enabled = False
        txtName.Enabled = False
        txtAddress.Enabled = False
        txtBusiness.Enabled = False
        txtTIN.Enabled = False
        txtCategory.Enabled = False
        txtFAX.Enabled = False
        txtContactPerson.Enabled = False
        txtPosition.Enabled = False
        txtTelephone.Enabled = False
        txtMobNo.Enabled = False
        txtEmail.Enabled = False
        txtTerms.Enabled = False
        txtPurchase.Enabled = False
        txtRemarks.Enabled = False
        txtPreparedBy.Enabled = False
        txtEncodeby.Enabled = False
        rbGovernment.Enabled = False
        rbNo.Enabled = False
        chkCategory.Enabled = False
        rbVatable.Enabled = False
        rbYes.Enabled = False
        rbZeroRated.Enabled = False
        dtBIRVerify.Enabled = False
        dtEncode.Enabled = False
        dtPrepared.Enabled = False
        txtPercent.Enabled = False
        txtTAXon.Enabled = False
    End Sub

    Private Sub EnableButton()
        BTNSEARCH.Enabled = True
        btnUpload.Enabled = True
    End Sub

    Private Sub DisableButton()
        BTNSEARCH.Enabled = False
        btnUpload.Enabled = False
    End Sub
#End Region

    Private Sub btnemp_add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_add.Click
        If btnemp_add.Text = "New" Then
            txtID.Focus()
            Call ClearText()
            Call EnableText()
            Call DisableButton()
            btnemp_delete.Visible = False
            btnemp_edit.Visible = False
            btnemp_close.Text = "Cancel"
            btnemp_add.Text = "Save"
            btnAttach.Enabled = True
            btnemp_edit.Image = My.Resources.save2
        ElseIf btnemp_add.Text = "Save" Then
            If txtID.Text = "" Then
                MessageBox.Show("Nothing to Save", "Invalid Saving", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Else
                If rbGovernment.Checked = True Then
                    TaxClassification = "Government"
                ElseIf rbVatable.Checked = True Then
                    TaxClassification = "Vatable"
                ElseIf rbZeroRated.Checked = True Then
                    TaxClassification = "Zero-Rated"
                End If

                If rbYes.Checked Then
                    TaxAgent = True
                ElseIf rbNo.Checked = True Then
                    TaxAgent = False
                End If

                If chkCategory.Checked = True Then
                    TaxCategory = True
                Else
                    TaxCategory = False
                End If
                Call AddVendors(txtID.Text.Trim, txtName.Text.Trim, txtAddress.Text.Trim, txtPurchase.Text.Trim, txtContactPerson.Text.Trim, txtTerms.Text.Trim, txtMobNo.Text.Trim, _
                                    txtTelephone.Text.Trim, txtTIN.Text.Trim, txtFAX.Text.Trim, txtEmail.Text.Trim, txtPercent.Text.Trim, dtBIRVerify.Value, txtCategory.Text.Trim, _
                                    txtBusiness.Text.Trim, txtRemarks.Text.Trim, txtPreparedBy.Text.Trim, dtPrepared.Value, txtEncodeby.Text.Trim, dtEncode.Value, TaxClassification, _
                                    TaxAgent, txtPercent.Text.Trim, TaxCategory, txtTAXon.Text.Trim, cboAttach.Text, txtAttach.Text)
                Call GetVendorrAttach(txtID.Text)
                Call DisableText()
                Call EnableButton()
                btnemp_delete.Visible = True
                btnemp_edit.Visible = True
                btnemp_close.Text = "Close"
                btnemp_add.Text = "New"
                btnAttach.Enabled = False
                btnemp_add.Image = My.Resources.new3
            End If
        End If
    End Sub

    Private Sub frmMember_Vendors_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnAttach.Enabled = False
        cboAttach.Enabled = False
        txtAttach.Enabled = False
        Call DisableText()
    End Sub

    Private Sub btnemp_edit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_edit.Click
        If txtID.Text = "" Then
            MessageBox.Show("Nothing to Edit", "Invalid Editing", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            If btnemp_edit.Text = "Edit" Then
                Call EnableText()
                Call DisableButton()
                btnemp_delete.Visible = False
                btnemp_add.Visible = False
                btnemp_close.Text = "Cancel"
                btnemp_edit.Text = "Update"
                btnAttach.Enabled = True
                btnemp_edit.Image = My.Resources.save2
            ElseIf btnemp_edit.Text = "Update" Then
                If rbGovernment.Checked = True Then
                    TaxClassification = "Government"
                ElseIf rbVatable.Checked = True Then
                    TaxClassification = "Vatable"
                ElseIf rbZeroRated.Checked = True Then
                    TaxClassification = "Zero-Rated"
                End If

                If rbYes.Checked Then
                    TaxAgent = True
                ElseIf rbNo.Checked = True Then
                    TaxAgent = False
                End If

                If chkCategory.Checked = True Then
                    TaxCategory = True
                Else
                    TaxCategory = False
                End If
                Call EditVendors(txtID.Text.Trim, txtName.Text.Trim, txtAddress.Text.Trim, txtPurchase.Text.Trim, txtContactPerson.Text.Trim, txtTerms.Text.Trim, txtMobNo.Text.Trim, _
                    txtTelephone.Text.Trim, txtTIN.Text.Trim, txtFAX.Text.Trim, txtEmail.Text.Trim, txtPercent.Text.Trim, dtBIRVerify.Value, txtCategory.Text.Trim, _
                    txtBusiness.Text.Trim, txtRemarks.Text.Trim, txtPreparedBy.Text.Trim, dtPrepared.Value, txtEncodeby.Text.Trim, dtEncode.Value, TaxClassification, _
                    TaxAgent, txtPercent.Text.Trim, TaxCategory, txtTAXon.Text.Trim, cboAttach.Text, txtAttach.Text, pk_Vendor)
                Call DisableText()
                Call EnableButton()
                btnemp_delete.Visible = True
                btnemp_edit.Visible = True
                btnemp_close.Text = "Close"
                btnemp_edit.Text = "Edit"
                btnAttach.Enabled = False
                btnemp_edit.Image = My.Resources.edit1
            End If
        End If
    End Sub

    Private Sub btnemp_delete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_delete.Click
        Dim x As New DialogResult
        x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
        If x = System.Windows.Forms.DialogResult.OK Then
            Call DeleteVendor(Me.txtID.Text.Trim)
            Call ClearText()
        ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
        End If
    End Sub

    Private Sub BTNSEARCH_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTNSEARCH.Click
        frmMember_SearchVendor.ShowDialog()
    End Sub

    Private Sub txtID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtID.TextChanged
        Call GetPk_Vendor(txtID.Text)
    End Sub

    Private Sub txtEmail_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtEmail.Validating
        Dim Expression As New System.Text.RegularExpressions.Regex("\S+@\S+\.\S+")
        If Expression.IsMatch(txtEmail.Text) Then

        Else
            MessageBox.Show("The email address is NOT valid.")
            txtEmail.Text = ""
        End If
    End Sub

#Region "Attach"

    Private Sub btnAttach_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAttach.Click
        If btnAttach.Text = "Attach" Then
            cboAttach.Enabled = True
            btnAttach.Image = Nothing
            btnAttach.Text = "Browse"
            btnAttach.TextAlign = ContentAlignment.MiddleCenter
        ElseIf btnAttach.Text = "Browse" Then
            FileAttachment()
            cboAttach.Enabled = False
            txtAttach.Enabled = False
            btnAttach.Enabled = False
            btnAttach.Text = "Attach"
            btnAttach.Image = My.Resources.images__14_
            btnAttach.TextAlign = ContentAlignment.MiddleRight
        End If
    End Sub

    Private Sub cboAttach_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAttach.SelectedValueChanged
        txtAttach.Enabled = True
    End Sub

    Private Sub FileAttachment()
        Dim openFileDialog1 As New OpenFileDialog()
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()

        Try
            openFileDialog1.Filter = "Text Files (*.txt)|*.txt|PDF Files (*.pdf)|*.pdf|Word Documents (*.docx)|*.docx|Excel Worksheets (*.xlsx)|*.xlsx|PowerPoint Presentations (*.pptx)|*.pptx|Word Documents 97-2003 (*.doc)|*.docx|Excel Worksheets 93-2003 (*.xls)|*.xls|PowerPoint Presentations (*.ppt)|*.ppt" & "|Office Files|*.docx;*.xlsx;*.pptx;*.doc;*.xls;*.ppt" & "|Images|*.jpg;*.png;*.gif"
            openFileDialog1.Title = "Select File"
            If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                AttachFile = openFileDialog1.FileName
                AttachPath = Path.GetExtension(AttachFile)
                AttachName = Path.GetFileName(AttachFile)

                Attach = File.ReadAllBytes(AttachFile)
                ex = Path.GetExtension(LTrim(RTrim(AttachFile)))

                Dim ms As New MemoryStream(Attach, 0, Attach.Length)

                ms.Write(Attach, 0, Attach.Length)
                txtAttach.Text = AttachName
            Else
                MessageBox.Show("User Cancelled!", "Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Attach Required")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Try
            If lvlAttachment.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Files to Download", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                btnUpload.Enabled = False
                Exit Sub
            Else
                Dim MyGuid As Guid = New Guid(Me.lvlAttachment.SelectedItems(0).SubItems(3).Text)
                Dim afile As Byte()
                Dim myconnection As New Clsappconfiguration
                Dim cmd As New SqlCommand("MSS_MembersInfo_GetVendorsAttach_AttachedFiles", myconnection.sqlconn)
                cmd.CommandType = CommandType.StoredProcedure
                myconnection.sqlconn.Open()
                cmd.Parameters.Add("@VendorNo", SqlDbType.VarChar, 256).Value = txtID.Text
                cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 100).Value = Me.lvlAttachment.SelectedItems(0).SubItems(1).Text
                cmd.Parameters.Add("@pk_Vendor", SqlDbType.UniqueIdentifier).Value = MyGuid
                SaveFileDialog1.Filter = "Text Files (*.txt)|*.txt|PDF Files (*.pdf)|*.pdf|Word Documents (*.docx)|*.docx|Excel Worksheets (*.xlsx)|*.xlsx|PowerPoint Presentations (*.pptx)|*.pptx|Word Documents 97-2003 (*.doc)|*.docx|Excel Worksheets 93-2003 (*.xls)|*.xls|PowerPoint Presentations (*.ppt)|*.ppt" & "|Office Files|*.docx;*.xlsx;*.pptx;*.doc;*.xls;*.ppt" & "|Images|*.jpg;*.png;*.gif"
                SaveFileDialog1.Title = "Upload Files"
                Using myreader As SqlDataReader = cmd.ExecuteReader
                    myreader.Read()
                    afile = DirectCast(myreader("fcAttachment"), Byte())
                End Using
                If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
                    Dim FiletoSave As String = SaveFileDialog1.FileName
                    Dim Stream As FileStream = New FileStream(FiletoSave, FileMode.Create, FileAccess.Write)
                    Stream.Write(afile, 0, afile.Length)
                    Stream.Close()
                    btnUpload.Enabled = False
                    MessageBox.Show("File Successfully Uploaded!", "Uploaded", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    SaveFileDialog1.FileName = ""
                ElseIf DialogResult.Cancel Then
                    btnUpload.Enabled = False
                    MessageBox.Show("User Cancelled!", "Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                myconnection.sqlconn.Close()
                btnUpload.Enabled = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Requirement Download File", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Public Sub GetVendorrAttach(ByVal Vendor As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetVendor_Attachment", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@fcVendorNo", SqlDbType.VarChar, 256).Value = Vendor
        With Me.lvlAttachment
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Attachment Type", 250, HorizontalAlignment.Left)
            .Columns.Add("Attachment Name", 300, HorizontalAlignment.Left)
            .Columns.Add("Date Attach", 100, HorizontalAlignment.Left)

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .SubItems.Add(myreader.Item(1))
                        .SubItems.Add(myreader.Item(2))
                        .SubItems.Add(myreader.Item(3))
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub

    Private Sub lvlAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvlAttachment.Click
        If btnemp_edit.Text = "Update" Then
            btnUpload.Enabled = False
        Else
            If lvlAttachment.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Files to Download", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                btnUpload.Enabled = False
                Exit Sub
            Else
                btnUpload.Enabled = True
            End If
        End If
    End Sub
#End Region
End Class