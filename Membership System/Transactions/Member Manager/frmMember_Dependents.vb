Imports System.Data.SqlClient.SqlConnection
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMember_Dependents
#Region "variables"
    Public getdep_id As String
#End Region
#Region "Get Property"
    Public Property getempdepID() As String
        Get
            Return getdep_id
        End Get
        Set(ByVal value As String)
            getdep_id = value
        End Set
    End Property
#End Region
#Region "Add/Edit Dependents"
    Private Sub AddEditDependents(ByVal empid As String, ByVal name As String, ByVal relationship As String, ByVal Birthdate As Date, ByVal pkid As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Dependents_AddEdit", _
                                      New SqlParameter("@employeeNo", empid), _
                                      New SqlParameter("@fcDependentName", name), _
                                      New SqlParameter("@fcRelationship", relationship), _
                                      New SqlParameter("@Birthdate", Birthdate), _
                                      New SqlParameter("@pk_Dependents", pkid))
            trans.Commit()
            MessageBox.Show("Record has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Add new Dependents")
        Finally
            gcon.sqlconn.Close()
        End Try

    End Sub
#End Region
    Private Sub btnclose_dep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose_dep.Click
        Me.Close()
    End Sub

    Private Sub btnsave_dep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave_dep.Click
        If txtname.Text = "" Or txtrelation.Text = "" Then
            MessageBox.Show("Complete Information is Needed", "Dependents Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Call AddEditDependents(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtname.Text, Me.txtrelation.Text, Me.dtBirthdate.Value, "")
            Call frmMember_Master.GetmemberDependents(frmMember_Master.txtEmployeeNo.Text.Trim)
            Call ClearText()
            'Me.Close()
        End If
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Call AddEditDependents(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtname.Text, Me.txtrelation.Text, Me.dtBirthdate.Value, getdep_id)
            Call frmMember_Master.GetmemberDependents(frmMember_Master.txtEmployeeNo.Text.Trim)
            Me.Close()
    End Sub

    Private Sub txtphone_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtphone.KeyPress
        e.Handled = Not txtphone_validate(e.KeyChar)
    End Sub

    Public Function txtphone_validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = "-") And Not (C = "(") And Not (C = ")") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub ClearText()
        txtname.Text = ""
        txtrelation.Text = ""
    End Sub
End Class