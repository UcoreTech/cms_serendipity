﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMember_Medical
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMember_Medical))
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtClaimType = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.lblMedical = New System.Windows.Forms.Label()
        Me.PanePanel2 = New WindowsApplication2.PanePanel()
        Me.btnupdate = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnclose = New System.Windows.Forms.Button()
        Me.txtAttachFiles = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnBrowse = New System.Windows.Forms.Button()
        Me.dtDateFiled = New System.Windows.Forms.DateTimePicker()
        Me.txtStat = New System.Windows.Forms.TextBox()
        Me.TxtStatus = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtPayeeNumber = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtPayeeName = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtAmountPaid = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtCheckNo = New System.Windows.Forms.TextBox()
        Me.dtFrom = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.dtdateTo = New System.Windows.Forms.DateTimePicker()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtConfineDate = New System.Windows.Forms.TextBox()
        Me.dtSettlement = New System.Windows.Forms.DateTimePicker()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.PanePanel1.SuspendLayout()
        Me.PanePanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(42, 62)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 13)
        Me.Label3.TabIndex = 61
        Me.Label3.Text = "Claim Type"
        '
        'txtClaimType
        '
        Me.txtClaimType.Location = New System.Drawing.Point(113, 59)
        Me.txtClaimType.MaxLength = 30
        Me.txtClaimType.Name = "txtClaimType"
        Me.txtClaimType.Size = New System.Drawing.Size(241, 20)
        Me.txtClaimType.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(46, 42)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 57
        Me.Label1.Text = "Date Filed"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 238)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(92, 13)
        Me.Label2.TabIndex = 58
        Me.Label2.Text = "Confinement Date"
        '
        'PanePanel1
        '
        Me.PanePanel1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.lblMedical)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel1.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(380, 30)
        Me.PanePanel1.TabIndex = 55
        '
        'lblMedical
        '
        Me.lblMedical.AutoSize = True
        Me.lblMedical.BackColor = System.Drawing.Color.Transparent
        Me.lblMedical.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMedical.ForeColor = System.Drawing.Color.White
        Me.lblMedical.Location = New System.Drawing.Point(15, 3)
        Me.lblMedical.Name = "lblMedical"
        Me.lblMedical.Size = New System.Drawing.Size(21, 19)
        Me.lblMedical.TabIndex = 18
        Me.lblMedical.Text = "..."
        '
        'PanePanel2
        '
        Me.PanePanel2.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel2.Controls.Add(Me.btnupdate)
        Me.PanePanel2.Controls.Add(Me.btnSave)
        Me.PanePanel2.Controls.Add(Me.btnclose)
        Me.PanePanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel2.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel2.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel2.Location = New System.Drawing.Point(0, 306)
        Me.PanePanel2.Name = "PanePanel2"
        Me.PanePanel2.Size = New System.Drawing.Size(380, 37)
        Me.PanePanel2.TabIndex = 56
        '
        'btnupdate
        '
        Me.btnupdate.Image = CType(resources.GetObject("btnupdate.Image"), System.Drawing.Image)
        Me.btnupdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnupdate.Location = New System.Drawing.Point(139, 5)
        Me.btnupdate.Name = "btnupdate"
        Me.btnupdate.Size = New System.Drawing.Size(66, 28)
        Me.btnupdate.TabIndex = 15
        Me.btnupdate.Text = "Update"
        Me.btnupdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnupdate.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(5, 5)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(66, 28)
        Me.btnSave.TabIndex = 14
        Me.btnSave.Text = "Save"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnclose
        '
        Me.btnclose.Image = Global.WindowsApplication2.My.Resources.Resources.eventlogError
        Me.btnclose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnclose.Location = New System.Drawing.Point(72, 5)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.Size = New System.Drawing.Size(66, 28)
        Me.btnclose.TabIndex = 16
        Me.btnclose.Text = "Cancel"
        Me.btnclose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnclose.UseVisualStyleBackColor = True
        '
        'txtAttachFiles
        '
        Me.txtAttachFiles.Enabled = False
        Me.txtAttachFiles.Location = New System.Drawing.Point(113, 280)
        Me.txtAttachFiles.Name = "txtAttachFiles"
        Me.txtAttachFiles.Size = New System.Drawing.Size(180, 20)
        Me.txtAttachFiles.TabIndex = 87
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(44, 283)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(57, 13)
        Me.Label8.TabIndex = 86
        Me.Label8.Text = "Attach File"
        '
        'btnBrowse
        '
        Me.btnBrowse.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnBrowse.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnBrowse.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btnBrowse.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrowse.Location = New System.Drawing.Point(299, 280)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(55, 20)
        Me.btnBrowse.TabIndex = 13
        Me.btnBrowse.Text = "..."
        Me.btnBrowse.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBrowse.UseVisualStyleBackColor = False
        '
        'dtDateFiled
        '
        Me.dtDateFiled.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtDateFiled.Location = New System.Drawing.Point(113, 36)
        Me.dtDateFiled.Name = "dtDateFiled"
        Me.dtDateFiled.Size = New System.Drawing.Size(107, 20)
        Me.dtDateFiled.TabIndex = 1
        '
        'txtStat
        '
        Me.txtStat.Location = New System.Drawing.Point(113, 257)
        Me.txtStat.Name = "txtStat"
        Me.txtStat.Size = New System.Drawing.Size(241, 20)
        Me.txtStat.TabIndex = 12
        '
        'TxtStatus
        '
        Me.TxtStatus.AutoSize = True
        Me.TxtStatus.Location = New System.Drawing.Point(64, 263)
        Me.TxtStatus.Name = "TxtStatus"
        Me.TxtStatus.Size = New System.Drawing.Size(37, 13)
        Me.TxtStatus.TabIndex = 90
        Me.TxtStatus.Text = "Status"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(24, 84)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(77, 13)
        Me.Label4.TabIndex = 92
        Me.Label4.Text = "Payee Number"
        '
        'txtPayeeNumber
        '
        Me.txtPayeeNumber.Location = New System.Drawing.Point(113, 81)
        Me.txtPayeeNumber.MaxLength = 30
        Me.txtPayeeNumber.Name = "txtPayeeNumber"
        Me.txtPayeeNumber.Size = New System.Drawing.Size(241, 20)
        Me.txtPayeeNumber.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(33, 106)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(68, 13)
        Me.Label5.TabIndex = 94
        Me.Label5.Text = "Payee Name"
        '
        'txtPayeeName
        '
        Me.txtPayeeName.Location = New System.Drawing.Point(113, 103)
        Me.txtPayeeName.MaxLength = 30
        Me.txtPayeeName.Name = "txtPayeeName"
        Me.txtPayeeName.Size = New System.Drawing.Size(241, 20)
        Me.txtPayeeName.TabIndex = 4
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(56, 128)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(45, 13)
        Me.Label6.TabIndex = 96
        Me.Label6.Text = "Address"
        '
        'txtAddress
        '
        Me.txtAddress.Location = New System.Drawing.Point(113, 125)
        Me.txtAddress.MaxLength = 30
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(241, 20)
        Me.txtAddress.TabIndex = 5
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(34, 150)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(67, 13)
        Me.Label7.TabIndex = 98
        Me.Label7.Text = "Amount Paid"
        '
        'txtAmountPaid
        '
        Me.txtAmountPaid.Location = New System.Drawing.Point(113, 147)
        Me.txtAmountPaid.MaxLength = 30
        Me.txtAmountPaid.Name = "txtAmountPaid"
        Me.txtAmountPaid.Size = New System.Drawing.Size(131, 20)
        Me.txtAmountPaid.TabIndex = 6
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(25, 172)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(78, 13)
        Me.Label9.TabIndex = 100
        Me.Label9.Text = "Check Number"
        '
        'txtCheckNo
        '
        Me.txtCheckNo.Location = New System.Drawing.Point(113, 169)
        Me.txtCheckNo.MaxLength = 30
        Me.txtCheckNo.Name = "txtCheckNo"
        Me.txtCheckNo.Size = New System.Drawing.Size(241, 20)
        Me.txtCheckNo.TabIndex = 7
        '
        'dtFrom
        '
        Me.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFrom.Location = New System.Drawing.Point(113, 213)
        Me.dtFrom.Name = "dtFrom"
        Me.dtFrom.Size = New System.Drawing.Size(107, 20)
        Me.dtFrom.TabIndex = 9
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(69, 216)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(30, 13)
        Me.Label10.TabIndex = 102
        Me.Label10.Text = "From"
        '
        'dtdateTo
        '
        Me.dtdateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtdateTo.Location = New System.Drawing.Point(250, 213)
        Me.dtdateTo.Name = "dtdateTo"
        Me.dtdateTo.Size = New System.Drawing.Size(107, 20)
        Me.dtdateTo.TabIndex = 10
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(225, 216)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(20, 13)
        Me.Label11.TabIndex = 104
        Me.Label11.Text = "To"
        '
        'txtConfineDate
        '
        Me.txtConfineDate.Enabled = False
        Me.txtConfineDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConfineDate.Location = New System.Drawing.Point(113, 235)
        Me.txtConfineDate.Name = "txtConfineDate"
        Me.txtConfineDate.Size = New System.Drawing.Size(63, 20)
        Me.txtConfineDate.TabIndex = 11
        '
        'dtSettlement
        '
        Me.dtSettlement.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtSettlement.Location = New System.Drawing.Point(113, 191)
        Me.dtSettlement.Name = "dtSettlement"
        Me.dtSettlement.Size = New System.Drawing.Size(132, 20)
        Me.dtSettlement.TabIndex = 8
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(18, 194)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(83, 13)
        Me.Label12.TabIndex = 107
        Me.Label12.Text = "Settlement Date"
        '
        'frmMember_Medical
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(380, 343)
        Me.Controls.Add(Me.dtSettlement)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.txtConfineDate)
        Me.Controls.Add(Me.dtdateTo)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.dtFrom)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtCheckNo)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtAmountPaid)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtAddress)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtPayeeName)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtPayeeNumber)
        Me.Controls.Add(Me.txtStat)
        Me.Controls.Add(Me.TxtStatus)
        Me.Controls.Add(Me.dtDateFiled)
        Me.Controls.Add(Me.txtAttachFiles)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.btnBrowse)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtClaimType)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PanePanel2)
        Me.Name = "frmMember_Medical"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Medical"
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.PanePanel2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblMedical As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtClaimType As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PanePanel2 As WindowsApplication2.PanePanel
    Friend WithEvents btnupdate As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnclose As System.Windows.Forms.Button
    Friend WithEvents txtAttachFiles As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents dtDateFiled As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtStat As System.Windows.Forms.TextBox
    Friend WithEvents TxtStatus As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtPayeeNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtPayeeName As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtAddress As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtAmountPaid As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtCheckNo As System.Windows.Forms.TextBox
    Friend WithEvents dtFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dtdateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtConfineDate As System.Windows.Forms.TextBox
    Friend WithEvents dtSettlement As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label12 As System.Windows.Forms.Label
End Class
