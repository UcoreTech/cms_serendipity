﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmMember_EmploymentHistory
    Dim pk_Employment As String

#Region "Property"
    Public Property getpkEmployment() As String
        Get
            Return pk_Employment
        End Get
        Set(ByVal value As String)
            pk_Employment = value
        End Set
    End Property
#End Region

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub AddEditEmployment(ByVal EmpNo As String, ByVal dtIncDateFrom As Date, ByVal CompName As String, ByVal Pos As String, ByVal JobDesc As String, _
                                  ByVal EmpStatus As String, ByVal Superior As String, ByVal Reason As String, ByVal telNo As String, ByVal pkEmployment As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_EmploymentHistory_AddEdit", _
                                     New SqlParameter("@employeeNo", EmpNo), _
                                     New SqlParameter("@dtDateFrom", dtIncDateFrom), _
                                     New SqlParameter("@dtDateTo", dtIncDateTo.Value), _
                                     New SqlParameter("@fcCompanyName", CompName), _
                                     New SqlParameter("@fcPosition", Pos), _
                                     New SqlParameter("@fcJobDesc", JobDesc), _
                                     New SqlParameter("@fcEmploymentStat", EmpStatus), _
                                     New SqlParameter("@fcSuperior", Superior), _
                                     New SqlParameter("@fcReasons", Reason), _
                                     New SqlParameter("@fcTelNo", telNo), _
                                     New SqlParameter("@pk_EmploymentHistory", pkEmployment))
            trans.Commit()
            MessageBox.Show("Record has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Save Employment History")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtCompName.Text = "" Or txtEmpStat.Text = "" Or txtPosition.Text = "" Or txtReasons.Text = "" Or txtSuperior.Text = "" Or txtTelNo.Text = "" Then
            MessageBox.Show("Complete Information is Needed", "Employment History Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Call AddEditEmployment(frmMember_Master.txtEmployeeNo.Text.Trim, Me.dtIncDateFrom.Value, Me.txtCompName.Text.Trim, Me.txtPosition.Text.Trim, Me.txtJobDesc.Text.Trim,
                                   Me.txtEmpStat.Text.Trim, Me.txtSuperior.Text.Trim, Me.txtReasons.Text.Trim, Me.txtTelNo.Text.Trim, "")
            Call frmMember_Master.GetmemberEmployment(frmMember_Master.txtEmployeeNo.Text)
            Call ClearText()
            'Me.Close()
        End If
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Call AddEditEmployment(frmMember_Master.txtEmployeeNo.Text.Trim, Me.dtIncDateFrom.Value, Me.txtCompName.Text.Trim, Me.txtPosition.Text.Trim, Me.txtJobDesc.Text.Trim,
                                   Me.txtEmpStat.Text.Trim, Me.txtSuperior.Text.Trim, Me.txtReasons.Text.Trim, Me.txtTelNo.Text.Trim, pk_Employment)
        Call frmMember_Master.GetmemberEmployment(frmMember_Master.txtEmployeeNo.Text)
        Me.Close()
    End Sub

    Private Sub ClearText()
        txtCompName.Text = ""
        txtEmpStat.Text = ""
        txtPosition.Text = ""
        txtReasons.Text = ""
        txtSuperior.Text = ""
        txtTelNo.Text = ""
        txtJobDesc.Text = ""
    End Sub
End Class