<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMember_Master
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMember_Master))
        Me.tabMember = New System.Windows.Forms.TabControl()
        Me.ClientInfo = New System.Windows.Forms.TabPage()
        Me.Label170 = New System.Windows.Forms.Label()
        Me.Label171 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.rbEmployee = New System.Windows.Forms.RadioButton()
        Me.rbClient = New System.Windows.Forms.RadioButton()
        Me.cboOutlet = New System.Windows.Forms.ComboBox()
        Me.cboSubgroup = New System.Windows.Forms.ComboBox()
        Me.Label169 = New System.Windows.Forms.Label()
        Me.cboRank = New System.Windows.Forms.ComboBox()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.Label164 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label163 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.cbofType = New System.Windows.Forms.ComboBox()
        Me.cbofCategory = New System.Windows.Forms.ComboBox()
        Me.Label107 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Cbomem_Status = New System.Windows.Forms.ComboBox()
        Me.Label135 = New System.Windows.Forms.Label()
        Me.grpPayrollStatus = New System.Windows.Forms.GroupBox()
        Me.rdoNonExempt = New System.Windows.Forms.RadioButton()
        Me.rdoExempt = New System.Windows.Forms.RadioButton()
        Me.btnContractRate = New System.Windows.Forms.Button()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.txtorgchart = New System.Windows.Forms.TextBox()
        Me.txtContractrate = New System.Windows.Forms.TextBox()
        Me.dtBoardApproval = New System.Windows.Forms.DateTimePicker()
        Me.txtCompanyChart = New System.Windows.Forms.TextBox()
        Me.Label150 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.cbofGroup = New System.Windows.Forms.ComboBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.txtEcola = New System.Windows.Forms.TextBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.cboClient = New System.Windows.Forms.ComboBox()
        Me.mem_WithdrawDate = New System.Windows.Forms.DateTimePicker()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.cbotaxcode = New System.Windows.Forms.ComboBox()
        Me.chkNo = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtbasicpay = New System.Windows.Forms.TextBox()
        Me.cbopayroll = New System.Windows.Forms.ComboBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.chkyes = New System.Windows.Forms.CheckBox()
        Me.cborate = New System.Windows.Forms.ComboBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.txtwithdrawal = New System.Windows.Forms.TextBox()
        Me.emp_Datehired = New System.Windows.Forms.DateTimePicker()
        Me.mem_MemberDate = New System.Windows.Forms.DateTimePicker()
        Me.Label111 = New System.Windows.Forms.Label()
        Me.Label108 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.cboemp_status = New System.Windows.Forms.ComboBox()
        Me.cboPositionLevel = New System.Windows.Forms.ComboBox()
        Me.Label114 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.emp_Ytenures = New System.Windows.Forms.TextBox()
        Me.Label100 = New System.Windows.Forms.Label()
        Me.cbotitledesignation = New System.Windows.Forms.ComboBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboEmp_type = New System.Windows.Forms.ComboBox()
        Me.StatutoryInfo = New System.Windows.Forms.TabPage()
        Me.Label106 = New System.Windows.Forms.Label()
        Me.PictureBox25 = New System.Windows.Forms.PictureBox()
        Me.txtCommonrefNo = New System.Windows.Forms.TextBox()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.txtPhilhealth = New System.Windows.Forms.TextBox()
        Me.txtHDMF = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.txtSSS = New System.Windows.Forms.TextBox()
        Me.txtTin = New System.Windows.Forms.TextBox()
        Me.AddInfo = New System.Windows.Forms.TabPage()
        Me.Label154 = New System.Windows.Forms.Label()
        Me.Label132 = New System.Windows.Forms.Label()
        Me.Label158 = New System.Windows.Forms.Label()
        Me.Label153 = New System.Windows.Forms.Label()
        Me.Label160 = New System.Windows.Forms.Label()
        Me.Label161 = New System.Windows.Forms.Label()
        Me.Label155 = New System.Windows.Forms.Label()
        Me.Label156 = New System.Windows.Forms.Label()
        Me.Label162 = New System.Windows.Forms.Label()
        Me.Label157 = New System.Windows.Forms.Label()
        Me.txtPermProvince = New System.Windows.Forms.TextBox()
        Me.txtPermCity = New System.Windows.Forms.TextBox()
        Me.txtPermSitio = New System.Windows.Forms.TextBox()
        Me.txtPermStreet = New System.Windows.Forms.TextBox()
        Me.txtPermBldg = New System.Windows.Forms.TextBox()
        Me.txtPrevProvince = New System.Windows.Forms.TextBox()
        Me.txtPrevCity = New System.Windows.Forms.TextBox()
        Me.txtPrevSitio = New System.Windows.Forms.TextBox()
        Me.txtPrevStreet = New System.Windows.Forms.TextBox()
        Me.txtPrevBldg = New System.Windows.Forms.TextBox()
        Me.Label151 = New System.Windows.Forms.Label()
        Me.txtPresProvince = New System.Windows.Forms.TextBox()
        Me.Label152 = New System.Windows.Forms.Label()
        Me.txtPresCity = New System.Windows.Forms.TextBox()
        Me.Label99 = New System.Windows.Forms.Label()
        Me.txtPresSitio = New System.Windows.Forms.TextBox()
        Me.Label97 = New System.Windows.Forms.Label()
        Me.txtPresStreet = New System.Windows.Forms.TextBox()
        Me.Label96 = New System.Windows.Forms.Label()
        Me.txtPresBldg = New System.Windows.Forms.TextBox()
        Me.cboStay3 = New System.Windows.Forms.ComboBox()
        Me.cboStay2 = New System.Windows.Forms.ComboBox()
        Me.cboStay1 = New System.Windows.Forms.ComboBox()
        Me.txtStay3 = New System.Windows.Forms.TextBox()
        Me.txtStay2 = New System.Windows.Forms.TextBox()
        Me.txtStay1 = New System.Windows.Forms.TextBox()
        Me.Label103 = New System.Windows.Forms.Label()
        Me.Label102 = New System.Windows.Forms.Label()
        Me.Label101 = New System.Windows.Forms.Label()
        Me.Label95 = New System.Windows.Forms.Label()
        Me.Label94 = New System.Windows.Forms.Label()
        Me.btnSearchPermAddress = New System.Windows.Forms.Button()
        Me.PictureBox22 = New System.Windows.Forms.PictureBox()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnSearchProvince = New System.Windows.Forms.Button()
        Me.btnSearchHome = New System.Windows.Forms.Button()
        Me.ContactInfo = New System.Windows.Forms.TabPage()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.PictureBox20 = New System.Windows.Forms.PictureBox()
        Me.txtFacebook2 = New System.Windows.Forms.TextBox()
        Me.txtEmailAddress = New System.Windows.Forms.TextBox()
        Me.txtLandline2 = New System.Windows.Forms.TextBox()
        Me.txtFacebook1 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtLandline = New System.Windows.Forms.TextBox()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.txtresidencephone2 = New System.Windows.Forms.TextBox()
        Me.txtresidencephone = New System.Windows.Forms.MaskedTextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtemailaddress2 = New System.Windows.Forms.TextBox()
        Me.Ownership = New System.Windows.Forms.TabPage()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.Label146 = New System.Windows.Forms.Label()
        Me.Label144 = New System.Windows.Forms.Label()
        Me.txtCar = New System.Windows.Forms.TextBox()
        Me.rbCNo = New System.Windows.Forms.RadioButton()
        Me.rbCYes = New System.Windows.Forms.RadioButton()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.rbMYes = New System.Windows.Forms.RadioButton()
        Me.Label143 = New System.Windows.Forms.Label()
        Me.Label145 = New System.Windows.Forms.Label()
        Me.txtMotor = New System.Windows.Forms.TextBox()
        Me.rbMNo = New System.Windows.Forms.RadioButton()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.rbRented = New System.Windows.Forms.RadioButton()
        Me.Label142 = New System.Windows.Forms.Label()
        Me.rbOwned = New System.Windows.Forms.RadioButton()
        Me.rbParents = New System.Windows.Forms.RadioButton()
        Me.Label140 = New System.Windows.Forms.Label()
        Me.PictureBox28 = New System.Windows.Forms.PictureBox()
        Me.Family = New System.Windows.Forms.TabPage()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.cboContactPerson = New System.Windows.Forms.ComboBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.btnUpdateSibling = New System.Windows.Forms.Button()
        Me.btnNewSibling = New System.Windows.Forms.Button()
        Me.lvlSibling = New System.Windows.Forms.ListView()
        Me.txtChildNo = New System.Windows.Forms.TextBox()
        Me.Label134 = New System.Windows.Forms.Label()
        Me.txtSpouseStay = New System.Windows.Forms.TextBox()
        Me.rbSpouseYears = New System.Windows.Forms.RadioButton()
        Me.rbSpouseMonth = New System.Windows.Forms.RadioButton()
        Me.Label131 = New System.Windows.Forms.Label()
        Me.txtSpouseAdd = New System.Windows.Forms.TextBox()
        Me.Label133 = New System.Windows.Forms.Label()
        Me.btnSpouseSearchAdd = New System.Windows.Forms.Button()
        Me.txtOccupation = New System.Windows.Forms.TextBox()
        Me.Label130 = New System.Windows.Forms.Label()
        Me.txtSpouseContact = New System.Windows.Forms.TextBox()
        Me.Label128 = New System.Windows.Forms.Label()
        Me.txtSpouseName = New System.Windows.Forms.TextBox()
        Me.Label129 = New System.Windows.Forms.Label()
        Me.txtMContact = New System.Windows.Forms.TextBox()
        Me.Label125 = New System.Windows.Forms.Label()
        Me.txtFContact = New System.Windows.Forms.TextBox()
        Me.Label124 = New System.Windows.Forms.Label()
        Me.btnMSearchAdd = New System.Windows.Forms.Button()
        Me.btnFSearchAdd = New System.Windows.Forms.Button()
        Me.txtMAdd = New System.Windows.Forms.TextBox()
        Me.Label120 = New System.Windows.Forms.Label()
        Me.txtFAdd = New System.Windows.Forms.TextBox()
        Me.Label119 = New System.Windows.Forms.Label()
        Me.txtMotherName = New System.Windows.Forms.TextBox()
        Me.Label116 = New System.Windows.Forms.Label()
        Me.txtFatherName = New System.Windows.Forms.TextBox()
        Me.Label115 = New System.Windows.Forms.Label()
        Me.Label113 = New System.Windows.Forms.Label()
        Me.PictureBox26 = New System.Windows.Forms.PictureBox()
        Me.Dependents = New System.Windows.Forms.TabPage()
        Me.LvlEmployeeDependent = New System.Windows.Forms.ListView()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.emp_pic = New System.Windows.Forms.PictureBox()
        Me.temp_marks = New System.Windows.Forms.TextBox()
        Me.txtitemno = New System.Windows.Forms.TextBox()
        Me.txtremarks = New System.Windows.Forms.TextBox()
        Me.temp_add3 = New System.Windows.Forms.TextBox()
        Me.txtparent_ID = New System.Windows.Forms.TextBox()
        Me.txtmonthreqular = New System.Windows.Forms.TextBox()
        Me.txtkeysection = New System.Windows.Forms.TextBox()
        Me.temp_height = New System.Windows.Forms.TextBox()
        Me.txtdescsection = New System.Windows.Forms.TextBox()
        Me.txtsalarygrade = New System.Windows.Forms.TextBox()
        Me.txtdescdepartment = New System.Windows.Forms.TextBox()
        Me.temp_add2 = New System.Windows.Forms.TextBox()
        Me.txtdescdivision = New System.Windows.Forms.TextBox()
        Me.txtweight = New System.Windows.Forms.TextBox()
        Me.txtkeydepartment = New System.Windows.Forms.TextBox()
        Me.txtdateregular = New System.Windows.Forms.TextBox()
        Me.txtkeydivision = New System.Windows.Forms.TextBox()
        Me.Dateregular = New System.Windows.Forms.DateTimePicker()
        Me.txtkeycompany = New System.Windows.Forms.TextBox()
        Me.emp_extphone = New System.Windows.Forms.MaskedTextBox()
        Me.cbosalarygrade = New System.Windows.Forms.ComboBox()
        Me.temp_citizen = New System.Windows.Forms.TextBox()
        Me.temp_designation = New System.Windows.Forms.TextBox()
        Me.txtType_employee = New System.Windows.Forms.TextBox()
        Me.txtfxkeypositionlevel = New System.Windows.Forms.TextBox()
        Me.txtfullname = New System.Windows.Forms.TextBox()
        Me.TXTRECID = New System.Windows.Forms.TextBox()
        Me.cbodept = New System.Windows.Forms.TextBox()
        Me.Dateresigned = New System.Windows.Forms.DateTimePicker()
        Me.TXTKEYEMPLOYEEID = New System.Windows.Forms.TextBox()
        Me.btnemp_editdepdts = New System.Windows.Forms.Button()
        Me.btndelete_dep = New System.Windows.Forms.Button()
        Me.btnadd_dep = New System.Windows.Forms.Button()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.Benefeciary = New System.Windows.Forms.TabPage()
        Me.BtnEditContact = New System.Windows.Forms.Button()
        Me.BtnDeleteContact = New System.Windows.Forms.Button()
        Me.btnaddContact = New System.Windows.Forms.Button()
        Me.lvlNearestRelatives = New System.Windows.Forms.ListView()
        Me.Label98 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.EducationalInfo = New System.Windows.Forms.TabPage()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.btnUpdateEI = New System.Windows.Forms.Button()
        Me.btnDeleteEI = New System.Windows.Forms.Button()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.btnNewEI = New System.Windows.Forms.Button()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.lvlEducInfo = New System.Windows.Forms.ListView()
        Me.EmploymentHistory = New System.Windows.Forms.TabPage()
        Me.lvlEmploymentHistory = New System.Windows.Forms.ListView()
        Me.btnUpdateEH = New System.Windows.Forms.Button()
        Me.btnDeleteEH = New System.Windows.Forms.Button()
        Me.btnNewEH = New System.Windows.Forms.Button()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.PictureBox21 = New System.Windows.Forms.PictureBox()
        Me.Trainings = New System.Windows.Forms.TabPage()
        Me.btnTDownload = New System.Windows.Forms.Button()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.btnUpdateT = New System.Windows.Forms.Button()
        Me.btnDeleteT = New System.Windows.Forms.Button()
        Me.btnNewT = New System.Windows.Forms.Button()
        Me.PictureBox12 = New System.Windows.Forms.PictureBox()
        Me.lvlTrainings = New System.Windows.Forms.ListView()
        Me.GovExamination = New System.Windows.Forms.TabPage()
        Me.btnUpdateGE = New System.Windows.Forms.Button()
        Me.btnDeleteGE = New System.Windows.Forms.Button()
        Me.btnNewGE = New System.Windows.Forms.Button()
        Me.lvlGovExam = New System.Windows.Forms.ListView()
        Me.Label91 = New System.Windows.Forms.Label()
        Me.PictureBox23 = New System.Windows.Forms.PictureBox()
        Me.Skills = New System.Windows.Forms.TabPage()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.btnUpdateSkills = New System.Windows.Forms.Button()
        Me.btnDeleteSkills = New System.Windows.Forms.Button()
        Me.btnNewSkills = New System.Windows.Forms.Button()
        Me.PictureBox13 = New System.Windows.Forms.PictureBox()
        Me.lvlSkills = New System.Windows.Forms.ListView()
        Me.Organizations = New System.Windows.Forms.TabPage()
        Me.btnUpdateOrg = New System.Windows.Forms.Button()
        Me.btnDeleteOrg = New System.Windows.Forms.Button()
        Me.btnNewOrg = New System.Windows.Forms.Button()
        Me.lvlOrg = New System.Windows.Forms.ListView()
        Me.Label104 = New System.Windows.Forms.Label()
        Me.PictureBox24 = New System.Windows.Forms.PictureBox()
        Me.Medical = New System.Windows.Forms.TabPage()
        Me.btnMedUpload = New System.Windows.Forms.Button()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.btnUpdateMed = New System.Windows.Forms.Button()
        Me.btnDeleteMed = New System.Windows.Forms.Button()
        Me.btnNewMed = New System.Windows.Forms.Button()
        Me.PictureBox17 = New System.Windows.Forms.PictureBox()
        Me.lvlMedical = New System.Windows.Forms.ListView()
        Me.BankInfo = New System.Windows.Forms.TabPage()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.btnUpdateBA = New System.Windows.Forms.Button()
        Me.btnDeleteBA = New System.Windows.Forms.Button()
        Me.btnNewBA = New System.Windows.Forms.Button()
        Me.lvlBankInfo = New System.Windows.Forms.ListView()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.SOIInfo = New System.Windows.Forms.TabPage()
        Me.btnUpdateSInc = New System.Windows.Forms.Button()
        Me.btnDeleteSInc = New System.Windows.Forms.Button()
        Me.btnsaveSInc = New System.Windows.Forms.Button()
        Me.lvlSourceIncome = New System.Windows.Forms.ListView()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.Discipline = New System.Windows.Forms.TabPage()
        Me.btnDDownload = New System.Windows.Forms.Button()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.btnUpdateDisc = New System.Windows.Forms.Button()
        Me.btnDeleteDisc = New System.Windows.Forms.Button()
        Me.btnNewDisc = New System.Windows.Forms.Button()
        Me.PictureBox16 = New System.Windows.Forms.PictureBox()
        Me.lvlDiscipline = New System.Windows.Forms.ListView()
        Me.PerfEval = New System.Windows.Forms.TabPage()
        Me.btnPEDownload = New System.Windows.Forms.Button()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.btnUpdatePE = New System.Windows.Forms.Button()
        Me.btnDeletePE = New System.Windows.Forms.Button()
        Me.btnNewPE = New System.Windows.Forms.Button()
        Me.PictureBox15 = New System.Windows.Forms.PictureBox()
        Me.lvlPerfEval = New System.Windows.Forms.ListView()
        Me.Legal = New System.Windows.Forms.TabPage()
        Me.btnLegalUpload = New System.Windows.Forms.Button()
        Me.Label136 = New System.Windows.Forms.Label()
        Me.btnUpdateLC = New System.Windows.Forms.Button()
        Me.btnDeleteLC = New System.Windows.Forms.Button()
        Me.btnNewLC = New System.Windows.Forms.Button()
        Me.PictureBox27 = New System.Windows.Forms.PictureBox()
        Me.lvlLegal = New System.Windows.Forms.ListView()
        Me.JobDesc = New System.Windows.Forms.TabPage()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.btnUpdateJD = New System.Windows.Forms.Button()
        Me.btnDeleteJD = New System.Windows.Forms.Button()
        Me.btnNewJD = New System.Windows.Forms.Button()
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.lvlJobDesc = New System.Windows.Forms.ListView()
        Me.Awards = New System.Windows.Forms.TabPage()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.btnUpdateAward = New System.Windows.Forms.Button()
        Me.btnDeleteAward = New System.Windows.Forms.Button()
        Me.btnNewAward = New System.Windows.Forms.Button()
        Me.PictureBox14 = New System.Windows.Forms.PictureBox()
        Me.lvlAwards = New System.Windows.Forms.ListView()
        Me.Requirement = New System.Windows.Forms.TabPage()
        Me.btnUpload = New System.Windows.Forms.Button()
        Me.Label167 = New System.Windows.Forms.Label()
        Me.btnAttach = New System.Windows.Forms.Button()
        Me.PictureBox35 = New System.Windows.Forms.PictureBox()
        Me.lvlAttachment = New System.Windows.Forms.ListView()
        Me.Contribution = New System.Windows.Forms.TabPage()
        Me.lvlContri = New System.Windows.Forms.ListView()
        Me.cboContribution = New System.Windows.Forms.ComboBox()
        Me.Label147 = New System.Windows.Forms.Label()
        Me.PictureBox30 = New System.Windows.Forms.PictureBox()
        Me.OtherDeductions = New System.Windows.Forms.TabPage()
        Me.lvlDeductions = New System.Windows.Forms.ListView()
        Me.Label148 = New System.Windows.Forms.Label()
        Me.PictureBox31 = New System.Windows.Forms.PictureBox()
        Me.MonthPay = New System.Windows.Forms.TabPage()
        Me.lvl13thMonth = New System.Windows.Forms.ListView()
        Me.Label149 = New System.Windows.Forms.Label()
        Me.PictureBox32 = New System.Windows.Forms.PictureBox()
        Me.InfoHistory = New System.Windows.Forms.TabPage()
        Me.Label141 = New System.Windows.Forms.Label()
        Me.PictureBox29 = New System.Windows.Forms.PictureBox()
        Me.dgvemail = New System.Windows.Forms.DataGridView()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.dgvphone = New System.Windows.Forms.DataGridView()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.dgvcivil = New System.Windows.Forms.DataGridView()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.dgvaddress = New System.Windows.Forms.DataGridView()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.dgvlastname = New System.Windows.Forms.DataGridView()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.ContractHistory = New System.Windows.Forms.TabPage()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.btnSetContract = New System.Windows.Forms.Button()
        Me.PictureBox33 = New System.Windows.Forms.PictureBox()
        Me.lvlContractHis = New System.Windows.Forms.ListView()
        Me.PayrollHis = New System.Windows.Forms.TabPage()
        Me.btnPayrollHistory = New System.Windows.Forms.Button()
        Me.PictureBox19 = New System.Windows.Forms.PictureBox()
        Me.lvlPayrollHistory = New System.Windows.Forms.ListView()
        Me.LeaveLedger = New System.Windows.Forms.TabPage()
        Me.cboLeave = New System.Windows.Forms.ComboBox()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.PictureBox18 = New System.Windows.Forms.PictureBox()
        Me.lvlLeave = New System.Windows.Forms.ListView()
        Me.others = New System.Windows.Forms.TabPage()
        Me.btnEditMobile = New System.Windows.Forms.Button()
        Me.gridMobileNos = New System.Windows.Forms.DataGridView()
        Me.txtEloadingLimit = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txtEloaderPIN = New System.Windows.Forms.TextBox()
        Me.chkShowPINChar = New System.Windows.Forms.CheckBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btnAddMobileNo = New System.Windows.Forms.Button()
        Me.chkIsEloader = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtMobileNo = New System.Windows.Forms.MaskedTextBox()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.btnrestricted = New System.Windows.Forms.Button()
        Me.txtContractPrice = New System.Windows.Forms.TextBox()
        Me.txtofficeadd = New System.Windows.Forms.TextBox()
        Me.chkBereaveYes = New System.Windows.Forms.CheckBox()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.chkBereaveNo = New System.Windows.Forms.CheckBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.cboPaycode = New System.Windows.Forms.ComboBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.mem_PaycontDate = New System.Windows.Forms.DateTimePicker()
        Me.Label110 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtpayrollcontriamount = New System.Windows.Forms.TextBox()
        Me.Label109 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label105 = New System.Windows.Forms.Label()
        Me.txtofficenumber = New System.Windows.Forms.TextBox()
        Me.emp_company = New System.Windows.Forms.TextBox()
        Me.txtlocalofficenumber = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtEmployeeNo = New System.Windows.Forms.TextBox()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.TextBox18 = New System.Windows.Forms.TextBox()
        Me.TextBox19 = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.MaskedTextBox5 = New System.Windows.Forms.MaskedTextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cboemp_gender = New System.Windows.Forms.ComboBox()
        Me.cboemp_civil = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtSuffix = New System.Windows.Forms.TextBox()
        Me.Label165 = New System.Windows.Forms.Label()
        Me.Label139 = New System.Windows.Forms.Label()
        Me.txtHeightInch = New System.Windows.Forms.TextBox()
        Me.Label138 = New System.Windows.Forms.Label()
        Me.Label137 = New System.Windows.Forms.Label()
        Me.txtW = New System.Windows.Forms.TextBox()
        Me.txtage1 = New System.Windows.Forms.TextBox()
        Me.txtHeightFt = New System.Windows.Forms.TextBox()
        Me.txtNickname = New System.Windows.Forms.TextBox()
        Me.txtpk_Employee = New System.Windows.Forms.TextBox()
        Me.temp_placebirth = New System.Windows.Forms.TextBox()
        Me.temp_midname = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.lblage = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.temp_fname = New System.Windows.Forms.TextBox()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.EMP_DATEbirth = New System.Windows.Forms.DateTimePicker()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.temp_lname = New System.Windows.Forms.TextBox()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtCompanyName = New System.Windows.Forms.TextBox()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.OpenFileDialog2 = New System.Windows.Forms.OpenFileDialog()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.btnPrevTab = New System.Windows.Forms.Button()
        Me.btnNextTab = New System.Windows.Forms.Button()
        Me.txtMemberID = New System.Windows.Forms.TextBox()
        Me.txtDepartment2 = New System.Windows.Forms.TextBox()
        Me.lblemployee_name = New System.Windows.Forms.Label()
        Me.label = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnMaxSignature = New System.Windows.Forms.Button()
        Me.btnMaxPhoto = New System.Windows.Forms.Button()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.btnBrowseSignature = New System.Windows.Forms.Button()
        Me.picEmpSignature = New System.Windows.Forms.PictureBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.btnBrowsePicture = New System.Windows.Forms.Button()
        Me.picEmpPhoto = New System.Windows.Forms.PictureBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.txtProject = New System.Windows.Forms.TextBox()
        Me.btnemp_close = New System.Windows.Forms.Button()
        Me.PanePanel2 = New WindowsApplication2.PanePanel()
        Me.btnemp_next = New System.Windows.Forms.Button()
        Me.btnemp_delete = New System.Windows.Forms.Button()
        Me.BTNSEARCH = New System.Windows.Forms.Button()
        Me.btnemp_edit = New System.Windows.Forms.Button()
        Me.btnemp_previous = New System.Windows.Forms.Button()
        Me.btnemp_add = New System.Windows.Forms.Button()
        Me.tabMember.SuspendLayout()
        Me.ClientInfo.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpPayrollStatus.SuspendLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatutoryInfo.SuspendLayout()
        CType(Me.PictureBox25, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.AddInfo.SuspendLayout()
        CType(Me.PictureBox22, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContactInfo.SuspendLayout()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Ownership.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.PictureBox28, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Family.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        CType(Me.PictureBox26, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Dependents.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.emp_pic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Benefeciary.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EducationalInfo.SuspendLayout()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EmploymentHistory.SuspendLayout()
        CType(Me.PictureBox21, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Trainings.SuspendLayout()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GovExamination.SuspendLayout()
        CType(Me.PictureBox23, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Skills.SuspendLayout()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Organizations.SuspendLayout()
        CType(Me.PictureBox24, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Medical.SuspendLayout()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BankInfo.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SOIInfo.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Discipline.SuspendLayout()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PerfEval.SuspendLayout()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Legal.SuspendLayout()
        CType(Me.PictureBox27, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.JobDesc.SuspendLayout()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Awards.SuspendLayout()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Requirement.SuspendLayout()
        CType(Me.PictureBox35, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Contribution.SuspendLayout()
        CType(Me.PictureBox30, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.OtherDeductions.SuspendLayout()
        CType(Me.PictureBox31, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MonthPay.SuspendLayout()
        CType(Me.PictureBox32, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.InfoHistory.SuspendLayout()
        CType(Me.PictureBox29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvemail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvphone, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvcivil, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvaddress, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvlastname, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContractHistory.SuspendLayout()
        CType(Me.PictureBox33, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PayrollHis.SuspendLayout()
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LeaveLedger.SuspendLayout()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.others.SuspendLayout()
        CType(Me.gridMobileNos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.picEmpSignature, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picEmpPhoto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.PanePanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabMember
        '
        Me.tabMember.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabMember.Controls.Add(Me.ClientInfo)
        Me.tabMember.Controls.Add(Me.StatutoryInfo)
        Me.tabMember.Controls.Add(Me.AddInfo)
        Me.tabMember.Controls.Add(Me.ContactInfo)
        Me.tabMember.Controls.Add(Me.Ownership)
        Me.tabMember.Controls.Add(Me.Family)
        Me.tabMember.Controls.Add(Me.Dependents)
        Me.tabMember.Controls.Add(Me.Benefeciary)
        Me.tabMember.Controls.Add(Me.EducationalInfo)
        Me.tabMember.Controls.Add(Me.EmploymentHistory)
        Me.tabMember.Controls.Add(Me.Trainings)
        Me.tabMember.Controls.Add(Me.GovExamination)
        Me.tabMember.Controls.Add(Me.Skills)
        Me.tabMember.Controls.Add(Me.Organizations)
        Me.tabMember.Controls.Add(Me.Medical)
        Me.tabMember.Controls.Add(Me.BankInfo)
        Me.tabMember.Controls.Add(Me.SOIInfo)
        Me.tabMember.Controls.Add(Me.Discipline)
        Me.tabMember.Controls.Add(Me.PerfEval)
        Me.tabMember.Controls.Add(Me.Legal)
        Me.tabMember.Controls.Add(Me.JobDesc)
        Me.tabMember.Controls.Add(Me.Awards)
        Me.tabMember.Controls.Add(Me.Requirement)
        Me.tabMember.Controls.Add(Me.Contribution)
        Me.tabMember.Controls.Add(Me.OtherDeductions)
        Me.tabMember.Controls.Add(Me.MonthPay)
        Me.tabMember.Controls.Add(Me.InfoHistory)
        Me.tabMember.Controls.Add(Me.ContractHistory)
        Me.tabMember.Controls.Add(Me.PayrollHis)
        Me.tabMember.Controls.Add(Me.LeaveLedger)
        Me.tabMember.Controls.Add(Me.others)
        Me.tabMember.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabMember.ItemSize = New System.Drawing.Size(120, 18)
        Me.tabMember.Location = New System.Drawing.Point(12, 259)
        Me.tabMember.Name = "tabMember"
        Me.tabMember.SelectedIndex = 0
        Me.tabMember.Size = New System.Drawing.Size(1011, 301)
        Me.tabMember.TabIndex = 190
        '
        'ClientInfo
        '
        Me.ClientInfo.BackColor = System.Drawing.SystemColors.Window
        Me.ClientInfo.Controls.Add(Me.txtProject)
        Me.ClientInfo.Controls.Add(Me.Label44)
        Me.ClientInfo.Controls.Add(Me.Label170)
        Me.ClientInfo.Controls.Add(Me.Label171)
        Me.ClientInfo.Controls.Add(Me.GroupBox4)
        Me.ClientInfo.Controls.Add(Me.cboOutlet)
        Me.ClientInfo.Controls.Add(Me.cboSubgroup)
        Me.ClientInfo.Controls.Add(Me.Label169)
        Me.ClientInfo.Controls.Add(Me.cboRank)
        Me.ClientInfo.Controls.Add(Me.Label86)
        Me.ClientInfo.Controls.Add(Me.Label88)
        Me.ClientInfo.Controls.Add(Me.Label164)
        Me.ClientInfo.Controls.Add(Me.Label60)
        Me.ClientInfo.Controls.Add(Me.Label163)
        Me.ClientInfo.Controls.Add(Me.Label53)
        Me.ClientInfo.Controls.Add(Me.cbofType)
        Me.ClientInfo.Controls.Add(Me.cbofCategory)
        Me.ClientInfo.Controls.Add(Me.Label107)
        Me.ClientInfo.Controls.Add(Me.PictureBox2)
        Me.ClientInfo.Controls.Add(Me.Label47)
        Me.ClientInfo.Controls.Add(Me.Cbomem_Status)
        Me.ClientInfo.Controls.Add(Me.Label135)
        Me.ClientInfo.Controls.Add(Me.grpPayrollStatus)
        Me.ClientInfo.Controls.Add(Me.btnContractRate)
        Me.ClientInfo.Controls.Add(Me.Label12)
        Me.ClientInfo.Controls.Add(Me.Label84)
        Me.ClientInfo.Controls.Add(Me.Label48)
        Me.ClientInfo.Controls.Add(Me.Label46)
        Me.ClientInfo.Controls.Add(Me.txtorgchart)
        Me.ClientInfo.Controls.Add(Me.txtContractrate)
        Me.ClientInfo.Controls.Add(Me.dtBoardApproval)
        Me.ClientInfo.Controls.Add(Me.txtCompanyChart)
        Me.ClientInfo.Controls.Add(Me.Label150)
        Me.ClientInfo.Controls.Add(Me.Label52)
        Me.ClientInfo.Controls.Add(Me.Label58)
        Me.ClientInfo.Controls.Add(Me.Label51)
        Me.ClientInfo.Controls.Add(Me.Label83)
        Me.ClientInfo.Controls.Add(Me.cbofGroup)
        Me.ClientInfo.Controls.Add(Me.Label50)
        Me.ClientInfo.Controls.Add(Me.txtEcola)
        Me.ClientInfo.Controls.Add(Me.Label45)
        Me.ClientInfo.Controls.Add(Me.cboClient)
        Me.ClientInfo.Controls.Add(Me.mem_WithdrawDate)
        Me.ClientInfo.Controls.Add(Me.Label63)
        Me.ClientInfo.Controls.Add(Me.cbotaxcode)
        Me.ClientInfo.Controls.Add(Me.chkNo)
        Me.ClientInfo.Controls.Add(Me.Label1)
        Me.ClientInfo.Controls.Add(Me.txtbasicpay)
        Me.ClientInfo.Controls.Add(Me.cbopayroll)
        Me.ClientInfo.Controls.Add(Me.Label59)
        Me.ClientInfo.Controls.Add(Me.Label62)
        Me.ClientInfo.Controls.Add(Me.chkyes)
        Me.ClientInfo.Controls.Add(Me.cborate)
        Me.ClientInfo.Controls.Add(Me.Label37)
        Me.ClientInfo.Controls.Add(Me.Label69)
        Me.ClientInfo.Controls.Add(Me.txtwithdrawal)
        Me.ClientInfo.Controls.Add(Me.emp_Datehired)
        Me.ClientInfo.Controls.Add(Me.mem_MemberDate)
        Me.ClientInfo.Controls.Add(Me.Label111)
        Me.ClientInfo.Controls.Add(Me.Label108)
        Me.ClientInfo.Controls.Add(Me.Label13)
        Me.ClientInfo.Controls.Add(Me.Label65)
        Me.ClientInfo.Controls.Add(Me.cboemp_status)
        Me.ClientInfo.Controls.Add(Me.cboPositionLevel)
        Me.ClientInfo.Controls.Add(Me.Label114)
        Me.ClientInfo.Controls.Add(Me.Label11)
        Me.ClientInfo.Controls.Add(Me.PictureBox8)
        Me.ClientInfo.Controls.Add(Me.emp_Ytenures)
        Me.ClientInfo.Controls.Add(Me.Label100)
        Me.ClientInfo.Controls.Add(Me.cbotitledesignation)
        Me.ClientInfo.Controls.Add(Me.Label18)
        Me.ClientInfo.Controls.Add(Me.Label2)
        Me.ClientInfo.Controls.Add(Me.cboEmp_type)
        Me.ClientInfo.Location = New System.Drawing.Point(4, 22)
        Me.ClientInfo.Name = "ClientInfo"
        Me.ClientInfo.Size = New System.Drawing.Size(1003, 275)
        Me.ClientInfo.TabIndex = 4
        Me.ClientInfo.Text = "Contract Info *"
        Me.ClientInfo.UseVisualStyleBackColor = True
        '
        'Label170
        '
        Me.Label170.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label170.AutoSize = True
        Me.Label170.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label170.ForeColor = System.Drawing.Color.Red
        Me.Label170.Location = New System.Drawing.Point(984, 26)
        Me.Label170.Name = "Label170"
        Me.Label170.Size = New System.Drawing.Size(15, 20)
        Me.Label170.TabIndex = 134
        Me.Label170.Text = "*"
        '
        'Label171
        '
        Me.Label171.AutoSize = True
        Me.Label171.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label171.ForeColor = System.Drawing.Color.Red
        Me.Label171.Location = New System.Drawing.Point(979, 78)
        Me.Label171.Name = "Label171"
        Me.Label171.Size = New System.Drawing.Size(15, 20)
        Me.Label171.TabIndex = 147
        Me.Label171.Text = "*"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.rbEmployee)
        Me.GroupBox4.Controls.Add(Me.rbClient)
        Me.GroupBox4.Enabled = False
        Me.GroupBox4.Location = New System.Drawing.Point(654, 229)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(267, 42)
        Me.GroupBox4.TabIndex = 119
        Me.GroupBox4.TabStop = False
        '
        'rbEmployee
        '
        Me.rbEmployee.AutoSize = True
        Me.rbEmployee.Location = New System.Drawing.Point(131, 14)
        Me.rbEmployee.Name = "rbEmployee"
        Me.rbEmployee.Size = New System.Drawing.Size(80, 19)
        Me.rbEmployee.TabIndex = 50
        Me.rbEmployee.Text = "Employee"
        Me.rbEmployee.UseVisualStyleBackColor = True
        '
        'rbClient
        '
        Me.rbClient.AutoSize = True
        Me.rbClient.Checked = True
        Me.rbClient.Location = New System.Drawing.Point(42, 14)
        Me.rbClient.Name = "rbClient"
        Me.rbClient.Size = New System.Drawing.Size(56, 19)
        Me.rbClient.TabIndex = 49
        Me.rbClient.TabStop = True
        Me.rbClient.Text = "Client"
        Me.rbClient.UseVisualStyleBackColor = True
        '
        'cboOutlet
        '
        Me.cboOutlet.AutoCompleteCustomSource.AddRange(New String() {"RANK AND FILE", "MANAGERIAL", "SUPERVISOR", "EXECUTIVE"})
        Me.cboOutlet.BackColor = System.Drawing.Color.White
        Me.cboOutlet.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOutlet.FormattingEnabled = True
        Me.cboOutlet.Location = New System.Drawing.Point(450, 202)
        Me.cboOutlet.Name = "cboOutlet"
        Me.cboOutlet.Size = New System.Drawing.Size(153, 23)
        Me.cboOutlet.TabIndex = 34
        '
        'cboSubgroup
        '
        Me.cboSubgroup.Enabled = False
        Me.cboSubgroup.FormattingEnabled = True
        Me.cboSubgroup.Location = New System.Drawing.Point(343, 50)
        Me.cboSubgroup.Name = "cboSubgroup"
        Me.cboSubgroup.Size = New System.Drawing.Size(167, 23)
        Me.cboSubgroup.TabIndex = 29
        '
        'Label169
        '
        Me.Label169.AutoSize = True
        Me.Label169.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label169.ForeColor = System.Drawing.Color.Red
        Me.Label169.Location = New System.Drawing.Point(308, 204)
        Me.Label169.Name = "Label169"
        Me.Label169.Size = New System.Drawing.Size(15, 20)
        Me.Label169.TabIndex = 136
        Me.Label169.Text = "*"
        '
        'cboRank
        '
        Me.cboRank.Enabled = False
        Me.cboRank.FormattingEnabled = True
        Me.cboRank.Location = New System.Drawing.Point(739, 155)
        Me.cboRank.Name = "cboRank"
        Me.cboRank.Size = New System.Drawing.Size(109, 23)
        Me.cboRank.TabIndex = 32
        '
        'Label86
        '
        Me.Label86.AutoSize = True
        Me.Label86.BackColor = System.Drawing.Color.Transparent
        Me.Label86.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label86.Location = New System.Drawing.Point(651, 159)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(37, 15)
        Me.Label86.TabIndex = 76
        Me.Label86.Text = "Rank:"
        '
        'Label88
        '
        Me.Label88.AutoSize = True
        Me.Label88.BackColor = System.Drawing.Color.Transparent
        Me.Label88.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label88.Location = New System.Drawing.Point(356, 207)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(47, 15)
        Me.Label88.TabIndex = 122
        Me.Label88.Text = "Outlet:"
        '
        'Label164
        '
        Me.Label164.AutoSize = True
        Me.Label164.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label164.ForeColor = System.Drawing.Color.Red
        Me.Label164.Location = New System.Drawing.Point(569, 79)
        Me.Label164.Name = "Label164"
        Me.Label164.Size = New System.Drawing.Size(15, 20)
        Me.Label164.TabIndex = 135
        Me.Label164.Text = "*"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.BackColor = System.Drawing.Color.Transparent
        Me.Label60.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label60.Location = New System.Drawing.Point(275, 55)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(62, 14)
        Me.Label60.TabIndex = 118
        Me.Label60.Text = "Sub-Group:"
        '
        'Label163
        '
        Me.Label163.AutoSize = True
        Me.Label163.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label163.ForeColor = System.Drawing.Color.Red
        Me.Label163.Location = New System.Drawing.Point(308, 153)
        Me.Label163.Name = "Label163"
        Me.Label163.Size = New System.Drawing.Size(15, 20)
        Me.Label163.TabIndex = 134
        Me.Label163.Text = "*"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.BackColor = System.Drawing.Color.Transparent
        Me.Label53.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label53.ForeColor = System.Drawing.Color.Red
        Me.Label53.Location = New System.Drawing.Point(604, 103)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(15, 20)
        Me.Label53.TabIndex = 115
        Me.Label53.Text = "*"
        '
        'cbofType
        '
        Me.cbofType.Enabled = False
        Me.cbofType.FormattingEnabled = True
        Me.cbofType.Location = New System.Drawing.Point(809, 50)
        Me.cbofType.Name = "cbofType"
        Me.cbofType.Size = New System.Drawing.Size(167, 23)
        Me.cbofType.TabIndex = 31
        '
        'cbofCategory
        '
        Me.cbofCategory.Enabled = False
        Me.cbofCategory.FormattingEnabled = True
        Me.cbofCategory.Location = New System.Drawing.Point(581, 50)
        Me.cbofCategory.Name = "cbofCategory"
        Me.cbofCategory.Size = New System.Drawing.Size(167, 23)
        Me.cbofCategory.TabIndex = 30
        '
        'Label107
        '
        Me.Label107.AutoSize = True
        Me.Label107.BackColor = System.Drawing.Color.Transparent
        Me.Label107.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label107.Location = New System.Drawing.Point(355, 109)
        Me.Label107.Name = "Label107"
        Me.Label107.Size = New System.Drawing.Size(80, 15)
        Me.Label107.TabIndex = 102
        Me.Label107.Text = "Client Status:"
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.PictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(958, 26)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(25, 21)
        Me.PictureBox2.TabIndex = 45
        Me.PictureBox2.TabStop = False
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.BackColor = System.Drawing.Color.Transparent
        Me.Label47.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.Location = New System.Drawing.Point(766, 55)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(37, 14)
        Me.Label47.TabIndex = 111
        Me.Label47.Text = "Type :"
        '
        'Cbomem_Status
        '
        Me.Cbomem_Status.FormattingEnabled = True
        Me.Cbomem_Status.Location = New System.Drawing.Point(450, 102)
        Me.Cbomem_Status.Name = "Cbomem_Status"
        Me.Cbomem_Status.Size = New System.Drawing.Size(153, 23)
        Me.Cbomem_Status.TabIndex = 33
        '
        'Label135
        '
        Me.Label135.AutoSize = True
        Me.Label135.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label135.ForeColor = System.Drawing.Color.Red
        Me.Label135.Location = New System.Drawing.Point(308, 180)
        Me.Label135.Name = "Label135"
        Me.Label135.Size = New System.Drawing.Size(15, 20)
        Me.Label135.TabIndex = 133
        Me.Label135.Text = "*"
        '
        'grpPayrollStatus
        '
        Me.grpPayrollStatus.Controls.Add(Me.rdoNonExempt)
        Me.grpPayrollStatus.Controls.Add(Me.rdoExempt)
        Me.grpPayrollStatus.Location = New System.Drawing.Point(654, 180)
        Me.grpPayrollStatus.Name = "grpPayrollStatus"
        Me.grpPayrollStatus.Size = New System.Drawing.Size(267, 49)
        Me.grpPayrollStatus.TabIndex = 118
        Me.grpPayrollStatus.TabStop = False
        Me.grpPayrollStatus.Text = "Payroll Status"
        '
        'rdoNonExempt
        '
        Me.rdoNonExempt.AutoSize = True
        Me.rdoNonExempt.Location = New System.Drawing.Point(131, 19)
        Me.rdoNonExempt.Name = "rdoNonExempt"
        Me.rdoNonExempt.Size = New System.Drawing.Size(94, 19)
        Me.rdoNonExempt.TabIndex = 48
        Me.rdoNonExempt.Text = "Non-Exempt"
        Me.rdoNonExempt.UseVisualStyleBackColor = True
        '
        'rdoExempt
        '
        Me.rdoExempt.AutoSize = True
        Me.rdoExempt.Checked = True
        Me.rdoExempt.Location = New System.Drawing.Point(42, 19)
        Me.rdoExempt.Name = "rdoExempt"
        Me.rdoExempt.Size = New System.Drawing.Size(67, 19)
        Me.rdoExempt.TabIndex = 47
        Me.rdoExempt.TabStop = True
        Me.rdoExempt.Text = "Exempt"
        Me.rdoExempt.UseVisualStyleBackColor = True
        '
        'btnContractRate
        '
        Me.btnContractRate.Enabled = False
        Me.btnContractRate.Location = New System.Drawing.Point(932, 78)
        Me.btnContractRate.Name = "btnContractRate"
        Me.btnContractRate.Size = New System.Drawing.Size(45, 21)
        Me.btnContractRate.TabIndex = 142
        Me.btnContractRate.Text = "....."
        Me.btnContractRate.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(7, 28)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(80, 15)
        Me.Label12.TabIndex = 21
        Me.Label12.Text = "Organization:"
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label84.ForeColor = System.Drawing.Color.Red
        Me.Label84.Location = New System.Drawing.Point(308, 106)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(15, 20)
        Me.Label84.TabIndex = 131
        Me.Label84.Text = "*"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.ForeColor = System.Drawing.Color.Red
        Me.Label48.Location = New System.Drawing.Point(308, 128)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(15, 20)
        Me.Label48.TabIndex = 130
        Me.Label48.Text = "*"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.BackColor = System.Drawing.Color.Transparent
        Me.Label46.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.Location = New System.Drawing.Point(526, 53)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(57, 14)
        Me.Label46.TabIndex = 110
        Me.Label46.Text = "Category :"
        '
        'txtorgchart
        '
        Me.txtorgchart.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtorgchart.BackColor = System.Drawing.SystemColors.Window
        Me.txtorgchart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtorgchart.Enabled = False
        Me.txtorgchart.Location = New System.Drawing.Point(93, 26)
        Me.txtorgchart.Name = "txtorgchart"
        Me.txtorgchart.Size = New System.Drawing.Size(859, 21)
        Me.txtorgchart.TabIndex = 27
        '
        'txtContractrate
        '
        Me.txtContractrate.Enabled = False
        Me.txtContractrate.Location = New System.Drawing.Point(739, 79)
        Me.txtContractrate.Name = "txtContractrate"
        Me.txtContractrate.ReadOnly = True
        Me.txtContractrate.Size = New System.Drawing.Size(187, 21)
        Me.txtContractrate.TabIndex = 55
        '
        'dtBoardApproval
        '
        Me.dtBoardApproval.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtBoardApproval.Location = New System.Drawing.Point(450, 227)
        Me.dtBoardApproval.Name = "dtBoardApproval"
        Me.dtBoardApproval.Size = New System.Drawing.Size(117, 21)
        Me.dtBoardApproval.TabIndex = 45
        '
        'txtCompanyChart
        '
        Me.txtCompanyChart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCompanyChart.Enabled = False
        Me.txtCompanyChart.Location = New System.Drawing.Point(93, 26)
        Me.txtCompanyChart.Name = "txtCompanyChart"
        Me.txtCompanyChart.Size = New System.Drawing.Size(804, 21)
        Me.txtCompanyChart.TabIndex = 8
        Me.txtCompanyChart.Visible = False
        '
        'Label150
        '
        Me.Label150.AutoSize = True
        Me.Label150.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label150.Location = New System.Drawing.Point(651, 82)
        Me.Label150.Name = "Label150"
        Me.Label150.Size = New System.Drawing.Size(82, 15)
        Me.Label150.TabIndex = 126
        Me.Label150.Text = "Contract Rate"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label52.ForeColor = System.Drawing.Color.Red
        Me.Label52.Location = New System.Drawing.Point(604, 179)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(15, 20)
        Me.Label52.TabIndex = 117
        Me.Label52.Text = "*"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label58.Location = New System.Drawing.Point(355, 229)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(95, 15)
        Me.Label58.TabIndex = 128
        Me.Label58.Text = "Board Approval:"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label51.ForeColor = System.Drawing.Color.Red
        Me.Label51.Location = New System.Drawing.Point(604, 154)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(15, 20)
        Me.Label51.TabIndex = 116
        Me.Label51.Text = "*"
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label83.Location = New System.Drawing.Point(22, 180)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(130, 15)
        Me.Label83.TabIndex = 47
        Me.Label83.Text = "Client Representative:"
        '
        'cbofGroup
        '
        Me.cbofGroup.Enabled = False
        Me.cbofGroup.FormattingEnabled = True
        Me.cbofGroup.Location = New System.Drawing.Point(93, 50)
        Me.cbofGroup.Name = "cbofGroup"
        Me.cbofGroup.Size = New System.Drawing.Size(167, 23)
        Me.cbofGroup.TabIndex = 28
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.ForeColor = System.Drawing.Color.Red
        Me.Label50.Location = New System.Drawing.Point(604, 129)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(15, 20)
        Me.Label50.TabIndex = 115
        Me.Label50.Text = "*"
        '
        'txtEcola
        '
        Me.txtEcola.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEcola.Location = New System.Drawing.Point(739, 131)
        Me.txtEcola.MaxLength = 255
        Me.txtEcola.Name = "txtEcola"
        Me.txtEcola.ReadOnly = True
        Me.txtEcola.Size = New System.Drawing.Size(85, 21)
        Me.txtEcola.TabIndex = 57
        Me.txtEcola.Text = "0.00"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.BackColor = System.Drawing.Color.Transparent
        Me.Label45.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.Location = New System.Drawing.Point(7, 55)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(43, 14)
        Me.Label45.TabIndex = 109
        Me.Label45.Text = "Group :"
        '
        'cboClient
        '
        Me.cboClient.AutoCompleteCustomSource.AddRange(New String() {"RANK AND FILE", "MANAGERIAL", "SUPERVISOR", "EXECUTIVE"})
        Me.cboClient.BackColor = System.Drawing.Color.White
        Me.cboClient.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClient.FormattingEnabled = True
        Me.cboClient.Location = New System.Drawing.Point(155, 177)
        Me.cboClient.Name = "cboClient"
        Me.cboClient.Size = New System.Drawing.Size(152, 23)
        Me.cboClient.TabIndex = 40
        '
        'mem_WithdrawDate
        '
        Me.mem_WithdrawDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.mem_WithdrawDate.Location = New System.Drawing.Point(252, 250)
        Me.mem_WithdrawDate.Name = "mem_WithdrawDate"
        Me.mem_WithdrawDate.Size = New System.Drawing.Size(20, 21)
        Me.mem_WithdrawDate.TabIndex = 121
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label63.Location = New System.Drawing.Point(651, 135)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(47, 15)
        Me.Label63.TabIndex = 119
        Me.Label63.Text = "ECOLA:"
        '
        'cbotaxcode
        '
        Me.cbotaxcode.Enabled = False
        Me.cbotaxcode.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbotaxcode.FormattingEnabled = True
        Me.cbotaxcode.Location = New System.Drawing.Point(450, 177)
        Me.cbotaxcode.Name = "cbotaxcode"
        Me.cbotaxcode.Size = New System.Drawing.Size(153, 23)
        Me.cbotaxcode.TabIndex = 53
        '
        'chkNo
        '
        Me.chkNo.AutoSize = True
        Me.chkNo.Location = New System.Drawing.Point(216, 81)
        Me.chkNo.Name = "chkNo"
        Me.chkNo.Size = New System.Drawing.Size(42, 19)
        Me.chkNo.TabIndex = 36
        Me.chkNo.Text = "No"
        Me.chkNo.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(356, 181)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 15)
        Me.Label1.TabIndex = 108
        Me.Label1.Text = "Tax Code:"
        '
        'txtbasicpay
        '
        Me.txtbasicpay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtbasicpay.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbasicpay.Location = New System.Drawing.Point(739, 105)
        Me.txtbasicpay.Name = "txtbasicpay"
        Me.txtbasicpay.ReadOnly = True
        Me.txtbasicpay.Size = New System.Drawing.Size(85, 21)
        Me.txtbasicpay.TabIndex = 56
        Me.txtbasicpay.Text = "0.00"
        '
        'cbopayroll
        '
        Me.cbopayroll.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbopayroll.FormattingEnabled = True
        Me.cbopayroll.Location = New System.Drawing.Point(450, 127)
        Me.cbopayroll.Name = "cbopayroll"
        Me.cbopayroll.Size = New System.Drawing.Size(153, 23)
        Me.cbopayroll.TabIndex = 51
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.Location = New System.Drawing.Point(651, 109)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(59, 15)
        Me.Label59.TabIndex = 12
        Me.Label59.Text = "Basic Pay:"
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label62.Location = New System.Drawing.Point(356, 134)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(79, 15)
        Me.Label62.TabIndex = 15
        Me.Label62.Text = "Payroll Code:"
        '
        'chkyes
        '
        Me.chkyes.AutoSize = True
        Me.chkyes.Location = New System.Drawing.Point(157, 81)
        Me.chkyes.Name = "chkyes"
        Me.chkyes.Size = New System.Drawing.Size(46, 19)
        Me.chkyes.TabIndex = 35
        Me.chkyes.Text = "Yes"
        Me.chkyes.UseVisualStyleBackColor = True
        '
        'cborate
        '
        Me.cborate.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cborate.FormattingEnabled = True
        Me.cborate.Location = New System.Drawing.Point(450, 152)
        Me.cborate.Name = "cborate"
        Me.cborate.Size = New System.Drawing.Size(153, 23)
        Me.cborate.TabIndex = 52
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.Location = New System.Drawing.Point(22, 83)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(65, 15)
        Me.Label37.TabIndex = 115
        Me.Label37.Text = "Employed:"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label69.Location = New System.Drawing.Point(356, 155)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(65, 15)
        Me.Label69.TabIndex = 34
        Me.Label69.Text = "Rate Type:"
        '
        'txtwithdrawal
        '
        Me.txtwithdrawal.Location = New System.Drawing.Point(154, 250)
        Me.txtwithdrawal.Name = "txtwithdrawal"
        Me.txtwithdrawal.Size = New System.Drawing.Size(99, 21)
        Me.txtwithdrawal.TabIndex = 43
        '
        'emp_Datehired
        '
        Me.emp_Datehired.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.emp_Datehired.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.emp_Datehired.Location = New System.Drawing.Point(451, 79)
        Me.emp_Datehired.Name = "emp_Datehired"
        Me.emp_Datehired.Size = New System.Drawing.Size(117, 21)
        Me.emp_Datehired.TabIndex = 44
        Me.emp_Datehired.Value = New Date(2006, 5, 3, 0, 0, 0, 0)
        '
        'mem_MemberDate
        '
        Me.mem_MemberDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.mem_MemberDate.Location = New System.Drawing.Point(155, 227)
        Me.mem_MemberDate.Name = "mem_MemberDate"
        Me.mem_MemberDate.Size = New System.Drawing.Size(117, 21)
        Me.mem_MemberDate.TabIndex = 41
        '
        'Label111
        '
        Me.Label111.AutoSize = True
        Me.Label111.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label111.Location = New System.Drawing.Point(22, 254)
        Me.Label111.Name = "Label111"
        Me.Label111.Size = New System.Drawing.Size(104, 15)
        Me.Label111.TabIndex = 109
        Me.Label111.Text = "Withdrawal Date:"
        '
        'Label108
        '
        Me.Label108.AutoSize = True
        Me.Label108.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label108.Location = New System.Drawing.Point(22, 229)
        Me.Label108.Name = "Label108"
        Me.Label108.Size = New System.Drawing.Size(110, 15)
        Me.Label108.TabIndex = 102
        Me.Label108.Text = "Membership Date:"
        Me.Label108.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(356, 83)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(70, 15)
        Me.Label13.TabIndex = 22
        Me.Label13.Text = "Date Hired:"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label65.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label65.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label65.Location = New System.Drawing.Point(6, 1)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(137, 18)
        Me.Label65.TabIndex = 9
        Me.Label65.Text = "Contract Information"
        '
        'cboemp_status
        '
        Me.cboemp_status.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboemp_status.FormattingEnabled = True
        Me.cboemp_status.Location = New System.Drawing.Point(155, 202)
        Me.cboemp_status.Name = "cboemp_status"
        Me.cboemp_status.Size = New System.Drawing.Size(152, 23)
        Me.cboemp_status.TabIndex = 42
        '
        'cboPositionLevel
        '
        Me.cboPositionLevel.FormattingEnabled = True
        Me.cboPositionLevel.Location = New System.Drawing.Point(155, 152)
        Me.cboPositionLevel.Name = "cboPositionLevel"
        Me.cboPositionLevel.Size = New System.Drawing.Size(152, 23)
        Me.cboPositionLevel.TabIndex = 39
        '
        'Label114
        '
        Me.Label114.AutoSize = True
        Me.Label114.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label114.Location = New System.Drawing.Point(22, 156)
        Me.Label114.Name = "Label114"
        Me.Label114.Size = New System.Drawing.Size(87, 15)
        Me.Label114.TabIndex = 49
        Me.Label114.Text = "Position Level:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(21, 207)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(125, 15)
        Me.Label11.TabIndex = 20
        Me.Label11.Text = "Employement Status:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'PictureBox8
        '
        Me.PictureBox8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox8.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox8.Location = New System.Drawing.Point(1, 0)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(1083, 22)
        Me.PictureBox8.TabIndex = 119
        Me.PictureBox8.TabStop = False
        '
        'emp_Ytenures
        '
        Me.emp_Ytenures.Location = New System.Drawing.Point(450, 250)
        Me.emp_Ytenures.Name = "emp_Ytenures"
        Me.emp_Ytenures.ReadOnly = True
        Me.emp_Ytenures.Size = New System.Drawing.Size(117, 21)
        Me.emp_Ytenures.TabIndex = 41
        Me.emp_Ytenures.Text = "46"
        '
        'Label100
        '
        Me.Label100.AutoSize = True
        Me.Label100.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label100.Location = New System.Drawing.Point(355, 254)
        Me.Label100.Name = "Label100"
        Me.Label100.Size = New System.Drawing.Size(95, 15)
        Me.Label100.TabIndex = 45
        Me.Label100.Text = "Years of Tenure:"
        '
        'cbotitledesignation
        '
        Me.cbotitledesignation.FormattingEnabled = True
        Me.cbotitledesignation.Location = New System.Drawing.Point(155, 126)
        Me.cbotitledesignation.Name = "cbotitledesignation"
        Me.cbotitledesignation.Size = New System.Drawing.Size(152, 23)
        Me.cbotitledesignation.TabIndex = 38
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(22, 130)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(82, 15)
        Me.Label18.TabIndex = 27
        Me.Label18.Text = "Position Title:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(22, 109)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 15)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Position Type:"
        '
        'cboEmp_type
        '
        Me.cboEmp_type.AutoCompleteCustomSource.AddRange(New String() {"RANK AND FILE", "MANAGERIAL", "SUPERVISOR", "EXECUTIVE"})
        Me.cboEmp_type.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmp_type.FormattingEnabled = True
        Me.cboEmp_type.Location = New System.Drawing.Point(155, 101)
        Me.cboEmp_type.Name = "cboEmp_type"
        Me.cboEmp_type.Size = New System.Drawing.Size(152, 23)
        Me.cboEmp_type.TabIndex = 37
        '
        'StatutoryInfo
        '
        Me.StatutoryInfo.BackColor = System.Drawing.Color.White
        Me.StatutoryInfo.Controls.Add(Me.Label106)
        Me.StatutoryInfo.Controls.Add(Me.PictureBox25)
        Me.StatutoryInfo.Controls.Add(Me.txtCommonrefNo)
        Me.StatutoryInfo.Controls.Add(Me.Label66)
        Me.StatutoryInfo.Controls.Add(Me.txtPhilhealth)
        Me.StatutoryInfo.Controls.Add(Me.txtHDMF)
        Me.StatutoryInfo.Controls.Add(Me.Label54)
        Me.StatutoryInfo.Controls.Add(Me.Label55)
        Me.StatutoryInfo.Controls.Add(Me.Label56)
        Me.StatutoryInfo.Controls.Add(Me.Label57)
        Me.StatutoryInfo.Controls.Add(Me.txtSSS)
        Me.StatutoryInfo.Controls.Add(Me.txtTin)
        Me.StatutoryInfo.Location = New System.Drawing.Point(4, 22)
        Me.StatutoryInfo.Name = "StatutoryInfo"
        Me.StatutoryInfo.Size = New System.Drawing.Size(1003, 275)
        Me.StatutoryInfo.TabIndex = 35
        Me.StatutoryInfo.Text = "Statutory"
        '
        'Label106
        '
        Me.Label106.AutoSize = True
        Me.Label106.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label106.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label106.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label106.Location = New System.Drawing.Point(3, 2)
        Me.Label106.Name = "Label106"
        Me.Label106.Size = New System.Drawing.Size(142, 18)
        Me.Label106.TabIndex = 131
        Me.Label106.Text = "Statutory Information"
        '
        'PictureBox25
        '
        Me.PictureBox25.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox25.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox25.Location = New System.Drawing.Point(-1, 0)
        Me.PictureBox25.Name = "PictureBox25"
        Me.PictureBox25.Size = New System.Drawing.Size(1080, 22)
        Me.PictureBox25.TabIndex = 130
        Me.PictureBox25.TabStop = False
        '
        'txtCommonrefNo
        '
        Me.txtCommonrefNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCommonrefNo.Location = New System.Drawing.Point(563, 59)
        Me.txtCommonrefNo.Name = "txtCommonrefNo"
        Me.txtCommonrefNo.Size = New System.Drawing.Size(178, 21)
        Me.txtCommonrefNo.TabIndex = 60
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label66.Location = New System.Drawing.Point(533, 42)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(142, 15)
        Me.Label66.TabIndex = 128
        Me.Label66.Text = "Common Reference No.:"
        '
        'txtPhilhealth
        '
        Me.txtPhilhealth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPhilhealth.Location = New System.Drawing.Point(311, 106)
        Me.txtPhilhealth.Name = "txtPhilhealth"
        Me.txtPhilhealth.Size = New System.Drawing.Size(187, 21)
        Me.txtPhilhealth.TabIndex = 62
        '
        'txtHDMF
        '
        Me.txtHDMF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtHDMF.Location = New System.Drawing.Point(45, 106)
        Me.txtHDMF.Name = "txtHDMF"
        Me.txtHDMF.Size = New System.Drawing.Size(187, 21)
        Me.txtHDMF.TabIndex = 61
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label54.Location = New System.Drawing.Point(277, 42)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(125, 15)
        Me.Label54.TabIndex = 120
        Me.Label54.Text = "Tax Identification No.:"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label55.Location = New System.Drawing.Point(14, 88)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(114, 15)
        Me.Label55.TabIndex = 121
        Me.Label55.Text = "Pag-Ibig/HDMF No.:"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label56.Location = New System.Drawing.Point(277, 88)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(89, 15)
        Me.Label56.TabIndex = 122
        Me.Label56.Text = "PhilHealth No.:"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label57.Location = New System.Drawing.Point(14, 42)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(51, 15)
        Me.Label57.TabIndex = 123
        Me.Label57.Text = "SSS No.:"
        '
        'txtSSS
        '
        Me.txtSSS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSSS.Location = New System.Drawing.Point(45, 60)
        Me.txtSSS.Name = "txtSSS"
        Me.txtSSS.Size = New System.Drawing.Size(187, 21)
        Me.txtSSS.TabIndex = 58
        '
        'txtTin
        '
        Me.txtTin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTin.Location = New System.Drawing.Point(311, 60)
        Me.txtTin.Name = "txtTin"
        Me.txtTin.Size = New System.Drawing.Size(187, 21)
        Me.txtTin.TabIndex = 59
        '
        'AddInfo
        '
        Me.AddInfo.BackColor = System.Drawing.Color.White
        Me.AddInfo.Controls.Add(Me.Label154)
        Me.AddInfo.Controls.Add(Me.Label132)
        Me.AddInfo.Controls.Add(Me.Label158)
        Me.AddInfo.Controls.Add(Me.Label153)
        Me.AddInfo.Controls.Add(Me.Label160)
        Me.AddInfo.Controls.Add(Me.Label161)
        Me.AddInfo.Controls.Add(Me.Label155)
        Me.AddInfo.Controls.Add(Me.Label156)
        Me.AddInfo.Controls.Add(Me.Label162)
        Me.AddInfo.Controls.Add(Me.Label157)
        Me.AddInfo.Controls.Add(Me.txtPermProvince)
        Me.AddInfo.Controls.Add(Me.txtPermCity)
        Me.AddInfo.Controls.Add(Me.txtPermSitio)
        Me.AddInfo.Controls.Add(Me.txtPermStreet)
        Me.AddInfo.Controls.Add(Me.txtPermBldg)
        Me.AddInfo.Controls.Add(Me.txtPrevProvince)
        Me.AddInfo.Controls.Add(Me.txtPrevCity)
        Me.AddInfo.Controls.Add(Me.txtPrevSitio)
        Me.AddInfo.Controls.Add(Me.txtPrevStreet)
        Me.AddInfo.Controls.Add(Me.txtPrevBldg)
        Me.AddInfo.Controls.Add(Me.Label151)
        Me.AddInfo.Controls.Add(Me.txtPresProvince)
        Me.AddInfo.Controls.Add(Me.Label152)
        Me.AddInfo.Controls.Add(Me.txtPresCity)
        Me.AddInfo.Controls.Add(Me.Label99)
        Me.AddInfo.Controls.Add(Me.txtPresSitio)
        Me.AddInfo.Controls.Add(Me.Label97)
        Me.AddInfo.Controls.Add(Me.txtPresStreet)
        Me.AddInfo.Controls.Add(Me.Label96)
        Me.AddInfo.Controls.Add(Me.txtPresBldg)
        Me.AddInfo.Controls.Add(Me.cboStay3)
        Me.AddInfo.Controls.Add(Me.cboStay2)
        Me.AddInfo.Controls.Add(Me.cboStay1)
        Me.AddInfo.Controls.Add(Me.txtStay3)
        Me.AddInfo.Controls.Add(Me.txtStay2)
        Me.AddInfo.Controls.Add(Me.txtStay1)
        Me.AddInfo.Controls.Add(Me.Label103)
        Me.AddInfo.Controls.Add(Me.Label102)
        Me.AddInfo.Controls.Add(Me.Label101)
        Me.AddInfo.Controls.Add(Me.Label95)
        Me.AddInfo.Controls.Add(Me.Label94)
        Me.AddInfo.Controls.Add(Me.btnSearchPermAddress)
        Me.AddInfo.Controls.Add(Me.PictureBox22)
        Me.AddInfo.Controls.Add(Me.Label85)
        Me.AddInfo.Controls.Add(Me.Label10)
        Me.AddInfo.Controls.Add(Me.btnSearchProvince)
        Me.AddInfo.Controls.Add(Me.btnSearchHome)
        Me.AddInfo.ForeColor = System.Drawing.Color.Black
        Me.AddInfo.Location = New System.Drawing.Point(4, 22)
        Me.AddInfo.Name = "AddInfo"
        Me.AddInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.AddInfo.Size = New System.Drawing.Size(1003, 275)
        Me.AddInfo.TabIndex = 34
        Me.AddInfo.Text = "Address"
        '
        'Label154
        '
        Me.Label154.AutoSize = True
        Me.Label154.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label154.Location = New System.Drawing.Point(84, 233)
        Me.Label154.Name = "Label154"
        Me.Label154.Size = New System.Drawing.Size(104, 15)
        Me.Label154.TabIndex = 190
        Me.Label154.Text = "City/Municipality"
        '
        'Label132
        '
        Me.Label132.AutoSize = True
        Me.Label132.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label132.Location = New System.Drawing.Point(84, 150)
        Me.Label132.Name = "Label132"
        Me.Label132.Size = New System.Drawing.Size(104, 15)
        Me.Label132.TabIndex = 189
        Me.Label132.Text = "City/Municipality"
        '
        'Label158
        '
        Me.Label158.AutoSize = True
        Me.Label158.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label158.Location = New System.Drawing.Point(384, 150)
        Me.Label158.Name = "Label158"
        Me.Label158.Size = New System.Drawing.Size(55, 15)
        Me.Label158.TabIndex = 188
        Me.Label158.Text = "Province"
        '
        'Label153
        '
        Me.Label153.AutoSize = True
        Me.Label153.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label153.Location = New System.Drawing.Point(384, 234)
        Me.Label153.Name = "Label153"
        Me.Label153.Size = New System.Drawing.Size(55, 15)
        Me.Label153.TabIndex = 186
        Me.Label153.Text = "Province"
        '
        'Label160
        '
        Me.Label160.AutoSize = True
        Me.Label160.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label160.Location = New System.Drawing.Point(683, 193)
        Me.Label160.Name = "Label160"
        Me.Label160.Size = New System.Drawing.Size(89, 15)
        Me.Label160.TabIndex = 184
        Me.Label160.Text = "Sitio, Barangay"
        '
        'Label161
        '
        Me.Label161.AutoSize = True
        Me.Label161.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label161.Location = New System.Drawing.Point(384, 195)
        Me.Label161.Name = "Label161"
        Me.Label161.Size = New System.Drawing.Size(127, 15)
        Me.Label161.TabIndex = 183
        Me.Label161.Text = "Street No, Street Name"
        '
        'Label155
        '
        Me.Label155.AutoSize = True
        Me.Label155.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label155.Location = New System.Drawing.Point(683, 111)
        Me.Label155.Name = "Label155"
        Me.Label155.Size = New System.Drawing.Size(89, 15)
        Me.Label155.TabIndex = 182
        Me.Label155.Text = "Sitio, Barangay"
        '
        'Label156
        '
        Me.Label156.AutoSize = True
        Me.Label156.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label156.Location = New System.Drawing.Point(384, 113)
        Me.Label156.Name = "Label156"
        Me.Label156.Size = New System.Drawing.Size(127, 15)
        Me.Label156.TabIndex = 181
        Me.Label156.Text = "Street No, Street Name"
        '
        'Label162
        '
        Me.Label162.AutoSize = True
        Me.Label162.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label162.Location = New System.Drawing.Point(84, 195)
        Me.Label162.Name = "Label162"
        Me.Label162.Size = New System.Drawing.Size(233, 15)
        Me.Label162.TabIndex = 180
        Me.Label162.Text = "Unit, Floor, Bldg., Compound, Subdivision"
        '
        'Label157
        '
        Me.Label157.AutoSize = True
        Me.Label157.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label157.Location = New System.Drawing.Point(84, 113)
        Me.Label157.Name = "Label157"
        Me.Label157.Size = New System.Drawing.Size(233, 15)
        Me.Label157.TabIndex = 179
        Me.Label157.Text = "Unit, Floor, Bldg., Compound, Subdivision"
        '
        'txtPermProvince
        '
        Me.txtPermProvince.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPermProvince.Location = New System.Drawing.Point(400, 249)
        Me.txtPermProvince.Name = "txtPermProvince"
        Me.txtPermProvince.Size = New System.Drawing.Size(264, 20)
        Me.txtPermProvince.TabIndex = 81
        '
        'txtPermCity
        '
        Me.txtPermCity.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPermCity.Location = New System.Drawing.Point(100, 249)
        Me.txtPermCity.Name = "txtPermCity"
        Me.txtPermCity.Size = New System.Drawing.Size(264, 20)
        Me.txtPermCity.TabIndex = 80
        '
        'txtPermSitio
        '
        Me.txtPermSitio.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPermSitio.Location = New System.Drawing.Point(699, 209)
        Me.txtPermSitio.Name = "txtPermSitio"
        Me.txtPermSitio.Size = New System.Drawing.Size(264, 20)
        Me.txtPermSitio.TabIndex = 79
        '
        'txtPermStreet
        '
        Me.txtPermStreet.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPermStreet.Location = New System.Drawing.Point(400, 211)
        Me.txtPermStreet.Name = "txtPermStreet"
        Me.txtPermStreet.Size = New System.Drawing.Size(264, 20)
        Me.txtPermStreet.TabIndex = 78
        '
        'txtPermBldg
        '
        Me.txtPermBldg.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPermBldg.Location = New System.Drawing.Point(100, 211)
        Me.txtPermBldg.Name = "txtPermBldg"
        Me.txtPermBldg.Size = New System.Drawing.Size(264, 20)
        Me.txtPermBldg.TabIndex = 77
        '
        'txtPrevProvince
        '
        Me.txtPrevProvince.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrevProvince.Location = New System.Drawing.Point(400, 166)
        Me.txtPrevProvince.Name = "txtPrevProvince"
        Me.txtPrevProvince.Size = New System.Drawing.Size(264, 20)
        Me.txtPrevProvince.TabIndex = 74
        '
        'txtPrevCity
        '
        Me.txtPrevCity.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrevCity.Location = New System.Drawing.Point(100, 166)
        Me.txtPrevCity.Name = "txtPrevCity"
        Me.txtPrevCity.Size = New System.Drawing.Size(264, 20)
        Me.txtPrevCity.TabIndex = 73
        '
        'txtPrevSitio
        '
        Me.txtPrevSitio.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrevSitio.Location = New System.Drawing.Point(699, 127)
        Me.txtPrevSitio.Name = "txtPrevSitio"
        Me.txtPrevSitio.Size = New System.Drawing.Size(264, 20)
        Me.txtPrevSitio.TabIndex = 72
        '
        'txtPrevStreet
        '
        Me.txtPrevStreet.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrevStreet.Location = New System.Drawing.Point(400, 129)
        Me.txtPrevStreet.Name = "txtPrevStreet"
        Me.txtPrevStreet.Size = New System.Drawing.Size(264, 20)
        Me.txtPrevStreet.TabIndex = 71
        '
        'txtPrevBldg
        '
        Me.txtPrevBldg.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrevBldg.Location = New System.Drawing.Point(100, 129)
        Me.txtPrevBldg.Name = "txtPrevBldg"
        Me.txtPrevBldg.Size = New System.Drawing.Size(264, 20)
        Me.txtPrevBldg.TabIndex = 70
        '
        'Label151
        '
        Me.Label151.AutoSize = True
        Me.Label151.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label151.Location = New System.Drawing.Point(384, 68)
        Me.Label151.Name = "Label151"
        Me.Label151.Size = New System.Drawing.Size(55, 15)
        Me.Label151.TabIndex = 155
        Me.Label151.Text = "Province"
        '
        'txtPresProvince
        '
        Me.txtPresProvince.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPresProvince.Location = New System.Drawing.Point(400, 84)
        Me.txtPresProvince.Name = "txtPresProvince"
        Me.txtPresProvince.Size = New System.Drawing.Size(264, 20)
        Me.txtPresProvince.TabIndex = 67
        '
        'Label152
        '
        Me.Label152.AutoSize = True
        Me.Label152.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label152.Location = New System.Drawing.Point(84, 68)
        Me.Label152.Name = "Label152"
        Me.Label152.Size = New System.Drawing.Size(104, 15)
        Me.Label152.TabIndex = 153
        Me.Label152.Text = "City/Municipality"
        '
        'txtPresCity
        '
        Me.txtPresCity.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPresCity.Location = New System.Drawing.Point(100, 84)
        Me.txtPresCity.Name = "txtPresCity"
        Me.txtPresCity.Size = New System.Drawing.Size(264, 20)
        Me.txtPresCity.TabIndex = 66
        '
        'Label99
        '
        Me.Label99.AutoSize = True
        Me.Label99.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label99.Location = New System.Drawing.Point(683, 29)
        Me.Label99.Name = "Label99"
        Me.Label99.Size = New System.Drawing.Size(89, 15)
        Me.Label99.TabIndex = 151
        Me.Label99.Text = "Sitio, Barangay"
        '
        'txtPresSitio
        '
        Me.txtPresSitio.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPresSitio.Location = New System.Drawing.Point(699, 45)
        Me.txtPresSitio.Name = "txtPresSitio"
        Me.txtPresSitio.Size = New System.Drawing.Size(264, 20)
        Me.txtPresSitio.TabIndex = 65
        '
        'Label97
        '
        Me.Label97.AutoSize = True
        Me.Label97.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label97.Location = New System.Drawing.Point(384, 31)
        Me.Label97.Name = "Label97"
        Me.Label97.Size = New System.Drawing.Size(127, 15)
        Me.Label97.TabIndex = 149
        Me.Label97.Text = "Street No, Street Name"
        '
        'txtPresStreet
        '
        Me.txtPresStreet.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPresStreet.Location = New System.Drawing.Point(400, 47)
        Me.txtPresStreet.Name = "txtPresStreet"
        Me.txtPresStreet.Size = New System.Drawing.Size(264, 20)
        Me.txtPresStreet.TabIndex = 64
        '
        'Label96
        '
        Me.Label96.AutoSize = True
        Me.Label96.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label96.Location = New System.Drawing.Point(84, 31)
        Me.Label96.Name = "Label96"
        Me.Label96.Size = New System.Drawing.Size(233, 15)
        Me.Label96.TabIndex = 147
        Me.Label96.Text = "Unit, Floor, Bldg., Compound, Subdivision"
        '
        'txtPresBldg
        '
        Me.txtPresBldg.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPresBldg.Location = New System.Drawing.Point(100, 47)
        Me.txtPresBldg.Name = "txtPresBldg"
        Me.txtPresBldg.Size = New System.Drawing.Size(264, 20)
        Me.txtPresBldg.TabIndex = 63
        '
        'cboStay3
        '
        Me.cboStay3.FormattingEnabled = True
        Me.cboStay3.Items.AddRange(New Object() {"Month/s", "Year/s"})
        Me.cboStay3.Location = New System.Drawing.Point(761, 247)
        Me.cboStay3.Name = "cboStay3"
        Me.cboStay3.Size = New System.Drawing.Size(92, 23)
        Me.cboStay3.TabIndex = 83
        '
        'cboStay2
        '
        Me.cboStay2.FormattingEnabled = True
        Me.cboStay2.Items.AddRange(New Object() {"Month/s", "Year/s"})
        Me.cboStay2.Location = New System.Drawing.Point(761, 161)
        Me.cboStay2.Name = "cboStay2"
        Me.cboStay2.Size = New System.Drawing.Size(92, 23)
        Me.cboStay2.TabIndex = 76
        '
        'cboStay1
        '
        Me.cboStay1.FormattingEnabled = True
        Me.cboStay1.Items.AddRange(New Object() {"Month/s", "Year/s"})
        Me.cboStay1.Location = New System.Drawing.Point(761, 82)
        Me.cboStay1.Name = "cboStay1"
        Me.cboStay1.Size = New System.Drawing.Size(92, 23)
        Me.cboStay1.TabIndex = 69
        '
        'txtStay3
        '
        Me.txtStay3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStay3.Location = New System.Drawing.Point(705, 249)
        Me.txtStay3.Name = "txtStay3"
        Me.txtStay3.Size = New System.Drawing.Size(49, 20)
        Me.txtStay3.TabIndex = 82
        '
        'txtStay2
        '
        Me.txtStay2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStay2.Location = New System.Drawing.Point(705, 163)
        Me.txtStay2.Name = "txtStay2"
        Me.txtStay2.Size = New System.Drawing.Size(49, 20)
        Me.txtStay2.TabIndex = 75
        '
        'txtStay1
        '
        Me.txtStay1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStay1.Location = New System.Drawing.Point(705, 83)
        Me.txtStay1.Name = "txtStay1"
        Me.txtStay1.Size = New System.Drawing.Size(49, 20)
        Me.txtStay1.TabIndex = 68
        '
        'Label103
        '
        Me.Label103.AutoSize = True
        Me.Label103.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label103.Location = New System.Drawing.Point(683, 232)
        Me.Label103.Name = "Label103"
        Me.Label103.Size = New System.Drawing.Size(85, 15)
        Me.Label103.TabIndex = 133
        Me.Label103.Text = "Length of Stay:"
        '
        'Label102
        '
        Me.Label102.AutoSize = True
        Me.Label102.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label102.Location = New System.Drawing.Point(683, 148)
        Me.Label102.Name = "Label102"
        Me.Label102.Size = New System.Drawing.Size(85, 15)
        Me.Label102.TabIndex = 129
        Me.Label102.Text = "Length of Stay:"
        '
        'Label101
        '
        Me.Label101.AutoSize = True
        Me.Label101.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label101.Location = New System.Drawing.Point(683, 66)
        Me.Label101.Name = "Label101"
        Me.Label101.Size = New System.Drawing.Size(85, 15)
        Me.Label101.TabIndex = 116
        Me.Label101.Text = "Length of Stay:"
        '
        'Label95
        '
        Me.Label95.AutoSize = True
        Me.Label95.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label95.Location = New System.Drawing.Point(8, 193)
        Me.Label95.Name = "Label95"
        Me.Label95.Size = New System.Drawing.Size(69, 15)
        Me.Label95.TabIndex = 116
        Me.Label95.Text = "Permanent"
        '
        'Label94
        '
        Me.Label94.AutoSize = True
        Me.Label94.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label94.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label94.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label94.Location = New System.Drawing.Point(5, 2)
        Me.Label94.Name = "Label94"
        Me.Label94.Size = New System.Drawing.Size(135, 18)
        Me.Label94.TabIndex = 121
        Me.Label94.Text = "Address Information"
        '
        'btnSearchPermAddress
        '
        Me.btnSearchPermAddress.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.viewmag_
        Me.btnSearchPermAddress.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSearchPermAddress.Location = New System.Drawing.Point(952, 165)
        Me.btnSearchPermAddress.Name = "btnSearchPermAddress"
        Me.btnSearchPermAddress.Size = New System.Drawing.Size(35, 19)
        Me.btnSearchPermAddress.TabIndex = 117
        Me.btnSearchPermAddress.UseVisualStyleBackColor = True
        Me.btnSearchPermAddress.Visible = False
        '
        'PictureBox22
        '
        Me.PictureBox22.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox22.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox22.Location = New System.Drawing.Point(1, 0)
        Me.PictureBox22.Name = "PictureBox22"
        Me.PictureBox22.Size = New System.Drawing.Size(1080, 22)
        Me.PictureBox22.TabIndex = 120
        Me.PictureBox22.TabStop = False
        '
        'Label85
        '
        Me.Label85.AutoSize = True
        Me.Label85.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label85.Location = New System.Drawing.Point(8, 29)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(50, 15)
        Me.Label85.TabIndex = 74
        Me.Label85.Text = "Present"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(7, 111)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(54, 15)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "Previous"
        '
        'btnSearchProvince
        '
        Me.btnSearchProvince.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.viewmag_
        Me.btnSearchProvince.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSearchProvince.Location = New System.Drawing.Point(911, 165)
        Me.btnSearchProvince.Name = "btnSearchProvince"
        Me.btnSearchProvince.Size = New System.Drawing.Size(35, 19)
        Me.btnSearchProvince.TabIndex = 109
        Me.btnSearchProvince.UseVisualStyleBackColor = True
        Me.btnSearchProvince.Visible = False
        '
        'btnSearchHome
        '
        Me.btnSearchHome.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.viewmag_
        Me.btnSearchHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSearchHome.Location = New System.Drawing.Point(870, 163)
        Me.btnSearchHome.Name = "btnSearchHome"
        Me.btnSearchHome.Size = New System.Drawing.Size(35, 23)
        Me.btnSearchHome.TabIndex = 108
        Me.btnSearchHome.UseVisualStyleBackColor = True
        Me.btnSearchHome.Visible = False
        '
        'ContactInfo
        '
        Me.ContactInfo.BackColor = System.Drawing.Color.White
        Me.ContactInfo.Controls.Add(Me.Label80)
        Me.ContactInfo.Controls.Add(Me.PictureBox20)
        Me.ContactInfo.Controls.Add(Me.txtFacebook2)
        Me.ContactInfo.Controls.Add(Me.txtEmailAddress)
        Me.ContactInfo.Controls.Add(Me.txtLandline2)
        Me.ContactInfo.Controls.Add(Me.txtFacebook1)
        Me.ContactInfo.Controls.Add(Me.Label14)
        Me.ContactInfo.Controls.Add(Me.txtLandline)
        Me.ContactInfo.Controls.Add(Me.Label93)
        Me.ContactInfo.Controls.Add(Me.Label92)
        Me.ContactInfo.Controls.Add(Me.txtresidencephone2)
        Me.ContactInfo.Controls.Add(Me.txtresidencephone)
        Me.ContactInfo.Controls.Add(Me.Label17)
        Me.ContactInfo.Controls.Add(Me.txtemailaddress2)
        Me.ContactInfo.Location = New System.Drawing.Point(4, 22)
        Me.ContactInfo.Name = "ContactInfo"
        Me.ContactInfo.Size = New System.Drawing.Size(1003, 275)
        Me.ContactInfo.TabIndex = 48
        Me.ContactInfo.Text = "Contact Info"
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label80.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label80.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label80.Location = New System.Drawing.Point(9, 3)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(197, 18)
        Me.Label80.TabIndex = 49
        Me.Label80.Text = "Employee Contact Information"
        '
        'PictureBox20
        '
        Me.PictureBox20.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox20.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox20.Location = New System.Drawing.Point(2, 0)
        Me.PictureBox20.Name = "PictureBox20"
        Me.PictureBox20.Size = New System.Drawing.Size(1071, 24)
        Me.PictureBox20.TabIndex = 48
        Me.PictureBox20.TabStop = False
        '
        'txtFacebook2
        '
        Me.txtFacebook2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFacebook2.Location = New System.Drawing.Point(355, 143)
        Me.txtFacebook2.Name = "txtFacebook2"
        Me.txtFacebook2.Size = New System.Drawing.Size(255, 20)
        Me.txtFacebook2.TabIndex = 91
        '
        'txtEmailAddress
        '
        Me.txtEmailAddress.BackColor = System.Drawing.Color.White
        Me.txtEmailAddress.Enabled = False
        Me.txtEmailAddress.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmailAddress.Location = New System.Drawing.Point(353, 54)
        Me.txtEmailAddress.MaxLength = 50
        Me.txtEmailAddress.Name = "txtEmailAddress"
        Me.txtEmailAddress.Size = New System.Drawing.Size(257, 20)
        Me.txtEmailAddress.TabIndex = 86
        '
        'txtLandline2
        '
        Me.txtLandline2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLandline2.Location = New System.Drawing.Point(68, 78)
        Me.txtLandline2.Name = "txtLandline2"
        Me.txtLandline2.Size = New System.Drawing.Size(193, 20)
        Me.txtLandline2.TabIndex = 85
        '
        'txtFacebook1
        '
        Me.txtFacebook1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFacebook1.Location = New System.Drawing.Point(354, 119)
        Me.txtFacebook1.Name = "txtFacebook1"
        Me.txtFacebook1.Size = New System.Drawing.Size(256, 20)
        Me.txtFacebook1.TabIndex = 90
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(40, 104)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(81, 15)
        Me.Label14.TabIndex = 23
        Me.Label14.Text = "Mobile No(s):"
        '
        'txtLandline
        '
        Me.txtLandline.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLandline.Location = New System.Drawing.Point(68, 54)
        Me.txtLandline.Name = "txtLandline"
        Me.txtLandline.Size = New System.Drawing.Size(193, 20)
        Me.txtLandline.TabIndex = 84
        '
        'Label93
        '
        Me.Label93.AutoSize = True
        Me.Label93.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label93.Location = New System.Drawing.Point(40, 39)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(70, 15)
        Me.Label93.TabIndex = 113
        Me.Label93.Text = "Landline(s):"
        '
        'Label92
        '
        Me.Label92.AutoSize = True
        Me.Label92.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label92.Location = New System.Drawing.Point(333, 104)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(62, 15)
        Me.Label92.TabIndex = 114
        Me.Label92.Text = "Facebook:"
        '
        'txtresidencephone2
        '
        Me.txtresidencephone2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtresidencephone2.Location = New System.Drawing.Point(68, 143)
        Me.txtresidencephone2.Name = "txtresidencephone2"
        Me.txtresidencephone2.Size = New System.Drawing.Size(193, 20)
        Me.txtresidencephone2.TabIndex = 89
        '
        'txtresidencephone
        '
        Me.txtresidencephone.BackColor = System.Drawing.Color.White
        Me.txtresidencephone.Enabled = False
        Me.txtresidencephone.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtresidencephone.Location = New System.Drawing.Point(68, 119)
        Me.txtresidencephone.Name = "txtresidencephone"
        Me.txtresidencephone.Size = New System.Drawing.Size(193, 20)
        Me.txtresidencephone.TabIndex = 88
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(332, 39)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(107, 15)
        Me.Label17.TabIndex = 26
        Me.Label17.Text = "Email Address(es):"
        '
        'txtemailaddress2
        '
        Me.txtemailaddress2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtemailaddress2.Location = New System.Drawing.Point(353, 78)
        Me.txtemailaddress2.Name = "txtemailaddress2"
        Me.txtemailaddress2.Size = New System.Drawing.Size(257, 20)
        Me.txtemailaddress2.TabIndex = 87
        '
        'Ownership
        '
        Me.Ownership.BackColor = System.Drawing.Color.White
        Me.Ownership.Controls.Add(Me.GroupBox7)
        Me.Ownership.Controls.Add(Me.GroupBox6)
        Me.Ownership.Controls.Add(Me.GroupBox5)
        Me.Ownership.Controls.Add(Me.Label140)
        Me.Ownership.Controls.Add(Me.PictureBox28)
        Me.Ownership.Location = New System.Drawing.Point(4, 22)
        Me.Ownership.Name = "Ownership"
        Me.Ownership.Size = New System.Drawing.Size(1003, 275)
        Me.Ownership.TabIndex = 41
        Me.Ownership.Text = "Ownership"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.Label146)
        Me.GroupBox7.Controls.Add(Me.Label144)
        Me.GroupBox7.Controls.Add(Me.txtCar)
        Me.GroupBox7.Controls.Add(Me.rbCNo)
        Me.GroupBox7.Controls.Add(Me.rbCYes)
        Me.GroupBox7.Location = New System.Drawing.Point(645, 37)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(312, 135)
        Me.GroupBox7.TabIndex = 63
        Me.GroupBox7.TabStop = False
        '
        'Label146
        '
        Me.Label146.AutoSize = True
        Me.Label146.Location = New System.Drawing.Point(121, 100)
        Me.Label146.Name = "Label146"
        Me.Label146.Size = New System.Drawing.Size(72, 15)
        Me.Label146.TabIndex = 51
        Me.Label146.Text = "How Many?"
        '
        'Label144
        '
        Me.Label144.AutoSize = True
        Me.Label144.Location = New System.Drawing.Point(34, 26)
        Me.Label144.Name = "Label144"
        Me.Label144.Size = New System.Drawing.Size(114, 15)
        Me.Label144.TabIndex = 46
        Me.Label144.Text = "Do You Own a Car?"
        '
        'txtCar
        '
        Me.txtCar.Location = New System.Drawing.Point(206, 97)
        Me.txtCar.Name = "txtCar"
        Me.txtCar.Size = New System.Drawing.Size(61, 21)
        Me.txtCar.TabIndex = 100
        '
        'rbCNo
        '
        Me.rbCNo.AutoSize = True
        Me.rbCNo.Location = New System.Drawing.Point(83, 51)
        Me.rbCNo.Name = "rbCNo"
        Me.rbCNo.Size = New System.Drawing.Size(41, 19)
        Me.rbCNo.TabIndex = 98
        Me.rbCNo.TabStop = True
        Me.rbCNo.Text = "No"
        Me.rbCNo.UseVisualStyleBackColor = True
        '
        'rbCYes
        '
        Me.rbCYes.AutoSize = True
        Me.rbCYes.Location = New System.Drawing.Point(83, 76)
        Me.rbCYes.Name = "rbCYes"
        Me.rbCYes.Size = New System.Drawing.Size(45, 19)
        Me.rbCYes.TabIndex = 99
        Me.rbCYes.TabStop = True
        Me.rbCYes.Text = "Yes"
        Me.rbCYes.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.rbMYes)
        Me.GroupBox6.Controls.Add(Me.Label143)
        Me.GroupBox6.Controls.Add(Me.Label145)
        Me.GroupBox6.Controls.Add(Me.txtMotor)
        Me.GroupBox6.Controls.Add(Me.rbMNo)
        Me.GroupBox6.Location = New System.Drawing.Point(297, 37)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(323, 135)
        Me.GroupBox6.TabIndex = 62
        Me.GroupBox6.TabStop = False
        '
        'rbMYes
        '
        Me.rbMYes.AutoSize = True
        Me.rbMYes.Location = New System.Drawing.Point(86, 76)
        Me.rbMYes.Name = "rbMYes"
        Me.rbMYes.Size = New System.Drawing.Size(45, 19)
        Me.rbMYes.TabIndex = 96
        Me.rbMYes.TabStop = True
        Me.rbMYes.Text = "Yes"
        Me.rbMYes.UseVisualStyleBackColor = True
        '
        'Label143
        '
        Me.Label143.AutoSize = True
        Me.Label143.Location = New System.Drawing.Point(31, 26)
        Me.Label143.Name = "Label143"
        Me.Label143.Size = New System.Drawing.Size(152, 15)
        Me.Label143.TabIndex = 42
        Me.Label143.Text = "Do you Own a Motorcycle?"
        '
        'Label145
        '
        Me.Label145.AutoSize = True
        Me.Label145.Location = New System.Drawing.Point(134, 100)
        Me.Label145.Name = "Label145"
        Me.Label145.Size = New System.Drawing.Size(72, 15)
        Me.Label145.TabIndex = 50
        Me.Label145.Text = "How Many?"
        '
        'txtMotor
        '
        Me.txtMotor.Location = New System.Drawing.Point(222, 97)
        Me.txtMotor.Name = "txtMotor"
        Me.txtMotor.Size = New System.Drawing.Size(65, 21)
        Me.txtMotor.TabIndex = 97
        '
        'rbMNo
        '
        Me.rbMNo.AutoSize = True
        Me.rbMNo.Location = New System.Drawing.Point(86, 51)
        Me.rbMNo.Name = "rbMNo"
        Me.rbMNo.Size = New System.Drawing.Size(41, 19)
        Me.rbMNo.TabIndex = 95
        Me.rbMNo.TabStop = True
        Me.rbMNo.Text = "No"
        Me.rbMNo.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.rbRented)
        Me.GroupBox5.Controls.Add(Me.Label142)
        Me.GroupBox5.Controls.Add(Me.rbOwned)
        Me.GroupBox5.Controls.Add(Me.rbParents)
        Me.GroupBox5.Location = New System.Drawing.Point(33, 37)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(239, 135)
        Me.GroupBox5.TabIndex = 61
        Me.GroupBox5.TabStop = False
        '
        'rbRented
        '
        Me.rbRented.AutoSize = True
        Me.rbRented.Location = New System.Drawing.Point(53, 75)
        Me.rbRented.Name = "rbRented"
        Me.rbRented.Size = New System.Drawing.Size(65, 19)
        Me.rbRented.TabIndex = 93
        Me.rbRented.TabStop = True
        Me.rbRented.Text = "Rented"
        Me.rbRented.UseVisualStyleBackColor = True
        '
        'Label142
        '
        Me.Label142.AutoSize = True
        Me.Label142.Location = New System.Drawing.Point(11, 21)
        Me.Label142.Name = "Label142"
        Me.Label142.Size = New System.Drawing.Size(103, 15)
        Me.Label142.TabIndex = 38
        Me.Label142.Text = "Home Ownership"
        '
        'rbOwned
        '
        Me.rbOwned.AutoSize = True
        Me.rbOwned.Location = New System.Drawing.Point(53, 50)
        Me.rbOwned.Name = "rbOwned"
        Me.rbOwned.Size = New System.Drawing.Size(64, 19)
        Me.rbOwned.TabIndex = 92
        Me.rbOwned.TabStop = True
        Me.rbOwned.Text = "Owned"
        Me.rbOwned.UseVisualStyleBackColor = True
        '
        'rbParents
        '
        Me.rbParents.AutoSize = True
        Me.rbParents.Location = New System.Drawing.Point(53, 100)
        Me.rbParents.Name = "rbParents"
        Me.rbParents.Size = New System.Drawing.Size(180, 19)
        Me.rbParents.TabIndex = 94
        Me.rbParents.TabStop = True
        Me.rbParents.Text = "Living with Parents/Relatives"
        Me.rbParents.UseVisualStyleBackColor = True
        '
        'Label140
        '
        Me.Label140.AutoSize = True
        Me.Label140.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label140.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label140.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label140.Location = New System.Drawing.Point(7, 2)
        Me.Label140.Name = "Label140"
        Me.Label140.Size = New System.Drawing.Size(150, 18)
        Me.Label140.TabIndex = 37
        Me.Label140.Text = "Ownership Declaration"
        '
        'PictureBox28
        '
        Me.PictureBox28.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox28.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox28.Location = New System.Drawing.Point(1, 0)
        Me.PictureBox28.Name = "PictureBox28"
        Me.PictureBox28.Size = New System.Drawing.Size(1050, 22)
        Me.PictureBox28.TabIndex = 36
        Me.PictureBox28.TabStop = False
        '
        'Family
        '
        Me.Family.BackColor = System.Drawing.Color.White
        Me.Family.Controls.Add(Me.GroupBox9)
        Me.Family.Controls.Add(Me.GroupBox8)
        Me.Family.Controls.Add(Me.txtChildNo)
        Me.Family.Controls.Add(Me.Label134)
        Me.Family.Controls.Add(Me.txtSpouseStay)
        Me.Family.Controls.Add(Me.rbSpouseYears)
        Me.Family.Controls.Add(Me.rbSpouseMonth)
        Me.Family.Controls.Add(Me.Label131)
        Me.Family.Controls.Add(Me.txtSpouseAdd)
        Me.Family.Controls.Add(Me.Label133)
        Me.Family.Controls.Add(Me.btnSpouseSearchAdd)
        Me.Family.Controls.Add(Me.txtOccupation)
        Me.Family.Controls.Add(Me.Label130)
        Me.Family.Controls.Add(Me.txtSpouseContact)
        Me.Family.Controls.Add(Me.Label128)
        Me.Family.Controls.Add(Me.txtSpouseName)
        Me.Family.Controls.Add(Me.Label129)
        Me.Family.Controls.Add(Me.txtMContact)
        Me.Family.Controls.Add(Me.Label125)
        Me.Family.Controls.Add(Me.txtFContact)
        Me.Family.Controls.Add(Me.Label124)
        Me.Family.Controls.Add(Me.btnMSearchAdd)
        Me.Family.Controls.Add(Me.btnFSearchAdd)
        Me.Family.Controls.Add(Me.txtMAdd)
        Me.Family.Controls.Add(Me.Label120)
        Me.Family.Controls.Add(Me.txtFAdd)
        Me.Family.Controls.Add(Me.Label119)
        Me.Family.Controls.Add(Me.txtMotherName)
        Me.Family.Controls.Add(Me.Label116)
        Me.Family.Controls.Add(Me.txtFatherName)
        Me.Family.Controls.Add(Me.Label115)
        Me.Family.Controls.Add(Me.Label113)
        Me.Family.Controls.Add(Me.PictureBox26)
        Me.Family.Location = New System.Drawing.Point(4, 22)
        Me.Family.Name = "Family"
        Me.Family.Size = New System.Drawing.Size(1003, 275)
        Me.Family.TabIndex = 39
        Me.Family.Text = "Family"
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.cboContactPerson)
        Me.GroupBox9.Controls.Add(Me.Label34)
        Me.GroupBox9.Location = New System.Drawing.Point(803, 139)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(192, 133)
        Me.GroupBox9.TabIndex = 184
        Me.GroupBox9.TabStop = False
        '
        'cboContactPerson
        '
        Me.cboContactPerson.FormattingEnabled = True
        Me.cboContactPerson.Items.AddRange(New Object() {"Mother", "Father", "Spouse", "Brother", "Sister"})
        Me.cboContactPerson.Location = New System.Drawing.Point(26, 64)
        Me.cboContactPerson.Name = "cboContactPerson"
        Me.cboContactPerson.Size = New System.Drawing.Size(163, 23)
        Me.cboContactPerson.TabIndex = 1
        Me.cboContactPerson.Text = "SELECT"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(3, 35)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(93, 15)
        Me.Label34.TabIndex = 0
        Me.Label34.Text = "Contact Person:"
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.btnUpdateSibling)
        Me.GroupBox8.Controls.Add(Me.btnNewSibling)
        Me.GroupBox8.Controls.Add(Me.lvlSibling)
        Me.GroupBox8.Location = New System.Drawing.Point(9, 136)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(792, 136)
        Me.GroupBox8.TabIndex = 183
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Brother/s and Sister/s"
        '
        'btnUpdateSibling
        '
        Me.btnUpdateSibling.Image = CType(resources.GetObject("btnUpdateSibling.Image"), System.Drawing.Image)
        Me.btnUpdateSibling.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateSibling.Location = New System.Drawing.Point(717, 101)
        Me.btnUpdateSibling.Name = "btnUpdateSibling"
        Me.btnUpdateSibling.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnUpdateSibling.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateSibling.TabIndex = 185
        Me.btnUpdateSibling.Text = "Edit"
        Me.btnUpdateSibling.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateSibling.UseVisualStyleBackColor = True
        '
        'btnNewSibling
        '
        Me.btnNewSibling.Image = CType(resources.GetObject("btnNewSibling.Image"), System.Drawing.Image)
        Me.btnNewSibling.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewSibling.Location = New System.Drawing.Point(717, 71)
        Me.btnNewSibling.Name = "btnNewSibling"
        Me.btnNewSibling.Size = New System.Drawing.Size(68, 24)
        Me.btnNewSibling.TabIndex = 184
        Me.btnNewSibling.Text = "New"
        Me.btnNewSibling.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewSibling.UseVisualStyleBackColor = True
        '
        'lvlSibling
        '
        Me.lvlSibling.Location = New System.Drawing.Point(24, 20)
        Me.lvlSibling.Name = "lvlSibling"
        Me.lvlSibling.Size = New System.Drawing.Size(686, 105)
        Me.lvlSibling.TabIndex = 183
        Me.lvlSibling.UseCompatibleStateImageBehavior = False
        '
        'txtChildNo
        '
        Me.txtChildNo.Location = New System.Drawing.Point(865, 29)
        Me.txtChildNo.Name = "txtChildNo"
        Me.txtChildNo.Size = New System.Drawing.Size(56, 21)
        Me.txtChildNo.TabIndex = 120
        '
        'Label134
        '
        Me.Label134.AutoSize = True
        Me.Label134.Location = New System.Drawing.Point(775, 31)
        Me.Label134.Name = "Label134"
        Me.Label134.Size = New System.Drawing.Size(87, 15)
        Me.Label134.TabIndex = 178
        Me.Label134.Text = "No Of Children"
        '
        'txtSpouseStay
        '
        Me.txtSpouseStay.Location = New System.Drawing.Point(576, 53)
        Me.txtSpouseStay.Name = "txtSpouseStay"
        Me.txtSpouseStay.Size = New System.Drawing.Size(49, 21)
        Me.txtSpouseStay.TabIndex = 124
        '
        'rbSpouseYears
        '
        Me.rbSpouseYears.AutoSize = True
        Me.rbSpouseYears.Location = New System.Drawing.Point(702, 54)
        Me.rbSpouseYears.Name = "rbSpouseYears"
        Me.rbSpouseYears.Size = New System.Drawing.Size(59, 19)
        Me.rbSpouseYears.TabIndex = 126
        Me.rbSpouseYears.TabStop = True
        Me.rbSpouseYears.Text = "Year/s"
        Me.rbSpouseYears.UseVisualStyleBackColor = True
        '
        'rbSpouseMonth
        '
        Me.rbSpouseMonth.AutoSize = True
        Me.rbSpouseMonth.Location = New System.Drawing.Point(631, 54)
        Me.rbSpouseMonth.Name = "rbSpouseMonth"
        Me.rbSpouseMonth.Size = New System.Drawing.Size(69, 19)
        Me.rbSpouseMonth.TabIndex = 125
        Me.rbSpouseMonth.TabStop = True
        Me.rbSpouseMonth.Text = "Month/s"
        Me.rbSpouseMonth.UseVisualStyleBackColor = True
        '
        'Label131
        '
        Me.Label131.AutoSize = True
        Me.Label131.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label131.Location = New System.Drawing.Point(484, 56)
        Me.Label131.Name = "Label131"
        Me.Label131.Size = New System.Drawing.Size(85, 15)
        Me.Label131.TabIndex = 172
        Me.Label131.Text = "Length of Stay:"
        '
        'txtSpouseAdd
        '
        Me.txtSpouseAdd.Location = New System.Drawing.Point(113, 54)
        Me.txtSpouseAdd.Name = "txtSpouseAdd"
        Me.txtSpouseAdd.Size = New System.Drawing.Size(312, 21)
        Me.txtSpouseAdd.TabIndex = 121
        '
        'Label133
        '
        Me.Label133.AutoSize = True
        Me.Label133.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label133.Location = New System.Drawing.Point(18, 58)
        Me.Label133.Name = "Label133"
        Me.Label133.Size = New System.Drawing.Size(95, 15)
        Me.Label133.TabIndex = 170
        Me.Label133.Text = "Present Address"
        '
        'btnSpouseSearchAdd
        '
        Me.btnSpouseSearchAdd.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.viewmag_
        Me.btnSpouseSearchAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSpouseSearchAdd.Location = New System.Drawing.Point(431, 53)
        Me.btnSpouseSearchAdd.Name = "btnSpouseSearchAdd"
        Me.btnSpouseSearchAdd.Size = New System.Drawing.Size(35, 23)
        Me.btnSpouseSearchAdd.TabIndex = 122
        Me.btnSpouseSearchAdd.UseVisualStyleBackColor = True
        '
        'txtOccupation
        '
        Me.txtOccupation.Location = New System.Drawing.Point(625, 28)
        Me.txtOccupation.Name = "txtOccupation"
        Me.txtOccupation.Size = New System.Drawing.Size(136, 21)
        Me.txtOccupation.TabIndex = 119
        '
        'Label130
        '
        Me.Label130.AutoSize = True
        Me.Label130.Location = New System.Drawing.Point(550, 31)
        Me.Label130.Name = "Label130"
        Me.Label130.Size = New System.Drawing.Size(69, 15)
        Me.Label130.TabIndex = 167
        Me.Label130.Text = "Occupation"
        '
        'txtSpouseContact
        '
        Me.txtSpouseContact.Location = New System.Drawing.Point(420, 29)
        Me.txtSpouseContact.Name = "txtSpouseContact"
        Me.txtSpouseContact.Size = New System.Drawing.Size(113, 21)
        Me.txtSpouseContact.TabIndex = 118
        '
        'Label128
        '
        Me.Label128.AutoSize = True
        Me.Label128.Location = New System.Drawing.Point(344, 31)
        Me.Label128.Name = "Label128"
        Me.Label128.Size = New System.Drawing.Size(70, 15)
        Me.Label128.TabIndex = 165
        Me.Label128.Text = "Contact No."
        '
        'txtSpouseName
        '
        Me.txtSpouseName.Location = New System.Drawing.Point(113, 28)
        Me.txtSpouseName.Name = "txtSpouseName"
        Me.txtSpouseName.Size = New System.Drawing.Size(226, 21)
        Me.txtSpouseName.TabIndex = 117
        '
        'Label129
        '
        Me.Label129.AutoSize = True
        Me.Label129.Location = New System.Drawing.Point(17, 31)
        Me.Label129.Name = "Label129"
        Me.Label129.Size = New System.Drawing.Size(86, 15)
        Me.Label129.TabIndex = 163
        Me.Label129.Text = "Spouse Name"
        '
        'txtMContact
        '
        Me.txtMContact.Location = New System.Drawing.Point(848, 112)
        Me.txtMContact.Name = "txtMContact"
        Me.txtMContact.Size = New System.Drawing.Size(143, 21)
        Me.txtMContact.TabIndex = 108
        '
        'Label125
        '
        Me.Label125.AutoSize = True
        Me.Label125.Location = New System.Drawing.Point(775, 115)
        Me.Label125.Name = "Label125"
        Me.Label125.Size = New System.Drawing.Size(70, 15)
        Me.Label125.TabIndex = 157
        Me.Label125.Text = "Contact No."
        '
        'txtFContact
        '
        Me.txtFContact.Location = New System.Drawing.Point(848, 85)
        Me.txtFContact.Name = "txtFContact"
        Me.txtFContact.Size = New System.Drawing.Size(143, 21)
        Me.txtFContact.TabIndex = 104
        '
        'Label124
        '
        Me.Label124.AutoSize = True
        Me.Label124.Location = New System.Drawing.Point(775, 88)
        Me.Label124.Name = "Label124"
        Me.Label124.Size = New System.Drawing.Size(70, 15)
        Me.Label124.TabIndex = 155
        Me.Label124.Text = "Contact No."
        '
        'btnMSearchAdd
        '
        Me.btnMSearchAdd.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.viewmag_
        Me.btnMSearchAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnMSearchAdd.Location = New System.Drawing.Point(725, 111)
        Me.btnMSearchAdd.Name = "btnMSearchAdd"
        Me.btnMSearchAdd.Size = New System.Drawing.Size(35, 23)
        Me.btnMSearchAdd.TabIndex = 107
        Me.btnMSearchAdd.UseVisualStyleBackColor = True
        '
        'btnFSearchAdd
        '
        Me.btnFSearchAdd.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.viewmag_
        Me.btnFSearchAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnFSearchAdd.Location = New System.Drawing.Point(726, 83)
        Me.btnFSearchAdd.Name = "btnFSearchAdd"
        Me.btnFSearchAdd.Size = New System.Drawing.Size(35, 23)
        Me.btnFSearchAdd.TabIndex = 103
        Me.btnFSearchAdd.UseVisualStyleBackColor = True
        '
        'txtMAdd
        '
        Me.txtMAdd.Location = New System.Drawing.Point(420, 112)
        Me.txtMAdd.Name = "txtMAdd"
        Me.txtMAdd.Size = New System.Drawing.Size(299, 21)
        Me.txtMAdd.TabIndex = 106
        '
        'Label120
        '
        Me.Label120.AutoSize = True
        Me.Label120.Location = New System.Drawing.Point(363, 115)
        Me.Label120.Name = "Label120"
        Me.Label120.Size = New System.Drawing.Size(51, 15)
        Me.Label120.TabIndex = 144
        Me.Label120.Text = "Address"
        '
        'txtFAdd
        '
        Me.txtFAdd.Location = New System.Drawing.Point(420, 85)
        Me.txtFAdd.Name = "txtFAdd"
        Me.txtFAdd.Size = New System.Drawing.Size(299, 21)
        Me.txtFAdd.TabIndex = 102
        '
        'Label119
        '
        Me.Label119.AutoSize = True
        Me.Label119.Location = New System.Drawing.Point(363, 88)
        Me.Label119.Name = "Label119"
        Me.Label119.Size = New System.Drawing.Size(51, 15)
        Me.Label119.TabIndex = 142
        Me.Label119.Text = "Address"
        '
        'txtMotherName
        '
        Me.txtMotherName.Location = New System.Drawing.Point(113, 112)
        Me.txtMotherName.Name = "txtMotherName"
        Me.txtMotherName.Size = New System.Drawing.Size(226, 21)
        Me.txtMotherName.TabIndex = 105
        '
        'Label116
        '
        Me.Label116.AutoSize = True
        Me.Label116.Location = New System.Drawing.Point(17, 115)
        Me.Label116.Name = "Label116"
        Me.Label116.Size = New System.Drawing.Size(92, 15)
        Me.Label116.TabIndex = 136
        Me.Label116.Text = "Mother's Name"
        '
        'txtFatherName
        '
        Me.txtFatherName.Location = New System.Drawing.Point(113, 85)
        Me.txtFatherName.Name = "txtFatherName"
        Me.txtFatherName.Size = New System.Drawing.Size(226, 21)
        Me.txtFatherName.TabIndex = 101
        '
        'Label115
        '
        Me.Label115.AutoSize = True
        Me.Label115.Location = New System.Drawing.Point(17, 88)
        Me.Label115.Name = "Label115"
        Me.Label115.Size = New System.Drawing.Size(88, 15)
        Me.Label115.TabIndex = 134
        Me.Label115.Text = "Father's Name"
        '
        'Label113
        '
        Me.Label113.AutoSize = True
        Me.Label113.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label113.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label113.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label113.Location = New System.Drawing.Point(6, 2)
        Me.Label113.Name = "Label113"
        Me.Label113.Size = New System.Drawing.Size(124, 18)
        Me.Label113.TabIndex = 133
        Me.Label113.Text = "Family Background"
        '
        'PictureBox26
        '
        Me.PictureBox26.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox26.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox26.Location = New System.Drawing.Point(2, 0)
        Me.PictureBox26.Name = "PictureBox26"
        Me.PictureBox26.Size = New System.Drawing.Size(1080, 22)
        Me.PictureBox26.TabIndex = 132
        Me.PictureBox26.TabStop = False
        '
        'Dependents
        '
        Me.Dependents.BackColor = System.Drawing.SystemColors.Window
        Me.Dependents.Controls.Add(Me.LvlEmployeeDependent)
        Me.Dependents.Controls.Add(Me.Label68)
        Me.Dependents.Controls.Add(Me.GroupBox3)
        Me.Dependents.Controls.Add(Me.btnemp_editdepdts)
        Me.Dependents.Controls.Add(Me.btndelete_dep)
        Me.Dependents.Controls.Add(Me.btnadd_dep)
        Me.Dependents.Controls.Add(Me.PictureBox5)
        Me.Dependents.Location = New System.Drawing.Point(4, 22)
        Me.Dependents.Name = "Dependents"
        Me.Dependents.Size = New System.Drawing.Size(1003, 275)
        Me.Dependents.TabIndex = 2
        Me.Dependents.Text = "Dependents"
        Me.Dependents.UseVisualStyleBackColor = True
        '
        'LvlEmployeeDependent
        '
        Me.LvlEmployeeDependent.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LvlEmployeeDependent.Location = New System.Drawing.Point(10, 25)
        Me.LvlEmployeeDependent.Name = "LvlEmployeeDependent"
        Me.LvlEmployeeDependent.Size = New System.Drawing.Size(909, 217)
        Me.LvlEmployeeDependent.TabIndex = 68
        Me.LvlEmployeeDependent.UseCompatibleStateImageBehavior = False
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label68.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label68.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label68.Location = New System.Drawing.Point(3, 1)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(149, 18)
        Me.Label68.TabIndex = 8
        Me.Label68.Text = "Employee Dependents"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.emp_pic)
        Me.GroupBox3.Controls.Add(Me.temp_marks)
        Me.GroupBox3.Controls.Add(Me.txtitemno)
        Me.GroupBox3.Controls.Add(Me.txtremarks)
        Me.GroupBox3.Controls.Add(Me.temp_add3)
        Me.GroupBox3.Controls.Add(Me.txtparent_ID)
        Me.GroupBox3.Controls.Add(Me.txtmonthreqular)
        Me.GroupBox3.Controls.Add(Me.txtkeysection)
        Me.GroupBox3.Controls.Add(Me.temp_height)
        Me.GroupBox3.Controls.Add(Me.txtdescsection)
        Me.GroupBox3.Controls.Add(Me.txtsalarygrade)
        Me.GroupBox3.Controls.Add(Me.txtdescdepartment)
        Me.GroupBox3.Controls.Add(Me.temp_add2)
        Me.GroupBox3.Controls.Add(Me.txtdescdivision)
        Me.GroupBox3.Controls.Add(Me.txtweight)
        Me.GroupBox3.Controls.Add(Me.txtkeydepartment)
        Me.GroupBox3.Controls.Add(Me.txtdateregular)
        Me.GroupBox3.Controls.Add(Me.txtkeydivision)
        Me.GroupBox3.Controls.Add(Me.Dateregular)
        Me.GroupBox3.Controls.Add(Me.txtkeycompany)
        Me.GroupBox3.Controls.Add(Me.emp_extphone)
        Me.GroupBox3.Controls.Add(Me.cbosalarygrade)
        Me.GroupBox3.Controls.Add(Me.temp_citizen)
        Me.GroupBox3.Controls.Add(Me.temp_designation)
        Me.GroupBox3.Controls.Add(Me.txtType_employee)
        Me.GroupBox3.Controls.Add(Me.txtfxkeypositionlevel)
        Me.GroupBox3.Controls.Add(Me.txtfullname)
        Me.GroupBox3.Controls.Add(Me.TXTRECID)
        Me.GroupBox3.Controls.Add(Me.cbodept)
        Me.GroupBox3.Controls.Add(Me.Dateresigned)
        Me.GroupBox3.Controls.Add(Me.TXTKEYEMPLOYEEID)
        Me.GroupBox3.Enabled = False
        Me.GroupBox3.Location = New System.Drawing.Point(11, 253)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(299, 21)
        Me.GroupBox3.TabIndex = 67
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Omitted items in Personnel New Version"
        Me.GroupBox3.Visible = False
        '
        'emp_pic
        '
        Me.emp_pic.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar
        Me.emp_pic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.emp_pic.Location = New System.Drawing.Point(96, 92)
        Me.emp_pic.Name = "emp_pic"
        Me.emp_pic.Size = New System.Drawing.Size(126, 124)
        Me.emp_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.emp_pic.TabIndex = 0
        Me.emp_pic.TabStop = False
        '
        'temp_marks
        '
        Me.temp_marks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_marks.Location = New System.Drawing.Point(104, 182)
        Me.temp_marks.MaxLength = 50
        Me.temp_marks.Name = "temp_marks"
        Me.temp_marks.Size = New System.Drawing.Size(137, 21)
        Me.temp_marks.TabIndex = 9
        '
        'txtitemno
        '
        Me.txtitemno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtitemno.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtitemno.Location = New System.Drawing.Point(57, 37)
        Me.txtitemno.Name = "txtitemno"
        Me.txtitemno.Size = New System.Drawing.Size(35, 21)
        Me.txtitemno.TabIndex = 19
        '
        'txtremarks
        '
        Me.txtremarks.Location = New System.Drawing.Point(66, 208)
        Me.txtremarks.Multiline = True
        Me.txtremarks.Name = "txtremarks"
        Me.txtremarks.Size = New System.Drawing.Size(39, 22)
        Me.txtremarks.TabIndex = 43
        '
        'temp_add3
        '
        Me.temp_add3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_add3.Location = New System.Drawing.Point(104, 208)
        Me.temp_add3.MaxLength = 255
        Me.temp_add3.Name = "temp_add3"
        Me.temp_add3.Size = New System.Drawing.Size(156, 21)
        Me.temp_add3.TabIndex = 5
        '
        'txtparent_ID
        '
        Me.txtparent_ID.Location = New System.Drawing.Point(166, 183)
        Me.txtparent_ID.Name = "txtparent_ID"
        Me.txtparent_ID.Size = New System.Drawing.Size(28, 21)
        Me.txtparent_ID.TabIndex = 97
        Me.txtparent_ID.Visible = False
        '
        'txtmonthreqular
        '
        Me.txtmonthreqular.Location = New System.Drawing.Point(125, 60)
        Me.txtmonthreqular.MaxLength = 3
        Me.txtmonthreqular.Name = "txtmonthreqular"
        Me.txtmonthreqular.Size = New System.Drawing.Size(39, 21)
        Me.txtmonthreqular.TabIndex = 15
        '
        'txtkeysection
        '
        Me.txtkeysection.Location = New System.Drawing.Point(132, 183)
        Me.txtkeysection.Name = "txtkeysection"
        Me.txtkeysection.Size = New System.Drawing.Size(28, 21)
        Me.txtkeysection.TabIndex = 96
        Me.txtkeysection.Visible = False
        '
        'temp_height
        '
        Me.temp_height.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_height.Location = New System.Drawing.Point(78, 62)
        Me.temp_height.MaxLength = 10
        Me.temp_height.Name = "temp_height"
        Me.temp_height.Size = New System.Drawing.Size(51, 21)
        Me.temp_height.TabIndex = 7
        '
        'txtdescsection
        '
        Me.txtdescsection.Location = New System.Drawing.Point(166, 159)
        Me.txtdescsection.Name = "txtdescsection"
        Me.txtdescsection.Size = New System.Drawing.Size(59, 21)
        Me.txtdescsection.TabIndex = 95
        Me.txtdescsection.Visible = False
        '
        'txtsalarygrade
        '
        Me.txtsalarygrade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtsalarygrade.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsalarygrade.Location = New System.Drawing.Point(183, 32)
        Me.txtsalarygrade.Name = "txtsalarygrade"
        Me.txtsalarygrade.Size = New System.Drawing.Size(39, 21)
        Me.txtsalarygrade.TabIndex = 47
        Me.txtsalarygrade.Visible = False
        '
        'txtdescdepartment
        '
        Me.txtdescdepartment.Location = New System.Drawing.Point(166, 136)
        Me.txtdescdepartment.Name = "txtdescdepartment"
        Me.txtdescdepartment.Size = New System.Drawing.Size(59, 21)
        Me.txtdescdepartment.TabIndex = 94
        Me.txtdescdepartment.Visible = False
        '
        'temp_add2
        '
        Me.temp_add2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_add2.Location = New System.Drawing.Point(84, 133)
        Me.temp_add2.Name = "temp_add2"
        Me.temp_add2.Size = New System.Drawing.Size(25, 21)
        Me.temp_add2.TabIndex = 10
        Me.temp_add2.Visible = False
        '
        'txtdescdivision
        '
        Me.txtdescdivision.Location = New System.Drawing.Point(166, 113)
        Me.txtdescdivision.Name = "txtdescdivision"
        Me.txtdescdivision.Size = New System.Drawing.Size(59, 21)
        Me.txtdescdivision.TabIndex = 93
        Me.txtdescdivision.Visible = False
        '
        'txtweight
        '
        Me.txtweight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtweight.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtweight.Location = New System.Drawing.Point(202, 134)
        Me.txtweight.MaxLength = 10
        Me.txtweight.Name = "txtweight"
        Me.txtweight.Size = New System.Drawing.Size(32, 21)
        Me.txtweight.TabIndex = 8
        '
        'txtkeydepartment
        '
        Me.txtkeydepartment.Location = New System.Drawing.Point(132, 159)
        Me.txtkeydepartment.Name = "txtkeydepartment"
        Me.txtkeydepartment.Size = New System.Drawing.Size(28, 21)
        Me.txtkeydepartment.TabIndex = 92
        Me.txtkeydepartment.Visible = False
        '
        'txtdateregular
        '
        Me.txtdateregular.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtdateregular.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtdateregular.Location = New System.Drawing.Point(9, 91)
        Me.txtdateregular.Name = "txtdateregular"
        Me.txtdateregular.Size = New System.Drawing.Size(69, 21)
        Me.txtdateregular.TabIndex = 57
        Me.txtdateregular.Visible = False
        '
        'txtkeydivision
        '
        Me.txtkeydivision.Location = New System.Drawing.Point(132, 136)
        Me.txtkeydivision.Name = "txtkeydivision"
        Me.txtkeydivision.Size = New System.Drawing.Size(28, 21)
        Me.txtkeydivision.TabIndex = 91
        Me.txtkeydivision.Visible = False
        '
        'Dateregular
        '
        Me.Dateregular.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Dateregular.Location = New System.Drawing.Point(84, 91)
        Me.Dateregular.Name = "Dateregular"
        Me.Dateregular.Size = New System.Drawing.Size(21, 21)
        Me.Dateregular.TabIndex = 49
        Me.Dateregular.Value = New Date(2006, 7, 17, 0, 0, 0, 0)
        Me.Dateregular.Visible = False
        '
        'txtkeycompany
        '
        Me.txtkeycompany.Location = New System.Drawing.Point(132, 113)
        Me.txtkeycompany.Name = "txtkeycompany"
        Me.txtkeycompany.Size = New System.Drawing.Size(28, 21)
        Me.txtkeycompany.TabIndex = 90
        Me.txtkeycompany.Visible = False
        '
        'emp_extphone
        '
        Me.emp_extphone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.emp_extphone.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.emp_extphone.Location = New System.Drawing.Point(191, 89)
        Me.emp_extphone.Name = "emp_extphone"
        Me.emp_extphone.Size = New System.Drawing.Size(45, 21)
        Me.emp_extphone.TabIndex = 7
        '
        'cbosalarygrade
        '
        Me.cbosalarygrade.FormattingEnabled = True
        Me.cbosalarygrade.Location = New System.Drawing.Point(84, 86)
        Me.cbosalarygrade.Name = "cbosalarygrade"
        Me.cbosalarygrade.Size = New System.Drawing.Size(156, 23)
        Me.cbosalarygrade.TabIndex = 7
        '
        'temp_citizen
        '
        Me.temp_citizen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_citizen.Location = New System.Drawing.Point(97, 110)
        Me.temp_citizen.MaxLength = 50
        Me.temp_citizen.Name = "temp_citizen"
        Me.temp_citizen.Size = New System.Drawing.Size(137, 21)
        Me.temp_citizen.TabIndex = 6
        '
        'temp_designation
        '
        Me.temp_designation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_designation.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.temp_designation.Location = New System.Drawing.Point(67, 95)
        Me.temp_designation.Name = "temp_designation"
        Me.temp_designation.Size = New System.Drawing.Size(25, 21)
        Me.temp_designation.TabIndex = 16
        Me.temp_designation.Visible = False
        '
        'txtType_employee
        '
        Me.txtType_employee.Location = New System.Drawing.Point(118, 53)
        Me.txtType_employee.Name = "txtType_employee"
        Me.txtType_employee.Size = New System.Drawing.Size(42, 21)
        Me.txtType_employee.TabIndex = 80
        Me.txtType_employee.Visible = False
        '
        'txtfxkeypositionlevel
        '
        Me.txtfxkeypositionlevel.Location = New System.Drawing.Point(118, 76)
        Me.txtfxkeypositionlevel.Name = "txtfxkeypositionlevel"
        Me.txtfxkeypositionlevel.Size = New System.Drawing.Size(42, 21)
        Me.txtfxkeypositionlevel.TabIndex = 81
        Me.txtfxkeypositionlevel.Visible = False
        '
        'txtfullname
        '
        Me.txtfullname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtfullname.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfullname.Location = New System.Drawing.Point(99, 95)
        Me.txtfullname.MaxLength = 50
        Me.txtfullname.Name = "txtfullname"
        Me.txtfullname.Size = New System.Drawing.Size(22, 21)
        Me.txtfullname.TabIndex = 62
        Me.txtfullname.Visible = False
        '
        'TXTRECID
        '
        Me.TXTRECID.Location = New System.Drawing.Point(145, 65)
        Me.TXTRECID.Name = "TXTRECID"
        Me.TXTRECID.Size = New System.Drawing.Size(72, 21)
        Me.TXTRECID.TabIndex = 52
        Me.TXTRECID.Visible = False
        '
        'cbodept
        '
        Me.cbodept.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.cbodept.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbodept.Location = New System.Drawing.Point(100, 123)
        Me.cbodept.MaxLength = 50
        Me.cbodept.Name = "cbodept"
        Me.cbodept.Size = New System.Drawing.Size(22, 21)
        Me.cbodept.TabIndex = 72
        Me.cbodept.Visible = False
        '
        'Dateresigned
        '
        Me.Dateresigned.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Dateresigned.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Dateresigned.Location = New System.Drawing.Point(67, 122)
        Me.Dateresigned.Name = "Dateresigned"
        Me.Dateresigned.Size = New System.Drawing.Size(28, 21)
        Me.Dateresigned.TabIndex = 58
        Me.Dateresigned.Value = New Date(2006, 5, 30, 0, 0, 0, 0)
        Me.Dateresigned.Visible = False
        '
        'TXTKEYEMPLOYEEID
        '
        Me.TXTKEYEMPLOYEEID.Location = New System.Drawing.Point(145, 89)
        Me.TXTKEYEMPLOYEEID.Name = "TXTKEYEMPLOYEEID"
        Me.TXTKEYEMPLOYEEID.Size = New System.Drawing.Size(72, 21)
        Me.TXTKEYEMPLOYEEID.TabIndex = 51
        Me.TXTKEYEMPLOYEEID.Visible = False
        '
        'btnemp_editdepdts
        '
        Me.btnemp_editdepdts.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_editdepdts.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnemp_editdepdts.Image = CType(resources.GetObject("btnemp_editdepdts.Image"), System.Drawing.Image)
        Me.btnemp_editdepdts.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_editdepdts.Location = New System.Drawing.Point(783, 246)
        Me.btnemp_editdepdts.Name = "btnemp_editdepdts"
        Me.btnemp_editdepdts.Size = New System.Drawing.Size(68, 24)
        Me.btnemp_editdepdts.TabIndex = 128
        Me.btnemp_editdepdts.Text = "Edit"
        Me.btnemp_editdepdts.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_editdepdts.UseVisualStyleBackColor = True
        '
        'btndelete_dep
        '
        Me.btndelete_dep.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btndelete_dep.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btndelete_dep.Image = CType(resources.GetObject("btndelete_dep.Image"), System.Drawing.Image)
        Me.btndelete_dep.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btndelete_dep.Location = New System.Drawing.Point(851, 246)
        Me.btndelete_dep.Name = "btndelete_dep"
        Me.btndelete_dep.Size = New System.Drawing.Size(68, 24)
        Me.btndelete_dep.TabIndex = 129
        Me.btndelete_dep.Text = "Delete"
        Me.btndelete_dep.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btndelete_dep.UseVisualStyleBackColor = True
        '
        'btnadd_dep
        '
        Me.btnadd_dep.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnadd_dep.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnadd_dep.Image = CType(resources.GetObject("btnadd_dep.Image"), System.Drawing.Image)
        Me.btnadd_dep.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnadd_dep.Location = New System.Drawing.Point(715, 246)
        Me.btnadd_dep.Name = "btnadd_dep"
        Me.btnadd_dep.Size = New System.Drawing.Size(68, 24)
        Me.btnadd_dep.TabIndex = 127
        Me.btnadd_dep.Text = "New"
        Me.btnadd_dep.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnadd_dep.UseVisualStyleBackColor = True
        '
        'PictureBox5
        '
        Me.PictureBox5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox5.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox5.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(1029, 20)
        Me.PictureBox5.TabIndex = 1
        Me.PictureBox5.TabStop = False
        '
        'Benefeciary
        '
        Me.Benefeciary.BackColor = System.Drawing.SystemColors.Window
        Me.Benefeciary.Controls.Add(Me.BtnEditContact)
        Me.Benefeciary.Controls.Add(Me.BtnDeleteContact)
        Me.Benefeciary.Controls.Add(Me.btnaddContact)
        Me.Benefeciary.Controls.Add(Me.lvlNearestRelatives)
        Me.Benefeciary.Controls.Add(Me.Label98)
        Me.Benefeciary.Controls.Add(Me.PictureBox1)
        Me.Benefeciary.Location = New System.Drawing.Point(4, 22)
        Me.Benefeciary.Name = "Benefeciary"
        Me.Benefeciary.Size = New System.Drawing.Size(1003, 275)
        Me.Benefeciary.TabIndex = 17
        Me.Benefeciary.Text = "Beneficiaries"
        Me.Benefeciary.UseVisualStyleBackColor = True
        '
        'BtnEditContact
        '
        Me.BtnEditContact.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnEditContact.Image = CType(resources.GetObject("BtnEditContact.Image"), System.Drawing.Image)
        Me.BtnEditContact.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnEditContact.Location = New System.Drawing.Point(790, 245)
        Me.BtnEditContact.Name = "BtnEditContact"
        Me.BtnEditContact.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BtnEditContact.Size = New System.Drawing.Size(68, 24)
        Me.BtnEditContact.TabIndex = 131
        Me.BtnEditContact.Text = "Edit"
        Me.BtnEditContact.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnEditContact.UseVisualStyleBackColor = True
        '
        'BtnDeleteContact
        '
        Me.BtnDeleteContact.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnDeleteContact.Image = CType(resources.GetObject("BtnDeleteContact.Image"), System.Drawing.Image)
        Me.BtnDeleteContact.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnDeleteContact.Location = New System.Drawing.Point(859, 245)
        Me.BtnDeleteContact.Name = "BtnDeleteContact"
        Me.BtnDeleteContact.Size = New System.Drawing.Size(68, 24)
        Me.BtnDeleteContact.TabIndex = 132
        Me.BtnDeleteContact.Text = "Delete"
        Me.BtnDeleteContact.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnDeleteContact.UseVisualStyleBackColor = True
        '
        'btnaddContact
        '
        Me.btnaddContact.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnaddContact.Image = CType(resources.GetObject("btnaddContact.Image"), System.Drawing.Image)
        Me.btnaddContact.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnaddContact.Location = New System.Drawing.Point(721, 245)
        Me.btnaddContact.Name = "btnaddContact"
        Me.btnaddContact.Size = New System.Drawing.Size(68, 24)
        Me.btnaddContact.TabIndex = 130
        Me.btnaddContact.Text = "New"
        Me.btnaddContact.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnaddContact.UseVisualStyleBackColor = True
        '
        'lvlNearestRelatives
        '
        Me.lvlNearestRelatives.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlNearestRelatives.Location = New System.Drawing.Point(8, 27)
        Me.lvlNearestRelatives.Name = "lvlNearestRelatives"
        Me.lvlNearestRelatives.Size = New System.Drawing.Size(919, 212)
        Me.lvlNearestRelatives.TabIndex = 4
        Me.lvlNearestRelatives.UseCompatibleStateImageBehavior = False
        '
        'Label98
        '
        Me.Label98.AutoSize = True
        Me.Label98.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label98.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label98.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label98.Location = New System.Drawing.Point(5, 2)
        Me.Label98.Name = "Label98"
        Me.Label98.Size = New System.Drawing.Size(153, 18)
        Me.Label98.TabIndex = 3
        Me.Label98.Text = "Employee Beneficiaries"
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(1028, 21)
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'EducationalInfo
        '
        Me.EducationalInfo.BackColor = System.Drawing.Color.White
        Me.EducationalInfo.Controls.Add(Me.Label70)
        Me.EducationalInfo.Controls.Add(Me.btnUpdateEI)
        Me.EducationalInfo.Controls.Add(Me.btnDeleteEI)
        Me.EducationalInfo.Controls.Add(Me.Label64)
        Me.EducationalInfo.Controls.Add(Me.btnNewEI)
        Me.EducationalInfo.Controls.Add(Me.PictureBox10)
        Me.EducationalInfo.Controls.Add(Me.lvlEducInfo)
        Me.EducationalInfo.Location = New System.Drawing.Point(4, 22)
        Me.EducationalInfo.Name = "EducationalInfo"
        Me.EducationalInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.EducationalInfo.Size = New System.Drawing.Size(1003, 275)
        Me.EducationalInfo.TabIndex = 22
        Me.EducationalInfo.Text = "Educational"
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label70.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label70.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label70.Location = New System.Drawing.Point(6, 2)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(155, 18)
        Me.Label70.TabIndex = 29
        Me.Label70.Text = "Educational Background"
        '
        'btnUpdateEI
        '
        Me.btnUpdateEI.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateEI.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateEI.Image = CType(resources.GetObject("btnUpdateEI.Image"), System.Drawing.Image)
        Me.btnUpdateEI.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateEI.Location = New System.Drawing.Point(785, 245)
        Me.btnUpdateEI.Name = "btnUpdateEI"
        Me.btnUpdateEI.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateEI.TabIndex = 134
        Me.btnUpdateEI.Text = "Edit"
        Me.btnUpdateEI.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateEI.UseVisualStyleBackColor = True
        '
        'btnDeleteEI
        '
        Me.btnDeleteEI.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteEI.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteEI.Image = CType(resources.GetObject("btnDeleteEI.Image"), System.Drawing.Image)
        Me.btnDeleteEI.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteEI.Location = New System.Drawing.Point(853, 245)
        Me.btnDeleteEI.Name = "btnDeleteEI"
        Me.btnDeleteEI.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteEI.TabIndex = 135
        Me.btnDeleteEI.Text = "Delete"
        Me.btnDeleteEI.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteEI.UseVisualStyleBackColor = True
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label64.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label64.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label64.Location = New System.Drawing.Point(3, 2)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(0, 18)
        Me.Label64.TabIndex = 25
        '
        'btnNewEI
        '
        Me.btnNewEI.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewEI.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNewEI.Image = CType(resources.GetObject("btnNewEI.Image"), System.Drawing.Image)
        Me.btnNewEI.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewEI.Location = New System.Drawing.Point(717, 245)
        Me.btnNewEI.Name = "btnNewEI"
        Me.btnNewEI.Size = New System.Drawing.Size(68, 24)
        Me.btnNewEI.TabIndex = 133
        Me.btnNewEI.Text = "New"
        Me.btnNewEI.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewEI.UseVisualStyleBackColor = True
        '
        'PictureBox10
        '
        Me.PictureBox10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox10.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox10.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(1050, 22)
        Me.PictureBox10.TabIndex = 24
        Me.PictureBox10.TabStop = False
        '
        'lvlEducInfo
        '
        Me.lvlEducInfo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlEducInfo.Location = New System.Drawing.Point(10, 28)
        Me.lvlEducInfo.Name = "lvlEducInfo"
        Me.lvlEducInfo.Size = New System.Drawing.Size(911, 211)
        Me.lvlEducInfo.TabIndex = 23
        Me.lvlEducInfo.UseCompatibleStateImageBehavior = False
        '
        'EmploymentHistory
        '
        Me.EmploymentHistory.BackColor = System.Drawing.Color.White
        Me.EmploymentHistory.Controls.Add(Me.lvlEmploymentHistory)
        Me.EmploymentHistory.Controls.Add(Me.btnUpdateEH)
        Me.EmploymentHistory.Controls.Add(Me.btnDeleteEH)
        Me.EmploymentHistory.Controls.Add(Me.btnNewEH)
        Me.EmploymentHistory.Controls.Add(Me.Label81)
        Me.EmploymentHistory.Controls.Add(Me.PictureBox21)
        Me.EmploymentHistory.Location = New System.Drawing.Point(4, 22)
        Me.EmploymentHistory.Name = "EmploymentHistory"
        Me.EmploymentHistory.Size = New System.Drawing.Size(1003, 275)
        Me.EmploymentHistory.TabIndex = 33
        Me.EmploymentHistory.Text = "Employment History"
        '
        'lvlEmploymentHistory
        '
        Me.lvlEmploymentHistory.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlEmploymentHistory.Location = New System.Drawing.Point(11, 28)
        Me.lvlEmploymentHistory.Name = "lvlEmploymentHistory"
        Me.lvlEmploymentHistory.Size = New System.Drawing.Size(911, 211)
        Me.lvlEmploymentHistory.TabIndex = 39
        Me.lvlEmploymentHistory.UseCompatibleStateImageBehavior = False
        '
        'btnUpdateEH
        '
        Me.btnUpdateEH.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateEH.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateEH.Image = CType(resources.GetObject("btnUpdateEH.Image"), System.Drawing.Image)
        Me.btnUpdateEH.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateEH.Location = New System.Drawing.Point(786, 245)
        Me.btnUpdateEH.Name = "btnUpdateEH"
        Me.btnUpdateEH.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateEH.TabIndex = 137
        Me.btnUpdateEH.Text = "Edit"
        Me.btnUpdateEH.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateEH.UseVisualStyleBackColor = True
        '
        'btnDeleteEH
        '
        Me.btnDeleteEH.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteEH.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteEH.Image = CType(resources.GetObject("btnDeleteEH.Image"), System.Drawing.Image)
        Me.btnDeleteEH.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteEH.Location = New System.Drawing.Point(854, 245)
        Me.btnDeleteEH.Name = "btnDeleteEH"
        Me.btnDeleteEH.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteEH.TabIndex = 138
        Me.btnDeleteEH.Text = "Delete"
        Me.btnDeleteEH.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteEH.UseVisualStyleBackColor = True
        '
        'btnNewEH
        '
        Me.btnNewEH.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewEH.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNewEH.Image = CType(resources.GetObject("btnNewEH.Image"), System.Drawing.Image)
        Me.btnNewEH.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewEH.Location = New System.Drawing.Point(718, 245)
        Me.btnNewEH.Name = "btnNewEH"
        Me.btnNewEH.Size = New System.Drawing.Size(68, 24)
        Me.btnNewEH.TabIndex = 136
        Me.btnNewEH.Text = "New"
        Me.btnNewEH.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewEH.UseVisualStyleBackColor = True
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label81.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label81.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label81.Location = New System.Drawing.Point(4, 1)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(134, 18)
        Me.Label81.TabIndex = 28
        Me.Label81.Text = "Employment History"
        '
        'PictureBox21
        '
        Me.PictureBox21.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox21.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox21.Location = New System.Drawing.Point(-3, 0)
        Me.PictureBox21.Name = "PictureBox21"
        Me.PictureBox21.Size = New System.Drawing.Size(1050, 22)
        Me.PictureBox21.TabIndex = 27
        Me.PictureBox21.TabStop = False
        '
        'Trainings
        '
        Me.Trainings.BackColor = System.Drawing.Color.White
        Me.Trainings.Controls.Add(Me.btnTDownload)
        Me.Trainings.Controls.Add(Me.Label72)
        Me.Trainings.Controls.Add(Me.btnUpdateT)
        Me.Trainings.Controls.Add(Me.btnDeleteT)
        Me.Trainings.Controls.Add(Me.btnNewT)
        Me.Trainings.Controls.Add(Me.PictureBox12)
        Me.Trainings.Controls.Add(Me.lvlTrainings)
        Me.Trainings.Location = New System.Drawing.Point(4, 22)
        Me.Trainings.Name = "Trainings"
        Me.Trainings.Padding = New System.Windows.Forms.Padding(3)
        Me.Trainings.Size = New System.Drawing.Size(1003, 275)
        Me.Trainings.TabIndex = 24
        Me.Trainings.Text = "Trainings"
        '
        'btnTDownload
        '
        Me.btnTDownload.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnTDownload.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnTDownload.Enabled = False
        Me.btnTDownload.Image = Global.WindowsApplication2.My.Resources.Resources.images__36_
        Me.btnTDownload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnTDownload.Location = New System.Drawing.Point(600, 245)
        Me.btnTDownload.Name = "btnTDownload"
        Me.btnTDownload.Size = New System.Drawing.Size(98, 24)
        Me.btnTDownload.TabIndex = 139
        Me.btnTDownload.Text = "Upload File"
        Me.btnTDownload.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnTDownload.UseVisualStyleBackColor = True
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label72.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label72.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label72.Location = New System.Drawing.Point(3, 2)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(125, 18)
        Me.Label72.TabIndex = 35
        Me.Label72.Text = "Trainings Attended"
        '
        'btnUpdateT
        '
        Me.btnUpdateT.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateT.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateT.Image = CType(resources.GetObject("btnUpdateT.Image"), System.Drawing.Image)
        Me.btnUpdateT.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateT.Location = New System.Drawing.Point(787, 245)
        Me.btnUpdateT.Name = "btnUpdateT"
        Me.btnUpdateT.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateT.TabIndex = 141
        Me.btnUpdateT.Text = "Edit"
        Me.btnUpdateT.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateT.UseVisualStyleBackColor = True
        '
        'btnDeleteT
        '
        Me.btnDeleteT.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteT.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteT.Image = CType(resources.GetObject("btnDeleteT.Image"), System.Drawing.Image)
        Me.btnDeleteT.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteT.Location = New System.Drawing.Point(855, 245)
        Me.btnDeleteT.Name = "btnDeleteT"
        Me.btnDeleteT.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteT.TabIndex = 142
        Me.btnDeleteT.Text = "Delete"
        Me.btnDeleteT.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteT.UseVisualStyleBackColor = True
        '
        'btnNewT
        '
        Me.btnNewT.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewT.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNewT.Image = CType(resources.GetObject("btnNewT.Image"), System.Drawing.Image)
        Me.btnNewT.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewT.Location = New System.Drawing.Point(719, 245)
        Me.btnNewT.Name = "btnNewT"
        Me.btnNewT.Size = New System.Drawing.Size(68, 24)
        Me.btnNewT.TabIndex = 140
        Me.btnNewT.Text = "New"
        Me.btnNewT.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewT.UseVisualStyleBackColor = True
        '
        'PictureBox12
        '
        Me.PictureBox12.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox12.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox12.Location = New System.Drawing.Point(-3, 0)
        Me.PictureBox12.Name = "PictureBox12"
        Me.PictureBox12.Size = New System.Drawing.Size(1050, 22)
        Me.PictureBox12.TabIndex = 31
        Me.PictureBox12.TabStop = False
        '
        'lvlTrainings
        '
        Me.lvlTrainings.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlTrainings.Location = New System.Drawing.Point(10, 29)
        Me.lvlTrainings.Name = "lvlTrainings"
        Me.lvlTrainings.Size = New System.Drawing.Size(913, 210)
        Me.lvlTrainings.TabIndex = 30
        Me.lvlTrainings.UseCompatibleStateImageBehavior = False
        '
        'GovExamination
        '
        Me.GovExamination.BackColor = System.Drawing.Color.White
        Me.GovExamination.Controls.Add(Me.btnUpdateGE)
        Me.GovExamination.Controls.Add(Me.btnDeleteGE)
        Me.GovExamination.Controls.Add(Me.btnNewGE)
        Me.GovExamination.Controls.Add(Me.lvlGovExam)
        Me.GovExamination.Controls.Add(Me.Label91)
        Me.GovExamination.Controls.Add(Me.PictureBox23)
        Me.GovExamination.Location = New System.Drawing.Point(4, 22)
        Me.GovExamination.Name = "GovExamination"
        Me.GovExamination.Padding = New System.Windows.Forms.Padding(3)
        Me.GovExamination.Size = New System.Drawing.Size(1003, 275)
        Me.GovExamination.TabIndex = 36
        Me.GovExamination.Text = "Government Exam"
        '
        'btnUpdateGE
        '
        Me.btnUpdateGE.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateGE.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateGE.Image = CType(resources.GetObject("btnUpdateGE.Image"), System.Drawing.Image)
        Me.btnUpdateGE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateGE.Location = New System.Drawing.Point(784, 245)
        Me.btnUpdateGE.Name = "btnUpdateGE"
        Me.btnUpdateGE.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateGE.TabIndex = 144
        Me.btnUpdateGE.Text = "Edit"
        Me.btnUpdateGE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateGE.UseVisualStyleBackColor = True
        '
        'btnDeleteGE
        '
        Me.btnDeleteGE.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteGE.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteGE.Image = CType(resources.GetObject("btnDeleteGE.Image"), System.Drawing.Image)
        Me.btnDeleteGE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteGE.Location = New System.Drawing.Point(852, 245)
        Me.btnDeleteGE.Name = "btnDeleteGE"
        Me.btnDeleteGE.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteGE.TabIndex = 145
        Me.btnDeleteGE.Text = "Delete"
        Me.btnDeleteGE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteGE.UseVisualStyleBackColor = True
        '
        'btnNewGE
        '
        Me.btnNewGE.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewGE.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNewGE.Image = CType(resources.GetObject("btnNewGE.Image"), System.Drawing.Image)
        Me.btnNewGE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewGE.Location = New System.Drawing.Point(716, 245)
        Me.btnNewGE.Name = "btnNewGE"
        Me.btnNewGE.Size = New System.Drawing.Size(68, 24)
        Me.btnNewGE.TabIndex = 143
        Me.btnNewGE.Text = "New"
        Me.btnNewGE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewGE.UseVisualStyleBackColor = True
        '
        'lvlGovExam
        '
        Me.lvlGovExam.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlGovExam.Location = New System.Drawing.Point(9, 28)
        Me.lvlGovExam.Name = "lvlGovExam"
        Me.lvlGovExam.Size = New System.Drawing.Size(911, 211)
        Me.lvlGovExam.TabIndex = 32
        Me.lvlGovExam.UseCompatibleStateImageBehavior = False
        '
        'Label91
        '
        Me.Label91.AutoSize = True
        Me.Label91.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label91.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label91.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label91.Location = New System.Drawing.Point(6, 2)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(222, 18)
        Me.Label91.TabIndex = 31
        Me.Label91.Text = "Government Examination(s) Taken"
        '
        'PictureBox23
        '
        Me.PictureBox23.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox23.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox23.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox23.Name = "PictureBox23"
        Me.PictureBox23.Size = New System.Drawing.Size(1050, 22)
        Me.PictureBox23.TabIndex = 30
        Me.PictureBox23.TabStop = False
        '
        'Skills
        '
        Me.Skills.BackColor = System.Drawing.Color.White
        Me.Skills.Controls.Add(Me.Label73)
        Me.Skills.Controls.Add(Me.btnUpdateSkills)
        Me.Skills.Controls.Add(Me.btnDeleteSkills)
        Me.Skills.Controls.Add(Me.btnNewSkills)
        Me.Skills.Controls.Add(Me.PictureBox13)
        Me.Skills.Controls.Add(Me.lvlSkills)
        Me.Skills.Location = New System.Drawing.Point(4, 22)
        Me.Skills.Name = "Skills"
        Me.Skills.Padding = New System.Windows.Forms.Padding(3)
        Me.Skills.Size = New System.Drawing.Size(1003, 275)
        Me.Skills.TabIndex = 25
        Me.Skills.Text = "Skills"
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label73.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label73.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label73.Location = New System.Drawing.Point(3, 2)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(40, 18)
        Me.Label73.TabIndex = 35
        Me.Label73.Text = "Skills"
        '
        'btnUpdateSkills
        '
        Me.btnUpdateSkills.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateSkills.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateSkills.Image = CType(resources.GetObject("btnUpdateSkills.Image"), System.Drawing.Image)
        Me.btnUpdateSkills.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateSkills.Location = New System.Drawing.Point(792, 245)
        Me.btnUpdateSkills.Name = "btnUpdateSkills"
        Me.btnUpdateSkills.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateSkills.TabIndex = 147
        Me.btnUpdateSkills.Text = "Edit"
        Me.btnUpdateSkills.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateSkills.UseVisualStyleBackColor = True
        '
        'btnDeleteSkills
        '
        Me.btnDeleteSkills.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteSkills.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteSkills.Image = CType(resources.GetObject("btnDeleteSkills.Image"), System.Drawing.Image)
        Me.btnDeleteSkills.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteSkills.Location = New System.Drawing.Point(860, 245)
        Me.btnDeleteSkills.Name = "btnDeleteSkills"
        Me.btnDeleteSkills.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteSkills.TabIndex = 148
        Me.btnDeleteSkills.Text = "Delete"
        Me.btnDeleteSkills.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteSkills.UseVisualStyleBackColor = True
        '
        'btnNewSkills
        '
        Me.btnNewSkills.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewSkills.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNewSkills.Image = CType(resources.GetObject("btnNewSkills.Image"), System.Drawing.Image)
        Me.btnNewSkills.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewSkills.Location = New System.Drawing.Point(724, 245)
        Me.btnNewSkills.Name = "btnNewSkills"
        Me.btnNewSkills.Size = New System.Drawing.Size(68, 24)
        Me.btnNewSkills.TabIndex = 146
        Me.btnNewSkills.Text = "New"
        Me.btnNewSkills.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewSkills.UseVisualStyleBackColor = True
        '
        'PictureBox13
        '
        Me.PictureBox13.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox13.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox13.Location = New System.Drawing.Point(-3, 0)
        Me.PictureBox13.Name = "PictureBox13"
        Me.PictureBox13.Size = New System.Drawing.Size(1050, 22)
        Me.PictureBox13.TabIndex = 31
        Me.PictureBox13.TabStop = False
        '
        'lvlSkills
        '
        Me.lvlSkills.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlSkills.Location = New System.Drawing.Point(6, 29)
        Me.lvlSkills.Name = "lvlSkills"
        Me.lvlSkills.Size = New System.Drawing.Size(922, 210)
        Me.lvlSkills.TabIndex = 30
        Me.lvlSkills.UseCompatibleStateImageBehavior = False
        '
        'Organizations
        '
        Me.Organizations.BackColor = System.Drawing.Color.White
        Me.Organizations.Controls.Add(Me.btnUpdateOrg)
        Me.Organizations.Controls.Add(Me.btnDeleteOrg)
        Me.Organizations.Controls.Add(Me.btnNewOrg)
        Me.Organizations.Controls.Add(Me.lvlOrg)
        Me.Organizations.Controls.Add(Me.Label104)
        Me.Organizations.Controls.Add(Me.PictureBox24)
        Me.Organizations.Location = New System.Drawing.Point(4, 22)
        Me.Organizations.Name = "Organizations"
        Me.Organizations.Size = New System.Drawing.Size(1003, 275)
        Me.Organizations.TabIndex = 37
        Me.Organizations.Text = "Organizations"
        '
        'btnUpdateOrg
        '
        Me.btnUpdateOrg.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateOrg.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateOrg.Image = CType(resources.GetObject("btnUpdateOrg.Image"), System.Drawing.Image)
        Me.btnUpdateOrg.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateOrg.Location = New System.Drawing.Point(796, 245)
        Me.btnUpdateOrg.Name = "btnUpdateOrg"
        Me.btnUpdateOrg.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateOrg.TabIndex = 150
        Me.btnUpdateOrg.Text = "Edit"
        Me.btnUpdateOrg.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateOrg.UseVisualStyleBackColor = True
        '
        'btnDeleteOrg
        '
        Me.btnDeleteOrg.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteOrg.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteOrg.Image = CType(resources.GetObject("btnDeleteOrg.Image"), System.Drawing.Image)
        Me.btnDeleteOrg.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteOrg.Location = New System.Drawing.Point(864, 245)
        Me.btnDeleteOrg.Name = "btnDeleteOrg"
        Me.btnDeleteOrg.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteOrg.TabIndex = 151
        Me.btnDeleteOrg.Text = "Delete"
        Me.btnDeleteOrg.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteOrg.UseVisualStyleBackColor = True
        '
        'btnNewOrg
        '
        Me.btnNewOrg.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewOrg.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNewOrg.Image = CType(resources.GetObject("btnNewOrg.Image"), System.Drawing.Image)
        Me.btnNewOrg.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewOrg.Location = New System.Drawing.Point(728, 245)
        Me.btnNewOrg.Name = "btnNewOrg"
        Me.btnNewOrg.Size = New System.Drawing.Size(68, 24)
        Me.btnNewOrg.TabIndex = 149
        Me.btnNewOrg.Text = "New"
        Me.btnNewOrg.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewOrg.UseVisualStyleBackColor = True
        '
        'lvlOrg
        '
        Me.lvlOrg.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlOrg.Location = New System.Drawing.Point(10, 28)
        Me.lvlOrg.Name = "lvlOrg"
        Me.lvlOrg.Size = New System.Drawing.Size(922, 211)
        Me.lvlOrg.TabIndex = 35
        Me.lvlOrg.UseCompatibleStateImageBehavior = False
        '
        'Label104
        '
        Me.Label104.AutoSize = True
        Me.Label104.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label104.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label104.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label104.Location = New System.Drawing.Point(7, 1)
        Me.Label104.Name = "Label104"
        Me.Label104.Size = New System.Drawing.Size(166, 18)
        Me.Label104.TabIndex = 30
        Me.Label104.Text = "Organizations/Affiliations"
        '
        'PictureBox24
        '
        Me.PictureBox24.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox24.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox24.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox24.Name = "PictureBox24"
        Me.PictureBox24.Size = New System.Drawing.Size(1050, 22)
        Me.PictureBox24.TabIndex = 29
        Me.PictureBox24.TabStop = False
        '
        'Medical
        '
        Me.Medical.BackColor = System.Drawing.Color.White
        Me.Medical.Controls.Add(Me.btnMedUpload)
        Me.Medical.Controls.Add(Me.Label77)
        Me.Medical.Controls.Add(Me.btnUpdateMed)
        Me.Medical.Controls.Add(Me.btnDeleteMed)
        Me.Medical.Controls.Add(Me.btnNewMed)
        Me.Medical.Controls.Add(Me.PictureBox17)
        Me.Medical.Controls.Add(Me.lvlMedical)
        Me.Medical.Location = New System.Drawing.Point(4, 22)
        Me.Medical.Name = "Medical"
        Me.Medical.Padding = New System.Windows.Forms.Padding(3)
        Me.Medical.Size = New System.Drawing.Size(1003, 275)
        Me.Medical.TabIndex = 29
        Me.Medical.Text = "Medical"
        '
        'btnMedUpload
        '
        Me.btnMedUpload.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMedUpload.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnMedUpload.Enabled = False
        Me.btnMedUpload.Image = Global.WindowsApplication2.My.Resources.Resources.images__36_
        Me.btnMedUpload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMedUpload.Location = New System.Drawing.Point(577, 245)
        Me.btnMedUpload.Name = "btnMedUpload"
        Me.btnMedUpload.Size = New System.Drawing.Size(97, 24)
        Me.btnMedUpload.TabIndex = 152
        Me.btnMedUpload.Text = "Upload File"
        Me.btnMedUpload.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMedUpload.UseVisualStyleBackColor = True
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label77.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label77.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label77.Location = New System.Drawing.Point(3, 2)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(58, 18)
        Me.Label77.TabIndex = 35
        Me.Label77.Text = "Medical"
        '
        'btnUpdateMed
        '
        Me.btnUpdateMed.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateMed.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateMed.Image = CType(resources.GetObject("btnUpdateMed.Image"), System.Drawing.Image)
        Me.btnUpdateMed.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateMed.Location = New System.Drawing.Point(788, 245)
        Me.btnUpdateMed.Name = "btnUpdateMed"
        Me.btnUpdateMed.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateMed.TabIndex = 154
        Me.btnUpdateMed.Text = "Edit"
        Me.btnUpdateMed.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateMed.UseVisualStyleBackColor = True
        '
        'btnDeleteMed
        '
        Me.btnDeleteMed.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteMed.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteMed.Image = CType(resources.GetObject("btnDeleteMed.Image"), System.Drawing.Image)
        Me.btnDeleteMed.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteMed.Location = New System.Drawing.Point(856, 245)
        Me.btnDeleteMed.Name = "btnDeleteMed"
        Me.btnDeleteMed.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteMed.TabIndex = 155
        Me.btnDeleteMed.Text = "Delete"
        Me.btnDeleteMed.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteMed.UseVisualStyleBackColor = True
        '
        'btnNewMed
        '
        Me.btnNewMed.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewMed.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNewMed.Image = CType(resources.GetObject("btnNewMed.Image"), System.Drawing.Image)
        Me.btnNewMed.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewMed.Location = New System.Drawing.Point(720, 245)
        Me.btnNewMed.Name = "btnNewMed"
        Me.btnNewMed.Size = New System.Drawing.Size(68, 24)
        Me.btnNewMed.TabIndex = 153
        Me.btnNewMed.Text = "New"
        Me.btnNewMed.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewMed.UseVisualStyleBackColor = True
        '
        'PictureBox17
        '
        Me.PictureBox17.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox17.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox17.Location = New System.Drawing.Point(-3, 0)
        Me.PictureBox17.Name = "PictureBox17"
        Me.PictureBox17.Size = New System.Drawing.Size(1050, 22)
        Me.PictureBox17.TabIndex = 31
        Me.PictureBox17.TabStop = False
        '
        'lvlMedical
        '
        Me.lvlMedical.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlMedical.Location = New System.Drawing.Point(10, 30)
        Me.lvlMedical.Name = "lvlMedical"
        Me.lvlMedical.Size = New System.Drawing.Size(914, 209)
        Me.lvlMedical.TabIndex = 30
        Me.lvlMedical.UseCompatibleStateImageBehavior = False
        '
        'BankInfo
        '
        Me.BankInfo.BackColor = System.Drawing.SystemColors.Window
        Me.BankInfo.Controls.Add(Me.Label35)
        Me.BankInfo.Controls.Add(Me.btnUpdateBA)
        Me.BankInfo.Controls.Add(Me.btnDeleteBA)
        Me.BankInfo.Controls.Add(Me.btnNewBA)
        Me.BankInfo.Controls.Add(Me.lvlBankInfo)
        Me.BankInfo.Controls.Add(Me.PictureBox4)
        Me.BankInfo.Location = New System.Drawing.Point(4, 22)
        Me.BankInfo.Name = "BankInfo"
        Me.BankInfo.Size = New System.Drawing.Size(1003, 275)
        Me.BankInfo.TabIndex = 18
        Me.BankInfo.Text = "Bank"
        Me.BankInfo.UseVisualStyleBackColor = True
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label35.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label35.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label35.Location = New System.Drawing.Point(6, 1)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(115, 18)
        Me.Label35.TabIndex = 26
        Me.Label35.Text = "Bank Information"
        '
        'btnUpdateBA
        '
        Me.btnUpdateBA.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateBA.Image = CType(resources.GetObject("btnUpdateBA.Image"), System.Drawing.Image)
        Me.btnUpdateBA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateBA.Location = New System.Drawing.Point(786, 246)
        Me.btnUpdateBA.Name = "btnUpdateBA"
        Me.btnUpdateBA.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnUpdateBA.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateBA.TabIndex = 157
        Me.btnUpdateBA.Text = "Edit"
        Me.btnUpdateBA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateBA.UseVisualStyleBackColor = True
        '
        'btnDeleteBA
        '
        Me.btnDeleteBA.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteBA.Image = CType(resources.GetObject("btnDeleteBA.Image"), System.Drawing.Image)
        Me.btnDeleteBA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteBA.Location = New System.Drawing.Point(855, 245)
        Me.btnDeleteBA.Name = "btnDeleteBA"
        Me.btnDeleteBA.Size = New System.Drawing.Size(68, 25)
        Me.btnDeleteBA.TabIndex = 158
        Me.btnDeleteBA.Text = "Delete"
        Me.btnDeleteBA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteBA.UseVisualStyleBackColor = True
        '
        'btnNewBA
        '
        Me.btnNewBA.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewBA.Image = CType(resources.GetObject("btnNewBA.Image"), System.Drawing.Image)
        Me.btnNewBA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewBA.Location = New System.Drawing.Point(715, 246)
        Me.btnNewBA.Name = "btnNewBA"
        Me.btnNewBA.Size = New System.Drawing.Size(68, 24)
        Me.btnNewBA.TabIndex = 156
        Me.btnNewBA.Text = "New"
        Me.btnNewBA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewBA.UseVisualStyleBackColor = True
        '
        'lvlBankInfo
        '
        Me.lvlBankInfo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlBankInfo.Location = New System.Drawing.Point(11, 29)
        Me.lvlBankInfo.Name = "lvlBankInfo"
        Me.lvlBankInfo.Size = New System.Drawing.Size(913, 211)
        Me.lvlBankInfo.TabIndex = 22
        Me.lvlBankInfo.UseCompatibleStateImageBehavior = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox4.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox4.Location = New System.Drawing.Point(-1, 0)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(1068, 22)
        Me.PictureBox4.TabIndex = 20
        Me.PictureBox4.TabStop = False
        '
        'SOIInfo
        '
        Me.SOIInfo.BackColor = System.Drawing.SystemColors.Window
        Me.SOIInfo.Controls.Add(Me.btnUpdateSInc)
        Me.SOIInfo.Controls.Add(Me.btnDeleteSInc)
        Me.SOIInfo.Controls.Add(Me.btnsaveSInc)
        Me.SOIInfo.Controls.Add(Me.lvlSourceIncome)
        Me.SOIInfo.Controls.Add(Me.Label36)
        Me.SOIInfo.Controls.Add(Me.PictureBox6)
        Me.SOIInfo.Location = New System.Drawing.Point(4, 22)
        Me.SOIInfo.Name = "SOIInfo"
        Me.SOIInfo.Size = New System.Drawing.Size(1003, 275)
        Me.SOIInfo.TabIndex = 19
        Me.SOIInfo.Text = "Source of Income"
        Me.SOIInfo.UseVisualStyleBackColor = True
        '
        'btnUpdateSInc
        '
        Me.btnUpdateSInc.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateSInc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateSInc.Image = CType(resources.GetObject("btnUpdateSInc.Image"), System.Drawing.Image)
        Me.btnUpdateSInc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateSInc.Location = New System.Drawing.Point(787, 245)
        Me.btnUpdateSInc.Name = "btnUpdateSInc"
        Me.btnUpdateSInc.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateSInc.TabIndex = 160
        Me.btnUpdateSInc.Text = "Edit"
        Me.btnUpdateSInc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateSInc.UseVisualStyleBackColor = True
        '
        'btnDeleteSInc
        '
        Me.btnDeleteSInc.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteSInc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteSInc.Image = CType(resources.GetObject("btnDeleteSInc.Image"), System.Drawing.Image)
        Me.btnDeleteSInc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteSInc.Location = New System.Drawing.Point(856, 245)
        Me.btnDeleteSInc.Name = "btnDeleteSInc"
        Me.btnDeleteSInc.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteSInc.TabIndex = 161
        Me.btnDeleteSInc.Text = "Delete"
        Me.btnDeleteSInc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteSInc.UseVisualStyleBackColor = True
        '
        'btnsaveSInc
        '
        Me.btnsaveSInc.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnsaveSInc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnsaveSInc.Image = CType(resources.GetObject("btnsaveSInc.Image"), System.Drawing.Image)
        Me.btnsaveSInc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsaveSInc.Location = New System.Drawing.Point(718, 245)
        Me.btnsaveSInc.Name = "btnsaveSInc"
        Me.btnsaveSInc.Size = New System.Drawing.Size(68, 24)
        Me.btnsaveSInc.TabIndex = 159
        Me.btnsaveSInc.Text = "New"
        Me.btnsaveSInc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsaveSInc.UseVisualStyleBackColor = True
        '
        'lvlSourceIncome
        '
        Me.lvlSourceIncome.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlSourceIncome.Location = New System.Drawing.Point(10, 28)
        Me.lvlSourceIncome.Name = "lvlSourceIncome"
        Me.lvlSourceIncome.Size = New System.Drawing.Size(914, 211)
        Me.lvlSourceIncome.TabIndex = 25
        Me.lvlSourceIncome.UseCompatibleStateImageBehavior = False
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label36.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label36.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label36.Location = New System.Drawing.Point(7, 2)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(115, 18)
        Me.Label36.TabIndex = 24
        Me.Label36.Text = "Source of Income"
        '
        'PictureBox6
        '
        Me.PictureBox6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox6.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox6.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(1006, 22)
        Me.PictureBox6.TabIndex = 23
        Me.PictureBox6.TabStop = False
        '
        'Discipline
        '
        Me.Discipline.BackColor = System.Drawing.Color.White
        Me.Discipline.Controls.Add(Me.btnDDownload)
        Me.Discipline.Controls.Add(Me.Label76)
        Me.Discipline.Controls.Add(Me.btnUpdateDisc)
        Me.Discipline.Controls.Add(Me.btnDeleteDisc)
        Me.Discipline.Controls.Add(Me.btnNewDisc)
        Me.Discipline.Controls.Add(Me.PictureBox16)
        Me.Discipline.Controls.Add(Me.lvlDiscipline)
        Me.Discipline.Location = New System.Drawing.Point(4, 22)
        Me.Discipline.Name = "Discipline"
        Me.Discipline.Padding = New System.Windows.Forms.Padding(3)
        Me.Discipline.Size = New System.Drawing.Size(1003, 275)
        Me.Discipline.TabIndex = 28
        Me.Discipline.Text = "Discipline"
        '
        'btnDDownload
        '
        Me.btnDDownload.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDDownload.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDDownload.Enabled = False
        Me.btnDDownload.Image = Global.WindowsApplication2.My.Resources.Resources.images__36_
        Me.btnDDownload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDDownload.Location = New System.Drawing.Point(595, 246)
        Me.btnDDownload.Name = "btnDDownload"
        Me.btnDDownload.Size = New System.Drawing.Size(98, 24)
        Me.btnDDownload.TabIndex = 162
        Me.btnDDownload.Text = "Upload File"
        Me.btnDDownload.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDDownload.UseVisualStyleBackColor = True
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label76.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label76.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label76.Location = New System.Drawing.Point(3, 2)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(118, 18)
        Me.Label76.TabIndex = 35
        Me.Label76.Text = "Disciplinary Cases"
        '
        'btnUpdateDisc
        '
        Me.btnUpdateDisc.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateDisc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateDisc.Image = CType(resources.GetObject("btnUpdateDisc.Image"), System.Drawing.Image)
        Me.btnUpdateDisc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateDisc.Location = New System.Drawing.Point(787, 246)
        Me.btnUpdateDisc.Name = "btnUpdateDisc"
        Me.btnUpdateDisc.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateDisc.TabIndex = 164
        Me.btnUpdateDisc.Text = "Edit"
        Me.btnUpdateDisc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateDisc.UseVisualStyleBackColor = True
        '
        'btnDeleteDisc
        '
        Me.btnDeleteDisc.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteDisc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteDisc.Image = CType(resources.GetObject("btnDeleteDisc.Image"), System.Drawing.Image)
        Me.btnDeleteDisc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteDisc.Location = New System.Drawing.Point(855, 246)
        Me.btnDeleteDisc.Name = "btnDeleteDisc"
        Me.btnDeleteDisc.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteDisc.TabIndex = 165
        Me.btnDeleteDisc.Text = "Delete"
        Me.btnDeleteDisc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteDisc.UseVisualStyleBackColor = True
        '
        'btnNewDisc
        '
        Me.btnNewDisc.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewDisc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNewDisc.Image = CType(resources.GetObject("btnNewDisc.Image"), System.Drawing.Image)
        Me.btnNewDisc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewDisc.Location = New System.Drawing.Point(719, 246)
        Me.btnNewDisc.Name = "btnNewDisc"
        Me.btnNewDisc.Size = New System.Drawing.Size(68, 24)
        Me.btnNewDisc.TabIndex = 163
        Me.btnNewDisc.Text = "New"
        Me.btnNewDisc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewDisc.UseVisualStyleBackColor = True
        '
        'PictureBox16
        '
        Me.PictureBox16.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox16.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox16.Location = New System.Drawing.Point(-3, 0)
        Me.PictureBox16.Name = "PictureBox16"
        Me.PictureBox16.Size = New System.Drawing.Size(1050, 22)
        Me.PictureBox16.TabIndex = 31
        Me.PictureBox16.TabStop = False
        '
        'lvlDiscipline
        '
        Me.lvlDiscipline.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlDiscipline.Location = New System.Drawing.Point(10, 30)
        Me.lvlDiscipline.Name = "lvlDiscipline"
        Me.lvlDiscipline.Size = New System.Drawing.Size(913, 210)
        Me.lvlDiscipline.TabIndex = 30
        Me.lvlDiscipline.UseCompatibleStateImageBehavior = False
        '
        'PerfEval
        '
        Me.PerfEval.BackColor = System.Drawing.Color.White
        Me.PerfEval.Controls.Add(Me.btnPEDownload)
        Me.PerfEval.Controls.Add(Me.Label75)
        Me.PerfEval.Controls.Add(Me.btnUpdatePE)
        Me.PerfEval.Controls.Add(Me.btnDeletePE)
        Me.PerfEval.Controls.Add(Me.btnNewPE)
        Me.PerfEval.Controls.Add(Me.PictureBox15)
        Me.PerfEval.Controls.Add(Me.lvlPerfEval)
        Me.PerfEval.Location = New System.Drawing.Point(4, 22)
        Me.PerfEval.Name = "PerfEval"
        Me.PerfEval.Padding = New System.Windows.Forms.Padding(3)
        Me.PerfEval.Size = New System.Drawing.Size(1003, 275)
        Me.PerfEval.TabIndex = 27
        Me.PerfEval.Text = "Perf. Evaluation"
        '
        'btnPEDownload
        '
        Me.btnPEDownload.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPEDownload.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnPEDownload.Enabled = False
        Me.btnPEDownload.Image = Global.WindowsApplication2.My.Resources.Resources.images__36_
        Me.btnPEDownload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPEDownload.Location = New System.Drawing.Point(591, 245)
        Me.btnPEDownload.Name = "btnPEDownload"
        Me.btnPEDownload.Size = New System.Drawing.Size(98, 24)
        Me.btnPEDownload.TabIndex = 166
        Me.btnPEDownload.Text = "Upload File"
        Me.btnPEDownload.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPEDownload.UseVisualStyleBackColor = True
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label75.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label75.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label75.Location = New System.Drawing.Point(3, 2)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(156, 18)
        Me.Label75.TabIndex = 35
        Me.Label75.Text = "Performance Evaluation"
        '
        'btnUpdatePE
        '
        Me.btnUpdatePE.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdatePE.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdatePE.Image = CType(resources.GetObject("btnUpdatePE.Image"), System.Drawing.Image)
        Me.btnUpdatePE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdatePE.Location = New System.Drawing.Point(788, 245)
        Me.btnUpdatePE.Name = "btnUpdatePE"
        Me.btnUpdatePE.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdatePE.TabIndex = 168
        Me.btnUpdatePE.Text = "Edit"
        Me.btnUpdatePE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdatePE.UseVisualStyleBackColor = True
        '
        'btnDeletePE
        '
        Me.btnDeletePE.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeletePE.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeletePE.Image = CType(resources.GetObject("btnDeletePE.Image"), System.Drawing.Image)
        Me.btnDeletePE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeletePE.Location = New System.Drawing.Point(856, 245)
        Me.btnDeletePE.Name = "btnDeletePE"
        Me.btnDeletePE.Size = New System.Drawing.Size(68, 24)
        Me.btnDeletePE.TabIndex = 169
        Me.btnDeletePE.Text = "Delete"
        Me.btnDeletePE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeletePE.UseVisualStyleBackColor = True
        '
        'btnNewPE
        '
        Me.btnNewPE.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewPE.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNewPE.Image = CType(resources.GetObject("btnNewPE.Image"), System.Drawing.Image)
        Me.btnNewPE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewPE.Location = New System.Drawing.Point(720, 245)
        Me.btnNewPE.Name = "btnNewPE"
        Me.btnNewPE.Size = New System.Drawing.Size(68, 24)
        Me.btnNewPE.TabIndex = 167
        Me.btnNewPE.Text = "New"
        Me.btnNewPE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewPE.UseVisualStyleBackColor = True
        '
        'PictureBox15
        '
        Me.PictureBox15.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox15.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox15.Location = New System.Drawing.Point(-3, 0)
        Me.PictureBox15.Name = "PictureBox15"
        Me.PictureBox15.Size = New System.Drawing.Size(1050, 22)
        Me.PictureBox15.TabIndex = 31
        Me.PictureBox15.TabStop = False
        '
        'lvlPerfEval
        '
        Me.lvlPerfEval.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlPerfEval.Location = New System.Drawing.Point(11, 30)
        Me.lvlPerfEval.Name = "lvlPerfEval"
        Me.lvlPerfEval.Size = New System.Drawing.Size(913, 209)
        Me.lvlPerfEval.TabIndex = 30
        Me.lvlPerfEval.UseCompatibleStateImageBehavior = False
        '
        'Legal
        '
        Me.Legal.BackColor = System.Drawing.Color.White
        Me.Legal.Controls.Add(Me.btnLegalUpload)
        Me.Legal.Controls.Add(Me.Label136)
        Me.Legal.Controls.Add(Me.btnUpdateLC)
        Me.Legal.Controls.Add(Me.btnDeleteLC)
        Me.Legal.Controls.Add(Me.btnNewLC)
        Me.Legal.Controls.Add(Me.PictureBox27)
        Me.Legal.Controls.Add(Me.lvlLegal)
        Me.Legal.Location = New System.Drawing.Point(4, 22)
        Me.Legal.Name = "Legal"
        Me.Legal.Size = New System.Drawing.Size(1003, 275)
        Me.Legal.TabIndex = 40
        Me.Legal.Text = "Legal"
        '
        'btnLegalUpload
        '
        Me.btnLegalUpload.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLegalUpload.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnLegalUpload.Image = Global.WindowsApplication2.My.Resources.Resources.images__36_
        Me.btnLegalUpload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLegalUpload.Location = New System.Drawing.Point(588, 244)
        Me.btnLegalUpload.Name = "btnLegalUpload"
        Me.btnLegalUpload.Size = New System.Drawing.Size(100, 24)
        Me.btnLegalUpload.TabIndex = 184
        Me.btnLegalUpload.Text = "Upload File"
        Me.btnLegalUpload.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLegalUpload.UseVisualStyleBackColor = True
        '
        'Label136
        '
        Me.Label136.AutoSize = True
        Me.Label136.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label136.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label136.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label136.Location = New System.Drawing.Point(8, 2)
        Me.Label136.Name = "Label136"
        Me.Label136.Size = New System.Drawing.Size(78, 18)
        Me.Label136.TabIndex = 41
        Me.Label136.Text = "Legal Cases"
        '
        'btnUpdateLC
        '
        Me.btnUpdateLC.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateLC.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateLC.Image = CType(resources.GetObject("btnUpdateLC.Image"), System.Drawing.Image)
        Me.btnUpdateLC.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateLC.Location = New System.Drawing.Point(792, 244)
        Me.btnUpdateLC.Name = "btnUpdateLC"
        Me.btnUpdateLC.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateLC.TabIndex = 171
        Me.btnUpdateLC.Text = "Edit"
        Me.btnUpdateLC.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateLC.UseVisualStyleBackColor = True
        '
        'btnDeleteLC
        '
        Me.btnDeleteLC.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteLC.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteLC.Image = CType(resources.GetObject("btnDeleteLC.Image"), System.Drawing.Image)
        Me.btnDeleteLC.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteLC.Location = New System.Drawing.Point(860, 244)
        Me.btnDeleteLC.Name = "btnDeleteLC"
        Me.btnDeleteLC.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteLC.TabIndex = 172
        Me.btnDeleteLC.Text = "Delete"
        Me.btnDeleteLC.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteLC.UseVisualStyleBackColor = True
        '
        'btnNewLC
        '
        Me.btnNewLC.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewLC.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNewLC.Image = CType(resources.GetObject("btnNewLC.Image"), System.Drawing.Image)
        Me.btnNewLC.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewLC.Location = New System.Drawing.Point(724, 244)
        Me.btnNewLC.Name = "btnNewLC"
        Me.btnNewLC.Size = New System.Drawing.Size(68, 24)
        Me.btnNewLC.TabIndex = 170
        Me.btnNewLC.Text = "New"
        Me.btnNewLC.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewLC.UseVisualStyleBackColor = True
        '
        'PictureBox27
        '
        Me.PictureBox27.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox27.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox27.Location = New System.Drawing.Point(2, 0)
        Me.PictureBox27.Name = "PictureBox27"
        Me.PictureBox27.Size = New System.Drawing.Size(1050, 22)
        Me.PictureBox27.TabIndex = 37
        Me.PictureBox27.TabStop = False
        '
        'lvlLegal
        '
        Me.lvlLegal.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlLegal.Location = New System.Drawing.Point(15, 30)
        Me.lvlLegal.Name = "lvlLegal"
        Me.lvlLegal.Size = New System.Drawing.Size(913, 208)
        Me.lvlLegal.TabIndex = 36
        Me.lvlLegal.UseCompatibleStateImageBehavior = False
        '
        'JobDesc
        '
        Me.JobDesc.BackColor = System.Drawing.Color.White
        Me.JobDesc.Controls.Add(Me.Label71)
        Me.JobDesc.Controls.Add(Me.btnUpdateJD)
        Me.JobDesc.Controls.Add(Me.btnDeleteJD)
        Me.JobDesc.Controls.Add(Me.btnNewJD)
        Me.JobDesc.Controls.Add(Me.PictureBox11)
        Me.JobDesc.Controls.Add(Me.lvlJobDesc)
        Me.JobDesc.Location = New System.Drawing.Point(4, 22)
        Me.JobDesc.Name = "JobDesc"
        Me.JobDesc.Padding = New System.Windows.Forms.Padding(3)
        Me.JobDesc.Size = New System.Drawing.Size(1003, 275)
        Me.JobDesc.TabIndex = 23
        Me.JobDesc.Text = "Job Description"
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label71.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label71.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label71.Location = New System.Drawing.Point(4, 2)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(103, 18)
        Me.Label71.TabIndex = 35
        Me.Label71.Text = "Job Description"
        '
        'btnUpdateJD
        '
        Me.btnUpdateJD.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateJD.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateJD.Image = CType(resources.GetObject("btnUpdateJD.Image"), System.Drawing.Image)
        Me.btnUpdateJD.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateJD.Location = New System.Drawing.Point(790, 245)
        Me.btnUpdateJD.Name = "btnUpdateJD"
        Me.btnUpdateJD.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateJD.TabIndex = 174
        Me.btnUpdateJD.Text = "Edit"
        Me.btnUpdateJD.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateJD.UseVisualStyleBackColor = True
        '
        'btnDeleteJD
        '
        Me.btnDeleteJD.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteJD.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteJD.Image = CType(resources.GetObject("btnDeleteJD.Image"), System.Drawing.Image)
        Me.btnDeleteJD.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteJD.Location = New System.Drawing.Point(858, 245)
        Me.btnDeleteJD.Name = "btnDeleteJD"
        Me.btnDeleteJD.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteJD.TabIndex = 175
        Me.btnDeleteJD.Text = "Delete"
        Me.btnDeleteJD.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteJD.UseVisualStyleBackColor = True
        '
        'btnNewJD
        '
        Me.btnNewJD.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewJD.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNewJD.Image = CType(resources.GetObject("btnNewJD.Image"), System.Drawing.Image)
        Me.btnNewJD.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewJD.Location = New System.Drawing.Point(722, 245)
        Me.btnNewJD.Name = "btnNewJD"
        Me.btnNewJD.Size = New System.Drawing.Size(68, 24)
        Me.btnNewJD.TabIndex = 173
        Me.btnNewJD.Text = "New"
        Me.btnNewJD.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewJD.UseVisualStyleBackColor = True
        '
        'PictureBox11
        '
        Me.PictureBox11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox11.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox11.Location = New System.Drawing.Point(-2, 0)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(1050, 22)
        Me.PictureBox11.TabIndex = 31
        Me.PictureBox11.TabStop = False
        '
        'lvlJobDesc
        '
        Me.lvlJobDesc.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlJobDesc.Location = New System.Drawing.Point(10, 29)
        Me.lvlJobDesc.Name = "lvlJobDesc"
        Me.lvlJobDesc.Size = New System.Drawing.Size(916, 210)
        Me.lvlJobDesc.TabIndex = 30
        Me.lvlJobDesc.UseCompatibleStateImageBehavior = False
        '
        'Awards
        '
        Me.Awards.BackColor = System.Drawing.Color.White
        Me.Awards.Controls.Add(Me.Label74)
        Me.Awards.Controls.Add(Me.btnUpdateAward)
        Me.Awards.Controls.Add(Me.btnDeleteAward)
        Me.Awards.Controls.Add(Me.btnNewAward)
        Me.Awards.Controls.Add(Me.PictureBox14)
        Me.Awards.Controls.Add(Me.lvlAwards)
        Me.Awards.Location = New System.Drawing.Point(4, 22)
        Me.Awards.Name = "Awards"
        Me.Awards.Padding = New System.Windows.Forms.Padding(3)
        Me.Awards.Size = New System.Drawing.Size(1003, 275)
        Me.Awards.TabIndex = 26
        Me.Awards.Text = "Awards"
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label74.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label74.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label74.Location = New System.Drawing.Point(3, 2)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(54, 18)
        Me.Label74.TabIndex = 35
        Me.Label74.Text = "Awards"
        '
        'btnUpdateAward
        '
        Me.btnUpdateAward.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateAward.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateAward.Image = CType(resources.GetObject("btnUpdateAward.Image"), System.Drawing.Image)
        Me.btnUpdateAward.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateAward.Location = New System.Drawing.Point(789, 245)
        Me.btnUpdateAward.Name = "btnUpdateAward"
        Me.btnUpdateAward.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateAward.TabIndex = 179
        Me.btnUpdateAward.Text = "Edit"
        Me.btnUpdateAward.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateAward.UseVisualStyleBackColor = True
        '
        'btnDeleteAward
        '
        Me.btnDeleteAward.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteAward.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteAward.Image = CType(resources.GetObject("btnDeleteAward.Image"), System.Drawing.Image)
        Me.btnDeleteAward.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteAward.Location = New System.Drawing.Point(857, 245)
        Me.btnDeleteAward.Name = "btnDeleteAward"
        Me.btnDeleteAward.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteAward.TabIndex = 180
        Me.btnDeleteAward.Text = "Delete"
        Me.btnDeleteAward.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteAward.UseVisualStyleBackColor = True
        '
        'btnNewAward
        '
        Me.btnNewAward.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewAward.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNewAward.Image = CType(resources.GetObject("btnNewAward.Image"), System.Drawing.Image)
        Me.btnNewAward.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewAward.Location = New System.Drawing.Point(721, 245)
        Me.btnNewAward.Name = "btnNewAward"
        Me.btnNewAward.Size = New System.Drawing.Size(68, 24)
        Me.btnNewAward.TabIndex = 178
        Me.btnNewAward.Text = "New"
        Me.btnNewAward.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewAward.UseVisualStyleBackColor = True
        '
        'PictureBox14
        '
        Me.PictureBox14.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox14.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox14.Location = New System.Drawing.Point(-3, 0)
        Me.PictureBox14.Name = "PictureBox14"
        Me.PictureBox14.Size = New System.Drawing.Size(1050, 22)
        Me.PictureBox14.TabIndex = 31
        Me.PictureBox14.TabStop = False
        '
        'lvlAwards
        '
        Me.lvlAwards.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlAwards.Location = New System.Drawing.Point(30, 29)
        Me.lvlAwards.Name = "lvlAwards"
        Me.lvlAwards.Size = New System.Drawing.Size(895, 210)
        Me.lvlAwards.TabIndex = 30
        Me.lvlAwards.UseCompatibleStateImageBehavior = False
        '
        'Requirement
        '
        Me.Requirement.BackColor = System.Drawing.Color.White
        Me.Requirement.Controls.Add(Me.btnUpload)
        Me.Requirement.Controls.Add(Me.Label167)
        Me.Requirement.Controls.Add(Me.btnAttach)
        Me.Requirement.Controls.Add(Me.PictureBox35)
        Me.Requirement.Controls.Add(Me.lvlAttachment)
        Me.Requirement.Location = New System.Drawing.Point(4, 22)
        Me.Requirement.Name = "Requirement"
        Me.Requirement.Size = New System.Drawing.Size(1003, 275)
        Me.Requirement.TabIndex = 49
        Me.Requirement.Text = "Requirement"
        '
        'btnUpload
        '
        Me.btnUpload.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpload.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpload.Image = Global.WindowsApplication2.My.Resources.Resources.images__36_
        Me.btnUpload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpload.Location = New System.Drawing.Point(813, 244)
        Me.btnUpload.Name = "btnUpload"
        Me.btnUpload.Size = New System.Drawing.Size(100, 26)
        Me.btnUpload.TabIndex = 183
        Me.btnUpload.Text = "Upload File"
        Me.btnUpload.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpload.UseVisualStyleBackColor = True
        '
        'Label167
        '
        Me.Label167.AutoSize = True
        Me.Label167.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label167.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label167.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label167.Location = New System.Drawing.Point(5, 2)
        Me.Label167.Name = "Label167"
        Me.Label167.Size = New System.Drawing.Size(96, 18)
        Me.Label167.TabIndex = 192
        Me.Label167.Text = "Requirements"
        '
        'btnAttach
        '
        Me.btnAttach.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAttach.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnAttach.Image = Global.WindowsApplication2.My.Resources.Resources.images__14_
        Me.btnAttach.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAttach.Location = New System.Drawing.Point(715, 244)
        Me.btnAttach.Name = "btnAttach"
        Me.btnAttach.Size = New System.Drawing.Size(96, 26)
        Me.btnAttach.TabIndex = 182
        Me.btnAttach.Text = "Attach File"
        Me.btnAttach.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAttach.UseVisualStyleBackColor = True
        '
        'PictureBox35
        '
        Me.PictureBox35.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox35.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox35.Location = New System.Drawing.Point(-1, 0)
        Me.PictureBox35.Name = "PictureBox35"
        Me.PictureBox35.Size = New System.Drawing.Size(1050, 22)
        Me.PictureBox35.TabIndex = 191
        Me.PictureBox35.TabStop = False
        '
        'lvlAttachment
        '
        Me.lvlAttachment.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlAttachment.Location = New System.Drawing.Point(8, 28)
        Me.lvlAttachment.Name = "lvlAttachment"
        Me.lvlAttachment.Size = New System.Drawing.Size(905, 211)
        Me.lvlAttachment.TabIndex = 190
        Me.lvlAttachment.UseCompatibleStateImageBehavior = False
        '
        'Contribution
        '
        Me.Contribution.BackColor = System.Drawing.Color.White
        Me.Contribution.Controls.Add(Me.lvlContri)
        Me.Contribution.Controls.Add(Me.cboContribution)
        Me.Contribution.Controls.Add(Me.Label147)
        Me.Contribution.Controls.Add(Me.PictureBox30)
        Me.Contribution.Location = New System.Drawing.Point(4, 22)
        Me.Contribution.Name = "Contribution"
        Me.Contribution.Size = New System.Drawing.Size(1003, 275)
        Me.Contribution.TabIndex = 43
        Me.Contribution.Text = "Contributions"
        '
        'lvlContri
        '
        Me.lvlContri.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlContri.Location = New System.Drawing.Point(10, 55)
        Me.lvlContri.Name = "lvlContri"
        Me.lvlContri.Size = New System.Drawing.Size(919, 217)
        Me.lvlContri.TabIndex = 45
        Me.lvlContri.UseCompatibleStateImageBehavior = False
        '
        'cboContribution
        '
        Me.cboContribution.FormattingEnabled = True
        Me.cboContribution.Items.AddRange(New Object() {"SSS", "PHIC/PhilHealth", "HDMF/Pag-Ibig"})
        Me.cboContribution.Location = New System.Drawing.Point(6, 26)
        Me.cboContribution.Name = "cboContribution"
        Me.cboContribution.Size = New System.Drawing.Size(200, 23)
        Me.cboContribution.TabIndex = 184
        '
        'Label147
        '
        Me.Label147.AutoSize = True
        Me.Label147.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label147.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label147.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label147.Location = New System.Drawing.Point(7, 2)
        Me.Label147.Name = "Label147"
        Me.Label147.Size = New System.Drawing.Size(228, 18)
        Me.Label147.TabIndex = 43
        Me.Label147.Text = "Government Agencies Contribution"
        '
        'PictureBox30
        '
        Me.PictureBox30.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox30.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox30.Location = New System.Drawing.Point(1, 0)
        Me.PictureBox30.Name = "PictureBox30"
        Me.PictureBox30.Size = New System.Drawing.Size(1050, 22)
        Me.PictureBox30.TabIndex = 42
        Me.PictureBox30.TabStop = False
        '
        'OtherDeductions
        '
        Me.OtherDeductions.BackColor = System.Drawing.Color.White
        Me.OtherDeductions.Controls.Add(Me.lvlDeductions)
        Me.OtherDeductions.Controls.Add(Me.Label148)
        Me.OtherDeductions.Controls.Add(Me.PictureBox31)
        Me.OtherDeductions.Location = New System.Drawing.Point(4, 22)
        Me.OtherDeductions.Name = "OtherDeductions"
        Me.OtherDeductions.Size = New System.Drawing.Size(1003, 275)
        Me.OtherDeductions.TabIndex = 44
        Me.OtherDeductions.Text = "Deductions"
        '
        'lvlDeductions
        '
        Me.lvlDeductions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlDeductions.Location = New System.Drawing.Point(10, 28)
        Me.lvlDeductions.Name = "lvlDeductions"
        Me.lvlDeductions.Size = New System.Drawing.Size(916, 244)
        Me.lvlDeductions.TabIndex = 40
        Me.lvlDeductions.UseCompatibleStateImageBehavior = False
        '
        'Label148
        '
        Me.Label148.AutoSize = True
        Me.Label148.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label148.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label148.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label148.Location = New System.Drawing.Point(3, 2)
        Me.Label148.Name = "Label148"
        Me.Label148.Size = New System.Drawing.Size(117, 18)
        Me.Label148.TabIndex = 39
        Me.Label148.Text = "Other Deductions"
        '
        'PictureBox31
        '
        Me.PictureBox31.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox31.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox31.Location = New System.Drawing.Point(-3, 0)
        Me.PictureBox31.Name = "PictureBox31"
        Me.PictureBox31.Size = New System.Drawing.Size(1050, 22)
        Me.PictureBox31.TabIndex = 38
        Me.PictureBox31.TabStop = False
        '
        'MonthPay
        '
        Me.MonthPay.BackColor = System.Drawing.Color.White
        Me.MonthPay.Controls.Add(Me.lvl13thMonth)
        Me.MonthPay.Controls.Add(Me.Label149)
        Me.MonthPay.Controls.Add(Me.PictureBox32)
        Me.MonthPay.Location = New System.Drawing.Point(4, 22)
        Me.MonthPay.Name = "MonthPay"
        Me.MonthPay.Size = New System.Drawing.Size(1003, 275)
        Me.MonthPay.TabIndex = 45
        Me.MonthPay.Text = "13th Month Pay"
        '
        'lvl13thMonth
        '
        Me.lvl13thMonth.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvl13thMonth.Location = New System.Drawing.Point(13, 28)
        Me.lvl13thMonth.Name = "lvl13thMonth"
        Me.lvl13thMonth.Size = New System.Drawing.Size(916, 244)
        Me.lvl13thMonth.TabIndex = 43
        Me.lvl13thMonth.UseCompatibleStateImageBehavior = False
        '
        'Label149
        '
        Me.Label149.AutoSize = True
        Me.Label149.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label149.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label149.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label149.Location = New System.Drawing.Point(6, 2)
        Me.Label149.Name = "Label149"
        Me.Label149.Size = New System.Drawing.Size(105, 18)
        Me.Label149.TabIndex = 42
        Me.Label149.Text = "13th Month Pay"
        '
        'PictureBox32
        '
        Me.PictureBox32.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox32.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox32.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox32.Name = "PictureBox32"
        Me.PictureBox32.Size = New System.Drawing.Size(1050, 22)
        Me.PictureBox32.TabIndex = 41
        Me.PictureBox32.TabStop = False
        '
        'InfoHistory
        '
        Me.InfoHistory.BackColor = System.Drawing.Color.White
        Me.InfoHistory.Controls.Add(Me.Label141)
        Me.InfoHistory.Controls.Add(Me.PictureBox29)
        Me.InfoHistory.Controls.Add(Me.dgvemail)
        Me.InfoHistory.Controls.Add(Me.Label43)
        Me.InfoHistory.Controls.Add(Me.dgvphone)
        Me.InfoHistory.Controls.Add(Me.Label42)
        Me.InfoHistory.Controls.Add(Me.dgvcivil)
        Me.InfoHistory.Controls.Add(Me.Label41)
        Me.InfoHistory.Controls.Add(Me.dgvaddress)
        Me.InfoHistory.Controls.Add(Me.Label40)
        Me.InfoHistory.Controls.Add(Me.dgvlastname)
        Me.InfoHistory.Controls.Add(Me.Label39)
        Me.InfoHistory.Location = New System.Drawing.Point(4, 22)
        Me.InfoHistory.Name = "InfoHistory"
        Me.InfoHistory.Padding = New System.Windows.Forms.Padding(3)
        Me.InfoHistory.Size = New System.Drawing.Size(1003, 275)
        Me.InfoHistory.TabIndex = 21
        Me.InfoHistory.Text = "Info History"
        '
        'Label141
        '
        Me.Label141.AutoSize = True
        Me.Label141.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label141.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label141.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label141.Location = New System.Drawing.Point(6, 2)
        Me.Label141.Name = "Label141"
        Me.Label141.Size = New System.Drawing.Size(129, 18)
        Me.Label141.TabIndex = 37
        Me.Label141.Text = "Information History"
        '
        'PictureBox29
        '
        Me.PictureBox29.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox29.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox29.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox29.Name = "PictureBox29"
        Me.PictureBox29.Size = New System.Drawing.Size(1050, 22)
        Me.PictureBox29.TabIndex = 36
        Me.PictureBox29.TabStop = False
        '
        'dgvemail
        '
        Me.dgvemail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvemail.BackgroundColor = System.Drawing.Color.White
        Me.dgvemail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvemail.Location = New System.Drawing.Point(603, 49)
        Me.dgvemail.Name = "dgvemail"
        Me.dgvemail.ReadOnly = True
        Me.dgvemail.RowHeadersVisible = False
        Me.dgvemail.Size = New System.Drawing.Size(302, 108)
        Me.dgvemail.TabIndex = 9
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(600, 33)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(92, 13)
        Me.Label43.TabIndex = 8
        Me.Label43.Text = "Previous Email:"
        '
        'dgvphone
        '
        Me.dgvphone.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvphone.BackgroundColor = System.Drawing.Color.White
        Me.dgvphone.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvphone.Location = New System.Drawing.Point(399, 49)
        Me.dgvphone.Name = "dgvphone"
        Me.dgvphone.ReadOnly = True
        Me.dgvphone.RowHeadersVisible = False
        Me.dgvphone.Size = New System.Drawing.Size(198, 108)
        Me.dgvphone.TabIndex = 7
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Location = New System.Drawing.Point(396, 33)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(158, 13)
        Me.Label42.TabIndex = 6
        Me.Label42.Text = "Previous Residence Phone:"
        '
        'dgvcivil
        '
        Me.dgvcivil.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvcivil.BackgroundColor = System.Drawing.Color.White
        Me.dgvcivil.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvcivil.Location = New System.Drawing.Point(235, 49)
        Me.dgvcivil.Name = "dgvcivil"
        Me.dgvcivil.ReadOnly = True
        Me.dgvcivil.RowHeadersVisible = False
        Me.dgvcivil.Size = New System.Drawing.Size(155, 108)
        Me.dgvcivil.TabIndex = 5
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.Location = New System.Drawing.Point(232, 33)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(128, 13)
        Me.Label41.TabIndex = 4
        Me.Label41.Text = "Previous Civil Status :"
        '
        'dgvaddress
        '
        Me.dgvaddress.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvaddress.BackgroundColor = System.Drawing.Color.White
        Me.dgvaddress.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvaddress.Location = New System.Drawing.Point(18, 187)
        Me.dgvaddress.Name = "dgvaddress"
        Me.dgvaddress.ReadOnly = True
        Me.dgvaddress.RowHeadersVisible = False
        Me.dgvaddress.Size = New System.Drawing.Size(674, 82)
        Me.dgvaddress.TabIndex = 3
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.Location = New System.Drawing.Point(15, 171)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(111, 13)
        Me.Label40.TabIndex = 2
        Me.Label40.Text = "Previous Address :"
        '
        'dgvlastname
        '
        Me.dgvlastname.AllowUserToAddRows = False
        Me.dgvlastname.AllowUserToDeleteRows = False
        Me.dgvlastname.AllowUserToResizeColumns = False
        Me.dgvlastname.AllowUserToResizeRows = False
        Me.dgvlastname.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvlastname.BackgroundColor = System.Drawing.Color.White
        Me.dgvlastname.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvlastname.Location = New System.Drawing.Point(18, 49)
        Me.dgvlastname.Name = "dgvlastname"
        Me.dgvlastname.ReadOnly = True
        Me.dgvlastname.RowHeadersVisible = False
        Me.dgvlastname.Size = New System.Drawing.Size(208, 108)
        Me.dgvlastname.TabIndex = 1
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.Location = New System.Drawing.Point(15, 33)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(121, 13)
        Me.Label39.TabIndex = 0
        Me.Label39.Text = "Previous Lastname :"
        '
        'ContractHistory
        '
        Me.ContractHistory.BackColor = System.Drawing.Color.White
        Me.ContractHistory.Controls.Add(Me.Label82)
        Me.ContractHistory.Controls.Add(Me.btnSetContract)
        Me.ContractHistory.Controls.Add(Me.PictureBox33)
        Me.ContractHistory.Controls.Add(Me.lvlContractHis)
        Me.ContractHistory.Location = New System.Drawing.Point(4, 22)
        Me.ContractHistory.Name = "ContractHistory"
        Me.ContractHistory.Size = New System.Drawing.Size(1003, 275)
        Me.ContractHistory.TabIndex = 46
        Me.ContractHistory.Text = "Contarct History"
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label82.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label82.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label82.Location = New System.Drawing.Point(7, 3)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(172, 18)
        Me.Label82.TabIndex = 47
        Me.Label82.Text = "Employee Contract History"
        '
        'btnSetContract
        '
        Me.btnSetContract.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnSetContract.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSetContract.Location = New System.Drawing.Point(952, 251)
        Me.btnSetContract.Name = "btnSetContract"
        Me.btnSetContract.Size = New System.Drawing.Size(46, 23)
        Me.btnSetContract.TabIndex = 185
        Me.btnSetContract.Text = "Set Contract"
        Me.btnSetContract.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSetContract.UseVisualStyleBackColor = True
        Me.btnSetContract.Visible = False
        '
        'PictureBox33
        '
        Me.PictureBox33.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox33.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox33.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox33.Name = "PictureBox33"
        Me.PictureBox33.Size = New System.Drawing.Size(1071, 24)
        Me.PictureBox33.TabIndex = 45
        Me.PictureBox33.TabStop = False
        '
        'lvlContractHis
        '
        Me.lvlContractHis.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlContractHis.Location = New System.Drawing.Point(9, 30)
        Me.lvlContractHis.Name = "lvlContractHis"
        Me.lvlContractHis.Size = New System.Drawing.Size(911, 242)
        Me.lvlContractHis.TabIndex = 44
        Me.lvlContractHis.UseCompatibleStateImageBehavior = False
        '
        'PayrollHis
        '
        Me.PayrollHis.BackColor = System.Drawing.Color.White
        Me.PayrollHis.Controls.Add(Me.btnPayrollHistory)
        Me.PayrollHis.Controls.Add(Me.PictureBox19)
        Me.PayrollHis.Controls.Add(Me.lvlPayrollHistory)
        Me.PayrollHis.Location = New System.Drawing.Point(4, 22)
        Me.PayrollHis.Name = "PayrollHis"
        Me.PayrollHis.Size = New System.Drawing.Size(1003, 275)
        Me.PayrollHis.TabIndex = 42
        Me.PayrollHis.Text = "Payroll History"
        '
        'btnPayrollHistory
        '
        Me.btnPayrollHistory.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnPayrollHistory.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPayrollHistory.Location = New System.Drawing.Point(6, 1)
        Me.btnPayrollHistory.Name = "btnPayrollHistory"
        Me.btnPayrollHistory.Size = New System.Drawing.Size(133, 23)
        Me.btnPayrollHistory.TabIndex = 186
        Me.btnPayrollHistory.Text = "Payroll History"
        Me.btnPayrollHistory.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnPayrollHistory.UseVisualStyleBackColor = True
        '
        'PictureBox19
        '
        Me.PictureBox19.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox19.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox19.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox19.Name = "PictureBox19"
        Me.PictureBox19.Size = New System.Drawing.Size(1071, 24)
        Me.PictureBox19.TabIndex = 42
        Me.PictureBox19.TabStop = False
        '
        'lvlPayrollHistory
        '
        Me.lvlPayrollHistory.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlPayrollHistory.Location = New System.Drawing.Point(5, 30)
        Me.lvlPayrollHistory.Name = "lvlPayrollHistory"
        Me.lvlPayrollHistory.Size = New System.Drawing.Size(913, 242)
        Me.lvlPayrollHistory.TabIndex = 41
        Me.lvlPayrollHistory.UseCompatibleStateImageBehavior = False
        '
        'LeaveLedger
        '
        Me.LeaveLedger.BackColor = System.Drawing.Color.White
        Me.LeaveLedger.Controls.Add(Me.cboLeave)
        Me.LeaveLedger.Controls.Add(Me.Label78)
        Me.LeaveLedger.Controls.Add(Me.PictureBox18)
        Me.LeaveLedger.Controls.Add(Me.lvlLeave)
        Me.LeaveLedger.Location = New System.Drawing.Point(4, 22)
        Me.LeaveLedger.Name = "LeaveLedger"
        Me.LeaveLedger.Padding = New System.Windows.Forms.Padding(3)
        Me.LeaveLedger.Size = New System.Drawing.Size(1003, 275)
        Me.LeaveLedger.TabIndex = 30
        Me.LeaveLedger.Text = "Leave"
        '
        'cboLeave
        '
        Me.cboLeave.FormattingEnabled = True
        Me.cboLeave.Items.AddRange(New Object() {"All"})
        Me.cboLeave.Location = New System.Drawing.Point(3, 28)
        Me.cboLeave.Name = "cboLeave"
        Me.cboLeave.Size = New System.Drawing.Size(200, 23)
        Me.cboLeave.TabIndex = 187
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label78.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label78.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label78.Location = New System.Drawing.Point(3, 2)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(89, 18)
        Me.Label78.TabIndex = 35
        Me.Label78.Text = "Leave Ledger"
        '
        'PictureBox18
        '
        Me.PictureBox18.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox18.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox18.Location = New System.Drawing.Point(-3, 0)
        Me.PictureBox18.Name = "PictureBox18"
        Me.PictureBox18.Size = New System.Drawing.Size(1050, 22)
        Me.PictureBox18.TabIndex = 31
        Me.PictureBox18.TabStop = False
        '
        'lvlLeave
        '
        Me.lvlLeave.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlLeave.Location = New System.Drawing.Point(2, 55)
        Me.lvlLeave.Name = "lvlLeave"
        Me.lvlLeave.Size = New System.Drawing.Size(923, 214)
        Me.lvlLeave.TabIndex = 30
        Me.lvlLeave.UseCompatibleStateImageBehavior = False
        '
        'others
        '
        Me.others.Controls.Add(Me.btnEditMobile)
        Me.others.Controls.Add(Me.gridMobileNos)
        Me.others.Controls.Add(Me.txtEloadingLimit)
        Me.others.Controls.Add(Me.Label31)
        Me.others.Controls.Add(Me.txtEloaderPIN)
        Me.others.Controls.Add(Me.chkShowPINChar)
        Me.others.Controls.Add(Me.Label24)
        Me.others.Controls.Add(Me.Label15)
        Me.others.Controls.Add(Me.btnAddMobileNo)
        Me.others.Controls.Add(Me.chkIsEloader)
        Me.others.Controls.Add(Me.Label4)
        Me.others.Controls.Add(Me.txtMobileNo)
        Me.others.Controls.Add(Me.PictureBox9)
        Me.others.Controls.Add(Me.PictureBox7)
        Me.others.Location = New System.Drawing.Point(4, 22)
        Me.others.Name = "others"
        Me.others.Padding = New System.Windows.Forms.Padding(3)
        Me.others.Size = New System.Drawing.Size(1003, 275)
        Me.others.TabIndex = 20
        Me.others.Text = "Others"
        Me.others.UseVisualStyleBackColor = True
        '
        'btnEditMobile
        '
        Me.btnEditMobile.Location = New System.Drawing.Point(423, 72)
        Me.btnEditMobile.Name = "btnEditMobile"
        Me.btnEditMobile.Size = New System.Drawing.Size(43, 21)
        Me.btnEditMobile.TabIndex = 45
        Me.btnEditMobile.Text = "Edit"
        Me.btnEditMobile.UseVisualStyleBackColor = True
        '
        'gridMobileNos
        '
        Me.gridMobileNos.AllowUserToAddRows = False
        Me.gridMobileNos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.gridMobileNos.BackgroundColor = System.Drawing.Color.White
        Me.gridMobileNos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.gridMobileNos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.gridMobileNos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridMobileNos.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2
        Me.gridMobileNos.Location = New System.Drawing.Point(203, 45)
        Me.gridMobileNos.Name = "gridMobileNos"
        Me.gridMobileNos.Size = New System.Drawing.Size(214, 125)
        Me.gridMobileNos.TabIndex = 44
        '
        'txtEloadingLimit
        '
        Me.txtEloadingLimit.Location = New System.Drawing.Point(571, 73)
        Me.txtEloadingLimit.Name = "txtEloadingLimit"
        Me.txtEloadingLimit.Size = New System.Drawing.Size(115, 21)
        Me.txtEloadingLimit.TabIndex = 43
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(484, 78)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(87, 15)
        Me.Label31.TabIndex = 42
        Me.Label31.Text = "Eloading Limit:"
        '
        'txtEloaderPIN
        '
        Me.txtEloaderPIN.Location = New System.Drawing.Point(571, 45)
        Me.txtEloaderPIN.MaxLength = 4
        Me.txtEloaderPIN.Name = "txtEloaderPIN"
        Me.txtEloaderPIN.Size = New System.Drawing.Size(115, 21)
        Me.txtEloaderPIN.TabIndex = 40
        Me.txtEloaderPIN.UseSystemPasswordChar = True
        '
        'chkShowPINChar
        '
        Me.chkShowPINChar.AutoSize = True
        Me.chkShowPINChar.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowPINChar.Location = New System.Drawing.Point(692, 49)
        Me.chkShowPINChar.Name = "chkShowPINChar"
        Me.chkShowPINChar.Size = New System.Drawing.Size(95, 17)
        Me.chkShowPINChar.TabIndex = 39
        Me.chkShowPINChar.Text = "Show PIN Char."
        Me.chkShowPINChar.UseVisualStyleBackColor = True
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.Black
        Me.Label24.Location = New System.Drawing.Point(7, 29)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(101, 15)
        Me.Label24.TabIndex = 38
        Me.Label24.Text = "E-Loading Service"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(484, 51)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(30, 15)
        Me.Label15.TabIndex = 36
        Me.Label15.Text = "PIN:"
        '
        'btnAddMobileNo
        '
        Me.btnAddMobileNo.Location = New System.Drawing.Point(423, 45)
        Me.btnAddMobileNo.Name = "btnAddMobileNo"
        Me.btnAddMobileNo.Size = New System.Drawing.Size(43, 21)
        Me.btnAddMobileNo.TabIndex = 27
        Me.btnAddMobileNo.Text = "Add"
        Me.btnAddMobileNo.UseVisualStyleBackColor = True
        '
        'chkIsEloader
        '
        Me.chkIsEloader.AutoSize = True
        Me.chkIsEloader.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsEloader.Location = New System.Drawing.Point(571, 98)
        Me.chkIsEloader.Name = "chkIsEloader"
        Me.chkIsEloader.Size = New System.Drawing.Size(131, 19)
        Me.chkIsEloader.TabIndex = 35
        Me.chkIsEloader.Text = "Registered Member"
        Me.chkIsEloader.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label4.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label4.Location = New System.Drawing.Point(5, 1)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(123, 18)
        Me.Label4.TabIndex = 34
        Me.Label4.Text = "Apps Registrations"
        '
        'txtMobileNo
        '
        Me.txtMobileNo.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMobileNo.Location = New System.Drawing.Point(487, 134)
        Me.txtMobileNo.Name = "txtMobileNo"
        Me.txtMobileNo.ReadOnly = True
        Me.txtMobileNo.Size = New System.Drawing.Size(199, 21)
        Me.txtMobileNo.TabIndex = 13
        Me.txtMobileNo.Visible = False
        '
        'PictureBox9
        '
        Me.PictureBox9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox9.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox9.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(975, 19)
        Me.PictureBox9.TabIndex = 33
        Me.PictureBox9.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.Image = Global.WindowsApplication2.My.Resources.Resources.img_eload
        Me.PictureBox7.Location = New System.Drawing.Point(9, 48)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(188, 124)
        Me.PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox7.TabIndex = 0
        Me.PictureBox7.TabStop = False
        '
        'btnrestricted
        '
        Me.btnrestricted.Enabled = False
        Me.btnrestricted.ForeColor = System.Drawing.Color.Red
        Me.btnrestricted.Location = New System.Drawing.Point(758, 566)
        Me.btnrestricted.Name = "btnrestricted"
        Me.btnrestricted.Size = New System.Drawing.Size(26, 25)
        Me.btnrestricted.TabIndex = 145
        Me.btnrestricted.Text = "Confidential"
        Me.btnrestricted.UseVisualStyleBackColor = True
        Me.btnrestricted.Visible = False
        '
        'txtContractPrice
        '
        Me.txtContractPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtContractPrice.Location = New System.Drawing.Point(731, 568)
        Me.txtContractPrice.MaxLength = 255
        Me.txtContractPrice.Name = "txtContractPrice"
        Me.txtContractPrice.Size = New System.Drawing.Size(18, 20)
        Me.txtContractPrice.TabIndex = 146
        Me.txtContractPrice.Text = "0.00"
        Me.txtContractPrice.Visible = False
        '
        'txtofficeadd
        '
        Me.txtofficeadd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtofficeadd.Location = New System.Drawing.Point(499, 568)
        Me.txtofficeadd.MaxLength = 50
        Me.txtofficeadd.Name = "txtofficeadd"
        Me.txtofficeadd.Size = New System.Drawing.Size(43, 20)
        Me.txtofficeadd.TabIndex = 2
        Me.txtofficeadd.Visible = False
        '
        'chkBereaveYes
        '
        Me.chkBereaveYes.AutoSize = True
        Me.chkBereaveYes.Location = New System.Drawing.Point(677, 5)
        Me.chkBereaveYes.Name = "chkBereaveYes"
        Me.chkBereaveYes.Size = New System.Drawing.Size(42, 19)
        Me.chkBereaveYes.TabIndex = 117
        Me.chkBereaveYes.Text = "Yes"
        Me.chkBereaveYes.UseVisualStyleBackColor = True
        Me.chkBereaveYes.Visible = False
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label61.Location = New System.Drawing.Point(620, 574)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(117, 15)
        Me.Label61.TabIndex = 121
        Me.Label61.Text = "Basic Contract Price:"
        Me.Label61.Visible = False
        '
        'chkBereaveNo
        '
        Me.chkBereaveNo.AutoSize = True
        Me.chkBereaveNo.Location = New System.Drawing.Point(708, 4)
        Me.chkBereaveNo.Name = "chkBereaveNo"
        Me.chkBereaveNo.Size = New System.Drawing.Size(39, 19)
        Me.chkBereaveNo.TabIndex = 116
        Me.chkBereaveNo.Text = "No"
        Me.chkBereaveNo.UseVisualStyleBackColor = True
        Me.chkBereaveNo.Visible = False
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.ForeColor = System.Drawing.Color.Red
        Me.Label49.Location = New System.Drawing.Point(873, 5)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(15, 20)
        Me.Label49.TabIndex = 114
        Me.Label49.Text = "*"
        Me.Label49.Visible = False
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(261, 569)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(62, 15)
        Me.Label20.TabIndex = 2
        Me.Label20.Text = "Company:"
        Me.Label20.Visible = False
        '
        'cboPaycode
        '
        Me.cboPaycode.BackColor = System.Drawing.Color.White
        Me.cboPaycode.FormattingEnabled = True
        Me.cboPaycode.Location = New System.Drawing.Point(850, 3)
        Me.cboPaycode.Name = "cboPaycode"
        Me.cboPaycode.Size = New System.Drawing.Size(38, 21)
        Me.cboPaycode.TabIndex = 54
        Me.cboPaycode.Visible = False
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.Location = New System.Drawing.Point(789, 5)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(61, 15)
        Me.Label38.TabIndex = 111
        Me.Label38.Text = "Pay Code:"
        Me.Label38.Visible = False
        '
        'mem_PaycontDate
        '
        Me.mem_PaycontDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.mem_PaycontDate.Location = New System.Drawing.Point(191, 569)
        Me.mem_PaycontDate.Name = "mem_PaycontDate"
        Me.mem_PaycontDate.Size = New System.Drawing.Size(85, 20)
        Me.mem_PaycontDate.TabIndex = 1
        Me.mem_PaycontDate.Visible = False
        '
        'Label110
        '
        Me.Label110.AutoSize = True
        Me.Label110.Location = New System.Drawing.Point(174, 572)
        Me.Label110.Name = "Label110"
        Me.Label110.Size = New System.Drawing.Size(30, 13)
        Me.Label110.TabIndex = 109
        Me.Label110.Text = "Date"
        Me.Label110.Visible = False
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(436, 573)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(90, 15)
        Me.Label21.TabIndex = 3
        Me.Label21.Text = "Office Address:"
        Me.Label21.Visible = False
        '
        'txtpayrollcontriamount
        '
        Me.txtpayrollcontriamount.AcceptsTab = True
        Me.txtpayrollcontriamount.Location = New System.Drawing.Point(130, 569)
        Me.txtpayrollcontriamount.Name = "txtpayrollcontriamount"
        Me.txtpayrollcontriamount.Size = New System.Drawing.Size(38, 20)
        Me.txtpayrollcontriamount.TabIndex = 0
        Me.txtpayrollcontriamount.Text = "0.00"
        Me.txtpayrollcontriamount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtpayrollcontriamount.Visible = False
        '
        'Label109
        '
        Me.Label109.AutoSize = True
        Me.Label109.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label109.Location = New System.Drawing.Point(9, 570)
        Me.Label109.Name = "Label109"
        Me.Label109.Size = New System.Drawing.Size(129, 15)
        Me.Label109.TabIndex = 109
        Me.Label109.Text = "Contribution Amount:"
        Me.Label109.Visible = False
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(339, 568)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(65, 15)
        Me.Label22.TabIndex = 4
        Me.Label22.Text = "Office No.:"
        Me.Label22.Visible = False
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(532, 574)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(60, 15)
        Me.Label23.TabIndex = 5
        Me.Label23.Text = "Local No.:"
        Me.Label23.Visible = False
        '
        'Label105
        '
        Me.Label105.AutoSize = True
        Me.Label105.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label105.Location = New System.Drawing.Point(619, 5)
        Me.Label105.Name = "Label105"
        Me.Label105.Size = New System.Drawing.Size(86, 15)
        Me.Label105.TabIndex = 102
        Me.Label105.Text = "Bereavement:"
        Me.Label105.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label105.Visible = False
        '
        'txtofficenumber
        '
        Me.txtofficenumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtofficenumber.Location = New System.Drawing.Point(403, 567)
        Me.txtofficenumber.MaxLength = 255
        Me.txtofficenumber.Name = "txtofficenumber"
        Me.txtofficenumber.Size = New System.Drawing.Size(43, 20)
        Me.txtofficenumber.TabIndex = 3
        Me.txtofficenumber.Visible = False
        '
        'emp_company
        '
        Me.emp_company.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.emp_company.Location = New System.Drawing.Point(318, 568)
        Me.emp_company.Name = "emp_company"
        Me.emp_company.Size = New System.Drawing.Size(35, 20)
        Me.emp_company.TabIndex = 103
        Me.emp_company.Visible = False
        '
        'txtlocalofficenumber
        '
        Me.txtlocalofficenumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtlocalofficenumber.Location = New System.Drawing.Point(589, 570)
        Me.txtlocalofficenumber.MaxLength = 255
        Me.txtlocalofficenumber.Name = "txtlocalofficenumber"
        Me.txtlocalofficenumber.Size = New System.Drawing.Size(43, 20)
        Me.txtlocalofficenumber.TabIndex = 4
        Me.txtlocalofficenumber.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(14, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(44, 15)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "ID No.:"
        '
        'txtEmployeeNo
        '
        Me.txtEmployeeNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtEmployeeNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEmployeeNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEmployeeNo.Enabled = False
        Me.txtEmployeeNo.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployeeNo.Location = New System.Drawing.Point(59, 7)
        Me.txtEmployeeNo.MaxLength = 50
        Me.txtEmployeeNo.Name = "txtEmployeeNo"
        Me.txtEmployeeNo.Size = New System.Drawing.Size(153, 21)
        Me.txtEmployeeNo.TabIndex = 12
        '
        'TextBox15
        '
        Me.TextBox15.Location = New System.Drawing.Point(92, 145)
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.Size = New System.Drawing.Size(156, 20)
        Me.TextBox15.TabIndex = 11
        '
        'TextBox16
        '
        Me.TextBox16.Location = New System.Drawing.Point(92, 119)
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.Size = New System.Drawing.Size(156, 20)
        Me.TextBox16.TabIndex = 10
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(92, 93)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.Size = New System.Drawing.Size(156, 20)
        Me.TextBox17.TabIndex = 9
        '
        'TextBox18
        '
        Me.TextBox18.Location = New System.Drawing.Point(92, 67)
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.Size = New System.Drawing.Size(156, 20)
        Me.TextBox18.TabIndex = 8
        '
        'TextBox19
        '
        Me.TextBox19.Location = New System.Drawing.Point(92, 41)
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.Size = New System.Drawing.Size(156, 20)
        Me.TextBox19.TabIndex = 7
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(17, 152)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(54, 13)
        Me.Label25.TabIndex = 6
        Me.Label25.Text = "Address 3"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(17, 126)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(54, 13)
        Me.Label26.TabIndex = 5
        Me.Label26.Text = "Address 2"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(17, 100)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(54, 13)
        Me.Label27.TabIndex = 4
        Me.Label27.Text = "Address 1"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(17, 74)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(45, 13)
        Me.Label28.TabIndex = 3
        Me.Label28.Text = "Religion"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(17, 48)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(70, 13)
        Me.Label29.TabIndex = 2
        Me.Label29.Text = "Place of Birth"
        '
        'MaskedTextBox5
        '
        Me.MaskedTextBox5.Location = New System.Drawing.Point(92, 15)
        Me.MaskedTextBox5.Mask = "LLLL,00,0000"
        Me.MaskedTextBox5.Name = "MaskedTextBox5"
        Me.MaskedTextBox5.Size = New System.Drawing.Size(79, 20)
        Me.MaskedTextBox5.TabIndex = 1
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(17, 22)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(66, 13)
        Me.Label30.TabIndex = 0
        Me.Label30.Text = "Date of Birth"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'cboemp_gender
        '
        Me.cboemp_gender.AutoCompleteCustomSource.AddRange(New String() {"RANK AND FILE", "MANAGERIAL", "SUPERVISOR", "EXECUTIVE"})
        Me.cboemp_gender.BackColor = System.Drawing.Color.White
        Me.cboemp_gender.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboemp_gender.FormattingEnabled = True
        Me.cboemp_gender.Items.AddRange(New Object() {"Female", "Male"})
        Me.cboemp_gender.Location = New System.Drawing.Point(528, 113)
        Me.cboemp_gender.Name = "cboemp_gender"
        Me.cboemp_gender.Size = New System.Drawing.Size(114, 22)
        Me.cboemp_gender.TabIndex = 20
        '
        'cboemp_civil
        '
        Me.cboemp_civil.AutoCompleteCustomSource.AddRange(New String() {"RANK AND FILE", "MANAGERIAL", "SUPERVISOR", "EXECUTIVE"})
        Me.cboemp_civil.BackColor = System.Drawing.Color.White
        Me.cboemp_civil.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboemp_civil.FormattingEnabled = True
        Me.cboemp_civil.Location = New System.Drawing.Point(180, 153)
        Me.cboemp_civil.Name = "cboemp_civil"
        Me.cboemp_civil.Size = New System.Drawing.Size(139, 22)
        Me.cboemp_civil.TabIndex = 22
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(507, 98)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(52, 15)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Gender:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(165, 139)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(70, 15)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "Civil Status:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.Panel1)
        Me.GroupBox1.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(15, 34)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(761, 214)
        Me.GroupBox1.TabIndex = 78
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "PERSONAL INFORMATION"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.txtSuffix)
        Me.Panel1.Controls.Add(Me.Label165)
        Me.Panel1.Controls.Add(Me.Label139)
        Me.Panel1.Controls.Add(Me.txtHeightInch)
        Me.Panel1.Controls.Add(Me.Label138)
        Me.Panel1.Controls.Add(Me.Label137)
        Me.Panel1.Controls.Add(Me.txtW)
        Me.Panel1.Controls.Add(Me.txtage1)
        Me.Panel1.Controls.Add(Me.txtHeightFt)
        Me.Panel1.Controls.Add(Me.txtNickname)
        Me.Panel1.Controls.Add(Me.txtpk_Employee)
        Me.Panel1.Controls.Add(Me.temp_placebirth)
        Me.Panel1.Controls.Add(Me.temp_midname)
        Me.Panel1.Controls.Add(Me.Label33)
        Me.Panel1.Controls.Add(Me.lblage)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label87)
        Me.Panel1.Controls.Add(Me.cboemp_gender)
        Me.Panel1.Controls.Add(Me.temp_fname)
        Me.Panel1.Controls.Add(Me.Label89)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.cboemp_civil)
        Me.Panel1.Controls.Add(Me.EMP_DATEbirth)
        Me.Panel1.Controls.Add(Me.Label67)
        Me.Panel1.Controls.Add(Me.Label19)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.temp_lname)
        Me.Panel1.Controls.Add(Me.chkBereaveYes)
        Me.Panel1.Controls.Add(Me.Label90)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label105)
        Me.Panel1.Controls.Add(Me.chkBereaveNo)
        Me.Panel1.Location = New System.Drawing.Point(3, 16)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(755, 191)
        Me.Panel1.TabIndex = 0
        '
        'txtSuffix
        '
        Me.txtSuffix.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSuffix.Location = New System.Drawing.Point(372, 115)
        Me.txtSuffix.MaxLength = 5
        Me.txtSuffix.Name = "txtSuffix"
        Me.txtSuffix.Size = New System.Drawing.Size(86, 20)
        Me.txtSuffix.TabIndex = 19
        '
        'Label165
        '
        Me.Label165.AutoSize = True
        Me.Label165.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label165.Location = New System.Drawing.Point(363, 99)
        Me.Label165.Name = "Label165"
        Me.Label165.Size = New System.Drawing.Size(41, 15)
        Me.Label165.TabIndex = 121
        Me.Label165.Text = "Suffix:"
        '
        'Label139
        '
        Me.Label139.AutoSize = True
        Me.Label139.Location = New System.Drawing.Point(459, 156)
        Me.Label139.Name = "Label139"
        Me.Label139.Size = New System.Drawing.Size(37, 15)
        Me.Label139.TabIndex = 117
        Me.Label139.Text = "inches"
        '
        'txtHeightInch
        '
        Me.txtHeightInch.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHeightInch.Location = New System.Drawing.Point(428, 154)
        Me.txtHeightInch.Name = "txtHeightInch"
        Me.txtHeightInch.Size = New System.Drawing.Size(31, 20)
        Me.txtHeightInch.TabIndex = 24
        '
        'Label138
        '
        Me.Label138.AutoSize = True
        Me.Label138.Location = New System.Drawing.Point(562, 156)
        Me.Label138.Name = "Label138"
        Me.Label138.Size = New System.Drawing.Size(42, 15)
        Me.Label138.TabIndex = 115
        Me.Label138.Text = "pounds"
        '
        'Label137
        '
        Me.Label137.AutoSize = True
        Me.Label137.Location = New System.Drawing.Point(405, 156)
        Me.Label137.Name = "Label137"
        Me.Label137.Size = New System.Drawing.Size(23, 15)
        Me.Label137.TabIndex = 113
        Me.Label137.Text = "feet"
        '
        'txtW
        '
        Me.txtW.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtW.Location = New System.Drawing.Point(528, 154)
        Me.txtW.Name = "txtW"
        Me.txtW.Size = New System.Drawing.Size(34, 20)
        Me.txtW.TabIndex = 25
        '
        'txtage1
        '
        Me.txtage1.BackColor = System.Drawing.SystemColors.Window
        Me.txtage1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtage1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtage1.Location = New System.Drawing.Point(544, 32)
        Me.txtage1.MaxLength = 3
        Me.txtage1.Multiline = True
        Me.txtage1.Name = "txtage1"
        Me.txtage1.ReadOnly = True
        Me.txtage1.Size = New System.Drawing.Size(63, 17)
        Me.txtage1.TabIndex = 15
        Me.txtage1.Text = "0"
        '
        'txtHeightFt
        '
        Me.txtHeightFt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHeightFt.Location = New System.Drawing.Point(372, 154)
        Me.txtHeightFt.Name = "txtHeightFt"
        Me.txtHeightFt.Size = New System.Drawing.Size(32, 20)
        Me.txtHeightFt.TabIndex = 23
        '
        'txtNickname
        '
        Me.txtNickname.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNickname.Location = New System.Drawing.Point(41, 154)
        Me.txtNickname.Name = "txtNickname"
        Me.txtNickname.Size = New System.Drawing.Size(100, 20)
        Me.txtNickname.TabIndex = 21
        '
        'txtpk_Employee
        '
        Me.txtpk_Employee.Location = New System.Drawing.Point(649, 138)
        Me.txtpk_Employee.Name = "txtpk_Employee"
        Me.txtpk_Employee.Size = New System.Drawing.Size(103, 20)
        Me.txtpk_Employee.TabIndex = 11
        Me.txtpk_Employee.Visible = False
        '
        'temp_placebirth
        '
        Me.temp_placebirth.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.temp_placebirth.Location = New System.Drawing.Point(372, 72)
        Me.temp_placebirth.MaxLength = 255
        Me.temp_placebirth.Name = "temp_placebirth"
        Me.temp_placebirth.Size = New System.Drawing.Size(352, 20)
        Me.temp_placebirth.TabIndex = 17
        '
        'temp_midname
        '
        Me.temp_midname.BackColor = System.Drawing.SystemColors.Window
        Me.temp_midname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_midname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.temp_midname.Enabled = False
        Me.temp_midname.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.temp_midname.Location = New System.Drawing.Point(41, 72)
        Me.temp_midname.MaxLength = 50
        Me.temp_midname.Name = "temp_midname"
        Me.temp_midname.Size = New System.Drawing.Size(296, 25)
        Me.temp_midname.TabIndex = 16
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(689, 161)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(63, 15)
        Me.Label33.TabIndex = 107
        Me.Label33.Text = "Signature:"
        Me.Label33.Visible = False
        '
        'lblage
        '
        Me.lblage.AutoSize = True
        Me.lblage.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblage.Location = New System.Drawing.Point(512, 31)
        Me.lblage.Name = "lblage"
        Me.lblage.Size = New System.Drawing.Size(32, 14)
        Me.lblage.TabIndex = 44
        Me.lblage.Text = "Age "
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(28, 58)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 14)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Middle Name"
        '
        'Label87
        '
        Me.Label87.AutoSize = True
        Me.Label87.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label87.Location = New System.Drawing.Point(28, 140)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(65, 15)
        Me.Label87.TabIndex = 48
        Me.Label87.Text = "Nickname:"
        '
        'temp_fname
        '
        Me.temp_fname.BackColor = System.Drawing.SystemColors.Window
        Me.temp_fname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_fname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.temp_fname.Enabled = False
        Me.temp_fname.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.temp_fname.Location = New System.Drawing.Point(41, 31)
        Me.temp_fname.MaxLength = 50
        Me.temp_fname.Name = "temp_fname"
        Me.temp_fname.Size = New System.Drawing.Size(296, 25)
        Me.temp_fname.TabIndex = 13
        '
        'Label89
        '
        Me.Label89.AutoSize = True
        Me.Label89.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label89.Location = New System.Drawing.Point(358, 138)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(47, 15)
        Me.Label89.TabIndex = 50
        Me.Label89.Text = "Height:"
        '
        'EMP_DATEbirth
        '
        Me.EMP_DATEbirth.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EMP_DATEbirth.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.EMP_DATEbirth.Location = New System.Drawing.Point(372, 29)
        Me.EMP_DATEbirth.Name = "EMP_DATEbirth"
        Me.EMP_DATEbirth.Size = New System.Drawing.Size(126, 20)
        Me.EMP_DATEbirth.TabIndex = 14
        Me.EMP_DATEbirth.Value = New Date(2006, 3, 17, 0, 0, 0, 0)
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label67.Location = New System.Drawing.Point(359, 56)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(69, 15)
        Me.Label67.TabIndex = 102
        Me.Label67.Text = "Birth Place:"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(358, 15)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(81, 15)
        Me.Label19.TabIndex = 0
        Me.Label19.Text = "Date of Birth:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(28, 99)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(65, 14)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Last Name"
        '
        'temp_lname
        '
        Me.temp_lname.BackColor = System.Drawing.SystemColors.Window
        Me.temp_lname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_lname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.temp_lname.Enabled = False
        Me.temp_lname.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.temp_lname.Location = New System.Drawing.Point(41, 113)
        Me.temp_lname.MaxLength = 50
        Me.temp_lname.Name = "temp_lname"
        Me.temp_lname.Size = New System.Drawing.Size(297, 25)
        Me.temp_lname.TabIndex = 18
        '
        'Label90
        '
        Me.Label90.AutoSize = True
        Me.Label90.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label90.Location = New System.Drawing.Point(508, 137)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(51, 15)
        Me.Label90.TabIndex = 109
        Me.Label90.Text = "Weight:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(28, 17)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(66, 14)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "First Name"
        '
        'txtCompanyName
        '
        Me.txtCompanyName.BackColor = System.Drawing.SystemColors.Window
        Me.txtCompanyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCompanyName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCompanyName.Enabled = False
        Me.txtCompanyName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCompanyName.Location = New System.Drawing.Point(976, 4)
        Me.txtCompanyName.Name = "txtCompanyName"
        Me.txtCompanyName.Size = New System.Drawing.Size(48, 20)
        Me.txtCompanyName.TabIndex = 26
        Me.txtCompanyName.Visible = False
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.BackColor = System.Drawing.Color.Transparent
        Me.Label79.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label79.Location = New System.Drawing.Point(880, 6)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(98, 15)
        Me.Label79.TabIndex = 120
        Me.Label79.Text = "Company Name:"
        Me.Label79.Visible = False
        '
        'OpenFileDialog2
        '
        Me.OpenFileDialog2.FileName = "OpenFileDialog2"
        '
        'btnPrevTab
        '
        Me.btnPrevTab.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrevTab.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrevTab.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPrevTab.Location = New System.Drawing.Point(890, 562)
        Me.btnPrevTab.Name = "btnPrevTab"
        Me.btnPrevTab.Size = New System.Drawing.Size(66, 26)
        Me.btnPrevTab.TabIndex = 10
        Me.btnPrevTab.Text = "<<"
        Me.btnPrevTab.UseVisualStyleBackColor = True
        '
        'btnNextTab
        '
        Me.btnNextTab.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNextTab.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNextTab.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnNextTab.Location = New System.Drawing.Point(956, 562)
        Me.btnNextTab.Name = "btnNextTab"
        Me.btnNextTab.Size = New System.Drawing.Size(66, 26)
        Me.btnNextTab.TabIndex = 11
        Me.btnNextTab.Text = ">>"
        Me.btnNextTab.UseVisualStyleBackColor = True
        '
        'txtMemberID
        '
        Me.txtMemberID.Location = New System.Drawing.Point(480, 0)
        Me.txtMemberID.Name = "txtMemberID"
        Me.txtMemberID.Size = New System.Drawing.Size(36, 20)
        Me.txtMemberID.TabIndex = 191
        Me.txtMemberID.Visible = False
        '
        'txtDepartment2
        '
        Me.txtDepartment2.Location = New System.Drawing.Point(480, 18)
        Me.txtDepartment2.Name = "txtDepartment2"
        Me.txtDepartment2.Size = New System.Drawing.Size(36, 20)
        Me.txtDepartment2.TabIndex = 192
        Me.txtDepartment2.Visible = False
        '
        'lblemployee_name
        '
        Me.lblemployee_name.AutoSize = True
        Me.lblemployee_name.Location = New System.Drawing.Point(443, 21)
        Me.lblemployee_name.Name = "lblemployee_name"
        Me.lblemployee_name.Size = New System.Drawing.Size(43, 13)
        Me.lblemployee_name.TabIndex = 193
        Me.lblemployee_name.Text = "lblname"
        Me.lblemployee_name.Visible = False
        '
        'label
        '
        Me.label.AutoSize = True
        Me.label.Location = New System.Drawing.Point(457, 2)
        Me.label.Name = "label"
        Me.label.Size = New System.Drawing.Size(29, 13)
        Me.label.TabIndex = 194
        Me.label.Text = "txtID"
        Me.label.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnMaxSignature)
        Me.GroupBox2.Controls.Add(Me.btnMaxPhoto)
        Me.GroupBox2.Controls.Add(Me.Label32)
        Me.GroupBox2.Controls.Add(Me.btnBrowseSignature)
        Me.GroupBox2.Controls.Add(Me.picEmpSignature)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.btnBrowsePicture)
        Me.GroupBox2.Controls.Add(Me.picEmpPhoto)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(226, 215)
        Me.GroupBox2.TabIndex = 127
        Me.GroupBox2.TabStop = False
        '
        'btnMaxSignature
        '
        Me.btnMaxSignature.Location = New System.Drawing.Point(189, 121)
        Me.btnMaxSignature.Name = "btnMaxSignature"
        Me.btnMaxSignature.Size = New System.Drawing.Size(29, 23)
        Me.btnMaxSignature.TabIndex = 9
        Me.btnMaxSignature.Text = "[*]"
        Me.btnMaxSignature.UseVisualStyleBackColor = True
        '
        'btnMaxPhoto
        '
        Me.btnMaxPhoto.Location = New System.Drawing.Point(191, 14)
        Me.btnMaxPhoto.Name = "btnMaxPhoto"
        Me.btnMaxPhoto.Size = New System.Drawing.Size(29, 23)
        Me.btnMaxPhoto.TabIndex = 8
        Me.btnMaxPhoto.Text = "[* ]"
        Me.btnMaxPhoto.UseVisualStyleBackColor = True
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(6, 12)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(44, 15)
        Me.Label32.TabIndex = 126
        Me.Label32.Text = "Photo:"
        '
        'btnBrowseSignature
        '
        Me.btnBrowseSignature.Location = New System.Drawing.Point(189, 186)
        Me.btnBrowseSignature.Name = "btnBrowseSignature"
        Me.btnBrowseSignature.Size = New System.Drawing.Size(29, 23)
        Me.btnBrowseSignature.TabIndex = 25
        Me.btnBrowseSignature.Text = "..."
        Me.btnBrowseSignature.UseVisualStyleBackColor = True
        Me.btnBrowseSignature.Visible = False
        '
        'picEmpSignature
        '
        Me.picEmpSignature.BackColor = System.Drawing.Color.DarkGray
        Me.picEmpSignature.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picEmpSignature.Image = CType(resources.GetObject("picEmpSignature.Image"), System.Drawing.Image)
        Me.picEmpSignature.Location = New System.Drawing.Point(65, 118)
        Me.picEmpSignature.Name = "picEmpSignature"
        Me.picEmpSignature.Size = New System.Drawing.Size(120, 91)
        Me.picEmpSignature.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picEmpSignature.TabIndex = 124
        Me.picEmpSignature.TabStop = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(3, 112)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(63, 15)
        Me.Label16.TabIndex = 121
        Me.Label16.Text = "Signature:"
        '
        'btnBrowsePicture
        '
        Me.btnBrowsePicture.Location = New System.Drawing.Point(189, 92)
        Me.btnBrowsePicture.Name = "btnBrowsePicture"
        Me.btnBrowsePicture.Size = New System.Drawing.Size(29, 23)
        Me.btnBrowsePicture.TabIndex = 23
        Me.btnBrowsePicture.Text = "..."
        Me.btnBrowsePicture.UseVisualStyleBackColor = True
        Me.btnBrowsePicture.Visible = False
        '
        'picEmpPhoto
        '
        Me.picEmpPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picEmpPhoto.Image = Global.WindowsApplication2.My.Resources.Resources.photo
        Me.picEmpPhoto.Location = New System.Drawing.Point(65, 12)
        Me.picEmpPhoto.Name = "picEmpPhoto"
        Me.picEmpPhoto.Size = New System.Drawing.Size(120, 100)
        Me.picEmpPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picEmpPhoto.TabIndex = 122
        Me.picEmpPhoto.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel3.BackColor = System.Drawing.Color.Transparent
        Me.Panel3.Controls.Add(Me.GroupBox2)
        Me.Panel3.Location = New System.Drawing.Point(782, 27)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(237, 227)
        Me.Panel3.TabIndex = 108
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.Location = New System.Drawing.Point(831, 135)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(49, 14)
        Me.Label44.TabIndex = 149
        Me.Label44.Text = "Project:"
        '
        'txtProject
        '
        Me.txtProject.Location = New System.Drawing.Point(880, 131)
        Me.txtProject.Name = "txtProject"
        Me.txtProject.Size = New System.Drawing.Size(115, 21)
        Me.txtProject.TabIndex = 150
        '
        'btnemp_close
        '
        Me.btnemp_close.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_close.BackColor = System.Drawing.Color.White
        Me.btnemp_close.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnemp_close.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnemp_close.Image = Global.WindowsApplication2.My.Resources.Resources.eventlogError
        Me.btnemp_close.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_close.Location = New System.Drawing.Point(958, 3)
        Me.btnemp_close.Name = "btnemp_close"
        Me.btnemp_close.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_close.TabIndex = 7
        Me.btnemp_close.Text = "Close"
        Me.btnemp_close.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_close.UseVisualStyleBackColor = False
        '
        'PanePanel2
        '
        Me.PanePanel2.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel2.Controls.Add(Me.btnemp_close)
        Me.PanePanel2.Controls.Add(Me.btnemp_next)
        Me.PanePanel2.Controls.Add(Me.btnemp_delete)
        Me.PanePanel2.Controls.Add(Me.BTNSEARCH)
        Me.PanePanel2.Controls.Add(Me.btnemp_edit)
        Me.PanePanel2.Controls.Add(Me.btnemp_previous)
        Me.PanePanel2.Controls.Add(Me.btnemp_add)
        Me.PanePanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel2.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel2.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel2.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel2.Location = New System.Drawing.Point(0, 592)
        Me.PanePanel2.Name = "PanePanel2"
        Me.PanePanel2.Size = New System.Drawing.Size(1028, 35)
        Me.PanePanel2.TabIndex = 83
        '
        'btnemp_next
        '
        Me.btnemp_next.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnemp_next.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnemp_next.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_next.Location = New System.Drawing.Point(147, 3)
        Me.btnemp_next.Name = "btnemp_next"
        Me.btnemp_next.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_next.TabIndex = 3
        Me.btnemp_next.Text = ">>"
        Me.btnemp_next.UseVisualStyleBackColor = True
        '
        'btnemp_delete
        '
        Me.btnemp_delete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_delete.BackColor = System.Drawing.Color.White
        Me.btnemp_delete.Image = CType(resources.GetObject("btnemp_delete.Image"), System.Drawing.Image)
        Me.btnemp_delete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_delete.Location = New System.Drawing.Point(891, 3)
        Me.btnemp_delete.Name = "btnemp_delete"
        Me.btnemp_delete.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_delete.TabIndex = 6
        Me.btnemp_delete.Text = "Delete"
        Me.btnemp_delete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_delete.UseVisualStyleBackColor = False
        '
        'BTNSEARCH
        '
        Me.BTNSEARCH.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BTNSEARCH.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BTNSEARCH.Image = CType(resources.GetObject("BTNSEARCH.Image"), System.Drawing.Image)
        Me.BTNSEARCH.Location = New System.Drawing.Point(80, 3)
        Me.BTNSEARCH.Name = "BTNSEARCH"
        Me.BTNSEARCH.Size = New System.Drawing.Size(66, 28)
        Me.BTNSEARCH.TabIndex = 2
        Me.BTNSEARCH.UseVisualStyleBackColor = True
        '
        'btnemp_edit
        '
        Me.btnemp_edit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_edit.BackColor = System.Drawing.Color.White
        Me.btnemp_edit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnemp_edit.Image = CType(resources.GetObject("btnemp_edit.Image"), System.Drawing.Image)
        Me.btnemp_edit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_edit.Location = New System.Drawing.Point(824, 3)
        Me.btnemp_edit.Name = "btnemp_edit"
        Me.btnemp_edit.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_edit.TabIndex = 5
        Me.btnemp_edit.Text = "Edit"
        Me.btnemp_edit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_edit.UseVisualStyleBackColor = False
        '
        'btnemp_previous
        '
        Me.btnemp_previous.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnemp_previous.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnemp_previous.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_previous.Location = New System.Drawing.Point(13, 3)
        Me.btnemp_previous.Name = "btnemp_previous"
        Me.btnemp_previous.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_previous.TabIndex = 1
        Me.btnemp_previous.Text = "<<"
        Me.btnemp_previous.UseVisualStyleBackColor = True
        '
        'btnemp_add
        '
        Me.btnemp_add.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_add.BackColor = System.Drawing.Color.White
        Me.btnemp_add.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnemp_add.Image = CType(resources.GetObject("btnemp_add.Image"), System.Drawing.Image)
        Me.btnemp_add.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_add.Location = New System.Drawing.Point(757, 3)
        Me.btnemp_add.Name = "btnemp_add"
        Me.btnemp_add.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_add.TabIndex = 4
        Me.btnemp_add.Text = "New"
        Me.btnemp_add.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_add.UseVisualStyleBackColor = False
        '
        'frmMember_Master
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.CancelButton = Me.btnemp_close
        Me.ClientSize = New System.Drawing.Size(1028, 627)
        Me.Controls.Add(Me.label)
        Me.Controls.Add(Me.lblemployee_name)
        Me.Controls.Add(Me.txtDepartment2)
        Me.Controls.Add(Me.txtMemberID)
        Me.Controls.Add(Me.btnPrevTab)
        Me.Controls.Add(Me.btnNextTab)
        Me.Controls.Add(Me.tabMember)
        Me.Controls.Add(Me.txtCompanyName)
        Me.Controls.Add(Me.btnrestricted)
        Me.Controls.Add(Me.txtContractPrice)
        Me.Controls.Add(Me.Label79)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.PanePanel2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtEmployeeNo)
        Me.Controls.Add(Me.txtofficeadd)
        Me.Controls.Add(Me.Label61)
        Me.Controls.Add(Me.Label49)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.emp_company)
        Me.Controls.Add(Me.cboPaycode)
        Me.Controls.Add(Me.Label109)
        Me.Controls.Add(Me.txtpayrollcontriamount)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.Label110)
        Me.Controls.Add(Me.Label38)
        Me.Controls.Add(Me.mem_PaycontDate)
        Me.Controls.Add(Me.txtlocalofficenumber)
        Me.Controls.Add(Me.txtofficenumber)
        Me.Controls.Add(Me.Label23)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(781, 581)
        Me.Name = "frmMember_Master"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Existing Member File"
        Me.tabMember.ResumeLayout(False)
        Me.ClientInfo.ResumeLayout(False)
        Me.ClientInfo.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpPayrollStatus.ResumeLayout(False)
        Me.grpPayrollStatus.PerformLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatutoryInfo.ResumeLayout(False)
        Me.StatutoryInfo.PerformLayout()
        CType(Me.PictureBox25, System.ComponentModel.ISupportInitialize).EndInit()
        Me.AddInfo.ResumeLayout(False)
        Me.AddInfo.PerformLayout()
        CType(Me.PictureBox22, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContactInfo.ResumeLayout(False)
        Me.ContactInfo.PerformLayout()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Ownership.ResumeLayout(False)
        Me.Ownership.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.PictureBox28, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Family.ResumeLayout(False)
        Me.Family.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        CType(Me.PictureBox26, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Dependents.ResumeLayout(False)
        Me.Dependents.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.emp_pic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Benefeciary.ResumeLayout(False)
        Me.Benefeciary.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EducationalInfo.ResumeLayout(False)
        Me.EducationalInfo.PerformLayout()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EmploymentHistory.ResumeLayout(False)
        Me.EmploymentHistory.PerformLayout()
        CType(Me.PictureBox21, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Trainings.ResumeLayout(False)
        Me.Trainings.PerformLayout()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GovExamination.ResumeLayout(False)
        Me.GovExamination.PerformLayout()
        CType(Me.PictureBox23, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Skills.ResumeLayout(False)
        Me.Skills.PerformLayout()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Organizations.ResumeLayout(False)
        Me.Organizations.PerformLayout()
        CType(Me.PictureBox24, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Medical.ResumeLayout(False)
        Me.Medical.PerformLayout()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BankInfo.ResumeLayout(False)
        Me.BankInfo.PerformLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SOIInfo.ResumeLayout(False)
        Me.SOIInfo.PerformLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Discipline.ResumeLayout(False)
        Me.Discipline.PerformLayout()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PerfEval.ResumeLayout(False)
        Me.PerfEval.PerformLayout()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Legal.ResumeLayout(False)
        Me.Legal.PerformLayout()
        CType(Me.PictureBox27, System.ComponentModel.ISupportInitialize).EndInit()
        Me.JobDesc.ResumeLayout(False)
        Me.JobDesc.PerformLayout()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Awards.ResumeLayout(False)
        Me.Awards.PerformLayout()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Requirement.ResumeLayout(False)
        Me.Requirement.PerformLayout()
        CType(Me.PictureBox35, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Contribution.ResumeLayout(False)
        Me.Contribution.PerformLayout()
        CType(Me.PictureBox30, System.ComponentModel.ISupportInitialize).EndInit()
        Me.OtherDeductions.ResumeLayout(False)
        Me.OtherDeductions.PerformLayout()
        CType(Me.PictureBox31, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MonthPay.ResumeLayout(False)
        Me.MonthPay.PerformLayout()
        CType(Me.PictureBox32, System.ComponentModel.ISupportInitialize).EndInit()
        Me.InfoHistory.ResumeLayout(False)
        Me.InfoHistory.PerformLayout()
        CType(Me.PictureBox29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvemail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvphone, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvcivil, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvaddress, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvlastname, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContractHistory.ResumeLayout(False)
        Me.ContractHistory.PerformLayout()
        CType(Me.PictureBox33, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PayrollHis.ResumeLayout(False)
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LeaveLedger.ResumeLayout(False)
        Me.LeaveLedger.PerformLayout()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).EndInit()
        Me.others.ResumeLayout(False)
        Me.others.PerformLayout()
        CType(Me.gridMobileNos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.picEmpSignature, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picEmpPhoto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.PanePanel2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tabMember As System.Windows.Forms.TabControl
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents temp_add3 As System.Windows.Forms.TextBox
    Friend WithEvents temp_add2 As System.Windows.Forms.TextBox
    Friend WithEvents txtofficenumber As System.Windows.Forms.TextBox
    Friend WithEvents txtofficeadd As System.Windows.Forms.TextBox
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox18 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox19 As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents MaskedTextBox5 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents temp_marks As System.Windows.Forms.TextBox
    Friend WithEvents temp_height As System.Windows.Forms.TextBox
    Friend WithEvents temp_citizen As System.Windows.Forms.TextBox
    Friend WithEvents Dependents As System.Windows.Forms.TabPage
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents btndelete_dep As System.Windows.Forms.Button
    Friend WithEvents btnadd_dep As System.Windows.Forms.Button
    Friend WithEvents btnemp_previous As System.Windows.Forms.Button
    Friend WithEvents btnemp_next As System.Windows.Forms.Button
    Friend WithEvents btnemp_add As System.Windows.Forms.Button
    Friend WithEvents btnemp_edit As System.Windows.Forms.Button
    Friend WithEvents btnemp_delete As System.Windows.Forms.Button
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents Address2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnemp_editdepdts As System.Windows.Forms.Button
    Friend WithEvents emp_pic As System.Windows.Forms.PictureBox
    Friend WithEvents BTNSEARCH As System.Windows.Forms.Button
    Friend WithEvents temp_designation As System.Windows.Forms.TextBox
    Friend WithEvents txtEmailAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtresidencephone As System.Windows.Forms.MaskedTextBox
    Friend WithEvents emp_Datehired As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cboemp_civil As System.Windows.Forms.ComboBox
    Friend WithEvents cboemp_gender As System.Windows.Forms.ComboBox
    Friend WithEvents txtEmployeeNo As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboEmp_type As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtlocalofficenumber As System.Windows.Forms.TextBox
    Friend WithEvents txtweight As System.Windows.Forms.TextBox
    Friend WithEvents txtremarks As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents cbosalarygrade As System.Windows.Forms.ComboBox
    Friend WithEvents TXTKEYEMPLOYEEID As System.Windows.Forms.TextBox
    Friend WithEvents TXTRECID As System.Windows.Forms.TextBox
    Friend WithEvents cbotitledesignation As System.Windows.Forms.ComboBox
    Friend WithEvents Dateresigned As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtorgchart As System.Windows.Forms.TextBox
    Friend WithEvents txtfullname As System.Windows.Forms.TextBox
    Friend WithEvents cbodept As System.Windows.Forms.TextBox
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtType_employee As System.Windows.Forms.TextBox
    Friend WithEvents txtfxkeypositionlevel As System.Windows.Forms.TextBox
    Friend WithEvents PanePanel2 As WindowsApplication2.PanePanel
    Friend WithEvents txtkeycompany As System.Windows.Forms.TextBox
    Friend WithEvents txtkeydepartment As System.Windows.Forms.TextBox
    Friend WithEvents txtkeydivision As System.Windows.Forms.TextBox
    Friend WithEvents txtdescsection As System.Windows.Forms.TextBox
    Friend WithEvents txtdescdepartment As System.Windows.Forms.TextBox
    Friend WithEvents txtdescdivision As System.Windows.Forms.TextBox
    Friend WithEvents txtkeysection As System.Windows.Forms.TextBox
    Friend WithEvents txtparent_ID As System.Windows.Forms.TextBox
    Friend WithEvents Benefeciary As System.Windows.Forms.TabPage
    Friend WithEvents lvlNearestRelatives As System.Windows.Forms.ListView
    Friend WithEvents Label98 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents BtnEditContact As System.Windows.Forms.Button
    Friend WithEvents BtnDeleteContact As System.Windows.Forms.Button
    Friend WithEvents btnaddContact As System.Windows.Forms.Button
    Friend WithEvents Label100 As System.Windows.Forms.Label
    Friend WithEvents emp_company As System.Windows.Forms.TextBox
    Friend WithEvents Label107 As System.Windows.Forms.Label
    Friend WithEvents Cbomem_Status As System.Windows.Forms.ComboBox
    Friend WithEvents emp_Ytenures As System.Windows.Forms.TextBox
    Friend WithEvents cboPositionLevel As System.Windows.Forms.ComboBox
    Friend WithEvents Label114 As System.Windows.Forms.Label
    Friend WithEvents BankInfo As System.Windows.Forms.TabPage
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlBankInfo As System.Windows.Forms.ListView
    Friend WithEvents SOIInfo As System.Windows.Forms.TabPage
    Friend WithEvents lvlSourceIncome As System.Windows.Forms.ListView
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents btnUpdateBA As System.Windows.Forms.Button
    Friend WithEvents btnDeleteBA As System.Windows.Forms.Button
    Friend WithEvents btnNewBA As System.Windows.Forms.Button
    Friend WithEvents btnUpdateSInc As System.Windows.Forms.Button
    Friend WithEvents btnDeleteSInc As System.Windows.Forms.Button
    Friend WithEvents btnsaveSInc As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtitemno As System.Windows.Forms.TextBox
    Friend WithEvents txtmonthreqular As System.Windows.Forms.TextBox
    Friend WithEvents txtsalarygrade As System.Windows.Forms.TextBox
    Friend WithEvents txtdateregular As System.Windows.Forms.TextBox
    Friend WithEvents Dateregular As System.Windows.Forms.DateTimePicker
    Friend WithEvents emp_extphone As System.Windows.Forms.MaskedTextBox
    Friend WithEvents btnAddMobileNo As System.Windows.Forms.Button
    Friend WithEvents others As System.Windows.Forms.TabPage
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox9 As System.Windows.Forms.PictureBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents chkIsEloader As System.Windows.Forms.CheckBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents chkShowPINChar As System.Windows.Forms.CheckBox
    Friend WithEvents txtEloaderPIN As System.Windows.Forms.TextBox
    Friend WithEvents txtEloadingLimit As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents gridMobileNos As System.Windows.Forms.DataGridView
    Friend WithEvents txtMobileNo As System.Windows.Forms.MaskedTextBox
    Friend WithEvents btnEditMobile As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog2 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents InfoHistory As System.Windows.Forms.TabPage
    Friend WithEvents dgvemail As System.Windows.Forms.DataGridView
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents dgvphone As System.Windows.Forms.DataGridView
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents dgvcivil As System.Windows.Forms.DataGridView
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents dgvaddress As System.Windows.Forms.DataGridView
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents dgvlastname As System.Windows.Forms.DataGridView
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents cbofCategory As System.Windows.Forms.ComboBox
    Friend WithEvents cbofType As System.Windows.Forms.ComboBox
    Friend WithEvents cbofGroup As System.Windows.Forms.ComboBox
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents ClientInfo As System.Windows.Forms.TabPage
    Friend WithEvents chkBereaveYes As System.Windows.Forms.CheckBox
    Friend WithEvents chkBereaveNo As System.Windows.Forms.CheckBox
    Friend WithEvents mem_WithdrawDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents mem_PaycontDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkNo As System.Windows.Forms.CheckBox
    Friend WithEvents Label110 As System.Windows.Forms.Label
    Friend WithEvents txtpayrollcontriamount As System.Windows.Forms.TextBox
    Friend WithEvents chkyes As System.Windows.Forms.CheckBox
    Friend WithEvents Label109 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents txtwithdrawal As System.Windows.Forms.TextBox
    Friend WithEvents mem_MemberDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label111 As System.Windows.Forms.Label
    Friend WithEvents Label108 As System.Windows.Forms.Label
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label105 As System.Windows.Forms.Label
    Friend WithEvents cboemp_status As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents cboRank As System.Windows.Forms.ComboBox
    Friend WithEvents txtCompanyChart As System.Windows.Forms.TextBox
    Friend WithEvents LvlEmployeeDependent As System.Windows.Forms.ListView
    Friend WithEvents txtHDMF As System.Windows.Forms.TextBox
    Friend WithEvents txtPhilhealth As System.Windows.Forms.TextBox
    Friend WithEvents txtSSS As System.Windows.Forms.TextBox
    Friend WithEvents txtTin As System.Windows.Forms.TextBox
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents dtBoardApproval As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents cboSubgroup As System.Windows.Forms.ComboBox
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents btnemp_close As System.Windows.Forms.Button
    Friend WithEvents EducationalInfo As System.Windows.Forms.TabPage
    Friend WithEvents btnUpdateEI As System.Windows.Forms.Button
    Friend WithEvents btnDeleteEI As System.Windows.Forms.Button
    Friend WithEvents btnNewEI As System.Windows.Forms.Button
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents PictureBox10 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlEducInfo As System.Windows.Forms.ListView
    Friend WithEvents JobDesc As System.Windows.Forms.TabPage
    Friend WithEvents Trainings As System.Windows.Forms.TabPage
    Friend WithEvents Skills As System.Windows.Forms.TabPage
    Friend WithEvents Awards As System.Windows.Forms.TabPage
    Friend WithEvents PerfEval As System.Windows.Forms.TabPage
    Friend WithEvents Discipline As System.Windows.Forms.TabPage
    Friend WithEvents Medical As System.Windows.Forms.TabPage
    Friend WithEvents LeaveLedger As System.Windows.Forms.TabPage
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents btnUpdateJD As System.Windows.Forms.Button
    Friend WithEvents btnDeleteJD As System.Windows.Forms.Button
    Friend WithEvents btnNewJD As System.Windows.Forms.Button
    Friend WithEvents PictureBox11 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlJobDesc As System.Windows.Forms.ListView
    Friend WithEvents Label72 As System.Windows.Forms.Label
    Friend WithEvents btnUpdateT As System.Windows.Forms.Button
    Friend WithEvents btnDeleteT As System.Windows.Forms.Button
    Friend WithEvents btnNewT As System.Windows.Forms.Button
    Friend WithEvents PictureBox12 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlTrainings As System.Windows.Forms.ListView
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents btnUpdateSkills As System.Windows.Forms.Button
    Friend WithEvents btnDeleteSkills As System.Windows.Forms.Button
    Friend WithEvents btnNewSkills As System.Windows.Forms.Button
    Friend WithEvents PictureBox13 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlSkills As System.Windows.Forms.ListView
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents btnUpdateAward As System.Windows.Forms.Button
    Friend WithEvents btnDeleteAward As System.Windows.Forms.Button
    Friend WithEvents btnNewAward As System.Windows.Forms.Button
    Friend WithEvents PictureBox14 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlAwards As System.Windows.Forms.ListView
    Friend WithEvents Label75 As System.Windows.Forms.Label
    Friend WithEvents btnUpdatePE As System.Windows.Forms.Button
    Friend WithEvents btnDeletePE As System.Windows.Forms.Button
    Friend WithEvents btnNewPE As System.Windows.Forms.Button
    Friend WithEvents PictureBox15 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlPerfEval As System.Windows.Forms.ListView
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents btnUpdateDisc As System.Windows.Forms.Button
    Friend WithEvents btnDeleteDisc As System.Windows.Forms.Button
    Friend WithEvents btnNewDisc As System.Windows.Forms.Button
    Friend WithEvents PictureBox16 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlDiscipline As System.Windows.Forms.ListView
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents btnUpdateMed As System.Windows.Forms.Button
    Friend WithEvents btnDeleteMed As System.Windows.Forms.Button
    Friend WithEvents btnNewMed As System.Windows.Forms.Button
    Friend WithEvents PictureBox17 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlMedical As System.Windows.Forms.ListView
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents PictureBox18 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlLeave As System.Windows.Forms.ListView
    Friend WithEvents cboLeave As System.Windows.Forms.ComboBox
    Friend WithEvents txtCompanyName As System.Windows.Forms.TextBox
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents btnPEDownload As System.Windows.Forms.Button
    Friend WithEvents btnTDownload As System.Windows.Forms.Button
    Friend WithEvents btnDDownload As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents btnMedUpload As System.Windows.Forms.Button
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents EmploymentHistory As System.Windows.Forms.TabPage
    Friend WithEvents Label81 As System.Windows.Forms.Label
    Friend WithEvents PictureBox21 As System.Windows.Forms.PictureBox
    Friend WithEvents Label83 As System.Windows.Forms.Label
    Friend WithEvents cboClient As System.Windows.Forms.ComboBox
    Friend WithEvents Label90 As System.Windows.Forms.Label
    Friend WithEvents Label89 As System.Windows.Forms.Label
    Friend WithEvents Label92 As System.Windows.Forms.Label
    Friend WithEvents Label93 As System.Windows.Forms.Label
    Friend WithEvents txtage1 As System.Windows.Forms.TextBox
    Friend WithEvents lblage As System.Windows.Forms.Label
    Friend WithEvents Label87 As System.Windows.Forms.Label
    Friend WithEvents EMP_DATEbirth As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents temp_placebirth As System.Windows.Forms.TextBox
    Friend WithEvents temp_lname As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents temp_midname As System.Windows.Forms.TextBox
    Friend WithEvents temp_fname As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtpk_Employee As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents txtFacebook2 As System.Windows.Forms.TextBox
    Friend WithEvents txtFacebook1 As System.Windows.Forms.TextBox
    Friend WithEvents txtemailaddress2 As System.Windows.Forms.TextBox
    Friend WithEvents txtresidencephone2 As System.Windows.Forms.TextBox
    Friend WithEvents txtLandline2 As System.Windows.Forms.TextBox
    Friend WithEvents txtLandline As System.Windows.Forms.TextBox
    Friend WithEvents txtW As System.Windows.Forms.TextBox
    Friend WithEvents txtHeightFt As System.Windows.Forms.TextBox
    Friend WithEvents txtNickname As System.Windows.Forms.TextBox
    Friend WithEvents StatutoryInfo As System.Windows.Forms.TabPage
    Friend WithEvents txtCommonrefNo As System.Windows.Forms.TextBox
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents GovExamination As System.Windows.Forms.TabPage
    Friend WithEvents btnUpdateGE As System.Windows.Forms.Button
    Friend WithEvents btnDeleteGE As System.Windows.Forms.Button
    Friend WithEvents btnNewGE As System.Windows.Forms.Button
    Friend WithEvents lvlGovExam As System.Windows.Forms.ListView
    Friend WithEvents Label91 As System.Windows.Forms.Label
    Friend WithEvents PictureBox23 As System.Windows.Forms.PictureBox
    Friend WithEvents btnUpdateEH As System.Windows.Forms.Button
    Friend WithEvents btnDeleteEH As System.Windows.Forms.Button
    Friend WithEvents btnNewEH As System.Windows.Forms.Button
    Friend WithEvents Label106 As System.Windows.Forms.Label
    Friend WithEvents PictureBox25 As System.Windows.Forms.PictureBox
    Friend WithEvents Organizations As System.Windows.Forms.TabPage
    Friend WithEvents btnUpdateOrg As System.Windows.Forms.Button
    Friend WithEvents btnDeleteOrg As System.Windows.Forms.Button
    Friend WithEvents btnNewOrg As System.Windows.Forms.Button
    Friend WithEvents lvlOrg As System.Windows.Forms.ListView
    Friend WithEvents Label104 As System.Windows.Forms.Label
    Friend WithEvents PictureBox24 As System.Windows.Forms.PictureBox
    Friend WithEvents Family As System.Windows.Forms.TabPage
    Friend WithEvents txtFAdd As System.Windows.Forms.TextBox
    Friend WithEvents Label119 As System.Windows.Forms.Label
    Friend WithEvents txtMotherName As System.Windows.Forms.TextBox
    Friend WithEvents Label116 As System.Windows.Forms.Label
    Friend WithEvents txtFatherName As System.Windows.Forms.TextBox
    Friend WithEvents Label115 As System.Windows.Forms.Label
    Friend WithEvents Label113 As System.Windows.Forms.Label
    Friend WithEvents PictureBox26 As System.Windows.Forms.PictureBox
    Friend WithEvents txtSpouseStay As System.Windows.Forms.TextBox
    Friend WithEvents rbSpouseYears As System.Windows.Forms.RadioButton
    Friend WithEvents rbSpouseMonth As System.Windows.Forms.RadioButton
    Friend WithEvents Label131 As System.Windows.Forms.Label
    Friend WithEvents txtSpouseAdd As System.Windows.Forms.TextBox
    Friend WithEvents Label133 As System.Windows.Forms.Label
    Friend WithEvents btnSpouseSearchAdd As System.Windows.Forms.Button
    Friend WithEvents txtOccupation As System.Windows.Forms.TextBox
    Friend WithEvents Label130 As System.Windows.Forms.Label
    Friend WithEvents txtSpouseContact As System.Windows.Forms.TextBox
    Friend WithEvents Label128 As System.Windows.Forms.Label
    Friend WithEvents txtSpouseName As System.Windows.Forms.TextBox
    Friend WithEvents Label129 As System.Windows.Forms.Label
    Friend WithEvents txtMContact As System.Windows.Forms.TextBox
    Friend WithEvents Label125 As System.Windows.Forms.Label
    Friend WithEvents txtFContact As System.Windows.Forms.TextBox
    Friend WithEvents Label124 As System.Windows.Forms.Label
    Friend WithEvents btnMSearchAdd As System.Windows.Forms.Button
    Friend WithEvents btnFSearchAdd As System.Windows.Forms.Button
    Friend WithEvents txtMAdd As System.Windows.Forms.TextBox
    Friend WithEvents Label120 As System.Windows.Forms.Label
    Friend WithEvents txtChildNo As System.Windows.Forms.TextBox
    Friend WithEvents Label134 As System.Windows.Forms.Label
    Friend WithEvents lvlEmploymentHistory As System.Windows.Forms.ListView
    Friend WithEvents Label135 As System.Windows.Forms.Label
    Friend WithEvents Label84 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label88 As System.Windows.Forms.Label
    Friend WithEvents grpPayrollStatus As System.Windows.Forms.GroupBox
    Friend WithEvents rdoNonExempt As System.Windows.Forms.RadioButton
    Friend WithEvents rdoExempt As System.Windows.Forms.RadioButton
    Friend WithEvents cboOutlet As System.Windows.Forms.ComboBox
    Friend WithEvents txtContractPrice As System.Windows.Forms.TextBox
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents txtEcola As System.Windows.Forms.TextBox
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents cboPaycode As System.Windows.Forms.ComboBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents cbotaxcode As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnrestricted As System.Windows.Forms.Button
    Friend WithEvents cbopayroll As System.Windows.Forms.ComboBox
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents cborate As System.Windows.Forms.ComboBox
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents txtbasicpay As System.Windows.Forms.TextBox
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Legal As System.Windows.Forms.TabPage
    Friend WithEvents Label136 As System.Windows.Forms.Label
    Friend WithEvents btnUpdateLC As System.Windows.Forms.Button
    Friend WithEvents btnDeleteLC As System.Windows.Forms.Button
    Friend WithEvents btnNewLC As System.Windows.Forms.Button
    Friend WithEvents PictureBox27 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlLegal As System.Windows.Forms.ListView
    Friend WithEvents Label138 As System.Windows.Forms.Label
    Friend WithEvents Label137 As System.Windows.Forms.Label
    Friend WithEvents Label139 As System.Windows.Forms.Label
    Friend WithEvents txtHeightInch As System.Windows.Forms.TextBox
    Friend WithEvents Ownership As System.Windows.Forms.TabPage
    Friend WithEvents Label140 As System.Windows.Forms.Label
    Friend WithEvents PictureBox28 As System.Windows.Forms.PictureBox
    Friend WithEvents Label141 As System.Windows.Forms.Label
    Friend WithEvents PictureBox29 As System.Windows.Forms.PictureBox
    Friend WithEvents txtCar As System.Windows.Forms.TextBox
    Friend WithEvents txtMotor As System.Windows.Forms.TextBox
    Friend WithEvents Label146 As System.Windows.Forms.Label
    Friend WithEvents Label145 As System.Windows.Forms.Label
    Friend WithEvents Label144 As System.Windows.Forms.Label
    Friend WithEvents Label143 As System.Windows.Forms.Label
    Friend WithEvents Label142 As System.Windows.Forms.Label
    Friend WithEvents PayrollHis As System.Windows.Forms.TabPage
    Friend WithEvents btnPayrollHistory As System.Windows.Forms.Button
    Friend WithEvents PictureBox19 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlPayrollHistory As System.Windows.Forms.ListView
    Friend WithEvents rbCNo As System.Windows.Forms.RadioButton
    Friend WithEvents rbCYes As System.Windows.Forms.RadioButton
    Friend WithEvents rbMNo As System.Windows.Forms.RadioButton
    Friend WithEvents rbMYes As System.Windows.Forms.RadioButton
    Friend WithEvents rbParents As System.Windows.Forms.RadioButton
    Friend WithEvents rbRented As System.Windows.Forms.RadioButton
    Friend WithEvents rbOwned As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Contribution As System.Windows.Forms.TabPage
    Friend WithEvents lvlContri As System.Windows.Forms.ListView
    Friend WithEvents cboContribution As System.Windows.Forms.ComboBox
    Friend WithEvents Label147 As System.Windows.Forms.Label
    Friend WithEvents PictureBox30 As System.Windows.Forms.PictureBox
    Friend WithEvents OtherDeductions As System.Windows.Forms.TabPage
    Friend WithEvents Label148 As System.Windows.Forms.Label
    Friend WithEvents PictureBox31 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlDeductions As System.Windows.Forms.ListView
    Friend WithEvents MonthPay As System.Windows.Forms.TabPage
    Friend WithEvents lvl13thMonth As System.Windows.Forms.ListView
    Friend WithEvents Label149 As System.Windows.Forms.Label
    Friend WithEvents PictureBox32 As System.Windows.Forms.PictureBox
    Friend WithEvents ContractHistory As System.Windows.Forms.TabPage
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents btnSetContract As System.Windows.Forms.Button
    Friend WithEvents PictureBox33 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlContractHis As System.Windows.Forms.ListView
    Friend WithEvents btnContractRate As System.Windows.Forms.Button
    Friend WithEvents txtContractrate As System.Windows.Forms.TextBox
    Friend WithEvents Label150 As System.Windows.Forms.Label
    Friend WithEvents AddInfo As System.Windows.Forms.TabPage
    Friend WithEvents Label158 As System.Windows.Forms.Label
    Friend WithEvents Label153 As System.Windows.Forms.Label
    Friend WithEvents Label160 As System.Windows.Forms.Label
    Friend WithEvents Label161 As System.Windows.Forms.Label
    Friend WithEvents Label155 As System.Windows.Forms.Label
    Friend WithEvents Label156 As System.Windows.Forms.Label
    Friend WithEvents Label162 As System.Windows.Forms.Label
    Friend WithEvents Label157 As System.Windows.Forms.Label
    Friend WithEvents txtPermProvince As System.Windows.Forms.TextBox
    Friend WithEvents txtPermCity As System.Windows.Forms.TextBox
    Friend WithEvents txtPermSitio As System.Windows.Forms.TextBox
    Friend WithEvents txtPermStreet As System.Windows.Forms.TextBox
    Friend WithEvents txtPermBldg As System.Windows.Forms.TextBox
    Friend WithEvents txtPrevProvince As System.Windows.Forms.TextBox
    Friend WithEvents txtPrevCity As System.Windows.Forms.TextBox
    Friend WithEvents txtPrevSitio As System.Windows.Forms.TextBox
    Friend WithEvents txtPrevStreet As System.Windows.Forms.TextBox
    Friend WithEvents txtPrevBldg As System.Windows.Forms.TextBox
    Friend WithEvents Label151 As System.Windows.Forms.Label
    Friend WithEvents txtPresProvince As System.Windows.Forms.TextBox
    Friend WithEvents Label152 As System.Windows.Forms.Label
    Friend WithEvents txtPresCity As System.Windows.Forms.TextBox
    Friend WithEvents Label99 As System.Windows.Forms.Label
    Friend WithEvents txtPresSitio As System.Windows.Forms.TextBox
    Friend WithEvents Label97 As System.Windows.Forms.Label
    Friend WithEvents txtPresStreet As System.Windows.Forms.TextBox
    Friend WithEvents Label96 As System.Windows.Forms.Label
    Friend WithEvents txtPresBldg As System.Windows.Forms.TextBox
    Friend WithEvents cboStay3 As System.Windows.Forms.ComboBox
    Friend WithEvents cboStay2 As System.Windows.Forms.ComboBox
    Friend WithEvents cboStay1 As System.Windows.Forms.ComboBox
    Friend WithEvents txtStay3 As System.Windows.Forms.TextBox
    Friend WithEvents txtStay2 As System.Windows.Forms.TextBox
    Friend WithEvents txtStay1 As System.Windows.Forms.TextBox
    Friend WithEvents Label103 As System.Windows.Forms.Label
    Friend WithEvents Label102 As System.Windows.Forms.Label
    Friend WithEvents Label101 As System.Windows.Forms.Label
    Friend WithEvents Label95 As System.Windows.Forms.Label
    Friend WithEvents Label94 As System.Windows.Forms.Label
    Friend WithEvents btnSearchPermAddress As System.Windows.Forms.Button
    Friend WithEvents PictureBox22 As System.Windows.Forms.PictureBox
    Friend WithEvents Label85 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnSearchProvince As System.Windows.Forms.Button
    Friend WithEvents btnSearchHome As System.Windows.Forms.Button
    Friend WithEvents Label164 As System.Windows.Forms.Label
    Friend WithEvents Label163 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents rbEmployee As System.Windows.Forms.RadioButton
    Friend WithEvents rbClient As System.Windows.Forms.RadioButton
    Friend WithEvents txtSuffix As System.Windows.Forms.TextBox
    Friend WithEvents Label165 As System.Windows.Forms.Label
    Friend WithEvents ContactInfo As System.Windows.Forms.TabPage
    Friend WithEvents Label80 As System.Windows.Forms.Label
    Friend WithEvents PictureBox20 As System.Windows.Forms.PictureBox
    Friend WithEvents btnPrevTab As System.Windows.Forms.Button
    Friend WithEvents btnNextTab As System.Windows.Forms.Button
    Friend WithEvents Requirement As System.Windows.Forms.TabPage
    Friend WithEvents btnUpload As System.Windows.Forms.Button
    Friend WithEvents Label167 As System.Windows.Forms.Label
    Friend WithEvents btnAttach As System.Windows.Forms.Button
    Friend WithEvents PictureBox35 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlAttachment As System.Windows.Forms.ListView
    Friend WithEvents Label170 As System.Windows.Forms.Label
    Friend WithEvents Label169 As System.Windows.Forms.Label
    Friend WithEvents Label171 As System.Windows.Forms.Label
    Friend WithEvents Label154 As System.Windows.Forms.Label
    Friend WithEvents Label132 As System.Windows.Forms.Label
    Friend WithEvents btnLegalUpload As System.Windows.Forms.Button
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents btnUpdateSibling As System.Windows.Forms.Button
    Friend WithEvents btnNewSibling As System.Windows.Forms.Button
    Friend WithEvents lvlSibling As System.Windows.Forms.ListView
    Friend WithEvents txtMemberID As System.Windows.Forms.TextBox
    Friend WithEvents txtDepartment2 As System.Windows.Forms.TextBox
    Friend WithEvents lblemployee_name As System.Windows.Forms.Label
    Friend WithEvents label As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnMaxSignature As System.Windows.Forms.Button
    Friend WithEvents btnMaxPhoto As System.Windows.Forms.Button
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents btnBrowseSignature As System.Windows.Forms.Button
    Friend WithEvents picEmpSignature As System.Windows.Forms.PictureBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents btnBrowsePicture As System.Windows.Forms.Button
    Friend WithEvents picEmpPhoto As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents cboContactPerson As System.Windows.Forms.ComboBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents txtProject As System.Windows.Forms.TextBox
    Friend WithEvents Label44 As System.Windows.Forms.Label
End Class
