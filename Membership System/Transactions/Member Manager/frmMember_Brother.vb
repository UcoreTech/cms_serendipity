﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Public Class frmMember_Brother

    Public pkSibling As String

#Region "Property"
    Public Property getpkSibling() As String
        Get
            Return pkSibling
        End Get
        Set(ByVal value As String)
            pkSibling = value
        End Set
    End Property
#End Region

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub AddEditSiblings(ByVal EmployeeNo As String, ByVal Name As String, ByVal Add As String, ByVal Contact As String, ByVal SiblingType As String, ByVal pk_Sibling As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_FamilyBackground_AddSibling", _
                                      New SqlParameter("@employeeNo", EmployeeNo), _
                                     New SqlParameter("@Name", Name), _
                                     New SqlParameter("@Add", Add), _
                                     New SqlParameter("@Contact", Contact), _
                                     New SqlParameter("@SiblingType", SiblingType), _
                                     New SqlParameter("@pk_Sibling", pk_Sibling))
            trans.Commit()
            MessageBox.Show("Record has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Save Sibling")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Call AddEditSiblings(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtName.Text.Trim, Me.txtAdd.Text.Trim, Me.txtContact.Text.Trim, Me.cboSiblingType.Text.Trim, "")
        Call frmMember_Master.GetmemberFamilySibling(frmMember_Master.txtEmployeeNo.Text)
        Call ClearText()
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Call AddEditSiblings(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtName.Text.Trim, Me.txtAdd.Text.Trim, Me.txtContact.Text.Trim, Me.cboSiblingType.Text.Trim, pkSibling)
        Call frmMember_Master.GetmemberFamilySibling(frmMember_Master.txtEmployeeNo.Text)
        Me.Close()
    End Sub

    Private Sub ClearText()
        cboSiblingType.Text = ""
        txtAdd.Text = ""
        txtContact.Text = ""
        txtName.Text = ""
    End Sub
End Class