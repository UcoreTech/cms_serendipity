﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMember_SetContract
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMember_SetContract))
        Me.lblContract = New System.Windows.Forms.Label()
        Me.PanePanel2 = New WindowsApplication2.PanePanel()
        Me.btnupdate = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnclose = New System.Windows.Forms.Button()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.txtCompanyChart = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.picOrg = New System.Windows.Forms.PictureBox()
        Me.cboCPosition = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnContractRate = New System.Windows.Forms.Button()
        Me.txtContractrate = New System.Windows.Forms.TextBox()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.dtCStart = New System.Windows.Forms.DateTimePicker()
        Me.dtCEnd = New System.Windows.Forms.DateTimePicker()
        Me.txtApprover = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.PanePanel2.SuspendLayout()
        Me.PanePanel1.SuspendLayout()
        CType(Me.picOrg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblContract
        '
        Me.lblContract.AutoSize = True
        Me.lblContract.BackColor = System.Drawing.Color.Transparent
        Me.lblContract.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContract.ForeColor = System.Drawing.Color.White
        Me.lblContract.Location = New System.Drawing.Point(15, 3)
        Me.lblContract.Name = "lblContract"
        Me.lblContract.Size = New System.Drawing.Size(21, 19)
        Me.lblContract.TabIndex = 18
        Me.lblContract.Text = "..."
        '
        'PanePanel2
        '
        Me.PanePanel2.BackColor = System.Drawing.Color.White
        Me.PanePanel2.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel2.Controls.Add(Me.btnupdate)
        Me.PanePanel2.Controls.Add(Me.btnSave)
        Me.PanePanel2.Controls.Add(Me.btnclose)
        Me.PanePanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel2.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel2.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel2.Location = New System.Drawing.Point(0, 157)
        Me.PanePanel2.Name = "PanePanel2"
        Me.PanePanel2.Size = New System.Drawing.Size(436, 37)
        Me.PanePanel2.TabIndex = 52
        '
        'btnupdate
        '
        Me.btnupdate.Image = CType(resources.GetObject("btnupdate.Image"), System.Drawing.Image)
        Me.btnupdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnupdate.Location = New System.Drawing.Point(139, 5)
        Me.btnupdate.Name = "btnupdate"
        Me.btnupdate.Size = New System.Drawing.Size(66, 28)
        Me.btnupdate.TabIndex = 7
        Me.btnupdate.Text = "Update"
        Me.btnupdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnupdate.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(5, 5)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(66, 28)
        Me.btnSave.TabIndex = 5
        Me.btnSave.Text = "Save"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnclose
        '
        Me.btnclose.Image = Global.WindowsApplication2.My.Resources.Resources.eventlogError
        Me.btnclose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnclose.Location = New System.Drawing.Point(72, 5)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.Size = New System.Drawing.Size(66, 28)
        Me.btnclose.TabIndex = 6
        Me.btnclose.Text = "Cancel"
        Me.btnclose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnclose.UseVisualStyleBackColor = True
        '
        'PanePanel1
        '
        Me.PanePanel1.BackColor = System.Drawing.Color.White
        Me.PanePanel1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.lblContract)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel1.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(436, 30)
        Me.PanePanel1.TabIndex = 51
        '
        'txtCompanyChart
        '
        Me.txtCompanyChart.Enabled = False
        Me.txtCompanyChart.Location = New System.Drawing.Point(95, 37)
        Me.txtCompanyChart.Name = "txtCompanyChart"
        Me.txtCompanyChart.Size = New System.Drawing.Size(292, 20)
        Me.txtCompanyChart.TabIndex = 120
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(8, 38)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(58, 15)
        Me.Label12.TabIndex = 118
        Me.Label12.Text = "Company"
        '
        'picOrg
        '
        Me.picOrg.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picOrg.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.picOrg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picOrg.Image = CType(resources.GetObject("picOrg.Image"), System.Drawing.Image)
        Me.picOrg.Location = New System.Drawing.Point(394, 38)
        Me.picOrg.Name = "picOrg"
        Me.picOrg.Size = New System.Drawing.Size(25, 18)
        Me.picOrg.TabIndex = 119
        Me.picOrg.TabStop = False
        '
        'cboCPosition
        '
        Me.cboCPosition.FormattingEnabled = True
        Me.cboCPosition.Location = New System.Drawing.Point(94, 61)
        Me.cboCPosition.Name = "cboCPosition"
        Me.cboCPosition.Size = New System.Drawing.Size(187, 21)
        Me.cboCPosition.TabIndex = 121
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 63)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 15)
        Me.Label1.TabIndex = 122
        Me.Label1.Text = "Position"
        '
        'btnContractRate
        '
        Me.btnContractRate.Location = New System.Drawing.Point(320, 83)
        Me.btnContractRate.Name = "btnContractRate"
        Me.btnContractRate.Size = New System.Drawing.Size(45, 21)
        Me.btnContractRate.TabIndex = 128
        Me.btnContractRate.Text = "....."
        Me.btnContractRate.UseVisualStyleBackColor = True
        '
        'txtContractrate
        '
        Me.txtContractrate.Enabled = False
        Me.txtContractrate.Location = New System.Drawing.Point(93, 85)
        Me.txtContractrate.Name = "txtContractrate"
        Me.txtContractrate.Size = New System.Drawing.Size(221, 20)
        Me.txtContractrate.TabIndex = 127
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label82.Location = New System.Drawing.Point(8, 87)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(82, 15)
        Me.Label82.TabIndex = 126
        Me.Label82.Text = "Contract Rate"
        '
        'dtCStart
        '
        Me.dtCStart.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtCStart.Location = New System.Drawing.Point(93, 108)
        Me.dtCStart.Name = "dtCStart"
        Me.dtCStart.Size = New System.Drawing.Size(103, 20)
        Me.dtCStart.TabIndex = 129
        '
        'dtCEnd
        '
        Me.dtCEnd.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtCEnd.Location = New System.Drawing.Point(284, 108)
        Me.dtCEnd.Name = "dtCEnd"
        Me.dtCEnd.Size = New System.Drawing.Size(103, 20)
        Me.dtCEnd.TabIndex = 130
        '
        'txtApprover
        '
        Me.txtApprover.Location = New System.Drawing.Point(93, 131)
        Me.txtApprover.Name = "txtApprover"
        Me.txtApprover.Size = New System.Drawing.Size(326, 20)
        Me.txtApprover.TabIndex = 132
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 133)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 15)
        Me.Label2.TabIndex = 131
        Me.Label2.Text = "Approver"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(8, 110)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(84, 15)
        Me.Label3.TabIndex = 133
        Me.Label3.Text = "Contract Start"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(204, 110)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(77, 15)
        Me.Label4.TabIndex = 134
        Me.Label4.Text = "Contract End"
        '
        'frmMember_SetContract
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(436, 194)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtApprover)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dtCEnd)
        Me.Controls.Add(Me.dtCStart)
        Me.Controls.Add(Me.btnContractRate)
        Me.Controls.Add(Me.txtContractrate)
        Me.Controls.Add(Me.Label82)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cboCPosition)
        Me.Controls.Add(Me.txtCompanyChart)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.picOrg)
        Me.Controls.Add(Me.PanePanel2)
        Me.Controls.Add(Me.PanePanel1)
        Me.Name = "frmMember_SetContract"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Set Contract"
        Me.PanePanel2.ResumeLayout(False)
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        CType(Me.picOrg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblContract As System.Windows.Forms.Label
    Friend WithEvents PanePanel2 As WindowsApplication2.PanePanel
    Friend WithEvents btnupdate As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnclose As System.Windows.Forms.Button
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents txtCompanyChart As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents picOrg As System.Windows.Forms.PictureBox
    Friend WithEvents cboCPosition As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnContractRate As System.Windows.Forms.Button
    Friend WithEvents txtContractrate As System.Windows.Forms.TextBox
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents dtCStart As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtCEnd As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtApprover As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
End Class
