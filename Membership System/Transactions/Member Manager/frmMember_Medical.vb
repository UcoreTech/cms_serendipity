﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Public Class frmMember_Medical

    Public pkMedicals As String
    Dim openFileDialog1 As New OpenFileDialog()
    Dim fpath As String
    Dim filenym As String
    Dim fileData As Byte()
    Dim ex As String
    Dim afiles As String
    Dim filefromdb As Byte()
    Public filefromMem As String

#Region "Property"
    Public Property getpkMedicals() As String
        Get
            Return pkMedicals
        End Get
        Set(ByVal value As String)
            pkMedicals = value
        End Set
    End Property
#End Region

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub AddEditMedicals(ByVal EmpNo As String, ByVal DateFiled As Date, ByVal ClaimType As String, ByVal Confinement As String, ByVal Status As String,
                                ByVal AttachmentName As String, ByVal Attachment As Byte(), ByVal pk_Medicals As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Medical_AddEdit", _
                                     New SqlParameter("@employeeNo", EmpNo), _
                                     New SqlParameter("@DateFiled", DateFiled), _
                                     New SqlParameter("@ClaimType", ClaimType), _
                                     New SqlParameter("@Confinement", Confinement), _
                                     New SqlParameter("@PayeeNo", txtPayeeNumber.Text), _
                                     New SqlParameter("@PayeeName", txtPayeeName.Text), _
                                     New SqlParameter("@Address", txtAddress.Text), _
                                     New SqlParameter("@Amount", txtAmountPaid.Text), _
                                     New SqlParameter("@SettlementDate", dtSettlement.Value), _
                                     New SqlParameter("@CheckNo", txtCheckNo.Text), _
                                     New SqlParameter("ConfineFrom", dtFrom.Value), _
                                     New SqlParameter("@ConfineTo", dtdateTo.Value), _
                                     New SqlParameter("@Status", Status), _
                                     New SqlParameter("@FileName", AttachmentName), _
                                     New SqlParameter("@AttachedFiles", Attachment), _
                                     New SqlParameter("@pk_Medicals", pk_Medicals))
            trans.Commit()
            MessageBox.Show("Record has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Save Medical")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Call AddEditMedicals(frmMember_Master.txtEmployeeNo.Text, dtDateFiled.Value, txtClaimType.Text, txtConfineDate.Text, txtStat.Text,
                             txtAttachFiles.Text, fileData, "")
        Call frmMember_Master.GetmemberMedicals(frmMember_Master.txtEmployeeNo.Text)
        Call ClearText()
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        If fileData Is Nothing Then
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("MSS_MembersInfo_GetMedicals_AttachedFiles", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            myconnection.sqlconn.Open()
            cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = frmMember_Master.txtEmployeeNo.Text
            cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 100).Value = txtAttachFiles.Text
            Using myreader As SqlDataReader = cmd.ExecuteReader
                myreader.Read()
                filefromdb = DirectCast(myreader("fcAttachedFiles"), Byte())
            End Using
            Call AddEditMedicals(frmMember_Master.txtEmployeeNo.Text, dtDateFiled.Value, txtClaimType.Text, txtConfineDate.Text, txtStat.Text,
                                 txtAttachFiles.Text, filefromdb, pkMedicals)
            Call frmMember_Master.GetmemberTrainings(frmMember_Master.txtEmployeeNo.Text)
            Me.Close()
        Else
            Call AddEditMedicals(frmMember_Master.txtEmployeeNo.Text, dtDateFiled.Value, txtClaimType.Text, txtConfineDate.Text, txtStat.Text,
                                 txtAttachFiles.Text, fileData, pkMedicals)
            Call frmMember_Master.GetmemberMedicals(frmMember_Master.txtEmployeeNo.Text)
            Me.Close()
        End If
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtdateTo.ValueChanged
        txtConfineDate.Text = dtdateTo.Value.Subtract(dtFrom.Value).Days
    End Sub

    Private Sub dtFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtFrom.ValueChanged
        txtConfineDate.Text = dtdateTo.Value.Subtract(dtFrom.Value).Days
    End Sub

    Private Sub txtAmountPaid_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAmountPaid.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                MessageBox.Show("Invalid Input! This field Allow 0-9 Only.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub ClearText()
        txtAddress.Text = ""
        txtAmountPaid.Text = ""
        txtAttachFiles.Text = ""
        txtCheckNo.Text = ""
        txtClaimType.Text = ""
        txtConfineDate.Text = ""
        txtPayeeName.Text = ""
        txtPayeeNumber.Text = ""
        txtStat.Text = ""
        TxtStatus.Text = ""
        txtAttachFiles.Text = ""
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Try
            openFileDialog1.Filter = "Text Files (*.txt)|*.txt|PDF Files (*.pdf)|*.pdf|Word Documents (*.docx)|*.docx|Excel Worksheets (*.xlsx)|*.xlsx|PowerPoint Presentations (*.pptx)|*.pptx|Word Documents 97-2003 (*.doc)|*.docx|Excel Worksheets 93-2003 (*.xls)|*.xls|PowerPoint Presentations (*.ppt)|*.ppt" & "|Office Files|*.docx;*.xlsx;*.pptx;*.doc;*.xls;*.ppt" & "|Images|*.jpg;*.png;*.gif"
            openFileDialog1.Title = "Select File"
            openFileDialog1.CheckFileExists = True
            If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                filenym = openFileDialog1.FileName
                fpath = Path.GetExtension(filenym)
                txtAttachFiles.Text = Path.GetFileName(filenym)

                fileData = File.ReadAllBytes(filenym)
                ex = Path.GetExtension(LTrim(RTrim(filenym)))

                Dim ms As New MemoryStream(fileData, 0, fileData.Length)

                ms.Write(fileData, 0, fileData.Length)

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class