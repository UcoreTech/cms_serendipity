﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Public Class frmMember_PerfEval

    Dim pkEvaluation As String
    Dim fpath As String
    Dim filenym As String
    Dim fileData As Byte()
    Dim ex As String
    Dim files As String
    Dim picData As Byte()
    Dim img As Image
    'Dim FileDirectory As String
    Public FilefromMem As String
    Dim filesFromdb As Byte()

#Region "Property"
    Public Property getpkEvaluation() As String
        Get
            Return pkEvaluation
        End Get
        Set(ByVal value As String)
            pkEvaluation = value
        End Set
    End Property
#End Region

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Try
            Dim openFileDialog1 As New OpenFileDialog()

            openFileDialog1.Filter = "Text Files (*.txt)|*.txt|PDF Files (*.pdf)|*.pdf|Word Documents (*.docx)|*.docx|Excel Worksheets (*.xlsx)|*.xlsx|PowerPoint Presentations (*.pptx)|*.pptx|Word Documents 97-2003 (*.doc)|*.docx|Excel Worksheets 93-2003 (*.xls)|*.xls|PowerPoint Presentations (*.ppt)|*.ppt" & "|Office Files|*.docx;*.xlsx;*.pptx;*.doc;*.xls;*.ppt" & "|Images|*.jpg;*.png;*.gif"
            openFileDialog1.Title = "Select File"
            If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                filenym = openFileDialog1.FileName
                fpath = Path.GetExtension(filenym)
                txtAttachFiles.Text = Path.GetFileName(filenym)

                'If fpath = ".docx" Or fpath = ".pptx" Or fpath = ".xlsx" Or fpath = ".txt" Or fpath = ".pdf" Or fpath = ".xls" Or fpath = ".doc" Or fpath = ".ppt" Then
                fileData = File.ReadAllBytes(filenym)
                ex = Path.GetExtension(LTrim(RTrim(filenym)))

                Dim ms As New MemoryStream(fileData, 0, fileData.Length)

                ms.Write(fileData, 0, fileData.Length)
                'ElseIf fpath = ".jpg" Or fpath = ".png" Or fpath = ".png" Then
                '    picData = File.ReadAllBytes(filenym)
                '    ex = Path.GetExtension(LTrim(RTrim(filenym)))
                '    Dim ms As New MemoryStream(picData, 0, picData.Length)
                '    ms.Write(picData, 0, picData.Length)
                '    txtAttachFiles.Text = filenym
                'End If
            Else
                'MessageBox.Show("Unable to Upload File!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                MessageBox.Show("File extension " + fpath + " is not allowed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                'MessageBox.Show("File extension " + fpath + " is not allowed")
                'End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            '    Exit Sub
        End Try
    End Sub

    Private Sub AddEditEvaluation(ByVal EmpNo As String, ByVal Desc As String, ByVal dtFrom As String, ByVal dtTo As String,
                                  ByVal EvaluatedBy As String, ByVal AttachName As String, ByVal Attachment As Byte(), ByVal pk_Evaluation As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_PerfEval_AddEdit", _
                                     New SqlParameter("@employeeNo", EmpNo), _
                                     New SqlParameter("@Description", Desc), _
                                     New SqlParameter("@From", dtFrom), _
                                     New SqlParameter("@To", dtTo), _
                                     New SqlParameter("@EvaluatedBy", EvaluatedBy), _
                                     New SqlParameter("@FileName", AttachName), _
                                     New SqlParameter("@AttachedFiles", Attachment), _
                                     New SqlParameter("@pk_Evaluations", pk_Evaluation))
            trans.Commit()
            MessageBox.Show("Record has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Save Performance Evaluation")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtEvalDesc.Text = "" Or txtFrom.Text = "" Or txtTo.Text = "" Or txtEvalBy.Text = "" Then
            MessageBox.Show("Complete Information is Needed", "Performance Evaluation Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Call AddEditEvaluation(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtEvalDesc.Text.Trim, Me.txtFrom.Text.Trim,
                                   Me.txtTo.Text.Trim, Me.txtEvalBy.Text.Trim, txtAttachFiles.Text, fileData, "")
            Call frmMember_Master.GetmemberPerfEvaluation(frmMember_Master.txtEmployeeNo.Text)
            Call ClearText()
            'Me.Close()
        End If
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        If fileData Is Nothing Then
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("MSS_MembersInfo_GetPerfEval_AttachedFiles", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            myconnection.sqlconn.Open()
            cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = frmMember_Master.txtEmployeeNo.Text
            cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 100).Value = txtAttachFiles.Text
            Using myreader As SqlDataReader = cmd.ExecuteReader
                myreader.Read()
                filesFromdb = DirectCast(myreader("fcAttachedFiles"), Byte())
            End Using
            Call AddEditEvaluation(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtEvalDesc.Text.Trim, Me.txtFrom.Text.Trim,
                                       Me.txtTo.Text.Trim, Me.txtEvalBy.Text.Trim, txtAttachFiles.Text, filesFromdb, pkEvaluation)
            Call frmMember_Master.GetmemberPerfEvaluation(frmMember_Master.txtEmployeeNo.Text)
            Me.Close()
        Else
            Call AddEditEvaluation(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtEvalDesc.Text.Trim, Me.txtFrom.Text.Trim,
                                       Me.txtTo.Text.Trim, Me.txtEvalBy.Text.Trim, txtAttachFiles.Text, fileData, pkEvaluation)
            Call frmMember_Master.GetmemberPerfEvaluation(frmMember_Master.txtEmployeeNo.Text)
            Me.Close()
        End If
    End Sub

    Private Sub txtFrom_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFrom.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                MessageBox.Show("Invalid Input! This field Allow 0-9 Only.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtTo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTo.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                MessageBox.Show("Invalid Input! This field Allow 0-9 Only.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub ClearText()
        txtAttachFiles.Text = ""
        txtEvalBy.Text = ""
        txtEvalDesc.Text = ""
        txtFrom.Text = ""
        txtTo.Text = ""
    End Sub
End Class