﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMember_Skills

    Public pkSkills As String

#Region "Property"
    Public Property getpkSkills() As String
        Get
            Return pkSkills
        End Get
        Set(ByVal value As String)
            pkSkills = value
        End Set
    End Property
#End Region

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub AddEditSkills(ByVal EmpNo As String, ByVal license As String, ByVal Wheels As String, ByVal Automatic As String, ByVal Computer As String, _
                              ByVal WpM As String, ByVal Technical As String, ByVal TechSkills As String, ByVal pk_Skills As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Skills_AddEdit", _
                                     New SqlParameter("@employeeNo", EmpNo), _
                                     New SqlParameter("@License", license), _
                                     New SqlParameter("@Wheels", Wheels), _
                                     New SqlParameter("@Transmission", Automatic), _
                                     New SqlParameter("@Computer", Computer), _
                                     New SqlParameter("@WpM", WpM), _
                                     New SqlParameter("@Technical", Technical), _
                                     New SqlParameter("@OtherTechSkills", TechSkills), _
                                     New SqlParameter("@pk_Skills", pk_Skills))
            trans.Commit()
            MessageBox.Show("Record has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Save Skills")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'If txtSpecify.Text = "" Or txtLicense.Text = "" Then
        '    MessageBox.Show("Complete Infomation is Needed", "Skills Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        'Else
        Call AddEditSkills(frmMember_Master.txtEmployeeNo.Text, txtLicense.Text, cboWheels.Text, cboTransmission.Text, txtSpecify.Text, txtWpM.Text, cboTech.Text, txtSpecifyTech.Text, "")
        Call frmMember_Master.GetmemberSkills(frmMember_Master.txtEmployeeNo.Text)
        Call ClearText()
        'Me.Close()
        'End If
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Call AddEditSkills(frmMember_Master.txtEmployeeNo.Text, txtLicense.Text, cboWheels.Text, cboTransmission.Text, txtSpecify.Text, txtWpM.Text, cboTech.Text, txtSpecifyTech.Text, pkSkills)
        Call frmMember_Master.GetmemberSkills(frmMember_Master.txtEmployeeNo.Text)
        Me.Close()
    End Sub

    Private Sub ClearText()
        txtLicense.Text = ""
        txtSpecify.Text = ""
        txtSpecifyTech.Text = ""
        txtWpM.Text = ""
        cboTransmission.Text = ""
        cboTech.Text = ""
        cboWheels.Text = ""
    End Sub

    Private Sub cboTech_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTech.SelectedIndexChanged

    End Sub

    Private Sub cboTech_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTech.SelectedValueChanged
        If Me.cboTech.Text = "Other" Then
            txtSpecifyTech.Enabled = True
        Else
            txtSpecifyTech.Enabled = False
        End If
    End Sub

    Private Sub frmMember_Skills_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtSpecifyTech.Enabled = False
    End Sub
End Class