Imports System.Data.SqlClient.SqlConnection
Imports System.Data.SqlClient
Imports System.Drawing
Imports Microsoft.VisualBasic.FileSystem
Imports System.Data
Imports System.IO
Imports Microsoft.VisualBasic
Imports System.Security
Imports System.Security.Principal.WindowsIdentity
Imports Microsoft.ApplicationBlocks.Data
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Web.Security
Imports System.Configuration

Public Class frmMember_Master
    '=====================================================================
    ' Project:          ClickSoftware Personnel System
    ' Client:           Mindshare,Thunderbird
    ' Module:           Form6(Employee Masterfile)
    ' Author:           Jolan P.Mahinay
    ' Description:      Employee 201 file
    ' Date:             February 28,2006
    ' Date Revised :    May 30,2014

    ' Updated By :      Vincent Nacar
    ' Last Update :     June 9,2014
    '   June 14,2014
    '=====================================================================
#Region "Variables"
    Dim mycon As New Clsappconfiguration
    Public orgid As String
    Public xorgid As String
    Public position As CurrencyManager
    Dim mydataset As DataSet
    Dim mycommand As New SqlCommand
    Dim myadapter As New SqlDataAdapter
    Dim chrmodeTemp As String
    Private y As Integer
    Public filename As String = ""

    Public sfirstname(15) As String
    Public slastname(15) As String
    Public smiddlename(15) As String
    Public semployeetype(15) As String
    Public dhired(15) As String
    Public snumber(15) As String
    Public sGender(15) As String
    Public sdepartment(15) As String
    Public smobile_phone(15) As String
    Public saddress_1(15) As String
    Public m_sOldIDNumber As String
    Public oldid(100) As String
    Public resigned As String
    Private getevalid As String
    Public keyemployee As String
    Public employeeID As String
    Public emergencykey As String
    'Public admin As String
    Public keyresigned As Integer = 0
    Public suspendesc As String
    Public bereavement As Integer
    Public fbemployed As Integer
    Public fbMembership As Integer
    Private isFirstLoad As Boolean = False

    Private formMode As String = "Edit"

    Private gCon As New Clsappconfiguration()

    'photo and signature
    Dim img As Image
    Dim fpath As String
    Dim filenym As String
    Dim Picdata As Byte()
    Dim SigData As Byte()
    Dim PicData2 As Byte()
    Dim SigData2 As Byte()
    Public dummydata As Byte() 'empty 
    Public entengfxkey As String

    Dim PresentStay As String
    Dim PreviousStay As String
    Dim PermanentStay As String
    Dim HomeOwned As String
    Dim Motor As Boolean
    Dim Car As Boolean
    Dim ContractEnd As String

    Private m_CurrentIndex As Integer
    Dim pkAttach As String
    Dim exempt As Boolean
    Dim MemberMngr As String
    Dim CurrentRate As String
    Dim pos As String
    Dim Salary As String
    Dim Ecola As String
#End Region

#Region "Get Property"
    Public Property oldidnumber() As String
        Get
            Return m_sOldIDNumber
        End Get
        Set(ByVal value As String)
            m_sOldIDNumber = value
        End Set
    End Property
    Public Property resigned_you() As String
        Get
            Return resigned
        End Get
        Set(ByVal value As String)
            resigned = value
        End Set
    End Property

    Public Property _getevalid() As String
        Get
            Return getevalid
        End Get
        Set(ByVal value As String)
            getevalid = value
        End Set
    End Property
    Public Property getfxkeyemployee() As String
        Get
            Return keyemployee
        End Get
        Set(ByVal value As String)
            keyemployee = value
        End Set
    End Property
    Public Property getemployeeID() As String
        Get
            Return employeeID
        End Get
        Set(ByVal value As String)
            employeeID = value
        End Set
    End Property
    Public Property getEmergencykey() As String
        Get
            Return emergencykey
        End Get
        Set(ByVal value As String)
            emergencykey = value
        End Set
    End Property
    Public Property getkeyresigned() As Integer
        Get
            Return keyresigned
        End Get
        Set(ByVal value As Integer)
            keyresigned = value
        End Set
    End Property
    Public Property getxorgid() As String
        Get
            Return xorgid
        End Get
        Set(ByVal value As String)
            xorgid = value
        End Set
    End Property
    Public Property getsupendesc() As String
        Get
            Return suspendesc
        End Get
        Set(ByVal value As String)
            suspendesc = value
        End Set
    End Property
    'Public Property getbereavement() As Integer
    '    Get
    '        Return bereavement
    '    End Get
    '    Set(ByVal value As Integer)
    '        bereavement = value
    '    End Set
    'End Property
    Public Property getbereavement() As Boolean
        Get
            Return bereavement
        End Get
        Set(ByVal value As Boolean)
            bereavement = value
        End Set
    End Property
    Public Property getfbemployed() As Boolean
        Get
            Return fbemployed
        End Get
        Set(ByVal value As Boolean)
            fbemployed = value
        End Set
    End Property
    Public Property getfbMembership() As Integer
        Get
            Return fbMembership
        End Get
        Set(ByVal value As Integer)
            fbMembership = value
        End Set
    End Property

    Private empKeyID As String
    Public Property GetEmployeeKeyID() As String
        Get
            Return empKeyID
        End Get
        Set(ByVal value As String)
            empKeyID = value
        End Set
    End Property

    Public Property getpkEAttach() As String
        Get
            Return pkAttach
        End Get
        Set(ByVal value As String)
            pkAttach = value
        End Set
    End Property
#End Region

#Region "User rights"
    Public Sub LoaduserRights()
        If frmLogin.admin = "Admin" Then

        ElseIf frmLogin.admin = "User" Then
            'Me.btnresigned.Enabled = False
            'Me.btnrestricted.Enabled = False

        End If
    End Sub
#End Region

#Region "Load Civil Status"
    Private Sub LoadCivilStatus()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_Civil_Status", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                Me.cboemp_civil.Items.Add(myreader.Item(0))
            End While

        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region

#Region "Validate Email add"

    Public Sub validateEmail(ByVal email As String)
        'Dim pattern As String = "^[-a-zA-Z0-9][-.a-zA-Z0-9]*@[-.a-zA-Z0-9]+(\.[-.a-zA-Z0-9]+)*\." & _
        '       "(com|edu|info|gov|int|mil|net|org|biz|name|museum|coop|aero|pro|tv|[a-zA-Z]{2})$"
        'Dim check As New System.Text.RegularExpressions.Regex(pattern, RegexOptions.IgnorePatternWhitespace)
        Dim Expression As New System.Text.RegularExpressions.Regex("\S+@\S+\.\S+")
        'make sure an email address was provided
        If Expression.IsMatch(email) Then

        Else
            MessageBox.Show("The email address is NOT valid.")
        End If
    End Sub
#End Region

    Public Sub Form6_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        isFirstLoad = True
        'hide tab
        tabMember.TabPages.Remove(others)
        'tabMember.TabPages.Remove(TabPage15)
        'tabMember.TabPages.Remove(TabPage16)

        Call LoadGroup()
        Call LoaduserRights()
        Call GetAge(EMP_DATEbirth.Value)
        Me.txtfullname.Text = Me.temp_lname.Text + "," + Me.temp_fname.Text + " " + Me.temp_midname.Text

        txtdateregular.Text = ""
        txtremarks.Visible = False

        Call load_configurationfile()
        Call load_salarygrade_1()
        Call Employee_Type()
        Call EmployeeStatus()
        Call Load_RecordId_Masterfile()
        Call LoadCivilStatus()
        'Call GetFirstRecord()
        Call PositionTitle()
        Call LoadPayrollCode()
        Call LoadSalaryRate()
        Call PayrollDescription()
        Call LoadTaxCode()
        Call LoadRank()
        Call LoanCompanyChart()
        'Call LoadLeaveTypes()
        'Call LoadEloadingRegistrationDetails(txtEmployeeNo.Text)

        Call LoadGroup()
        'Call LoadCategory()
        'Call LoadType()
        '======================================
        Dateresigned.Value = Microsoft.VisualBasic.FormatDateTime(Now, DateFormat.ShortDate)
        'emp_pic.ImageLocation = Application.StartupPath & "\SplashScreen v3.jpg"
        'emp_pic.Load()
        '======================================
        btnemp_next.Visible = True
        btnemp_previous.Visible = True
        btnemp_next.Visible = True
        btnemp_previous.Visible = True
        BTNSEARCH.Enabled = True
        '======================================
        Call Me.DisableControls()
        '======================================
        picEmpPhoto.Image = My.Resources.photo
        picEmpSignature.Image = My.Resources.signature
        '-------------------------------------
        Call PerEmployeeStatus()
        'Call employeeType_convert
        'Call LoadEmloymentHistory(Me.txtEmployeeNo.Text, Me.cboEmpHistory.Text)

        Call LoadClient()
        Call LoadOutlet()
        Call LoadContractEnd()
        Call DisableHRISButton()
        m_CurrentIndex = 0
        Me.txtContractPrice.Text = ""
        Me.txtContractrate.Text = ""
    End Sub

    Private Sub LoanCompanyChart() 'Added by Vincent
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gCon.cnstring, "_Select_CooperativeName")
        While rd.Read
            txtCompanyChart.Text = rd("fcCoopName")
        End While
    End Sub

    Private Sub LoadRank()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gCon.cnstring, "_Select_Rank_Master")
        While rd.Read
            With cboRank
                .Items.Add(rd("Description"))
            End With
        End While
    End Sub

    'load group ,category, type
    Private Sub LoadGroup()
        Dim rd As SqlDataReader
        cbofGroup.Items.Clear()
        rd = SqlHelper.ExecuteReader(mycon.cnstring, "CIMS_Masterfile_SelectAll")
        While rd.Read
            cbofGroup.Items.Add(rd("Fc_GroupDesc"))
        End While
    End Sub

    Private Sub LoadSubgroup()
        cboSubgroup.Items.Clear()
        cboSubgroup.Text = ""
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.cnstring, "_Select_Subgroup_Desc",
                                      New SqlParameter("@group", cbofGroup.Text))
        While rd.Read
            cboSubgroup.Items.Add(rd(0).ToString)
        End While
    End Sub

    'Private Sub LoadCategory()
    '    Dim rd As SqlDataReader
    '    rd = SqlHelper.ExecuteReader(mycon.cnstring, "CIMS_Masterfile_Category_Select_ByGroup",
    '                                 New SqlParameter("@Fc_Group", cbofGroup.Text))
    '    While rd.Read
    '        cbofCategory.Items.Add(rd("Fc_Category"))
    '    End While
    'End Sub

    'Private Sub LoadType()
    '    Dim rd As SqlDataReader
    '    rd = SqlHelper.ExecuteReader(mycon.cnstring, "CIMS_Masterfile_Type_Select_ByCategory",
    '                                 New SqlParameter("@Fc_Category", cbofCategory.Text))
    '    While rd.Read
    '        cbofType.Items.Add(rd("Fc_Type"))
    '    End While
    'End Sub

#Region "Position Title"
    Private Sub PositionTitle()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_m_Employment_Positions")
        Try
            While rd.Read
                Me.cbotitledesignation.Items.Add(rd.Item(0))
            End While
            rd.Close()
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Tax Code"
    Private Sub LoadTaxCode()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_Member_LoadTaxCode")
        Try
            While rd.Read
                Me.cbotaxcode.Items.Add(rd.Item(0))
            End While
            rd.Close()
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Payroll Description"
    Private Sub PayrollDescription()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_Member_LoadPayrollDesc")
        Try
            While rd.Read
                Me.cbopayroll.Items.Add(rd.Item(1))
            End While
            rd.Close()
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Salary Rate"
    Private Sub LoadSalaryRate()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_Member_LoadSalaryRate")
        Try
            While rd.Read
                Me.cborate.Items.Add(rd.Item(1))
            End While
            rd.Close()
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Load Payroll Code"
    Private Sub LoadPayrollCode()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_Member_LoadPayCode")
        Try
            While rd.Read
                Me.cboPaycode.Items.Add(rd.Item(1))
            End While
            rd.Close()
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Check Employee IDnumber if Duplicate"
    Private Sub CheckDuplicateIDnumber()

        Dim oStream As New MemoryStream
        Dim bmp As New Bitmap(picEmpPhoto.Image)
        bmp.Save(oStream, Imaging.ImageFormat.Jpeg)

        Dim oStream1 As New MemoryStream
        Dim bmp1 As New Bitmap(picEmpSignature.Image)
        bmp1.Save(oStream1, Imaging.ImageFormat.Jpeg)

        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("CIMS_m_Member_checkduplicateID", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@fcEmployeeNo", SqlDbType.VarChar, 50).Value = Me.txtEmployeeNo.Text
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.HasRows Then
                MessageBox.Show("IDnumber already exist please try another one", "IDnumber Exists..", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                If Me.PicData2 Is Nothing And Me.SigData2 Is Nothing Then
                    Me.PicData2 = oStream.GetBuffer
                    Me.SigData2 = oStream1.GetBuffer
                End If

                '#######################################################
                If cbotaxcode.Text = "" Then
                    cbotaxcode.Text = "ME"
                End If
                '#######################################################
                If rdoExempt.Checked Then
                    exempt = True
                Else
                    exempt = False
                End If
                '=======================================================
                If rbOwned.Checked = True Then
                    HomeOwned = "Owned"
                ElseIf rbRented.Checked = True Then
                    HomeOwned = "Rented"
                ElseIf rbParents.Checked = True Then
                    HomeOwned = "Living with Parents"
                End If

                If rbMYes.Checked = True Then
                    Motor = True
                Else
                    Motor = False
                End If

                If rbCYes.Checked = True Then
                    Car = True
                Else
                    Car = False
                End If
                '=======================================================

                'If txtCompanyName.Text <> "" And temp_lname.Text = "" And temp_fname.Text = "" And temp_midname.Text = "" Then
                '    'Call AddCompanyName()
                '    Call AddEditnewExistingmember(fbemployed, Me.txtEmployeeNo.Text.Trim, "", "", "", Me.txtCompanyName.Text, _
                '              Me.cboemp_gender.Text, Me.cboemp_civil.Text, Me.EMP_DATEbirth.Text, Me.temp_placebirth.Text, Me.txtLandline.Text, Me.txtresidencephone.Text, Me.emp_company.Text, Me.txtofficeadd.Text, _
                '              Me.emp_Datehired.Value, Me.txtlocalofficenumber.Text, Me.txtofficenumber.Text, Me.txtbasicpay.Text, Me.mem_MemberDate.Value, _
                '              bereavement, Me.txtwithdrawal.Text, Me.txtEmailAddress.Text, Me.txtpayrollcontriamount.Text, Me.mem_PaycontDate.Value, Me.txtorgchart.Tag, _
                '              Me.cbotaxcode.Text, "", Me.Cbomem_Status.Text, Me.cbotitledesignation.Text, Me.cborate.Text, Me.cboPaycode.Text, Me.cbopayroll.Text,
                '              Me.PicData2, Me.SigData2, Me.cbofGroup.Text, Me.cbofCategory.Text, Me.cbofType.Text, Me.cboemp_status.Text, Me.cboEmp_type.Text, Me.cboStay2.Text, Me.cboStay3.Text, _
                '              Me.cboStay1.Text)
                'Else
                Call AddEditnewExistingmember(fbemployed, Me.txtEmployeeNo.Text.Trim, "", Me.temp_fname.Text, Me.temp_midname.Text, Me.temp_lname.Text, _
                                              Me.cboemp_gender.Text, Me.cboemp_civil.Text, Me.EMP_DATEbirth.Text, Me.temp_placebirth.Text, Me.txtLandline.Text, Me.txtresidencephone.Text, Me.emp_company.Text, Me.txtofficeadd.Text, _
                                              Me.emp_Datehired.Value, Me.txtlocalofficenumber.Text, Me.txtofficenumber.Text, Me.txtbasicpay.Text, Me.mem_MemberDate.Value, _
                                              bereavement, Me.txtwithdrawal.Text, Me.txtEmailAddress.Text, Me.txtpayrollcontriamount.Text, Me.mem_PaycontDate.Value, Me.txtorgchart.Tag, _
                                              Me.cbotaxcode.Text, "", Me.Cbomem_Status.Text, Me.cbotitledesignation.Text, Me.cborate.Text, Me.cboPaycode.Text, Me.cbopayroll.Text,
                                              Me.PicData2, Me.SigData2, Me.cbofGroup.Text, Me.cbofCategory.Text, Me.cbofType.Text, Me.cboemp_status.Text, Me.cboEmp_type.Text, Me.cboStay2.Text, Me.cboStay3.Text, _
                                              Me.cboStay1.Text)
                'End If
            End If
            myreader.Close()
        Finally
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region

#Region "Add new existing member"
    Private Sub AddEditnewExistingmember(ByVal fbemployed As Boolean, ByVal EmpID As String, ByVal MembershipID As String, _
                                     ByVal firstname As String, ByVal middlename As String, ByVal lastname As String, ByVal gender As String, ByVal civilstatus As String, _
                                     ByVal birthday As Date, ByVal birthplace As String, ByVal residencetelno As String, ByVal mobileno As String, ByVal company As String, ByVal companyaddress As String, ByVal datehired As Date, _
                                     ByVal localno As String, ByVal officeno As String, ByVal salary As Decimal, ByVal membershipdate As Date, ByVal bereavement As Boolean, _
                                     ByVal withdrawdate As String, ByVal emailaddress As String, ByVal contribution As Decimal, ByVal contributiondate As Date, ByVal orgid As String, _
                                     ByVal taxcode As String, ByVal coopname As String, ByVal membershipstatus As String, ByVal position As String, ByVal salarycategory As String, _
                                     ByVal paycodedesc As String, ByVal prolldesc As String, ByVal pic As Byte(), ByVal sig As Byte(), ByVal fgroup As String, ByVal fcategory As String, _
                                     ByVal ftype As String, ByVal EmpStatus As String, ByVal PosLevel As String, ByVal PrevStay As String, ByVal PerStay As String, _
                                     ByVal PresStay As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Master_AddEdit", _
                                      New SqlParameter("@fbEmployed", fbemployed), _
                                      New SqlParameter("@fcEmployeeNo", EmpID), _
                                      New SqlParameter("@fcMembershipNo", MembershipID), _
                                      New SqlParameter("@fcFirstName", firstname), _
                                      New SqlParameter("@fcMiddleName", middlename), _
                                      New SqlParameter("@fcLastName", lastname), _
                                      New SqlParameter("@fcGender", gender), _
                                      New SqlParameter("@fcStatus", civilstatus), _
                                      New SqlParameter("@fcBirthday", birthday), _
                                      New SqlParameter("@fcBirthplace", birthplace), _
                                      New SqlParameter("@fcResidenceTelNo", residencetelno), _
                                      New SqlParameter("@fcMobileNo", mobileno), _
                                      New SqlParameter("@fcCompany", company), _
                                      New SqlParameter("@fcCompanyAddress", companyaddress), _
                                      New SqlParameter("@dtDateHired", datehired), _
                                      New SqlParameter("@fcLocalNo", localno), _
                                      New SqlParameter("@fcOfficeNo", officeno), _
                                      New SqlParameter("@fdSalary", salary), _
                                      New SqlParameter("@dtMembershipDate", membershipdate), _
                                      New SqlParameter("@fbBereavementMemberStatus", bereavement), _
                                      New SqlParameter("@fbWithrawDate", withdrawdate), _
                                      New SqlParameter("@fcEmailAddress", emailaddress), _
                                      New SqlParameter("@fdContribution", contribution), _
                                      New SqlParameter("@fdContributionDate", contributiondate), _
                                      New SqlParameter("@orgID", orgid), _
                                      New SqlParameter("@fk_TaxCode", taxcode), _
                                      New SqlParameter("@coopName", coopname), _
                                      New SqlParameter("@membershipStatus_Desc", membershipstatus), _
                                      New SqlParameter("@position", position), _
                                      New SqlParameter("@SalaryCategoryDesc", salarycategory), _
                                      New SqlParameter("@PayCodeDesc", paycodedesc), _
                                      New SqlParameter("@PRollDesc", prolldesc), _
                                      New SqlParameter("@isExempt", exempt), _
                                      New SqlParameter("@Fb_Picture", pic), _
                                      New SqlParameter("@Fb_Signature", sig), _
                                      New SqlParameter("@fcGroup", fgroup), _
                                      New SqlParameter("@fcCategory", fcategory), _
                                      New SqlParameter("@fcType", ftype), _
                                      New SqlParameter("@fcRank", cboRank.Text), _
                                      New SqlParameter("@fcTin", txtTin.Text), _
                                      New SqlParameter("@fcSSS", txtSSS.Text), _
                                      New SqlParameter("@fcPhilhealth", txtPhilhealth.Text), _
                                      New SqlParameter("@fcHdmf", txtHDMF.Text), _
                                      New SqlParameter("@dtBoardApproval", dtBoardApproval.Value), _
                                      New SqlParameter("@fcSubgroup", cboSubgroup.Text), _
                                      New SqlParameter("@Ecola", txtEcola.Text), _
                                      New SqlParameter("@ContractPrice", txtContractPrice.Text), _
                                      New SqlParameter("@EmpStatus", EmpStatus), _
                                      New SqlParameter("@PositionType", PosLevel), _
                                      New SqlParameter("@fcContractCode", txtContractrate.Text), _
                                      New SqlParameter("@fcSupervisor", cboClient.Text), _
                                      New SqlParameter("@fcOutlet", cboOutlet.Text), _
                                      New SqlParameter("@fcNickname", txtNickname.Text), _
                                      New SqlParameter("@fcHeightFt", txtHeightFt.Text), _
                                      New SqlParameter("@fcHeightInch", txtHeightInch.Text), _
                                      New SqlParameter("@fcWeight", txtW.Text), _
                                      New SqlParameter("@fcTelNo", txtLandline2.Text), _
                                      New SqlParameter("@fcMobNo", txtresidencephone2.Text), _
                                      New SqlParameter("@fcAlternateEmail", txtemailaddress2.Text), _
                                      New SqlParameter("@fcFacebook", txtFacebook1.Text), _
                                      New SqlParameter("@fcFacebook1", txtFacebook2.Text), _
                                      New SqlParameter("@fcCRN", txtCommonrefNo.Text), _
                                      New SqlParameter("@fcPrevStay", PrevStay), _
                                      New SqlParameter("@fcPerStay", PerStay), _
                                      New SqlParameter("@fcPresStay", PresStay), _
                                      New SqlParameter("@fcPrevLength", txtStay2.Text), _
                                      New SqlParameter("@fcPerLength", txtStay3.Text), _
                                      New SqlParameter("@fcPresLength", txtStay1.Text), _
                                      New SqlParameter("@fbHomeOwned", HomeOwned), _
                                      New SqlParameter("@fbMotor", Motor), _
                                      New SqlParameter("@fbCar", Car), _
                                      New SqlParameter("@fcMotorCount", txtMotor.Text), _
                                      New SqlParameter("@fcCarCount", txtCar.Text), _
                                      New SqlParameter("@fcSuffix", txtSuffix.Text), _
                                      New SqlParameter("@fcProject", txtProject.Text))

            trans.Commit()

            Call AddEditFamiy()
            Call AddEdit_Address()
            'Call Integrate_CIMSToAccounting()

            Dim x As New DialogResult
            x = MessageBox.Show("Records successfully saved!", "Saving Record...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtEmployeeNo.BackColor = Color.White
            temp_lname.BackColor = Color.White
            temp_fname.BackColor = Color.White
            temp_midname.BackColor = Color.White
            cbotaxcode.BackColor = Color.White
            txtorgchart.BackColor = Color.White
            Cbomem_Status.BackColor = Color.White
            txtCompanyName.BackColor = Color.White

            Me.txtEmployeeNo.Enabled = False
            Me.temp_lname.Enabled = False
            Me.temp_fname.Enabled = False
            Me.temp_midname.Enabled = False
            Me.cbotaxcode.Enabled = False

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "CIMS_Member_Master_AddEdit", MessageBoxButtons.OK)
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region

#Region "Edit new existing member"
    Private Sub EditnewExistingmember(ByVal fbemployed As Boolean, ByVal EmpID As String, ByVal MembershipID As String, _
                                         ByVal firstname As String, ByVal middlename As String, ByVal lastname As String, ByVal gender As String, ByVal civilstatus As String, _
                                         ByVal birthday As Date, ByVal birthplace As String, ByVal residencetelno As String, ByVal mobileno As String, ByVal company As String, ByVal companyaddress As String, ByVal datehired As Date, _
                                         ByVal localno As String, ByVal officeno As String, ByVal salary As Decimal, ByVal membershipdate As Date, ByVal bereavement As Boolean, _
                                         ByVal withdrawdate As String, ByVal emailaddress As String, ByVal contribution As Decimal, ByVal contributiondate As Date, ByVal orgid As String, _
                                         ByVal taxcode As String, ByVal coopname As String, ByVal membershipstatus As String, ByVal position As String, ByVal salarycategory As String, _
                                         ByVal paycodedesc As String, ByVal prolldesc As String, ByVal pic As Byte(), ByVal sig As Byte(), ByVal fgroup As String, ByVal fCategory As String, _
                                         ByVal ftype As String, ByVal vinfxkey As String, ByVal EmpStaus As String, ByVal PosLevel As String, ByVal PrevStay As String, ByVal PerStay As String, _
                                         ByVal PresStay As String)


        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()

        Try
            '#######################################
            'Added by Vincent Nacar
            Dim MyGuid As Guid = New Guid(vinfxkey)
            '#######################################
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Master_Edit", _
                                      New SqlParameter("@fbEmployed", fbemployed), _
                                      New SqlParameter("@fcEmployeeNo", EmpID), _
                                      New SqlParameter("@fcMembershipNo", MembershipID), _
                                      New SqlParameter("@fcFirstName", firstname), _
                                      New SqlParameter("@fcMiddleName", middlename), _
                                      New SqlParameter("@fcLastName", lastname), _
                                      New SqlParameter("@fcGender", gender), _
                                      New SqlParameter("@fcStatus", civilstatus), _
                                      New SqlParameter("@fcBirthday", birthday), _
                                      New SqlParameter("@fcBirthplace", birthplace), _
                                      New SqlParameter("@fcResidenceTelNo", residencetelno), _
                                      New SqlParameter("@fcMobileNo", mobileno), _
                                      New SqlParameter("@fcCompany", company), _
                                      New SqlParameter("@fcCompanyAddress", companyaddress), _
                                      New SqlParameter("@dtDateHired", datehired), _
                                      New SqlParameter("@fcLocalNo", localno), _
                                      New SqlParameter("@fcOfficeNo", officeno), _
                                      New SqlParameter("@fdSalary", salary), _
                                      New SqlParameter("@dtMembershipDate", membershipdate), _
                                      New SqlParameter("@fbBereavementMemberStatus", bereavement), _
                                      New SqlParameter("@fbWithrawDate", withdrawdate), _
                                      New SqlParameter("@fcEmailAddress", emailaddress), _
                                      New SqlParameter("@fdContribution", contribution), _
                                      New SqlParameter("@fdContributionDate", contributiondate), _
                                      New SqlParameter("@orgID", orgid), _
                                      New SqlParameter("@fk_TaxCode", taxcode), _
                                      New SqlParameter("@coopName", coopname), _
                                      New SqlParameter("@membershipStatus_Desc", membershipstatus), _
                                      New SqlParameter("@position", position), _
                                      New SqlParameter("@SalaryCategoryDesc", salarycategory), _
                                      New SqlParameter("@PayCodeDesc", paycodedesc), _
                                      New SqlParameter("@PRollDesc", prolldesc), _
                                      New SqlParameter("@isExempt", exempt), _
                                      New SqlParameter("@Fb_Picture", pic), _
                                      New SqlParameter("@Fb_Signature", sig), _
                                      New SqlParameter("@fcGroup", fgroup), _
                                      New SqlParameter("@fcCategory", fCategory), _
                                      New SqlParameter("@fcType", ftype), _
                                      New SqlParameter("@entengfxkey", MyGuid), _
                                      New SqlParameter("@fcRank", cboRank.Text), _
                                      New SqlParameter("@fcTin", txtTin.Text), _
                                      New SqlParameter("@fcSSS", txtSSS.Text), _
                                      New SqlParameter("@fcPhilhealth", txtPhilhealth.Text), _
                                      New SqlParameter("@fcHdmf", txtHDMF.Text), _
                                      New SqlParameter("@dtBoardApproval", dtBoardApproval.Value), _
                                      New SqlParameter("@fcSubgroup", cboSubgroup.Text), _
                                      New SqlParameter("@Ecola", txtEcola.Text), _
                                      New SqlParameter("@ContractPrice", txtContractPrice.Text), _
                                      New SqlParameter("@EmpStatus", EmpStaus), _
                                      New SqlParameter("@PositionType", PosLevel), _
                                      New SqlParameter("@DateUpdated", Date.Now),
                                      New SqlParameter("@fcContractCode", txtContractrate.Text),
                                      New SqlParameter("@fcSupervisor", cboClient.Text), _
                                      New SqlParameter("@fcOutlet", cboOutlet.Text), _
                                      New SqlParameter("@fcNickname", txtNickname.Text), _
                                      New SqlParameter("@fcHeightFt", txtHeightFt.Text), _
                                      New SqlParameter("@fcHeightInch", txtHeightInch.Text), _
                                      New SqlParameter("@fcWeight", txtW.Text), _
                                      New SqlParameter("@fcTelNo", txtLandline2.Text), _
                                      New SqlParameter("@fcMobNo", txtresidencephone2.Text), _
                                      New SqlParameter("@fcAlternateEmail", txtemailaddress2.Text), _
                                      New SqlParameter("@fcFacebook", txtFacebook1.Text), _
                                      New SqlParameter("@fcFacebook1", txtFacebook2.Text), _
                                      New SqlParameter("@fcCRN", txtCommonrefNo.Text), _
                                      New SqlParameter("@fcPrevStay", PrevStay), _
                                      New SqlParameter("@fcPerStay", PerStay), _
                                      New SqlParameter("@fcPresStay", PresStay), _
                                      New SqlParameter("@fcPrevLength", txtStay2.Text), _
                                      New SqlParameter("@fcPerLength", txtStay3.Text), _
                                      New SqlParameter("@fcPresLength", txtStay1.Text), _
                                      New SqlParameter("@fbHomeOwned", HomeOwned), _
                                      New SqlParameter("@fbMotor", Motor), _
                                      New SqlParameter("@fbCar", Car), _
                                      New SqlParameter("@fcMotorCount", txtMotor.Text), _
                                      New SqlParameter("@fcCarCount", txtCar.Text), _
                                      New SqlParameter("@fcSuffix", txtSuffix.Text), _
                                      New SqlParameter("@fcProject", txtProject.Text))

            Call AddEditFamiy()
            Call AddEdit_Address()
            'Call Integrate_CIMSToAccounting()

            Dim x As New DialogResult
            x = MessageBox.Show("Records successfully updated!", "Updating Record...", MessageBoxButtons.OK, MessageBoxIcon.Information)

            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            'Throw ex
            MessageBox.Show(ex.Message, "CIMS_Member_Master_AddEdit", MessageBoxButtons.OK)
        Finally
            gcon.sqlconn.Close()
        End Try

        'load history info / refresh
        Call GetLastnameHistory(Me.txtEmployeeNo.Text)
        Call GetAddressHistory(Me.txtEmployeeNo.Text)
        Call GetCivilHistory(Me.txtEmployeeNo.Text)
        Call GEtPhone(Me.txtEmployeeNo.Text)
        Call GetEmail(Me.txtEmployeeNo.Text)
    End Sub
#End Region

#Region "Generate Membership ID"
    Private Sub GenerateMembershipID()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_GENERATE_MEMBERSHIPID")
        Try
            While rd.Read
                Me.txtMemberID.Text = rd.Item(0)
            End While
            rd.Close()
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Get first Member Record ascending Order"
    Private Sub GetFirstRecord()
        Dim gcon As New Clsappconfiguration
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Member_GetfirstRecord")
                While rd.Read
                    Me.txtpk_Employee.Text = rd.Item(0).ToString.Trim
                End While
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message, "GetFirstRecord")
        End Try
    End Sub
#End Region

    '''''''''''''''''''''''''''''''
    ''clear txt for emp-masterfile'
    '''''''''''''''''''''''''''''''

    Public Sub cleartxt()
        txtTin.Text = ""
        txtSSS.Text = ""
        txtPhilhealth.Text = ""
        txtHDMF.Text = ""
        txtEcola.Text = "0.00"
        txtContractPrice.Text = "0.00"
        txtbasicpay.Text = "0.00"
        txtContractrate.Text = ""

        Me.txtMemberID.Text = ""
        Me.txtkeycompany.Text = ""
        Me.txtdescdivision.Text = ""
        Me.txtdescdepartment.Text = ""
        Me.txtdescsection.Text = ""
        Me.txtpk_Employee.Text = ""
        Me.txtEmployeeNo.Text = ""

        Me.lblemployee_name.Text = ""


        emp_Datehired.Text = ""
        EMP_DATEbirth.Text = ""

        Me.txtType_employee.Text = ""
        Me.txtfxkeypositionlevel.Text = ""
        Me.TXTKEYEMPLOYEEID.Text = ""
        Me.txtDepartment2.Text = ""
        Me.txtPresBldg.Text = ""

        Me.Text = "" 'for fullname

        Me.txtfullname.Text = ""
        Me.txtitemno.Text = ""
        Me.emp_pic.ImageLocation = ""

        Me.txtweight.Text = ""
        Me.temp_fname.Text = ""
        Me.temp_lname.Text = ""
        Me.temp_midname.Text = ""
        Me.cboEmp_type.Text = ""
        Me.cboemp_gender.Text = ""
        Me.cboemp_civil.Text = ""
        Me.cboemp_status.Text = ""
        Me.txtorgchart.Text = ""
        Me.txtorgchart.Tag = ""
        Me.txtresidencephone.Text = ""
        Me.emp_extphone.Text = ""
        Me.txtMobileNo.Text = ""
        Me.txtEmailAddress.Text = ""
        Me.temp_designation.Text = ""
        'Me.txtPrevAdd.Text = ""
        Me.txtEmployeeNo.Text = ""
        Me.temp_placebirth.Text = ""
        Me.txtofficeadd.Text = ""
        Me.temp_citizen.Text = ""
        Me.temp_height.Text = ""
        Me.txtofficenumber.Text = ""
        Me.txtlocalofficenumber.Text = ""
        Me.temp_add3.Text = ""
        Me.temp_marks.Text = ""

        Me.cbotitledesignation.Text = ""
        Me.cbosalarygrade.Text = ""
        Me.txtdateregular.Text = ""

        'addtional

        Me.txtpayrollcontriamount.Text = "0.00"
        Me.mem_PaycontDate.Text = ""
        Me.mem_WithdrawDate.Text = ""
        Me.mem_MemberDate.Text = ""
        Me.Cbomem_Status.Text = ""

        Me.cboPaycode.Text = ""
        Me.cbopayroll.Text = ""
        Me.cborate.Text = ""
        Me.cbotaxcode.Text = ""
        Me.emp_Ytenures.Text = ""
        Me.chkyes.Checked = False
        Me.chkNo.Checked = False
        Me.txtCompanyName.Text = ""
        Me.cboClient.Text = ""
        Me.txtNickname.Text = ""
        Me.txtHeightFt.Text = ""
        Me.txtHeightInch.Text = ""
        Me.txtLandline.Text = ""
        Me.txtLandline2.Text = ""
        Me.txtresidencephone2.Text = ""
        Me.txtemailaddress2.Text = ""
        Me.txtFacebook1.Text = ""
        Me.txtFacebook2.Text = ""
        'Me.txtPrevAdd.Text = ""
        Me.txtPresBldg.Text = ""
        Me.txtStay1.Text = ""
        Me.txtStay2.Text = ""
        Me.txtStay3.Text = ""
        Me.cboStay3.Text = ""
        Me.txtStay1.Text = ""
        Me.cboStay2.Text = ""
        Me.txtSpouseName.Text = ""
        Me.txtSpouseAdd.Text = ""
        Me.txtSpouseContact.Text = ""
        Me.txtSpouseStay.Text = ""
        Me.txtOccupation.Text = ""
        Me.txtChildNo.Text = ""
        Me.btnSpouseSearchAdd.Text = ""
        Me.rbSpouseMonth.Checked = False
        Me.rbSpouseYears.Checked = False
        Me.rbOwned.Checked = False
        Me.rbRented.Checked = False
        Me.rbParents.Checked = False
        Me.rbMYes.Checked = False
        Me.rbMNo.Checked = False
        Me.rbCYes.Checked = False
        Me.rbCNo.Checked = False
        Me.txtMotor.Text = ""
        Me.txtCar.Text = ""
        Me.cboOutlet.Text = ""

        With Me
            .txtPresBldg.Text = ""
            .txtPresStreet.Text = ""
            .txtPresSitio.Text = ""
            .txtPresCity.Text = ""
            .txtPresProvince.Text = ""
            .txtPrevBldg.Text = ""
            .txtPrevStreet.Text = ""
            .txtPrevSitio.Text = ""
            .txtPrevCity.Text = ""
            .txtPrevProvince.Text = ""
            .txtPermBldg.Text = ""
            .txtPermStreet.Text = ""
            .txtPermSitio.Text = ""
            .txtPermCity.Text = ""
            .txtPermProvince.Text = ""
        End With
        picEmpPhoto.Image = My.Resources.photo
        picEmpSignature.Image = My.Resources.signature
        txtFatherName.Text = ""
        txtFAdd.Text = ""
        txtFContact.Text = ""
        txtMotherName.Text = ""
        txtMAdd.Text = ""
        txtMContact.Text = ""
        cboContactPerson.Text = "SELECT"
        txtProject.Text = ""
    End Sub

#Region "Clear listview"
    Public Sub clearlistview()
        Me.LvlEmployeeDependent.Clear()
        Me.lvlBankInfo.Clear()
        Me.lvlAwards.Clear()
        Me.lvlDiscipline.Clear()
        Me.lvlEducInfo.Clear()
        Me.lvlJobDesc.Clear()
        Me.lvlLeave.Clear()
        Me.lvlMedical.Clear()
        Me.lvlPerfEval.Clear()
        Me.lvlNearestRelatives.Clear()
        Me.lvlSkills.Clear()
        Me.lvlSourceIncome.Clear()
        Me.lvlTrainings.Clear()
        Me.lvlPayrollHistory.Clear()
        Me.lvlEmploymentHistory.Clear()
        'Me.lvlLoanHistory.Clear()
    End Sub
#End Region

    Private Sub btnemp_add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_add.Click
        Me.txtEmployeeNo.BackColor = Color.LightGreen
        Me.temp_lname.BackColor = Color.LightGreen
        Me.temp_fname.BackColor = Color.LightGreen
        Me.temp_midname.BackColor = Color.LightGreen
        Me.cbotaxcode.BackColor = Color.LightGreen
        Me.txtorgchart.BackColor = Color.LightGreen

        Me.Cbomem_Status.BackColor = Color.LightGreen

        Me.txtEmployeeNo.Enabled = True
        Me.temp_lname.Enabled = True
        Me.temp_fname.Enabled = True
        Me.temp_midname.Enabled = True
        Me.cbotaxcode.Enabled = True
        Me.txtCompanyName.Enabled = True
        'Me.txtContractrate.Enabled = True
        Me.btnContractRate.Enabled = True
        'Me.cboClient.Enabled = True

        formMode = "New"

        If btnemp_add.Text = "New" Then

            'reset photo and signature to default
            picEmpPhoto.Image = My.Resources.photo
            picEmpSignature.Image = My.Resources.signature

            If isEloadingMemberValid() Then
                emp_Datehired.Value = Microsoft.VisualBasic.FormatDateTime(Now, DateFormat.ShortDate)
                EMP_DATEbirth.Value = Microsoft.VisualBasic.FormatDateTime(Now, DateFormat.ShortDate)

                Call clearlistview()
                Call EnableControls()
                Call cleartxt()
                Call GenerateMembershipID()
                Call ShowBrowseBtn()
                Call ClientorEmployee()
                Me.txtEmployeeNo.Focus()
                Me.TXTRECID.Text = ""
                Me.txtage1.Text = "0"
                Me.txtbasicpay.Text = "0.00"
                Me.txtEcola.Text = "0.00"
                txtContractPrice.Text = "0.00"
                Me.btnUpload.Enabled = False
                Me.btnLegalUpload.Enabled = False

                btnemp_delete.Visible = False
                btnemp_edit.Visible = False
                btnemp_close.Text = "Cancel"
                btnemp_add.Text = "Save"
                btnemp_add.Image = My.Resources.save2
                ' btnemp_close.Location = New System.Drawing.Point(838, 3)
            End If

        ElseIf btnemp_add.Text = "Save" Then

            If txtEmployeeNo.Text = "" Then
                MessageBox.Show("Nothing to Save", "Invalid Saving", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Else
                If isEloadingMemberValid() Then
                    'Changes i turned = instead of <> sabi ni sir nono
                    ' If Me.txtorgchart.Text = "" Then
                    Call CheckDuplicateIDnumber()
                    Call SaveContractHistory()
                    tabMember.Enabled = True
                    btnemp_add.Text = "New"
                    btnemp_add.Image = My.Resources.new3
                    btnemp_close.Text = "Close"
                    ' btnemp_close.Location = New System.Drawing.Point(972, 3)
                    btnemp_edit.Visible = True
                    btnemp_delete.Visible = True
                    Me.btnUpload.Enabled = True
                    Me.btnLegalUpload.Enabled = True

                    Call DisableControls()
                    Call HideBrowseBtn()
                    'Call DisableHRISButton()
                    If txtCompanyName.Text <> "" And Me.temp_fname.Text.Trim = "" Or Me.temp_lname.Text.Trim = "" Then
                        Me.lblemployee_name.Text = Me.txtCompanyName.Text
                    Else
                        Me.lblemployee_name.Text = Me.temp_fname.Text + " " + Me.temp_lname.Text + " " + Me.temp_midname.Text
                    End If
                    'Else
                    '    'MessageBox.Show("Supply the required fields", "Cannot Save", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    '    frmMsgBox.txtMessage.Text = "Cannot Save , Supply the required field!"
                    '    frmMsgBox.Show()
                    'End If
                End If
            End If
        End If

    End Sub


    Private Sub btnemp_close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_close.Click
        If btnemp_close.Text = "Cancel" Then

            '************************************
            Me.txtEmployeeNo.Enabled = False
            Me.temp_lname.Enabled = False
            Me.temp_fname.Enabled = False
            Me.temp_midname.Enabled = False
            Me.cbotaxcode.Enabled = False
            '************************************
            'Me.cboClient.Enabled = False
            '************************************

            Call HideBrowseBtn()
            btnemp_add.Text = "New"
            btnemp_add.Image = My.Resources.new3
            Call DisableControls()
            Call DisableHRISButton()

            Me.btnemp_next.Enabled = True
            Me.btnemp_previous.Enabled = True
            Me.BTNSEARCH.Enabled = True
            Me.btnUpload.Enabled = True
            Me.btnLegalUpload.Enabled = True

            'btnrestricted.Visible = True



            ' *******************************************************
            txtEmployeeNo.BackColor = Color.White
            temp_lname.BackColor = Color.White
            temp_fname.BackColor = Color.White
            temp_midname.BackColor = Color.White
            cbotaxcode.BackColor = Color.White
            txtorgchart.BackColor = Color.White
            Cbomem_Status.BackColor = Color.White

            txtEmployeeNo.Enabled = False
            temp_lname.Enabled = False
            temp_fname.Enabled = False
            temp_midname.Enabled = False
            cbotaxcode.Enabled = False
            'cboClient.Enabled = False
            ' *******************************************************


            btnemp_close.Text = "Close"
            ' btnemp_close.Location = New System.Drawing.Point(972, 3)
            btnemp_edit.Visible = True
            btnemp_edit.Text = "Edit"
            btnemp_edit.Image = My.Resources.edit1
            btnemp_delete.Visible = True
            btnemp_add.Visible = True

            'Me.txtContractrate.Enabled = False
            Me.btnContractRate.Enabled = False

        ElseIf btnemp_close.Text = "Close" Then
            Me.Close()
        End If
    End Sub

    Private Sub ShowBrowseBtn()
        btnBrowsePicture.Visible = True
        btnBrowseSignature.Visible = True
    End Sub

    Private Sub HideBrowseBtn()
        btnBrowsePicture.Visible = False
        btnBrowseSignature.Visible = False
    End Sub
#Region "Load Fxkeyemployee in form6"
    Public Sub Loadfxkeyemployee()
        Dim mycon As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_fxkeyemployee_load", mycon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        mycon.sqlconn.Open()
        Dim reader As SqlDataReader = cmd.ExecuteReader
        Try
            While reader.Read
                Me.TXTKEYEMPLOYEEID.Text = reader.Item(0)
            End While
            reader.Close()
            mycon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Load Fxkeyemployee", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region

    Private Sub BTNSEARCH_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTNSEARCH.Click
        picEmpPhoto.Image = My.Resources.photo
        picEmpSignature.Image = My.Resources.signature
        Employee_Search.txtid.Text = ""
        Employee_Search.ShowDialog()
        Call DisableHRISButton()
        btnPEDownload.Enabled = False
        btnDDownload.Enabled = False
        btnTDownload.Enabled = False
        btnMedUpload.Enabled = False
        btnUpload.Enabled = False
        btnLegalUpload.Enabled = False
    End Sub

#Region "Disable text"
    Private Sub DisableControls()
        txtTin.ReadOnly = True
        txtSSS.ReadOnly = True
        txtPhilhealth.ReadOnly = True
        txtHDMF.ReadOnly = True
        txtEcola.ReadOnly = True
        txtContractPrice.ReadOnly = True

        'Me.txtage1.Enabled = False
        Me.grpPayrollStatus.Enabled = False
        Me.chkNo.Enabled = False
        Me.chkyes.Enabled = False
        Me.txtMemberID.ReadOnly = True
        Me.PictureBox2.Enabled = False
        Me.cboRank.Enabled = False
        Me.txtresidencephone.Enabled = False
        Me.txtEmailAddress.Enabled = False

        Me.cbofGroup.Enabled = False
        Me.cbofType.Enabled = False
        Me.cbofCategory.Enabled = False
        Me.cboSubgroup.Enabled = False
        Me.txtPresBldg.ReadOnly = True
        Me.txtDepartment2.ReadOnly = True
        Me.txtorgchart.ReadOnly = True

        Me.txtfullname.Enabled = False
        Me.txtitemno.Enabled = False


        Me.txtweight.ReadOnly = True
        Me.temp_fname.ReadOnly = True
        Me.temp_lname.ReadOnly = True
        Me.temp_midname.ReadOnly = True

        Me.txtorgchart.ReadOnly = True
        Me.txtresidencephone.ReadOnly = True
        Me.emp_extphone.ReadOnly = True
        'Me.txtMobileNo.ReadOnly = True
        Me.txtEmailAddress.ReadOnly = True
        Me.temp_designation.Enabled = False
        'Me.txtPrevAdd.ReadOnly = True
        Me.txtEmployeeNo.ReadOnly = True
        Me.temp_placebirth.ReadOnly = True
        Me.txtofficeadd.ReadOnly = True
        Me.temp_citizen.ReadOnly = True
        Me.temp_height.ReadOnly = True
        Me.txtofficenumber.ReadOnly = True
        Me.txtlocalofficenumber.ReadOnly = True
        Me.temp_add3.ReadOnly = True
        Me.temp_marks.ReadOnly = True

        'ADDITIONAL DISABLED TEXTBOX

        'Me.Cbomem_Bereavement.ReadOnly = True
        'Me.mem_amountpaid.ReadOnly = True
        Me.txtpayrollcontriamount.ReadOnly = True
        Me.emp_Ytenures.ReadOnly = True
        Me.emp_company.ReadOnly = True

        Me.txtdateregular.ReadOnly = True
        Me.txtorgchart.ReadOnly = True
        With Me
            .txtweight.ReadOnly = True
        End With
        'Date and combo
        cboemp_status.Enabled = False
        cboemp_gender.Enabled = False
        cboemp_civil.Enabled = False
        EMP_DATEbirth.Enabled = False
        'Cbomem_Bereavement.Enabled = False
        Cbomem_Status.Enabled = False
        mem_MemberDate.Enabled = False
        mem_WithdrawDate.Enabled = False
        mem_PaycontDate.Enabled = False
        emp_Datehired.Enabled = False

        cboEmp_type.Enabled = False
        cbotitledesignation.Enabled = False
        cbopayroll.Enabled = False
        cborate.Enabled = False
        cbotaxcode.Enabled = False
        cboPositionLevel.Enabled = False
        Me.cboPaycode.Enabled = False
        Me.txtwithdrawal.Enabled = False

        btnAddMobileNo.Enabled = False
        chkIsEloader.Enabled = False
        txtEloaderPIN.Enabled = False

        gridMobileNos.Enabled = False
        btnEditMobile.Enabled = False

        txtCompanyName.Enabled = False
        Me.cboClient.Enabled = False
        Me.txtNickname.Enabled = False
        Me.txtHeightFt.Enabled = False
        Me.txtHeightInch.Enabled = False
        Me.txtW.Enabled = False
        Me.txtLandline.ReadOnly = True
        Me.txtLandline2.Enabled = False
        Me.txtresidencephone2.Enabled = False
        Me.txtemailaddress2.Enabled = False
        Me.txtFacebook1.Enabled = False
        Me.txtFacebook2.Enabled = False
        'Me.txtPrevAdd.ReadOnly = True
        Me.txtPresBldg.ReadOnly = True
        Me.txtStay1.Enabled = False
        Me.txtStay2.Enabled = False
        Me.txtStay3.Enabled = False
        Me.cboStay1.Enabled = False
        Me.cboStay2.Enabled = False
        Me.cboStay3.Enabled = False
        Me.txtCommonrefNo.Enabled = False
        Me.BTNSEARCHHome.Enabled = False
        Me.BTNSEARCHProvince.Enabled = False
        Me.BTNSEARCHPermAddress.Enabled = False
        Me.txtMotherName.Enabled = False
        Me.txtMAdd.Enabled = False
        Me.txtMContact.Enabled = False
        Me.txtFatherName.Enabled = False
        Me.txtFAdd.Enabled = False
        Me.txtFContact.Enabled = False
        Me.txtSpouseName.Enabled = False
        Me.txtSpouseAdd.Enabled = False
        Me.txtSpouseContact.Enabled = False
        Me.txtSpouseStay.Enabled = False
        Me.btnMSearchAdd.Enabled = False
        Me.btnFSearchAdd.Enabled = False
        Me.btnSpouseSearchAdd.Enabled = False
        Me.txtOccupation.Enabled = False
        Me.txtChildNo.Enabled = False
        Me.btnSpouseSearchAdd.Enabled = False
        Me.rbSpouseMonth.Enabled = False
        Me.rbSpouseYears.Enabled = False
        Me.rbOwned.Enabled = False
        Me.rbRented.Enabled = False
        Me.rbParents.Enabled = False
        Me.rbMYes.Enabled = False
        Me.rbMNo.Enabled = False
        Me.rbCYes.Enabled = False
        Me.rbCNo.Enabled = False
        Me.txtCar.Enabled = False
        Me.txtMotor.Enabled = False
        Me.cboOutlet.Enabled = False
        Me.rbClient.Enabled = False
        Me.rbEmployee.Enabled = False
        With Me
            .txtPresBldg.Enabled = False
            .txtPresStreet.Enabled = False
            .txtPresSitio.Enabled = False
            .txtPresCity.Enabled = False
            .txtPresProvince.Enabled = False
            .txtPrevBldg.Enabled = False
            .txtPrevStreet.Enabled = False
            .txtPrevSitio.Enabled = False
            .txtPrevCity.Enabled = False
            .txtPrevProvince.Enabled = False
            .txtPermBldg.Enabled = False
            .txtPermStreet.Enabled = False
            .txtPermSitio.Enabled = False
            .txtPermCity.Enabled = False
            .txtPermProvince.Enabled = False
            .txtSuffix.Enabled = False
            .dtBoardApproval.Enabled = False
            .txtbasicpay.Enabled = False
        End With
        cboContactPerson.Enabled = False
        txtProject.Enabled = False
    End Sub
#End Region

#Region "Enable text"
    Public Sub EnableControls()
        txtTin.ReadOnly = False
        txtSSS.ReadOnly = False
        txtPhilhealth.ReadOnly = False
        txtHDMF.ReadOnly = False
        'txtEcola.ReadOnly = False
        txtContractPrice.ReadOnly = False

        'Me.txtage1.Enabled = True
        Me.grpPayrollStatus.Enabled = True
        Me.chkyes.Enabled = True
        Me.chkNo.Enabled = True
        Me.txtMemberID.ReadOnly = False
        Me.PictureBox2.Enabled = True
        Me.cbofGroup.Enabled = True
        Me.cbofType.Enabled = True
        Me.cbofCategory.Enabled = True
        Me.cboSubgroup.Enabled = True
        Me.emp_Datehired.Enabled = True
        Me.EMP_DATEbirth.Enabled = True
        Me.cboRank.Enabled = True
        Me.txtresidencephone.Enabled = True
        Me.txtEmailAddress.Enabled = True

        Me.txtPresBldg.ReadOnly = False
        Me.txtDepartment2.ReadOnly = False
        Me.txtorgchart.ReadOnly = False
        'Me.Text = "" 'for fullname

        Me.txtfullname.ReadOnly = False
        Me.txtitemno.ReadOnly = False
        'Me.emp_pic.ImageLocation = ""

        Me.txtweight.ReadOnly = False
        Me.temp_fname.ReadOnly = False
        Me.temp_lname.ReadOnly = False
        Me.temp_midname.ReadOnly = False
        Me.cboEmp_type.Enabled = True
        Me.cboemp_gender.Enabled = True
        Me.cboemp_civil.Enabled = True
        Me.cboemp_status.Enabled = True
        Me.txtorgchart.ReadOnly = False
        'Me.txtorgchart.Tag = ""
        Me.txtresidencephone.ReadOnly = False
        Me.emp_extphone.ReadOnly = False
        Me.txtMobileNo.ReadOnly = False
        Me.txtEmailAddress.ReadOnly = False
        Me.temp_designation.ReadOnly = False
        'Me.txtPrevAdd.ReadOnly = False
        Me.txtEmployeeNo.ReadOnly = False
        Me.temp_placebirth.ReadOnly = False
        Me.txtofficeadd.ReadOnly = False
        Me.temp_citizen.ReadOnly = False
        Me.temp_height.ReadOnly = False
        Me.txtofficenumber.ReadOnly = False
        Me.txtlocalofficenumber.ReadOnly = False
        Me.temp_add3.ReadOnly = False
        Me.temp_marks.ReadOnly = False

        'ADDITIONAL ENABLED TEXTBOX    

        'Me.Cbomem_Bereavement.ReadOnly = False
        'Me.mem_amountpaid.ReadOnly = False
        Me.txtpayrollcontriamount.ReadOnly = False
        Me.emp_Ytenures.ReadOnly = False
        Me.emp_company.ReadOnly = False

        'Me.tlocation.ReadOnly = False
        'Me.txtlocation1.ReadOnly = False

        Me.cbotitledesignation.Enabled = True
        Me.cbosalarygrade.Enabled = True
        Me.txtdateregular.ReadOnly = False
        With Me
            .txtweight.ReadOnly = False
            .cborate.Enabled = True
            '.txtbasicpay.ReadOnly = False
        End With
        'Date and Combo
        cboemp_status.Enabled = True
        cboemp_gender.Enabled = True
        cboemp_civil.Enabled = True
        EMP_DATEbirth.Enabled = True
        'Cbomem_Bereavement.Enabled = True
        Cbomem_Status.Enabled = True
        mem_MemberDate.Enabled = True
        mem_WithdrawDate.Enabled = True
        mem_PaycontDate.Enabled = True
        emp_Datehired.Enabled = True

        cboEmp_type.Enabled = True
        cbotitledesignation.Enabled = True
        cbopayroll.Enabled = True
        cborate.Enabled = True
        cbotaxcode.Enabled = True
        cboPositionLevel.Enabled = True
        Me.cboPaycode.Enabled = True

        Me.txtwithdrawal.Enabled = True

        btnAddMobileNo.Enabled = True
        chkIsEloader.Enabled = True
        txtEloaderPIN.Enabled = True

        gridMobileNos.Enabled = True
        btnEditMobile.Enabled = True

        txtCompanyName.Enabled = True
        Me.cboClient.Enabled = True
        Me.txtNickname.Enabled = True
        Me.txtHeightFt.Enabled = True
        Me.txtHeightInch.Enabled = True
        Me.txtW.Enabled = True
        Me.txtLandline.ReadOnly = False
        Me.txtLandline2.Enabled = True
        Me.txtresidencephone2.Enabled = True
        Me.txtemailaddress2.Enabled = True
        Me.txtFacebook1.Enabled = True
        Me.txtFacebook2.Enabled = True
        'Me.txtPrevAdd.ReadOnly = False
        Me.txtPresBldg.ReadOnly = False
        Me.txtStay1.Enabled = True
        Me.txtStay2.Enabled = True
        Me.txtStay3.Enabled = True
        Me.cboStay1.Enabled = True
        Me.cboStay2.Enabled = True
        Me.cboStay3.Enabled = True
        Me.txtCommonrefNo.Enabled = True
        Me.BTNSEARCHHome.Enabled = True
        Me.BTNSEARCHProvince.Enabled = True
        Me.BTNSEARCHPermAddress.Enabled = True
        Me.txtMotherName.Enabled = True
        Me.txtMAdd.Enabled = True
        Me.txtMContact.Enabled = True
        Me.txtFatherName.Enabled = True
        Me.txtFAdd.Enabled = True
        Me.txtFContact.Enabled = True
        Me.btnMSearchAdd.Enabled = True
        Me.btnFSearchAdd.Enabled = True
        If cboemp_civil.Text = "Married" Then
            Me.txtSpouseName.Enabled = True
            Me.txtSpouseAdd.Enabled = True
            Me.txtSpouseContact.Enabled = True
            Me.txtSpouseStay.Enabled = True
            Me.txtOccupation.Enabled = True
            Me.txtChildNo.Enabled = True
            Me.btnSpouseSearchAdd.Enabled = True
            Me.rbSpouseMonth.Enabled = True
            Me.rbSpouseYears.Enabled = True
        End If
        Me.rbOwned.Enabled = True
        Me.rbRented.Enabled = True
        Me.rbParents.Enabled = True
        Me.rbMYes.Enabled = True
        Me.rbMNo.Enabled = True
        Me.rbCYes.Enabled = True
        Me.rbCNo.Enabled = True
        Me.txtCar.Enabled = True
        Me.txtMotor.Enabled = True
        If Me.txtDepartment2.Text = "MAS RESELLER" Then
            Me.cboOutlet.Enabled = True
        End If
        Me.rbClient.Enabled = True
        Me.rbEmployee.Enabled = True
        With Me
            .txtPresBldg.Enabled = True
            .txtPresStreet.Enabled = True
            .txtPresSitio.Enabled = True
            .txtPresCity.Enabled = True
            .txtPresProvince.Enabled = True
            .txtPrevBldg.Enabled = True
            .txtPrevStreet.Enabled = True
            .txtPrevSitio.Enabled = True
            .txtPrevCity.Enabled = True
            .txtPrevProvince.Enabled = True
            .txtPermBldg.Enabled = True
            .txtPermStreet.Enabled = True
            .txtPermSitio.Enabled = True
            .txtPermCity.Enabled = True
            .txtPermProvince.Enabled = True
            .txtSuffix.Enabled = True
            .dtBoardApproval.Enabled = True
            .txtbasicpay.Enabled = True
        End With
        cboContactPerson.Enabled = True
        txtProject.Enabled = True
    End Sub
#End Region

    Private Sub btnemp_edit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_edit.Click
        formMode = "Edit"
        If txtEmployeeNo.Text = "" Then
            MessageBox.Show("Nothing to Edit", "Invalid Editing", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            If btnemp_edit.Text = "Edit" Then
                '************************************
                Me.txtEmployeeNo.Enabled = True
                Me.temp_lname.Enabled = True
                Me.temp_fname.Enabled = True
                Me.temp_midname.Enabled = True
                Me.cbotaxcode.Enabled = True
                '************************************
                'Me.txtContractrate.Enabled = True
                Me.btnContractRate.Enabled = True
                'Me.cboClient.Enabled = True
                '************************************
                Call EnableControls()
                Call ShowBrowseBtn()
                Call EnableHRISButton()
                Call ClientorEmployee()
                Call DisableUploadButton()
                Me.txtEmployeeNo.Focus()
                btnemp_edit.Text = "Update"
                btnemp_edit.Image = My.Resources.save2
                btnemp_close.Text = "Cancel"
                ' btnemp_close.Location = New System.Drawing.Point(905, 3)
                btnemp_add.Visible = False
                btnemp_delete.Visible = False

                Me.btnemp_next.Enabled = False
                Me.btnemp_previous.Enabled = False
                Me.BTNSEARCH.Enabled = False
                Me.btnUpload.Enabled = False
                Me.btnLegalUpload.Enabled = False

                Call GetPk_Employee(Me.txtEmployeeNo.Text)

            ElseIf btnemp_edit.Text = "Update" Then
                Try
                    If isEloadingMemberValid() Then
                        'I change <> to = haha
                        'If Me.txtorgchart.Text = "" Then
                        Dim oStream As New MemoryStream
                        Dim bmp As New Bitmap(picEmpPhoto.Image)
                        bmp.Save(oStream, Imaging.ImageFormat.Jpeg)

                        Dim oStream1 As New MemoryStream
                        Dim bmp1 As New Bitmap(picEmpSignature.Image)
                        bmp1.Save(oStream1, Imaging.ImageFormat.Jpeg)

                        If Me.PicData2 Is Nothing And Me.SigData2 Is Nothing Then
                            Me.PicData2 = oStream.GetBuffer
                            Me.SigData2 = oStream1.GetBuffer
                        End If

                        If Me.SigData2 Is Nothing Then
                            picEmpSignature.Image = My.Resources.signature
                            Me.SigData2 = oStream1.GetBuffer
                        End If

                        '#######################################################
                        If cbotaxcode.Text = "" Then
                            cbotaxcode.Text = "ME"
                        End If
                        '#######################################################
                        If rdoExempt.Checked Then
                            exempt = True
                        Else
                            exempt = False
                        End If
                        '=======================================================
                        If rbOwned.Checked = True Then
                            HomeOwned = "Owned"
                        ElseIf rbRented.Checked = True Then
                            HomeOwned = "Rented"
                        ElseIf rbParents.Checked = True Then
                            HomeOwned = "Living With Parents"
                        End If

                        If rbMYes.Checked = True Then
                            Motor = True
                        Else
                            Motor = False
                        End If

                        If rbCYes.Checked = True Then
                            Car = True
                        Else
                            Car = False
                        End If
                        '=======================================================

                        'If txtCompanyName.Text <> "" And temp_lname.Text = "" And temp_fname.Text = "" And temp_midname.Text = "" Then
                        '    'Call AddCompanyName()
                        '    Call EditnewExistingmember(fbemployed, Me.txtEmployeeNo.Text.Trim, Me.txtMemberID.Text.Trim, "", "", Me.txtCompanyName.Text, _
                        '                                                 Me.cboemp_gender.Text, Me.cboemp_civil.Text.Trim, Me.EMP_DATEbirth.Value.Date, Me.temp_placebirth.Text, Me.txtLandline.Text, Me.txtresidencephone.Text, Me.emp_company.Text, Me.txtofficeadd.Text, _
                        '                                                 Me.emp_Datehired.Value.Date, Me.txtlocalofficenumber.Text, Me.txtofficenumber.Text, Me.txtbasicpay.Text, Me.mem_MemberDate.Value.Date, _
                        '                                                 bereavement, Me.txtwithdrawal.Text, Me.txtEmailAddress.Text, Me.txtpayrollcontriamount.Text, Me.mem_PaycontDate.Value.Date, Me.txtorgchart.Tag, _
                        '                                                 Me.cbotaxcode.Text, "", Me.Cbomem_Status.Text, Me.cbotitledesignation.Text, Me.cborate.Text, Me.cboPaycode.Text, Me.cbopayroll.Text, _
                        '                                                 Me.PicData2, Me.SigData2, Me.cbofGroup.Text, Me.cbofCategory.Text, Me.cbofType.Text, entengfxkey, Me.cboemp_status.Text, Me.cboEmp_type.Text, Me.cboStay2.Text, Me.cboStay3.Text, _
                        '                                                 Me.cboStay1.Text)
                        'Else
                        Call EditnewExistingmember(fbemployed, Me.txtEmployeeNo.Text.Trim, Me.txtMemberID.Text.Trim, Me.temp_fname.Text, Me.temp_midname.Text, Me.temp_lname.Text, _
                                                                     Me.cboemp_gender.Text, Me.cboemp_civil.Text.Trim, Me.EMP_DATEbirth.Value.Date, Me.temp_placebirth.Text, Me.txtLandline.Text, Me.txtresidencephone.Text, Me.emp_company.Text, Me.txtofficeadd.Text, _
                                                                     Me.emp_Datehired.Value.Date, Me.txtlocalofficenumber.Text, Me.txtofficenumber.Text, Me.txtbasicpay.Text, Me.mem_MemberDate.Value.Date, _
                                                                     bereavement, Me.txtwithdrawal.Text, Me.txtEmailAddress.Text, Me.txtpayrollcontriamount.Text, Me.mem_PaycontDate.Value.Date, Me.txtorgchart.Tag, _
                                                                     Me.cbotaxcode.Text, "", Me.Cbomem_Status.Text, Me.cbotitledesignation.Text, Me.cborate.Text, Me.cboPaycode.Text, Me.cbopayroll.Text, _
                                                                     Me.PicData2, Me.SigData2, Me.cbofGroup.Text, Me.cbofCategory.Text, Me.cbofType.Text, entengfxkey, Me.cboemp_status.Text, Me.cboEmp_type.Text, Me.cboStay2.Text, Me.cboStay3.Text, _
                                                                     Me.cboStay1.Text)
                        Call UpdatePayroll_EmployeeFile()
                        Call getContractHistoryComp()
                        Call getContractorgchartvalue()
                        Call SaveContractHistory()
                        'End If
                        'End If
                        'End If
                        'eloading Service Registration
                        If chkIsEloader.Checked = True Then
                            Call RegisterMemberToEloadingApps(txtEmployeeNo.Text.Trim, txtEloaderPIN.Text, txtEloadingLimit.Text)
                        Else
                            Call UnRegisterMemberToEloadingApps(txtEmployeeNo.Text.Trim)
                        End If

                        '************************************
                        Me.txtEmployeeNo.Enabled = False
                        Me.temp_lname.Enabled = False
                        Me.temp_fname.Enabled = False
                        Me.temp_midname.Enabled = False
                        Me.cbotaxcode.Enabled = False
                        '************************************
                        'Me.txtContractrate.Enabled = False
                        Me.btnContractRate.Enabled = False
                        'Me.cboClient.Enabled = False
                        '************************************
                        'btnrestricted.Visible = True
                        Call HideBrowseBtn()
                        Call DisableHRISButton()
                        Call DisableControls()
                        btnemp_edit.Text = "Edit"
                        btnemp_edit.Image = My.Resources.edit1
                        btnemp_close.Text = "Close"
                        btnemp_add.Visible = True
                        btnemp_delete.Visible = True
                        'btnrestricted.Visible = True
                        btnemp_next.Enabled = True
                        btnemp_previous.Enabled = True
                        BTNSEARCH.Enabled = True
                        Me.btnUpload.Enabled = True
                        Me.btnLegalUpload.Enabled = True
                        ' btnemp_close.Location = New System.Drawing.Point(972, 3)
                        'Else
                        '    'MessageBox.Show("Supply the required fields", "Cannot Update", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        '    frmMsgBox.txtMessage.Text = "Cannot Update , Supply the required field!"
                        '    frmMsgBox.Show()
                        'End If
                    End If
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            End If
        End If
    End Sub

    '=========================================
    '''''''''''''''''''''''' 
    ''NAVIGATING RECORDS  ''
    ''created aug 25,2006 ''
    '==========================================    

    Private Sub Navigate_Record(ByVal pkemployee As String, ByVal chardirection As String)


        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_Member_RecordNavigation", _
                                     New SqlParameter("@fxkeyemployee", pkemployee), _
                                     New SqlParameter("@chrMode", chardirection))
        Try
            While rd.Read
                Me.txtpk_Employee.Text = rd.Item(0)
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Navigation", MessageBoxButtons.OK)
        End Try


    End Sub

    'Private Sub btnaddpic_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnaddpic.Click
    '    Me.OpenFileDialog1.FileName = ""
    '    Me.OpenFileDialog1.Filter = "jpg (*.jpg)|*.* |All Files (*.*)|*.*"
    '    Me.OpenFileDialog1.FilterIndex = 2
    '    Try
    '        If Me.OpenFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
    '            Try
    '                txtpicture.Text = Me.OpenFileDialog1.FileName
    '                emp_pic.Image = New Bitmap(OpenFileDialog1.FileName)
    '            Catch ex As Exception
    '                MessageBox.Show("File Format is not valid", "Invalid Format", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '            End Try
    '        End If
    '    Catch ex As Exception
    '    End Try
    'End Sub
    'Public Sub getin_picture()
    '    Dim appRdr As New System.Configuration.AppSettingsReader
    '    Dim myconnection As New Clsappconfiguration
    '    Dim cmd As New SqlCommand("INSERT INTO Temployee_picture (Picturedata,idnumber) " & _
    '        "VALUES (@picturedata,idnumber)", myconnection.sqlconn)
    '    Dim strBLOBFilePath As String = txtpicture.Text
    '    '"C:\Documents and Settings\All Users\Documents" & _
    '    '"\My Pictures\Sample Pictures\winter.jpg"
    '    Dim fsBLOBFile As New FileStream(strBLOBFilePath, _
    '        FileMode.Open, FileAccess.Read)
    '    Dim bytBLOBData(fsBLOBFile.Length() - 1) As Byte
    '    fsBLOBFile.Read(bytBLOBData, 0, bytBLOBData.Length)
    '    fsBLOBFile.Close()
    '    Dim prm As New SqlParameter("@picturedata", SqlDbType.VarBinary, _
    '        bytBLOBData.Length, ParameterDirection.Input, False, _
    '        0, 0, Nothing, DataRowVersion.Current, bytBLOBData)
    '    cmd.Parameters.Add(prm)
    '    myconnection.sqlconn.Open()
    '    cmd.ExecuteNonQuery()
    '    myconnection.sqlconn.Close()
    'End Sub

    Public Sub LOAD_PICTURE_FROM_DBASE(ByVal i As String)
        'Dim myconnection As New SqlConnection(My.Settings.CNString)
        'Dim cmd As New SqlCommand("SELECT BLOBID, " & _
        '"BLOBData FROM BLOBTest WHERE BLOBID=", DataGridView1.CurrentRow.Cells(0).Value)
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        'Dim myconnection As New SqlConnection(My.Settings.CNString)

        Dim cmd As New SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet

        cmd.CommandText = "select * from Temployee_picture where idnumber=" & "'" & i & "'" 'i & "'"
        cmd.CommandType = CommandType.Text
        cmd.Connection = myconnection.sqlconn
        myconnection.sqlconn.Open()

        da.SelectCommand = cmd
        da.Fill(ds, "Temployee_picture")

        Dim c As Integer = ds.Tables("Temployee_picture").Rows.Count
        If c > 0 Then
            Dim bytBLOBData() As Byte = _
                ds.Tables("Temployee_picture").Rows(c - 1)("picturedata")
            Dim stmBLOBData As New MemoryStream(bytBLOBData)
            emp_pic.Image = Image.FromStream(stmBLOBData)


        Else
            emp_pic.ImageLocation = Application.StartupPath & "\SplashScreen v3.jpg"
            emp_pic.Load()
        End If
        myconnection.sqlconn.Close()
    End Sub

    Private Sub Temp_educ_attainmentBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


    End Sub

#Region "Delete Second Company"
    Private Sub deleteSecondCompany()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_secondcompany_delete", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        With cmd.Parameters
            .Add("@fxkeyemployee", SqlDbType.Char, 10).Value = Me.TXTRECID.Text
        End With
        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region

    Private Sub btnemp_delete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_delete.Click
        Dim x As New DialogResult
        x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
        If x = System.Windows.Forms.DialogResult.OK Then
            Call DeleteAwards(txtEmployeeNo.Text)
            Call DeleteBankAccount(txtEmployeeNo.Text)
            Call DeleteDependents(txtEmployeeNo.Text)
            Call DeleteDiscipline(txtEmployeeNo.Text)
            Call DeleteEducationalInfo(txtEmployeeNo.Text)
            Call DeleteEmployment(txtEmployeeNo.Text)
            Call DeleteEmpOrg(txtEmployeeNo.Text)
            Call DeleteEvaluation(txtEmployeeNo.Text)
            Call DeleteGovExam(txtEmployeeNo.Text)
            Call DeleteJobDescription(txtEmployeeNo.Text)
            Call DeleteLegalCase(txtEmployeeNo.Text)
            Call DeleteMedicals(txtEmployeeNo.Text)
            Call DeleteRelatives(txtEmployeeNo.Text)
            Call DeleteSkills(txtEmployeeNo.Text)
            Call DeleteSourceIncome(txtEmployeeNo.Text)
            Call DeleteTrainings(txtEmployeeNo.Text)
            Call TaggedDeleted(Me.txtEmployeeNo.Text.Trim, frmLogin.admin)
            Call cleartxt()
            Call clearlistview()
            lvlSibling.Clear()
            lvlAttachment.Clear()
            lvlContractHis.Clear()
            picEmpPhoto.Image = My.Resources.photo
            picEmpSignature.Image = My.Resources.signature
        ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
        End If
    End Sub

#Region "Tagging Deleted in Member Records"
    Private Sub TaggedDeleted(ByVal empID As String, ByVal Users As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Master_Delete", _
                                      New SqlParameter("@employeeNo", empID), _
                                      New SqlParameter("@user", Users))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            'MessageBox.Show(ex.Message, "CIMS_Member_Master_Delete")
            frmMsgBox.txtMessage.Text = "Unable to Delete, Client already haved a transaction!"
            frmMsgBox.ShowDialog()
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region

#Region "Delete masterfile permanently"
    Private Sub deletemasterfilepermanent(ByVal fxkey As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_delete_permanently", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            cmd.Parameters.Add("@fxkeyemployee", SqlDbType.VarChar, 10).Value = fxkey
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show("There is no file delete", "eHRMS", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub
#End Region
    Private Sub Delete_all_employee_records()

        Dim myconnection As New Clsappconfiguration
        'Dim cmd As New SqlCommand("usp_per_employee_master_delete_update", myconnection.sqlconn)
        Dim cmd As New SqlCommand("usp_per_employee_master_delete_update_PERSTK", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            cmd.Parameters.Add("@fxkeyemployee", SqlDbType.VarChar, 10).Value = Me.TXTRECID.Text
            'cmd.Parameters.Add("@Emp_Deleted", SqlDbType.Bit).Value = 1
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Delete Record", MessageBoxButtons.OK)
        End Try
    End Sub

#Region "Employee Disciplinary Case Delete/stored proc"
    Private Sub setdefaultvaluedatesuspend()
        Dim mycon As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_update_suspensiondate", mycon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            With cmd.Parameters
                .Add("@OPTION", SqlDbType.Int).Value = 2
                .Add("@fxKeyEmployee", SqlDbType.Char, 6).Value = Me.TXTKEYEMPLOYEEID.Text
                .Add("@fdDateSuspendedFrom", SqlDbType.SmallDateTime).Value = "1/1/1900"
                .Add("@fdDateSuspendedTo", SqlDbType.SmallDateTime).Value = "1/1/1900"
            End With
            mycon.sqlconn.Open()
            cmd.ExecuteNonQuery()
            mycon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Suspension", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region

    Private empIDValue As String
    Public Property empIDs() As String
        Get
            Return empIDValue
        End Get
        Set(ByVal value As String)
            empIDValue = value
        End Set
    End Property

    Private Sub txtbasicpay_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtbasicpay.KeyPress
        e.Handled = Not CurrencyInput_Validate(e.KeyChar)

    End Sub

    Private Sub txtpermonth_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not CurrencyInput_Validate(e.KeyChar)
    End Sub

    Private Sub txtecola_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not CurrencyInput_Validate(e.KeyChar)
    End Sub

    Public Function CurrencyInput_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = ".") And Not (C = "-") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Numeric only")
            Return False
        Else
            Return True
        End If
    End Function

    Public Sub computedatetenures()
        'Try
        '    Dim iMonthHired As Long = 0
        '    iMonthHired = DateDiff(DateInterval.Month, Me.emp_Datehired.Value, Now.Date)
        '    If iMonthHired < 12 Then
        '        Label39.Text = "0"
        '    Else
        '        Label39.Text = CInt(DateDiff(DateInterval.Month, Me.emp_Datehired.Value, Now) / 12)
        '    End If
        '    Me.lblmonth.Text = iMonthHired Mod 12
        'Catch ex As Exception
        'End Try
    End Sub


    Private Sub EMP_DATEbirth_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles EMP_DATEbirth.TextChanged
        'Call compute_age()
        Call GetAge(EMP_DATEbirth.Value)
    End Sub
    Private Sub EMP_DATEbirth_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EMP_DATEbirth.ValueChanged
        'Call compute_age()
        Call GetAge(EMP_DATEbirth.Value)
    End Sub
    Public Sub compute_age()
        Dim month1 As Integer = EMP_DATEbirth.Value.Month
        Dim month2 As Integer = Now.Month
        Dim oldday As Integer = EMP_DATEbirth.Value.Day
        Dim presentday As Integer = Now.Day
        Dim xBday As New Date

        Dim x, y, a, b As Integer

        x = EMP_DATEbirth.Value.Month
        y = Now.Month
        a = EMP_DATEbirth.Value.Day
        b = Now.Day

        If x >= y And a > b Then

            'Label41.Text = DateDiff(DateInterval.Year, EMP_DATEbirth.Value, Now) - 1 '& " yr's" & " " & "old"
            Me.txtage1.Text = DateDiff(DateInterval.Year, EMP_DATEbirth.Value, Now) - 1
        ElseIf x <= y And a < b Then
            'Label41.Text = DateDiff(DateInterval.Year, EMP_DATEbirth.Value, Now)
            Me.txtage1.Text = DateDiff(DateInterval.Year, EMP_DATEbirth.Value, Now)
        ElseIf x <= y And a <= b Then
            'Label41.Text = DateDiff(DateInterval.Year, EMP_DATEbirth.Value, Now) '& '" yr's" & " " & "old"
            Me.txtage1.Text = DateDiff(DateInterval.Year, EMP_DATEbirth.Value, Now)
        ElseIf x <= y And a >= b Then
            'Label41.Text = DateDiff(DateInterval.Year, EMP_DATEbirth.Value, Now) '& '" yr's" & " " & "old"
            Me.txtage1.Text = DateDiff(DateInterval.Year, EMP_DATEbirth.Value, Now)
        End If
    End Sub
#Region "function to compute exact age"
    Public Function GetAge(ByVal Birthdate As System.DateTime, _
    Optional ByVal AsOf As System.DateTime = #1/1/1700#) _
    As String

        'Don't set second parameter if you want Age as of today

        'Demo 1: get age of person born 2/11/1954
        'Dim objDate As New System.DateTime(1954, 2, 11)
        'Debug.WriteLine(GetAge(objDate))

        'Demo 1: get same person's age 10 years from now
        'Dim objDate As New System.DateTime(1954, 2, 11)
        'Dim objdate2 As System.DateTime
        'objdate2 = Now.AddYears(10)
        'Debug.WriteLine(GetAge(objDate, objdate2))

        Dim iMonths As Integer
        Dim iYears As Integer
        Dim dYears As Decimal
        Dim lDayOfBirth As Long
        Dim lAsOf As Long
        Dim iBirthMonth As Integer
        Dim iAsOFMonth As Integer

        If AsOf = "#1/1/1700#" Then
            AsOf = DateTime.Now
        End If
        lDayOfBirth = DatePart(DateInterval.Day, Birthdate)
        lAsOf = DatePart(DateInterval.Day, AsOf)

        iBirthMonth = DatePart(DateInterval.Month, Birthdate)
        iAsOFMonth = DatePart(DateInterval.Month, AsOf)

        iMonths = DateDiff(DateInterval.Month, Birthdate, AsOf)

        dYears = iMonths / 12

        iYears = Math.Floor(dYears)

        If iBirthMonth = iAsOFMonth Then
            If lAsOf < lDayOfBirth Then
                iYears = iYears - 1
            End If
        End If
        Me.txtage1.Text = iYears
        Return iYears

    End Function

#End Region
    Private Sub cboEmp_type_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmp_type.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub
    Private Sub cboEmp_type_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmp_type.SelectedIndexChanged
        Call employee_type_selected_parameters()

    End Sub

#Region "Emplyee type convert from key to description"
    Private Sub employeeType_convert(ByVal typecode As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_type_code_description_select", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@fxkeyemptype", SqlDbType.Char, 3).Value = typecode
        End With
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                Me.cboEmp_type.Text = myreader.Item("Typedescription")
            End While
            myreader.Read()
        Finally

        End Try
        myconnection.sqlconn.Close()
    End Sub
#End Region

#Region "Employee status convert key to description"
    'Private Sub employeeStatusdescription()
    '    Dim myconnection As New Clsappconfiguration
    '    Dim cmd As New SqlCommand("usp_per_employee_status_description_select", myconnection.sqlconn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    myconnection.sqlconn.Open()
    '    With cmd.Parameters
    '        .Add("@Statuskey", SqlDbType.Char, 3).Value = Me.cboemp_status.Text
    '    End With
    '    Dim myreader As SqlDataReader
    '    myreader = cmd.ExecuteReader
    '    Try
    '        While myreader.Read
    '            Me.cboemp_status.Text = myreader.Item(0)
    '        End While
    '        myreader.Close()
    '    Finally

    '    End Try
    '    myconnection.sqlconn.Close()
    'End Sub
#End Region

#Region "Employee Type selected parameters"
    Private Sub employee_type_selected_parameters()
        Dim myconnection As New Clsappconfiguration

        Dim cmd As New SqlCommand("usp_per_employee_type_select_get", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@Typedescription", SqlDbType.VarChar, 50).Value = Me.cboEmp_type.Text

        Dim mydatareader As SqlDataReader = cmd.ExecuteReader()
        Try
            While mydatareader.Read()

                Me.txtType_employee.Text = mydatareader.Item(0)
            End While
        Finally
            mydatareader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region

#Region "Employee Position in Combo"
    Public Sub load_employeeposition_combo()
        Dim myconnection As New Clsappconfiguration

        Dim cmdjol As String = "Select Description from dbo.Per_Employee_Position order by description asc"
        Dim cmd As New SqlCommand(cmdjol, myconnection.sqlconn)
        myconnection.sqlconn.Open()

        Dim mydatareader As SqlDataReader = cmd.ExecuteReader()

        Try
            While mydatareader.Read()

                cbotitledesignation.Items.Add(mydatareader(0))
            End While
        Finally
            mydatareader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region

#Region "Employee Positionlevelcode"
    Public Sub Employee_position_fxkeyposition()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_position_fxkeyposition", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@description", SqlDbType.VarChar, 80).Value = Me.cbotitledesignation.Text
        Dim mydatareader As SqlDataReader = cmd.ExecuteReader()
        Try
            While mydatareader.Read()
                Me.txtfxkeypositionlevel.Text = mydatareader.Item(0)
            End While
        Finally
            mydatareader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region

#Region "Employee Status"
    Public Sub EmployeeStatus()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("CIMS_m_Membership_Status_view", myconnection.sqlconn)
        myconnection.sqlconn.Open()
        Dim mydatareader As SqlDataReader = cmd.ExecuteReader()
        Try
            While mydatareader.Read()
                Me.Cbomem_Status.Items.Add(mydatareader(0))
            End While
        Finally
            mydatareader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region

#Region "Employee Type in Combo"
    Public Sub Employee_Type()
        Dim myconnection As New Clsappconfiguration

        Dim cmdjol As String = "Select typeDescription from dbo.Per_Employee_type order by typedescription asc"
        Dim cmd As New SqlCommand(cmdjol, myconnection.sqlconn)
        myconnection.sqlconn.Open()
        Dim mydatareader As SqlDataReader = cmd.ExecuteReader()
        Try
            While mydatareader.Read()
                cboEmp_type.Items.Add(mydatareader(0))
            End While
        Finally
            mydatareader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region

    'Private Sub txtpermonth_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
    '    If txtpermonth.Text = "" Then
    '        Me.txtpermonth.Text = "0.00"

    '    Else

    '        If Not IsNumeric(txtpermonth.Text) Then
    '            ErrorProvider1.SetError(txtpermonth, "Please write correct number format")
    '            txtpermonth.Focus()
    '            ErrorProvider1.SetError(txtbasicpay, "")
    '        Else
    '            txtpermonth.Text = Format(Decimal.Parse(txtpermonth.Text), "###,###,###.000")

    '        End If

    '    End If
    'End Sub

    Public Sub resigned_employee2()
        Dim myreader As SqlDataReader
        Dim ctr As Integer = 0
        Dim counter As Integer = 0
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        'Dim myconnection As New SqlConnection(My.Settings.CNString)
        Dim cmd As New SqlCommand
        cmd.CommandText = "select * from dbo.temp_masterfile where idnumber=" & "'" & txtEmployeeNo.Text & "'"
        myconnection.sqlconn.Open()
        cmd.CommandType = CommandType.Text
        cmd.Connection = myconnection.sqlconn

        myreader = cmd.ExecuteReader()
        With myreader
            If .HasRows Then
                While .Read
                    snumber(ctr) = .Item("idnumber")
                    sfirstname(ctr) = .Item("firstname")
                    slastname(ctr) = .Item("lastname")
                    smiddlename(ctr) = .Item("middlename")
                    semployeetype(ctr) = .Item("employee_type")
                    sGender(ctr) = .Item("gender")
                    sdepartment(ctr) = .Item("department")
                    dhired(ctr) = .Item("date_hired")
                    smobile_phone(ctr) = .Item("mobile_phone")
                    saddress_1(ctr) = .Item("address_1")
                    ctr += 1
                    counter += 1
                End While
            End If
        End With
        myreader.Close()


        cmd.CommandText = "USP_EMPLOYEE_RESIGNED_INSERT"
        cmd.CommandType = CommandType.StoredProcedure

        For ctr = 0 To counter - 1
            With cmd.Parameters
                .Clear()
                .Add("@idnumber", SqlDbType.VarChar, 50).Value = snumber(ctr)
                .Add("@firstname", SqlDbType.VarChar, 50).Value = sfirstname(ctr)
                .Add("@lastname", SqlDbType.VarChar, 50).Value = slastname(ctr)
                .Add("@middlename", SqlDbType.VarChar, 50).Value = smiddlename(ctr)
                .Add("@employee_type", SqlDbType.VarChar, 50).Value = semployeetype(ctr)
                .Add("@Gender", SqlDbType.VarChar, 50).Value = sGender(ctr)
                .Add("@department", SqlDbType.VarChar, 50).Value = sdepartment(ctr)
                .Add("@date_hired", SqlDbType.DateTime, 8).Value = dhired(ctr)
                .Add("@mobile_phone", SqlDbType.VarChar, 50).Value = smobile_phone(ctr)
                .Add("@address_1", SqlDbType.VarChar, 50).Value = saddress_1(ctr)
            End With
            myreader = cmd.ExecuteReader
            myreader.Close()
        Next
        myconnection.sqlconn.Close()
    End Sub

    Private Sub cboregularization_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not cboregularization_Validate(e.KeyChar)
    End Sub
    Public Function cboregularization_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")
            txtitemno.Focus()
            Return False
        Else
            Return True
        End If
    End Function
    Public Sub load_configurationfile()

        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        'Dim myconnection As New SqlConnection(My.Settings.CNString)

        Dim months As String = "select * from dbo.tconfigurationfile"
        Dim cmd As New SqlCommand(months, myconnection.sqlconn)
        cmd.Connection = myconnection.sqlconn
        myconnection.sqlconn.Open()

        Dim myreader As SqlDataReader = cmd.ExecuteReader()
        Try
            While myreader.Read()
                'cboregularization.Items.Add(myreader(1))
            End While
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub

    Private Sub cboemp_status_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboemp_status.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub


#Region "Update idnumber for newly hire employee"
    Public Sub updateid_newhiredemployee()
        Dim count As Integer = 0
        Dim rowcount As Integer = 0
        Dim dr As SqlDataReader
        Dim cm As SqlCommand
        Dim myconnection As New Clsappconfiguration


        Try
            cm = New SqlCommand
            cm.Connection = myconnection.sqlconn
            myconnection.sqlconn.Open()
            cm.CommandText = "USP_IDNUMBER_UPDATE"
            cm.CommandType = CommandType.StoredProcedure

            cm.Parameters.Add("@IDnumber", SqlDbType.VarChar, 50).Value = txtEmployeeNo.Text
            cm.Parameters.Add("@OldIDNumber", SqlDbType.VarChar, 50).Value = "" 'm_sOldIDNumber

            dr = cm.ExecuteReader

            dr.Close()
        Catch ex As Exception

        End Try
    End Sub
#End Region

    Private Sub txtsss_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not txtsss_Validate(e.KeyChar)
    End Sub
    Public Function txtsss_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = "-") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub txtpagibig_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not txtpagibig_Validate(e.KeyChar)
    End Sub

    Public Function txtpagibig_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = "-") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")

            Return False
        Else
            Return True
        End If
    End Function

    Private Sub txthealth_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not txthealth_Validate(e.KeyChar)
    End Sub
    Public Function txthealth_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = "-") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")

            Return False
        Else
            Return True
        End If
    End Function

    Private Sub emp_phone_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtresidencephone.KeyPress
        e.Handled = Not emp_phone_Validate(e.KeyChar)
    End Sub

    Public Function emp_phone_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = "-") And Not (C = "(") And Not (C = ")") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) _
        And Not (Microsoft.VisualBasic.AscW(C) = 22) And Not (Microsoft.VisualBasic.AscW(C) = 3) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")
            txtresidencephone.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub emp_extphone_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles emp_extphone.KeyPress
        e.Handled = Not emp_extphone_Validate(e.KeyChar)
    End Sub
    Public Function emp_extphone_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = "-") And Not (C = "(") And Not (C = ")") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) _
        And Not (Microsoft.VisualBasic.AscW(C) = 22) And Not (Microsoft.VisualBasic.AscW(C) = 3) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")
            emp_extphone.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Public Function emp_mobile_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = "-") And Not (C = "(") And Not (C = ")") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) _
        And Not (Microsoft.VisualBasic.AscW(C) = 22) And Not (Microsoft.VisualBasic.AscW(C) = 3) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")
            txtMobileNo.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub cbopayroll_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cbopayroll.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cbopayroll_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbopayroll.SelectedIndexChanged

    End Sub

    Private Sub cbotax_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cbotax_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub cboshift_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cboshift_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'load_shiftschedule()
    End Sub

    Private Sub cborate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cborate.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cboemp_gender_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboemp_gender.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cboemp_civil_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboemp_civil.Click

    End Sub

    Private Sub cboemp_civil_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboemp_civil.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cbodept_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click

        Dim orgchart As New frmOrgChart_Master(orgchart.selection.Org_Emp)
        orgchart.Width = 257
        orgchart.btnClose.Location = New System.Drawing.Point(179, 6)
        orgchart.btnAdd.Enabled = False
        orgchart.btnPrint.Visible = False
        orgchart.btnPrint.Visible = False
        If orgchart.ShowDialog = Windows.Forms.DialogResult.Yes Then
            Me.txtorgchart.Text = orgchart.org_parent_desc
            Me.txtorgchart.Tag = orgchart.orgid
            Me.txtDepartment2.Text = orgchart.department
            Me.txtorgchart.Text = Mid(Me.txtorgchart.Text, 21)
            Call proposeddeptdivision(Me.txtorgchart.Tag)
        End If
        If Me.txtDepartment2.Text = "MAS RESELLER" Then
            Me.cboOutlet.Enabled = True
        End If
    End Sub

    Private Sub cborate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cborate.SelectedIndexChanged


    End Sub
#Region "Rate typecode convert to description"
    Private Sub rateToDescription()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_emloyee_rate_codedecscription_select", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@Rate_code", SqlDbType.Char, 4).Value = Me.cborate.Text
        End With
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                Me.cborate.Text = myreader.Item(0)
            End While
            myreader.Close()
        Finally
        End Try
        myconnection.sqlconn.Close()
    End Sub

#End Region

    Public Sub delete_picture()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        'Dim myconnection As New SqlConnection(My.Settings.CNString)

        Dim cmd As New SqlCommand("USP_PICTURE_DELETE", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@idnumber", SqlDbType.VarChar, 50).Value = txtEmployeeNo.Text

        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub

    Private Sub cbosalarygrade_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cbosalarygrade.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Public Sub load_salarygrade_1()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        'Dim myconnection As New SqlConnection(My.Settings.CNString)

        Dim myreader As SqlDataReader

        Dim mydataset As New DataSet
        Dim cmdjol As String = "Select Salarycode,salary1,salary2 from dbo.Per_Employee_SalaryGrade order by salarycode asc"
        Dim cmd As New SqlCommand(cmdjol, myconnection.sqlconn)
        cmd.CommandType = CommandType.Text
        myconnection.sqlconn.Open()

        myreader = cmd.ExecuteReader()

        Try
            While myreader.Read()

                Me.cbosalarygrade.Items.Add(myreader(0))

            End While
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub

#Region "Increment key employee id/stored proc"
    Public Sub INCREMENT_KEYEMPLOYEEID()
        Dim myconnection As New Clsappconfiguration
        Dim CMD As New SqlCommand("USP_INCREMENT_KEYEMPLOYEEID_NUMBER_SELECT", myconnection.sqlconn)
        CMD.CommandType = CommandType.StoredProcedure
        'CMD.Parameters.Add("@fxkeyemployee", SqlDbType.VarChar, 10).Value = TXTKEYEMPLOYEEID.Text
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = CMD.ExecuteReader
        Try
            While myreader.Read()
                TXTKEYEMPLOYEEID.Text = myreader.Item(0)
            End While
        Finally
            myconnection.sqlconn.Close()
            myreader.Close()
        End Try
    End Sub
#End Region

    Private Sub btnemp_next_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_next.Click

        Call Navigate_Record(Me.txtpk_Employee.Text.Trim, "N")
        Call LoadEloadingRegistrationDetails(txtEmployeeNo.Text)

        'Call compute_age()
        Call GetAge(EMP_DATEbirth.Value)
        'Call computedatetenures()
        If txtCompanyName.Text <> "" And Me.temp_fname.Text = "" And Me.temp_lname.Text = "" Then
            Me.lblemployee_name.Text = Me.txtCompanyName.Text
        Else
            Me.lblemployee_name.Text = Me.temp_fname.Text + " " + Me.temp_midname.Text + " " + Me.temp_lname.Text
        End If

    End Sub

    Private Sub btnemp_previous_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_previous.Click

        Call Navigate_Record(Me.txtpk_Employee.Text.Trim, "P")
        Call LoadEloadingRegistrationDetails(txtEmployeeNo.Text)

        'Call computedatetenures()
        'Call compute_age()
        Call GetAge(EMP_DATEbirth.Value)
        If txtCompanyName.Text <> "" And Me.temp_fname.Text = "" And Me.temp_lname.Text = "" Then
            Me.lblemployee_name.Text = Me.txtCompanyName.Text
        Else
            Me.lblemployee_name.Text = Me.temp_fname.Text + " " + Me.temp_midname.Text + " " + Me.temp_lname.Text
        End If

    End Sub

#Region "Get fxkeyemployee"
    Private Sub getfexkeyemployee()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_masterfile_selectkey_get", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@IDnumber", SqlDbType.VarChar, 50).Value = Me.txtEmployeeNo.Text
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                Me.TXTKEYEMPLOYEEID.Text = myreader.Item(0)
            End While
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
    Public Sub load_masterfile_in_txtchangedID()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        Dim x As Integer = 0
        'Dim myconnection As New SqlConnection(My.Settings.CNString)

        Dim myreader As SqlDataReader
        Dim cmd As New SqlCommand
        cmd.CommandText = "USP_NAVIGATE_EMPLOYEE_RECORDS_SELECT"
        myconnection.sqlconn.Open()
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = myconnection.sqlconn

        cmd.Parameters.Add("@FXKEYEMPLOYEE", SqlDbType.VarChar, 10).Value = Me.TXTRECID.Text

        myreader = cmd.ExecuteReader
        Try
            While myreader.Read()

                'Me.txtreplicateIDnumber.Text = myreader.Item(0)
                'txtEmpID.Text = myreader.Item(0)
                temp_fname.Text = myreader.Item(1)
                temp_lname.Text = myreader.Item(2)
                temp_midname.Text = myreader.Item(3)
                'cboEmp_type.Text = myreader.Item(4)
                cboemp_gender.Text = myreader.Item(5)
                cboemp_civil.Text = myreader.Item(6)
                cboemp_status.Text = myreader.Item(7)
                Me.txtorgchart.Text = myreader.Item(8)
                emp_Datehired.Text = myreader.Item(9)
                txtresidencephone.Text = myreader.Item(10) '10 COLUMNS
                emp_extphone.Text = myreader.Item(11)
                txtMobileNo.Text = myreader.Item(12)
                txtEmailAddress.Text = myreader.Item(13)
                Me.cbotitledesignation.Text = myreader.Item(14)
                'txtPrevAdd.Text = myreader.Item(15)
                EMP_DATEbirth.Text = myreader.Item(16)
                temp_placebirth.Text = myreader.Item(17)
                txtofficeadd.Text = myreader.Item(18)
                temp_citizen.Text = myreader.Item(19)
                '20 COLUMNS
                temp_height.Text = myreader.Item(20)
                txtofficenumber.Text = myreader.Item(21)
                txtlocalofficenumber.Text = myreader.Item(22)
                temp_add3.Text = myreader.Item(23)
                temp_marks.Text = myreader.Item(24)

                '30 COLUMNS

                cborate.Text = myreader.Item(31)
                txtbasicpay.Text = myreader.Item(32)


                Me.txtmonthreqular.Text = myreader.Item(42)
                txtweight.Text = myreader.Item(43)
                txtremarks.Text = myreader.Item(45) '50 COLUMNS

                cbosalarygrade.Text = myreader.Item(47)

                Me.txtage1.Text = myreader.Item(50)
                Me.TXTKEYEMPLOYEEID.Text = myreader.Item(51)
                TXTRECID.Text = myreader.Item(51) 'fxkeyemployee
                Me.txtorgchart.Tag = myreader.Item(52)


                Me.txtPresBldg.Text = myreader.Item(55)
                Me.txtDepartment2.Text = myreader.Item(56)

                'Me.TxtSeconded.Text = myreader.Item(58)

                Me.txtMemberID.Text = myreader.Item(62)


                'ADDITIONAL 

                'Me.Cbomem_Bereavement.Text = myreader.Item(71)
                'Me.mem_amountpaid.Text = myreader.Item(72)
                Me.txtpayrollcontriamount.Text = myreader.Item(73)
                Me.mem_PaycontDate.Text = myreader.Item(74)
                Me.mem_WithdrawDate.Text = myreader.Item(75)
                Me.mem_MemberDate.Text = myreader.Item(76)
                Me.Cbomem_Status.Text = myreader.Item(77)
                Me.emp_company.Text = myreader.Item(78)


            End While
            'If Me.txtpicture.Text = "" Then
            '    emp_pic.ImageLocation = Application.StartupPath & "\SplashScreen v3.jpg"
            '    emp_pic.Load()
            'End If

            Call getorgchartvalue(Me.txtorgchart.Tag)
            'Call getorgchartvalue(Me.TxtSeconded.Tag)
            Call getorgchartIDtodepartment2(Me.txtorgchart.Tag)
            Call getorgcode(Me.txtorgchart.Tag)
            Call getorgparentid(Me.txtorgchart.Tag)
            Call proposeddeptdivision(Me.txtorgchart.Tag)
            'oldidnumber = txtEmpID.Text

        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#Region "Toggle Resigned"
    Public Sub ToggleNavigateResigned()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        'Dim myconnection As New SqlConnection(My.Settings.CNString)

        Dim myreader As SqlDataReader
        Dim cmd As New SqlCommand
        cmd.CommandText = "USP_NAVIGATE_EMPLOYEE_RECORDS_SELECT_Resigned"
        myconnection.sqlconn.Open()
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = myconnection.sqlconn

        cmd.Parameters.Add("@FXKEYEMPLOYEE", SqlDbType.VarChar, 10).Value = Me.TXTRECID.Text

        myreader = cmd.ExecuteReader
        Try
            While myreader.Read()

                'Me.txtreplicateIDnumber.Text = myreader.Item(0)
                txtEmployeeNo.Text = myreader.Item(0)
                temp_fname.Text = myreader.Item(1)
                temp_lname.Text = myreader.Item(2)
                temp_midname.Text = myreader.Item(3)
                cboEmp_type.Text = myreader.Item(4)
                cboemp_gender.Text = myreader.Item(5)
                cboemp_civil.Text = myreader.Item(6)
                cboemp_status.Text = myreader.Item(7)
                Me.txtDepartment2.Text = myreader.Item(8)
                emp_Datehired.Text = myreader.Item(9) '10 COLUMNS
                txtresidencephone.Text = myreader.Item(10)
                emp_extphone.Text = myreader.Item(11)
                txtMobileNo.Text = myreader.Item(12)
                txtEmailAddress.Text = myreader.Item(13)
                Me.cbotitledesignation.Text = myreader.Item(14)
                'txtPrevAdd.Text = myreader.Item(15)
                EMP_DATEbirth.Text = myreader.Item(16)
                temp_placebirth.Text = myreader.Item(17)
                txtofficeadd.Text = myreader.Item(18)
                temp_citizen.Text = myreader.Item(19)
                '20 COLUMNS
                temp_height.Text = myreader.Item(20)
                txtofficenumber.Text = myreader.Item(21)
                txtlocalofficenumber.Text = myreader.Item(22)
                temp_add3.Text = myreader.Item(23)
                temp_marks.Text = myreader.Item(24)

                '30 COLUMNS

                cborate.Text = myreader.Item(31)
                txtbasicpay.Text = myreader.Item(32)


                cbopayroll.Text = myreader.Item(35)


                'txtlocation1.Text = myreader.Item(40)

                Me.txtmonthreqular.Text = myreader.Item(42)
                txtweight.Text = myreader.Item(43)
                txtremarks.Text = myreader.Item(45) '50 COLUMNS

                cbosalarygrade.Text = myreader.Item(47)

                'Label39.Text = myreader.Item(49)tenure
                ' tenure
                Me.txtage1.Text = myreader.Item(50)

                Me.TXTKEYEMPLOYEEID.Text = myreader.Item(51)
                TXTRECID.Text = myreader.Item(51) 'fxkeyemployee

                'Me.txtorgchart.Tag = myreader.Item(52)

                'Me.lblmonth.Text = myreader.Item(53)'tenure
                Me.txtPresBldg.Text = myreader.Item(53)

                Me.txtDepartment2.Text = myreader.Item(55)


            End While
            'If Me.txtpicture.Text = "" Then
            '    emp_pic.ImageLocation = Application.StartupPath & "\SplashScreen v3.jpg"
            '    emp_pic.Load()
            'End If

            Call getorgchartvalue(Me.txtorgchart.Tag)
            'Call getorgchartvalue(Me.TxtSeconded.Tag)
            Call getorgchartIDtodepartment2(Me.txtorgchart.Tag)
            Call getorgcode(Me.txtorgchart.Tag)
            Call getorgparentid(Me.txtorgchart.Tag)
            Call proposeddeptdivision(Me.txtorgchart.Tag)
            'oldidnumber = tempid_no.Text
            If myreader.Item("Resigned") = "1" Then
                'Me.btnresigned.Enabled = False
            End If
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
    Private Sub Load_RecordId_Masterfile()
        'Dim x As String
        Dim myconnection As New Clsappconfiguration
        Dim myreader As SqlDataReader

        Dim cmd As New SqlCommand("usp_per_employee_keyemployee", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        'cmd.Parameters.Add("@key", SqlDbType.VarChar, 10).Value = Me.TXTRECID.Text
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                Me.TXTRECID.Text = myreader.Item(0)
                'Call LOAD_PICTURE_FROM_DBASE(tempid_no.Text)
            End While
            'Me.TXTRECID.Text = x
            myreader.Close()
            myconnection.sqlconn.Close()
        Catch ex As Exception

        End Try

    End Sub

#Region "Employee payroll delete"
    Public Sub Employee_payroll_tk_delete()
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("spu_EMPLOYEEFile_MARKDelete", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@chrKeyEmployee", SqlDbType.Char, 6).Value = Me.TXTKEYEMPLOYEEID.Text
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Check Stored Delete", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
    Private Sub TXTRECID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TXTRECID.TextChanged
        getfxkeyemployee = Me.TXTRECID.Text

        If TXTRECID.Text = TXTRECID.Text Then
            Call load_masterfile_in_txtchangedID()
        End If

    End Sub

    Private Sub txtitemno_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtitemno.KeyPress
        e.Handled = Not txtitemno_Validate(e.KeyChar)
    End Sub

    Public Function txtitemno_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")
            txtitemno.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub Dateregular_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Dateregular.ValueChanged
        txtdateregular.Text = Dateregular.Value
    End Sub

    Public Sub RESIGNED_EMPLOYEE1()
        Try
            Dim myconnection As New Clsappconfiguration
            'Dim cmd As New SqlCommand("USP_UPDATE_EMPLOYEE_RESIGNED", myconnection.sqlconn)
            Dim cmd As New SqlCommand("USP_UPDATE_EMPLOYEE_RESIGNED_PERSTK", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@dateResigned", SqlDbType.DateTime).Value = Format(Now, "MM/dd/yyyy")
            cmd.Parameters.Add("@fxkeyemployee", SqlDbType.VarChar, 10).Value = RTrim(Me.TXTKEYEMPLOYEEID.Text)
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Resigned", MessageBoxButtons.OK)
        End Try
    End Sub

#Region "Transfer Deleted Employee"
    Private Sub TransferDeletedEmployee(ByVal idnumber As String, ByVal firstname As String, ByVal lastname As String, _
                                        ByVal middlename As String, ByVal emptype As String, ByVal gender As String, _
                                        ByVal civilstatus As String, ByVal empstatus As String, ByVal department As String, _
                                        ByVal datehired As String, ByVal position As String, ByVal SG As String, ByVal datebirth As String, _
                                        ByVal placebirth As String, ByVal religion As String, ByVal citizenship As String, _
                                        ByVal height As String, ByVal add1 As String, ByVal add2 As String, ByVal add3 As String, _
                                        ByVal atm As String, ByVal gsis As String, ByVal pagibig As String, ByVal tin As String, _
                                        ByVal philhealth As String, ByVal nbi As String, ByVal sss As String, ByVal rate As String, _
                                        ByVal basicpay As String, ByVal permonth As String, ByVal ecola As String, ByVal payschedule As String, _
                                        ByVal tax As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_employee_transferDeleted_insert", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("@idnumber", SqlDbType.VarChar, 50).Value = idnumber
            cmd.Parameters.Add("@fxkeyemployee", SqlDbType.VarChar, 9).Value = ""
            cmd.Parameters.Add("@Firstname", SqlDbType.VarChar, 50).Value = firstname
            cmd.Parameters.Add("@Lastname", SqlDbType.VarChar, 50).Value = lastname
            cmd.Parameters.Add("@Middlename", SqlDbType.VarChar, 50).Value = middlename
            cmd.Parameters.Add("@Employee_Type", SqlDbType.VarChar, 50).Value = emptype
            cmd.Parameters.Add("@Gender", SqlDbType.VarChar, 50).Value = gender
            cmd.Parameters.Add("@Civil_status", SqlDbType.VarChar, 50).Value = civilstatus
            cmd.Parameters.Add("@Status", SqlDbType.VarChar, 50).Value = empstatus
            cmd.Parameters.Add("@Department", SqlDbType.VarChar, 50).Value = department
            cmd.Parameters.Add("@Date_Hired", SqlDbType.DateTime).Value = datehired
            cmd.Parameters.Add("@Positions", SqlDbType.VarChar, 50).Value = position
            cmd.Parameters.Add("@Salary_Grade", SqlDbType.VarChar, 50).Value = SG
            cmd.Parameters.Add("@Date_of_Birth", SqlDbType.DateTime).Value = datebirth
            cmd.Parameters.Add("@Place_of_Birth", SqlDbType.VarChar, 255).Value = placebirth
            cmd.Parameters.Add("@Religion", SqlDbType.VarChar, 50).Value = religion
            cmd.Parameters.Add("@Citizenship", SqlDbType.VarChar, 50).Value = citizenship
            cmd.Parameters.Add("@Height", SqlDbType.VarChar, 50).Value = height
            cmd.Parameters.Add("@Address_1", SqlDbType.VarChar, 255).Value = add1
            cmd.Parameters.Add("@Address_2", SqlDbType.VarChar, 255).Value = add2
            cmd.Parameters.Add("@Address_3", SqlDbType.VarChar, 255).Value = add3
            cmd.Parameters.Add("@ATM_No", SqlDbType.VarChar, 50).Value = atm
            cmd.Parameters.Add("@GSIS_No", SqlDbType.VarChar, 50).Value = gsis
            cmd.Parameters.Add("@Pag_ibig_No", SqlDbType.VarChar, 50).Value = pagibig
            cmd.Parameters.Add("@TIN_No", SqlDbType.VarChar, 50).Value = tin
            cmd.Parameters.Add("@Phil_Health_No", SqlDbType.VarChar, 50).Value = philhealth
            cmd.Parameters.Add("@NBI_No", SqlDbType.VarChar, 50).Value = nbi
            cmd.Parameters.Add("@SSS", SqlDbType.VarChar, 50).Value = sss
            cmd.Parameters.Add("@Rate", SqlDbType.VarChar, 50).Value = rate
            cmd.Parameters.Add("@Basic_Pay", SqlDbType.VarChar, 50).Value = basicpay
            cmd.Parameters.Add("@Per_Month", SqlDbType.VarChar, 50).Value = permonth
            cmd.Parameters.Add("@ECOLA", SqlDbType.VarChar, 50).Value = ecola
            cmd.Parameters.Add("@Payroll", SqlDbType.VarChar, 50).Value = payschedule
            cmd.Parameters.Add("@Tax", SqlDbType.VarChar, 50).Value = tax
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show("Check your SP for Delete", "Clicksoftware", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
#End Region
#Region "Employee Personnel Resigned/stored proc"
    Public Sub Resigned_employee()

        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_masterfile_resigned_update", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            cmd.Parameters.Add("@recid", SqlDbType.Char, 30).Value = Me.TXTRECID.Text
            cmd.Parameters.Add("@Resigned", SqlDbType.Bit).Value = 1
            'Label46.Text = "Resigned"
            cmd.Parameters.Add("@date_resigned", SqlDbType.DateTime).Value = Date.Now 'Microsoft.VisualBasic.FormatDateTime(Dateresigned.Value, DateFormat.ShortDate) 'form6.Label48.Text

            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show("There is no employee record's to be resigned", "eHRMS", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub
#End Region
#Region "Employee Personne in Payroll $ TK Resigned"
    Public Sub Employee_resigned_payroll_tk()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_payroll_employee_masterfile_resigned_update", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            cmd.Parameters.Add("@fxKeyEmployee", SqlDbType.Char, 6, ParameterDirection.Input).Value = TXTKEYEMPLOYEEID.Text
            cmd.Parameters.Add("@fbActive", SqlDbType.Bit).Value = 0
            cmd.Parameters.Add("@fdDateSeparated", SqlDbType.DateTime).Value = Date.Now

            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()

        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "Resigned..")
        End Try
    End Sub
#End Region

    Private Sub temp_fname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles temp_fname.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub temp_lname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles temp_lname.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub temp_midname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles temp_midname.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub tnotes_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub temp_placebirth_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles temp_placebirth.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub temp_religion_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtofficeadd.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub temp_add1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtofficenumber.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub empadd2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtlocalofficenumber.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub temp_add3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles temp_add3.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub temp_citizen_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles temp_citizen.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub temp_marks_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles temp_marks.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    'Private Sub temp_fname_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles temp_fname.Validating
    '    If temp_fname.Text = "" Then
    '        MessageBox.Show("Empty field First Name is required", "System Message", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '        temp_fname.Focus()
    '    Else
    '    End If
    'End Sub

    Private Sub temp_lname_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles temp_lname.Validating
        If temp_lname.Text = "" Then
            MessageBox.Show("Empty field Last Name is required", "System Message", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            temp_lname.Focus()
        Else
        End If
    End Sub

    Private Sub temp_midname_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles temp_midname.Validating
        If temp_midname.Text = "" Then
            MessageBox.Show("Empty field Middle Name is required", "System Message", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            temp_midname.Focus()
        Else
        End If
    End Sub

    Private Sub temp_fname_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles temp_fname.TextChanged
        'Me.txtfullname.Text = temp_fname.Text + " " + Me.temp_lname.Text + " " + Me.temp_midname.Text
        Me.txtfullname.Text = Me.temp_lname.Text + "," + temp_fname.Text + " " + Me.temp_midname.Text
        Me.lblemployee_name.Text = Me.temp_fname.Text + " " + Me.temp_midname.Text + " " + Me.temp_lname.Text
    End Sub

    Private Sub temp_lname_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles temp_lname.TextChanged
        'Me.txtfullname.Text = temp_fname.Text + " " + Me.temp_lname.Text + " " + Me.temp_midname.Text
        Me.txtfullname.Text = Me.temp_lname.Text + "," + temp_fname.Text + " " + Me.temp_midname.Text
        Me.lblemployee_name.Text = Me.temp_fname.Text + " " + Me.temp_midname.Text + " " + Me.temp_lname.Text
    End Sub

    Private Sub temp_midname_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles temp_midname.TextChanged
        'Me.txtfullname.Text = temp_fname.Text + " " + Me.temp_lname.Text + " " + Me.temp_midname.Text
        Me.txtfullname.Text = Me.temp_lname.Text + "," + temp_fname.Text + " " + Me.temp_midname.Text
        Me.lblemployee_name.Text = Me.temp_fname.Text + " " + Me.temp_midname.Text + " " + Me.temp_lname.Text
    End Sub

    Private Sub Label41_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Call compute_age()
    End Sub

    Private Sub cbotitledesignation_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cbotitledesignation.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cbotitledesignation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbotitledesignation.SelectedIndexChanged
        Call Employee_position_fxkeyposition()
    End Sub

    Private Sub txtmonthreqular_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtmonthreqular.KeyPress
        e.Handled = Not txtcode_Validate(e.KeyChar)
    End Sub

    Public Function txtcode_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")
            'txtcode.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub cbohours_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not numeric_Validate(e.KeyChar)
    End Sub

    'Private Sub LvlPerformanceEvaluation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Me.tlocation.Text = Me.LvlPerformanceEvaluation.SelectedItems(0).SubItems(4).Text
    '    Catch ex As Exception
    '    End Try
    'End Sub

    'Private Sub LvlDisciplineCase_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Me.txtDiscFileShow.Text = Me.LvlDisciplineCase.SelectedItems(0).SubItems(8).Text
    '    Catch ex As Exception
    '    End Try
    'End Sub

    'Private Sub RdAllTraining_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Me.RdAllTraining.Checked = True Then
    '        Me.RdCurrentTraining.Checked = False
    '        Me.RdPreviousTraining.Checked = False
    '        Call load_employee_training_with_selection()
    '    End If
    'End Sub
    'Private Sub RdCurrentTraining_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Me.RdCurrentTraining.Checked = True Then
    '        Me.RdPreviousTraining.Checked = False
    '        Me.RdAllTraining.Checked = False
    '        Call load_employee_training_with_selection1()
    '    End If
    ''End Sub
    'Private Sub RdPreviousTraining_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Me.RdPreviousTraining.Checked = True Then
    '        Me.RdAllTraining.Checked = False
    '        Me.RdCurrentTraining.Checked = False
    '        Call load_employee_training_with_selection2()
    '    End If
    'End Sub

    'Private Sub lvlTraining_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Me.txtFileShow.Text = Me.lvlTraining.SelectedItems(0).SubItems(7).Text
    '    Catch ex As Exception
    '    End Try
    'End Sub



    'Private Sub BtnDel_jobdesc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        frmEmployee_JobDescription.getempJobDescID() = Me.LvlJobDescription.SelectedItems(0).SubItems(4).Text
    '        Dim x As New DialogResult
    '        x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
    '        If x = System.Windows.Forms.DialogResult.OK Then
    '            Call Job_Description_Delele()
    '            Call frmEmployee_JobDescription.Job_Description_Get()
    '        ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
    '        End If
    '    Catch ex As Exception
    '    End Try
    'End Sub
#Region "Employee Job Description Delete/stored proc"
    Private Sub Job_Description_Delele()
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_employee_jobdescription_delete", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@key_id", SqlDbType.VarChar, 10).Value = frmEmployee_JobDescription.getjobdesc_id
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
        End Try
    End Sub
#End Region

    'Private Sub BtnEdit_jobDesc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        With frmEmployee_JobDescription
    '            .Label1.Text = "Edit Job Description"
    '            .txtReportingto.Text = Me.LvlJobDescription.SelectedItems(0).Text
    '            .Show()
    '            .btnsave.Visible = False
    '            .btnupdate.Visible = True
    '            .btnupdate.Location = New System.Drawing.Point(5, 4)
    '            .txtLocation.Text = Me.LvlJobDescription.SelectedItems(0).SubItems(1).Text
    '            .txtJobDescription.Text = Me.LvlJobDescription.SelectedItems(0).SubItems(2).Text
    '            .getempJobDescID() = Me.LvlJobDescription.SelectedItems(0).SubItems(4).Text
    '        End With
    '    Catch ex As Exception
    '    End Try
    'End Sub

    Private Sub BtnAdd_Skills_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmEmployee_Skills.Label1.Text = "Add Skills"
        frmEmployee_Skills.btnupdate.Visible = False
        frmEmployee_Skills.Show()
    End Sub
    'Private Sub BtnEdit_Skills_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        With frmEmployee_Skills
    '            .Label1.Text = "Edit Skills"
    '            .txtSkills.Text = Me.LvlSkills.SelectedItems(0).Text
    '            .Show()
    '            .btnsave.Visible = False
    '            .btnupdate.Visible = True
    '            .btnupdate.Location = New System.Drawing.Point(5, 5)
    '            .txtRemark.Text = Me.LvlSkills.SelectedItems(0).SubItems(1).Text
    '            .getempSkillsID = Me.LvlSkills.SelectedItems(0).SubItems(3).Text 'key_id
    '        End With
    '    Catch ex As Exception
    '    End Try
    'End Sub

    'Private Sub BtnDel_Skills_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        frmEmployee_Skills.getempSkillsID = Me.LvlSkills.SelectedItems(0).SubItems(3).Text
    '        Dim x As New DialogResult
    '        x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
    '        If x = System.Windows.Forms.DialogResult.OK Then
    '            Call Employee_Skills_Delete()
    '            Call frmEmployee_Skills.Employee_Skills_Get()
    '        ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
    '        End If
    '    Catch ex As Exception
    '    End Try
    'End Sub
#Region "Employee Skills Delete/stored proc"
    Private Sub Employee_Skills_Delete()
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_employee_skills_delete", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@key_id", SqlDbType.VarChar, 10).Value = frmEmployee_Skills.getskills_id
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
        End Try
    End Sub
#End Region

    Private Sub BtnAdd_Awards_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmEmployee_Awards.Label1.Text = "Add Awards"
        frmEmployee_Awards.btnupdate.Visible = False
        frmEmployee_Awards.Show()
    End Sub

#Region "Employee Awards Delete/stored proc"
    Private Sub Employee_award_delete()
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_employee_award_delete", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@key_id", SqlDbType.VarChar, 10).Value = frmEmployee_Awards.getaward_ID
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
        End Try
    End Sub
#End Region

    Private Sub BtnAdd_Medical_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmEmployee_Medical.Label1.Text = "Add Medical"
        frmEmployee_Medical.btnupdate.Visible = False
        frmEmployee_Medical.Show()
    End Sub

#Region "Employee Medical Delete/stored proc"
    Private Sub Employee_medical_delete()
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_employee_medical_delete", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@key_id", SqlDbType.VarChar, 10).Value = frmEmployee_Medical.getmedical_ID
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
        End Try
    End Sub
#End Region
#Region "Payroll Loan History get/stored proc"
    Public Sub Payroll_loan_history_get()
        'Try
        '    Dim myconnection As New Clsappconfiguration
        '    Dim cmd As New SqlCommand("usp_payroll_loan_history_get", myconnection.sqlconn)
        '    cmd.CommandType = CommandType.StoredProcedure
        '    myconnection.sqlconn.Open()
        '    cmd.Parameters.Add("@fxKeyEmployee", SqlDbType.VarChar, 7).Value = Me.TXTKEYEMPLOYEEID.Text
        '    With Me.LvlLoanHistory
        '        .Clear()
        '        .View = View.Details
        '        .GridLines = True
        '        .FullRowSelect = True
        '        .Columns.Add("Loan Reference", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Date of Loan", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Starting Loan", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Ending Loan", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Approved By", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Remarks", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Payment", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Loan Balance", 100, HorizontalAlignment.Left)
        '        Dim myreader As SqlDataReader
        '        myreader = cmd.ExecuteReader
        '        Try
        '            While myreader.Read
        '                With .Items.Add(myreader.Item(0))
        '                    .Subitems.add(myreader.Item(1))
        '                    .Subitems.add(myreader.Item(2))
        '                    .Subitems.add(myreader.Item(3))
        '                    .Subitems.add(myreader.Item(4))
        '                    .Subitems.add(myreader.Item(5))
        '                    .Subitems.add(myreader.Item(6))
        '                    .Subitems.add(myreader.Item(7))
        '                    .Subitems.add(myreader.Item(8))
        '                    .Subitems.add(myreader.Item(9))
        '                End With
        '            End While

        '        Finally
        '            myreader.Close()
        '            myconnection.sqlconn.Close()
        '        End Try
        '    End With
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, "Payroll loan history", MessageBoxButtons.OK)
        'End Try
    End Sub
#End Region
#Region "Payroll History get/stored proc"
    Public Sub Payroll_history_get()
        'Try
        '    Dim myconnection As New Clsappconfiguration
        '    Dim cmd As New SqlCommand("spu_EMPLOYEEFile_PayrollHistory", myconnection.sqlconn)
        '    cmd.CommandType = CommandType.StoredProcedure
        '    myconnection.sqlconn.Open()
        '    cmd.Parameters.Add("@fxKeyEmployee", SqlDbType.VarChar, 7).Value = Me.TXTKEYEMPLOYEEID.Text
        '    With Me.LvlPayrollHistory
        '        .Clear()
        '        .View = View.Details
        '        .GridLines = True
        '        .FullRowSelect = True
        '        .Columns.Add("Cut Off Date", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Basic Pay", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Gross Pay", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Deduction", 100, HorizontalAlignment.Left)
        '        .Columns.Add("Net Pay", 100, HorizontalAlignment.Left)
        '        Dim myreader As SqlDataReader
        '        myreader = cmd.ExecuteReader
        '        Try
        '            While myreader.Read
        '                With .Items.Add(myreader.Item(0))
        '                    .Subitems.add(myreader.Item(1))
        '                    .Subitems.add(myreader.Item(2))
        '                    .Subitems.add(myreader.Item(3))
        '                    .Subitems.add(myreader.Item(4))
        '                    .Subitems.add(myreader.Item(5))
        '                End With
        '            End While

        '        Finally
        '            myreader.Close()
        '            myconnection.sqlconn.Close()
        '        End Try
        '    End With
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, "Payroll history", MessageBoxButtons.OK)
        'End Try
    End Sub
#End Region
    Private Sub TXTKEYEMPLOYEEID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TXTKEYEMPLOYEEID.TextChanged
        If Me.TXTKEYEMPLOYEEID.Text = Me.TXTKEYEMPLOYEEID.Text Then
            'Call Timekeeping_leaves_ledger_get()
            Call Payroll_loan_history_get()
            Call Payroll_history_get()
        End If
    End Sub

    Private Sub PcSeconded_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim orgchart As New frmOrgChart_Master(orgchart.selection.Org_Emp)
        orgchart.Width = 257
        orgchart.btnClose.Location = New System.Drawing.Point(170, 12)
        orgchart.btnAdd.Enabled = False
        orgchart.btnPrint.Visible = False
        orgchart.btnPrint.Visible = False
    End Sub


    Private Sub txtpagibigcontri_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not CurrencyInput_Validate(e.KeyChar)
    End Sub

#Region "Get Orgchart from the root to node"
    Public Sub getorgchartvalue(ByVal m As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_orgchart_get", myconnection.sqlconn)
        With cmd.Parameters
            .Add("@getid", SqlDbType.VarChar, 10).Value = m
        End With
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Dim x As String
        Try
            While myreader.Read
                Me.txtorgchart.Text = myreader.Item(0)
                x = myreader.Item(0)
            End While
            myreader.Close()
            'Me.txtdescdivision.Text = Mid(x, 2, 32)
            'Me.txtdescdepartment.Text = Mid(x, 35, 24)
            'Me.txtdescsection.Text = Mid(x, 60, 5)
        Finally
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "GetdepartmentID"
    Public Sub getorgchartIDtodepartment2(ByVal orgid As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_department2_select", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@department", SqlDbType.VarChar, 20).Value = orgid
        End With
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            Me.txtDepartment2.Text = ""
            While myreader.Read
                Me.txtDepartment2.Text = myreader.Item(0)
            End While
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Get orgcode"
    Public Sub getorgcode(ByVal m As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_orgchart_get_code", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@getid", SqlDbType.VarChar, 10).Value = m
        End With
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader

        Dim x1 As String

        Dim split As New Collection
        Try
            While myreader.Read
                x1 = myreader.Item(0)

            End While
            myreader.Close()
            Me.txtkeycompany.Text = Mid(x1, 2, 2)
            Me.txtkeydivision.Text = Mid(x1, 5, 2)
            Me.txtkeydepartment.Text = Mid(x1, 8, 2)
            Me.txtkeysection.Text = Mid(x1, 11, 2)
        Finally
            'myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Get parent id from orgchart"
    Public Sub getorgparentid(ByVal m As String)

        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_orgchart_get_parentid", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@getid", SqlDbType.VarChar, 10).Value = m
        End With
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader

        Dim x1 As String

        Dim split As New Collection
        Try
            While myreader.Read
                x1 = myreader.Item(0)
            End While
            myreader.Close()

            Me.txtdescdivision.Text = Mid(x1, 5, 2)

        Finally
            'myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Proposed dept/division"
    Public Sub proposeddeptdivision(ByVal m As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_orgchart_getid", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@getid", SqlDbType.VarChar, 10).Value = m
        End With
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Dim x As String

        Try
            While myreader.Read
                x = myreader.Item(0)
            End While
            myreader.Close()

            Dim arr As String() = x.Split("\".ToCharArray())
            For i As Integer = 0 To (arr.Length - 1)
                'Me.txtkeycompany.Text = arr(2)
                'Me.txtdescdivision.Text = arr(3)
                'Me.txtdescdepartment.Text = arr(4)
                'Me.txtdescsection.Text = arr(5)
            Next

        Finally
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "SpacedelimeterFunction"
    Public Function SplitDelimitedLine(ByVal CurrentLine As String, ByVal Delimiter As String, ByVal Qualifier As String) As Collection

        Dim i As Integer
        Dim SplitString As New Collection
        Dim CountDelimiter As Boolean
        Dim Total As Integer
        Dim Ch As Char
        Dim Section As String

        ' We want to count the delimiter unless it is within the text qualifier
        CountDelimiter = True
        Total = 0
        Section = ""

        For i = 1 To Len(CurrentLine)

            Ch = Mid(CurrentLine, i, 1)
            Select Case Ch

                Case Qualifier
                    If CountDelimiter Then
                        CountDelimiter = False
                    Else
                        CountDelimiter = True
                    End If

                Case Delimiter
                    If CountDelimiter Then

                        ' Add current section to collection
                        SplitString.Add(Section)

                        Section = ""
                        Total = Total + 1

                    End If

                Case Else

                    Section = Section & Ch

            End Select


        Next

        ' Get the last field - as most files will not have an ending delimiter
        If CountDelimiter Then

            ' Add current section to collection
            SplitString.Add(Section)

        End If


        SplitDelimitedLine = SplitString


    End Function


#End Region

#Region "Update Education for Change IDnumber"
    Public Sub updateeducationEmployee(ByVal idnumber As String)

        Dim count As Integer = 0
        Dim counter As Integer = 0
        Dim myconnection As New Clsappconfiguration
        Dim myreader As SqlDataReader
        Dim cmd1 As New SqlCommand
        cmd1.CommandText = "select * from Per_Employee_Education where idnumber=" & "'" & idnumber & "'"
        cmd1.CommandType = CommandType.Text
        cmd1.Connection = myconnection.sqlconn
        myconnection.sqlconn.Open()

        myreader = cmd1.ExecuteReader
        With myreader
            If .HasRows Then
                While .Read
                    oldid(count) = .Item("idnumber")
                    count += 1
                    counter += 1
                End While
            End If
        End With
        myreader.Close()

        cmd1.Connection = myconnection.sqlconn
        cmd1.CommandText = "usp_per_education_new_id_update"
        cmd1.CommandType = CommandType.StoredProcedure

        For count = 0 To counter - 1
            With cmd1.Parameters
                .Clear()
                .Add("@idnumber", SqlDbType.VarChar, 50).Value = oldid(count)

            End With
            myreader = cmd1.ExecuteReader
            myreader.Close()
        Next
        myconnection.sqlconn.Close()

        'Dim cmd As New SqlCommand("usp_per_education_new_id_update", myconnection.sqlconn)
        'cmd.CommandType = CommandType.StoredProcedure
        'myconnection.sqlconn.Open()
        'cmd.Parameters.Add("@idnumber", SqlDbType.VarChar, 50).Value = idnumber



        'Try
        '    While myreader.Read
        '        idnumber = myreader.Item(0)
        '    End While
        '    myreader.Close()
        'Finally
        '    myconnection.sqlconn.Close()
        'End Try




    End Sub
#End Region

    Private Sub cborate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cborate.TextChanged
        Call rateToDescription()
    End Sub

    Private Sub txtorgchart_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtorgchart.TextChanged

    End Sub
#Region "Delete Emergency Contact"
    Private Sub deleteEmergencyContact()
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_employee_EmergencyContact_Delete", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@fxkeyemployee", SqlDbType.Char, 10).Value = emergencykey
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show("usp_per_employee_EmergencyContact_Delete", "Check this stored procedure", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
#End Region

    Private Sub txtfullname_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtfullname.TextChanged
        Me.txtfullname.Text = Me.temp_lname.Text + "," + Me.temp_fname.Text + " " + Me.temp_midname.Text
    End Sub

    Private Sub mem_amountpaid_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not CurrencyInput_Validate(e.KeyChar)
    End Sub

    Private Sub txtpayrollcontriamount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtpayrollcontriamount.KeyPress
        e.Handled = Not CurrencyInput_Validate(e.KeyChar)
    End Sub

    Private Sub mem_baccount1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not CurrencyInput_Validate(e.KeyChar)
    End Sub

    Private Sub mem_baccount2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not CurrencyInput_Validate(e.KeyChar)
    End Sub

    Private Sub mem_baccount3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not CurrencyInput_Validate(e.KeyChar)
    End Sub

    Private Sub mem_WithdrawDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mem_WithdrawDate.ValueChanged
        Me.txtwithdrawal.Text = Me.mem_WithdrawDate.Text
    End Sub

    Private Sub txtEmpID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEmployeeNo.TextChanged
        If cboemp_status.Text = "PROJECT BASE" Then
            Me.rbClient.Checked = True
        ElseIf cboemp_status.Text = "INTERNAL EMPLOYEE" Then
            Me.rbEmployee.Checked = True
        End If
        If btnemp_add.Text = "New" Then
            If Me.txtEmployeeNo.Text = "" Then
                Call DisableHRISButton()
            Else
                Call LoadGroup()
                Call LoadMembersInfo(Me.txtEmployeeNo.Text)
                Call LoadPicture_Signature(Me.txtEmployeeNo.Text)
                Call LoanRankofEmployee(Me.txtEmployeeNo.Text)
                Call EnableHRISButton()

                Call GetNearestRelatives(Me.txtEmployeeNo.Text)
                Call GetmemberDependents(Me.txtEmployeeNo.Text)
                Call GetmemberBankInfo(Me.txtEmployeeNo.Text)
                Call GetsourceofIncomce(Me.txtEmployeeNo.Text)
                Call GetmemberEducationalInfo(Me.txtEmployeeNo.Text)
                Call GetmemberJobDescription(Me.txtEmployeeNo.Text)
                Call GetmemberSkills(Me.txtEmployeeNo.Text)
                Call GetmemberAwards(Me.txtEmployeeNo.Text)
                Call GetmemberMedicals(Me.txtEmployeeNo.Text)
                'Call GetmemberLoanHistory(Me.txtEmployeeNo.Text)
                Call GetmemberLeave(Me.txtEmployeeNo.Text, Me.cboLeave.Text)
                Call GetmemberTrainings(Me.txtEmployeeNo.Text)
                Call GetmemberDiscipline(Me.txtEmployeeNo.Text)
                Call GetmemberPerfEvaluation(Me.txtEmployeeNo.Text)
                'Call LoadEmloymentHistory(Me.txtEmployeeNo.Text, Me.cboEmpHistory.Text)
                Call GetmemberGovExam(Me.txtEmployeeNo.Text)
                Call GetmemberEmployment(Me.txtEmployeeNo.Text)
                Call GetmemberLegalCase(Me.txtEmployeeNo.Text)
                Call GetmemberDecutions(Me.txtEmployeeNo.Text)
                Call GetmemberMonthPay(Me.txtEmployeeNo.Text)
                Call GetmemberAttachReq(Me.txtEmployeeNo.Text)
                Call GetmemberFamilySibling(Me.txtEmployeeNo.Text)
                Call getEmploymentStatus()

                'If MemberMngr = "PROJECT BASE" Then
                '    Me.rbClient.Checked = True
                '    txtContractPrice.Text = ""
                '    txtEcola.Text = ""
                '    Call GetmemberContractHistory(Me.txtEmployeeNo.Text)
                'ElseIf MemberMngr = "INTERNAL EMPLOYEE" Then
                '    Me.rbEmployee.Checked = True
                '    txtContractPrice.Text = ""
                '    txtEcola.Text = ""
                '    txtContractrate.Text = ""
                Call GetmemberContractHistory(Me.txtEmployeeNo.Text)
                'End If

                'load history info
                Call GetLastnameHistory(Me.txtEmployeeNo.Text)
                Call GetAddressHistory(Me.txtEmployeeNo.Text)
                Call GetCivilHistory(Me.txtEmployeeNo.Text)
                Call GEtPhone(Me.txtEmployeeNo.Text)
                Call GetEmail(Me.txtEmployeeNo.Text)
                Call GetmemberEmpOrg(Me.txtEmployeeNo.Text)

                Call Load_TIN_SSS_PH_HDMF()
            End If
        Else
            Exit Sub
        End If
    End Sub

    Private Sub Load_TIN_SSS_PH_HDMF() 'Added by Vincent Nacar 7/18/14
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(gCon.cnstring, "_Select_TIN_SSS_PH_HDMF",
                                           New SqlParameter("@fcEmployeeNo", txtEmployeeNo.Text))
            While rd.Read
                txtTin.Text = rd(0)
                txtSSS.Text = rd(1)
                txtPhilhealth.Text = rd(2)
                txtHDMF.Text = rd(3)

                If rd(4) Is DBNull.Value Then
                    dtBoardApproval.Value = Date.Now
                Else
                    dtBoardApproval.Value = rd(4)
                End If
            End While
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub LoanRankofEmployee(ByVal empno As String)
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(gCon.cnstring, "_Select_RankofEmployee",
                                          New SqlParameter("@empNo", empno))
            While rd.Read
                cboRank.Text = rd("fcRank")
            End While
        Catch ex As Exception
            '    MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub GetPk_Employee(ByVal empno As String)
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gCon.cnstring, "_Select_pkEmployee_For_Editing",
                                      New SqlParameter("@fcEmployeeNo", empno))
        While rd.Read
            entengfxkey = rd("fxkey").ToString
        End While
    End Sub

    Private Sub GetEmail(ByVal empno As String)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(gCon.cnstring, "CIMS_MemberHistoryInfo_Select_Email",
                                       New SqlParameter("@employeeno", empno))
        dgvemail.DataSource = ds.Tables(0)
        dgvemail.Columns(0).HeaderText = "Email Address"
    End Sub

    Private Sub GEtPhone(ByVal empno As String)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(gCon.cnstring, "CIMS_MemberHistoryInfo_Select_Phone",
                                       New SqlParameter("@employeeno", empno))
        dgvphone.DataSource = ds.Tables(0)
        dgvphone.Columns(0).HeaderText = "Residence Phone"
    End Sub
    Private Sub GetCivilHistory(ByVal empno As String)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(gCon.cnstring, "CIMS_MemberHistoryInfo_Select_Civil",
                                       New SqlParameter("@employeeno", empno))
        dgvcivil.DataSource = ds.Tables(0)
        dgvcivil.Columns(0).HeaderText = "Civil Status"
    End Sub

    Private Sub GetAddressHistory(ByVal empno As String)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(gCon.cnstring, "CIMS_AddressHistory_Select",
                                       New SqlParameter("@employeeno", empno))
        dgvaddress.DataSource = ds.Tables(0)
    End Sub

    Private Sub GetLastnameHistory(ByVal empno As String)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(gCon.cnstring, "CIMS_LastnameHistory_Select",
                                       New SqlParameter("@employeeno", empno))
        dgvlastname.DataSource = ds.Tables(0)
    End Sub

    Public Sub LoadPicture_Signature(ByVal empno As String)
        Try
            Dim rd As SqlDataReader
            Dim myconnection As New Clsappconfiguration
            'kelangan pag wla image ung member magdidisplay ung default image

            rd = SqlHelper.ExecuteReader(myconnection.cnstring, "CIMS_Member_ShowPictureSignature",
                               New SqlParameter("@employeeno", empno))
            While rd.Read
                If rd.Item("Fb_Picture") Is DBNull.Value Then
                    picEmpPhoto.Image = My.Resources.photo
                Else
                    Picdata = rd.Item("Fb_Picture")
                End If
                If rd.Item("Fb_Signature") Is DBNull.Value Then
                    picEmpSignature.Image = My.Resources.signature
                Else
                    SigData = rd.Item("Fb_Signature")
                End If
            End While

            PicData2 = Picdata
            SigData2 = SigData

            'load picture
            Dim ms As New MemoryStream(Picdata, 0, Picdata.Length)
            ms.Write(Picdata, 0, Picdata.Length)
            img = Image.FromStream(ms, True)
            picEmpPhoto.Image = img
            picEmpPhoto.SizeMode = PictureBoxSizeMode.StretchImage

            'load signature
            Dim ms0 As New MemoryStream(SigData, 0, SigData.Length)
            ms0.Write(SigData, 0, SigData.Length)
            img = Image.FromStream(ms0, True)
            picEmpSignature.Image = img
            picEmpSignature.SizeMode = PictureBoxSizeMode.StretchImage
        Catch
            'Picdata = My.Resources.photo
            'SigData = Nothing
            'img = Nothing
        End Try
    End Sub

    Private Sub LoadMembersInfo(ByVal empno As String)
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Select_RegisteredMembers", _
                                     New SqlParameter("@employeeNo", empno))
        Try

            While rd.Read
                With Me
                    Select Case (rd.Item(0))
                        Case True
                            .chkyes.Checked = True
                        Case False
                            .chkNo.Checked = False
                        Case Else
                    End Select

                    '.txtMemberID.Text = rd.Item(2).Value

                    'If rd.Item(5).Value = "" And rd.Item(6).Value = "" Then
                    '    .temp_lname.Clear()
                    '    .txtCompanyName.Text = rd.Item(4).Value
                    'Else
                    '    .temp_lname.Text = rd.Item(4).Value
                    '    .txtCompanyName.Text = ""
                    'End If
                    .temp_lname.Text = rd.Item(4)
                    .temp_fname.Text = rd.Item(5)
                    .temp_midname.Text = rd.Item(6)
                    .cboemp_gender.Text = rd.Item(7)
                    .cboemp_civil.Text = rd.Item(8)
                    .EMP_DATEbirth.Text = rd.Item(9)
                    .temp_placebirth.Text = rd.Item(10)
                    '.txtPresBldg.Text = rd.Item(11).Value
                    '.txtPrevAdd.Text = rd.Item(12).Value
                    .txtLandline.Text = rd.Item(13)
                    .txtresidencephone.Text = rd.Item(14)
                    .txtEmailAddress.Text = rd.Item(15)
                    .cbotitledesignation.Text = rd.Item(16)
                    .cborate.Text = rd.Item(17)
                    .txtofficenumber.Text = rd.Item(18)
                    .txtlocalofficenumber.Text = rd.Item(19)
                    .emp_Datehired.Text = rd.Item(20)
                    .txtbasicpay.Text = rd.Item(21)
                    .mem_MemberDate.Text = rd.Item(22)
                    .txtwithdrawal.Text = rd.Item(23)

                    Select Case (rd.Item(24))
                        Case True
                            .chkBereaveYes.Checked = True
                        Case False
                            .chkBereaveNo.Checked = False
                        Case Else
                    End Select

                    '.cboemp_status.Text = rd.Item(27).Value
                    .Cbomem_Status.Text = rd.Item(27)

                    Dim x As Double = rd.Item(29)
                    Dim y As Double = rd.Item(30)
                    .emp_Ytenures.Text = CStr(x) + " yr's" + " and " + CStr(y) + " m"

                    .GetNearestRelatives(rd.Item(1))
                    .GetmemberDependents(rd.Item(1))
                    .GetmemberBankInfo(rd.Item(1))
                    .GetsourceofIncomce(rd.Item(1))
                    .GetmemberEducationalInfo(rd.Item(1))
                    .GetmemberJobDescription(rd.Item(1))
                    .GetmemberSkills(rd.Item(1))
                    .GetmemberAwards(rd.Item(1))
                    .GetmemberMedicals(rd.Item(1))
                    '.GetmemberLoanHistory(rd.Item(1).Value)
                    '.GetmemberLeave(rd.Item(1).Value, .cboLeave.Text)
                    .GetmemberTrainings(rd.Item(1))
                    .GetmemberDiscipline(rd.Item(1))
                    .GetmemberPerfEvaluation(rd.Item(1))

                    .txtEmployeeNo.Text = rd.Item(1)
                    .cbotaxcode.Text = rd.Item(31)
                    .emp_company.Text = rd.Item(32)
                    .txtofficeadd.Text = rd.Item(33)
                    .txtpayrollcontriamount.Text = rd.Item(34)
                    .txtorgchart.Tag = rd.Item(35)
                    .cboPaycode.Text = rd.Item(36)
                    .cbopayroll.Text = rd.Item(37)
                    .mem_PaycontDate.Text = rd.Item(38)
                    .cboemp_status.Text = rd.Item(40)
                    .cboEmp_type.Text = rd.Item(41)
                    .txtContractrate.Text = rd.Item(42)
                    .txtNickname.Text = rd.Item(44)
                    .txtHeightFt.Text = rd.Item(45)
                    .txtHeightInch.Text = rd.Item(46)
                    .txtW.Text = rd.Item(47)
                    .txtLandline2.Text = rd.Item(48)
                    .txtresidencephone2.Text = rd.Item(49)
                    .txtemailaddress2.Text = rd.Item(50)
                    .txtFacebook1.Text = rd.Item(51)
                    .txtFacebook2.Text = rd.Item(52)
                    '.txtPermanentAdd.Text = rd.Item(53).Value
                    .txtCommonrefNo.Text = rd.Item(54)
                    '.txtZipCode1.Text = rd.Item(55).Value
                    '.txtZipcode2.Text = rd.Item(56).Value
                    '.txtZipCode3.Text = rd.Item(57).Value
                    .cboStay1.Text = rd.Item(58)
                    .cboStay2.Text = rd.Item(59)
                    .cboStay3.Text = rd.Item(60)
                    .txtStay2.Text = rd.Item(61)
                    .txtStay3.Text = rd.Item(62)
                    .txtStay1.Text = rd.Item(63)
                    .txtFatherName.Text = rd.Item(64)
                    .txtFAdd.Text = rd.Item(65)
                    .txtFContact.Text = rd.Item(66)
                    .txtMotherName.Text = rd.Item(67)
                    .txtMAdd.Text = rd.Item(68)
                    .txtMContact.Text = rd.Item(69)
                    .txtSpouseName.Text = rd.Item(70)
                    .txtSpouseAdd.Text = rd.Item(71)
                    .txtSpouseContact.Text = rd.Item(72)
                    .txtOccupation.Text = rd.Item(73)
                    .txtSpouseStay.Text = rd.Item(75)

                    Select Case (rd.Item(76))
                        Case "Month"
                            .rbSpouseMonth.Checked = True
                        Case "Year"
                            .rbSpouseYears.Checked = True
                    End Select

                    .txtChildNo.Text = rd.Item(77)

                    Select Case (rd.Item(78))
                        Case "Owned"
                            .rbOwned.Checked = True
                        Case "Rented"
                            .rbRented.Checked = True
                        Case "Living with Parents"
                            .rbParents.Checked = True
                        Case Else
                            .rbParents.Checked = False
                            .rbRented.Checked = False
                            .rbOwned.Checked = False
                    End Select

                    Select Case (rd.Item(79))
                        Case True
                            .rbMYes.Checked = True
                        Case False
                            .rbMNo.Checked = True
                        Case Else
                            .rbMNo.Checked = False
                            .rbMYes.Checked = False
                    End Select

                    Select Case (rd.Item(80))
                        Case True
                            .rbCYes.Checked = True
                        Case False
                            .rbCNo.Checked = True
                        Case Else
                            .rbCYes.Checked = False
                            .rbCNo.Checked = False
                    End Select

                    .txtMotor.Text = rd.Item(81)
                    .txtCar.Text = rd.Item(82)
                    .cboOutlet.Text = rd.Item(83)

                    .txtPresBldg.Text = rd.Item(84)
                    .txtPresStreet.Text = rd.Item(85)
                    .txtPresSitio.Text = rd.Item(86)
                    .txtPresCity.Text = rd.Item(87)
                    .txtPresProvince.Text = rd.Item(88)
                    .txtPrevBldg.Text = rd.Item(89)
                    .txtPrevStreet.Text = rd.Item(90)
                    .txtPrevSitio.Text = rd.Item(91)
                    .txtPrevCity.Text = rd.Item(92)
                    .txtPrevProvince.Text = rd.Item(93)
                    .txtPermBldg.Text = rd.Item(94)
                    .txtPermStreet.Text = rd.Item(95)
                    .txtPermSitio.Text = rd.Item(96)
                    .txtPermCity.Text = rd.Item(97)
                    .txtPermProvince.Text = rd.Item(98)
                    .txtSuffix.Text = rd.Item(99)
                    .txtContractPrice.Text = rd.Item(101)
                    .txtEcola.Text = rd.Item(102)
                    .cboContactPerson.Text = rd.Item(103)
                    .txtProject.Text = rd.Item(104)

                    'If frmMember_Master.txtCompanyName.Text <> "" Or .temp_fname.Text = "" And .temp_midname.Text = "" Then
                    '    frmMember_Master.lblemployee_name.Text = frmMember_Master.txtCompanyName.Text
                    'Else
                    '    frmMember_Master.lblemployee_name.Text = My.Forms.frmMember_Master.temp_fname.Text + " " + My.Forms.frmMember_Master.temp_midname.Text + " " + My.Forms.frmMember_Master.temp_lname.Text
                    'End If
                    .LoadEloadingRegistrationDetails(.txtEmployeeNo.Text)

                    If rd.Item(39) = True Then
                        .rdoExempt.Checked = True
                        .rdoNonExempt.Checked = False
                    Else
                        .rdoExempt.Checked = False
                        .rdoNonExempt.Checked = True
                    End If
                    'End If
                End With
            End While
            Call getorgchartvalue(Me.txtorgchart.Tag)
            Call getorgchartIDtodepartment2(Me.txtorgchart.Tag)
            rd.Close()
            If temp_fname.Text = "" And temp_midname.Text = "" Then
                temp_lname.Text = ""
            End If
        Catch ex As Exception
            'REDO
            'MessageBox.Show(ex.Message, "PlotRecord")
        End Try
    End Sub

    Private Sub txtpk_Employee_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtpk_Employee.TextChanged
        If Me.txtpk_Employee.Text <> "" Then
            Call PlotRecord(Me.txtpk_Employee.Text.Trim)
        End If
    End Sub
#Region "Plot record base on pk_employee"
    Public Sub PlotRecord(ByVal pkemployee As String)
        Dim gcon As New Clsappconfiguration

        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "MSS_MembersInfo_GetMasterInfo_Navigation", _
                                     New SqlParameter("@employeeNo", pkemployee))
            Try

                While rd.Read
                    With Me
                        Select Case (rd.Item(0).Value)
                            Case True
                                .chkyes.Checked = True
                            Case False
                                .chkNo.Checked = False
                            Case Else
                        End Select

                        '.txtMemberID.Text = rd.Item(2).Value

                        'If rd.Item(5).Value = "" And rd.Item(6).Value = "" Then
                        '    .temp_lname.Clear()
                        '    .txtCompanyName.Text = rd.Item(4).Value
                        'Else
                        '    .temp_lname.Text = rd.Item(4).Value
                        '    .txtCompanyName.Text = ""
                        'End If

                        .temp_fname.Text = rd.Item(5).Value
                        .temp_midname.Text = rd.Item(6).Value
                        .cboemp_gender.Text = rd.Item(7).Value
                        .cboemp_civil.Text = rd.Item(8).Value
                        .EMP_DATEbirth.Text = rd.Item(9).Value
                        .temp_placebirth.Text = rd.Item(10).Value
                        '.txtPresBldg.Text = rd.Item(11).Value
                        '.txtPrevAdd.Text = rd.Item(12).Value
                        .txtLandline.Text = rd.Item(13).Value
                        .txtresidencephone.Text = rd.Item(14).Value
                        .txtEmailAddress.Text = rd.Item(15).Value
                        .cbotitledesignation.Text = rd.Item(16).Value
                        .cborate.Text = rd.Item(17).Value
                        .txtofficenumber.Text = rd.Item(18).Value
                        .txtlocalofficenumber.Text = rd.Item(19).Value
                        .emp_Datehired.Text = rd.Item(20).Value
                        .txtbasicpay.Text = rd.Item(21).Value
                        .mem_MemberDate.Text = rd.Item(22).Value
                        .txtwithdrawal.Text = rd.Item(23).Value

                        Select Case (rd.Item(24).Value)
                            Case True
                                .chkBereaveYes.Checked = True
                            Case False
                                .chkBereaveNo.Checked = False
                            Case Else
                        End Select

                        '.cboemp_status.Text = rd.Item(27).Value
                        .Cbomem_Status.Text = rd.Item(27).Value

                        Dim x As Double = rd.Item(29).Value
                        Dim y As Double = rd.Item(30).Value
                        .emp_Ytenures.Text = CStr(x) + " yr's" + " and " + CStr(y) + " m"

                        .GetNearestRelatives(rd.Item(1).Value)
                        .GetmemberDependents(rd.Item(1).Value)
                        .GetmemberBankInfo(rd.Item(1).Value)
                        .GetsourceofIncomce(rd.Item(1).Value)
                        .GetmemberEducationalInfo(rd.Item(1).Value)
                        .GetmemberJobDescription(rd.Item(1).Value)
                        .GetmemberSkills(rd.Item(1).Value)
                        .GetmemberAwards(rd.Item(1).Value)
                        .GetmemberMedicals(rd.Item(1).Value)
                        '.GetmemberLoanHistory(rd.Item(1).Value)
                        '.GetmemberLeave(rd.Item(1).Value, .cboLeave.Text)
                        .GetmemberTrainings(rd.Item(1).Value)
                        .GetmemberDiscipline(rd.Item(1).Value)
                        .GetmemberPerfEvaluation(rd.Item(1).Value)

                        .txtEmployeeNo.Text = rd.Item(1).Value
                        .cbotaxcode.Text = rd.Item(31).Value
                        .emp_company.Text = rd.Item(32).Value
                        .txtofficeadd.Text = rd.Item(33).Value
                        .txtpayrollcontriamount.Text = rd.Item(34).Value
                        .txtorgchart.Tag = rd.Item(35).Value
                        .cboPaycode.Text = rd.Item(36).Value
                        .cbopayroll.Text = rd.Item(37).Value
                        .mem_PaycontDate.Text = rd.Item(38).Value
                        .cboemp_status.Text = rd.Item(40).Value
                        .cboEmp_type.Text = rd.Item(41).Value
                        .txtContractrate.Text = rd.Item(42).Value
                        .txtNickname.Text = rd.Item(44).Value
                        .txtHeightFt.Text = rd.Item(45).Value
                        .txtHeightInch.Text = rd.Item(46).Value
                        .txtW.Text = rd.Item(47).Value
                        .txtLandline2.Text = rd.Item(48).Value
                        .txtresidencephone2.Text = rd.Item(49).Value
                        .txtemailaddress2.Text = rd.Item(50).Value
                        .txtFacebook1.Text = rd.Item(51).Value
                        .txtFacebook2.Text = rd.Item(52).Value
                        '.txtPermanentAdd.Text = rd.Item(53).Value
                        .txtCommonrefNo.Text = rd.Item(54).Value
                        '.txtZipCode1.Text = rd.Item(55).Value
                        '.txtZipcode2.Text = rd.Item(56).Value
                        '.txtZipCode3.Text = rd.Item(57).Value
                        .cboStay1.Text = rd.Item(58).Value
                        .cboStay2.Text = rd.Item(59).Value
                        .cboStay3.Text = rd.Item(60).Value
                        .txtStay2.Text = rd.Item(61).Value
                        .txtStay3.Text = rd.Item(62).Value
                        .txtStay1.Text = rd.Item(63).Value
                        .txtFatherName.Text = rd.Item(64).Value
                        .txtFAdd.Text = rd.Item(65).Value
                        .txtFContact.Text = rd.Item(66).Value
                        .txtMotherName.Text = rd.Item(67).Value
                        .txtMAdd.Text = rd.Item(68).Value
                        .txtMContact.Text = rd.Item(69).Value
                        .txtSpouseName.Text = rd.Item(70).Value
                        .txtSpouseAdd.Text = rd.Item(71).Value
                        .txtSpouseContact.Text = rd.Item(72).Value
                        .txtOccupation.Text = rd.Item(73).Value
                        .txtSpouseStay.Text = rd.Item(75).Value

                        Select Case (rd.Item(76).Value)
                            Case "Month"
                                .rbSpouseMonth.Checked = True
                            Case "Year"
                                .rbSpouseYears.Checked = True
                        End Select

                        .txtChildNo.Text = rd.Item(77).Value

                        Select Case (rd.Item(78).Value)
                            Case "Owned"
                                .rbOwned.Checked = True
                            Case "Rented"
                                .rbRented.Checked = True
                            Case "Living with Parents"
                                .rbParents.Checked = True
                            Case Else
                                .rbParents.Checked = False
                                .rbRented.Checked = False
                                .rbOwned.Checked = False
                        End Select

                        Select Case (rd.Item(79).Value)
                            Case True
                                .rbMYes.Checked = True
                            Case False
                                .rbMNo.Checked = True
                            Case Else
                                .rbMNo.Checked = False
                                .rbMYes.Checked = False
                        End Select

                        Select Case (rd.Item(80).Value)
                            Case True
                                .rbCYes.Checked = True
                            Case False
                                .rbCNo.Checked = True
                            Case Else
                                .rbCYes.Checked = False
                                .rbCNo.Checked = False
                        End Select

                        .txtMotor.Text = rd.Item(81).Value
                        .txtCar.Text = rd.Item(82).Value
                        .cboOutlet.Text = rd.Item(83).Value

                        .txtPresBldg.Text = rd.Item(84).Value
                        .txtPresStreet.Text = rd.Item(85).Value
                        .txtPresSitio.Text = rd.Item(86).Value
                        .txtPresCity.Text = rd.Item(87).Value
                        .txtPresProvince.Text = rd.Item(88).Value
                        .txtPrevBldg.Text = rd.Item(89).Value
                        .txtPrevStreet.Text = rd.Item(90).Value
                        .txtPrevSitio.Text = rd.Item(91).Value
                        .txtPrevCity.Text = rd.Item(92).Value
                        .txtPrevProvince.Text = rd.Item(93).Value
                        .txtPermBldg.Text = rd.Item(94).Value
                        .txtPermStreet.Text = rd.Item(95).Value
                        .txtPermSitio.Text = rd.Item(96).Value
                        .txtPermCity.Text = rd.Item(97).Value
                        .txtPermProvince.Text = rd.Item(98).Value
                        .txtSuffix.Text = rd.Item(99).Value
                        .txtContractPrice.Text = rd.Item(101).Value
                        .txtEcola.Text = rd.Item(102).Value
                        .cboContactPerson.Text = rd.Item(103).Value
                        .txtProject.Text = rd.Item(104).Value

                        'If frmMember_Master.txtCompanyName.Text <> "" Or .temp_fname.Text = "" And .temp_midname.Text = "" Then
                        '    frmMember_Master.lblemployee_name.Text = frmMember_Master.txtCompanyName.Text
                        'Else
                        '    frmMember_Master.lblemployee_name.Text = My.Forms.frmMember_Master.temp_fname.Text + " " + My.Forms.frmMember_Master.temp_midname.Text + " " + My.Forms.frmMember_Master.temp_lname.Text
                        'End If
                        .LoadEloadingRegistrationDetails(.txtEmployeeNo.Text)

                        If rd.Item(39).Value = True Then
                            .rdoExempt.Checked = True
                            .rdoNonExempt.Checked = False
                        Else
                            .rdoExempt.Checked = False
                            .rdoNonExempt.Checked = True
                        End If
                        'End If
                    End With
                End While
                Call getorgchartvalue(Me.txtorgchart.Tag)
                Call getorgchartIDtodepartment2(Me.txtorgchart.Tag)
                rd.Close()
            Catch ex As Exception
                'REDO
                'MessageBox.Show(ex.Message, "PlotRecord")
            End Try
        End Using
    End Sub
#End Region

    Private Sub txtpk_Employee_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtpk_Employee.Validating
        Call PlotRecord(Me.txtpk_Employee.Text.Trim)
    End Sub

    Private Sub btnNewBA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewBA.Click
        frmMember_BankAccount.lblBankAcct.Text = "Add New Bank Account"
        frmMember_BankAccount.btnupdate.Visible = False
        frmMember_BankAccount.ShowDialog()
    End Sub

    Private Sub btnUpdateBA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateBA.Click
        Try
            If lvlBankInfo.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Data to be Edit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                With frmMember_BankAccount
                    .lblBankAcct.Text = "Edit Bank Account"
                    .btnSave.Visible = False
                    .btnupdate.Location = New System.Drawing.Point(5, 5)
                    .txtBankAccount.Text = Me.lvlBankInfo.SelectedItems(0).Text
                    .txtBankName.Text = Me.lvlBankInfo.SelectedItems(0).SubItems(1).Text
                    .txtBranch.Text = Me.lvlBankInfo.SelectedItems(0).SubItems(2).Text
                    .txtAccountType.Text = Me.lvlBankInfo.SelectedItems(0).SubItems(3).Text
                    .txtAtmNo.Text = Me.lvlBankInfo.SelectedItems(0).SubItems(4).Text
                    .cboStatus.Text = Me.lvlBankInfo.SelectedItems(0).SubItems(5).Text
                    .getpkBA() = Me.lvlBankInfo.SelectedItems(0).SubItems(8).Text
                    .ShowDialog()
                End With
                'Else
                '    'MessageBox.Show("Nothing to Update", "Bank Account Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                '    Exit Sub
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btnDeleteBA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteBA.Click
        Try
            Dim x As New DialogResult
            Dim pkId As String = Me.lvlBankInfo.SelectedItems(0).SubItems(5).Text
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
            If x = System.Windows.Forms.DialogResult.OK Then
                If pkId <> "" Then
                    Call DeleteBankAccount(pkId)
                    Call GetmemberBankInfo(Me.txtEmployeeNo.Text)
                End If
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception

        End Try
    End Sub
#Region "Delete Bank account"
    Private Sub DeleteBankAccount(ByVal pkBA As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_BankAccounts_Delete", _
                                      New SqlParameter("@employeeNo", pkBA))
            trans.Commit()

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete Bank Account")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region

    Private Sub btnsaveSInc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsaveSInc.Click
        frmMember_SourceofIncome.lblsourcInc.Text = "Add New Source of Income"
        frmMember_SourceofIncome.btnupdate.Visible = False
        frmMember_SourceofIncome.ShowDialog()
    End Sub

    Private Sub btnUpdateSInc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateSInc.Click
        Try
            If lvlSourceIncome.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Data to be Edit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                'If Not IsNothing(lvlSourceIncome.SelectedItems(0).SubItems(0)) AndAlso lvlSourceIncome.SelectedItems(0).SubItems(1).Text <> "" Then
                '    'andalso lvlSourceIncome.SelectedItems(0).SubItems(2).Text  <> "" Then
                With frmMember_SourceofIncome
                    .lblsourcInc.Text = "Update Source of Income"
                    .btnSave.Visible = False
                    .btnupdate.Location = New System.Drawing.Point(5, 5)
                    .txtSourceInc.Text = Me.lvlSourceIncome.SelectedItems(0).Text
                    .txtAnnuallInc.Text = Me.lvlSourceIncome.SelectedItems(0).SubItems(1).Text
                    .getpksource() = Me.lvlSourceIncome.SelectedItems(0).SubItems(2).Text
                    .ShowDialog()
                End With
                'Else
                '    'MessageBox.Show("Nothing to Update", "Source of Income Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                '    Exit Sub
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btnDeleteSInc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteSInc.Click
        Try
            Dim x As New DialogResult
            Dim pkId As String = Me.lvlSourceIncome.SelectedItems(0).SubItems(2).Text
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
            If x = System.Windows.Forms.DialogResult.OK Then
                If pkId <> "" Then
                    Call DeleteSourceIncome(pkId)
                    Call GetsourceofIncomce(Me.txtEmployeeNo.Text)
                End If
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception

        End Try
    End Sub

#Region "Delete Source of Income"
    Private Sub DeleteSourceIncome(ByVal pkid As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_OtherIncomeSource_Delete", _
                                      New SqlParameter("@employeeNo", pkid))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete Source of Income")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region

    'Private Sub Cbomem_Bereavement_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Me.Cbomem_Bereavement.Text = "True" Then
    '        getbereavement() = 1
    '    Else
    '        getbereavement() = 0
    '    End If
    'End Sub

    Private Sub chkyes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkyes.CheckedChanged
        If Me.chkyes.Checked = True Then
            Me.chkNo.Enabled = False
            Me.chkNo.Checked = False
            getfbemployed() = True
        Else
            Me.chkNo.Enabled = True
        End If
    End Sub

    Private Sub chkNo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNo.CheckedChanged
        If Me.chkNo.Checked = True Then
            Me.chkyes.Enabled = False
            Me.chkyes.Checked = False
            getfbemployed() = False
        Else
            Me.chkyes.Enabled = True

        End If
    End Sub

    Private Sub Cbomem_Status_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cbomem_Status.SelectedIndexChanged
        If Me.Cbomem_Status.Text = "Active" Then
            getfbMembership() = 1
        ElseIf Me.Cbomem_Status.Text = "Inactive" Then
            getfbMembership() = 0
        End If
    End Sub

    Public Sub Integrate_CIMSToAccounting()
        Try
            Dim employeeNo As String = txtEmployeeNo.Text
            Dim memberName As String = lblemployee_name.Text
            Dim dateHired As Date = emp_Datehired.Value.Date
            Dim companyName As String = memberName
            Dim lastName As String = temp_lname.Text
            Dim firstName As String = temp_fname.Text
            Dim middleName As String = temp_midname.Text
            Dim telNo As String = txtresidencephone.Text
            Dim mobileNo As String = txtMobileNo.Text
            Dim officeNo As String = txtofficenumber.Text
            Dim emailAddress As String = txtEmailAddress.Text
            Dim isActive As String = "1"
            Dim balance As Decimal = 0
            Dim companyID As String = Integrate_CIMSGetCompanyID()
            Dim isDelete As String = "0"
            Dim user As String = frmLogin.Username.Text
            Dim dataCreated As Date = Date.Now.Date
            Dim address As String = txtPresBldg.Text

            Dim BankName As String
            Dim BankAccountNo As String
            Dim BankAccountType As String

            If formMode = "Edit" Then
                BankName = IIf(Me.lvlBankInfo.Items(0).SubItems(0).Text <> "N/A", lvlBankInfo.Items(0).SubItems(1).Text, "")
                BankAccountNo = IIf(Me.lvlBankInfo.Items(0).SubItems(0).Text <> "N/A", lvlBankInfo.Items(0).SubItems(0).Text, "")
                BankAccountType = IIf(Me.lvlBankInfo.Items(0).SubItems(0).Text <> "N/A", lvlBankInfo.Items(0).SubItems(2).Text, "")
            End If

            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Member_Integration_Accounting", _
                        New SqlParameter("@employeeNo", employeeNo), _
                        New SqlParameter("@customerName", memberName), _
                        New SqlParameter("@dateHired", dateHired), _
                        New SqlParameter("@companyName", companyName), _
                        New SqlParameter("@lastName", lastName), _
                        New SqlParameter("@firstName", firstName), _
                        New SqlParameter("@middleName", middleName), _
                        New SqlParameter("@telNo", telNo), _
                        New SqlParameter("@mobileNo", mobileNo), _
                        New SqlParameter("@officeNo", officeNo), _
                        New SqlParameter("@emailAddress", emailAddress), _
                        New SqlParameter("@active", isActive), _
                        New SqlParameter("@fxKeyCompany", companyID), _
                        New SqlParameter("@balance", balance), _
                        New SqlParameter("@deleted", isDelete), _
                        New SqlParameter("@user", user), _
                        New SqlParameter("@dateCreated", dataCreated), _
                        New SqlParameter("@address", address), _
                        New SqlParameter("@bankAccount", BankAccountNo), _
                        New SqlParameter("@bankName", BankName))
            ', _
            '                        New SqlParameter("@accountType", BankAccountType))
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Function Integrate_CIMSGetCompanyID() As String
        Dim companyID As String

        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Integration_GetCompanyID")
            If rd.Read() Then
                companyID = rd.Item("accountingID").ToString()
            Else
                MessageBox.Show("Company ID Error: Please contact System Administrator,", _
                            "Accounting Integration", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End Using

        Return companyID
    End Function

    'Public Sub LoadWebRegistrationDetails()
    '    Try
    '        Using rd As SqlDataReader = (SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Admin_CheckIfMemberIsWebRegistered", _
    '                New SqlParameter("@employeeNo", txtEmployeeNo.Text)))
    '            If rd.Read() Then
    '                chkIsWebRegistered.Checked = True
    '                txtDefaultPassword.Text = rd.Item("Default Password").ToString()
    '            End If
    '        End Using
    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Private Sub chkShowPassword_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If chkShowPassword.Checked = True Then
    '        txtDefaultPassword.UseSystemPasswordChar = False
    '    Else
    '        txtDefaultPassword.UseSystemPasswordChar = True
    '    End If
    'End Sub

    'Private Sub RegisterSelectedMemberToWeb()
    '    Try
    '        Dim status As MembershipCreateStatus
    '        Dim username As String = txtEmployeeNo.Text
    '        Dim emailAddress As String = txtEmailAddress.Text
    '        Dim password As String

    '        Using rd As SqlDataReader = (SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "MSS_Admin_EmployeeNo_GetPassword", _
    '                New SqlParameter("@fcEmployeeNo", username)))
    '            If rd.Read Then
    '                password = rd.Item("fcPassword").ToString()
    '            End If
    '        End Using

    '        Membership.CreateUser(username, password, emailAddress, "Question", "Answer", True, status)

    '        If Not Roles.IsUserInRole(username, "Member") Then
    '            Roles.AddUserToRole(username, "Member")
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub UnRegisterSelectedMemberToWeb()
    '    Try
    '        Dim isDeleted As Boolean
    '        Dim username As String = txtEmployeeNo.Text
    '        isDeleted = Membership.DeleteUser(username, True)
    '        MessageBox.Show(isDeleted)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

#Region "Eloading"
    Private Sub btnAddMobileNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddMobileNo.Click
        frmMember_AddMobile.txtMobileNo.Text = ""
        frmMember_AddMobile.GetEmpNo() = txtEmployeeNo.Text
        frmMember_AddMobile.GetMode() = "Add"
        frmMember_AddMobile.ShowDialog()
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditMobile.Click
        Try
            With frmMember_AddMobile
                .txtMobileNo.Text = gridMobileNos.Item("Mobile No", gridMobileNos.CurrentRow.Index).Value.ToString()
                .GetOldMobileNo() = gridMobileNos.Item("Mobile No", gridMobileNos.CurrentRow.Index).Value.ToString()
                .GetEmpNo() = txtEmployeeNo.Text
                .GetMode() = "Edit"
                .ShowDialog()
            End With

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub chkShowPINChar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowPINChar.CheckedChanged
        If chkShowPINChar.Checked = True Then
            txtEloaderPIN.UseSystemPasswordChar = False
        Else
            txtEloaderPIN.UseSystemPasswordChar = True
        End If
    End Sub

    Private Function isEloadingMemberValid() As Boolean
        Dim isValid As Boolean = True
        If chkIsEloader.Checked = True Then
            'If txtMobileNo.Text = "" Then
            '    MessageBox.Show("You are trying to register this member to e-loading Service," + vbNewLine + "but have not entered a valid mobile No." _
            '            + vbNewLine + vbNewLine + "Please Enter a mobile No. First.", "E-loading Service", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '    isValid = False

            'Else
            If txtEloaderPIN.Text = "" Or txtEloaderPIN.Text.Length <> 4 Then
                MessageBox.Show("You are trying to register this member to e-loading Service," + vbNewLine + "but have not entered a valid PIN No." _
                        + vbNewLine + vbNewLine + "Please enter a valid PIN No. (Must be 4 Chars only).", "E-loading Service", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                isValid = False
            ElseIf Cbomem_Status.Text.Trim <> "Active" And Cbomem_Status.Text.Trim <> "Associate" Then
                MessageBox.Show("You are trying to register this member to e-loading Service," + vbNewLine + "but the member is not active." _
                       + vbNewLine + vbNewLine + "Please Activate His membership first.", "E-loading Service", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                isValid = False
            End If
        End If
        Return isValid
    End Function

    Private Sub RegisterMemberToEloadingApps(ByVal empNo As String, ByVal PIN As String, ByVal limit As Decimal)

        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Member_Eloading_RegisterMember", _
                    New SqlParameter("@coopID", empNo), _
                    New SqlParameter("@PIN", PIN), _
                    New SqlParameter("@limit", limit))
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub UnRegisterMemberToEloadingApps(ByVal empNo As String)

        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Member_Eloading_UnRegisterMember", _
                    New SqlParameter("@coopID", empNo))
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub LoadEloadingRegistrationDetails(ByVal empNo As String)
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Member_Eloading_Status_PerMember", _
                    New SqlParameter("@fcEmployeeNo", empNo))
                If rd.Read() Then

                    'Validate Is Loader
                    Dim isLoader As String
                    isLoader = rd.Item("Is Eloader").ToString()
                    If isLoader <> "0" Or isLoader <> "1" Then
                        chkIsEloader.Checked = False
                    Else
                        chkIsEloader.Checked = rd.Item("Is Eloader")

                    End If

                    txtEloaderPIN.Text = rd.Item("PIN").ToString()
                    txtEloadingLimit.Text = rd.Item("E-Loading Limit").ToString()
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try


        ' Call LoadMemberMobileNumbers(empNo)
    End Sub

    'Public Sub LoadMemberMobileNumbers(ByVal empNo As String)
    '    Dim ds As New DataSet
    '    ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Member_Eloading_ViewMobileNos", _
    '            New SqlParameter("@employeeNo", empNo))

    '    With gridMobileNos
    '        .DataSource = ds.Tables(0)

    '        .Columns("pk_Employee").Visible = False
    '    End With
    'End Sub

    Private Sub gridMobileNos_UserDeletingRow(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles gridMobileNos.UserDeletingRow
        Dim mobileNo As String = e.Row.Cells("Mobile No").Value.ToString()

        If MessageBox.Show("Are you sure you want to delete this number from member's record?", "Delete Mobile Nos.", MessageBoxButtons.YesNo, MessageBoxIcon.Stop) = Windows.Forms.DialogResult.Yes Then
            DeleteMemberMobileNo(mobileNo)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub DeleteMemberMobileNo(ByVal mobileNo As String)
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Member_Eloading_DeleteMobileNo", _
            New SqlParameter("@mobileNo", mobileNo))
    End Sub

#End Region

    Private Sub chkBereaveYes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBereaveYes.CheckedChanged

        If Me.chkBereaveYes.Checked = True Then
            Me.chkBereaveNo.Enabled = False
            Me.chkBereaveNo.Checked = False
            getbereavement() = True
        Else
            Me.chkBereaveNo.Enabled = True
        End If
    End Sub

    Private Sub chkBereaveNo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBereaveNo.CheckedChanged
        If Me.chkBereaveNo.Checked = True Then
            Me.chkBereaveYes.Enabled = False
            Me.chkBereaveYes.Checked = False
            getbereavement() = False
        Else
            Me.chkBereaveYes.Enabled = True

        End If
    End Sub

    'Private Sub tabMember_Selected(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TabControlEventArgs) Handles tabMember.Selected
    '    If TabPage2.Click = True Then
    '        If btnemp_add.Text = "Save" Then
    '            MessageBox.Show("Please save first the member information to add the bank account information", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information)

    '            'MessageBox.Show("Supply the required fields", "Cannot Save", MessageBoxButtons.OK, MessageBoxIcon.Warning)

    '        End If
    '    End If

    'End Sub

    Private Sub TabPage2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BankInfo.Click

        If btnemp_add.Text = "Save" Then
            MessageBox.Show("Please save first the member information to add the bank account information", "Save", _
                            MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    Private Sub btnrestricted_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnrestricted.Click
        frmMember_BasicPayLogin.Show()
    End Sub

    Private Sub btnBrowseSignature_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseSignature.Click
        On Error GoTo e
        If OpenFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Dim ex As String
            OpenFileDialog1.ShowDialog()
            filenym = OpenFileDialog1.FileName
            fpath = Path.GetExtension(filenym)

            SigData2 = File.ReadAllBytes(filenym)
            ex = Path.GetExtension(LTrim(RTrim(filenym)))


            Dim ms As New MemoryStream(SigData2, 0, SigData2.Length)

            ms.Write(SigData2, 0, SigData2.Length)

            img = Image.FromStream(ms, True)
            picEmpSignature.Image = img
            picEmpSignature.SizeMode = PictureBoxSizeMode.StretchImage
        Else
            MessageBox.Show("User Cancelled!", "Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Information)
            picEmpSignature.Image = My.Resources.signature
        End If
e:
        Exit Sub
    End Sub

    Private Sub btnBrowsePicture_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowsePicture.Click
        On Error GoTo e
        If OpenFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Dim ex As String
            OpenFileDialog1.ShowDialog()
            filenym = OpenFileDialog1.FileName
            fpath = Path.GetExtension(filenym)

            PicData2 = File.ReadAllBytes(filenym)
            ex = Path.GetExtension(LTrim(RTrim(filenym)))


            Dim ms As New MemoryStream(PicData2, 0, PicData2.Length)

            ms.Write(PicData2, 0, PicData2.Length)

            img = Image.FromStream(ms, True)
            picEmpPhoto.Image = img
            picEmpPhoto.SizeMode = PictureBoxSizeMode.StretchImage
        Else
            MessageBox.Show("User Cancelled!", "Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Information)
            picEmpPhoto.Image = My.Resources.photo
        End If
e:
        Exit Sub
    End Sub

    Private Sub cbofGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbofGroup.SelectedIndexChanged
        LoadSubgroup()

        cbofCategory.Text = ""
        cbofCategory.Items.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.cnstring, "CIMS_Masterfile_Category_Select_ByGroup",
                                     New SqlParameter("@Fc_Group", cbofGroup.Text))
        While rd.Read
            cbofCategory.Items.Add(rd("Fc_Category"))
        End While
    End Sub

    Private Sub cbofCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbofCategory.SelectedIndexChanged
        cbofType.Text = ""
        cbofType.Items.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.cnstring, "CIMS_Masterfile_Type_Select_ByCategory",
                                     New SqlParameter("@Fc_Category", cbofCategory.Text))
        While rd.Read
            cbofType.Items.Add(rd("Fc_Type"))
        End While
    End Sub

    Private Sub btnMaxPhoto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMaxPhoto.Click
        frmPhotoMaximize.PictureBox1.Image = Me.picEmpPhoto.Image
        frmPhotoMaximize.PictureBox1.SizeMode = PictureBoxSizeMode.StretchImage
        frmPhotoMaximize.StartPosition = FormStartPosition.CenterScreen
        frmPhotoMaximize.ShowDialog()
    End Sub

    Private Sub btnMaxSignature_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMaxSignature.Click
        frmPhotoMaximize.PictureBox1.Image = Me.picEmpSignature.Image
        frmPhotoMaximize.PictureBox1.SizeMode = PictureBoxSizeMode.StretchImage
        frmPhotoMaximize.StartPosition = FormStartPosition.CenterScreen
        frmPhotoMaximize.ShowDialog()
    End Sub

#Region "Educational Information"

#Region "Get member Educational information"
    Public Sub GetmemberEducationalInfo(ByVal empinfo As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetEducationalInformation", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
        With Me.lvlEducInfo
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Educational Level", 150, HorizontalAlignment.Left)
            .Columns.Add("School/University", 250, HorizontalAlignment.Left)
            .Columns.Add("Degree Earned/Course", 200, HorizontalAlignment.Left)
            .Columns.Add("Years Attended", 150, HorizontalAlignment.Left)
            .Columns.Add("Honors/Special Awards Received", 200, HorizontalAlignment.Left)
            .Columns.Add("Specify", 100, HorizontalAlignment.Left)

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .SubItems.Add(myreader.Item(1))
                        .subItems.Add(myreader.Item(2))
                        .subItems.Add(myreader.Item(3))
                        .subItems.Add(myreader.Item(4))
                        .subItems.Add(myreader.Item(5))
                        .subItems.Add(myreader.Item(6))
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
            'If (.Items(0).Text) = "" And .Items(0).SubItems(1).Text = "" And
            '            .Items(0).SubItems(2).Text = "" And .Items(0).SubItems(3).Text = "" And
            '            .Items(0).SubItems(4).Text = "" And .Items(0).SubItems(5).Text = "" Then
            '    'AndAlso lvlEducInfo.SelectedItems(0).SubItems(6).Text <> "" Then
            '    btnUpdateEI.Enabled = False
            '    'MessageBox.Show("Nothing to Update", "Educational Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            'Else
            '    btnUpdateEI.Enabled = True
            'End If
        End With
    End Sub
#End Region

    Private Sub btnNewEI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewEI.Click
        frmMember_EducationalInfo.lblEducInfo.Text = "Add New Educational Information"
        frmMember_EducationalInfo.btnupdate.Visible = False
        frmMember_EducationalInfo.ShowDialog()
    End Sub

    Private Sub btnUpdateEI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateEI.Click
        Dim myconnection As New Clsappconfiguration

        Try
            If lvlEducInfo.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Data to be Edit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                With frmMember_EducationalInfo
                    .lblEducInfo.Text = "Edit Educational Information"
                    .btnSave.Visible = False
                    .btnupdate.Location = New System.Drawing.Point(5, 5)
                    .cboDegree.Text = Me.lvlEducInfo.SelectedItems(0).Text
                    .txtSchool.Text = Me.lvlEducInfo.SelectedItems(0).SubItems(1).Text
                    .txtCourse.Text = Me.lvlEducInfo.SelectedItems(0).SubItems(2).Text
                    .txtFrom.Text = Me.lvlEducInfo.SelectedItems(0).SubItems(3).Text
                    .txtAwards.Text = Me.lvlEducInfo.SelectedItems(0).SubItems(4).Text
                    .cboSpecify.Text = Me.lvlEducInfo.SelectedItems(0).SubItems(5).Text
                    .getpkSchools() = Me.lvlEducInfo.SelectedItems(0).SubItems(6).Text
                    .ShowDialog()
                End With
                'Dim rd As SqlDataReader
                'rd = SqlHelper.ExecuteReader(myconnection.cnstring, CommandType.StoredProcedure, "MSS_MembersInfo_GetEducationalInformation_other",
                '                     New SqlParameter("@EmployeeNo", txtEmployeeNo.Text), _
                '                     New SqlParameter("@pkSchools", frmMember_EducationalInfo.getpkSchools))
                'While rd.Read
                '    With frmMember_EducationalInfo
                '        .txtCourse.Text = rd.Item("Course")
                '        .txtMajorIn.Text = rd.Item("Major In")
                '        .txtAwards.Text = rd.Item("Awards")
                '        .txtProfExam.Text = rd.Item("Professional Exam")
                '        .txtScholarship.Text = rd.Item("Scholarship")
                '    End With
                'End While
                'rd.Close()
            End If
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Update Educational Information")
        End Try
    End Sub

    Private Sub DeleteEducationalInfo(ByVal Schools As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_MemberEducationalInformation_Delete", _
                                      New SqlParameter("@pkSchools", Schools))
            trans.Commit()

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete Educational Information")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnDeleteEI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteEI.Click
        Try
            Dim x As New DialogResult
            Dim pkId As String = Me.lvlEducInfo.SelectedItems(0).SubItems(6).Text
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If x = System.Windows.Forms.DialogResult.Yes Then
                If pkId <> "" Then
                    Call DeleteEducationalInfo(pkId)
                    Call GetmemberEducationalInfo(Me.txtEmployeeNo.Text)
                End If
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Job Description"

#Region "Get member Job Description"
    Public Sub GetmemberJobDescription(ByVal empinfo As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetJobDescription", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
        With Me.lvlJobDesc
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Reporting To", 250, HorizontalAlignment.Left)
            .Columns.Add("Location", 300, HorizontalAlignment.Left)
            .Columns.Add("Job Description", 300, HorizontalAlignment.Left)

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .SubItems.Add(myreader.Item(1))
                        .subItems.Add(myreader.Item(2))
                        .subItems.Add(myreader.Item(3))
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
            'If (.Items(0).Text) = "" And .Items(0).SubItems(1).Text = "" And .Items(0).SubItems(2).Text = "" Then
            '    'AndAlso lvlJobDesc.SelectedItems(0).SubItems(3).Text AndAlso
            '    btnUpdateJD.Enabled = False
            '    ''MessageBox.Show("Nothing to Update", "Job Description Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            '    'Exit Sub
            'Else
            '    btnUpdateJD.Enabled = True
            'End If
        End With
    End Sub
#End Region

    Private Sub btnNewJD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewJD.Click
        frmMember_JobDesc.lblJobDesc.Text = "Add New Job Description"
        frmMember_JobDesc.btnupdate.Visible = False
        frmMember_JobDesc.ShowDialog()
    End Sub

    Private Sub btnUpdateJD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateJD.Click
        Try
            If lvlJobDesc.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Data to be Edit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                With frmMember_JobDesc
                    .lblJobDesc.Text = "Edit Job Description"
                    .btnSave.Visible = False
                    .btnupdate.Location = New System.Drawing.Point(5, 5)
                    .txtReportTo.Text = Me.lvlJobDesc.SelectedItems(0).Text
                    .txtLocation.Text = Me.lvlJobDesc.SelectedItems(0).SubItems(1).Text
                    .txtJobDesc.Text = Me.lvlJobDesc.SelectedItems(0).SubItems(2).Text
                    .getpkJobDesc() = Me.lvlJobDesc.SelectedItems(0).SubItems(3).Text
                    .ShowDialog()
                End With
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Update Job Description")
        End Try
    End Sub

    Private Sub DeleteJobDescription(ByVal Desc As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_JobDesc_Delete", _
                                      New SqlParameter("@pk_JobDesc", Desc))
            trans.Commit()

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete Job Description")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnDeleteJD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteJD.Click
        Try
            Dim x As New DialogResult
            Dim pkId As String = Me.lvlJobDesc.SelectedItems(0).SubItems(3).Text
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If x = System.Windows.Forms.DialogResult.Yes Then
                If pkId <> "" Then
                    Call DeleteJobDescription(pkId)
                    Call GetmemberJobDescription(Me.txtEmployeeNo.Text)
                End If
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Skills"

#Region "Get member Skills"
    Public Sub GetmemberSkills(ByVal empinfo As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetSkills", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
        With Me.lvlSkills
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("License", 150, HorizontalAlignment.Left)
            .Columns.Add("Wheels", 200, HorizontalAlignment.Left)
            .Columns.Add("Transmission", 200, HorizontalAlignment.Left)
            .Columns.Add("Computer", 200, HorizontalAlignment.Left)
            .Columns.Add("Typing(WpM)", 150, HorizontalAlignment.Left)
            .Columns.Add("Technical Skills", 200, HorizontalAlignment.Left)
            .Columns.Add("Other Technical Skills", 200, HorizontalAlignment.Left)

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .SubItems.Add(myreader.Item(1))
                        .SubItems.Add(myreader.Item(2))
                        .SubItems.Add(myreader.Item(3))
                        .SubItems.Add(myreader.Item(4))
                        .SubItems.Add(myreader.Item(5))
                        .SubItems.Add(myreader.Item(6))
                        .SubItems.Add(myreader.Item(7))
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
            'If (.Items(0)).Text = "" And .Items(0).SubItems(1).Text = "" And .Items(0).SubItems(2).Text = "" Then
            '    'AndAlso lvlSkills.SelectedItems(0).SubItems(3).Text AndAlso
            '    btnUpdateSkills.Enabled = False
            '    ''MessageBox.Show("Nothing to Update", "Job Description Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            'Else
            '    btnUpdateSkills.Enabled = True
            'End If
        End With
    End Sub
#End Region

    Private Sub btnNewSkills_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewSkills.Click
        frmMember_Skills.lblSkills.Text = "Add New Skills"
        frmMember_Skills.btnupdate.Visible = False
        frmMember_Skills.ShowDialog()
    End Sub

    Private Sub btnUpdateSkills_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateSkills.Click
        Try
            If lvlSkills.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Data to be Edit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                With frmMember_Skills
                    .lblSkills.Text = "Edit Skills"
                    .btnSave.Visible = False
                    .btnupdate.Location = New System.Drawing.Point(5, 5)
                    .txtLicense.Text = Me.lvlSkills.SelectedItems(0).Text
                    .cboWheels.Text = Me.lvlSkills.SelectedItems(0).SubItems(1).Text
                    .cboTransmission.Text = Me.lvlSkills.SelectedItems(0).SubItems(2).Text
                    .txtSpecify.Text = Me.lvlSkills.SelectedItems(0).SubItems(3).Text
                    .txtWpM.Text = Me.lvlSkills.SelectedItems(0).SubItems(4).Text
                    .cboTech.Text = Me.lvlSkills.SelectedItems(0).SubItems(5).Text
                    .txtSpecifyTech.Text = Me.lvlSkills.SelectedItems(0).SubItems(6).Text
                    .getpkSkills() = Me.lvlSkills.SelectedItems(0).SubItems(7).Text
                    .ShowDialog()
                End With
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Update Skills")
        End Try
    End Sub

    Private Sub DeleteSkills(ByVal Skills As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Skills_Delete", _
                                      New SqlParameter("@pk_Skills", Skills))
            trans.Commit()

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete Skills")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnDeleteSkills_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteSkills.Click
        Try
            Dim x As New DialogResult
            Dim pkId As String = Me.lvlSkills.SelectedItems(0).SubItems(7).Text
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If x = System.Windows.Forms.DialogResult.Yes Then
                If pkId <> "" Then
                    Call DeleteSkills(pkId)
                    Call GetmemberSkills(Me.txtEmployeeNo.Text)
                End If
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Awards"

#Region "Get member Awards"
    Public Sub GetmemberAwards(ByVal empinfo As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetAwards", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
        With Me.lvlAwards
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Awards", 300, HorizontalAlignment.Left)
            .Columns.Add("Date Given", 200, HorizontalAlignment.Left)
            .Columns.Add("Given By", 300, HorizontalAlignment.Left)

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .SubItems.Add(myreader.Item(1))
                        .subItems.Add(myreader.Item(2))
                        .subItems.Add(myreader.Item(3))
                    End With

                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
            'If (.Items(0)).Text = "" And .Items(0).SubItems(1).Text = "" And .Items(0).SubItems(2).Text = "" Then
            '    btnUpdateAward.Enabled = False
            '    ''MessageBox.Show("Nothing to Update", "Awards Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            '    'Exit Sub
            'Else
            '    btnUpdateAward.Enabled = True
            'End If
        End With
    End Sub
#End Region

    Private Sub btnNewAward_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewAward.Click
        frmMember_Award.lblAwards.Text = "Add New Award"
        frmMember_Award.btnupdate.Visible = False
        frmMember_Award.ShowDialog()
    End Sub

    Private Sub btnUpdateAward_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateAward.Click
        Try
            If lvlAwards.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Data to be Edit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                With frmMember_Award
                    .lblAwards.Text = "Edit Award"
                    .btnSave.Visible = False
                    .btnupdate.Location = New System.Drawing.Point(5, 5)
                    .txtAwards.Text = Me.lvlAwards.SelectedItems(0).Text
                    .dtGiven.Text = Me.lvlAwards.SelectedItems(0).SubItems(1).Text
                    .txtGivenBy.Text = Me.lvlAwards.SelectedItems(0).SubItems(2).Text
                    .getpkAwards() = Me.lvlAwards.SelectedItems(0).SubItems(3).Text
                    .ShowDialog()
                End With
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Update Awards")
        End Try
    End Sub

    Private Sub DeleteAwards(ByVal Awards As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Awards_Delete", _
                                      New SqlParameter("@pk_Awards", Awards))
            trans.Commit()

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete Awards")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnDeleteAward_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteAward.Click
        Try
            Dim x As New DialogResult
            Dim pkId As String = Me.lvlAwards.SelectedItems(0).SubItems(3).Text
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If x = System.Windows.Forms.DialogResult.Yes Then
                If pkId <> "" Then
                    Call DeleteAwards(pkId)
                    Call GetmemberAwards(Me.txtEmployeeNo.Text)
                End If
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Medicals"
    Dim fnames As String
    Dim fnamepath As String

#Region "Get member Medeicals"
    Public Sub GetmemberMedicals(ByVal empinfo As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetMedicals", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
        With Me.lvlMedical
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Payee Name", 300, HorizontalAlignment.Left)
            .Columns.Add("Payee Number", 150, HorizontalAlignment.Left)
            .Columns.Add("Address", 300, HorizontalAlignment.Left)
            .Columns.Add("Amount Paid", 200, HorizontalAlignment.Left)
            .Columns.Add("Check No", 200, HorizontalAlignment.Left)
            .Columns.Add("Date Filed", 100, HorizontalAlignment.Left)
            .Columns.Add("Claim Type", 300, HorizontalAlignment.Left)
            .Columns.Add("Settlement Date", 150, HorizontalAlignment.Left)
            .Columns.Add("Confinement Start", 150, HorizontalAlignment.Left)
            .Columns.Add("Confinement End", 150, HorizontalAlignment.Left)
            .Columns.Add("Confinement Period", 130, HorizontalAlignment.Left)
            .Columns.Add("Status", 250, HorizontalAlignment.Left)
            .Columns.Add("Attached File Name", 300, HorizontalAlignment.Left)

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .SubItems.Add(myreader.Item(1))
                        .subItems.Add(myreader.Item(2))
                        .subItems.Add(myreader.Item(3))
                        .subItems.Add(myreader.Item(4))
                        .subItems.Add(myreader.Item(5))
                        .subItems.Add(myreader.Item(6))
                        .subItems.Add(myreader.Item(7))
                        .subItems.Add(myreader.Item(8))
                        .subItems.Add(myreader.Item(9))
                        .subItems.Add(myreader.Item(10))
                        .subItems.Add(myreader.Item(11))
                        .subItems.Add(myreader.Item(12))
                        .subItems.Add(myreader.Item(13))
                    End With

                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
#End Region

    Private Sub btnNewMed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewMed.Click
        frmMember_Medical.lblMedical.Text = "Add New Medical"
        frmMember_Medical.btnupdate.Visible = False
        frmMember_Medical.ShowDialog()
    End Sub

    Private Sub btnUpdateMed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateMed.Click
        Try
            If lvlMedical.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Data to be Edit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                With frmMember_Medical
                    .lblMedical.Text = "Edit Medical"
                    .btnSave.Visible = False
                    .btnupdate.Location = New System.Drawing.Point(5, 5)




                    .txtPayeeName.Text = Me.lvlMedical.SelectedItems(0).Text
                    .txtPayeeNumber.Text = Me.lvlMedical.SelectedItems(0).SubItems(1).Text
                    .txtAddress.Text = Me.lvlMedical.SelectedItems(0).SubItems(2).Text
                    .txtAmountPaid.Text = Me.lvlMedical.SelectedItems(0).SubItems(3).Text
                    .txtCheckNo.Text = Me.lvlMedical.SelectedItems(0).SubItems(4).Text
                    .dtDateFiled.Text = Me.lvlMedical.SelectedItems(0).SubItems(5).Text
                    .txtClaimType.Text = Me.lvlMedical.SelectedItems(0).SubItems(6).Text
                    .dtSettlement.Text = Me.lvlMedical.SelectedItems(0).SubItems(7).Text
                    .dtFrom.Text = Me.lvlMedical.SelectedItems(0).SubItems(8).Text
                    .dtdateTo.Text = Me.lvlMedical.SelectedItems(0).SubItems(9).Text
                    .txtConfineDate.Text = Me.lvlMedical.SelectedItems(0).SubItems(10).Text
                    .txtStat.Text = Me.lvlMedical.SelectedItems(0).SubItems(11).Text
                    .txtAttachFiles.Text = Me.lvlMedical.SelectedItems(0).SubItems(12).Text
                    .getpkMedicals() = Me.lvlMedical.SelectedItems(0).SubItems(13).Text
                    .ShowDialog()
                End With
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Update Medical")
        End Try
    End Sub

    Private Sub DeleteMedicals(ByVal medicals As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Medical_Delete", _
                                      New SqlParameter("@pk_Medicals", medicals))
            trans.Commit()

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete Medical")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnDeleteMed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteMed.Click
        Try
            Dim x As New DialogResult
            Dim pkId As String = Me.lvlMedical.SelectedItems(0).SubItems(13).Text
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If x = System.Windows.Forms.DialogResult.Yes Then
                If pkId <> "" Then
                    Call DeleteMedicals(pkId)
                    Call GetmemberMedicals(Me.txtEmployeeNo.Text)
                End If
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnMedUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMedUpload.Click
        Try
            If lvlMedical.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Files to Download", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                btnMedUpload.Enabled = False
                Exit Sub
            Else
                Dim afile As Byte()
                Dim myconnection As New Clsappconfiguration
                Dim cmd As New SqlCommand("MSS_MembersInfo_GetMedicals_AttachedFiles", myconnection.sqlconn)
                cmd.CommandType = CommandType.StoredProcedure
                myconnection.sqlconn.Open()
                cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = txtEmployeeNo.Text
                cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 100).Value = Me.lvlMedical.SelectedItems(0).SubItems(12).Text
                SaveFileDialog1.Filter = "Text Files (*.txt)|*.txt|PDF Files (*.pdf)|*.pdf|Word Documents (*.docx)|*.docx|Excel Worksheets (*.xlsx)|*.xlsx|PowerPoint Presentations (*.pptx)|*.pptx|Word Documents 97-2003 (*.doc)|*.docx|Excel Worksheets 93-2003 (*.xls)|*.xls|PowerPoint Presentations (*.ppt)|*.ppt" & "|Office Files|*.docx;*.xlsx;*.pptx;*.doc;*.xls;*.ppt" & "|Images|*.jpg;*.png;*.gif"
                SaveFileDialog1.Title = "Upload Files"
                Using myreader As SqlDataReader = cmd.ExecuteReader
                    myreader.Read()
                    afile = DirectCast(myreader("fcAttachedFiles"), Byte())
                End Using
                If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
                    Dim FiletoSave As String = SaveFileDialog1.FileName
                    Dim Stream As FileStream = New FileStream(FiletoSave, FileMode.Create, FileAccess.Write)
                    Stream.Write(afile, 0, afile.Length)
                    Stream.Close()
                    btnMedUpload.Enabled = False
                    MessageBox.Show("File Successfully Uploaded!", "Uploaded", MessageBoxButtons.OK, MessageBoxIcon.Information)
                ElseIf DialogResult.Cancel Then
                    btnMedUpload.Enabled = False
                    MessageBox.Show("User Cancelled!", "Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                myconnection.sqlconn.Close()
                btnPEDownload.Enabled = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Discipline Download Files", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub lvlMedical_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvlMedical.Click
        If btnemp_edit.Text = "Update" Then
            btnMedUpload.Enabled = False
        Else
            If lvlMedical.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Files to Download", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                btnMedUpload.Enabled = False
                Exit Sub
            Else
                btnMedUpload.Enabled = True
            End If
        End If
    End Sub
#End Region

#Region "Loan History"

    '#Region "Get member Loan History"
    '    Public Sub GetmemberLoanHistory(ByVal empinfo As String)
    '        Dim myconnection As New Clsappconfiguration
    '        Dim cmd As New SqlCommand("MSS_MembersInfo_GetLoans", myconnection.sqlconn)
    '        cmd.CommandType = CommandType.StoredProcedure
    '        myconnection.sqlconn.Open()
    '        cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
    '        cmd.Parameters.Add("@Loan", SqlDbType.VarChar, 50).Value = Me.cboLoan.Text
    '        With Me.lvlLoanHistory
    '            .Clear()
    '            .View = View.Details
    '            .GridLines = True
    '            .FullRowSelect = True
    '            .Columns.Add("Schedule", 250, HorizontalAlignment.Left)
    '            .Columns.Add("Date Paid", 150, HorizontalAlignment.Left)
    '            '.Columns.Add("Payroll Group", 100, HorizontalAlignment.Left)
    '            .Columns.Add("Principal", 150, HorizontalAlignment.Left)
    '            .Columns.Add("Balance", 150, HorizontalAlignment.Left)
    '            '.Columns.Add("Amount to Offset Balance", 200, HorizontalAlignment.Left)
    '            .Columns.Add("Status", 250, HorizontalAlignment.Left)
    '            '.Columns.Add("Remarks", 250, HorizontalAlignment.Left)

    '            Dim myreader As SqlDataReader
    '            myreader = cmd.ExecuteReader
    '            Try
    '                While myreader.Read
    '                    With .Items.Add(myreader.Item(0))
    '                        .SubItems.Add(myreader.Item(1))
    '                        .subItems.Add(myreader.Item(2))
    '                        .subItems.Add(myreader.Item(3))
    '                        .subItems.Add(myreader.Item(4))
    '                        '.subItems.Add(myreader.Item(5))
    '                        '.subItems.Add(myreader.Item(6))
    '                        '.subItems.Add(myreader.Item(7))
    '                        '.subItems.Add(myreader.Item(8))
    '                    End With
    '                End While
    '            Finally
    '                myreader.Close()
    '                myconnection.sqlconn.Close()
    '            End Try
    '        End With
    '    End Sub
    '#End Region

#End Region

#Region "Payroll History"

#Region "Get member Payroll History"
    Public Sub GetmemberPayrollHistory(ByVal empinfo As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetPayrollHistory", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
        With Me.lvlPayrollHistory
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Cut Off From", 150, HorizontalAlignment.Left)
            .Columns.Add("Cut Off To", 150, HorizontalAlignment.Left)
            .Columns.Add("Basi Pay", 150, HorizontalAlignment.Left)
            .Columns.Add("Gross Pay", 150, HorizontalAlignment.Left)
            .Columns.Add("Deduction", 150, HorizontalAlignment.Left)
            .Columns.Add("Net Pay", 150, HorizontalAlignment.Left)

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .SubItems.Add(myreader.Item(1))
                        .subItems.Add(myreader.Item(2))
                        .subItems.Add(myreader.Item(3))
                        .subItems.Add(myreader.Item(4))
                        .subItems.Add(myreader.Item(5))
                    End With

                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
#End Region

    Private Sub btnPayrollHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPayrollHistory.Click
        If btnPayrollHistory.Text = "Payroll History" Then
            frmMember_BasicPayLogin.ShowDialog()
            frmMember_BasicPayLogin.Text = "Login Payroll History"
        Else
            If btnPayrollHistory.Text = "Close" Then
                lvlPayrollHistory.Clear()
                lvlPayrollHistory.GridLines = False
                btnPayrollHistory.Text = "Payroll History"
            End If
        End If
    End Sub

#End Region

#Region "Leave"

#Region "Get member Leave"
    Public Sub GetmemberLeave(ByVal empinfo As String, ByVal LeaveType As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetLeave", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
        cmd.Parameters.Add("@LeaveType", SqlDbType.VarChar, 30).Value = LeaveType
        With Me.lvlLeave
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            '.Columns.Add("Date From", 100, HorizontalAlignment.Left)
            '.Columns.Add("Date To", 100, HorizontalAlignment.Left)
            '.Columns.Add("Debit", 100, HorizontalAlignment.Left)
            '.Columns.Add("Credit", 100, HorizontalAlignment.Left)
            '.Columns.Add("Balance", 100, HorizontalAlignment.Left)
            .Columns.Add("Allocated", 268, HorizontalAlignment.Left)
            .Columns.Add("Added", 200, HorizontalAlignment.Left)
            .Columns.Add("Used", 150, HorizontalAlignment.Left)
            .Columns.Add("Cash", 150, HorizontalAlignment.Left)
            .Columns.Add("Paid/Unpaid", 150, HorizontalAlignment.Left)

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .SubItems.Add(myreader.Item(1))
                        .subItems.Add(myreader.Item(2))
                        .subItems.Add(myreader.Item(3))
                        .subItems.Add(myreader.Item(4))
                    End With

                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
#End Region

    Private Sub LoadLeaveTypes()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gCon.cnstring, "CIMS_Member_Leaves_List")
        While rd.Read
            With cboLeave
                .Items.Add(rd("fcLeaveDescription"))
            End With
        End While
    End Sub
#End Region

#Region "Trainings"
    Dim files As Byte()
    Dim Hrs As Integer

#Region "Get member Trainings"
    Public Sub GetmemberTrainings(ByVal empinfo As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("MSS_MembersInfo_GetTrainings", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            myconnection.sqlconn.Open()
            cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
            With Me.lvlTrainings
                .Clear()
                .View = View.Details
                .GridLines = True
                .FullRowSelect = True
                .Columns.Add("Inclusive Dates", 150, HorizontalAlignment.Left)
                .Columns.Add("Trainer/Organization", 300, HorizontalAlignment.Left)
                .Columns.Add("Topic/Subject Title", 309, HorizontalAlignment.Left)
                .Columns.Add("No of Hours/Days", 150, HorizontalAlignment.Left)
                .Columns.Add("Attached File Name", 200, HorizontalAlignment.Left)

                Dim myreader As SqlDataReader
                myreader = cmd.ExecuteReader
                Try
                    While myreader.Read
                        With .Items.Add(myreader.Item(0))
                            .SubItems.Add(myreader.Item(1))
                            .subItems.Add(myreader.Item(2))
                            .subItems.Add(myreader.Item(3))
                            .subItems.Add(myreader.Item(4))
                            .subItems.Add(myreader.Item(5))
                            .subItems.Add(myreader.Item(6))
                            .subItems.Add(myreader.Item(7))
                            '.subItems.Add(myreader.Item(8))
                            '.subItems.Add(myreader.Item(9))
                        End With
                    End While
                Finally
                    myreader.Close()
                    myconnection.sqlconn.Close()
                End Try
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Trainings List")
        End Try
    End Sub
#End Region

    Private Sub btnNewT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewT.Click
        frmMember_Trainings.lblTrainings.Text = "Add New Training"
        frmMember_Trainings.btnEdit.Visible = False
        frmMember_Trainings.ShowDialog()
    End Sub

    Private Sub DeleteTrainings(ByVal trainings As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Trainings_Delete", _
                                      New SqlParameter("@pk_Trainings", trainings))
            trans.Commit()

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete Trainings")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnDeleteT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteT.Click
        Try
            Dim x As New DialogResult
            Dim pkId As String = Me.lvlTrainings.SelectedItems(0).SubItems(7).Text
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If x = System.Windows.Forms.DialogResult.Yes Then
                If pkId <> "" Then
                    Call DeleteTrainings(pkId)
                    Call GetmemberTrainings(Me.txtEmployeeNo.Text)
                End If
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnUpdateT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateT.Click
        Dim HoursDays As String

        Try
            If lvlTrainings.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Data to be Edit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                With frmMember_Trainings
                    .lblTraining.Text = "Edit Trainings"
                    .btnAdd.Visible = False
                    .btnEdit.Location = New System.Drawing.Point(5, 5)
                    .dtFrom.Text = Me.lvlTrainings.SelectedItems(0).Text
                    .txtOfferedBy.Text = Me.lvlTrainings.SelectedItems(0).SubItems(1).Text
                    .txtTitle.Text = Me.lvlTrainings.SelectedItems(0).SubItems(2).Text
                    .txtAttachFiles.Text = Me.lvlTrainings.SelectedItems(0).SubItems(4).Text
                    .txtHrs.Text = Me.lvlTrainings.SelectedItems(0).SubItems(5).Text
                    HoursDays = Me.lvlTrainings.SelectedItems(0).SubItems(6).Text
                    .getpkTrainings() = Me.lvlTrainings.SelectedItems(0).SubItems(7).Text
                    If HoursDays = "Hours" Then
                        .rbHours.Checked = True
                    Else
                        .rbHours.Checked = False
                    End If
                    If HoursDays = "Days" Then
                        .rbDays.Checked = True
                    Else
                        .rbDays.Checked = False
                    End If
                    .ShowDialog()
                End With
                'Else
                'MessageBox.Show("Nothing to Update", "Medical Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Update Trainings")
        End Try
    End Sub

    Private Sub btnTDownload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTDownload.Click
        Try
            If lvlTrainings.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Files to Download", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                btnTDownload.Enabled = False
                Exit Sub
            Else
                Dim afile As Byte()
                Dim myconnection As New Clsappconfiguration
                Dim cmd As New SqlCommand("MSS_MembersInfo_GetTrainings_AttachedFiles", myconnection.sqlconn)
                cmd.CommandType = CommandType.StoredProcedure
                myconnection.sqlconn.Open()
                cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = txtEmployeeNo.Text
                cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 100).Value = Me.lvlTrainings.SelectedItems(0).SubItems(4).Text
                SaveFileDialog1.Filter = "Text Files (*.txt)|*.txt|PDF Files (*.pdf)|*.pdf|Word Documents (*.docx)|*.docx|Excel Worksheets (*.xlsx)|*.xlsx|PowerPoint Presentations (*.pptx)|*.pptx|Word Documents 97-2003 (*.doc)|*.docx|Excel Worksheets 93-2003 (*.xls)|*.xls|PowerPoint Presentations (*.ppt)|*.ppt" & "|Office Files|*.docx;*.xlsx;*.pptx;*.doc;*.xls;*.ppt" & "|Images|*.jpg;*.png;*.gif"
                SaveFileDialog1.Title = "Upload Files"
                Using myreader As SqlDataReader = cmd.ExecuteReader
                    myreader.Read()
                    afile = DirectCast(myreader("fcAttachedFiles"), Byte())
                End Using
                If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
                    Dim FiletoSave As String = SaveFileDialog1.FileName
                    Dim Stream As FileStream = New FileStream(FiletoSave, FileMode.Create, FileAccess.Write)
                    Stream.Write(afile, 0, afile.Length)
                    Stream.Close()
                    btnTDownload.Enabled = False
                    MessageBox.Show("File Successfully Uploaded!", "Uploaded", MessageBoxButtons.OK, MessageBoxIcon.Information)
                ElseIf DialogResult.Cancel Then
                    btnTDownload.Enabled = False
                    MessageBox.Show("User Cancelled!", "Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                myconnection.sqlconn.Close()
                btnPEDownload.Enabled = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Training Download Files", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub lvlTrainings_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvlTrainings.Click
        If btnemp_edit.Text = "Update" Then
            btnTDownload.Enabled = False
        Else
            If lvlTrainings.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Files to Download", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                btnTDownload.Enabled = False
                Exit Sub
            Else
                btnTDownload.Enabled = True
            End If
        End If
    End Sub
#End Region

#Region "Performance Evaluation"

#Region "Get member Performance Evaluation"
    Public Sub GetmemberPerfEvaluation(ByVal empinfo As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetPerfEval", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
        With Me.lvlPerfEval
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Evaluation Description", 300, HorizontalAlignment.Left)
            .Columns.Add("From", 100, HorizontalAlignment.Left)
            .Columns.Add("To", 100, HorizontalAlignment.Left)
            .Columns.Add("Evaluated By", 300, HorizontalAlignment.Left)
            .Columns.Add("Attached File Name", 200, HorizontalAlignment.Left)

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .SubItems.Add(myreader.Item(1))
                        .subItems.Add(myreader.Item(2))
                        .subItems.Add(myreader.Item(3))
                        .subItems.Add(myreader.Item(4))
                        .subItems.Add(myreader.Item(5))
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
#End Region

    Private Sub btnNewPE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewPE.Click
        frmMember_PerfEval.lblPerfEval.Text = "Add New Performance Evaluation"
        frmMember_PerfEval.btnupdate.Visible = False
        frmMember_PerfEval.ShowDialog()
    End Sub

    Private Sub btnPEDownload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPEDownload.Click
        Try
            If lvlPerfEval.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Files to Download", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                btnPEDownload.Enabled = False
                Exit Sub
            Else
                Dim afile As Byte()
                Dim myconnection As New Clsappconfiguration
                Dim cmd As New SqlCommand("MSS_MembersInfo_GetPerfEval_AttachedFiles", myconnection.sqlconn)
                cmd.CommandType = CommandType.StoredProcedure
                myconnection.sqlconn.Open()
                cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = txtEmployeeNo.Text
                cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 100).Value = Me.lvlPerfEval.SelectedItems(0).SubItems(4).Text
                SaveFileDialog1.Filter = "Text Files (*.txt)|*.txt|PDF Files (*.pdf)|*.pdf|Word Documents (*.docx)|*.docx|Excel Worksheets (*.xlsx)|*.xlsx|PowerPoint Presentations (*.pptx)|*.pptx|Word Documents 97-2003 (*.doc)|*.docx|Excel Worksheets 93-2003 (*.xls)|*.xls|PowerPoint Presentations (*.ppt)|*.ppt" & "|Office Files|*.docx;*.xlsx;*.pptx;*.doc;*.xls;*.ppt" & "|Images|*.jpg;*.png;*.gif"
                SaveFileDialog1.Title = "Upload Files"
                Using myreader As SqlDataReader = cmd.ExecuteReader
                    myreader.Read()
                    afile = DirectCast(myreader("fcAttachedFiles"), Byte())
                End Using
                If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
                    Dim FiletoSave As String = SaveFileDialog1.FileName
                    Dim Stream As FileStream = New FileStream(FiletoSave, FileMode.Create, FileAccess.Write)
                    Stream.Write(afile, 0, afile.Length)
                    Stream.Close()
                    btnPEDownload.Enabled = False
                    MessageBox.Show("File Successfully Uploaded!", "Uploaded", MessageBoxButtons.OK, MessageBoxIcon.Information)
                ElseIf DialogResult.Cancel Then
                    btnPEDownload.Enabled = False
                    MessageBox.Show("User Cancelled!", "Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                myconnection.sqlconn.Close()
                btnPEDownload.Enabled = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Performance Evaluation Download Files", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub lvlPerfEval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvlPerfEval.Click
        If btnemp_edit.Text = "Update" Then
            btnPEDownload.Enabled = False
        Else
            If lvlPerfEval.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Files to Download", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                btnPEDownload.Enabled = False
                Exit Sub
            Else
                btnPEDownload.Enabled = True
            End If
        End If
    End Sub
    Private Sub btnUpdatePE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdatePE.Click
        Try
            If lvlPerfEval.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Data to be Edit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                With frmMember_PerfEval
                    .lblPerfEval.Text = "Edit Performance Evaluation"
                    .btnSave.Visible = False
                    .btnupdate.Location = New System.Drawing.Point(5, 5)
                    .txtEvalDesc.Text = Me.lvlPerfEval.SelectedItems(0).Text
                    .txtFrom.Text = Me.lvlPerfEval.SelectedItems(0).SubItems(1).Text
                    .txtTo.Text = Me.lvlPerfEval.SelectedItems(0).SubItems(2).Text
                    .txtEvalBy.Text = Me.lvlPerfEval.SelectedItems(0).SubItems(3).Text
                    .txtAttachFiles.Text = Me.lvlPerfEval.SelectedItems(0).SubItems(4).Text
                    .getpkEvaluation() = Me.lvlPerfEval.SelectedItems(0).SubItems(5).Text
                    .ShowDialog()
                End With
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Update Perfomance Evaluation")
        End Try
    End Sub

    Private Sub DeleteEvaluation(ByVal eval As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_PerfEval_Delete", _
                                      New SqlParameter("@pk_Evaluations", eval))
            trans.Commit()

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete Evaluation")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnDeletePE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeletePE.Click
        Try
            Dim x As New DialogResult
            Dim pkId As String = Me.lvlPerfEval.SelectedItems(0).SubItems(6).Text
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If x = System.Windows.Forms.DialogResult.Yes Then
                If pkId <> "" Then
                    Call DeleteEvaluation(pkId)
                    Call GetmemberPerfEvaluation(Me.txtEmployeeNo.Text)
                End If
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Discipline"

#Region "Get member Discipline"
    Public Sub GetmemberDiscipline(ByVal empinfo As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetDiscipline", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
        With Me.lvlDiscipline
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Date of Offense", 100, HorizontalAlignment.Left)
            .Columns.Add("Offense", 200, HorizontalAlignment.Left)
            .Columns.Add("Details", 200, HorizontalAlignment.Left)
            .Columns.Add("Disciplinary Action", 308, HorizontalAlignment.Left)
            '.Columns.Add("Offense Count", 100, HorizontalAlignment.Left)
            '.Columns.Add("Admin Hearing Date", 200, HorizontalAlignment.Left)
            '.Columns.Add("Penalty", 100, HorizontalAlignment.Left)
            '.Columns.Add("Date Resolved", 100, HorizontalAlignment.Left)
            .Columns.Add("Attached File Name", 200, HorizontalAlignment.Left)

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .SubItems.Add(myreader.Item(1))
                        .subItems.Add(myreader.Item(2))
                        .subItems.Add(myreader.Item(3))
                        .subItems.Add(myreader.Item(4))
                        .subItems.Add(myreader.Item(5))
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
#End Region

    Private Sub btnNewDisc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewDisc.Click
        frmMember_Discipline.lblDiscipline.Text = "Add New Discipline"
        frmMember_Discipline.btnupdate.Visible = False
        frmMember_Discipline.ShowDialog()
    End Sub

    Private Sub btnDDownload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDDownload.Click
        Try
            If lvlDiscipline.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Files to Download", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                btnDDownload.Enabled = False
                Exit Sub
            Else
                Dim afile As Byte()
                Dim myconnection As New Clsappconfiguration
                Dim cmd As New SqlCommand("MSS_MembersInfo_GetDiscipline_AttachedFiles", myconnection.sqlconn)
                cmd.CommandType = CommandType.StoredProcedure
                myconnection.sqlconn.Open()
                cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = txtEmployeeNo.Text
                cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 100).Value = Me.lvlDiscipline.SelectedItems(0).SubItems(4).Text
                SaveFileDialog1.Filter = "Text Files (*.txt)|*.txt|PDF Files (*.pdf)|*.pdf|Word Documents (*.docx)|*.docx|Excel Worksheets (*.xlsx)|*.xlsx|PowerPoint Presentations (*.pptx)|*.pptx|Word Documents 97-2003 (*.doc)|*.docx|Excel Worksheets 93-2003 (*.xls)|*.xls|PowerPoint Presentations (*.ppt)|*.ppt" & "|Office Files|*.docx;*.xlsx;*.pptx;*.doc;*.xls;*.ppt" & "|Images|*.jpg;*.png;*.gif"
                SaveFileDialog1.Title = "Upload Files"
                Using myreader As SqlDataReader = cmd.ExecuteReader
                    myreader.Read()
                    afile = DirectCast(myreader("fcAttachedFiles"), Byte())
                End Using
                If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
                    Dim FiletoSave As String = SaveFileDialog1.FileName
                    Dim Stream As FileStream = New FileStream(FiletoSave, FileMode.Create, FileAccess.Write)
                    Stream.Write(afile, 0, afile.Length)
                    Stream.Close()
                    btnDDownload.Enabled = False
                    MessageBox.Show("File Successfully Uploaded!", "Uploaded", MessageBoxButtons.OK, MessageBoxIcon.Information)
                ElseIf DialogResult.Cancel Then
                    btnDDownload.Enabled = False
                    MessageBox.Show("User Cancelled!", "Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                myconnection.sqlconn.Close()
                btnPEDownload.Enabled = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Discipline Download Files", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub lvlDiscipline_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvlDiscipline.Click
        If btnemp_edit.Text = "Update" Then
            btnDDownload.Enabled = False
        Else
            If lvlDiscipline.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Files to Download", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                btnDDownload.Enabled = False
                Exit Sub
            Else
                btnDDownload.Enabled = True
            End If
        End If
    End Sub

    Private Sub btnUpdateDisc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateDisc.Click
        Try
            If lvlDiscipline.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Data to be Edit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                With frmMember_Discipline
                    .lblDiscipline.Text = "Edit Discipline"
                    .btnSave.Visible = False
                    .btnupdate.Location = New System.Drawing.Point(5, 5)
                    .dtOffense.Text = Me.lvlDiscipline.SelectedItems(0).Text
                    .txtOffense.Text = Me.lvlDiscipline.SelectedItems(0).SubItems(1).Text
                    .txtCaseHis.Text = Me.lvlDiscipline.SelectedItems(0).SubItems(2).Text
                    .txtStatus.Text = Me.lvlDiscipline.SelectedItems(0).SubItems(3).Text
                    .txtAttachFiles.Text = Me.lvlDiscipline.SelectedItems(0).SubItems(4).Text
                    .getpkDiscipline() = Me.lvlDiscipline.SelectedItems(0).SubItems(5).Text
                    .ShowDialog()
                End With
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Update Perfomance Evaluation")
        End Try
    End Sub

    Private Sub DeleteDiscipline(ByVal disc As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Discipline_Delete", _
                                      New SqlParameter("@pk_Discipline", disc))
            trans.Commit()

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete Discipline")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnDeleteDisc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteDisc.Click
        Try
            Dim x As New DialogResult
            Dim pkId As String = Me.lvlDiscipline.SelectedItems(0).SubItems(5).Text
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If x = System.Windows.Forms.DialogResult.Yes Then
                If pkId <> "" Then
                    Call DeleteDiscipline(pkId)
                    Call GetmemberDiscipline(Me.txtEmployeeNo.Text)
                End If
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Benefeciaries"

#Region "Get Beneficiaries"
    Public Sub GetNearestRelatives(ByVal empinfo As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetRelatives", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
        With Me.lvlNearestRelatives
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Name", 400, HorizontalAlignment.Left)
            .Columns.Add("Relationship", 314, HorizontalAlignment.Left)
            .Columns.Add("Birth Date", 200, HorizontalAlignment.Left)
            '.Columns.Add("Address", 300, HorizontalAlignment.Left)

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .SubItems.Add(myreader.Item(1))
                        .SubItems.Add(myreader.Item(2))
                        .SubItems.Add(myreader.Item(3))
                        '.SubItems.Add(myreader.Item(4))
                    End With
                End While
                'If (.Items(0).Text) = "" And .Items(0).SubItems(1).Text = "" And .Items(0).SubItems(2).Text = "" Then
                '    'AndAlso lvlNearestRelatives.SelectedItems(0).SubItems(4).Text <> "" Then
                '    BtnEditContact.Enabled = False
                '    'MessageBox.Show("Nothing to Update", "Bank Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                'Else
                '    BtnEditContact.Enabled = True
                'End If
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
#End Region

    Private Sub btnaddContact_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnaddContact.Click
        frmMember_Relatives.Label1.Text = "Add New Relatives"
        frmMember_Relatives.ShowDialog()
        frmMember_Relatives.btnupdate.Visible = False
    End Sub

    Private Sub BtnEditContact_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnEditContact.Click
        Try
            If lvlNearestRelatives.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Data to be Edit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                With frmMember_Relatives
                    .Label1.Text = "Edit Nearest Relatives"
                    .btnsave.Visible = False
                    .btnupdate.Visible = True
                    .btnupdate.Location = New System.Drawing.Point(5, 5)
                    .txtname.Text = Me.lvlNearestRelatives.SelectedItems(0).Text
                    .txtrelationship.Text = Me.lvlNearestRelatives.SelectedItems(0).SubItems(1).Text
                    .dtBirthdate.Text = Me.lvlNearestRelatives.SelectedItems(0).SubItems(2).Text
                    '.txtaddress.Text = Me.lvlNearestRelatives.SelectedItems(0).SubItems(3).Text
                    .getRelativepk() = Me.lvlNearestRelatives.SelectedItems(0).SubItems(3).Text
                    .ShowDialog()
                End With
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BtnDeleteContact_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnDeleteContact.Click
        Try
            Dim x As New DialogResult
            Dim pkSID As String = Me.lvlNearestRelatives.SelectedItems(0).SubItems(3).Text
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
            If x = System.Windows.Forms.DialogResult.OK Then
                If pkSID <> "" Then
                    Call DeleteRelatives(pkSID)
                    Call GetNearestRelatives(Me.txtEmployeeNo.Text.Trim)
                End If
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub DeleteRelatives(ByVal pk As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Relatives_Delete", _
                                     New SqlParameter("@employeeNo", pk))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete Relatives")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region

#Region "Dependents"

#Region "Get Dependents"
    Public Sub GetmemberDependents(ByVal empinfo As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetDependents", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
        With Me.LvlEmployeeDependent
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Name", 400, HorizontalAlignment.Left)
            .Columns.Add("Relationship", 304, HorizontalAlignment.Left)
            .Columns.Add("Birth Date", 200, HorizontalAlignment.Left)
            '.Columns.Add("Address", 300, HorizontalAlignment.Left)

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .SubItems.Add(myreader.Item(1))
                        .SubItems.Add(myreader.Item(2))
                        .SubItems.Add(myreader.Item(3))
                        '.SubItems.Add(myreader.Item(4))
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
            'If (.Items(0).Text) = "" And .Items(0).SubItems(1).Text = "" And .Items(0).SubItems(2).Text = "" Then
            '    'AndAlso LvlEmployeeDependent.SelectedItems(0).SubItems(4).Text <> "" Then
            '    btnemp_editdepdts.Enabled = False
            '    'MessageBox.Show("Nothing to Update", "Dependents Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            'Else
            '    btnemp_editdepdts.Enabled = True
            'End If
        End With
    End Sub
#End Region

    Private Sub btnadd_dep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnadd_dep.Click
        frmMember_Dependents.Label1.Text = "Add New Dependents"
        frmMember_Dependents.ShowDialog()
        frmMember_Dependents.btnupdate.Visible = False
    End Sub

    Private Sub btnemp_editdepdts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_editdepdts.Click
        Try
            If LvlEmployeeDependent.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Data to be Edit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                'If Not IsNothing(LvlEmployeeDependent.SelectedItems(0).SubItems(0)) AndAlso LvlEmployeeDependent.SelectedItems(0).SubItems(1).Text AndAlso
                '    LvlEmployeeDependent.SelectedItems(0).SubItems(2).Text AndAlso LvlEmployeeDependent.SelectedItems(0).SubItems(3).Text <> "" Then
                '    'andalso LvlEmployeeDependent.SelectedItems(0).SubItems(4).Text  <> "" Then
                With frmMember_Dependents
                    .Label1.Text = "Edit Dependents"
                    .btnsave_dep.Visible = False
                    .btnupdate.Location = New System.Drawing.Point(5, 5)
                    .txtname.Text = Me.LvlEmployeeDependent.SelectedItems(0).Text
                    .txtrelation.Text = Me.LvlEmployeeDependent.SelectedItems(0).SubItems(1).Text
                    .dtBirthdate.Text = Me.LvlEmployeeDependent.SelectedItems(0).SubItems(2).Text
                    '.txtaddress.Text = Me.LvlEmployeeDependent.SelectedItems(0).SubItems(3).Text
                    .getempdepID() = Me.LvlEmployeeDependent.SelectedItems(0).SubItems(3).Text 'key_id
                    .ShowDialog()
                End With
                'Else
                ''MessageBox.Show("Nothing to Update", "Dependents Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                'Exit Sub
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btndelete_dep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete_dep.Click
        Try

            Dim x As New DialogResult
            Dim pksid As String = Me.LvlEmployeeDependent.SelectedItems(0).SubItems(3).Text 'key_id
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
            If x = System.Windows.Forms.DialogResult.OK Then
                If pksid <> "" Then
                    Call DeleteDependents(pksid)
                    Call GetmemberDependents(Me.txtEmployeeNo.Text.Trim)
                End If
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub DeleteDependents(ByVal pks As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Dependents_Delete", _
                                     New SqlParameter("@employeeNo", pks))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete dependents")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region

#Region "bank information"
    Public Sub GetmemberBankInfo(ByVal empinfo As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetBankAccounts", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
        With Me.lvlBankInfo
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Bank Account", 200, HorizontalAlignment.Left)
            .Columns.Add("Bank Name", 208, HorizontalAlignment.Left)
            .Columns.Add("Branch", 200, HorizontalAlignment.Left)
            .Columns.Add("Account Type", 150, HorizontalAlignment.Left)
            .Columns.Add("ATM No", 150, HorizontalAlignment.Left)
            .Columns.Add("Status", 150, HorizontalAlignment.Left)
            .Columns.Add("User", 200, HorizontalAlignment.Left)
            .Columns.Add("Date Updated", 100, HorizontalAlignment.Left)

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .SubItems.Add(myreader.Item(1))
                        .subItems.Add(myreader.Item(2))
                        .subItems.Add(myreader.Item(3))
                        .subItems.Add(myreader.Item(4))
                        .subItems.Add(myreader.Item(5))
                        .subItems.Add(myreader.Item(6))
                        .subItems.Add(myreader.Item(7))
                        .subItems.Add(myreader.Item(8)) 'pk_bankAccounts
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
            'If (.Items(0).Text) = "" And .Items(0).SubItems(1).Text = "" And .Items(0).SubItems(2).Text = "" AndAlso
            '    .Items(0).SubItems(3).Text = "" And .Items(0).SubItems(4).Text = "" And .Items(0).SubItems(5).Text = "" Then
            '    btnUpdateBA.Enabled = False
            '    'MessageBox.Show("Nothing to Update", "Bank Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            'Else
            '    btnUpdateBA.Enabled = True
            'End If
        End With
    End Sub
#End Region

#Region "source of income"
    Public Sub GetsourceofIncomce(ByVal empinfo As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetSourceOfIncome", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
        With Me.lvlSourceIncome
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Source of Income", 400, HorizontalAlignment.Left)
            .Columns.Add("Annual Income", 300, HorizontalAlignment.Left)


            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .SubItems.Add(myreader.Item(1))
                        .subitems.add(myreader.Item(2))

                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
            'If (.Items(0).Text) = "" And .Items(0).SubItems(1).Text = "0.00" Then
            '    'AndAlso lvlSourceIncome.SelectedItems(0).SubItems(2).Text <> "" Then
            '    btnUpdateSInc.Enabled = False
            '    'MessageBox.Show("Nothing to Update", "Bank Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            'Else
            '    btnUpdateSInc.Enabled = True
            'End If
        End With
    End Sub
#End Region

#Region "Company Name"
    '    Private Sub AddCompanyName()
    '        Try
    '            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Member_Master_AddCompanyName", _
    '                                      New SqlParameter("@EmployeeNo", txtEmployeeNo.Text), _
    '                                      New SqlParameter("@CompanyName", txtCompanyName.Text))
    '        Catch ex As Exception
    '            MessageBox.Show(ex.Message)
    '        End Try
    '    End Sub
    '    Private Sub EditCompanyName()
    '        Try
    '            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Member_Master_EditCompanyName", _
    '                                      New SqlParameter("@EmployeeNo", txtEmployeeNo.Text), _
    '                                      New SqlParameter("@CompanyName", txtCompanyName.Text))
    '        Catch ex As Exception
    '            MessageBox.Show(ex.Message)
    '        End Try
    '    End Sub

    Private Sub txtCompanyName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCompanyName.TextChanged
        Me.lblemployee_name.Text = txtCompanyName.Text
    End Sub
#End Region

#Region "per Employee Status"
    Public Sub PerEmployeeStatus()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_status", myconnection.sqlconn)
        myconnection.sqlconn.Open()
        Dim mydatareader As SqlDataReader = cmd.ExecuteReader()
        Try
            While mydatareader.Read()
                Me.cboemp_status.Items.Add(mydatareader(0))
            End While
        Finally
            mydatareader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub

    'Private Sub LoadStatus()
    '    Dim rd As SqlDataReader
    '    rd = SqlHelper.ExecuteReader(gCon.cnstring, "usp_per_employee_status_perID", _
    '                                 New SqlParameter("@Employee", txtEmployeeNo.Text))
    '    While rd.Read
    '        With cboemp_status
    '            .Items.Add(rd("Statusdescription"))
    '        End With
    '    End While
    'End Sub
#End Region

#Region "Update Employee in Payroll"
    Private Sub UpdatePayroll_EmployeeFile()
        Dim TKid As String
        Dim schedule As String
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "Payroll_EmployeeFile_Update",
                                    New SqlParameter("@chrEmployeeID", txtEmployeeNo.Text),
                                    New SqlParameter("@EmpStatus", cboemp_status.Text),
                                    New SqlParameter("@EmpType", cboEmp_type.Text),
                                    New SqlParameter("@chrKeySection", txtorgchart.Tag),
                                    New SqlParameter("@fcLastName", temp_lname.Text),
                                    New SqlParameter("@fcFirstName", temp_fname.Text),
                                    New SqlParameter("@fcMiddleName", temp_midname.Text),
                                    New SqlParameter("@fcEmail", txtEmailAddress.Text),
                                    New SqlParameter("@fnBasicPay", txtbasicpay.Text),
                                    New SqlParameter("@RateType", cborate.Text),
                                    New SqlParameter("@PositionLevel", cbotitledesignation.Text),
                                    New SqlParameter("@fdDateHired", emp_Datehired.Value),
                                    New SqlParameter("@fcSSSNo", txtSSS.Text),
                                    New SqlParameter("@fcPhilHealthNo", txtPhilhealth.Text),
                                    New SqlParameter("@fcTINNo", txtTin.Text),
                                    New SqlParameter("@fcTaxCode", cbotaxcode.Text),
                                    New SqlParameter("@fcPagIBIGNo", txtHDMF.Text),
                                    New SqlParameter("@DECCOLAMonthly", txtEcola.Text),
                                    New SqlParameter("@fcOfficePhone", txtofficenumber.Text),
                                    New SqlParameter("@fcMobilePhone", txtresidencephone.Text),
                                    New SqlParameter("@fdDateBirth", EMP_DATEbirth.Value),
                                    New SqlParameter("@fcGender", cboemp_gender.Text),
                                    New SqlParameter("@ContractCode", txtContractrate.Text))
            'Dim x As New DialogResult
            'x = MessageBox.Show("Records successfully updated!", "Updating Record...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'New SqlParameter("@schedule", schedule),
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            'Throw ex
            MessageBox.Show(ex.Message, "UpdatePayroll_EmployeeFile", MessageBoxButtons.OK)
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region

#Region "Load Client"
    Private Sub LoadClient()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_Member_LoadClientRepresentative")
        Try
            While rd.Read
                Me.cboClient.Items.Add(rd.Item("Full Name"))
            End While
            rd.Close()
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Government Examination"

#Region "Get member Gov Exam"
    Public Sub GetmemberGovExam(ByVal empinfo As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("MSS_MembersInfo_GetGovExam", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            myconnection.sqlconn.Open()
            cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
            With Me.lvlGovExam
                .Clear()
                .View = View.Details
                .GridLines = True
                .FullRowSelect = True
                .Columns.Add("Dates", 200, HorizontalAlignment.Left)
                .Columns.Add("Examination", 400, HorizontalAlignment.Left)
                .Columns.Add("Ratings", 306, HorizontalAlignment.Left)

                Dim myreader As SqlDataReader
                myreader = cmd.ExecuteReader
                Try
                    While myreader.Read
                        With .Items.Add(myreader.Item(0))
                            .SubItems.Add(myreader.Item(1))
                            .subItems.Add(myreader.Item(2))
                            .subItems.Add(myreader.Item(3))
                        End With
                    End While
                Finally
                    myreader.Close()
                    myconnection.sqlconn.Close()
                End Try
                '    If (.Items(0).Text) = "" And .Items(0).SubItems(1).Text = "" And .Items(0).SubItems(2).Text = "" Then
                '        btnUpdateGE.Enabled = False
                '    Else
                '        btnUpdateGE.Enabled = True
                '    End If
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Government Examination List")
        End Try
    End Sub
#End Region

    Private Sub btnNewGE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewGE.Click
        frmMember_GovExam.lblGovExam.Text = "Add New Government Examination Taken"
        frmMember_GovExam.btnupdate.Visible = False
        frmMember_GovExam.ShowDialog()
    End Sub

    Private Sub btnUpdateGE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateGE.Click
        Try
            If lvlGovExam.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Data to be Edit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                With frmMember_GovExam
                    .lblGovExam.Text = "Edit Exam"
                    .btnSave.Visible = False
                    .btnupdate.Location = New System.Drawing.Point(5, 5)
                    .dtDate.Text = Me.lvlGovExam.SelectedItems(0).Text
                    .txtExam.Text = Me.lvlGovExam.SelectedItems(0).SubItems(1).Text
                    .txtRatings.Text = Me.lvlGovExam.SelectedItems(0).SubItems(2).Text
                    .getpkGovExam() = Me.lvlGovExam.SelectedItems(0).SubItems(3).Text
                    .ShowDialog()
                End With
                'Else
                'MessageBox.Show("Nothing to Update", "Medical Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Update Government Exam")
        End Try
    End Sub

    Private Sub DeleteGovExam(ByVal Exam As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_GovExam_Delete", _
                                      New SqlParameter("@pk_Exam", Exam))
            trans.Commit()

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete Examination")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnDeleteGE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteGE.Click
        Try
            Dim x As New DialogResult
            Dim pkId As String = Me.lvlGovExam.SelectedItems(0).SubItems(3).Text
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If x = System.Windows.Forms.DialogResult.Yes Then
                If pkId <> "" Then
                    Call DeleteGovExam(pkId)
                    Call GetmemberGovExam(Me.txtEmployeeNo.Text)
                End If
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Employment History"

#Region "Get Employment History"
    Public Sub GetmemberEmployment(ByVal empinfo As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("MSS_MembersInfo_GetEmploymentHistory", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            myconnection.sqlconn.Open()
            cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
            With Me.lvlEmploymentHistory
                .Clear()
                .View = View.Details
                .GridLines = True
                .FullRowSelect = True
                .Columns.Add("Inclusives Dates From", 150, HorizontalAlignment.Left)
                .Columns.Add("Inclusives To", 150, HorizontalAlignment.Left)
                .Columns.Add("Name of Company", 250, HorizontalAlignment.Left)
                .Columns.Add("Position/Title", 150, HorizontalAlignment.Left)
                .Columns.Add("Job Description", 400, HorizontalAlignment.Left)
                .Columns.Add("Employment Status", 150, HorizontalAlignment.Left)
                .Columns.Add("Immediate Superior", 150, HorizontalAlignment.Left)
                .Columns.Add("Reason/s for Leaving", 400, HorizontalAlignment.Left)
                .Columns.Add("Telephone No", 100, HorizontalAlignment.Left)

                Dim myreader As SqlDataReader
                myreader = cmd.ExecuteReader
                Try
                    While myreader.Read
                        With .Items.Add(myreader.Item(0))
                            .SubItems.Add(myreader.Item(1))
                            .subItems.Add(myreader.Item(2))
                            .subItems.Add(myreader.Item(3))
                            .subItems.Add(myreader.Item(4))
                            .subItems.Add(myreader.Item(5))
                            .subItems.Add(myreader.Item(6))
                            .subItems.Add(myreader.Item(7))
                            .subItems.Add(myreader.Item(8))
                            .subItems.Add(myreader.Item(9))
                        End With
                    End While
                Finally
                    myreader.Close()
                    myconnection.sqlconn.Close()
                End Try
                'If (.Items(0).Text) = "" And .Items(0).SubItems(1).Text = "" And .Items(0).SubItems(2).Text = "" And .Items(0).SubItems(3).Text = "" And .Items(0).SubItems(4).Text = "" AndAlso
                '    .Items(0).SubItems(5).Text = "" And .Items(0).SubItems(6).Text = "" Then
                '    btnUpdateGE.Enabled = False
                'Else
                '    btnUpdateGE.Enabled = True
                'End If
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Employment History List")
        End Try
    End Sub
#End Region

    Private Sub btnNewEH_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewEH.Click
        frmMember_EmploymentHistory.lblEmploymentHistory.Text = "Add New Employment History"
        frmMember_EmploymentHistory.btnupdate.Visible = False
        frmMember_EmploymentHistory.ShowDialog()
    End Sub

    Private Sub btnUpdateEH_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateEH.Click
        Try
            If lvlEmploymentHistory.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Data to be Edit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                With frmMember_EmploymentHistory
                    .lblEmploymentHistory.Text = "Edit History"
                    .btnSave.Visible = False
                    .btnupdate.Location = New System.Drawing.Point(5, 5)
                    .dtIncDateFrom.Text = Me.lvlEmploymentHistory.SelectedItems(0).Text
                    .dtIncDateTo.Text = Me.lvlEmploymentHistory.SelectedItems(0).SubItems(1).Text
                    .txtCompName.Text = Me.lvlEmploymentHistory.SelectedItems(0).SubItems(2).Text
                    .txtPosition.Text = Me.lvlEmploymentHistory.SelectedItems(0).SubItems(3).Text
                    .txtJobDesc.Text = Me.lvlEmploymentHistory.SelectedItems(0).SubItems(4).Text
                    .txtEmpStat.Text = Me.lvlEmploymentHistory.SelectedItems(0).SubItems(5).Text
                    .txtSuperior.Text = Me.lvlEmploymentHistory.SelectedItems(0).SubItems(6).Text
                    .txtReasons.Text = Me.lvlEmploymentHistory.SelectedItems(0).SubItems(7).Text
                    .txtTelNo.Text = Me.lvlEmploymentHistory.SelectedItems(0).SubItems(8).Text
                    .getpkEmployment() = Me.lvlEmploymentHistory.SelectedItems(0).SubItems(9).Text
                    .ShowDialog()
                End With
                'Else
                'MessageBox.Show("Nothing to Update", "Medical Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Update Employment History")
        End Try
    End Sub

    Private Sub DeleteEmployment(ByVal EmpHis As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_EmploymentHistory_Delete", _
                                      New SqlParameter("@pk_EmploymentHistory", EmpHis))
            trans.Commit()

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete Employment")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnDeleteEH_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteEH.Click
        Try
            Dim x As New DialogResult
            Dim pkId As String = Me.lvlEmploymentHistory.SelectedItems(0).SubItems(9).Text
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If x = System.Windows.Forms.DialogResult.Yes Then
                If pkId <> "" Then
                    Call DeleteEmployment(pkId)
                    Call GetmemberEmployment(Me.txtEmployeeNo.Text)
                End If
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Organizations/Affiliations"

#Region "Get Employee Organization"
    Public Sub GetmemberEmpOrg(ByVal empinfo As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("MSS_MembersInfo_GetEmpOrg", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            myconnection.sqlconn.Open()
            cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
            With Me.lvlOrg
                .Clear()
                .View = View.Details
                .GridLines = True
                .FullRowSelect = True
                .Columns.Add("Name", 317, HorizontalAlignment.Left)
                .Columns.Add("Address", 300, HorizontalAlignment.Left)
                .Columns.Add("Position", 150, HorizontalAlignment.Left)
                .Columns.Add("Period", 150, HorizontalAlignment.Left)

                Dim myreader As SqlDataReader
                myreader = cmd.ExecuteReader
                Try
                    While myreader.Read
                        With .Items.Add(myreader.Item(0))
                            .SubItems.Add(myreader.Item(1))
                            .subItems.Add(myreader.Item(2))
                            .subItems.Add(myreader.Item(3))
                            .subItems.Add(myreader.Item(4))
                        End With
                    End While
                Finally
                    myreader.Close()
                    myconnection.sqlconn.Close()
                End Try
                'If (.Items(0).Text) = "" And .Items(0).SubItems(1).Text = "" And .Items(0).SubItems(2).Text = "" And .Items(0).SubItems(3).Text = "" Then
                '    btnUpdateGE.Enabled = False
                'Else
                '    btnUpdateGE.Enabled = True
                'End If
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Employment History List")
        End Try
    End Sub
#End Region

    Private Sub btnNewOrg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewOrg.Click
        frmMember_Organizations.lblOrg.Text = "Add New Organizations"
        frmMember_Organizations.btnupdate.Visible = False
        frmMember_Organizations.ShowDialog()
    End Sub

    Private Sub btnUpdateOrg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateOrg.Click
        Try
            If lvlOrg.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Data to be Edit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                With frmMember_Organizations
                    .lblOrg.Text = "Edit Organization"
                    .btnSave.Visible = False
                    .btnupdate.Location = New System.Drawing.Point(5, 5)
                    .txtName.Text = Me.lvlOrg.SelectedItems(0).Text
                    .txtAdd.Text = Me.lvlOrg.SelectedItems(0).SubItems(1).Text
                    .txtPosition.Text = Me.lvlOrg.SelectedItems(0).SubItems(2).Text
                    .txtPeriod.Text = Me.lvlOrg.SelectedItems(0).SubItems(3).Text
                    .getpkEmpOrg() = Me.lvlOrg.SelectedItems(0).SubItems(4).Text
                    .ShowDialog()
                End With
                'Else
                'MessageBox.Show("Nothing to Update", "Medical Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Update Employee Organization")
        End Try
    End Sub

    Private Sub DeleteEmpOrg(ByVal EmpOrg As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_EmpOrg_Delete", _
                                      New SqlParameter("@pk_EmpOrg", EmpOrg))
            trans.Commit()

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete Organization")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnDeleteOrg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteOrg.Click
        Try
            Dim x As New DialogResult
            Dim pkId As String = Me.lvlOrg.SelectedItems(0).SubItems(4).Text
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If x = System.Windows.Forms.DialogResult.Yes Then
                If pkId <> "" Then
                    Call DeleteEmpOrg(pkId)
                    Call GetmemberEmpOrg(Me.txtEmployeeNo.Text)
                End If
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Legal Cases"

#Region "Get Employee Legal Case"
    Public Sub GetmemberLegalCase(ByVal empinfo As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("MSS_MembersInfo_GetLegalCase", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            myconnection.sqlconn.Open()
            cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
            With Me.lvlLegal
                .Clear()
                .View = View.Details
                .GridLines = True
                .FullRowSelect = True
                .Columns.Add("Reference No", 100, HorizontalAlignment.Left)
                .Columns.Add("Details", 300, HorizontalAlignment.Left)
                .Columns.Add("Date", 100, HorizontalAlignment.Left)
                .Columns.Add("Court Filed", 300, HorizontalAlignment.Left)
                .Columns.Add("Prosecutor", 250, HorizontalAlignment.Left)
                .Columns.Add("Hearing Date", 100, HorizontalAlignment.Left)
                .Columns.Add("Case Status", 150, HorizontalAlignment.Left)
                .Columns.Add("Attached File Name", 200, HorizontalAlignment.Left)

                Dim myreader As SqlDataReader
                myreader = cmd.ExecuteReader
                Try
                    While myreader.Read
                        With .Items.Add(myreader.Item(0))
                            .SubItems.Add(myreader.Item(1))
                            .subItems.Add(myreader.Item(2))
                            .subItems.Add(myreader.Item(3))
                            .subItems.Add(myreader.Item(4))
                            .subItems.Add(myreader.Item(5))
                            .subItems.Add(myreader.Item(6))
                            .subItems.Add(myreader.Item(7))
                            .subItems.Add(myreader.Item(8))
                        End With
                    End While
                Finally
                    myreader.Close()
                    myconnection.sqlconn.Close()
                End Try
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Legal Case List")
        End Try
    End Sub
#End Region

    Private Sub btnNewLC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewLC.Click
        frmMember_Legal.lblLegal.Text = "Add New Legal Cases"
        frmMember_Legal.btnupdate.Visible = False
        frmMember_Legal.ShowDialog()
    End Sub

    Private Sub btnUpdateLC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateLC.Click
        Try
            If lvlLegal.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Data to be Edit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                With frmMember_Legal
                    .lblLegal.Text = "Edit Legal Case"
                    .btnSave.Visible = False
                    .btnupdate.Location = New System.Drawing.Point(5, 5)
                    .txtRefNo.Text = Me.lvlLegal.SelectedItems(0).Text
                    .txtDetails.Text = Me.lvlLegal.SelectedItems(0).SubItems(1).Text
                    .dtDates.Text = Me.lvlLegal.SelectedItems(0).SubItems(2).Text
                    .txtCourt.Text = Me.lvlLegal.SelectedItems(0).SubItems(3).Text
                    .txtProsec.Text = Me.lvlLegal.SelectedItems(0).SubItems(4).Text
                    .dtHDate.Text = Me.lvlLegal.SelectedItems(0).SubItems(5).Text
                    .txtCaseStat.Text = Me.lvlLegal.SelectedItems(0).SubItems(6).Text
                    .txtAttachFiles.Text = Me.lvlLegal.SelectedItems(0).SubItems(7).Text
                    .getpkLegalCase() = Me.lvlLegal.SelectedItems(0).SubItems(8).Text
                    .ShowDialog()
                End With
                'Else
                'MessageBox.Show("Nothing to Update", "Medical Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Update Employee Legal Case")
        End Try
    End Sub

    Private Sub DeleteLegalCase(ByVal LegalCase As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_LegalCase_Delete", _
                                      New SqlParameter("@pk_LegalCase", LegalCase))
            trans.Commit()

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete Legal Case")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnDeleteLC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteLC.Click
        Try
            Dim x As New DialogResult
            Dim pkId As String = Me.lvlLegal.SelectedItems(0).SubItems(8).Text
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If x = System.Windows.Forms.DialogResult.Yes Then
                If pkId <> "" Then
                    Call DeleteLegalCase(pkId)
                    Call GetmemberLegalCase(Me.txtEmployeeNo.Text)
                End If
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnLegalUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLegalUpload.Click
        Try
            If lvlLegal.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Files to Download", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                btnLegalUpload.Enabled = False
                Exit Sub
            Else
                Dim afile As Byte()
                Dim myconnection As New Clsappconfiguration
                Dim cmd As New SqlCommand("MSS_MembersInfo_GetLegal_AttachedFiles", myconnection.sqlconn)
                cmd.CommandType = CommandType.StoredProcedure
                myconnection.sqlconn.Open()
                cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = txtEmployeeNo.Text
                cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 100).Value = Me.lvlLegal.SelectedItems(0).SubItems(7).Text
                SaveFileDialog1.Filter = "Text Files (*.txt)|*.txt|PDF Files (*.pdf)|*.pdf|Word Documents (*.docx)|*.docx|Excel Worksheets (*.xlsx)|*.xlsx|PowerPoint Presentations (*.pptx)|*.pptx|Word Documents 97-2003 (*.doc)|*.docx|Excel Worksheets 93-2003 (*.xls)|*.xls|PowerPoint Presentations (*.ppt)|*.ppt" & "|Office Files|*.docx;*.xlsx;*.pptx;*.doc;*.xls;*.ppt" & "|Images|*.jpg;*.png;*.gif"
                SaveFileDialog1.Title = "Upload Files"
                Using myreader As SqlDataReader = cmd.ExecuteReader
                    myreader.Read()
                    afile = DirectCast(myreader("fcAttachedFiles"), Byte())
                End Using
                If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
                    Dim FiletoSave As String = SaveFileDialog1.FileName
                    Dim Stream As FileStream = New FileStream(FiletoSave, FileMode.Create, FileAccess.Write)
                    Stream.Write(afile, 0, afile.Length)
                    Stream.Close()
                    btnLegalUpload.Enabled = False
                    MessageBox.Show("File Successfully Uploaded!", "Uploaded", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    SaveFileDialog1.FileName = ""
                ElseIf DialogResult.Cancel Then
                    btnLegalUpload.Enabled = False
                    MessageBox.Show("User Cancelled!", "Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                myconnection.sqlconn.Close()
                btnLegalUpload.Enabled = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Legal Case File Download", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub lvlLegal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvlLegal.Click
        If btnemp_edit.Text = "Update" Then
            btnLegalUpload.Enabled = False
        Else
            If lvlLegal.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Files to Download", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                btnLegalUpload.Enabled = False
                Exit Sub
            Else
                btnLegalUpload.Enabled = True
            End If
        End If
    End Sub
#End Region

#Region "Search Address Button"
    Private Sub BTNSEARCHHome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchHome.Click
        frmPreviewAddress.SearchAddress(txtPresBldg.Text)
        frmPreviewAddress.WindowState = FormWindowState.Maximized
        frmPreviewAddress.ShowDialog()
    End Sub

    Private Sub BTNSEARCHProvince_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchProvince.Click
        'frmPreviewAddress.SearchAddress(txtPrevAdd.Text)
        'frmPreviewAddress.WindowState = FormWindowState.Maximized
        'frmPreviewAddress.ShowDialog()
    End Sub

    Private Sub btnPermAddress_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchPermAddress.Click
        'frmPreviewAddress.SearchAddress(txtPermanentAdd.Text)
        'frmPreviewAddress.WindowState = FormWindowState.Maximized
        'frmPreviewAddress.ShowDialog()
    End Sub

    Private Sub btnFSearchAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFSearchAdd.Click
        frmPreviewAddress.SearchAddress(txtFAdd.Text)
        frmPreviewAddress.WindowState = FormWindowState.Maximized
        frmPreviewAddress.ShowDialog()
    End Sub

    Private Sub btnMSearchAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMSearchAdd.Click
        frmPreviewAddress.SearchAddress(txtMAdd.Text)
        frmPreviewAddress.WindowState = FormWindowState.Maximized
        frmPreviewAddress.ShowDialog()
    End Sub

    'Private Sub btnBSearchAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    frmPreviewAddress.SearchAddress(txtBAdd.Text)
    '    frmPreviewAddress.WindowState = FormWindowState.Maximized
    '    frmPreviewAddress.ShowDialog()
    'End Sub

    'Private Sub btnSSearchAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    frmPreviewAddress.SearchAddress(txtSAdd.Text)
    '    frmPreviewAddress.WindowState = FormWindowState.Maximized
    '    frmPreviewAddress.ShowDialog()
    'End Sub

    Private Sub btnSpouseSearchAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpouseSearchAdd.Click
        frmPreviewAddress.SearchAddress(txtSpouseAdd.Text)
        frmPreviewAddress.WindowState = FormWindowState.Maximized
        frmPreviewAddress.ShowDialog()
    End Sub
#End Region

    Private Sub cboemp_civil_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboemp_civil.SelectedIndexChanged
        If btnemp_edit.Text = "Update" Or btnemp_add.Text = "Save" Then
            If cboemp_civil.Text = "Married" Or cboemp_civil.Text = "Live-in" Then
                Me.txtSpouseName.Enabled = True
                Me.txtSpouseAdd.Enabled = True
                Me.txtSpouseContact.Enabled = True
                Me.txtSpouseStay.Enabled = True
                Me.txtOccupation.Enabled = True
                Me.txtChildNo.Enabled = True
                Me.btnSpouseSearchAdd.Enabled = True
                Me.rbSpouseMonth.Enabled = True
                Me.rbSpouseYears.Enabled = True
            Else
                Me.txtSpouseName.Enabled = False
                Me.txtSpouseAdd.Enabled = False
                Me.txtSpouseContact.Enabled = False
                Me.txtSpouseStay.Enabled = False
                Me.txtOccupation.Enabled = False
                Me.txtChildNo.Enabled = False
                Me.btnSpouseSearchAdd.Enabled = False
                Me.rbSpouseMonth.Enabled = False
                Me.rbSpouseYears.Enabled = False
            End If
        End If
    End Sub

#Region "Family Background"
    Private Sub AddEditFamiy()
        Dim gcon As New Clsappconfiguration
        Dim StayLength As String
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gCon.sqlconn.BeginTransaction()
        Try
            If rbSpouseMonth.Checked = True Then
                StayLength = "Month"
            Else
                StayLength = "Year"
            End If
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_FamilyBackground_AddEdit", _
                                     New SqlParameter("@employeeNo", txtEmployeeNo.Text), _
                                     New SqlParameter("@FatherName", txtFatherName.Text), _
                                     New SqlParameter("@FatherAdd", txtFAdd.Text), _
                                     New SqlParameter("@FatherContact", txtFContact.Text), _
                                     New SqlParameter("@MotherName", txtMotherName.Text), _
                                     New SqlParameter("@MotherAdd", txtMAdd.Text), _
                                     New SqlParameter("@MotherContact", txtMContact.Text), _
                                     New SqlParameter("@SpouseName", txtSpouseName.Text), _
                                     New SqlParameter("@SpouseAdd", txtSpouseAdd.Text), _
                                     New SqlParameter("@SpouseContact", txtSpouseContact.Text), _
                                     New SqlParameter("@Occupation", txtOccupation.Text), _
                                     New SqlParameter("@Length", txtSpouseStay.Text), _
                                     New SqlParameter("@Stay", StayLength), _
                                     New SqlParameter("@NoOfChild", txtChildNo.Text),
                                     New SqlParameter("@fcContactPerson", cboContactPerson.Text))
            trans.Commit()
            'MessageBox.Show("Record has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            'MessageBox.Show(ex.Message, "Save Awards")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region

#Region "Outlet"
    Private Sub LoadOutlet()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "MSS_MembersInfo_GetOutlet")
        Try
            While rd.Read
                Me.cboOutlet.Items.Add(rd.Item("Description"))
            End While
            rd.Close()
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Deductions"
#Region "Get Deductions"
    Public Sub GetmemberDecutions(ByVal empinfo As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("MSS_MembersInfo_GetOtherDeduction", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            myconnection.sqlconn.Open()
            cmd.Parameters.Add("@fcEmployeeID", SqlDbType.VarChar, 256).Value = empinfo
            With Me.lvlDeductions
                .Clear()
                .View = View.Details
                .GridLines = True
                .FullRowSelect = True
                .Columns.Add("Other Deductions", 350, HorizontalAlignment.Left)
                .Columns.Add("Code", 150, HorizontalAlignment.Left)
                .Columns.Add("Amount", 150, HorizontalAlignment.Left)
                .Columns.Add("Type", 261, HorizontalAlignment.Left)

                Dim myreader As SqlDataReader
                myreader = cmd.ExecuteReader
                Try
                    While myreader.Read
                        With .Items.Add(myreader.Item(0))
                            .SubItems.Add(myreader.Item(1))
                            .subItems.Add(myreader.Item(2))
                            '.subItems.Add(myreader.Item(3))
                            '.subItems.Add(myreader.Item(4))
                            '.subItems.Add(myreader.Item(5))
                            '.subItems.Add(myreader.Item(6))
                            '.subItems.Add(myreader.Item(7))
                        End With
                    End While
                Finally
                    myreader.Close()
                    myconnection.sqlconn.Close()
                End Try
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Deductions List")
        End Try
    End Sub
#End Region
#End Region

#Region "13th Month Pay"
#Region "Get 13th Month Pay"
    Public Sub GetmemberMonthPay(ByVal empinfo As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("MSS_MembersInfo_Get13thMonthPay", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            myconnection.sqlconn.Open()
            cmd.Parameters.Add("@fcEmployeeID", SqlDbType.VarChar, 256).Value = empinfo
            With Me.lvl13thMonth
                .Clear()
                .View = View.Details
                .GridLines = True
                .FullRowSelect = True
                .Columns.Add("Period", 300, HorizontalAlignment.Left)
                .Columns.Add("Amount", 150, HorizontalAlignment.Left)
                .Columns.Add("Type", 153, HorizontalAlignment.Left)
                .Columns.Add("Released Date", 150, HorizontalAlignment.Left)
                .Columns.Add("Remarks", 158, HorizontalAlignment.Left)

                Dim myreader As SqlDataReader
                myreader = cmd.ExecuteReader
                Try
                    While myreader.Read
                        With .Items.Add(myreader.Item(0))
                            .SubItems.Add(myreader.Item(1))
                            '.subItems.Add(myreader.Item(2))
                            '.subItems.Add(myreader.Item(3))
                            '.subItems.Add(myreader.Item(4))
                            '.subItems.Add(myreader.Item(5))
                            '.subItems.Add(myreader.Item(6))
                            '.subItems.Add(myreader.Item(7))
                        End With
                    End While
                Finally
                    myreader.Close()
                    myconnection.sqlconn.Close()
                End Try
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Deductions List")
        End Try
    End Sub
#End Region
#End Region

#Region "Validate Email"
    Private Sub temp_email_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtEmailAddress.Validating
        Call validateEmail(Me.txtEmailAddress.Text)
    End Sub

    Private Sub txtemailaddress2_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtemailaddress2.Validating
        Call validateEmail(Me.txtemailaddress2.Text)
    End Sub

    Private Sub txtFacebook1_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtFacebook1.Validating
        Call validateEmail(Me.txtFacebook1.Text)
    End Sub

    Private Sub txtFacebook2_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtFacebook2.Validating
        Call validateEmail(Me.txtFacebook2.Text)
    End Sub
#End Region

#Region "Contract History"
    Dim ContractDept As String
    Dim ContractOrg As String
    Dim ContractHistoryComp As String
    Dim ContractComp As String
    Dim Contractcompany As String

#Region "Get Employee Contract History"
    Public Sub GetmemberContractHistory(ByVal empinfo As String)
        Try
            Dim ContractComp As String
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("MSS_MembersInfo_GetContractHistory", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            myconnection.sqlconn.Open()
            cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
            cmd.Parameters.Add("@Client", SqlDbType.VarChar, 50).Value = MemberMngr
            With Me.lvlContractHis
                .Clear()
                .View = View.Details
                .GridLines = True
                .FullRowSelect = True
                .Columns.Add("Previous Company", 450, HorizontalAlignment.Left)
                .Columns.Add("Previous Position", 300, HorizontalAlignment.Left)
                .Columns.Add("Previous Basic Pay", 150, HorizontalAlignment.Left)
                '.Columns.Add("Previous Contract Price", 150, HorizontalAlignment.Left)
                .Columns.Add("Previous Ecola", 150, HorizontalAlignment.Left)
                .Columns.Add("Contract Start", 150, HorizontalAlignment.Left)
                .Columns.Add("Contract End", 150, HorizontalAlignment.Left)
                .Columns.Add("Approver", 150, HorizontalAlignment.Left)

                Dim myreader As SqlDataReader
                myreader = cmd.ExecuteReader
                Try
                    While myreader.Read
                        ContractComp = myreader.Item(0)

                        getorgchartIDdepartment(ContractComp)

                        With .Items.Add(ContractDept)
                            .SubItems.Add(myreader.Item(1))
                            .SubItems.Add(myreader.Item(2))
                            '.SubItems.Add(myreader.Item(3))
                            .SubItems.Add(myreader.Item(4))
                            .SubItems.Add(myreader.Item(5))
                            .SubItems.Add(myreader.Item(6))
                            .SubItems.Add(myreader.Item(7))
                        End With
                    End While

                Finally
                    myreader.Close()
                    myconnection.sqlconn.Close()
                End Try
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Contract History List")
        End Try
    End Sub
#End Region

    Private Sub SaveContractHistory()

        Dim PrevDate As Date
            Dim gcon As New Clsappconfiguration
            gcon.sqlconn.Open()
            Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            getContract_Current()
            Select Case MemberMngr
                Case "INTERNAL EMPLOYEE"
                    If txtorgchart.Text <> ContractComp Or cbotitledesignation.Text <> pos Or txtbasicpay.Text <> Salary Or txtEcola.Text <> Ecola Then
                        If txtorgchart.Text <> ContractComp Or cbotitledesignation.Text <> pos Then
                            PrevDate = Date.Now.AddDays(-1)
                            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_ContractHistory_Add", _
                                                     New SqlParameter("@employeeNo", txtEmployeeNo.Text), _
                                                     New SqlParameter("@Company", txtorgchart.Tag), _
                                                     New SqlParameter("@Position", cbotitledesignation.Text), _
                                                     New SqlParameter("@Contract", txtContractrate.Text), _
                                                     New SqlParameter("@DateStart", Date.Now), _
                                                     New SqlParameter("@DateEnd", PrevDate), _
                                                     New SqlParameter("@Salary", txtbasicpay.Text), _
                                                     New SqlParameter("@ContractPrice", txtContractPrice.Text), _
                                                     New SqlParameter("@Ecola", txtEcola.Text), _
                                                     New SqlParameter("@Approver", frmLogin.Username.Text))
                            trans.Commit()
                            Call GetmemberContractHistory(Me.txtEmployeeNo.Text)
                        Else
                            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_ContractHistory_Add", _
                                                     New SqlParameter("@employeeNo", txtEmployeeNo.Text), _
                                                     New SqlParameter("@Company", txtorgchart.Tag), _
                                                     New SqlParameter("@Position", cbotitledesignation.Text), _
                                                     New SqlParameter("@Contract", txtContractrate.Text), _
                                                     New SqlParameter("@DateStart", Date.Now), _
                                                     New SqlParameter("@DateEnd", ContractEnd), _
                                                     New SqlParameter("@Salary", txtbasicpay.Text), _
                                                     New SqlParameter("@ContractPrice", txtContractPrice.Text), _
                                                     New SqlParameter("@Ecola", txtEcola.Text), _
                                                     New SqlParameter("@Approver", frmLogin.Username.Text))
                            trans.Commit()
                            Call GetmemberContractHistory(Me.txtEmployeeNo.Text)
                        End If
                    End If

                Case "PROJECT BASE"
                    If txtorgchart.Text <> ContractComp Or txtContractrate.Text <> CurrentRate Or cbotitledesignation.Text <> pos Then
                        If txtorgchart.Text <> ContractComp Then
                            UpdateContractEnd()
                            LoadContractEnd()
                            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_ContractHistory_Add", _
                                                     New SqlParameter("@employeeNo", txtEmployeeNo.Text), _
                                                     New SqlParameter("@Company", txtorgchart.Tag), _
                                                     New SqlParameter("@Position", cbotitledesignation.Text), _
                                                     New SqlParameter("@Contract", txtContractrate.Text), _
                                                     New SqlParameter("@DateStart", Date.Now), _
                                                     New SqlParameter("@DateEnd", ContractEnd), _
                                                     New SqlParameter("@Salary", txtbasicpay.Text), _
                                                     New SqlParameter("@ContractPrice", txtContractPrice.Text), _
                                                     New SqlParameter("@Ecola", txtEcola.Text), _
                                                     New SqlParameter("@Approver", frmLogin.Username.Text))
                            trans.Commit()
                            Call GetmemberContractHistory(Me.txtEmployeeNo.Text)
                        Else
                            If txtContractrate.Text <> "" Then
                                UpdateContractEnd()
                                LoadContractEnd()
                                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_ContractHistory_Add", _
                                                         New SqlParameter("@employeeNo", txtEmployeeNo.Text), _
                                                         New SqlParameter("@Company", txtorgchart.Tag), _
                                                         New SqlParameter("@Position", cbotitledesignation.Text), _
                                                         New SqlParameter("@Contract", txtContractrate.Text), _
                                                         New SqlParameter("@DateStart", Date.Now), _
                                                         New SqlParameter("@DateEnd", ContractEnd), _
                                                         New SqlParameter("@Salary", txtbasicpay.Text), _
                                                         New SqlParameter("@ContractPrice", txtContractPrice.Text), _
                                                         New SqlParameter("@Ecola", txtEcola.Text), _
                                                         New SqlParameter("@Approver", frmLogin.Username.Text))
                                trans.Commit()
                                Call GetmemberContractHistory(Me.txtEmployeeNo.Text)
                            Else
                                'MessageBox.Show("No Contract Set for Saving History", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            End If
                        End If
                    End If
            End Select

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Contract History")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub LoadContractEnd()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetContract_EndDate", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@ContractCode", SqlDbType.VarChar, 50).Value = Me.txtContractrate.Text
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                ContractEnd = myreader.Item("fdContractEnd")
            End While
            myreader.Close()
        Catch ex As Exception

        End Try
    End Sub

    Public Sub getorgchartIDdepartment(ByVal orgid As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetContractCompany", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@department", SqlDbType.VarChar, 20).Value = orgid
        End With
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                ContractDept = myreader.Item(0)
            End While
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub

    Private Sub UpdateContractEnd()

        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_ContractDateEnd_Edit", _
                                     New SqlParameter("@employeeNo", txtEmployeeNo.Text), _
                                     New SqlParameter("@DateEnd", Date.Now))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Contract History")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Public Sub getContractHistoryComp()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetContractEndHistory", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = txtEmployeeNo.Text
        End With
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                Contractcompany = myreader.Item(0)
            End While
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub

    Public Sub getContractorgchartvalue()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_orgchart_get", myconnection.sqlconn)
        With cmd.Parameters
            .Add("@getid", SqlDbType.VarChar, 10).Value = Contractcompany
        End With
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                ContractComp = myreader.Item(0)
            End While
            myreader.Close()
            'Me.txtdescdivision.Text = Mid(x, 2, 32)
            'Me.txtdescdepartment.Text = Mid(x, 35, 24)
            'Me.txtdescsection.Text = Mid(x, 60, 5)
        Finally
            myconnection.sqlconn.Close()
        End Try
    End Sub

    Public Sub getContract_Current()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetContractCurrent", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = txtEmployeeNo.Text
            .Add("@Client", SqlDbType.VarChar, 256).Value = Cbomem_Status.Text
        End With
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If MemberMngr = "INTERNAL EMPLOYEE" Then
                While myreader.Read
                    pos = myreader.Item(0)
                    Salary = myreader.Item(1)
                    Ecola = myreader.Item(2)
                End While
            ElseIf MemberMngr = "PROJECT BASE" Then
                While myreader.Read
                    pos = myreader.Item(0)
                    CurrentRate = myreader.Item(1)
                End While
            End If
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub

    Private Sub getEmploymentStatus()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetEmployementStatus", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = txtEmployeeNo.Text
        End With
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                MemberMngr = myreader.Item(0)
            End While
            myreader.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Contract History")
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region

    Private Sub btnContractRate_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContractRate.Click
        Dim frm As New frmContractRate
        frm.ShowDialog()
    End Sub

#Region "Address"
    Private Sub AddEdit_Address()
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Member_Address_AddEdit",
                                    New SqlParameter("@employeeNo", txtEmployeeNo.Text),
                                    New SqlParameter("@PresBldg", txtPresBldg.Text),
                                    New SqlParameter("@PresStreet", txtPresStreet.Text),
                                    New SqlParameter("@PresSitio", txtPresSitio.Text),
                                    New SqlParameter("@PresCity", txtPresCity.Text),
                                    New SqlParameter("@PresProvince", txtPresProvince.Text),
                                    New SqlParameter("@PrevBldg", txtPrevBldg.Text),
                                    New SqlParameter("@PrevStreet", txtPrevStreet.Text),
                                    New SqlParameter("@PrevSitio", txtPrevSitio.Text),
                                    New SqlParameter("@PrevCity", txtPrevCity.Text),
                                    New SqlParameter("@PrevProvince", txtPrevProvince.Text),
                                    New SqlParameter("@PermBldg", txtPermBldg.Text),
                                    New SqlParameter("@PermStreet", txtPermStreet.Text),
                                    New SqlParameter("@PermSitio", txtPermSitio.Text),
                                    New SqlParameter("@PermCity", txtPermCity.Text),
                                    New SqlParameter("@PermProvince", txtPermProvince.Text))
            'Dim x As New DialogResult
            'x = MessageBox.Show("Records successfully updated!", "Updating Record...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            'Throw ex
            MessageBox.Show(ex.Message, "Add Edit Address", MessageBoxButtons.OK)
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region

    Private Sub ClientorEmployee()
        If rbEmployee.Checked = True Then
            txtbasicpay.ReadOnly = False
            txtEcola.ReadOnly = False
            btnContractRate.Enabled = False
        ElseIf rbClient.Checked = True Then
            txtbasicpay.ReadOnly = True
            txtEcola.ReadOnly = True
            btnContractRate.Enabled = True
        End If
    End Sub

#Region "Attached Requirement"

    Dim Reqpath As String
    Dim Reqfilenym As String
    Dim ReqfileData As Byte()
    Dim ex As String
    Dim Reqfiles As String

#Region "Get member Attach Requirement"
    Public Sub GetmemberAttachReq(ByVal empinfo As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetReqAttach", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
        With Me.lvlAttachment
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Requirement", 250, HorizontalAlignment.Left)
            .Columns.Add("Attachment", 300, HorizontalAlignment.Left)
            .Columns.Add("Receive By", 250, HorizontalAlignment.Left)
            .Columns.Add("Date Receive", 100, HorizontalAlignment.Left)

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .SubItems.Add(myreader.Item(1))
                        .SubItems.Add(myreader.Item(2))
                        .SubItems.Add(myreader.Item(3))
                        .SubItems.Add(myreader.Item(4))
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
#End Region

    Private Sub btnAttach_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAttach.Click
        frmMember_Requirements.ShowDialog()
    End Sub

    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Try
            If lvlAttachment.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Files to Download", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                btnUpload.Enabled = False
                Exit Sub
            Else
                Dim MyGuid As Guid = New Guid(Me.lvlAttachment.SelectedItems(0).SubItems(4).Text)
                Dim afile As Byte()
                Dim myconnection As New Clsappconfiguration
                Dim cmd As New SqlCommand("MSS_MembersInfo_GetReqAttach_AttachedFiles", myconnection.sqlconn)
                cmd.CommandType = CommandType.StoredProcedure
                myconnection.sqlconn.Open()
                cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = txtEmployeeNo.Text
                cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 100).Value = Me.lvlAttachment.SelectedItems(0).SubItems(1).Text
                cmd.Parameters.Add("@pk_Attach", SqlDbType.UniqueIdentifier).Value = MyGuid
                SaveFileDialog1.Filter = "Text Files (*.txt)|*.txt|PDF Files (*.pdf)|*.pdf|Word Documents (*.docx)|*.docx|Excel Worksheets (*.xlsx)|*.xlsx|PowerPoint Presentations (*.pptx)|*.pptx|Word Documents 97-2003 (*.doc)|*.docx|Excel Worksheets 93-2003 (*.xls)|*.xls|PowerPoint Presentations (*.ppt)|*.ppt" & "|Office Files|*.docx;*.xlsx;*.pptx;*.doc;*.xls;*.ppt" & "|Images|*.jpg;*.png;*.gif"
                SaveFileDialog1.Title = "Upload Files"
                Using myreader As SqlDataReader = cmd.ExecuteReader
                    myreader.Read()
                    afile = DirectCast(myreader("fcAttach"), Byte())
                End Using
                If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
                    Dim FiletoSave As String = SaveFileDialog1.FileName
                    Dim Stream As FileStream = New FileStream(FiletoSave, FileMode.Create, FileAccess.Write)
                    Stream.Write(afile, 0, afile.Length)
                    Stream.Close()
                    btnUpload.Enabled = False
                    MessageBox.Show("File Successfully Uploaded!", "Uploaded", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    SaveFileDialog1.FileName = ""
                ElseIf DialogResult.Cancel Then
                    btnUpload.Enabled = False
                    MessageBox.Show("User Cancelled!", "Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                myconnection.sqlconn.Close()
                btnUpload.Enabled = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Requirement Download File", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub lvlAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvlAttachment.Click
        If btnemp_edit.Text = "Update" Then
            btnUpload.Enabled = False
        Else
            If lvlAttachment.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Files to Download", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                btnUpload.Enabled = False
                Exit Sub
            Else
                btnUpload.Enabled = True
            End If
        End If
    End Sub
#End Region

#Region "Hide TabPage"
    Public Sub TabPageforEmployee()
        With tabMember
            '.TabPages.Remove(Contribution)
            '.TabPages.Remove(OtherDeductions)
            '.TabPages.Remove(MonthPay)
            '.TabPages.Remove(InfoHistory)
            '.TabPages.Remove(ContractHistory)
            '.TabPages.Remove(PayrollHis)
            '.TabPages.Remove(LeaveLedger)
        End With
    End Sub

    Public Sub TabPageforClient()
        With tabMember
            .TabPages.Remove(Contribution)
            .TabPages.Remove(OtherDeductions)
            .TabPages.Remove(MonthPay)
            '.TabPages.Remove(InfoHistory)
            '.TabPages.Remove(ContractHistory)
            .TabPages.Remove(PayrollHis)
            .TabPages.Remove(LeaveLedger)
        End With
    End Sub

    Public Sub TabPageforApplicant()
        With tabMember
            .TabPages.Remove(Contribution)
            .TabPages.Remove(OtherDeductions)
            .TabPages.Remove(MonthPay)
            '.TabPages.Remove(InfoHistory)
            .TabPages.Remove(ContractHistory)
            .TabPages.Remove(PayrollHis)
            .TabPages.Remove(LeaveLedger)
        End With
    End Sub
#End Region

#Region "Tab Button Disabled/enabled"
#Region "Disable HRIS Button"
    Public Sub DisableHRISButton()
        With Me
            .btnadd_dep.Enabled = False
            .btnemp_editdepdts.Enabled = False
            .btndelete_dep.Enabled = False
            .btnaddContact.Enabled = False
            .BtnEditContact.Enabled = False
            .BtnDeleteContact.Enabled = False
            .btnNewEI.Enabled = False
            .btnUpdateEI.Enabled = False
            .btnDeleteEI.Enabled = False
            .btnNewEH.Enabled = False
            .btnUpdateEH.Enabled = False
            .btnDeleteEH.Enabled = False
            .btnNewT.Enabled = False
            .btnUpdateT.Enabled = False
            .btnDeleteT.Enabled = False
            .btnNewGE.Enabled = False
            .btnUpdateGE.Enabled = False
            .btnDeleteGE.Enabled = False
            .btnNewSkills.Enabled = False
            .btnUpdateSkills.Enabled = False
            .btnDeleteSkills.Enabled = False
            .btnNewOrg.Enabled = False
            .btnUpdateOrg.Enabled = False
            .btnDeleteOrg.Enabled = False
            .btnNewMed.Enabled = False
            .btnUpdateMed.Enabled = False
            .btnDeleteMed.Enabled = False
            .btnNewBA.Enabled = False
            .btnUpdateBA.Enabled = False
            .btnDeleteBA.Enabled = False
            .btnsaveSInc.Enabled = False
            .btnUpdateSInc.Enabled = False
            .btnDeleteSInc.Enabled = False
            .btnNewDisc.Enabled = False
            .btnUpdateDisc.Enabled = False
            .btnDeleteDisc.Enabled = False
            .btnNewPE.Enabled = False
            .btnUpdatePE.Enabled = False
            .btnDeletePE.Enabled = False
            .btnNewLC.Enabled = False
            .btnUpdateLC.Enabled = False
            .btnDeleteLC.Enabled = False
            .btnNewJD.Enabled = False
            .btnUpdateJD.Enabled = False
            .btnDeleteJD.Enabled = False
            .btnNewAward.Enabled = False
            .btnUpdateAward.Enabled = False
            .btnDeleteAward.Enabled = False
            .btnAttach.Enabled = False
            .btnNewSibling.Enabled = False
            .btnUpdateSibling.Enabled = False
            .btnTDownload.Enabled = False
            .btnMedUpload.Enabled = False
            .btnDDownload.Enabled = False
            .btnPEDownload.Enabled = False
            .btnLegalUpload.Enabled = False
        End With
    End Sub
#End Region

#Region "Enable HRIS Button"
    Private Sub EnableHRISButton()
        With Me
            .btnadd_dep.Enabled = True
            .btnemp_editdepdts.Enabled = True
            .btndelete_dep.Enabled = True
            .btnaddContact.Enabled = True
            .BtnEditContact.Enabled = True
            .BtnDeleteContact.Enabled = True
            .btnNewEI.Enabled = True
            .btnUpdateEI.Enabled = True
            .btnDeleteEI.Enabled = True
            .btnNewEH.Enabled = True
            .btnUpdateEH.Enabled = True
            .btnDeleteEH.Enabled = True
            .btnNewT.Enabled = True
            .btnUpdateT.Enabled = True
            .btnDeleteT.Enabled = True
            .btnNewGE.Enabled = True
            .btnUpdateGE.Enabled = True
            .btnDeleteGE.Enabled = True
            .btnNewSkills.Enabled = True
            .btnUpdateSkills.Enabled = True
            .btnDeleteSkills.Enabled = True
            .btnNewOrg.Enabled = True
            .btnUpdateOrg.Enabled = True
            .btnDeleteOrg.Enabled = True
            .btnNewMed.Enabled = True
            .btnUpdateMed.Enabled = True
            .btnDeleteMed.Enabled = True
            .btnNewBA.Enabled = True
            .btnUpdateBA.Enabled = True
            .btnDeleteBA.Enabled = True
            .btnsaveSInc.Enabled = True
            .btnUpdateSInc.Enabled = True
            .btnDeleteSInc.Enabled = True
            .btnNewDisc.Enabled = True
            .btnUpdateDisc.Enabled = True
            .btnDeleteDisc.Enabled = True
            .btnNewPE.Enabled = True
            .btnUpdatePE.Enabled = True
            .btnDeletePE.Enabled = True
            .btnNewLC.Enabled = True
            .btnUpdateLC.Enabled = True
            .btnDeleteLC.Enabled = True
            .btnNewJD.Enabled = True
            .btnUpdateJD.Enabled = True
            .btnDeleteJD.Enabled = True
            .btnNewAward.Enabled = True
            .btnUpdateAward.Enabled = True
            .btnDeleteAward.Enabled = True
            .btnAttach.Enabled = True
            .btnNewSibling.Enabled = True
            .btnUpdateSibling.Enabled = True
            '.btnTDownload.Enabled = True
            '.btnMedUpload.Enabled = True
            '.btnDDownload.Enabled = True
            '.btnPEDownload.Enabled = True
            '.btnLegalUpload.Enabled = True
        End With
    End Sub
#End Region

#Region "Disable Upload Button"
    Private Sub DisableUploadButton()
        With Me
            .btnTDownload.Enabled = False
            .btnMedUpload.Enabled = False
            .btnDDownload.Enabled = False
            .btnPEDownload.Enabled = False
            .btnLegalUpload.Enabled = False
        End With
    End Sub
#End Region
#End Region

    Private Sub btnNextTab_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNextTab.Click
        m_CurrentIndex = tabMember.SelectedIndex
        m_CurrentIndex = m_CurrentIndex + 1
        If (m_CurrentIndex < tabMember.TabCount) Then
            tabMember.SelectedIndex = m_CurrentIndex
        End If
    End Sub

    Private Sub btnPrevTab_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrevTab.Click
        m_CurrentIndex = tabMember.SelectedIndex
        m_CurrentIndex = m_CurrentIndex - 1
        If (m_CurrentIndex > -1) Then
            tabMember.SelectedIndex = m_CurrentIndex
        End If
    End Sub

#Region "Sibling"

#Region "Get Employee Sibling"
    Public Sub GetmemberFamilySibling(ByVal empinfo As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("MSS_MembersInfo_GetFamilySibling", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            myconnection.sqlconn.Open()
            cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = empinfo
            With Me.lvlSibling
                .Clear()
                .View = View.Details
                .GridLines = True
                .FullRowSelect = True
                .Columns.Add("Sibling Type", 100, HorizontalAlignment.Left)
                .Columns.Add("Name", 250, HorizontalAlignment.Left)
                .Columns.Add("Address", 350, HorizontalAlignment.Left)
                .Columns.Add("Contact", 140, HorizontalAlignment.Left)

                Dim myreader As SqlDataReader
                myreader = cmd.ExecuteReader
                Try
                    While myreader.Read
                        With .Items.Add(myreader.Item(0))
                            .SubItems.Add(myreader.Item(1))
                            .subItems.Add(myreader.Item(2))
                            .subItems.Add(myreader.Item(3))
                            .subItems.Add(myreader.Item(4))
                        End With
                    End While
                Finally
                    myreader.Close()
                    myconnection.sqlconn.Close()
                End Try
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sibling List")
        End Try
    End Sub
#End Region

    Private Sub btnNewSibling_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewSibling.Click
        frmMember_Brother.lblSibling.Text = "Add Sibling"
        frmMember_Brother.btnupdate.Visible = False
        frmMember_Brother.ShowDialog()
    End Sub

    Private Sub btnUpdateSibling_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateSibling.Click
        Try
            If lvlSibling.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Data to be Edit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            Else
                With frmMember_Brother
                    .lblSibling.Text = "Edit Sibling"
                    .btnSave.Visible = False
                    .btnupdate.Location = New System.Drawing.Point(5, 5)
                    .cboSiblingType.Text = Me.lvlSibling.SelectedItems(0).Text
                    .txtName.Text = Me.lvlSibling.SelectedItems(0).SubItems(1).Text
                    .txtAdd.Text = Me.lvlSibling.SelectedItems(0).SubItems(2).Text
                    .txtContact.Text = Me.lvlSibling.SelectedItems(0).SubItems(3).Text
                    .getpkSibling = Me.lvlSibling.SelectedItems(0).SubItems(4).Text
                    .ShowDialog()
                End With
                Exit Sub
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Update Employee Sibling")
        End Try
    End Sub
#End Region

    Private Sub cboemp_status_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboemp_status.SelectedIndexChanged
        If cboemp_status.Text = "PROJECT BASE" Then
            rbClient.Checked = True
            ClientorEmployee()
        ElseIf cboemp_status.Text = "INTERNAL EMPLOYEE" Then
            rbEmployee.Checked = True
            ClientorEmployee()
        End If
    End Sub

    Private Sub cboemp_status_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboemp_status.Click
        If cboemp_status.Text = "PROJECT BASE" Then
            txtbasicpay.Text = "0.00"
            txtEcola.Text = "0.00"
            txtContractPrice.Text = "0.00"
            txtContractrate.Text = ""
            btnContractRate.Enabled = True
        ElseIf cboemp_status.Text = "INTERNAL EMPLOYEE" Then
            txtbasicpay.Text = "0.00"
            txtEcola.Text = "0.00"
            txtContractPrice.Text = "0.00"
            txtContractrate.Text = ""
            btnContractRate.Enabled = False
        End If
    End Sub
End Class