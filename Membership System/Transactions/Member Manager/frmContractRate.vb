﻿Imports System.Data.SqlClient.SqlConnection
Imports System.Data.SqlClient
Imports System.Drawing
Imports Microsoft.VisualBasic.FileSystem
Imports System.Data
Imports System.IO
Imports Microsoft.VisualBasic
Imports System.Security
Imports System.Security.Principal.WindowsIdentity
Imports Microsoft.ApplicationBlocks.Data
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Web.Security
Imports System.Configuration

Public Class frmContractRate
    Private gCon As New Clsappconfiguration()
    Private Sub frmContractRate_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadRate()
        LoadDepartment()
    End Sub
    Private Sub LoadRate() 'Added by Kenneth
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(gCon.cnstring, "Payroll_ContractRate_List",
                                      New SqlParameter("@Filter", txtFiter.Text))
        dgvContractRate.DataSource = ds.Tables(0)
        With dgvContractRate
            .Columns(0).HeaderText = "Contract Code"
            .Columns(0).Width = 275
            .Columns(1).HeaderText = "Basic Pay"
            .Columns(1).Width = 100
            .Columns(2).HeaderText = "Contract Price"
            .Columns(2).Width = 100
            .Columns(3).Visible = False
            .Columns(3).HeaderText = "Ecola"
            .Columns(3).Width = 85

        End With
    End Sub
    Public Sub LoadDepartment() 'Added by Kenneth
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gCon.cnstring, "Payroll_Contract_Departments_List")
        While rd.Read
            cbocompany.Items.Add(rd(0))
        End While

        'Dim ds As New DataSet
        'Dim ad As New SqlDataAdapter
        'Dim cmd As New SqlCommand("Payroll_Contract_Departments_List", gCon.sqlconn)
        'cmd.CommandType = CommandType.StoredProcedure

        'Try
        '    ad.SelectCommand = cmd
        '    ad.Fill(ds, "tblOrgChart")
        '    With cbocompany
        '        .DisplayMember = "fcDepartmentName"
        '        .DataSource = ds.Tables(0)
        '        .Text = "Select"
        '    End With
        '    gCon.sqlconn.Close()
        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub FilterByCompany() 'Added by Kenneth
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(gCon.cnstring, "Payroll_ContractRate_ListByCompany",
                                      New SqlParameter("@chrCompany", cbocompany.Text))
        dgvContractRate.DataSource = ds.Tables(0)
        With dgvContractRate
            .Columns(0).HeaderText = "Contract Code"
            .Columns(0).Width = 275
            .Columns(1).HeaderText = "Basic Pay"
            .Columns(1).Width = 100
            .Columns(2).HeaderText = "Contract Price"
            .Columns(2).Width = 100
            .Columns(3).Visible = False
            .Columns(3).HeaderText = "Ecola"
            .Columns(3).Width = 93
        End With
    End Sub
    Private Sub FilterByBranch() 'Added by Kenneth
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(gCon.cnstring, "Payroll_ContractRate_ListByBranch",
                                      New SqlParameter("@chrCompany", cbocompany.Text),
                                      New SqlParameter("@chrBranch", cboBranch.Text))
        dgvContractRate.DataSource = ds.Tables(0)
    End Sub
    Private Sub FilterByLocation() 'Added by Kenneth
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(gCon.cnstring, "Payroll_ContractRate_ListByLocation",
                                      New SqlParameter("@chrCompany", cbocompany.Text),
                                      New SqlParameter("@chrBranch", cboBranch.Text),
                                      New SqlParameter("@chrLocation", cboDepartment.Text))
        dgvContractRate.DataSource = ds.Tables(0)
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub cbocompany_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles cbocompany.KeyPress
        e.Handled = True
    End Sub

    Private Sub cbocompany_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbocompany.SelectedIndexChanged
        'BranchList()
        FilterByCompany()
        cboBranch.Text = ""
        cboDepartment.Text = ""
    End Sub
    Private Sub BranchList()
        Dim rd As SqlDataReader
        cboBranch.Items.Clear()
        rd = SqlHelper.ExecuteReader(gCon.cnstring, "Payroll_Contract_Branch_List",
                                     New SqlParameter("@chrdepartment", cbocompany.Text))
        While rd.Read
            cboBranch.Items.Add(rd(0))
        End While

    End Sub
    Private Sub LocationList()
        Dim rd As SqlDataReader
        cboDepartment.Items.Clear()
        rd = SqlHelper.ExecuteReader(gCon.cnstring, "Payroll_Contract_Location_List",
                                     New SqlParameter("@chrdepartment", cbocompany.Text),
                                     New SqlParameter("@chrBranch", cboBranch.Text))
        While rd.Read
            cboDepartment.Items.Add(rd(0))
        End While

    End Sub

    Private Sub cboBranch_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBranch.SelectedIndexChanged
        LocationList()
        FilterByBranch()
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        'Dim i As Integer
        Try
            frmMember_Master.txtContractrate.Text = dgvContractRate.SelectedCells(0).Value.ToString
            frmMember_Master.txtbasicpay.Text = dgvContractRate.SelectedCells(1).Value.ToString
            frmMember_Master.txtContractPrice.Text = dgvContractRate.SelectedCells(2).Value.ToString
            frmMember_Master.txtEcola.Text = dgvContractRate.SelectedCells(3).Value.ToString
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Contract Rate")
            gCon.sqlconn.Close()
        End Try
    End Sub

    Private Sub txtFiter_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtFiter.TextChanged
        LoadRate()
    End Sub
End Class