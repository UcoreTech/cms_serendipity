﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Public Class frmMember_Legal

    Dim pkLegalCase As String
    Dim OpenFileDialog1 As New OpenFileDialog()
    Dim fpath As String
    Dim filenym As String
    Dim fileData As Byte()
    Dim ex As String
    Dim afiles As String
    Public FileDirectory As String
    Public filefromMem As String
    Dim filesFormdb As Byte()

#Region "Property"
    Public Property getpkLegalCase() As String
        Get
            Return pkLegalCase
        End Get
        Set(ByVal value As String)
            pkLegalCase = value
        End Set
    End Property
#End Region

    Private Sub AddEditLegalCase(ByVal EmpNo As String, ByVal RefNo As String, ByVal Details As String, ByVal dtDate As Date, ByVal CourtFiled As String, _
                            ByVal Prosec As String, ByVal HearingDate As String, ByVal CaseStatus As String,
                            ByVal AttachFileName As String, ByVal AttachedFile As Byte(), ByVal pk_LegalCase As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_LegalCase_AddEdit", _
                                     New SqlParameter("@employeeNo", EmpNo), _
                                     New SqlParameter("@fcRefNo", RefNo), _
                                     New SqlParameter("@fcDetails", Details), _
                                     New SqlParameter("@fdDate", dtDate), _
                                     New SqlParameter("@fcCourtFiled", CourtFiled), _
                                     New SqlParameter("@fcProsecutors", Prosec), _
                                     New SqlParameter("@fdHearingDate", HearingDate), _
                                     New SqlParameter("@fcCaseStatus", CaseStatus), _
                                     New SqlParameter("@FileName", AttachFileName), _
                                     New SqlParameter("@AttachedFiles", AttachedFile), _
                                     New SqlParameter("@pk_LegalCase", pk_LegalCase))
            trans.Commit()
            MessageBox.Show("Record has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Save Legal Case")
        Finally
            gcon.sqlconn.Close()
        End Try
        'New SqlParameter("@FileDirectory", AttachFileDirectory), _
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        If fileData Is Nothing Then
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("MSS_MembersInfo_GetLegal_AttachedFiles", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            myconnection.sqlconn.Open()
            cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = frmMember_Master.txtEmployeeNo.Text
            cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 100).Value = txtAttachFiles.Text
            Using myreader As SqlDataReader = cmd.ExecuteReader
                myreader.Read()
                filesFormdb = DirectCast(myreader("fcAttachedFiles"), Byte())
            End Using
            Call AddEditLegalCase(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtRefNo.Text.Trim, Me.txtDetails.Text.Trim, Me.dtDates.Value,
                         Me.txtCourt.Text.Trim, Me.txtProsec.Text.Trim, dtHDate.Value, Me.txtCaseStat.Text.Trim,
                         txtAttachFiles.Text, filesFormdb, pkLegalCase)
            Call frmMember_Master.GetmemberTrainings(frmMember_Master.txtEmployeeNo.Text)
            Me.Close()
        Else
            Call AddEditLegalCase(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtRefNo.Text.Trim, Me.txtDetails.Text.Trim, Me.dtDates.Value,
                         Me.txtCourt.Text.Trim, Me.txtProsec.Text.Trim, dtHDate.Value, Me.txtCaseStat.Text.Trim,
                         txtAttachFiles.Text, fileData, pkLegalCase)
            Call frmMember_Master.GetmemberLegalCase(frmMember_Master.txtEmployeeNo.Text)
            Me.Close()
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtDetails.Text = "" Or txtDetails.Text = "" Or txtCourt.Text = "" Or txtRefNo.Text = "" Or txtProsec.Text = "" Then
            MessageBox.Show("Complete Information is Needed", "Discipline Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Call AddEditLegalCase(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtRefNo.Text.Trim, Me.txtDetails.Text.Trim, Me.dtDates.Value,
                                 Me.txtCourt.Text.Trim, Me.txtProsec.Text.Trim, dtHDate.Value, Me.txtCaseStat.Text.Trim,
                                 txtAttachFiles.Text, fileData, pkLegalCase)
            Call frmMember_Master.GetmemberLegalCase(frmMember_Master.txtEmployeeNo.Text)
            Call ClearText()
        End If
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub ClearText()
        txtCaseStat.Text = ""
        txtCourt.Text = ""
        txtDetails.Text = ""
        txtProsec.Text = ""
        txtRefNo.Text = ""
        txtAttachFiles.Text = ""
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Try
            OpenFileDialog1.Filter = "Text Files (*.txt)|*.txt|PDF Files (*.pdf)|*.pdf|Word Documents (*.docx)|*.docx|Excel Worksheets (*.xlsx)|*.xlsx|PowerPoint Presentations (*.pptx)|*.pptx|Word Documents 97-2003 (*.doc)|*.docx|Excel Worksheets 93-2003 (*.xls)|*.xls|PowerPoint Presentations (*.ppt)|*.ppt" & "|Office Files|*.docx;*.xlsx;*.pptx;*.doc;*.xls;*.ppt" & "|Images|*.jpg;*.png;*.gif"
            OpenFileDialog1.Title = "Select File"
            OpenFileDialog1.CheckFileExists = True
            If OpenFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                filenym = OpenFileDialog1.FileName
                fpath = Path.GetExtension(filenym)
                txtAttachFiles.Text = Path.GetFileName(filenym)

                fileData = File.ReadAllBytes(filenym)
                ex = Path.GetExtension(LTrim(RTrim(filenym)))

                Dim ms As New MemoryStream(fileData, 0, fileData.Length)

                ms.Write(fileData, 0, fileData.Length)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class