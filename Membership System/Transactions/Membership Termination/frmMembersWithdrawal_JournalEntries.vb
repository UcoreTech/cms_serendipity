Public Class frmMembersWithdrawal_JournalEntries

    Private gCon As New Clsappconfiguration()
    Private rptsummary As New ReportDocument

#Region "Properties"
    Private employeeNo As String
    Public Property GetSetEmployeeNo() As String
        Get
            Return employeeNo
        End Get
        Set(ByVal value As String)
            employeeNo = value
        End Set
    End Property
    Private effectiveDate As Date
    Public Property GetSetEffectiveDate() As Date
        Get
            Return effectiveDate
        End Get
        Set(ByVal value As Date)
            effectiveDate = value
        End Set
    End Property
    Private user As String
    Public Property GetSetUser() As String
        Get
            Return user
        End Get
        Set(ByVal value As String)
            user = value
        End Set
    End Property
    Public EditedInterestRefnd As String = ""
#End Region

#Region "Sub-Routines"

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub InitializeReport()
        rptsummary.Load(Application.StartupPath & "\LoanReport\MembersWithdrawal_Journal.rpt")
        rptsummary.Refresh()
        Logon(rptsummary, gCon.Server, gCon.Database, gCon.Username, gCon.Password)
        rptsummary.SetParameterValue("@employeeNo", GetSetEmployeeNo())
        rptsummary.SetParameterValue("@effectiveDate", GetSetEffectiveDate())
        rptsummary.SetParameterValue("@user", GetSetUser())
        rptsummary.SetParameterValue("@EditedIntrstRfund", EditedInterestRefnd)
    End Sub

    Private Sub LoadReportToViewer()
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary
    End Sub

    Private Sub HideLoadingStatus()
        loadingLabel.Visible = False
    End Sub

#End Region

    Private Sub bgwLoadReport_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwLoadReport.DoWork
        Call InitializeReport()
    End Sub

    Private Sub bgwLoadReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwLoadReport.RunWorkerCompleted
        Call LoadReportToViewer()
        Call HideLoadingStatus()
    End Sub

    Private Sub frmMembersWithdrawal_JournalEntries_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        bgwLoadReport.RunWorkerAsync()
    End Sub

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Close()
    End Sub
End Class