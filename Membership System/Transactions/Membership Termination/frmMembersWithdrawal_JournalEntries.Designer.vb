<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMembersWithdrawal_JournalEntries
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMembersWithdrawal_JournalEntries))
        Me.panelButton = New System.Windows.Forms.Panel()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.panelReport = New System.Windows.Forms.Panel()
        Me.CrvRpt = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.bgwLoadReport = New System.ComponentModel.BackgroundWorker()
        Me.loadingLabel = New System.Windows.Forms.Label()
        Me.panelButton.SuspendLayout()
        Me.panelReport.SuspendLayout()
        Me.SuspendLayout()
        '
        'panelButton
        '
        Me.panelButton.Controls.Add(Me.btnClose)
        Me.panelButton.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panelButton.Location = New System.Drawing.Point(0, 468)
        Me.panelButton.Name = "panelButton"
        Me.panelButton.Size = New System.Drawing.Size(763, 36)
        Me.panelButton.TabIndex = 0
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.btnClose.Location = New System.Drawing.Point(673, 6)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(87, 24)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'panelReport
        '
        Me.panelReport.Controls.Add(Me.CrvRpt)
        Me.panelReport.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panelReport.Location = New System.Drawing.Point(0, 0)
        Me.panelReport.Name = "panelReport"
        Me.panelReport.Size = New System.Drawing.Size(763, 468)
        Me.panelReport.TabIndex = 0
        '
        'CrvRpt
        '
        Me.CrvRpt.ActiveViewIndex = -1
        Me.CrvRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrvRpt.Cursor = System.Windows.Forms.Cursors.Default
        Me.CrvRpt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrvRpt.Location = New System.Drawing.Point(0, 0)
        Me.CrvRpt.Name = "CrvRpt"
        Me.CrvRpt.ShowCloseButton = False
        Me.CrvRpt.ShowParameterPanelButton = False
        Me.CrvRpt.ShowRefreshButton = False
        Me.CrvRpt.Size = New System.Drawing.Size(763, 468)
        Me.CrvRpt.TabIndex = 0
        Me.CrvRpt.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        '
        'bgwLoadReport
        '
        '
        'loadingLabel
        '
        Me.loadingLabel.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.loadingLabel.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.loadingLabel.Image = Global.WindowsApplication2.My.Resources.Resources.LoadingData
        Me.loadingLabel.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.loadingLabel.Location = New System.Drawing.Point(322, 195)
        Me.loadingLabel.Name = "loadingLabel"
        Me.loadingLabel.Size = New System.Drawing.Size(119, 114)
        Me.loadingLabel.TabIndex = 4
        Me.loadingLabel.Text = "Loading Report..."
        Me.loadingLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'frmMembersWithdrawal_JournalEntries
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(763, 504)
        Me.Controls.Add(Me.loadingLabel)
        Me.Controls.Add(Me.panelReport)
        Me.Controls.Add(Me.panelButton)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMembersWithdrawal_JournalEntries"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Members Withdrawal - Journal Entries"
        Me.panelButton.ResumeLayout(False)
        Me.panelReport.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents panelButton As System.Windows.Forms.Panel
    Friend WithEvents panelReport As System.Windows.Forms.Panel
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents CrvRpt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents bgwLoadReport As System.ComponentModel.BackgroundWorker
    Friend WithEvents loadingLabel As System.Windows.Forms.Label
End Class
