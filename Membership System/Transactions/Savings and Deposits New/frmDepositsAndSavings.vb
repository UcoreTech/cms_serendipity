Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmDepositsAndSavings

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub FrmDeposits_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Call Load_MemberNumber()
        Call Load_MemberName()
    End Sub
#Region "Load Member number"
    Private Sub Load_MemberNumber()
        Dim gCon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList")
        While rd.Read
            lstNumber.Items.Add(rd.Item("Employee No")).ToString()
        End While
        rd.Close()
        gCon.sqlconn.Close()

    End Sub
#End Region
#Region "Load Member fullname"
    Private Sub Load_MemberName()
        Dim gCon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList")
        With Me.lstMembers
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("IDnumber", 70, HorizontalAlignment.Left)
            .Columns.Add("Member's Fullname", 200, HorizontalAlignment.Left)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        .SubItems.Add(rd.Item(1))
                    End With
                End While
                rd.Close()
                gCon.sqlconn.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Load Member list")
            End Try
        End With
        'Dim gCon As New Clsappconfiguration
        'Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList")
        'While rd.Read
        '    lstNames.Items.Add(rd.Item("Full Name")).ToString()
        'End While
        'rd.Close()
        'gCon.sqlconn.Close()
    End Sub
#End Region

    'Private Sub optNumber_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optNumber.CheckedChanged
    '    Me.txtSearchnumber.Visible = True
    '    Me.lstNumber.Visible = True
    '    Me.txtSearchLastname.Visible = False
    '    Me.lstNames.Visible = False
    'End Sub

    'Private Sub optName_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optName.CheckedChanged
    '    Me.txtSearchLastname.Visible = True
    '    Me.lstNames.Visible = True
    '    Me.txtSearchnumber.Visible = False
    '    Me.lstNumber.Visible = False
    'End Sub

    Private Sub txtSearchnumber_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchnumber.TextChanged
        Dim gcon As New Clsappconfiguration
        Dim searchno As String
        lstNumber.Items.Clear()

        searchno = "select fcEmployeeNo from dbo.CIMS_m_Member_Employment " & _
                    "where fcEmployeeNo LIKE '%" & txtSearchnumber.Text & "%'"


        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.Text, searchno)
            While rd.Read
                lstNumber.Items.Add(rd.Item(0).ToString)
            End While
            rd.Close()
            gcon.sqlconn.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtSearchLastname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSearchLastname.KeyPress
        If e.KeyChar = Chr(13) Then
            Call SearchMember(Me.txtSearchLastname.Text)
        End If
    End Sub

    Private Sub cmdSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        Call SearchMember(Me.txtSearchLastname.Text)
    End Sub
#Region "Get EMP idnumber"
    Private Sub getEmpIdnumber(ByVal id As String)
        Dim gcon As New Clsappconfiguration
        Dim searchno As String
        lstNumber.Items.Clear()

        searchno = "select fcEmployeeNo from dbo.CIMS_m_Member_Employment " & _
                    "where fcEmployeeNo LIKE '%" & id & "%'"


        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.Text, searchno)
            While rd.Read
                lstNumber.Items.Add(rd.Item(0).ToString)
            End While
            rd.Close()
            gcon.sqlconn.Close()
        Catch ex As Exception

        End Try
    End Sub
#End Region
#Region "Get Emp lastname"
    Private Sub getEmplastname(ByVal lastname As String)
        Dim gcon As New Clsappconfiguration
        lstNames.Items.Clear()
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "MSS_MembersInfo_SearchName", New SqlParameter("@membername", lastname.Trim))
            While rd.Read
                lstNames.Items.Add(rd.Item(0).ToString)
            End While
            rd.Close()
            gcon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub

#End Region
#Region "Search member"
    Private Sub SearchMember(ByVal searchname As String)
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList_SearchName", _
                                                          New SqlParameter("@SEARCH", searchname))
        With Me.lstMembers
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("IDnumber", 70, HorizontalAlignment.Left)
            .Columns.Add("Member's Fullname", 200, HorizontalAlignment.Left)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        .SubItems.Add(rd.Item(1))
                    End With
                End While
                rd.Close()
                gcon.sqlconn.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Load Member list")
            End Try
        End With

        'If Me.optNumber.Checked = True Then
        '    Call getEmpIdnumber(Me.txtSearchnumber.Text.Trim)
        'End If
        'If Me.optName.Checked = True Then
        '    Call getEmplastname(Me.txtSearchLastname.Text.Trim)
        'End If
    End Sub
#End Region

    Private Sub lstNumber_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstNumber.SelectedIndexChanged
        Call ViewSavingsDeposit(Me.lstNumber.SelectedItem.ToString)
        Call ViewTimeDeposit(Me.lstNumber.SelectedItem.ToString)
    End Sub
#Region "View Saving Deposit"
    Private Sub ViewSavingsDeposit(ByVal Empinfo As String)
        Dim gcon As New Clsappconfiguration
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("MSS_LoanTracking_GetSavingsDeposit", gcon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        gcon.sqlconn.Open()
        With cmd.Parameters
            .Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = Empinfo
        End With
        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "CIMS_m_Member")

            Me.grdSavingDeposit.DataSource = ds
            Me.grdSavingDeposit.DataMember = "CIMS_m_Member"
            Me.grdSavingDeposit.Columns(0).Width = 150
            Me.grdSavingDeposit.Columns(2).DefaultCellStyle.Format = "#,##0.00"
            Me.grdSavingDeposit.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
            Me.grdSavingDeposit.Columns(3).DefaultCellStyle.Format = "#,##0.00"
            Me.grdSavingDeposit.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
            Me.grdSavingDeposit.Columns(4).DefaultCellStyle.Format = "#,##0.00"
            Me.grdSavingDeposit.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight



            cmd.ExecuteNonQuery()
            gcon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "View Saving Deposit")
        End Try
    End Sub
#End Region
#Region "View Time Deposit"
    Private Sub ViewTimeDeposit(ByVal Empinof As String)
        Dim gcon As New Clsappconfiguration
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("MSS_LoanTracking_GetTimeDeposit", gcon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        gcon.sqlconn.Open()
        With cmd.Parameters
            .Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = Empinof
        End With

        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "CIMS_m_Member")

            Me.grdTimeDeposit.DataSource = ds
            Me.grdTimeDeposit.DataMember = "CIMS_m_Member"
            Me.grdTimeDeposit.Columns(0).Width = 150
            Me.grdTimeDeposit.Columns(3).DefaultCellStyle.Format = "#,##0.00"
            Me.grdTimeDeposit.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
            Me.grdTimeDeposit.Columns(4).DefaultCellStyle.Format = "#,##0.00"
            Me.grdTimeDeposit.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
            Me.grdTimeDeposit.Columns(5).DefaultCellStyle.Format = "#,##0.00"
            Me.grdTimeDeposit.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight

            cmd.ExecuteNonQuery()
            gcon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "View Time Deposit")
        End Try
    End Sub
#End Region

    Private Sub lstNames_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstNames.SelectedIndexChanged
        Dim x, result As String
        x = lstNames.SelectedItem.ToString
        Dim y As String() = x.Split(New Char() {","})
        result = y(0)
        Call ViewSavingsDeposit(result)
        Call ViewTimeDeposit(result)
    End Sub

    Private Sub lstMembers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstMembers.Click
        Call ViewSavingsDeposit(Me.lstMembers.SelectedItems(0).Text)
        Call ViewTimeDeposit(Me.lstMembers.SelectedItems(0).Text)
        Me.Text = "Deposits" + " ( " + Me.lstMembers.SelectedItems(0).Text.ToUpper + " : " + Me.lstMembers.SelectedItems(0).SubItems(1).Text.ToUpper + " ) "
    End Sub

End Class