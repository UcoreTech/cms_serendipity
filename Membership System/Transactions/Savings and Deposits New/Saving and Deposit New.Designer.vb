<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Saving_and_Deposit_New
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.Members = New System.Windows.Forms.TabPage()
        Me.lstNames = New System.Windows.Forms.ListBox()
        Me.lstNumbers = New System.Windows.Forms.ListBox()
        Me.txtSearch = New System.Windows.Forms.TextBox()
        Me.lstmember = New System.Windows.Forms.ListView()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnsrch = New System.Windows.Forms.Button()
        Me.txtSearchName = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboAccountName = New System.Windows.Forms.ComboBox()
        Me.btnInt = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Splitter1 = New System.Windows.Forms.Splitter()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.btnwDraw = New System.Windows.Forms.Button()
        Me.btnDep = New System.Windows.Forms.Button()
        Me.btnpreview = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.dtend = New System.Windows.Forms.DateTimePicker()
        Me.dtstart = New System.Windows.Forms.DateTimePicker()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.CrvRpt = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.grdTimeDeposit = New System.Windows.Forms.DataGridView()
        Me.grdSavingDeposit = New System.Windows.Forms.DataGridView()
        Me.btnSettings = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbosavtd = New System.Windows.Forms.ComboBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txttdavailbal = New System.Windows.Forms.TextBox()
        Me.txttdcurbal = New System.Windows.Forms.TextBox()
        Me.txtdtmatur = New System.Windows.Forms.TextBox()
        Me.txtterm = New System.Windows.Forms.TextBox()
        Me.dtdate = New System.Windows.Forms.TextBox()
        Me.lblterm = New System.Windows.Forms.Label()
        Me.lbldateofmaturity = New System.Windows.Forms.Label()
        Me.txtavailbal = New System.Windows.Forms.TextBox()
        Me.txtcurbal = New System.Windows.Forms.TextBox()
        Me.Txtname = New System.Windows.Forms.TextBox()
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.PanePanel5 = New WindowsApplication2.PanePanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.Members.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.grdTimeDeposit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdSavingDeposit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.PanePanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(14, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(79, 19)
        Me.Label19.TabIndex = 14
        Me.Label19.Text = "Deposits"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 32)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.TabControl1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.BackColor = System.Drawing.Color.White
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel2.Controls.Add(Me.cboAccountName)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnInt)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Button1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Splitter1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Button8)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnwDraw)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnDep)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnpreview)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnSettings)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel2.Controls.Add(Me.cbosavtd)
        Me.SplitContainer1.Panel2.Controls.Add(Me.GroupBox2)
        Me.SplitContainer1.Size = New System.Drawing.Size(1013, 464)
        Me.SplitContainer1.SplitterDistance = 293
        Me.SplitContainer1.TabIndex = 54
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.Members)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(293, 464)
        Me.TabControl1.TabIndex = 55
        '
        'Members
        '
        Me.Members.Controls.Add(Me.lstNames)
        Me.Members.Controls.Add(Me.lstNumbers)
        Me.Members.Controls.Add(Me.txtSearch)
        Me.Members.Controls.Add(Me.lstmember)
        Me.Members.Controls.Add(Me.Button2)
        Me.Members.Controls.Add(Me.GroupBox1)
        Me.Members.Location = New System.Drawing.Point(4, 22)
        Me.Members.Name = "Members"
        Me.Members.Padding = New System.Windows.Forms.Padding(3)
        Me.Members.Size = New System.Drawing.Size(285, 438)
        Me.Members.TabIndex = 0
        Me.Members.Text = "Members"
        Me.Members.UseVisualStyleBackColor = True
        '
        'lstNames
        '
        Me.lstNames.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lstNames.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstNames.FormattingEnabled = True
        Me.lstNames.Location = New System.Drawing.Point(25, 261)
        Me.lstNames.Name = "lstNames"
        Me.lstNames.Size = New System.Drawing.Size(181, 28)
        Me.lstNames.TabIndex = 52
        Me.lstNames.Visible = False
        '
        'lstNumbers
        '
        Me.lstNumbers.FormattingEnabled = True
        Me.lstNumbers.Location = New System.Drawing.Point(156, 69)
        Me.lstNumbers.Name = "lstNumbers"
        Me.lstNumbers.Size = New System.Drawing.Size(120, 186)
        Me.lstNumbers.TabIndex = 4
        Me.lstNumbers.Visible = False
        '
        'txtSearch
        '
        Me.txtSearch.Location = New System.Drawing.Point(28, 209)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(100, 21)
        Me.txtSearch.TabIndex = 5
        Me.txtSearch.Visible = False
        '
        'lstmember
        '
        Me.lstmember.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstmember.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstmember.Location = New System.Drawing.Point(0, 45)
        Me.lstmember.Name = "lstmember"
        Me.lstmember.Size = New System.Drawing.Size(286, 357)
        Me.lstmember.TabIndex = 3
        Me.lstmember.UseCompatibleStateImageBehavior = False
        '
        'Button2
        '
        Me.Button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button2.BackColor = System.Drawing.Color.White
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Image = Global.WindowsApplication2.My.Resources.Resources.File
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.Location = New System.Drawing.Point(195, 406)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(87, 31)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "Reports"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button2.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.btnsrch)
        Me.GroupBox1.Controls.Add(Me.txtSearchName)
        Me.GroupBox1.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(284, 42)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Search"
        '
        'btnsrch
        '
        Me.btnsrch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnsrch.BackColor = System.Drawing.Color.White
        Me.btnsrch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsrch.Location = New System.Drawing.Point(195, 14)
        Me.btnsrch.Name = "btnsrch"
        Me.btnsrch.Size = New System.Drawing.Size(84, 24)
        Me.btnsrch.TabIndex = 4
        Me.btnsrch.Text = "Search"
        Me.btnsrch.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsrch.UseVisualStyleBackColor = False
        '
        'txtSearchName
        '
        Me.txtSearchName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchName.Location = New System.Drawing.Point(3, 16)
        Me.txtSearchName.Name = "txtSearchName"
        Me.txtSearchName.Size = New System.Drawing.Size(182, 22)
        Me.txtSearchName.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(211, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(103, 15)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "Account Name:"
        '
        'cboAccountName
        '
        Me.cboAccountName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAccountName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.cboAccountName.FormattingEnabled = True
        Me.cboAccountName.Location = New System.Drawing.Point(320, 13)
        Me.cboAccountName.Name = "cboAccountName"
        Me.cboAccountName.Size = New System.Drawing.Size(273, 24)
        Me.cboAccountName.TabIndex = 12
        '
        'btnInt
        '
        Me.btnInt.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnInt.BackColor = System.Drawing.Color.Beige
        Me.btnInt.Enabled = False
        Me.btnInt.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.btnInt.Image = Global.WindowsApplication2.My.Resources.Resources.Coins
        Me.btnInt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnInt.Location = New System.Drawing.Point(488, 429)
        Me.btnInt.Name = "btnInt"
        Me.btnInt.Size = New System.Drawing.Size(91, 31)
        Me.btnInt.TabIndex = 11
        Me.btnInt.Text = "Interest"
        Me.btnInt.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnInt.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button1.BackColor = System.Drawing.Color.Beige
        Me.Button1.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Button1.Image = Global.WindowsApplication2.My.Resources.Resources.check
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(362, 429)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(120, 31)
        Me.Button1.TabIndex = 10
        Me.Button1.Text = "Transaction"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Splitter1
        '
        Me.Splitter1.Location = New System.Drawing.Point(0, 0)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(3, 464)
        Me.Splitter1.TabIndex = 9
        Me.Splitter1.TabStop = False
        '
        'Button8
        '
        Me.Button8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button8.BackColor = System.Drawing.Color.Beige
        Me.Button8.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Button8.Image = Global.WindowsApplication2.My.Resources.Resources.Folder___Home
        Me.Button8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button8.Location = New System.Drawing.Point(626, 429)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(87, 31)
        Me.Button8.TabIndex = 8
        Me.Button8.Text = "Close"
        Me.Button8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button8.UseVisualStyleBackColor = False
        '
        'btnwDraw
        '
        Me.btnwDraw.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnwDraw.BackColor = System.Drawing.Color.Beige
        Me.btnwDraw.Enabled = False
        Me.btnwDraw.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnwDraw.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnwDraw.Location = New System.Drawing.Point(231, 429)
        Me.btnwDraw.Name = "btnwDraw"
        Me.btnwDraw.Size = New System.Drawing.Size(125, 31)
        Me.btnwDraw.TabIndex = 6
        Me.btnwDraw.Text = "Withdraw"
        Me.btnwDraw.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnwDraw.UseVisualStyleBackColor = False
        '
        'btnDep
        '
        Me.btnDep.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnDep.BackColor = System.Drawing.Color.Beige
        Me.btnDep.Enabled = False
        Me.btnDep.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.btnDep.Image = Global.WindowsApplication2.My.Resources.Resources.cash_register
        Me.btnDep.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDep.Location = New System.Drawing.Point(111, 429)
        Me.btnDep.Name = "btnDep"
        Me.btnDep.Size = New System.Drawing.Size(114, 31)
        Me.btnDep.TabIndex = 5
        Me.btnDep.Text = "Deposit"
        Me.btnDep.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDep.UseVisualStyleBackColor = False
        '
        'btnpreview
        '
        Me.btnpreview.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnpreview.BackColor = System.Drawing.Color.Beige
        Me.btnpreview.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.btnpreview.Image = Global.WindowsApplication2.My.Resources.Resources.files
        Me.btnpreview.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnpreview.Location = New System.Drawing.Point(2, 429)
        Me.btnpreview.Name = "btnpreview"
        Me.btnpreview.Size = New System.Drawing.Size(103, 31)
        Me.btnpreview.TabIndex = 3
        Me.btnpreview.Text = "Preview"
        Me.btnpreview.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnpreview.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.GroupBox4)
        Me.Panel1.Controls.Add(Me.GroupBox3)
        Me.Panel1.Location = New System.Drawing.Point(3, 177)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(710, 247)
        Me.Panel1.TabIndex = 4
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Button3)
        Me.GroupBox4.Controls.Add(Me.dtend)
        Me.GroupBox4.Controls.Add(Me.dtstart)
        Me.GroupBox4.Controls.Add(Me.Label11)
        Me.GroupBox4.Controls.Add(Me.Label10)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(7, 4)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(695, 37)
        Me.GroupBox4.TabIndex = 2
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Filter"
        '
        'Button3
        '
        Me.Button3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button3.Location = New System.Drawing.Point(603, 11)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(84, 23)
        Me.Button3.TabIndex = 12
        Me.Button3.Text = "Display"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'dtend
        '
        Me.dtend.CustomFormat = ""
        Me.dtend.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtend.Location = New System.Drawing.Point(323, 12)
        Me.dtend.Name = "dtend"
        Me.dtend.Size = New System.Drawing.Size(137, 21)
        Me.dtend.TabIndex = 11
        '
        'dtstart
        '
        Me.dtstart.CustomFormat = ""
        Me.dtstart.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtstart.Location = New System.Drawing.Point(106, 12)
        Me.dtstart.Name = "dtstart"
        Me.dtstart.Size = New System.Drawing.Size(137, 21)
        Me.dtstart.TabIndex = 10
        Me.dtstart.Value = New Date(2000, 1, 1, 0, 0, 0, 0)
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(271, 17)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(33, 13)
        Me.Label11.TabIndex = 5
        Me.Label11.Text = "End:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(44, 17)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(38, 13)
        Me.Label10.TabIndex = 4
        Me.Label10.Text = "Start:"
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.CrvRpt)
        Me.GroupBox3.Controls.Add(Me.grdTimeDeposit)
        Me.GroupBox3.Controls.Add(Me.grdSavingDeposit)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(4, 47)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(703, 197)
        Me.GroupBox3.TabIndex = 1
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Savings Deposit"
        '
        'CrvRpt
        '
        Me.CrvRpt.ActiveViewIndex = -1
        Me.CrvRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrvRpt.Cursor = System.Windows.Forms.Cursors.Default
        Me.CrvRpt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrvRpt.Location = New System.Drawing.Point(3, 16)
        Me.CrvRpt.Name = "CrvRpt"
        Me.CrvRpt.SelectionFormula = ""
        Me.CrvRpt.ShowGroupTreeButton = False
        Me.CrvRpt.ShowRefreshButton = False
        Me.CrvRpt.Size = New System.Drawing.Size(697, 178)
        Me.CrvRpt.TabIndex = 2
        Me.CrvRpt.ViewTimeSelectionFormula = ""
        Me.CrvRpt.Visible = False
        '
        'grdTimeDeposit
        '
        Me.grdTimeDeposit.AllowUserToAddRows = False
        Me.grdTimeDeposit.AllowUserToDeleteRows = False
        Me.grdTimeDeposit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdTimeDeposit.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdTimeDeposit.Location = New System.Drawing.Point(3, 16)
        Me.grdTimeDeposit.Name = "grdTimeDeposit"
        Me.grdTimeDeposit.ReadOnly = True
        Me.grdTimeDeposit.RowHeadersVisible = False
        Me.grdTimeDeposit.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdTimeDeposit.Size = New System.Drawing.Size(697, 178)
        Me.grdTimeDeposit.TabIndex = 1
        Me.grdTimeDeposit.Visible = False
        '
        'grdSavingDeposit
        '
        Me.grdSavingDeposit.AllowUserToAddRows = False
        Me.grdSavingDeposit.AllowUserToDeleteRows = False
        Me.grdSavingDeposit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdSavingDeposit.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdSavingDeposit.Location = New System.Drawing.Point(3, 16)
        Me.grdSavingDeposit.Name = "grdSavingDeposit"
        Me.grdSavingDeposit.ReadOnly = True
        Me.grdSavingDeposit.RowHeadersVisible = False
        Me.grdSavingDeposit.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdSavingDeposit.Size = New System.Drawing.Size(697, 178)
        Me.grdSavingDeposit.TabIndex = 0
        '
        'btnSettings
        '
        Me.btnSettings.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSettings.BackColor = System.Drawing.Color.Beige
        Me.btnSettings.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSettings.Image = Global.WindowsApplication2.My.Resources.Resources.run
        Me.btnSettings.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSettings.Location = New System.Drawing.Point(599, 3)
        Me.btnSettings.Name = "btnSettings"
        Me.btnSettings.Size = New System.Drawing.Size(111, 43)
        Me.btnSettings.TabIndex = 3
        Me.btnSettings.Text = "Settings"
        Me.btnSettings.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSettings.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(9, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(95, 15)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Account Type:"
        '
        'cbosavtd
        '
        Me.cbosavtd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbosavtd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.cbosavtd.FormattingEnabled = True
        Me.cbosavtd.Location = New System.Drawing.Point(110, 12)
        Me.cbosavtd.Name = "cbosavtd"
        Me.cbosavtd.Size = New System.Drawing.Size(95, 24)
        Me.cbosavtd.TabIndex = 1
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.txttdavailbal)
        Me.GroupBox2.Controls.Add(Me.txttdcurbal)
        Me.GroupBox2.Controls.Add(Me.txtdtmatur)
        Me.GroupBox2.Controls.Add(Me.txtterm)
        Me.GroupBox2.Controls.Add(Me.dtdate)
        Me.GroupBox2.Controls.Add(Me.lblterm)
        Me.GroupBox2.Controls.Add(Me.lbldateofmaturity)
        Me.GroupBox2.Controls.Add(Me.txtavailbal)
        Me.GroupBox2.Controls.Add(Me.txtcurbal)
        Me.GroupBox2.Controls.Add(Me.Txtname)
        Me.GroupBox2.Controls.Add(Me.txtID)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(2, 42)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(710, 134)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Account Details:"
        '
        'txttdavailbal
        '
        Me.txttdavailbal.BackColor = System.Drawing.SystemColors.ControlLight
        Me.txttdavailbal.Location = New System.Drawing.Point(499, 44)
        Me.txttdavailbal.Name = "txttdavailbal"
        Me.txttdavailbal.Size = New System.Drawing.Size(204, 22)
        Me.txttdavailbal.TabIndex = 17
        Me.txttdavailbal.Visible = False
        '
        'txttdcurbal
        '
        Me.txttdcurbal.BackColor = System.Drawing.SystemColors.ControlLight
        Me.txttdcurbal.Location = New System.Drawing.Point(499, 15)
        Me.txttdcurbal.Name = "txttdcurbal"
        Me.txttdcurbal.Size = New System.Drawing.Size(204, 22)
        Me.txttdcurbal.TabIndex = 16
        Me.txttdcurbal.Visible = False
        '
        'txtdtmatur
        '
        Me.txtdtmatur.BackColor = System.Drawing.SystemColors.ControlLight
        Me.txtdtmatur.Location = New System.Drawing.Point(499, 100)
        Me.txtdtmatur.Name = "txtdtmatur"
        Me.txtdtmatur.Size = New System.Drawing.Size(204, 22)
        Me.txtdtmatur.TabIndex = 15
        Me.txtdtmatur.Visible = False
        '
        'txtterm
        '
        Me.txtterm.BackColor = System.Drawing.SystemColors.ControlLight
        Me.txtterm.Location = New System.Drawing.Point(499, 72)
        Me.txtterm.Name = "txtterm"
        Me.txtterm.Size = New System.Drawing.Size(204, 22)
        Me.txtterm.TabIndex = 14
        Me.txtterm.Visible = False
        '
        'dtdate
        '
        Me.dtdate.BackColor = System.Drawing.SystemColors.ControlLight
        Me.dtdate.Location = New System.Drawing.Point(81, 17)
        Me.dtdate.Name = "dtdate"
        Me.dtdate.Size = New System.Drawing.Size(137, 22)
        Me.dtdate.TabIndex = 13
        '
        'lblterm
        '
        Me.lblterm.AutoSize = True
        Me.lblterm.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblterm.Location = New System.Drawing.Point(435, 77)
        Me.lblterm.Name = "lblterm"
        Me.lblterm.Size = New System.Drawing.Size(45, 13)
        Me.lblterm.TabIndex = 12
        Me.lblterm.Text = "Term:"
        Me.lblterm.Visible = False
        '
        'lbldateofmaturity
        '
        Me.lbldateofmaturity.AutoSize = True
        Me.lbldateofmaturity.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbldateofmaturity.Location = New System.Drawing.Point(364, 105)
        Me.lbldateofmaturity.Name = "lbldateofmaturity"
        Me.lbldateofmaturity.Size = New System.Drawing.Size(116, 13)
        Me.lbldateofmaturity.TabIndex = 10
        Me.lbldateofmaturity.Text = "Date of Maturity:"
        Me.lbldateofmaturity.Visible = False
        '
        'txtavailbal
        '
        Me.txtavailbal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtavailbal.BackColor = System.Drawing.SystemColors.ControlLight
        Me.txtavailbal.Location = New System.Drawing.Point(498, 44)
        Me.txtavailbal.Name = "txtavailbal"
        Me.txtavailbal.ReadOnly = True
        Me.txtavailbal.Size = New System.Drawing.Size(182, 22)
        Me.txtavailbal.TabIndex = 9
        '
        'txtcurbal
        '
        Me.txtcurbal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtcurbal.BackColor = System.Drawing.SystemColors.ControlLight
        Me.txtcurbal.Location = New System.Drawing.Point(498, 15)
        Me.txtcurbal.Name = "txtcurbal"
        Me.txtcurbal.ReadOnly = True
        Me.txtcurbal.Size = New System.Drawing.Size(182, 22)
        Me.txtcurbal.TabIndex = 8
        '
        'Txtname
        '
        Me.Txtname.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Txtname.Location = New System.Drawing.Point(81, 72)
        Me.Txtname.Name = "Txtname"
        Me.Txtname.ReadOnly = True
        Me.Txtname.Size = New System.Drawing.Size(245, 22)
        Me.Txtname.TabIndex = 7
        '
        'txtID
        '
        Me.txtID.BackColor = System.Drawing.SystemColors.ControlLight
        Me.txtID.Location = New System.Drawing.Point(81, 44)
        Me.txtID.Name = "txtID"
        Me.txtID.ReadOnly = True
        Me.txtID.Size = New System.Drawing.Size(245, 22)
        Me.txtID.TabIndex = 6
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(353, 49)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(127, 13)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "Available Balance:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(365, 22)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(115, 13)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "Current Balance:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(18, 77)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(48, 13)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Name:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(18, 49)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(51, 13)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "ID No.:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(18, 22)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Date:"
        '
        'PanePanel5
        '
        Me.PanePanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel5.Controls.Add(Me.Label1)
        Me.PanePanel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel5.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel5.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel5.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel5.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel5.Name = "PanePanel5"
        Me.PanePanel5.Size = New System.Drawing.Size(1013, 32)
        Me.PanePanel5.TabIndex = 53
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(3, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(258, 19)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Coop Bank Management System"
        '
        'Saving_and_Deposit_New
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1013, 496)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.PanePanel5)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Saving_and_Deposit_New"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = " "
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.Members.ResumeLayout(False)
        Me.Members.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.grdTimeDeposit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdSavingDeposit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.PanePanel5.ResumeLayout(False)
        Me.PanePanel5.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents PanePanel5 As WindowsApplication2.PanePanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents Members As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnsrch As System.Windows.Forms.Button
    Friend WithEvents txtSearchName As System.Windows.Forms.TextBox
    Friend WithEvents btnSettings As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cbosavtd As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtavailbal As System.Windows.Forms.TextBox
    Friend WithEvents txtcurbal As System.Windows.Forms.TextBox
    Friend WithEvents Txtname As System.Windows.Forms.TextBox
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents dtend As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtstart As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents grdSavingDeposit As System.Windows.Forms.DataGridView
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents btnwDraw As System.Windows.Forms.Button
    Friend WithEvents btnDep As System.Windows.Forms.Button
    Friend WithEvents btnpreview As System.Windows.Forms.Button
    Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
    Friend WithEvents lblterm As System.Windows.Forms.Label
    Friend WithEvents lbldateofmaturity As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lstmember As System.Windows.Forms.ListView
    Friend WithEvents lstNumbers As System.Windows.Forms.ListBox
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents lstNames As System.Windows.Forms.ListBox
    Friend WithEvents txtdtmatur As System.Windows.Forms.TextBox
    Friend WithEvents txtterm As System.Windows.Forms.TextBox
    Friend WithEvents dtdate As System.Windows.Forms.TextBox
    Friend WithEvents txttdavailbal As System.Windows.Forms.TextBox
    Friend WithEvents txttdcurbal As System.Windows.Forms.TextBox
    Friend WithEvents grdTimeDeposit As System.Windows.Forms.DataGridView
    Friend WithEvents btnInt As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboAccountName As System.Windows.Forms.ComboBox
    Private WithEvents CrvRpt As CrystalDecisions.Windows.Forms.CrystalReportViewer
End Class
