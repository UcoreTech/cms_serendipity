Imports system.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports WMPs_Money_Figure_Convert_to_Words

Public Class frmWithdrawal
    Private gcon As New Clsappconfiguration
#Region "Property"
    Private idNo As String
    Public Property GetIDNo() As String
        Get
            Return idNo
        End Get
        Set(ByVal value As String)
            idNo = value
        End Set
    End Property
    Private Name As String
    Public Property GetName() As String
        Get
            Return Name
        End Get
        Set(ByVal value As String)
            Name = value
        End Set
    End Property
    Private SelectSTD As String
    Public Property GetSelectSTD() As String
        Get
            Return SelectSTD
        End Get
        Set(ByVal value As String)
            SelectSTD = value
        End Set
    End Property
    Private UniqEmp As String
    Public Property GetUniqEmp() As String
        Get
            Return UniqEmp
        End Get
        Set(ByVal value As String)
            UniqEmp = value
        End Set
    End Property
#End Region
    Private Sub Withdrawal_Savings(ByVal wdate As Date, ByVal descr As String, ByVal empID As String, _
                                    ByVal wAmount As Integer, ByVal clear As Boolean)
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "[CIMS_m_Withdrawal_Savings_Deposit]", _
                                    New SqlParameter("@dtDepdate", wdate), New SqlParameter("@fcDes", descr), _
                                    New SqlParameter("@Emp", empID), New SqlParameter("@fdWithdrawalAmount", wAmount), New SqlParameter("@clear", clear))


            trans.Commit()
            MessageBox.Show("Success!")

        Catch ex As Exception
            trans.Rollback()
            Throw ex
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
    Private Sub Withdrawal_Time_Deposit(ByVal wdate As Date, ByVal descr As String, ByVal empID As String, _
                                    ByVal wAmount As Integer, ByVal clear As Boolean)
                                    
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "[CIMS_m_Withdrawal_Time_Deposit]", _
                                New SqlParameter("@dtWdrawdate", wdate), New SqlParameter("@fcDes", descr), _
                             New SqlParameter("@Emp", empID), New SqlParameter("@fdWithdrawalAmount", wAmount), New SqlParameter("@clear", clear))
            trans.Commit()
            MessageBox.Show("Success!")


        Catch ex As Exception
            trans.Rollback()
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub frmWithdrawal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtID.Text = GetIDNo()
        txtname.Text = GetName()

    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Close()
    End Sub
    Private Sub btnwDrawal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnwDrawal.Click
        If SelectSTD = "Savings" Then
            Withdrawal_Savings(dtDate.Value, "Withdrawal_Savings", GetUniqEmp(), Me.txtAmount.Text, 1)

            'Accounting Integration (Savings Withdrawal)
            Call AccountingEntry_SavingsWithdrawal(txtID.Text, txtAmount.Text, "Withdrawal", dtDate.Value, frmMain.username)
        Else
            Withdrawal_Time_Deposit(dtDate.Value, "Withdrawal_Time_Deposit", GetUniqEmp(), txtAmount.Text, 1)

            'Accounting Integration (Time Withdrawal)
            Call AccountingEntry_TimeWithdrawal(txtID.Text, txtAmount.Text, "Withdrawal", dtDate.Value, frmMain.username)
        End If
    End Sub

    Private Sub AccountingEntry_TimeWithdrawal(ByVal empNo As String, ByVal amount As Decimal, ByVal particulars As String, ByVal effectiveDate As Date, ByVal user As String)
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_TimeWithdrawal_AccountingEntries_ToGenJournal", _
                New SqlParameter("@employeeNo", empNo), _
                New SqlParameter("@amountWithdrawn", amount), _
                New SqlParameter("@particulars", particulars), _
                New SqlParameter("@effectiveDate", effectiveDate), _
                New SqlParameter("@user", user))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub AccountingEntry_SavingsWithdrawal(ByVal empNo As String, ByVal amount As Decimal, ByVal particulars As String, ByVal effectiveDate As Date, ByVal user As String)
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_SavingsWithdrawal_AccountingEntries_ToGenJournal", _
                New SqlParameter("@employeeNo", empNo), _
                New SqlParameter("@amountWithdrawn", amount), _
                New SqlParameter("@particulars", particulars), _
                New SqlParameter("@effectiveDate", effectiveDate), _
                New SqlParameter("@user", user))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub txtAmount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAmount.KeyPress
        If e.KeyChar <> Chr(46) Then
            If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
            If e.KeyChar = Chr(8) Then e.Handled = False
            If e.KeyChar = Chr(13) Then txtAmount.Focus()
        End If
    End Sub
End Class