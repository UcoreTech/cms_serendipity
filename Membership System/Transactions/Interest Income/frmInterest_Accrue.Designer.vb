<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInterest_Accrue
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.PBar = New System.Windows.Forms.ProgressBar
        Me.LblCounter = New System.Windows.Forms.Label
        Me.lblLoanNo = New System.Windows.Forms.Label
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.BGWorker = New System.ComponentModel.BackgroundWorker
        Me.SuspendLayout()
        '
        'PBar
        '
        Me.PBar.Location = New System.Drawing.Point(5, 4)
        Me.PBar.Name = "PBar"
        Me.PBar.Size = New System.Drawing.Size(437, 23)
        Me.PBar.TabIndex = 0
        '
        'LblCounter
        '
        Me.LblCounter.Location = New System.Drawing.Point(241, 30)
        Me.LblCounter.Name = "LblCounter"
        Me.LblCounter.Size = New System.Drawing.Size(202, 13)
        Me.LblCounter.TabIndex = 1
        Me.LblCounter.Text = "0 of 0 record/s"
        Me.LblCounter.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblLoanNo
        '
        Me.lblLoanNo.Location = New System.Drawing.Point(3, 30)
        Me.lblLoanNo.Name = "lblLoanNo"
        Me.lblLoanNo.Size = New System.Drawing.Size(232, 13)
        Me.lblLoanNo.TabIndex = 1
        Me.lblLoanNo.Text = "Processing: [Loan Number] . . . "
        Me.lblLoanNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 10
        '
        'BGWorker
        '
        Me.BGWorker.WorkerReportsProgress = True
        '
        'FrmProcessAccrue
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(446, 50)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblLoanNo)
        Me.Controls.Add(Me.LblCounter)
        Me.Controls.Add(Me.PBar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmProcessAccrue"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Processing . . ."
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PBar As System.Windows.Forms.ProgressBar
    Friend WithEvents LblCounter As System.Windows.Forms.Label
    Friend WithEvents lblLoanNo As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents BGWorker As System.ComponentModel.BackgroundWorker
End Class
