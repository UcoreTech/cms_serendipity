<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoan_Policy_PositionType
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoan_Policy_PositionType))
        Me.gridPolicyPosition = New System.Windows.Forms.DataGridView()
        Me.cboPositionType = New System.Windows.Forms.ComboBox()
        Me.btnclose = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btndelete = New System.Windows.Forms.Button()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.lblPolicyPositionType = New System.Windows.Forms.Label()
        Me.lblUpper = New System.Windows.Forms.Label()
        Me.lblowerLimit = New System.Windows.Forms.Label()
        Me.txtUpperLimit = New System.Windows.Forms.TextBox()
        Me.txtLowerLimit = New System.Windows.Forms.TextBox()
        Me.panelBottom = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnNew = New System.Windows.Forms.Button()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.panelTop = New WindowsApplication2.PanePanel()
        Me.lblPositionType = New System.Windows.Forms.Label()
        CType(Me.gridPolicyPosition, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelBottom.SuspendLayout()
        Me.panelTop.SuspendLayout()
        Me.SuspendLayout()
        '
        'gridPolicyPosition
        '
        Me.gridPolicyPosition.AllowUserToAddRows = False
        Me.gridPolicyPosition.AllowUserToDeleteRows = False
        Me.gridPolicyPosition.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gridPolicyPosition.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.gridPolicyPosition.BackgroundColor = System.Drawing.Color.White
        Me.gridPolicyPosition.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridPolicyPosition.Location = New System.Drawing.Point(10, 142)
        Me.gridPolicyPosition.Name = "gridPolicyPosition"
        Me.gridPolicyPosition.ReadOnly = True
        Me.gridPolicyPosition.RowHeadersVisible = False
        Me.gridPolicyPosition.Size = New System.Drawing.Size(558, 301)
        Me.gridPolicyPosition.TabIndex = 0
        '
        'cboPositionType
        '
        Me.cboPositionType.Enabled = False
        Me.cboPositionType.FormattingEnabled = True
        Me.cboPositionType.Location = New System.Drawing.Point(108, 51)
        Me.cboPositionType.Name = "cboPositionType"
        Me.cboPositionType.Size = New System.Drawing.Size(176, 23)
        Me.cboPositionType.TabIndex = 2
        '
        'btnclose
        '
        Me.btnclose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnclose.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnclose.Image = CType(resources.GetObject("btnclose.Image"), System.Drawing.Image)
        Me.btnclose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnclose.Location = New System.Drawing.Point(459, 3)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.Size = New System.Drawing.Size(70, 37)
        Me.btnclose.TabIndex = 9
        Me.btnclose.Text = "Close"
        Me.btnclose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnclose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Image = Global.WindowsApplication2.My.Resources.Resources.Floppy
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(79, 3)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(70, 37)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "Save"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btndelete
        '
        Me.btndelete.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btndelete.Image = CType(resources.GetObject("btndelete.Image"), System.Drawing.Image)
        Me.btndelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btndelete.Location = New System.Drawing.Point(307, 3)
        Me.btndelete.Name = "btndelete"
        Me.btndelete.Size = New System.Drawing.Size(70, 37)
        Me.btndelete.TabIndex = 8
        Me.btndelete.Text = "Delete"
        Me.btndelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btndelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Image = CType(resources.GetObject("btnEdit.Image"), System.Drawing.Image)
        Me.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEdit.Location = New System.Drawing.Point(155, 3)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(70, 37)
        Me.btnEdit.TabIndex = 7
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'lblPolicyPositionType
        '
        Me.lblPolicyPositionType.AutoSize = True
        Me.lblPolicyPositionType.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPolicyPositionType.Location = New System.Drawing.Point(12, 54)
        Me.lblPolicyPositionType.Name = "lblPolicyPositionType"
        Me.lblPolicyPositionType.Size = New System.Drawing.Size(82, 15)
        Me.lblPolicyPositionType.TabIndex = 3
        Me.lblPolicyPositionType.Text = "Position Type:"
        '
        'lblUpper
        '
        Me.lblUpper.AutoSize = True
        Me.lblUpper.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUpper.Location = New System.Drawing.Point(12, 118)
        Me.lblUpper.Name = "lblUpper"
        Me.lblUpper.Size = New System.Drawing.Size(73, 15)
        Me.lblUpper.TabIndex = 4
        Me.lblUpper.Text = "Upper Limit:"
        '
        'lblowerLimit
        '
        Me.lblowerLimit.AutoSize = True
        Me.lblowerLimit.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblowerLimit.Location = New System.Drawing.Point(12, 86)
        Me.lblowerLimit.Name = "lblowerLimit"
        Me.lblowerLimit.Size = New System.Drawing.Size(72, 15)
        Me.lblowerLimit.TabIndex = 5
        Me.lblowerLimit.Text = "Lower Limit:"
        '
        'txtUpperLimit
        '
        Me.txtUpperLimit.Enabled = False
        Me.txtUpperLimit.Location = New System.Drawing.Point(108, 113)
        Me.txtUpperLimit.Name = "txtUpperLimit"
        Me.txtUpperLimit.Size = New System.Drawing.Size(176, 23)
        Me.txtUpperLimit.TabIndex = 6
        Me.txtUpperLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLowerLimit
        '
        Me.txtLowerLimit.Enabled = False
        Me.txtLowerLimit.Location = New System.Drawing.Point(108, 83)
        Me.txtLowerLimit.Name = "txtLowerLimit"
        Me.txtLowerLimit.Size = New System.Drawing.Size(176, 23)
        Me.txtLowerLimit.TabIndex = 7
        Me.txtLowerLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'panelBottom
        '
        Me.panelBottom.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.panelBottom.Controls.Add(Me.btnNew)
        Me.panelBottom.Controls.Add(Me.btnSave)
        Me.panelBottom.Controls.Add(Me.btnEdit)
        Me.panelBottom.Controls.Add(Me.btnUpdate)
        Me.panelBottom.Controls.Add(Me.btndelete)
        Me.panelBottom.Controls.Add(Me.btnCancel)
        Me.panelBottom.Controls.Add(Me.btnclose)
        Me.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panelBottom.Location = New System.Drawing.Point(0, 464)
        Me.panelBottom.Name = "panelBottom"
        Me.panelBottom.Size = New System.Drawing.Size(581, 45)
        Me.panelBottom.TabIndex = 10
        '
        'btnNew
        '
        Me.btnNew.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.Image = Global.WindowsApplication2.My.Resources.Resources.new3
        Me.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNew.Location = New System.Drawing.Point(3, 3)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(70, 37)
        Me.btnNew.TabIndex = 10
        Me.btnNew.Text = "New"
        Me.btnNew.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnUpdate
        '
        Me.btnUpdate.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdate.Image = CType(resources.GetObject("btnUpdate.Image"), System.Drawing.Image)
        Me.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdate.Location = New System.Drawing.Point(231, 3)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(70, 37)
        Me.btnUpdate.TabIndex = 11
        Me.btnUpdate.Text = "Update"
        Me.btnUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(383, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(70, 37)
        Me.btnCancel.TabIndex = 12
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'panelTop
        '
        Me.panelTop.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.panelTop.Controls.Add(Me.lblPositionType)
        Me.panelTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelTop.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.panelTop.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.panelTop.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.panelTop.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.panelTop.Location = New System.Drawing.Point(0, 0)
        Me.panelTop.Name = "panelTop"
        Me.panelTop.Size = New System.Drawing.Size(581, 33)
        Me.panelTop.TabIndex = 0
        '
        'lblPositionType
        '
        Me.lblPositionType.AutoSize = True
        Me.lblPositionType.BackColor = System.Drawing.Color.Transparent
        Me.lblPositionType.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPositionType.ForeColor = System.Drawing.Color.White
        Me.lblPositionType.Location = New System.Drawing.Point(4, 7)
        Me.lblPositionType.Name = "lblPositionType"
        Me.lblPositionType.Size = New System.Drawing.Size(213, 19)
        Me.lblPositionType.TabIndex = 3
        Me.lblPositionType.Text = "Loan Policy: Position Type"
        '
        'frmLoan_Policy_PositionType
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btnclose
        Me.ClientSize = New System.Drawing.Size(581, 509)
        Me.Controls.Add(Me.panelBottom)
        Me.Controls.Add(Me.txtLowerLimit)
        Me.Controls.Add(Me.txtUpperLimit)
        Me.Controls.Add(Me.lblowerLimit)
        Me.Controls.Add(Me.lblUpper)
        Me.Controls.Add(Me.lblPolicyPositionType)
        Me.Controls.Add(Me.cboPositionType)
        Me.Controls.Add(Me.gridPolicyPosition)
        Me.Controls.Add(Me.panelTop)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(589, 536)
        Me.Name = "frmLoan_Policy_PositionType"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Loan Policy: Position Type"
        CType(Me.gridPolicyPosition, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelBottom.ResumeLayout(False)
        Me.panelTop.ResumeLayout(False)
        Me.panelTop.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents panelTop As WindowsApplication2.PanePanel
    Friend WithEvents gridPolicyPosition As System.Windows.Forms.DataGridView
    Friend WithEvents btnclose As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btndelete As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents lblPositionType As System.Windows.Forms.Label
    Friend WithEvents cboPositionType As System.Windows.Forms.ComboBox
    Friend WithEvents lblPolicyPositionType As System.Windows.Forms.Label
    Friend WithEvents lblUpper As System.Windows.Forms.Label
    Friend WithEvents lblowerLimit As System.Windows.Forms.Label
    Friend WithEvents txtUpperLimit As System.Windows.Forms.TextBox
    Friend WithEvents txtLowerLimit As System.Windows.Forms.TextBox
    Friend WithEvents panelBottom As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
End Class
