<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSelectApprover
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSelectApprover))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtsearch = New System.Windows.Forms.TextBox()
        Me.btnfind = New System.Windows.Forms.Button()
        Me.lvlApprovers = New System.Windows.Forms.ListView()
        Me.PanePanel2 = New WindowsApplication2.PanePanel()
        Me.btnclose = New System.Windows.Forms.Button()
        Me.btnok = New System.Windows.Forms.Button()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PanePanel2.SuspendLayout()
        Me.PanePanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = " Name"
        '
        'txtsearch
        '
        Me.txtsearch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtsearch.Location = New System.Drawing.Point(56, 34)
        Me.txtsearch.MaxLength = 5000
        Me.txtsearch.Name = "txtsearch"
        Me.txtsearch.Size = New System.Drawing.Size(295, 20)
        Me.txtsearch.TabIndex = 3
        '
        'btnfind
        '
        Me.btnfind.Location = New System.Drawing.Point(355, 33)
        Me.btnfind.Name = "btnfind"
        Me.btnfind.Size = New System.Drawing.Size(58, 23)
        Me.btnfind.TabIndex = 10
        Me.btnfind.Text = "Find"
        Me.btnfind.UseVisualStyleBackColor = True
        '
        'lvlApprovers
        '
        Me.lvlApprovers.Location = New System.Drawing.Point(15, 60)
        Me.lvlApprovers.Name = "lvlApprovers"
        Me.lvlApprovers.Size = New System.Drawing.Size(398, 126)
        Me.lvlApprovers.TabIndex = 11
        Me.lvlApprovers.UseCompatibleStateImageBehavior = False
        '
        'PanePanel2
        '
        Me.PanePanel2.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel2.Controls.Add(Me.btnclose)
        Me.PanePanel2.Controls.Add(Me.btnok)
        Me.PanePanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel2.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel2.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel2.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel2.Location = New System.Drawing.Point(0, 192)
        Me.PanePanel2.Name = "PanePanel2"
        Me.PanePanel2.Size = New System.Drawing.Size(416, 29)
        Me.PanePanel2.TabIndex = 12
        '
        'btnclose
        '
        Me.btnclose.Location = New System.Drawing.Point(337, 3)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.Size = New System.Drawing.Size(75, 23)
        Me.btnclose.TabIndex = 1
        Me.btnclose.Text = "Close"
        Me.btnclose.UseVisualStyleBackColor = True
        '
        'btnok
        '
        Me.btnok.Location = New System.Drawing.Point(256, 3)
        Me.btnok.Name = "btnok"
        Me.btnok.Size = New System.Drawing.Size(75, 23)
        Me.btnok.TabIndex = 0
        Me.btnok.Text = "OK"
        Me.btnok.UseVisualStyleBackColor = True
        '
        'PanePanel1
        '
        Me.PanePanel1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.Label2)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel1.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(416, 28)
        Me.PanePanel1.TabIndex = 9
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label2.Location = New System.Drawing.Point(3, 4)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(85, 19)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Client List"
        '
        'frmSelectApprover
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(416, 221)
        Me.Controls.Add(Me.PanePanel2)
        Me.Controls.Add(Me.lvlApprovers)
        Me.Controls.Add(Me.btnfind)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.txtsearch)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSelectApprover"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "..."
        Me.PanePanel2.ResumeLayout(False)
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnok As System.Windows.Forms.Button
    Friend WithEvents btnclose As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtsearch As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents btnfind As System.Windows.Forms.Button
    Friend WithEvents lvlApprovers As System.Windows.Forms.ListView
    Friend WithEvents PanePanel2 As WindowsApplication2.PanePanel
End Class
