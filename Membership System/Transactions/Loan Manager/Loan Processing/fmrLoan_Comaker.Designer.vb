﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fmrLoan_Comaker
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvList = New System.Windows.Forms.DataGridView()
        Me.picStep1 = New WindowsApplication2.PanePanel()
        Me.txtSearch = New System.Windows.Forms.TextBox()
        Me.lblStepOne = New System.Windows.Forms.Label()
        Me.picStep2 = New WindowsApplication2.PanePanel()
        Me.btnView = New System.Windows.Forms.Button()
        Me.btnOk = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.picStep1.SuspendLayout()
        Me.picStep2.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvList
        '
        Me.dgvList.AllowUserToAddRows = False
        Me.dgvList.AllowUserToDeleteRows = False
        Me.dgvList.AllowUserToResizeColumns = False
        Me.dgvList.AllowUserToResizeRows = False
        Me.dgvList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvList.BackgroundColor = System.Drawing.Color.White
        Me.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvList.Location = New System.Drawing.Point(0, 37)
        Me.dgvList.Name = "dgvList"
        Me.dgvList.ReadOnly = True
        Me.dgvList.RowHeadersVisible = False
        Me.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvList.Size = New System.Drawing.Size(556, 251)
        Me.dgvList.TabIndex = 0
        '
        'picStep1
        '
        Me.picStep1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picStep1.Controls.Add(Me.txtSearch)
        Me.picStep1.Controls.Add(Me.lblStepOne)
        Me.picStep1.Dock = System.Windows.Forms.DockStyle.Top
        Me.picStep1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.picStep1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.picStep1.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.picStep1.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.picStep1.Location = New System.Drawing.Point(0, 0)
        Me.picStep1.Name = "picStep1"
        Me.picStep1.Size = New System.Drawing.Size(556, 36)
        Me.picStep1.TabIndex = 29
        '
        'txtSearch
        '
        Me.txtSearch.Location = New System.Drawing.Point(84, 3)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(459, 27)
        Me.txtSearch.TabIndex = 19
        '
        'lblStepOne
        '
        Me.lblStepOne.AutoSize = True
        Me.lblStepOne.BackColor = System.Drawing.Color.Transparent
        Me.lblStepOne.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStepOne.ForeColor = System.Drawing.Color.White
        Me.lblStepOne.Location = New System.Drawing.Point(3, 8)
        Me.lblStepOne.Name = "lblStepOne"
        Me.lblStepOne.Size = New System.Drawing.Size(75, 19)
        Me.lblStepOne.TabIndex = 18
        Me.lblStepOne.Text = "Search: "
        '
        'picStep2
        '
        Me.picStep2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picStep2.Controls.Add(Me.btnView)
        Me.picStep2.Controls.Add(Me.btnOk)
        Me.picStep2.Controls.Add(Me.btnClose)
        Me.picStep2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.picStep2.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.picStep2.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.picStep2.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.picStep2.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.picStep2.Location = New System.Drawing.Point(0, 288)
        Me.picStep2.Name = "picStep2"
        Me.picStep2.Size = New System.Drawing.Size(556, 30)
        Me.picStep2.TabIndex = 42
        '
        'btnView
        '
        Me.btnView.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnView.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnView.Location = New System.Drawing.Point(0, 0)
        Me.btnView.Name = "btnView"
        Me.btnView.Size = New System.Drawing.Size(145, 28)
        Me.btnView.TabIndex = 2
        Me.btnView.Text = "View Details"
        Me.btnView.UseVisualStyleBackColor = True
        '
        'btnOk
        '
        Me.btnOk.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnOk.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.Location = New System.Drawing.Point(404, 0)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(75, 28)
        Me.btnOk.TabIndex = 1
        Me.btnOk.Text = "Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClose.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Location = New System.Drawing.Point(479, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 28)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'fmrLoan_Comaker
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(556, 318)
        Me.Controls.Add(Me.picStep2)
        Me.Controls.Add(Me.picStep1)
        Me.Controls.Add(Me.dgvList)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "fmrLoan_Comaker"
        Me.Text = "..."
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.picStep1.ResumeLayout(False)
        Me.picStep1.PerformLayout()
        Me.picStep2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvList As System.Windows.Forms.DataGridView
    Friend WithEvents picStep1 As WindowsApplication2.PanePanel
    Friend WithEvents lblStepOne As System.Windows.Forms.Label
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents picStep2 As WindowsApplication2.PanePanel
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnView As System.Windows.Forms.Button
End Class
