Public Class frmLoan_ConfirmLoanApplication
    Private Sub cmdSubmit_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSubmit.Click
        Dim msg As String
        msg = MsgBox("Your Loan Application will now be forwarded to the Credit Committee for approval", MsgBoxStyle.OkOnly)
        Me.Close()
        frmLoan_ApplicationMain.panelStep1.Visible = False
        frmLoan_ApplicationMain.panelstep2.Visible = False
        frmLoan_ApplicationMain.panelStep3.Visible = False
        frmLoan_ApplicationMain.lstNumbers.Enabled = True
        frmLoan_ApplicationMain.lstname.Enabled = True
        'frmloan_type.tabMembers.Enabled = True
        frmLoan_ApplicationMain.cmdSearch.Enabled = True
        'frmloan_type.txtSearch.Enabled = True
        'frmloan_type.Width = "238"
        frmLoan_ApplicationMain.Height = "633"
        frmLoan_ApplicationMain.panelStep1.Visible = True
        frmLoan_ApplicationMain.panelstep2.Visible = True
        frmLoan_ApplicationMain.panelStep3.Visible = True
        frmLoan_ApplicationMain.panelstep2.Enabled = False
        frmLoan_ApplicationMain.panelStep3.Enabled = False
    End Sub
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Me.Close()
        frmLoan_ApplicationMain.panelStep1.Enabled = True
        frmLoan_ApplicationMain.panelstep2.Enabled = True
        frmLoan_ApplicationMain.panelStep3.Enabled = True
        'frmloan_type.lstNumbers.Enabled = False
        'frmloan_type.cmdYes.Enabled = False
        frmLoan_ApplicationMain.cmdNo.Enabled = False
    End Sub
End Class