﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoan_Approval
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GrdLoanSummary = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtReason = New System.Windows.Forms.TextBox()
        Me.gb1 = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.grdMemberList = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.txtSearch = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtDisapprovedLoans = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtApprovedLoans = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtTotalLoanBalance = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtSalary = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtCivilStatus = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtPosition = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtEmployeeNumber = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtFullName = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnDisapprove = New System.Windows.Forms.Button()
        Me.btnApprove = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnFind = New System.Windows.Forms.Button()
        Me.txtApprover = New System.Windows.Forms.TextBox()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.PanePanel5 = New WindowsApplication2.PanePanel()
        Me.Label21 = New System.Windows.Forms.Label()
        CType(Me.GrdLoanSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gb1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.grdMemberList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.PanePanel1.SuspendLayout()
        Me.PanePanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'GrdLoanSummary
        '
        Me.GrdLoanSummary.AllowUserToAddRows = False
        Me.GrdLoanSummary.AllowUserToDeleteRows = False
        Me.GrdLoanSummary.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GrdLoanSummary.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.GrdLoanSummary.BackgroundColor = System.Drawing.SystemColors.Window
        Me.GrdLoanSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GrdLoanSummary.Enabled = False
        Me.GrdLoanSummary.GridColor = System.Drawing.Color.Black
        Me.GrdLoanSummary.Location = New System.Drawing.Point(15, 362)
        Me.GrdLoanSummary.Name = "GrdLoanSummary"
        Me.GrdLoanSummary.ReadOnly = True
        Me.GrdLoanSummary.RowHeadersVisible = False
        Me.GrdLoanSummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GrdLoanSummary.Size = New System.Drawing.Size(941, 180)
        Me.GrdLoanSummary.TabIndex = 86
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(3, 550)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 16)
        Me.Label1.TabIndex = 64
        Me.Label1.Text = "Reason:"
        '
        'txtReason
        '
        Me.txtReason.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtReason.BackColor = System.Drawing.Color.Silver
        Me.txtReason.Location = New System.Drawing.Point(75, 547)
        Me.txtReason.Name = "txtReason"
        Me.txtReason.Size = New System.Drawing.Size(522, 21)
        Me.txtReason.TabIndex = 63
        '
        'gb1
        '
        Me.gb1.Controls.Add(Me.Label5)
        Me.gb1.Controls.Add(Me.GroupBox1)
        Me.gb1.Controls.Add(Me.GroupBox2)
        Me.gb1.Enabled = False
        Me.gb1.Location = New System.Drawing.Point(14, 76)
        Me.gb1.Name = "gb1"
        Me.gb1.Size = New System.Drawing.Size(941, 253)
        Me.gb1.TabIndex = 87
        Me.gb1.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label5.Location = New System.Drawing.Point(12, 17)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(132, 16)
        Me.Label5.TabIndex = 87
        Me.Label5.Text = "Search Member ID:"
        Me.Label5.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.grdMemberList)
        Me.GroupBox1.Controls.Add(Me.Panel1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 10)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(458, 240)
        Me.GroupBox1.TabIndex = 106
        Me.GroupBox1.TabStop = False
        '
        'grdMemberList
        '
        Me.grdMemberList.AllowUserToAddRows = False
        Me.grdMemberList.AllowUserToDeleteRows = False
        Me.grdMemberList.AllowUserToResizeColumns = False
        Me.grdMemberList.AllowUserToResizeRows = False
        Me.grdMemberList.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdMemberList.BackgroundColor = System.Drawing.SystemColors.Window
        Me.grdMemberList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdMemberList.Enabled = False
        Me.grdMemberList.GridColor = System.Drawing.Color.Black
        Me.grdMemberList.Location = New System.Drawing.Point(12, 57)
        Me.grdMemberList.Name = "grdMemberList"
        Me.grdMemberList.ReadOnly = True
        Me.grdMemberList.RowHeadersVisible = False
        Me.grdMemberList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdMemberList.Size = New System.Drawing.Size(440, 177)
        Me.grdMemberList.TabIndex = 87
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnSearch)
        Me.Panel1.Controls.Add(Me.txtSearch)
        Me.Panel1.Location = New System.Drawing.Point(8, 11)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(313, 40)
        Me.Panel1.TabIndex = 86
        '
        'btnSearch
        '
        Me.btnSearch.BackColor = System.Drawing.Color.White
        Me.btnSearch.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearch.Image = Global.WindowsApplication2.My.Resources.Resources.PrintPreview
        Me.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSearch.Location = New System.Drawing.Point(4, 10)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(170, 25)
        Me.btnSearch.TabIndex = 59
        Me.btnSearch.Text = "View Loans to Approve"
        Me.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSearch.UseVisualStyleBackColor = False
        '
        'txtSearch
        '
        Me.txtSearch.BackColor = System.Drawing.Color.Silver
        Me.txtSearch.Location = New System.Drawing.Point(3, 10)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(192, 21)
        Me.txtSearch.TabIndex = 38
        Me.txtSearch.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.txtDisapprovedLoans)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.txtApprovedLoans)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.txtTotalLoanBalance)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.txtSalary)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.txtCivilStatus)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.txtPosition)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.txtEmployeeNumber)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.txtFullName)
        Me.GroupBox2.Location = New System.Drawing.Point(476, 10)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(458, 241)
        Me.GroupBox2.TabIndex = 109
        Me.GroupBox2.TabStop = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(7, 205)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(154, 13)
        Me.Label14.TabIndex = 121
        Me.Label14.Text = "No. of disapproved loans:"
        '
        'txtDisapprovedLoans
        '
        Me.txtDisapprovedLoans.BackColor = System.Drawing.Color.Silver
        Me.txtDisapprovedLoans.Location = New System.Drawing.Point(160, 197)
        Me.txtDisapprovedLoans.Name = "txtDisapprovedLoans"
        Me.txtDisapprovedLoans.ReadOnly = True
        Me.txtDisapprovedLoans.Size = New System.Drawing.Size(276, 21)
        Me.txtDisapprovedLoans.TabIndex = 120
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(22, 181)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(138, 13)
        Me.Label15.TabIndex = 119
        Me.Label15.Text = "No. of approved loans:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(7, -1)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(94, 13)
        Me.Label8.TabIndex = 91
        Me.Label8.Text = "Member Details"
        '
        'txtApprovedLoans
        '
        Me.txtApprovedLoans.BackColor = System.Drawing.Color.Silver
        Me.txtApprovedLoans.Location = New System.Drawing.Point(160, 170)
        Me.txtApprovedLoans.Name = "txtApprovedLoans"
        Me.txtApprovedLoans.ReadOnly = True
        Me.txtApprovedLoans.Size = New System.Drawing.Size(276, 21)
        Me.txtApprovedLoans.TabIndex = 118
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(34, 151)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(120, 13)
        Me.Label13.TabIndex = 117
        Me.Label13.Text = "Total Loan Balance:"
        '
        'txtTotalLoanBalance
        '
        Me.txtTotalLoanBalance.BackColor = System.Drawing.Color.Silver
        Me.txtTotalLoanBalance.Location = New System.Drawing.Point(160, 143)
        Me.txtTotalLoanBalance.Name = "txtTotalLoanBalance"
        Me.txtTotalLoanBalance.ReadOnly = True
        Me.txtTotalLoanBalance.Size = New System.Drawing.Size(276, 21)
        Me.txtTotalLoanBalance.TabIndex = 116
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(105, 124)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(49, 13)
        Me.Label12.TabIndex = 115
        Me.Label12.Text = "Salary:"
        '
        'txtSalary
        '
        Me.txtSalary.BackColor = System.Drawing.Color.Silver
        Me.txtSalary.Location = New System.Drawing.Point(160, 116)
        Me.txtSalary.Name = "txtSalary"
        Me.txtSalary.ReadOnly = True
        Me.txtSalary.Size = New System.Drawing.Size(276, 21)
        Me.txtSalary.TabIndex = 114
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(77, 99)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(77, 13)
        Me.Label11.TabIndex = 113
        Me.Label11.Text = "Civil Status:"
        '
        'txtCivilStatus
        '
        Me.txtCivilStatus.BackColor = System.Drawing.Color.Silver
        Me.txtCivilStatus.Location = New System.Drawing.Point(160, 91)
        Me.txtCivilStatus.Name = "txtCivilStatus"
        Me.txtCivilStatus.ReadOnly = True
        Me.txtCivilStatus.Size = New System.Drawing.Size(276, 21)
        Me.txtCivilStatus.TabIndex = 112
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(98, 73)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(56, 13)
        Me.Label10.TabIndex = 111
        Me.Label10.Text = "Position:"
        '
        'txtPosition
        '
        Me.txtPosition.BackColor = System.Drawing.Color.Silver
        Me.txtPosition.Location = New System.Drawing.Point(160, 65)
        Me.txtPosition.Name = "txtPosition"
        Me.txtPosition.ReadOnly = True
        Me.txtPosition.Size = New System.Drawing.Size(276, 21)
        Me.txtPosition.TabIndex = 110
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(101, 18)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(53, 13)
        Me.Label9.TabIndex = 109
        Me.Label9.Text = "ID No. :"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtEmployeeNumber
        '
        Me.txtEmployeeNumber.BackColor = System.Drawing.Color.Silver
        Me.txtEmployeeNumber.Location = New System.Drawing.Point(160, 15)
        Me.txtEmployeeNumber.Name = "txtEmployeeNumber"
        Me.txtEmployeeNumber.ReadOnly = True
        Me.txtEmployeeNumber.Size = New System.Drawing.Size(276, 21)
        Me.txtEmployeeNumber.TabIndex = 108
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(105, 40)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(49, 13)
        Me.Label7.TabIndex = 107
        Me.Label7.Text = "Name :"
        '
        'txtFullName
        '
        Me.txtFullName.BackColor = System.Drawing.Color.Silver
        Me.txtFullName.Location = New System.Drawing.Point(160, 38)
        Me.txtFullName.Name = "txtFullName"
        Me.txtFullName.ReadOnly = True
        Me.txtFullName.Size = New System.Drawing.Size(276, 21)
        Me.txtFullName.TabIndex = 106
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(75, 16)
        Me.Label2.TabIndex = 108
        Me.Label2.Text = "Approver:"
        '
        'btnDisapprove
        '
        Me.btnDisapprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDisapprove.BackColor = System.Drawing.Color.White
        Me.btnDisapprove.Enabled = False
        Me.btnDisapprove.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDisapprove.Image = Global.WindowsApplication2.My.Resources.Resources.button_cancel
        Me.btnDisapprove.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDisapprove.Location = New System.Drawing.Point(729, 546)
        Me.btnDisapprove.Name = "btnDisapprove"
        Me.btnDisapprove.Size = New System.Drawing.Size(118, 25)
        Me.btnDisapprove.TabIndex = 92
        Me.btnDisapprove.Text = "Disapprove"
        Me.btnDisapprove.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDisapprove.UseVisualStyleBackColor = False
        '
        'btnApprove
        '
        Me.btnApprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApprove.BackColor = System.Drawing.Color.White
        Me.btnApprove.Enabled = False
        Me.btnApprove.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApprove.Image = Global.WindowsApplication2.My.Resources.Resources.apply
        Me.btnApprove.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnApprove.Location = New System.Drawing.Point(604, 546)
        Me.btnApprove.Name = "btnApprove"
        Me.btnApprove.Size = New System.Drawing.Size(118, 25)
        Me.btnApprove.TabIndex = 91
        Me.btnApprove.Text = "Approve"
        Me.btnApprove.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnApprove.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = Global.WindowsApplication2.My.Resources.Resources.eventlogError
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(854, 546)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(101, 25)
        Me.btnClose.TabIndex = 93
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnFind)
        Me.GroupBox3.Controls.Add(Me.txtApprover)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Location = New System.Drawing.Point(14, 42)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(941, 38)
        Me.GroupBox3.TabIndex = 94
        Me.GroupBox3.TabStop = False
        '
        'btnFind
        '
        Me.btnFind.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFind.BackColor = System.Drawing.Color.White
        Me.btnFind.Location = New System.Drawing.Point(419, 11)
        Me.btnFind.Name = "btnFind"
        Me.btnFind.Size = New System.Drawing.Size(44, 23)
        Me.btnFind.TabIndex = 110
        Me.btnFind.Text = "..."
        Me.btnFind.UseVisualStyleBackColor = False
        '
        'txtApprover
        '
        Me.txtApprover.BackColor = System.Drawing.Color.Silver
        Me.txtApprover.Location = New System.Drawing.Point(106, 12)
        Me.txtApprover.Name = "txtApprover"
        Me.txtApprover.ReadOnly = True
        Me.txtApprover.Size = New System.Drawing.Size(305, 21)
        Me.txtApprover.TabIndex = 109
        '
        'PanePanel1
        '
        Me.PanePanel1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.Label4)
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel1.Location = New System.Drawing.Point(1, 331)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(969, 26)
        Me.PanePanel1.TabIndex = 90
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(7, 3)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(299, 19)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "Choose loan to approve/disapprove"
        '
        'PanePanel5
        '
        Me.PanePanel5.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel5.Controls.Add(Me.Label21)
        Me.PanePanel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel5.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel5.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel5.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel5.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel5.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel5.Name = "PanePanel5"
        Me.PanePanel5.Size = New System.Drawing.Size(969, 36)
        Me.PanePanel5.TabIndex = 89
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.BackColor = System.Drawing.Color.Transparent
        Me.Label21.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label21.ForeColor = System.Drawing.Color.Black
        Me.Label21.Location = New System.Drawing.Point(3, 8)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(120, 19)
        Me.Label21.TabIndex = 14
        Me.Label21.Text = "Loan Approval"
        '
        'frmLoan_Approval
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(969, 572)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnDisapprove)
        Me.Controls.Add(Me.btnApprove)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.PanePanel5)
        Me.Controls.Add(Me.gb1)
        Me.Controls.Add(Me.GrdLoanSummary)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtReason)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmLoan_Approval"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Loan Approval"
        CType(Me.GrdLoanSummary, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gb1.ResumeLayout(False)
        Me.gb1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.grdMemberList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.PanePanel5.ResumeLayout(False)
        Me.PanePanel5.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GrdLoanSummary As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtReason As System.Windows.Forms.TextBox
    Friend WithEvents gb1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents PanePanel5 As WindowsApplication2.PanePanel
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnDisapprove As System.Windows.Forms.Button
    Friend WithEvents btnApprove As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents grdMemberList As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtDisapprovedLoans As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtApprovedLoans As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtTotalLoanBalance As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtSalary As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtCivilStatus As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtPosition As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtEmployeeNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtFullName As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnFind As System.Windows.Forms.Button
    Friend WithEvents txtApprover As System.Windows.Forms.TextBox
End Class
