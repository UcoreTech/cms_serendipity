<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoan_ApplicationComputation
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TxtEmpName = New System.Windows.Forms.TextBox()
        Me.txtEmpNo = New System.Windows.Forms.TextBox()
        Me.lblEmpNo = New System.Windows.Forms.Label()
        Me.lblEmpName = New System.Windows.Forms.Label()
        Me.lblloantype = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtIntRefundRestruc = New System.Windows.Forms.TextBox()
        Me.txtTotalPreterminatedBal = New System.Windows.Forms.TextBox()
        Me.txtTotalIntrefundNetpreterm = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtTotalIntRefunds = New System.Windows.Forms.TextBox()
        Me.txtInterestAmt = New System.Windows.Forms.TextBox()
        Me.lblIntRefund = New System.Windows.Forms.Label()
        Me.txtLAppDate = New System.Windows.Forms.TextBox()
        Me.lblCapitalShare = New System.Windows.Forms.Label()
        Me.txtClassification = New System.Windows.Forms.TextBox()
        Me.txtCapitalShare = New System.Windows.Forms.TextBox()
        Me.lblServiceFee = New System.Windows.Forms.Label()
        Me.txtLType = New System.Windows.Forms.TextBox()
        Me.txtTerms = New System.Windows.Forms.TextBox()
        Me.lblInterest = New System.Windows.Forms.Label()
        Me.txtServicefee = New System.Windows.Forms.TextBox()
        Me.txtPrincipalAmt = New System.Windows.Forms.TextBox()
        Me.lblPrincipal = New System.Windows.Forms.Label()
        Me.lblApplicationDate = New System.Windows.Forms.Label()
        Me.lblTerms = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblType = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblClassification = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.chkStandard = New System.Windows.Forms.RadioButton()
        Me.chkDeductinPayroll = New System.Windows.Forms.RadioButton()
        Me.lblIntRefundofRestructure = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtAnnulInterest = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtEffectiveRate = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.dtFirstPayment = New System.Windows.Forms.DateTimePicker()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtAmorDateEnd = New System.Windows.Forms.DateTimePicker()
        Me.txtAmorDateStart = New System.Windows.Forms.DateTimePicker()
        Me.txtpreterm = New System.Windows.Forms.TextBox()
        Me.txtrestrucdiff = New System.Windows.Forms.TextBox()
        Me.txtoriginalintrefundrestruc = New System.Windows.Forms.TextBox()
        Me.txtrestrucbal = New System.Windows.Forms.TextBox()
        Me.txtpretermbal = New System.Windows.Forms.TextBox()
        Me.txtrestrucbalanceafterchange = New System.Windows.Forms.TextBox()
        Me.txtHiddenAmort = New System.Windows.Forms.TextBox()
        Me.txtMonthlyAmor = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnRefreshAmort = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.dgvDeductions = New System.Windows.Forms.DataGridView()
        Me.txtTotalInterestINcome = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtMissedPayments = New System.Windows.Forms.TextBox()
        Me.txtNetProceeds = New System.Windows.Forms.TextBox()
        Me.txtTotalPenalties = New System.Windows.Forms.TextBox()
        Me.txtNetPreterminatedloan = New System.Windows.Forms.TextBox()
        Me.txtservicefeedetails = New System.Windows.Forms.TextBox()
        Me.txtPrincipaldetails = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.chkCheck = New System.Windows.Forms.CheckBox()
        Me.chkATM = New System.Windows.Forms.CheckBox()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.panelBottom = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnPrintForm = New System.Windows.Forms.Button()
        Me.btnPrintDisclousure = New System.Windows.Forms.Button()
        Me.btnPrinAgreement = New System.Windows.Forms.Button()
        Me.txtTotUnearned = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.chkCash = New System.Windows.Forms.CheckBox()
        Me.dgvList = New System.Windows.Forms.DataGridView()
        Me.txtTotAmortization = New System.Windows.Forms.TextBox()
        Me.txtTotServiceFee = New System.Windows.Forms.TextBox()
        Me.txtTotInterest = New System.Windows.Forms.TextBox()
        Me.txtTotPrincipal = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.btnPost = New System.Windows.Forms.Button()
        Me.PanePanel3 = New WindowsApplication2.PanePanel()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.PanePanel2 = New WindowsApplication2.PanePanel()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.cmdSubmit = New System.Windows.Forms.Button()
        Me.cmdCancel = New System.Windows.Forms.Button()
        Me.TipAmort = New System.Windows.Forms.ToolTip(Me.components)
        Me.Label20 = New System.Windows.Forms.Label()
        Me.lblwhatdouwant = New System.Windows.Forms.Label()
        Me.lblSteptwo = New System.Windows.Forms.Label()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.dgvDeductions, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.panelBottom.SuspendLayout()
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanePanel3.SuspendLayout()
        Me.PanePanel2.SuspendLayout()
        Me.PanePanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TxtEmpName)
        Me.GroupBox1.Controls.Add(Me.txtEmpNo)
        Me.GroupBox1.Controls.Add(Me.lblEmpNo)
        Me.GroupBox1.Controls.Add(Me.lblEmpName)
        Me.GroupBox1.Controls.Add(Me.lblloantype)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(357, 81)
        Me.GroupBox1.TabIndex = 28
        Me.GroupBox1.TabStop = False
        '
        'TxtEmpName
        '
        Me.TxtEmpName.BackColor = System.Drawing.Color.White
        Me.TxtEmpName.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtEmpName.Location = New System.Drawing.Point(117, 55)
        Me.TxtEmpName.Name = "TxtEmpName"
        Me.TxtEmpName.ReadOnly = True
        Me.TxtEmpName.Size = New System.Drawing.Size(233, 22)
        Me.TxtEmpName.TabIndex = 4
        '
        'txtEmpNo
        '
        Me.txtEmpNo.BackColor = System.Drawing.Color.White
        Me.txtEmpNo.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmpNo.Location = New System.Drawing.Point(117, 32)
        Me.txtEmpNo.Name = "txtEmpNo"
        Me.txtEmpNo.ReadOnly = True
        Me.txtEmpNo.Size = New System.Drawing.Size(231, 23)
        Me.txtEmpNo.TabIndex = 3
        '
        'lblEmpNo
        '
        Me.lblEmpNo.AutoSize = True
        Me.lblEmpNo.Location = New System.Drawing.Point(10, 37)
        Me.lblEmpNo.Name = "lblEmpNo"
        Me.lblEmpNo.Size = New System.Drawing.Size(44, 13)
        Me.lblEmpNo.TabIndex = 2
        Me.lblEmpNo.Text = "ID No."
        '
        'lblEmpName
        '
        Me.lblEmpName.AutoSize = True
        Me.lblEmpName.Location = New System.Drawing.Point(10, 59)
        Me.lblEmpName.Name = "lblEmpName"
        Me.lblEmpName.Size = New System.Drawing.Size(77, 13)
        Me.lblEmpName.TabIndex = 1
        Me.lblEmpName.Text = "Client Name"
        '
        'lblloantype
        '
        Me.lblloantype.AutoSize = True
        Me.lblloantype.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblloantype.Location = New System.Drawing.Point(10, 12)
        Me.lblloantype.Name = "lblloantype"
        Me.lblloantype.Size = New System.Drawing.Size(20, 16)
        Me.lblloantype.TabIndex = 0
        Me.lblloantype.Text = "..."
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtIntRefundRestruc)
        Me.GroupBox2.Controls.Add(Me.txtTotalPreterminatedBal)
        Me.GroupBox2.Controls.Add(Me.txtTotalIntrefundNetpreterm)
        Me.GroupBox2.Controls.Add(Me.Label23)
        Me.GroupBox2.Controls.Add(Me.txtTotalIntRefunds)
        Me.GroupBox2.Controls.Add(Me.txtInterestAmt)
        Me.GroupBox2.Controls.Add(Me.lblIntRefund)
        Me.GroupBox2.Controls.Add(Me.txtLAppDate)
        Me.GroupBox2.Controls.Add(Me.lblCapitalShare)
        Me.GroupBox2.Controls.Add(Me.txtClassification)
        Me.GroupBox2.Controls.Add(Me.txtCapitalShare)
        Me.GroupBox2.Controls.Add(Me.lblServiceFee)
        Me.GroupBox2.Controls.Add(Me.txtLType)
        Me.GroupBox2.Controls.Add(Me.txtTerms)
        Me.GroupBox2.Controls.Add(Me.lblInterest)
        Me.GroupBox2.Controls.Add(Me.txtServicefee)
        Me.GroupBox2.Controls.Add(Me.txtPrincipalAmt)
        Me.GroupBox2.Controls.Add(Me.lblPrincipal)
        Me.GroupBox2.Controls.Add(Me.lblApplicationDate)
        Me.GroupBox2.Controls.Add(Me.lblTerms)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.lblType)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.lblClassification)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(5, 85)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(356, 143)
        Me.GroupBox2.TabIndex = 29
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Loan Details"
        '
        'txtIntRefundRestruc
        '
        Me.txtIntRefundRestruc.BackColor = System.Drawing.Color.White
        Me.txtIntRefundRestruc.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIntRefundRestruc.Location = New System.Drawing.Point(168, 142)
        Me.txtIntRefundRestruc.Name = "txtIntRefundRestruc"
        Me.txtIntRefundRestruc.ReadOnly = True
        Me.txtIntRefundRestruc.Size = New System.Drawing.Size(180, 20)
        Me.txtIntRefundRestruc.TabIndex = 29
        Me.txtIntRefundRestruc.TabStop = False
        Me.txtIntRefundRestruc.Text = "0.00"
        Me.txtIntRefundRestruc.Visible = False
        '
        'txtTotalPreterminatedBal
        '
        Me.txtTotalPreterminatedBal.BackColor = System.Drawing.Color.White
        Me.txtTotalPreterminatedBal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalPreterminatedBal.Location = New System.Drawing.Point(45, 182)
        Me.txtTotalPreterminatedBal.Name = "txtTotalPreterminatedBal"
        Me.txtTotalPreterminatedBal.Size = New System.Drawing.Size(116, 21)
        Me.txtTotalPreterminatedBal.TabIndex = 23
        Me.txtTotalPreterminatedBal.Text = "0.00"
        Me.txtTotalPreterminatedBal.Visible = False
        '
        'txtTotalIntrefundNetpreterm
        '
        Me.txtTotalIntrefundNetpreterm.BackColor = System.Drawing.Color.White
        Me.txtTotalIntrefundNetpreterm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalIntrefundNetpreterm.Location = New System.Drawing.Point(167, 182)
        Me.txtTotalIntrefundNetpreterm.Name = "txtTotalIntrefundNetpreterm"
        Me.txtTotalIntrefundNetpreterm.Size = New System.Drawing.Size(116, 21)
        Me.txtTotalIntrefundNetpreterm.TabIndex = 22
        Me.txtTotalIntrefundNetpreterm.Text = "0.00"
        Me.txtTotalIntrefundNetpreterm.Visible = False
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(10, 145)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(130, 13)
        Me.Label23.TabIndex = 27
        Me.Label23.Text = "Int. Refund Restructure :"
        Me.Label23.Visible = False
        '
        'txtTotalIntRefunds
        '
        Me.txtTotalIntRefunds.BackColor = System.Drawing.Color.White
        Me.txtTotalIntRefunds.Location = New System.Drawing.Point(167, 162)
        Me.txtTotalIntRefunds.Name = "txtTotalIntRefunds"
        Me.txtTotalIntRefunds.ReadOnly = True
        Me.txtTotalIntRefunds.Size = New System.Drawing.Size(180, 23)
        Me.txtTotalIntRefunds.TabIndex = 17
        Me.txtTotalIntRefunds.Text = "0.00"
        Me.txtTotalIntRefunds.Visible = False
        '
        'txtInterestAmt
        '
        Me.txtInterestAmt.Location = New System.Drawing.Point(301, 141)
        Me.txtInterestAmt.Name = "txtInterestAmt"
        Me.txtInterestAmt.ReadOnly = True
        Me.txtInterestAmt.Size = New System.Drawing.Size(126, 23)
        Me.txtInterestAmt.TabIndex = 14
        Me.txtInterestAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtInterestAmt.Visible = False
        '
        'lblIntRefund
        '
        Me.lblIntRefund.AutoSize = True
        Me.lblIntRefund.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIntRefund.Location = New System.Drawing.Point(169, 162)
        Me.lblIntRefund.Name = "lblIntRefund"
        Me.lblIntRefund.Size = New System.Drawing.Size(84, 13)
        Me.lblIntRefund.TabIndex = 26
        Me.lblIntRefund.Text = "Interest Refund"
        Me.lblIntRefund.Visible = False
        '
        'txtLAppDate
        '
        Me.txtLAppDate.BackColor = System.Drawing.Color.White
        Me.txtLAppDate.Location = New System.Drawing.Point(301, 17)
        Me.txtLAppDate.Name = "txtLAppDate"
        Me.txtLAppDate.ReadOnly = True
        Me.txtLAppDate.Size = New System.Drawing.Size(180, 23)
        Me.txtLAppDate.TabIndex = 9
        Me.txtLAppDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtLAppDate.Visible = False
        '
        'lblCapitalShare
        '
        Me.lblCapitalShare.AutoSize = True
        Me.lblCapitalShare.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCapitalShare.Location = New System.Drawing.Point(163, 127)
        Me.lblCapitalShare.Name = "lblCapitalShare"
        Me.lblCapitalShare.Size = New System.Drawing.Size(85, 13)
        Me.lblCapitalShare.TabIndex = 25
        Me.lblCapitalShare.Text = "Capital Share"
        Me.lblCapitalShare.Visible = False
        '
        'txtClassification
        '
        Me.txtClassification.BackColor = System.Drawing.Color.White
        Me.txtClassification.Location = New System.Drawing.Point(301, 41)
        Me.txtClassification.Name = "txtClassification"
        Me.txtClassification.ReadOnly = True
        Me.txtClassification.Size = New System.Drawing.Size(180, 23)
        Me.txtClassification.TabIndex = 10
        Me.txtClassification.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtClassification.Visible = False
        '
        'txtCapitalShare
        '
        Me.txtCapitalShare.BackColor = System.Drawing.Color.White
        Me.txtCapitalShare.Location = New System.Drawing.Point(301, 187)
        Me.txtCapitalShare.Name = "txtCapitalShare"
        Me.txtCapitalShare.ReadOnly = True
        Me.txtCapitalShare.Size = New System.Drawing.Size(180, 23)
        Me.txtCapitalShare.TabIndex = 16
        Me.txtCapitalShare.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCapitalShare.Visible = False
        '
        'lblServiceFee
        '
        Me.lblServiceFee.AutoSize = True
        Me.lblServiceFee.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServiceFee.Location = New System.Drawing.Point(163, 112)
        Me.lblServiceFee.Name = "lblServiceFee"
        Me.lblServiceFee.Size = New System.Drawing.Size(74, 13)
        Me.lblServiceFee.TabIndex = 24
        Me.lblServiceFee.Text = "Service Fee"
        Me.lblServiceFee.Visible = False
        '
        'txtLType
        '
        Me.txtLType.BackColor = System.Drawing.Color.White
        Me.txtLType.Location = New System.Drawing.Point(301, 65)
        Me.txtLType.Name = "txtLType"
        Me.txtLType.ReadOnly = True
        Me.txtLType.Size = New System.Drawing.Size(180, 23)
        Me.txtLType.TabIndex = 11
        Me.txtLType.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtLType.Visible = False
        '
        'txtTerms
        '
        Me.txtTerms.BackColor = System.Drawing.Color.White
        Me.txtTerms.Location = New System.Drawing.Point(301, 89)
        Me.txtTerms.Name = "txtTerms"
        Me.txtTerms.ReadOnly = True
        Me.txtTerms.Size = New System.Drawing.Size(180, 23)
        Me.txtTerms.TabIndex = 12
        Me.txtTerms.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTerms.Visible = False
        '
        'lblInterest
        '
        Me.lblInterest.AutoSize = True
        Me.lblInterest.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterest.Location = New System.Drawing.Point(163, 97)
        Me.lblInterest.Name = "lblInterest"
        Me.lblInterest.Size = New System.Drawing.Size(52, 13)
        Me.lblInterest.TabIndex = 23
        Me.lblInterest.Text = "Interest"
        '
        'txtServicefee
        '
        Me.txtServicefee.Location = New System.Drawing.Point(301, 162)
        Me.txtServicefee.Name = "txtServicefee"
        Me.txtServicefee.ReadOnly = True
        Me.txtServicefee.Size = New System.Drawing.Size(180, 23)
        Me.txtServicefee.TabIndex = 15
        Me.txtServicefee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtServicefee.Visible = False
        '
        'txtPrincipalAmt
        '
        Me.txtPrincipalAmt.BackColor = System.Drawing.Color.White
        Me.txtPrincipalAmt.Location = New System.Drawing.Point(301, 114)
        Me.txtPrincipalAmt.Name = "txtPrincipalAmt"
        Me.txtPrincipalAmt.ReadOnly = True
        Me.txtPrincipalAmt.Size = New System.Drawing.Size(180, 23)
        Me.txtPrincipalAmt.TabIndex = 13
        Me.txtPrincipalAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPrincipalAmt.Visible = False
        '
        'lblPrincipal
        '
        Me.lblPrincipal.AutoSize = True
        Me.lblPrincipal.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrincipal.Location = New System.Drawing.Point(163, 82)
        Me.lblPrincipal.Name = "lblPrincipal"
        Me.lblPrincipal.Size = New System.Drawing.Size(55, 13)
        Me.lblPrincipal.TabIndex = 22
        Me.lblPrincipal.Text = "Principal"
        '
        'lblApplicationDate
        '
        Me.lblApplicationDate.AutoSize = True
        Me.lblApplicationDate.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicationDate.Location = New System.Drawing.Point(163, 22)
        Me.lblApplicationDate.Name = "lblApplicationDate"
        Me.lblApplicationDate.Size = New System.Drawing.Size(131, 13)
        Me.lblApplicationDate.TabIndex = 18
        Me.lblApplicationDate.Text = "Loan Application Date"
        '
        'lblTerms
        '
        Me.lblTerms.AutoSize = True
        Me.lblTerms.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTerms.Location = New System.Drawing.Point(163, 67)
        Me.lblTerms.Name = "lblTerms"
        Me.lblTerms.Size = New System.Drawing.Size(43, 13)
        Me.lblTerms.TabIndex = 21
        Me.lblTerms.Text = "Terms"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(71, 126)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(94, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Capital Share :"
        Me.Label8.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(80, 111)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(83, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Service Fee :"
        Me.Label7.Visible = False
        '
        'lblType
        '
        Me.lblType.AutoSize = True
        Me.lblType.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblType.Location = New System.Drawing.Point(163, 52)
        Me.lblType.Name = "lblType"
        Me.lblType.Size = New System.Drawing.Size(66, 13)
        Me.lblType.TabIndex = 20
        Me.lblType.Text = "Loan Type"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(17, 163)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(123, 13)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Total Interest Refunds :"
        Me.Label9.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(100, 96)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Interest :"
        '
        'lblClassification
        '
        Me.lblClassification.AutoSize = True
        Me.lblClassification.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClassification.Location = New System.Drawing.Point(163, 37)
        Me.lblClassification.Name = "lblClassification"
        Me.lblClassification.Size = New System.Drawing.Size(82, 13)
        Me.lblClassification.TabIndex = 19
        Me.lblClassification.Text = "Classification"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(100, 81)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Principal :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(64, 66)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(100, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Terms (Month) :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(87, 51)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(75, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Loan Type :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(73, 36)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(91, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Classification :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(24, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(140, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Loan Application Date :"
        '
        'chkStandard
        '
        Me.chkStandard.AutoSize = True
        Me.chkStandard.Location = New System.Drawing.Point(8, 206)
        Me.chkStandard.Name = "chkStandard"
        Me.chkStandard.Size = New System.Drawing.Size(127, 20)
        Me.chkStandard.TabIndex = 31
        Me.chkStandard.TabStop = True
        Me.chkStandard.Text = "Direct Payment"
        Me.chkStandard.UseVisualStyleBackColor = True
        '
        'chkDeductinPayroll
        '
        Me.chkDeductinPayroll.AutoSize = True
        Me.chkDeductinPayroll.Location = New System.Drawing.Point(142, 205)
        Me.chkDeductinPayroll.Name = "chkDeductinPayroll"
        Me.chkDeductinPayroll.Size = New System.Drawing.Size(138, 20)
        Me.chkDeductinPayroll.TabIndex = 30
        Me.chkDeductinPayroll.TabStop = True
        Me.chkDeductinPayroll.Text = "Salary Deduction"
        Me.chkDeductinPayroll.UseVisualStyleBackColor = True
        '
        'lblIntRefundofRestructure
        '
        Me.lblIntRefundofRestructure.AutoSize = True
        Me.lblIntRefundofRestructure.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIntRefundofRestructure.Location = New System.Drawing.Point(28, 237)
        Me.lblIntRefundofRestructure.Name = "lblIntRefundofRestructure"
        Me.lblIntRefundofRestructure.Size = New System.Drawing.Size(99, 13)
        Me.lblIntRefundofRestructure.TabIndex = 28
        Me.lblIntRefundofRestructure.Text = "Int Refund Restruc"
        Me.lblIntRefundofRestructure.Visible = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtAnnulInterest)
        Me.GroupBox3.Controls.Add(Me.Label25)
        Me.GroupBox3.Controls.Add(Me.chkStandard)
        Me.GroupBox3.Controls.Add(Me.txtEffectiveRate)
        Me.GroupBox3.Controls.Add(Me.chkDeductinPayroll)
        Me.GroupBox3.Controls.Add(Me.Label24)
        Me.GroupBox3.Controls.Add(Me.dtFirstPayment)
        Me.GroupBox3.Controls.Add(Me.Label22)
        Me.GroupBox3.Controls.Add(Me.txtAmorDateEnd)
        Me.GroupBox3.Controls.Add(Me.txtAmorDateStart)
        Me.GroupBox3.Controls.Add(Me.txtpreterm)
        Me.GroupBox3.Controls.Add(Me.txtrestrucdiff)
        Me.GroupBox3.Controls.Add(Me.txtoriginalintrefundrestruc)
        Me.GroupBox3.Controls.Add(Me.txtrestrucbal)
        Me.GroupBox3.Controls.Add(Me.txtpretermbal)
        Me.GroupBox3.Controls.Add(Me.txtrestrucbalanceafterchange)
        Me.GroupBox3.Controls.Add(Me.txtHiddenAmort)
        Me.GroupBox3.Controls.Add(Me.lblIntRefundofRestructure)
        Me.GroupBox3.Controls.Add(Me.txtMonthlyAmor)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(367, 0)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(278, 228)
        Me.GroupBox3.TabIndex = 30
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Amortization Details"
        '
        'txtAnnulInterest
        '
        Me.txtAnnulInterest.BackColor = System.Drawing.Color.White
        Me.txtAnnulInterest.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAnnulInterest.Location = New System.Drawing.Point(140, 115)
        Me.txtAnnulInterest.Name = "txtAnnulInterest"
        Me.txtAnnulInterest.ReadOnly = True
        Me.txtAnnulInterest.Size = New System.Drawing.Size(128, 23)
        Me.txtAnnulInterest.TabIndex = 44
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(7, 118)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(116, 16)
        Me.Label25.TabIndex = 43
        Me.Label25.Text = "Annual Interest:"
        '
        'txtEffectiveRate
        '
        Me.txtEffectiveRate.BackColor = System.Drawing.Color.White
        Me.txtEffectiveRate.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEffectiveRate.Location = New System.Drawing.Point(140, 143)
        Me.txtEffectiveRate.Name = "txtEffectiveRate"
        Me.txtEffectiveRate.ReadOnly = True
        Me.txtEffectiveRate.Size = New System.Drawing.Size(128, 23)
        Me.txtEffectiveRate.TabIndex = 42
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(16, 149)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(108, 16)
        Me.Label24.TabIndex = 41
        Me.Label24.Text = "Effective Rate:"
        '
        'dtFirstPayment
        '
        Me.dtFirstPayment.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtFirstPayment.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFirstPayment.Location = New System.Drawing.Point(112, 51)
        Me.dtFirstPayment.Name = "dtFirstPayment"
        Me.dtFirstPayment.Size = New System.Drawing.Size(156, 22)
        Me.dtFirstPayment.TabIndex = 39
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(21, 56)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(77, 13)
        Me.Label22.TabIndex = 38
        Me.Label22.Text = "First Payment:"
        '
        'txtAmorDateEnd
        '
        Me.txtAmorDateEnd.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmorDateEnd.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtAmorDateEnd.Location = New System.Drawing.Point(113, 82)
        Me.txtAmorDateEnd.Name = "txtAmorDateEnd"
        Me.txtAmorDateEnd.Size = New System.Drawing.Size(156, 22)
        Me.txtAmorDateEnd.TabIndex = 37
        '
        'txtAmorDateStart
        '
        Me.txtAmorDateStart.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmorDateStart.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtAmorDateStart.Location = New System.Drawing.Point(113, 22)
        Me.txtAmorDateStart.Name = "txtAmorDateStart"
        Me.txtAmorDateStart.Size = New System.Drawing.Size(156, 22)
        Me.txtAmorDateStart.TabIndex = 36
        '
        'txtpreterm
        '
        Me.txtpreterm.BackColor = System.Drawing.Color.White
        Me.txtpreterm.Location = New System.Drawing.Point(31, 182)
        Me.txtpreterm.Name = "txtpreterm"
        Me.txtpreterm.Size = New System.Drawing.Size(116, 23)
        Me.txtpreterm.TabIndex = 34
        Me.txtpreterm.Text = "0.00"
        Me.txtpreterm.Visible = False
        '
        'txtrestrucdiff
        '
        Me.txtrestrucdiff.BackColor = System.Drawing.Color.White
        Me.txtrestrucdiff.Location = New System.Drawing.Point(154, 204)
        Me.txtrestrucdiff.Name = "txtrestrucdiff"
        Me.txtrestrucdiff.Size = New System.Drawing.Size(116, 23)
        Me.txtrestrucdiff.TabIndex = 33
        Me.txtrestrucdiff.Text = "0.00"
        Me.txtrestrucdiff.Visible = False
        '
        'txtoriginalintrefundrestruc
        '
        Me.txtoriginalintrefundrestruc.BackColor = System.Drawing.Color.White
        Me.txtoriginalintrefundrestruc.Location = New System.Drawing.Point(31, 209)
        Me.txtoriginalintrefundrestruc.Name = "txtoriginalintrefundrestruc"
        Me.txtoriginalintrefundrestruc.Size = New System.Drawing.Size(87, 23)
        Me.txtoriginalintrefundrestruc.TabIndex = 32
        Me.txtoriginalintrefundrestruc.Text = "0.00"
        Me.txtoriginalintrefundrestruc.Visible = False
        '
        'txtrestrucbal
        '
        Me.txtrestrucbal.BackColor = System.Drawing.Color.White
        Me.txtrestrucbal.Location = New System.Drawing.Point(154, 230)
        Me.txtrestrucbal.Name = "txtrestrucbal"
        Me.txtrestrucbal.Size = New System.Drawing.Size(116, 23)
        Me.txtrestrucbal.TabIndex = 31
        Me.txtrestrucbal.Text = "0.00"
        Me.txtrestrucbal.Visible = False
        '
        'txtpretermbal
        '
        Me.txtpretermbal.BackColor = System.Drawing.Color.White
        Me.txtpretermbal.Location = New System.Drawing.Point(154, 258)
        Me.txtpretermbal.Name = "txtpretermbal"
        Me.txtpretermbal.Size = New System.Drawing.Size(116, 23)
        Me.txtpretermbal.TabIndex = 30
        Me.txtpretermbal.Text = "0.00"
        Me.txtpretermbal.Visible = False
        '
        'txtrestrucbalanceafterchange
        '
        Me.txtrestrucbalanceafterchange.BackColor = System.Drawing.Color.White
        Me.txtrestrucbalanceafterchange.Location = New System.Drawing.Point(29, 258)
        Me.txtrestrucbalanceafterchange.Name = "txtrestrucbalanceafterchange"
        Me.txtrestrucbalanceafterchange.Size = New System.Drawing.Size(116, 23)
        Me.txtrestrucbalanceafterchange.TabIndex = 29
        Me.txtrestrucbalanceafterchange.Text = "0.00"
        Me.txtrestrucbalanceafterchange.Visible = False
        '
        'txtHiddenAmort
        '
        Me.txtHiddenAmort.BackColor = System.Drawing.Color.White
        Me.txtHiddenAmort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHiddenAmort.Location = New System.Drawing.Point(163, 188)
        Me.txtHiddenAmort.Name = "txtHiddenAmort"
        Me.txtHiddenAmort.ReadOnly = True
        Me.txtHiddenAmort.Size = New System.Drawing.Size(117, 21)
        Me.txtHiddenAmort.TabIndex = 21
        Me.txtHiddenAmort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtHiddenAmort.Visible = False
        '
        'txtMonthlyAmor
        '
        Me.txtMonthlyAmor.BackColor = System.Drawing.Color.White
        Me.txtMonthlyAmor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMonthlyAmor.Location = New System.Drawing.Point(90, 177)
        Me.txtMonthlyAmor.Name = "txtMonthlyAmor"
        Me.txtMonthlyAmor.ReadOnly = True
        Me.txtMonthlyAmor.Size = New System.Drawing.Size(117, 21)
        Me.txtMonthlyAmor.TabIndex = 18
        Me.txtMonthlyAmor.Text = "0.00"
        Me.txtMonthlyAmor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtMonthlyAmor.Visible = False
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(61, 144)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(180, 36)
        Me.Label13.TabIndex = 3
        Me.Label13.Text = "Subject to Change depending on Approval"
        Me.Label13.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(51, 87)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(51, 13)
        Me.Label12.TabIndex = 2
        Me.Label12.Text = "Maturity:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(22, 29)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(76, 13)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "Date Granted:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(0, 186)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(71, 13)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Amortization:"
        Me.Label10.Visible = False
        '
        'btnRefreshAmort
        '
        Me.btnRefreshAmort.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefreshAmort.ForeColor = System.Drawing.SystemColors.Highlight
        Me.btnRefreshAmort.Location = New System.Drawing.Point(3, 185)
        Me.btnRefreshAmort.Name = "btnRefreshAmort"
        Me.btnRefreshAmort.Size = New System.Drawing.Size(170, 23)
        Me.btnRefreshAmort.TabIndex = 40
        Me.btnRefreshAmort.Text = "Refresh Amortization"
        Me.btnRefreshAmort.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(98, 19)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(64, 13)
        Me.Label14.TabIndex = 4
        Me.Label14.Text = "Principal :"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(28, 79)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(68, 13)
        Me.Label16.TabIndex = 6
        Me.Label16.Text = "Service fee :"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(100, 204)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(75, 13)
        Me.Label17.TabIndex = 7
        Me.Label17.Text = "Net Proceeds:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(51, 145)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(50, 13)
        Me.Label18.TabIndex = 8
        Me.Label18.Text = "Penalty :"
        Me.Label18.Visible = False
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label15)
        Me.GroupBox4.Controls.Add(Me.dgvDeductions)
        Me.GroupBox4.Controls.Add(Me.txtTotalInterestINcome)
        Me.GroupBox4.Controls.Add(Me.Label21)
        Me.GroupBox4.Controls.Add(Me.txtMissedPayments)
        Me.GroupBox4.Controls.Add(Me.txtNetProceeds)
        Me.GroupBox4.Controls.Add(Me.txtTotalPenalties)
        Me.GroupBox4.Controls.Add(Me.txtNetPreterminatedloan)
        Me.GroupBox4.Controls.Add(Me.txtservicefeedetails)
        Me.GroupBox4.Controls.Add(Me.txtPrincipaldetails)
        Me.GroupBox4.Controls.Add(Me.Label19)
        Me.GroupBox4.Controls.Add(Me.Label14)
        Me.GroupBox4.Controls.Add(Me.Label18)
        Me.GroupBox4.Controls.Add(Me.Label17)
        Me.GroupBox4.Controls.Add(Me.Label16)
        Me.GroupBox4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(653, 0)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(319, 228)
        Me.GroupBox4.TabIndex = 31
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Net Proceed Details"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(11, 40)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(70, 13)
        Me.Label15.TabIndex = 29
        Me.Label15.Text = "Deductions"
        '
        'dgvDeductions
        '
        Me.dgvDeductions.AllowUserToAddRows = False
        Me.dgvDeductions.AllowUserToDeleteRows = False
        Me.dgvDeductions.AllowUserToResizeColumns = False
        Me.dgvDeductions.AllowUserToResizeRows = False
        Me.dgvDeductions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvDeductions.BackgroundColor = System.Drawing.Color.White
        Me.dgvDeductions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDeductions.Location = New System.Drawing.Point(6, 56)
        Me.dgvDeductions.Name = "dgvDeductions"
        Me.dgvDeductions.ReadOnly = True
        Me.dgvDeductions.RowHeadersVisible = False
        Me.dgvDeductions.Size = New System.Drawing.Size(307, 133)
        Me.dgvDeductions.TabIndex = 28
        '
        'txtTotalInterestINcome
        '
        Me.txtTotalInterestINcome.BackColor = System.Drawing.Color.White
        Me.txtTotalInterestINcome.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalInterestINcome.Location = New System.Drawing.Point(115, 109)
        Me.txtTotalInterestINcome.Name = "txtTotalInterestINcome"
        Me.txtTotalInterestINcome.ReadOnly = True
        Me.txtTotalInterestINcome.Size = New System.Drawing.Size(132, 21)
        Me.txtTotalInterestINcome.TabIndex = 27
        Me.txtTotalInterestINcome.Text = "0.00"
        Me.txtTotalInterestINcome.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(46, 113)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(53, 13)
        Me.Label21.TabIndex = 26
        Me.Label21.Text = "Interest :"
        '
        'txtMissedPayments
        '
        Me.txtMissedPayments.BackColor = System.Drawing.Color.White
        Me.txtMissedPayments.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMissedPayments.Location = New System.Drawing.Point(-42, 179)
        Me.txtMissedPayments.Name = "txtMissedPayments"
        Me.txtMissedPayments.ReadOnly = True
        Me.txtMissedPayments.Size = New System.Drawing.Size(132, 21)
        Me.txtMissedPayments.TabIndex = 25
        Me.txtMissedPayments.Text = "0.00"
        Me.txtMissedPayments.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtMissedPayments.Visible = False
        '
        'txtNetProceeds
        '
        Me.txtNetProceeds.BackColor = System.Drawing.Color.White
        Me.txtNetProceeds.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNetProceeds.Location = New System.Drawing.Point(181, 197)
        Me.txtNetProceeds.Name = "txtNetProceeds"
        Me.txtNetProceeds.ReadOnly = True
        Me.txtNetProceeds.Size = New System.Drawing.Size(132, 21)
        Me.txtNetProceeds.TabIndex = 23
        Me.txtNetProceeds.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalPenalties
        '
        Me.txtTotalPenalties.BackColor = System.Drawing.Color.White
        Me.txtTotalPenalties.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalPenalties.Location = New System.Drawing.Point(116, 140)
        Me.txtTotalPenalties.Name = "txtTotalPenalties"
        Me.txtTotalPenalties.ReadOnly = True
        Me.txtTotalPenalties.Size = New System.Drawing.Size(132, 21)
        Me.txtTotalPenalties.TabIndex = 22
        Me.txtTotalPenalties.Text = "0.00"
        Me.txtTotalPenalties.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalPenalties.Visible = False
        '
        'txtNetPreterminatedloan
        '
        Me.txtNetPreterminatedloan.BackColor = System.Drawing.Color.White
        Me.txtNetPreterminatedloan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNetPreterminatedloan.Location = New System.Drawing.Point(-45, 182)
        Me.txtNetPreterminatedloan.Name = "txtNetPreterminatedloan"
        Me.txtNetPreterminatedloan.ReadOnly = True
        Me.txtNetPreterminatedloan.Size = New System.Drawing.Size(132, 21)
        Me.txtNetPreterminatedloan.TabIndex = 21
        Me.txtNetPreterminatedloan.Text = "0.00"
        Me.txtNetPreterminatedloan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNetPreterminatedloan.Visible = False
        '
        'txtservicefeedetails
        '
        Me.txtservicefeedetails.BackColor = System.Drawing.Color.White
        Me.txtservicefeedetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtservicefeedetails.Location = New System.Drawing.Point(115, 76)
        Me.txtservicefeedetails.Name = "txtservicefeedetails"
        Me.txtservicefeedetails.ReadOnly = True
        Me.txtservicefeedetails.Size = New System.Drawing.Size(132, 21)
        Me.txtservicefeedetails.TabIndex = 20
        Me.txtservicefeedetails.Text = "0.00"
        Me.txtservicefeedetails.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPrincipaldetails
        '
        Me.txtPrincipaldetails.BackColor = System.Drawing.Color.White
        Me.txtPrincipaldetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrincipaldetails.Location = New System.Drawing.Point(167, 16)
        Me.txtPrincipaldetails.Name = "txtPrincipaldetails"
        Me.txtPrincipaldetails.ReadOnly = True
        Me.txtPrincipaldetails.Size = New System.Drawing.Size(132, 21)
        Me.txtPrincipaldetails.TabIndex = 19
        Me.txtPrincipaldetails.Text = "0.00"
        Me.txtPrincipaldetails.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(-3, 185)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(78, 13)
        Me.Label19.TabIndex = 9
        Me.Label19.Text = "Net Proceeds :"
        Me.Label19.Visible = False
        '
        'chkCheck
        '
        Me.chkCheck.AutoSize = True
        Me.chkCheck.Location = New System.Drawing.Point(33, 270)
        Me.chkCheck.Name = "chkCheck"
        Me.chkCheck.Size = New System.Drawing.Size(62, 17)
        Me.chkCheck.TabIndex = 38
        Me.chkCheck.Text = "Check"
        Me.chkCheck.UseVisualStyleBackColor = True
        '
        'chkATM
        '
        Me.chkATM.AutoSize = True
        Me.chkATM.Location = New System.Drawing.Point(133, 271)
        Me.chkATM.Name = "chkATM"
        Me.chkATM.Size = New System.Drawing.Size(50, 17)
        Me.chkATM.TabIndex = 39
        Me.chkATM.Text = "ATM"
        Me.chkATM.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.White
        Me.FlowLayoutPanel1.Controls.Add(Me.Panel1)
        Me.FlowLayoutPanel1.Controls.Add(Me.panelBottom)
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(14, 32)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(1057, 609)
        Me.FlowLayoutPanel1.TabIndex = 40
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.GroupBox2)
        Me.Panel1.Controls.Add(Me.GroupBox4)
        Me.Panel1.Controls.Add(Me.GroupBox3)
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1042, 231)
        Me.Panel1.TabIndex = 0
        '
        'panelBottom
        '
        Me.panelBottom.Controls.Add(Me.Button1)
        Me.panelBottom.Controls.Add(Me.btnPrintForm)
        Me.panelBottom.Controls.Add(Me.btnPrintDisclousure)
        Me.panelBottom.Controls.Add(Me.btnPrinAgreement)
        Me.panelBottom.Controls.Add(Me.txtTotUnearned)
        Me.panelBottom.Controls.Add(Me.Label30)
        Me.panelBottom.Controls.Add(Me.chkCash)
        Me.panelBottom.Controls.Add(Me.dgvList)
        Me.panelBottom.Controls.Add(Me.txtTotAmortization)
        Me.panelBottom.Controls.Add(Me.txtTotServiceFee)
        Me.panelBottom.Controls.Add(Me.txtTotInterest)
        Me.panelBottom.Controls.Add(Me.txtTotPrincipal)
        Me.panelBottom.Controls.Add(Me.Label26)
        Me.panelBottom.Controls.Add(Me.btnPost)
        Me.panelBottom.Controls.Add(Me.PanePanel3)
        Me.panelBottom.Controls.Add(Me.chkATM)
        Me.panelBottom.Controls.Add(Me.PanePanel2)
        Me.panelBottom.Controls.Add(Me.btnRefreshAmort)
        Me.panelBottom.Controls.Add(Me.chkCheck)
        Me.panelBottom.Controls.Add(Me.cmdSubmit)
        Me.panelBottom.Controls.Add(Me.cmdCancel)
        Me.panelBottom.Location = New System.Drawing.Point(3, 240)
        Me.panelBottom.Name = "panelBottom"
        Me.panelBottom.Size = New System.Drawing.Size(1042, 366)
        Me.panelBottom.TabIndex = 2
        '
        'Button1
        '
        Me.Button1.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Button1.Location = New System.Drawing.Point(735, 225)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(145, 23)
        Me.Button1.TabIndex = 53
        Me.Button1.Text = "Add Other Payment"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnPrintForm
        '
        Me.btnPrintForm.Location = New System.Drawing.Point(521, 331)
        Me.btnPrintForm.Name = "btnPrintForm"
        Me.btnPrintForm.Size = New System.Drawing.Size(164, 23)
        Me.btnPrintForm.TabIndex = 52
        Me.btnPrintForm.Text = "Print Application Form"
        Me.btnPrintForm.UseVisualStyleBackColor = True
        '
        'btnPrintDisclousure
        '
        Me.btnPrintDisclousure.Location = New System.Drawing.Point(864, 331)
        Me.btnPrintDisclousure.Name = "btnPrintDisclousure"
        Me.btnPrintDisclousure.Size = New System.Drawing.Size(164, 23)
        Me.btnPrintDisclousure.TabIndex = 51
        Me.btnPrintDisclousure.Text = "Print Loan Disclosure"
        Me.btnPrintDisclousure.UseVisualStyleBackColor = True
        '
        'btnPrinAgreement
        '
        Me.btnPrinAgreement.Location = New System.Drawing.Point(694, 331)
        Me.btnPrinAgreement.Name = "btnPrinAgreement"
        Me.btnPrinAgreement.Size = New System.Drawing.Size(164, 23)
        Me.btnPrinAgreement.TabIndex = 50
        Me.btnPrinAgreement.Text = "Print Agreement"
        Me.btnPrinAgreement.UseVisualStyleBackColor = True
        '
        'txtTotUnearned
        '
        Me.txtTotUnearned.BackColor = System.Drawing.Color.White
        Me.txtTotUnearned.ForeColor = System.Drawing.Color.Red
        Me.txtTotUnearned.Location = New System.Drawing.Point(584, 219)
        Me.txtTotUnearned.Name = "txtTotUnearned"
        Me.txtTotUnearned.ReadOnly = True
        Me.txtTotUnearned.Size = New System.Drawing.Size(144, 21)
        Me.txtTotUnearned.TabIndex = 49
        Me.txtTotUnearned.Text = "0.00"
        Me.txtTotUnearned.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(435, 222)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(146, 13)
        Me.Label30.TabIndex = 48
        Me.Label30.Text = "Total Advance Interest :"
        '
        'chkCash
        '
        Me.chkCash.AutoSize = True
        Me.chkCash.Location = New System.Drawing.Point(218, 271)
        Me.chkCash.Name = "chkCash"
        Me.chkCash.Size = New System.Drawing.Size(55, 17)
        Me.chkCash.TabIndex = 47
        Me.chkCash.Text = "Cash"
        Me.chkCash.UseVisualStyleBackColor = True
        '
        'dgvList
        '
        Me.dgvList.AllowUserToAddRows = False
        Me.dgvList.AllowUserToDeleteRows = False
        Me.dgvList.AllowUserToResizeColumns = False
        Me.dgvList.AllowUserToResizeRows = False
        Me.dgvList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvList.BackgroundColor = System.Drawing.Color.White
        Me.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvList.GridColor = System.Drawing.Color.White
        Me.dgvList.Location = New System.Drawing.Point(0, 3)
        Me.dgvList.Name = "dgvList"
        Me.dgvList.ReadOnly = True
        Me.dgvList.RowHeadersVisible = False
        Me.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvList.Size = New System.Drawing.Size(1039, 176)
        Me.dgvList.TabIndex = 46
        '
        'txtTotAmortization
        '
        Me.txtTotAmortization.Location = New System.Drawing.Point(734, 187)
        Me.txtTotAmortization.Name = "txtTotAmortization"
        Me.txtTotAmortization.ReadOnly = True
        Me.txtTotAmortization.Size = New System.Drawing.Size(142, 21)
        Me.txtTotAmortization.TabIndex = 45
        Me.txtTotAmortization.Text = "0.00"
        Me.txtTotAmortization.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotServiceFee
        '
        Me.txtTotServiceFee.Location = New System.Drawing.Point(584, 187)
        Me.txtTotServiceFee.Name = "txtTotServiceFee"
        Me.txtTotServiceFee.ReadOnly = True
        Me.txtTotServiceFee.Size = New System.Drawing.Size(142, 21)
        Me.txtTotServiceFee.TabIndex = 44
        Me.txtTotServiceFee.Text = "0.00"
        Me.txtTotServiceFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotInterest
        '
        Me.txtTotInterest.Location = New System.Drawing.Point(433, 187)
        Me.txtTotInterest.Name = "txtTotInterest"
        Me.txtTotInterest.ReadOnly = True
        Me.txtTotInterest.Size = New System.Drawing.Size(144, 21)
        Me.txtTotInterest.TabIndex = 43
        Me.txtTotInterest.Text = "0.00"
        Me.txtTotInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotPrincipal
        '
        Me.txtTotPrincipal.Location = New System.Drawing.Point(275, 188)
        Me.txtTotPrincipal.Name = "txtTotPrincipal"
        Me.txtTotPrincipal.ReadOnly = True
        Me.txtTotPrincipal.Size = New System.Drawing.Size(154, 21)
        Me.txtTotPrincipal.TabIndex = 42
        Me.txtTotPrincipal.Text = "0.00"
        Me.txtTotPrincipal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(219, 194)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(50, 13)
        Me.Label26.TabIndex = 41
        Me.Label26.Text = "Totals: "
        '
        'btnPost
        '
        Me.btnPost.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.btnPost.Image = Global.WindowsApplication2.My.Resources.Resources.OK
        Me.btnPost.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPost.Location = New System.Drawing.Point(248, 326)
        Me.btnPost.Name = "btnPost"
        Me.btnPost.Size = New System.Drawing.Size(169, 32)
        Me.btnPost.TabIndex = 40
        Me.btnPost.Text = "Post Carry Forward"
        Me.btnPost.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPost.UseVisualStyleBackColor = True
        '
        'PanePanel3
        '
        Me.PanePanel3.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel3.Controls.Add(Me.Label28)
        Me.PanePanel3.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel3.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel3.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel3.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel3.Location = New System.Drawing.Point(5, 238)
        Me.PanePanel3.Name = "PanePanel3"
        Me.PanePanel3.Size = New System.Drawing.Size(413, 26)
        Me.PanePanel3.TabIndex = 37
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.BackColor = System.Drawing.Color.Transparent
        Me.Label28.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(58, 3)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(182, 18)
        Me.Label28.TabIndex = 1
        Me.Label28.Text = "Select Payment Type"
        '
        'PanePanel2
        '
        Me.PanePanel2.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel2.Controls.Add(Me.Label29)
        Me.PanePanel2.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel2.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel2.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel2.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel2.Location = New System.Drawing.Point(5, 294)
        Me.PanePanel2.Name = "PanePanel2"
        Me.PanePanel2.Size = New System.Drawing.Size(1023, 26)
        Me.PanePanel2.TabIndex = 34
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.BackColor = System.Drawing.Color.Transparent
        Me.Label29.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(63, 3)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(64, 18)
        Me.Label29.TabIndex = 2
        Me.Label29.Text = "Finish?"
        '
        'cmdSubmit
        '
        Me.cmdSubmit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSubmit.Image = Global.WindowsApplication2.My.Resources.Resources.lists
        Me.cmdSubmit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSubmit.Location = New System.Drawing.Point(23, 325)
        Me.cmdSubmit.Name = "cmdSubmit"
        Me.cmdSubmit.Size = New System.Drawing.Size(106, 33)
        Me.cmdSubmit.TabIndex = 35
        Me.cmdSubmit.Text = "Submit"
        Me.cmdSubmit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdSubmit.UseVisualStyleBackColor = True
        '
        'cmdCancel
        '
        Me.cmdCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdCancel.Image = Global.WindowsApplication2.My.Resources.Resources.button_cancel
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.Location = New System.Drawing.Point(136, 326)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(105, 32)
        Me.cmdCancel.TabIndex = 36
        Me.cmdCancel.Text = "Close"
        Me.cmdCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Black
        Me.Label20.Location = New System.Drawing.Point(11, 3)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(319, 19)
        Me.Label20.TabIndex = 37
        Me.Label20.Text = "Choose which payment do you prefer?"
        '
        'lblwhatdouwant
        '
        Me.lblwhatdouwant.AutoSize = True
        Me.lblwhatdouwant.BackColor = System.Drawing.Color.Transparent
        Me.lblwhatdouwant.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblwhatdouwant.ForeColor = System.Drawing.Color.Black
        Me.lblwhatdouwant.Location = New System.Drawing.Point(11, 3)
        Me.lblwhatdouwant.Name = "lblwhatdouwant"
        Me.lblwhatdouwant.Size = New System.Drawing.Size(213, 19)
        Me.lblwhatdouwant.TabIndex = 37
        Me.lblwhatdouwant.Text = "What do you want to do?"
        '
        'lblSteptwo
        '
        Me.lblSteptwo.AutoSize = True
        Me.lblSteptwo.BackColor = System.Drawing.Color.Transparent
        Me.lblSteptwo.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSteptwo.ForeColor = System.Drawing.Color.Black
        Me.lblSteptwo.Location = New System.Drawing.Point(3, 1)
        Me.lblSteptwo.Name = "lblSteptwo"
        Me.lblSteptwo.Size = New System.Drawing.Size(255, 19)
        Me.lblSteptwo.TabIndex = 27
        Me.lblSteptwo.Text = "Loan Application Computation"
        '
        'PanePanel1
        '
        Me.PanePanel1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.Label27)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel1.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(1070, 26)
        Me.PanePanel1.TabIndex = 33
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.BackColor = System.Drawing.Color.Transparent
        Me.Label27.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(13, 6)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(252, 18)
        Me.Label27.TabIndex = 0
        Me.Label27.Text = "Loan Application Computation"
        '
        'frmLoan_ApplicationComputation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1070, 643)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.PanePanel1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoan_ApplicationComputation"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Loan Application Computation"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.dgvDeductions, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.panelBottom.ResumeLayout(False)
        Me.panelBottom.PerformLayout()
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanePanel3.ResumeLayout(False)
        Me.PanePanel3.PerformLayout()
        Me.PanePanel2.ResumeLayout(False)
        Me.PanePanel2.PerformLayout()
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblSteptwo As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblEmpNo As System.Windows.Forms.Label
    Friend WithEvents lblEmpName As System.Windows.Forms.Label
    Friend WithEvents lblloantype As System.Windows.Forms.Label
    Friend WithEvents TxtEmpName As System.Windows.Forms.TextBox
    Friend WithEvents txtEmpNo As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtLType As System.Windows.Forms.TextBox
    Friend WithEvents txtClassification As System.Windows.Forms.TextBox
    Friend WithEvents txtLAppDate As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtTotalIntRefunds As System.Windows.Forms.TextBox
    Friend WithEvents txtCapitalShare As System.Windows.Forms.TextBox
    Friend WithEvents txtServicefee As System.Windows.Forms.TextBox
    Friend WithEvents txtInterestAmt As System.Windows.Forms.TextBox
    Friend WithEvents txtPrincipalAmt As System.Windows.Forms.TextBox
    Friend WithEvents txtTerms As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtMonthlyAmor As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtNetProceeds As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalPenalties As System.Windows.Forms.TextBox
    Friend WithEvents txtNetPreterminatedloan As System.Windows.Forms.TextBox
    Friend WithEvents txtservicefeedetails As System.Windows.Forms.TextBox
    Friend WithEvents txtPrincipaldetails As System.Windows.Forms.TextBox
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents PanePanel2 As WindowsApplication2.PanePanel
    Friend WithEvents cmdSubmit As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents lblwhatdouwant As System.Windows.Forms.Label
    Friend WithEvents PanePanel3 As WindowsApplication2.PanePanel
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents chkCheck As System.Windows.Forms.CheckBox
    Friend WithEvents chkATM As System.Windows.Forms.CheckBox
    Friend WithEvents lblPrincipal As System.Windows.Forms.Label
    Friend WithEvents lblTerms As System.Windows.Forms.Label
    Friend WithEvents lblType As System.Windows.Forms.Label
    Friend WithEvents lblClassification As System.Windows.Forms.Label
    Friend WithEvents lblApplicationDate As System.Windows.Forms.Label
    Friend WithEvents lblInterest As System.Windows.Forms.Label
    Friend WithEvents lblIntRefund As System.Windows.Forms.Label
    Friend WithEvents lblCapitalShare As System.Windows.Forms.Label
    Friend WithEvents lblServiceFee As System.Windows.Forms.Label
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents panelBottom As System.Windows.Forms.Panel
    Friend WithEvents txtHiddenAmort As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtMissedPayments As System.Windows.Forms.TextBox
    Friend WithEvents TipAmort As System.Windows.Forms.ToolTip
    Friend WithEvents txtTotalIntrefundNetpreterm As System.Windows.Forms.TextBox
    Friend WithEvents lblIntRefundofRestructure As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtTotalPreterminatedBal As System.Windows.Forms.TextBox
    Friend WithEvents txtIntRefundRestruc As System.Windows.Forms.TextBox
    Friend WithEvents txtrestrucbalanceafterchange As System.Windows.Forms.TextBox
    Friend WithEvents txtpretermbal As System.Windows.Forms.TextBox
    Friend WithEvents txtrestrucbal As System.Windows.Forms.TextBox
    Friend WithEvents txtoriginalintrefundrestruc As System.Windows.Forms.TextBox
    Friend WithEvents txtrestrucdiff As System.Windows.Forms.TextBox
    Friend WithEvents txtpreterm As System.Windows.Forms.TextBox
    Friend WithEvents txtAmorDateEnd As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtAmorDateStart As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtFirstPayment As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents btnPost As System.Windows.Forms.Button
    Friend WithEvents btnRefreshAmort As System.Windows.Forms.Button
    Friend WithEvents txtEffectiveRate As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents chkStandard As System.Windows.Forms.RadioButton
    Friend WithEvents chkDeductinPayroll As System.Windows.Forms.RadioButton
    Friend WithEvents txtAnnulInterest As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents txtTotInterest As System.Windows.Forms.TextBox
    Friend WithEvents txtTotPrincipal As System.Windows.Forms.TextBox
    Friend WithEvents txtTotServiceFee As System.Windows.Forms.TextBox
    Friend WithEvents txtTotAmortization As System.Windows.Forms.TextBox
    Friend WithEvents dgvList As System.Windows.Forms.DataGridView
    Friend WithEvents txtTotalInterestINcome As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents chkCash As System.Windows.Forms.CheckBox
    Friend WithEvents txtTotUnearned As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents btnPrintForm As System.Windows.Forms.Button
    Friend WithEvents btnPrintDisclousure As System.Windows.Forms.Button
    Friend WithEvents btnPrinAgreement As System.Windows.Forms.Button
    Friend WithEvents dgvDeductions As System.Windows.Forms.DataGridView
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
