﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmBrowseLoanNo

    Private gcon As New Clsappconfiguration
    Dim ds As DataSet

    Private Sub frmBrowseLoanNo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If IssuedLoanNo("") Then
            txtSearch.Text = ""
            ActiveControl = txtSearch
        End If
    End Sub

    Private Function IssuedLoanNo(ByVal key As String) As Boolean
        Try
            ds = SqlHelper.ExecuteDataset(gcon.cnstring, "_Select_DocNumber_LoanNo",
                                          New SqlParameter("@search", key),
                                          New SqlParameter("@co_name", frmLoan_ApplicationMain.cboProject.Text))
            dgvLoanNo.DataSource = ds.Tables(0)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub dgvLoanNo_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLoanNo.DoubleClick
        If isSelected() Then
            Me.Close()
        End If
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        If isSelected() Then
            Me.Close()
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        dgvLoanNo.Columns.Clear()
        dgvLoanNo.DataSource = Nothing
        Me.Close()
    End Sub

    Private Function isSelected() As Boolean
        Try
            frmLoan_ApplicationMain.txtLoanNo.Text = Me.dgvLoanNo.SelectedRows(0).Cells(0).Value.ToString
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        If e.KeyCode = Keys.Enter Then
            If isSelected() Then
                Me.Close()
            End If
        End If
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        If IssuedLoanNo(txtSearch.Text) Then

        End If
    End Sub

End Class