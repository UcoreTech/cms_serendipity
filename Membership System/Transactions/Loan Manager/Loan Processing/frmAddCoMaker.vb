﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmAddCoMaker

    Private con As New Clsappconfiguration
    Private Sub frmAddCoMaker_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _LoadDefault()
        txtSearch.Text = ""
        ActiveControl = txtSearch
        SearchClients("")
        _LoadDefault_Acct()
    End Sub

    Private Sub SearchClients(ByVal search As String)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(con.cnstring, "_filter_Clients_Comaker",
                                       New SqlParameter("@Search", search),
                                       New SqlParameter("@DefAcct", txtDefaultAcct.Text))
        dgvComakerList.DataSource = ds.Tables(0)
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        SearchClients(txtSearch.Text)
    End Sub

    Private Sub LoadReports(ByVal empno As String)
        frmPrint.LoadREport(empno)
        frmPrint.StartPosition = FormStartPosition.CenterScreen
        frmPrint.WindowState = FormWindowState.Maximized
        frmPrint.ShowDialog()
    End Sub

    Private Sub btnViewDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewDetails.Click
        Dim idno As String = dgvComakerList.CurrentRow.Cells(0).Value.ToString
        LoadReports(idno)
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        refreshcols()
        Me.Close()
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        With dgvComakerList
            frmLoan_ApplicationMain._AddComaker(.CurrentRow.Cells(0).Value.ToString, .CurrentRow.Cells(1).Value.ToString, .CurrentRow.Cells(2).Value.ToString, .CurrentRow.Cells(3).Value.ToString, .CurrentRow.Cells(4).Value.ToString, "0.00")
        End With
        refreshcols()
        Me.Close()
    End Sub

    Private Sub refreshcols()
        dgvComakerList.DataSource = Nothing
        dgvComakerList.Rows.Clear()
        dgvComakerList.Columns.Clear()
    End Sub

    Private Sub btnHistoryView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHistoryView.Click
        frmComakerHistory.ViewCMHistory(dgvComakerList.CurrentRow.Cells(0).Value.ToString, dgvComakerList.CurrentRow.Cells(2).Value.ToString)
        frmComakerHistory.StartPosition = FormStartPosition.CenterScreen
        frmComakerHistory.ShowDialog()
    End Sub

#Region "Default Acct"
    Private Sub _LoadDefault_Acct()
        Try
            cboDefaultAcct.Items.Clear()
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_Comaker_Select_DefaultAccts")
            While rd.Read
                With cboDefaultAcct
                    .Items.Add(rd(0).ToString)
                End With
            End While
            cboDefaultAcct.Text = "Choose Default Acct."
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If btnEdit.Text = "Edit" Then
            cboDefaultAcct.Visible = True
            txtDefaultAcct.Visible = False
            btnEdit.Text = "Save"
            Exit Sub
        End If
        If btnEdit.Text = "Save" Then
            SqlHelper.ExecuteNonQuery(con.cnstring, "_InsertComaker_DefaultAcct",
                                       New SqlParameter("@Desc", cboDefaultAcct.Text))
            _LoadDefault()
            SearchClients("")
            cboDefaultAcct.Visible = False
            txtDefaultAcct.Visible = True
            btnEdit.Text = "Edit"
            Exit Sub
        End If
    End Sub
    Private Sub _LoadDefault()
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_Select_COMAKER_Default")
            While rd.Read
                txtDefaultAcct.Text = rd(0).ToString
            End While
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

End Class