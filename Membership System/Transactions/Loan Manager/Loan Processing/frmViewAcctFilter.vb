﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmViewAcctFilter

    Private con As New Clsappconfiguration
    Public vID As String

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        If chkSOA.Checked = True Then
            frmPrint.LoadREport(vID)
            frmPrint.WindowState = FormWindowState.Maximized
            frmPrint.ShowDialog()
        End If
        If chkLoans.Checked = True Then
            frmPrint.LoadLoanSoa(vID, Date.Now)
            frmPrint.WindowState = FormWindowState.Maximized
            frmPrint.ShowDialog()
        End If
    End Sub

    Private Sub chkSOA_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkSOA.CheckedChanged
        If chkSOA.Checked = True Then
            chkLoans.Checked = False
        End If
    End Sub

    Private Sub chkLoans_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkLoans.CheckedChanged
        If chkLoans.Checked = True Then
            chkSOA.Checked = False
        End If
    End Sub
End Class