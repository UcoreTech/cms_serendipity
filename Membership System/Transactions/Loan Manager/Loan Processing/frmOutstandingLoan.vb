﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmOutstandingLoan

    Private con As New Clsappconfiguration

    Private Sub frmOutstandingLoan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Public Sub SelectLoan_byID(ByVal id As String)
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_Select_Outstanding_Loan_byID",
                                           New SqlParameter("@ClientID", id))
            dgvList.DataSource = ds.Tables(0)
            dgvList.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ClickSelection()
        Try
            With dgvList.SelectedRows(0)
                frmChkVoucherEntry.AddOutstandingLoans(.Cells(0).Value, .Cells(1).Value, .Cells(2).Value, .Cells(3).Value, .Cells(4).Value, .Cells(5).Value)
                Me.Close()
            End With
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        ClickSelection()
    End Sub

    Private Sub dgvList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.DoubleClick
        ClickSelection()
    End Sub

End Class