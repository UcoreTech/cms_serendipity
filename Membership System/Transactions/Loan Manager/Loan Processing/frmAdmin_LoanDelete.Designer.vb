﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdmin_LoanDelete
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAdmin_LoanDelete))
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tabDeleteLoan = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.grdLoanDisplay = New System.Windows.Forms.DataGridView()
        Me.txtReason = New System.Windows.Forms.TextBox()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.TabDuplicate = New System.Windows.Forms.TabPage()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btndelete_duplicate = New System.Windows.Forms.Button()
        Me.GrdDupLoan = New System.Windows.Forms.DataGridView()
        Me.TabExpired = New System.Windows.Forms.TabPage()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.btnsave = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.lblLoanSummary = New System.Windows.Forms.Label()
        Me.tabDeleteLoan.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.grdLoanDisplay, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabDuplicate.SuspendLayout()
        CType(Me.GrdDupLoan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabExpired.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanePanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabDeleteLoan
        '
        Me.tabDeleteLoan.Controls.Add(Me.TabPage1)
        Me.tabDeleteLoan.Controls.Add(Me.TabDuplicate)
        Me.tabDeleteLoan.Controls.Add(Me.TabExpired)
        Me.tabDeleteLoan.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabDeleteLoan.Location = New System.Drawing.Point(3, 41)
        Me.tabDeleteLoan.Name = "tabDeleteLoan"
        Me.tabDeleteLoan.SelectedIndex = 0
        Me.tabDeleteLoan.Size = New System.Drawing.Size(714, 392)
        Me.tabDeleteLoan.TabIndex = 54
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.btnDelete)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.grdLoanDisplay)
        Me.TabPage1.Controls.Add(Me.txtReason)
        Me.TabPage1.Controls.Add(Me.btnClose)
        Me.TabPage1.Location = New System.Drawing.Point(4, 28)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(706, 360)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Delete Loan"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.Enabled = False
        Me.btnDelete.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Image = CType(resources.GetObject("btnDelete.Image"), System.Drawing.Image)
        Me.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDelete.Location = New System.Drawing.Point(523, 329)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(87, 25)
        Me.btnDelete.TabIndex = 57
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDelete.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(5, 329)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(135, 18)
        Me.Label1.TabIndex = 56
        Me.Label1.Text = "Reason for Deletion:"
        '
        'grdLoanDisplay
        '
        Me.grdLoanDisplay.AllowUserToAddRows = False
        Me.grdLoanDisplay.AllowUserToDeleteRows = False
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.grdLoanDisplay.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.grdLoanDisplay.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdLoanDisplay.BackgroundColor = System.Drawing.SystemColors.Window
        Me.grdLoanDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdLoanDisplay.Location = New System.Drawing.Point(3, 3)
        Me.grdLoanDisplay.Name = "grdLoanDisplay"
        Me.grdLoanDisplay.ReadOnly = True
        Me.grdLoanDisplay.RowHeadersVisible = False
        Me.grdLoanDisplay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdLoanDisplay.Size = New System.Drawing.Size(700, 320)
        Me.grdLoanDisplay.TabIndex = 2
        '
        'txtReason
        '
        Me.txtReason.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReason.Location = New System.Drawing.Point(144, 328)
        Me.txtReason.Name = "txtReason"
        Me.txtReason.Size = New System.Drawing.Size(368, 23)
        Me.txtReason.TabIndex = 55
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Location = New System.Drawing.Point(616, 329)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(87, 25)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'TabDuplicate
        '
        Me.TabDuplicate.Controls.Add(Me.Button1)
        Me.TabDuplicate.Controls.Add(Me.btndelete_duplicate)
        Me.TabDuplicate.Controls.Add(Me.GrdDupLoan)
        Me.TabDuplicate.Location = New System.Drawing.Point(4, 28)
        Me.TabDuplicate.Name = "TabDuplicate"
        Me.TabDuplicate.Size = New System.Drawing.Size(706, 360)
        Me.TabDuplicate.TabIndex = 1
        Me.TabDuplicate.Text = "Duplicate Loan"
        Me.TabDuplicate.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.White
        Me.Button1.Enabled = False
        Me.Button1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(616, 329)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(87, 25)
        Me.Button1.TabIndex = 27
        Me.Button1.Text = "Close"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'btndelete_duplicate
        '
        Me.btndelete_duplicate.BackColor = System.Drawing.Color.White
        Me.btndelete_duplicate.Enabled = False
        Me.btndelete_duplicate.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btndelete_duplicate.Image = CType(resources.GetObject("btndelete_duplicate.Image"), System.Drawing.Image)
        Me.btndelete_duplicate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btndelete_duplicate.Location = New System.Drawing.Point(523, 329)
        Me.btndelete_duplicate.Name = "btndelete_duplicate"
        Me.btndelete_duplicate.Size = New System.Drawing.Size(87, 25)
        Me.btndelete_duplicate.TabIndex = 26
        Me.btndelete_duplicate.Text = "Delete"
        Me.btndelete_duplicate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btndelete_duplicate.UseVisualStyleBackColor = False
        '
        'GrdDupLoan
        '
        Me.GrdDupLoan.AllowUserToAddRows = False
        Me.GrdDupLoan.AllowUserToDeleteRows = False
        Me.GrdDupLoan.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GrdDupLoan.BackgroundColor = System.Drawing.SystemColors.Window
        Me.GrdDupLoan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GrdDupLoan.Enabled = False
        Me.GrdDupLoan.Location = New System.Drawing.Point(3, 3)
        Me.GrdDupLoan.Name = "GrdDupLoan"
        Me.GrdDupLoan.ReadOnly = True
        Me.GrdDupLoan.Size = New System.Drawing.Size(700, 320)
        Me.GrdDupLoan.TabIndex = 25
        '
        'TabExpired
        '
        Me.TabExpired.Controls.Add(Me.Button2)
        Me.TabExpired.Controls.Add(Me.btnsave)
        Me.TabExpired.Controls.Add(Me.DataGridView1)
        Me.TabExpired.Location = New System.Drawing.Point(4, 28)
        Me.TabExpired.Name = "TabExpired"
        Me.TabExpired.Size = New System.Drawing.Size(706, 360)
        Me.TabExpired.TabIndex = 2
        Me.TabExpired.Text = "Expired Loan"
        Me.TabExpired.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.White
        Me.Button2.Enabled = False
        Me.Button2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(616, 329)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(87, 25)
        Me.Button2.TabIndex = 31
        Me.Button2.Text = "Close"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'btnsave
        '
        Me.btnsave.BackColor = System.Drawing.Color.White
        Me.btnsave.Enabled = False
        Me.btnsave.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsave.Image = CType(resources.GetObject("btnsave.Image"), System.Drawing.Image)
        Me.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsave.Location = New System.Drawing.Point(523, 329)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(87, 25)
        Me.btnsave.TabIndex = 30
        Me.btnsave.Text = "Update"
        Me.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsave.UseVisualStyleBackColor = False
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.BackgroundColor = System.Drawing.SystemColors.Window
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Enabled = False
        Me.DataGridView1.Location = New System.Drawing.Point(3, 3)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(700, 320)
        Me.DataGridView1.TabIndex = 29
        '
        'PanePanel1
        '
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.lblLoanSummary)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel1.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(719, 36)
        Me.PanePanel1.TabIndex = 53
        '
        'lblLoanSummary
        '
        Me.lblLoanSummary.AutoSize = True
        Me.lblLoanSummary.BackColor = System.Drawing.Color.Transparent
        Me.lblLoanSummary.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lblLoanSummary.ForeColor = System.Drawing.Color.White
        Me.lblLoanSummary.Location = New System.Drawing.Point(11, 8)
        Me.lblLoanSummary.Name = "lblLoanSummary"
        Me.lblLoanSummary.Size = New System.Drawing.Size(85, 16)
        Me.lblLoanSummary.TabIndex = 20
        Me.lblLoanSummary.Text = "Delete Loan"
        '
        'frmAdmin_LoanDelete
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(719, 435)
        Me.Controls.Add(Me.tabDeleteLoan)
        Me.Controls.Add(Me.PanePanel1)
        Me.Name = "frmAdmin_LoanDelete"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tabDeleteLoan.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.grdLoanDisplay, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabDuplicate.ResumeLayout(False)
        CType(Me.GrdDupLoan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabExpired.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents lblLoanSummary As System.Windows.Forms.Label
    Friend WithEvents tabDeleteLoan As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents grdLoanDisplay As System.Windows.Forms.DataGridView
    Friend WithEvents txtReason As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TabDuplicate As System.Windows.Forms.TabPage
    Friend WithEvents GrdDupLoan As System.Windows.Forms.DataGridView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btndelete_duplicate As System.Windows.Forms.Button
    Friend WithEvents TabExpired As System.Windows.Forms.TabPage
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents btnsave As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents btnDelete As System.Windows.Forms.Button
End Class
