﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChkVoucherEntry
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvEntry = New System.Windows.Forms.DataGridView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtDebit = New System.Windows.Forms.TextBox()
        Me.txtCredit = New System.Windows.Forms.TextBox()
        Me.txtDate = New System.Windows.Forms.TextBox()
        Me.txtCreatedby = New System.Windows.Forms.TextBox()
        Me.txtParticulars = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtNetproceeds = New System.Windows.Forms.TextBox()
        Me.btnCheckNo = New System.Windows.Forms.Button()
        Me.cbDocNum = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cboDoctype = New System.Windows.Forms.ComboBox()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.btnAddItem = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnOutstandingLoan = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.PanePanel5 = New WindowsApplication2.PanePanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboProject = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        CType(Me.dgvEntry, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanePanel1.SuspendLayout()
        Me.PanePanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvEntry
        '
        Me.dgvEntry.AllowUserToAddRows = False
        Me.dgvEntry.AllowUserToResizeColumns = False
        Me.dgvEntry.AllowUserToResizeRows = False
        Me.dgvEntry.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvEntry.BackgroundColor = System.Drawing.Color.White
        Me.dgvEntry.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvEntry.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEntry.Location = New System.Drawing.Point(10, 111)
        Me.dgvEntry.Name = "dgvEntry"
        Me.dgvEntry.RowHeadersVisible = False
        Me.dgvEntry.Size = New System.Drawing.Size(859, 338)
        Me.dgvEntry.TabIndex = 59
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(40, 39)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 13)
        Me.Label2.TabIndex = 60
        Me.Label2.Text = "Created by :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(11, 75)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(0, 13)
        Me.Label3.TabIndex = 61
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(14, 95)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(0, 13)
        Me.Label4.TabIndex = 62
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(545, 62)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(43, 13)
        Me.Label5.TabIndex = 63
        Me.Label5.Text = "Date :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(532, 87)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(56, 13)
        Me.Label6.TabIndex = 64
        Me.Label6.Text = "Doc. No."
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(487, 458)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(50, 13)
        Me.Label7.TabIndex = 65
        Me.Label7.Text = "Totals :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(541, 458)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(41, 13)
        Me.Label8.TabIndex = 66
        Me.Label8.Text = "Debit "
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(709, 458)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(46, 13)
        Me.Label9.TabIndex = 67
        Me.Label9.Text = "Credit "
        '
        'txtDebit
        '
        Me.txtDebit.Enabled = False
        Me.txtDebit.Location = New System.Drawing.Point(589, 455)
        Me.txtDebit.Name = "txtDebit"
        Me.txtDebit.Size = New System.Drawing.Size(115, 21)
        Me.txtDebit.TabIndex = 68
        '
        'txtCredit
        '
        Me.txtCredit.Enabled = False
        Me.txtCredit.Location = New System.Drawing.Point(754, 455)
        Me.txtCredit.Name = "txtCredit"
        Me.txtCredit.Size = New System.Drawing.Size(115, 21)
        Me.txtCredit.TabIndex = 69
        '
        'txtDate
        '
        Me.txtDate.Location = New System.Drawing.Point(591, 59)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.Size = New System.Drawing.Size(118, 21)
        Me.txtDate.TabIndex = 71
        '
        'txtCreatedby
        '
        Me.txtCreatedby.Enabled = False
        Me.txtCreatedby.Location = New System.Drawing.Point(120, 36)
        Me.txtCreatedby.Name = "txtCreatedby"
        Me.txtCreatedby.Size = New System.Drawing.Size(176, 21)
        Me.txtCreatedby.TabIndex = 73
        '
        'txtParticulars
        '
        Me.txtParticulars.Location = New System.Drawing.Point(92, 458)
        Me.txtParticulars.Name = "txtParticulars"
        Me.txtParticulars.Size = New System.Drawing.Size(296, 21)
        Me.txtParticulars.TabIndex = 75
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(11, 466)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(72, 13)
        Me.Label10.TabIndex = 74
        Me.Label10.Text = "Particulars:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(664, 498)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(87, 13)
        Me.Label11.TabIndex = 76
        Me.Label11.Text = "Net Proceeds:"
        '
        'txtNetproceeds
        '
        Me.txtNetproceeds.BackColor = System.Drawing.Color.White
        Me.txtNetproceeds.Enabled = False
        Me.txtNetproceeds.ForeColor = System.Drawing.Color.CadetBlue
        Me.txtNetproceeds.Location = New System.Drawing.Point(754, 495)
        Me.txtNetproceeds.Name = "txtNetproceeds"
        Me.txtNetproceeds.ReadOnly = True
        Me.txtNetproceeds.Size = New System.Drawing.Size(115, 21)
        Me.txtNetproceeds.TabIndex = 77
        Me.txtNetproceeds.Text = "0.00"
        '
        'btnCheckNo
        '
        Me.btnCheckNo.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCheckNo.Location = New System.Drawing.Point(760, 80)
        Me.btnCheckNo.Name = "btnCheckNo"
        Me.btnCheckNo.Size = New System.Drawing.Size(29, 23)
        Me.btnCheckNo.TabIndex = 78
        Me.btnCheckNo.Text = "..."
        Me.btnCheckNo.UseVisualStyleBackColor = True
        '
        'cbDocNum
        '
        Me.cbDocNum.BackColor = System.Drawing.Color.White
        Me.cbDocNum.Location = New System.Drawing.Point(591, 84)
        Me.cbDocNum.Name = "cbDocNum"
        Me.cbDocNum.ReadOnly = True
        Me.cbDocNum.Size = New System.Drawing.Size(164, 21)
        Me.cbDocNum.TabIndex = 79
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(7, 88)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(113, 13)
        Me.Label12.TabIndex = 80
        Me.Label12.Text = "Select Doc. Type :"
        '
        'cboDoctype
        '
        Me.cboDoctype.FormattingEnabled = True
        Me.cboDoctype.Items.AddRange(New Object() {"CHECK VOUCHER", "CASH VOUCHER", "JOURNAL VOUCHER"})
        Me.cboDoctype.Location = New System.Drawing.Point(120, 85)
        Me.cboDoctype.Name = "cboDoctype"
        Me.cboDoctype.Size = New System.Drawing.Size(176, 21)
        Me.cboDoctype.TabIndex = 81
        '
        'PanePanel1
        '
        Me.PanePanel1.BackColor = System.Drawing.Color.White
        Me.PanePanel1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.btnAddItem)
        Me.PanePanel1.Controls.Add(Me.btnDelete)
        Me.PanePanel1.Controls.Add(Me.btnOutstandingLoan)
        Me.PanePanel1.Controls.Add(Me.btnSave)
        Me.PanePanel1.Controls.Add(Me.btnCancel)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel1.Location = New System.Drawing.Point(0, 522)
        Me.PanePanel1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(881, 32)
        Me.PanePanel1.TabIndex = 70
        '
        'btnAddItem
        '
        Me.btnAddItem.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddItem.Location = New System.Drawing.Point(465, 4)
        Me.btnAddItem.Name = "btnAddItem"
        Me.btnAddItem.Size = New System.Drawing.Size(107, 23)
        Me.btnAddItem.TabIndex = 80
        Me.btnAddItem.Text = "Add Item"
        Me.btnAddItem.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Red
        Me.btnDelete.Location = New System.Drawing.Point(3, 3)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(155, 23)
        Me.btnDelete.TabIndex = 79
        Me.btnDelete.Text = "Delete Selected Item"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnOutstandingLoan
        '
        Me.btnOutstandingLoan.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOutstandingLoan.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.btnOutstandingLoan.Location = New System.Drawing.Point(575, 4)
        Me.btnOutstandingLoan.Name = "btnOutstandingLoan"
        Me.btnOutstandingLoan.Size = New System.Drawing.Size(155, 23)
        Me.btnOutstandingLoan.TabIndex = 78
        Me.btnOutstandingLoan.Text = "Add Outstanding Loan"
        Me.btnOutstandingLoan.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(735, 3)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(66, 23)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "SAVE"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(806, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(66, 23)
        Me.btnCancel.TabIndex = 0
        Me.btnCancel.Text = "CANCEL"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'PanePanel5
        '
        Me.PanePanel5.BackColor = System.Drawing.Color.White
        Me.PanePanel5.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel5.Controls.Add(Me.Label1)
        Me.PanePanel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel5.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel5.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel5.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel5.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel5.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.PanePanel5.Name = "PanePanel5"
        Me.PanePanel5.Size = New System.Drawing.Size(881, 32)
        Me.PanePanel5.TabIndex = 58
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(4, 6)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(113, 19)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Release Loan"
        '
        'cboProject
        '
        Me.cboProject.FormattingEnabled = True
        Me.cboProject.Items.AddRange(New Object() {"CHECK VOUCHER", "CASH VOUCHER", "JOURNAL VOUCHER"})
        Me.cboProject.Location = New System.Drawing.Point(120, 60)
        Me.cboProject.Name = "cboProject"
        Me.cboProject.Size = New System.Drawing.Size(268, 21)
        Me.cboProject.TabIndex = 83
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(25, 63)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(95, 13)
        Me.Label13.TabIndex = 82
        Me.Label13.Text = "Select Project :"
        '
        'frmChkVoucherEntry
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(881, 554)
        Me.Controls.Add(Me.cboProject)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.cboDoctype)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.cbDocNum)
        Me.Controls.Add(Me.btnCheckNo)
        Me.Controls.Add(Me.txtNetproceeds)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtParticulars)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtCreatedby)
        Me.Controls.Add(Me.txtDate)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.txtCredit)
        Me.Controls.Add(Me.txtDebit)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dgvEntry)
        Me.Controls.Add(Me.PanePanel5)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Name = "frmChkVoucherEntry"
        Me.Text = "Release Loan"
        CType(Me.dgvEntry, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel5.ResumeLayout(False)
        Me.PanePanel5.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanePanel5 As WindowsApplication2.PanePanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvEntry As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtDebit As System.Windows.Forms.TextBox
    Friend WithEvents txtCredit As System.Windows.Forms.TextBox
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents txtDate As System.Windows.Forms.TextBox
    Friend WithEvents txtCreatedby As System.Windows.Forms.TextBox
    Friend WithEvents txtParticulars As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtNetproceeds As System.Windows.Forms.TextBox
    Friend WithEvents btnOutstandingLoan As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnCheckNo As System.Windows.Forms.Button
    Friend WithEvents cbDocNum As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cboDoctype As System.Windows.Forms.ComboBox
    Friend WithEvents btnAddItem As System.Windows.Forms.Button
    Friend WithEvents cboProject As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
End Class
