Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient


Public Class frmLoan_ApplicationMain
    Private gCon As New Clsappconfiguration
    Private empno As String
    Private basicpay As Decimal
    Private loantype As String
    Private terms As String
    Private cooploans As String

    Private sssloans As Decimal
    Private philhealthloans As Decimal
    Private pag_ibigloans As Decimal

    Private misc As Decimal
    Public loancode As String
    Public classfication As String
    Public Empid As String = ""
    Public LoanNo As String
    Public Fkloanid As String
    Public LoanCategory As String

#Region "Variables for loan application computation"
    Public salary01 As String
    Public sss01 As String
    Public pagibig01 As String
    Public philhealth01 As String
    Public companyloan01 As String
    Public others01 As String

#End Region
#Region "Property for loan application computation"
    Public Property GetSalary() As Decimal
        Get
            Return salary01
        End Get
        Set(ByVal value As Decimal)
            salary01 = value
        End Set
    End Property
    Public Property GetSSSLoan() As Decimal
        Get
            Return sss01
        End Get
        Set(ByVal value As Decimal)
            sss01 = value
        End Set
    End Property
    Public Property GetPagibigLoan() As Decimal
        Get
            Return pagibig01
        End Get
        Set(ByVal value As Decimal)
            pagibig01 = value
        End Set
    End Property
    Public Property GetPhilhealthLoan() As Decimal
        Get
            Return philhealth01
        End Get
        Set(ByVal value As Decimal)
            philhealth01 = value
        End Set
    End Property
    Public Property GetCompanyLoan() As Decimal
        Get
            Return companyloan01
        End Get
        Set(ByVal value As Decimal)
            companyloan01 = value
        End Set
    End Property
    Public Property GetOtherDeductions() As Decimal
        Get
            Return others01
        End Get
        Set(ByVal value As Decimal)
            others01 = value
        End Set
    End Property

    Private totalOtherLoans As Decimal
    Public Property GetOtherLoansTotal() As Decimal
        Get
            Return totalOtherLoans
        End Get
        Set(ByVal value As Decimal)
            totalOtherLoans = value
        End Set
    End Property

#End Region
#Region "Other properties"
    Public Property getloadcode() As String
        Get
            Return loancode
        End Get
        Set(ByVal value As String)
            loancode = value
        End Set
    End Property
    Public Property getclassfication() As String
        Get
            Return classfication
        End Get
        Set(ByVal value As String)
            classfication = value
        End Set
    End Property
    Public Property GetEmployeeID() As String
        Get
            Return Empid
        End Get
        Set(ByVal value As String)
            Empid = value
        End Set
    End Property
    Public Property getloannumber() As String
        Get
            Return LoanNo
        End Get
        Set(ByVal value As String)
            LoanNo = value
        End Set
    End Property
    Public Property getFKloanID() As String
        Get
            Return Fkloanid
        End Get
        Set(ByVal value As String)
            Fkloanid = value
        End Set
    End Property


#End Region

#Region "Subroutines"
    Private Sub ClearFields()
        optNew.Checked = True
        cboLoanType.Text = ""
        cboTerms.Text = ""
        txtRateMonthly.Text = ""
        txtSSSLoan.Text = ""
        txtPagibigLoan.Text = ""
        txtPhilHealth.Text = ""
        txtCompanyLoan.Text = ""
        txtOtherNonRecurring.Text = ""
        txtContribution.Text = ""
        txtCapacityToPay.Text = ""
        txtMaxLoanAmount.Text = ""
        Me.txtPrincipalAmount.Text = ""
        Me.txtRestructure.Text = ""
        Me.TxtCoopLoan.Text = ""
        Me.cboModeofPayment.Text = ""
        Me.txtnoofpayment.Text = "0"

        txtLoanNo.Text = ""
        dgvComakerList.Columns.Clear()
    End Sub
    Private Sub Load_AllMembers()

        Dim gCon As New Clsappconfiguration

        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList")

            If rd.Read Then
                txtRateMonthly.Text = rd.Item("Full Name").ToString
            End If

            If rd.Read Then
                txtPagibigLoan.Text = rd.Item("Employee No").ToString
            End If

        End Using
    End Sub
    Private Sub Load_MemberNumber()
        Dim gCon As New Clsappconfiguration
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList")
            While rd.Read
                lstNumbers.Items.Add(rd.Item("Employee No")).ToString()
            End While

        End Using
    End Sub
    Private Sub Load_MemberName()
        Dim gCon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList")
        With Me.lstMembers
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("IDnumber", 70, HorizontalAlignment.Left)
            .Columns.Add("Member's Fullname", 200, HorizontalAlignment.Left)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        .SubItems.Add(rd.Item(1))
                    End With
                End While
                rd.Close()
                gCon.sqlconn.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Load Member list")
            End Try
        End With

    End Sub

    Private Function TrapKey(ByVal KCode As String) As Boolean
        If (KCode >= 48 And KCode <= 57) Or KCode = 8 Then
            TrapKey = False
        Else
            TrapKey = True
        End If
    End Function

    'Private Sub Load_LoanTypes()
    '    cboLoanType.Items.Clear()
    '    Dim gCon As New Clsappconfiguration
    '    Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "MSS_Loans_LoadLoanTypes")
    '        While rd.Read
    '            cboLoanType.Items.Add(rd.Item("Loan Type Name")).ToString()
    '        End While
    '    End Using
    'End Sub
    Private Sub Load_LoanTerms()
        Dim gCon As New Clsappconfiguration
        Dim rd As SqlDataReader
        Dim cmd As New SqlCommand

        cboTerms.Text = ""

        cmd.CommandText = "MSS_Loans_LoadLoanTerms"
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = gCon.sqlconn

        cmd.Parameters.Add("@loantype", SqlDbType.VarChar, 50, ParameterDirection.Input).Value = cboLoanType.Text
        'cmd.Parameters.Add("fnTermInMonths", SqlDbType.VarChar, 50, ParameterDirection.Input).Value = cboTerms.Text
        cmd.Parameters.Add("@employeeNo", SqlDbType.VarChar, 50, ParameterDirection.Input).Value = Me.lstMembers.SelectedItems(0).Text
        cmd.Parameters.Add("@co_name", SqlDbType.VarChar, 100, ParameterDirection.Input).Value = cboProject.Text
        gCon.sqlconn.Open()

        rd = cmd.ExecuteReader
        Try
            cboTerms.Items.Clear()
            While rd.Read()
                'cboTerms.Text = mydatareader.Item(0)
                cboTerms.Items.Add(rd.Item(1).ToString())
            End While
        Finally
            gCon.sqlconn.Close()
            rd.Close()
        End Try
    End Sub
    Private Sub Load_LoanTypes_Restructure()
        cboLoanType.Items.Clear()
        cboRestLoanNo.Items.Clear()
        Dim gCon As New Clsappconfiguration
        Dim parameter1 As String = lstNumbers.Text
        Dim MSS_LoanTracking_GetLoans As String = "MSS_LoanTracking_GetLoans '" & GetEmployeeID() & "', '" & "Approved" & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, MSS_LoanTracking_GetLoans)
                While rd.Read
                    cboLoanType.Items.Add(rd.Item(4).ToString)
                    cboRestLoanNo.Items.Add(rd.Item(3).ToString)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.ToString & " at Load_LoanTypes_Restructure")
        End Try
    End Sub
    Private Sub GetContribution()
        txtContribution.Clear()
        Dim gCon As New Clsappconfiguration
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, "CIMS_Member_GetContributions",
                                                                New SqlParameter("@employeeno", Me.lstMembers.SelectedItems(0).Text))
                While rd.Read
                    txtContribution.Text = rd("TotContribution")
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.ToString & " at GetContribution")
        End Try

    End Sub
    Private Sub returnToZero()
        If txtRateMonthly.Text = "" Or txtRateMonthly.Text Is Nothing Then
            txtRateMonthly.Text = "0"
        End If
        If txtSSSLoan.Text = "" Or txtSSSLoan.Text Is Nothing Then
            txtSSSLoan.Text = "0"
        End If
        If txtPagibigLoan.Text = "" Or txtPagibigLoan.Text Is Nothing Then
            txtPagibigLoan.Text = "0"
        End If
        If txtPhilHealth.Text = "" Or txtPhilHealth.Text Is Nothing Then
            txtPhilHealth.Text = "0"
        End If
        If txtCompanyLoan.Text = "" Or txtCompanyLoan.Text Is Nothing Then
            txtCompanyLoan.Text = "0"
        End If
        If txtOtherNonRecurring.Text = "" Or txtOtherNonRecurring.Text Is Nothing Then
            txtOtherNonRecurring.Text = "0"
        End If
        If txtContribution.Text = "" Or txtContribution.Text Is Nothing Then
            txtContribution.Text = "0"
        End If

    End Sub
    Private Sub Compute_MaxLoan()
        'cboChooseLoanType.Items.Clear()

        empno = Me.lstMembers.SelectedItems(0).Text
        basicpay = txtRateMonthly.Text
        loantype = cboLoanType.SelectedItem
        terms = cboTerms.SelectedItem
        cooploans = TxtCoopLoan.Text
        'sssloans = Convert.ToDecimal(txtSSSLoan.Text)   'test
        'philhealthloans = Convert.ToDecimal(txtPhilHealth.Text)   'test
        'pag_ibigloans = Convert.ToDecimal(txtPagibigLoan.Text)   'test
        misc = Convert.ToDecimal(txtOtherNonRecurring.Text)

        'Validation of Restructured Loan No
        If txtRestructure.Text = "" Then
            txtRestructure.Text = "0"
        End If

        'Dim Xsqlcmd As String = "MSS_Loans_ComputeMaxLoanAmount2'" & GetEmployeeID() & _
        '            "', '" & GetSalary() & "', '" & loantype & "', '" & terms & "', '" & _
        '             cooploans & "', '" & GetOtherLoansTotal() & "', '" & GetOtherDeductions() & "','" & txtRestructure.Text & "'"
        Dim Xsqlcmd As String = "MSS_Loans_ComputeMaxLoanAmount2'" & GetEmployeeID() & _
            "', '" & basicpay & "', '" & loantype & "', '" & terms & "', '" & _
            0 & "', '" & 0 & "', '" & 0 & "','" & 0 & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, Xsqlcmd)
                While rd.Read
                    txtContribution.Text = rd.Item("Max Loan Capital Cont").ToString
                    txtCapacityToPay.Text = rd.Item("Capacity to Pay").ToString
                    frmLoan_ApplicationComputation.getRemainingNetpay() = rd.Item("Other Loans").ToString 'rd.Item("Total Existing Loans").ToString
                    frmLoan_ApplicationComputation.getMaxloanCapacity() = rd.Item("Capacity to Pay").ToString
                    frmLoan_ApplicationComputation.getCapitalContribution() = rd.Item("Capital Contribution").ToString
                    frmLoan_ApplicationComputation.getPolicyRate() = rd.Item("Policy Rate").ToString
                    frmLoan_ApplicationComputation.getMaxloanCapital() = rd.Item("Max Loan Capital Cont").ToString
                    frmLoan_ApplicationComputation.getTotalCoopLoans() = rd.Item("Total Coop Loans").ToString
                End While
            End Using

        Catch ex As Exception
            ' MsgBox(ex.ToString, MsgBoxStyle.OkOnly, " error at Compute_MaxLoan")
        End Try

        Dim contri As Decimal = Decimal.Parse(Me.txtContribution.Text)
        Dim captopay As Decimal = Decimal.Parse(Me.txtCapacityToPay.Text)

        If contri < captopay Then
            txtMaxLoanAmount.Text = contri
            Call ApplyLoanPolicy_MaxTerms(Me.cboLoanType.Text, Decimal.Parse(Me.cboTerms.Text), Decimal.Parse(txtMaxLoanAmount.Text))

        ElseIf contri > captopay Then
            txtMaxLoanAmount.Text = captopay
            Call ApplyLoanPolicy_MaxTerms(Me.cboLoanType.Text, Decimal.Parse(Me.cboTerms.Text), Decimal.Parse(txtMaxLoanAmount.Text))

        End If
    End Sub
#Region "Recompute for new policies"
    Private Sub ApplyLoanPolicy_PositionType()

    End Sub

    Private Sub ApplyLoanPolicy_MaxTerms(ByVal loantype As String, ByVal terms As Decimal, ByVal maxloan As Decimal)
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "MSS_Loans_ApplyLoanPolicy_MaxTerms", _
                                    New SqlParameter("@loanType", loantype), _
                                    New SqlParameter("@terms", terms), _
                                    New SqlParameter("@maxLoanAmount", maxloan))
        Try
            While rd.Read
                txtMaxLoanAmount.Text = rd.Item(0).ToString
                txtPrincipalAmount.Text = txtMaxLoanAmount.Text
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Computation for new policies")
        End Try

    End Sub

#End Region
    '============================ easy sql step w/ parameter===========================
    'Dim sqlQuery As String = "MSS_Loans_LoadLoanTerms "
    'sqlQuery &= "@loantype ='" & cboChooseLoanType.Text & "'"
    'sqlQuery &= ",@status ='" & object & "'"

    'SqlHelper.ExecuteReader(myconnection.cnstring, CommandType.Text, sqlQuery)
    '=================================================================================

#End Region

#Region "Events"
    Private Sub loan_type_loads()

    End Sub
    Private Sub chkPreTerminate_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPreTerminate.CheckStateChanged
        If chkPreTerminate.Checked = True Then
            frmLoan_PreTerminate.getEmpinfo() = Empid
            frmLoan_PreTerminate.getExloanNumber() = Me.txtRestructure.Text.Trim
            frmLoan_PreTerminate.ShowDialog()
            frmLoan_ApplicationComputation.getMode() = "TERMINATION"
        Else
            panelStep1.Visible = True
            frmLoan_ApplicationComputation.getMode() = ""

        End If
    End Sub
    Private Sub frmloan_type_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _LoadAcctg_Proj()
        Load_MemberName()
        lblStepThree.ForeColor = Color.Black

        panelStep1.Enabled = False
        panelstep2.Enabled = False
        panelStep3.Enabled = False

        _LoadCustomized_Intmethod() '3/4/15
        _LoadStandard_IntMethod() '3/4/15
        PrepareColumns()
    End Sub

    Private Sub Load_LoanTypes()
        cboLoanType.Items.Clear()
        Dim gCon As New Clsappconfiguration
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "MSS_Loans_LoadLoanTypes",
                                      New SqlParameter("@ProjDesc", cboProject.Text))
            While rd.Read
                cboLoanType.Items.Add(rd.Item("Loan Type Name")).ToString()
            End While
        End Using
    End Sub

    Private Sub cboProject_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboProject.SelectedValueChanged
        Load_LoanTypes()
    End Sub

    Private Sub _LoadAcctg_Proj()
        Dim mycon As New Clsappconfiguration
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, "_Load_Project")
            cboProject.Items.Clear()
            While rd.Read
                With cboProject
                    .Items.Add(rd(0).ToString)
                End With
            End While
            cboProject.Text = "Select Project"
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub cmdDone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDone.Click
        If txtLoanNo.Text = "" Then
            MessageBox.Show("Please Select Loan No.")
            Exit Sub
        Else

            If Me.cboLoanType.Text = "" Or Me.cboTerms.Text = "" Then
                MessageBox.Show("Please select Loan Type and Terms.", "Empty field found.", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                panelstep2.Enabled = True
                panelStep1.Enabled = False
            End If

        End If

        'panelStep1.Enabled = False
        'panelstep2.Enabled = True
    End Sub
    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBack.Click
        panelStep1.Enabled = True
        panelstep2.Enabled = False
        panelStep3.Enabled = False
    End Sub
    Private Sub cmdShowComputation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdShowComputation.Click
        Dim restruct As Decimal
        If loantype <> Nothing Then
            'Validation of Restructured Loan No
            If txtRestructure.Text = "" Then
                restruct = 0
            End If

            frmFinStep.Show_Computation(GetEmployeeID(), txtRateMonthly.Text, loantype, terms, 0, 0, 0, restruct)
            'frmFinStep.Show_Computation(empno, txtRateMonthly.Text, cboLoanType.Text, cboTerms.Text, TxtCoopLoan.Text, txtSSSLoan.Text, txtPhilHealth.Text, txtPagibigLoan.Text, txtOtherNonRecurring.Text)
            frmLoanComputationShow001.txtSalary.Text = txtRateMonthly.Text
            frmLoanComputationShow001.ShowDialog()
            'frmFinStep.ShowDialog()
        Else
            MessageBox.Show("Please click Compute Button first.", "Show Computation", MessageBoxButtons.OK, MessageBoxIcon.Error)
            btnComputeMaxLoan.Focus()
        End If
    End Sub

    'NOTE: Reset Maximum Loan Amount when changing Member
    Private Sub ResetMaximumLoanableAmount()
        GetSalary() = Nothing
        loantype = Nothing
        terms = Nothing
        cooploans = Nothing
    End Sub

    Private Sub cmdContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdContinue.Click
        'panelStep3.Visible = True
        'panelStep1.Enabled = False
        'panelstep2.Enabled = False
        'panelStep3.Enabled = True
        panelStep1.Enabled = False
        panelStep3.Enabled = True
        panelstep2.Enabled = False
        'cmdYes.Enabled = True
        cmdNo.Enabled = True

    End Sub

    Private Sub cmdNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNo.Click
        'panelStep1.Enabled = True
        panelstep2.Enabled = True
        'panelStep3.Enabled = True
        'cmdYes.Enabled = False
    End Sub
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        ClearFields()
        Me.Refresh()
        panelStep1.Enabled = False

    End Sub

    Public inEditMode As Boolean = False  'added 8/6/2014 Vince

    Private Sub cmdCompute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnComputeMaxLoan.Click
        Dim restruct As Decimal
        'GetContribution()

        'Store Value from Textbox to property
        Try
            'GetSalary() = IIf(txtRateMonthly.Text <> "", txtRateMonthly.Text, 0)
            'GetSSSLoan() = IIf(txtSSSLoan.Text <> "", txtSSSLoan.Text, 0)
            'GetPagibigLoan() = IIf(txtPagibigLoan.Text <> "", txtPagibigLoan.Text, 0)
            'GetPhilhealthLoan() = IIf(txtPhilHealth.Text <> "", txtPhilHealth.Text, 0)
            'GetCompanyLoan() = IIf(txtCompanyLoan.Text <> "", txtCompanyLoan.Text, 0)
            'GetOtherDeductions() = IIf(txtOtherNonRecurring.Text <> "", txtOtherNonRecurring.Text, 0)

            'Computes for SSS, Pagibig, Philhealth, and Company Loans
            'GetOtherLoansTotal() = ComputeTotalOtherLoans()

            'Computes Governments Loans for Display Purpose Only
            'frmFinStep.GetGovtLoans() = ComputeGovtLoans()
            Call returnToZero()
            Call Compute_MaxLoan()

            If loantype <> Nothing Then
                'Validation of Restructured Loan No
                If txtRestructure.Text = "" Then
                    restruct = 0
                End If

                frmFinStep.Show_Computation(GetEmployeeID(), txtRateMonthly.Text, loantype, terms, 0, 0, 0, restruct)
                frmLoanComputationShow001.empno = empno
                frmLoanComputationShow001.ltype = cboLoanType.Text
                frmLoanComputationShow001.txtSalary.Text = txtRateMonthly.Text
                frmLoanComputationShow001.txtBasic.Text = txtRateMonthly.Text 'remaining net pay on first load of frm
                frmLoanComputationShow001.RComputeLoanableAmt(empno, cboLoanType.Text, CDec(txtRateMonthly.Text))
                'frmFinStep.Show_Computation(empno, txtRateMonthly.Text, cboLoanType.Text, cboTerms.Text, TxtCoopLoan.Text, txtSSSLoan.Text, txtPhilHealth.Text, txtPagibigLoan.Text, txtOtherNonRecurring.Text)
            End If
        Catch
        End Try

        If rdCarryForward.Checked = True Then
            txtPrincipalAmount.Text = CarryForwardPrincipal
        End If

        If inEditMode = True Then
            txtPrincipalAmount.Text = CarryForwardPrincipal
        End If
    End Sub

    Private Function ComputeTotalOtherLoans() As Decimal
        Dim total = CDec(GetSSSLoan()) + CDec(GetPagibigLoan()) + CDec(GetPhilhealthLoan()) + CDec(GetCompanyLoan())

        Return total
    End Function

    Private Function ComputeGovtLoans() As Decimal
        Dim total = CDec(GetSSSLoan()) + CDec(GetPagibigLoan()) + CDec(GetPhilhealthLoan())
        Return total
    End Function

    Private Sub cboChooseLoanType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLoanType.SelectedIndexChanged
        frmLoan_ApplicationComputation.getloantype() = Me.cboLoanType.Text
        Call Load_LoanTerms()
        If Me.optNew.Checked = True Then
            Call ValidateLoantype(Empid)
        ElseIf Me.optRestructure.Checked = True Then
            Call ValidateLoantype(Empid)
            'Call GetLoanNo(Empid, Me.cboLoanType.Text)
        End If

        Call CheckIfLoanCannotBerestructured(txtRestructure.Text)
    End Sub
#Region "Get loan Number"
    Private Sub GetLoanNo(ByVal empinfo As String, ByVal loantype As String)
        cboRestLoanNo.SelectedIndex = cboLoanType.SelectedIndex
        txtRestructure.Text = cboRestLoanNo.Text

    End Sub
#End Region
#Region "Validate Loan type if exist"
    Private Sub ValidateLoantype(ByVal empid As String)
        Dim reader As SqlDataReader
        Dim x As New DialogResult
        reader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "MSS_LoanTracking_GetLoans", _
                                     New SqlParameter("@EmployeeNo", empid), _
                                     New SqlParameter("@status", "Approved"))
        Try

            While reader.Read
                Dim apprdloan As String = reader.Item(3).ToString.Trim()
                If apprdloan = Me.cboLoanType.Text Then
                    x = MessageBox.Show("You have existing loan/s. Select one of your current loans you want to restructure.", "Loan Exist", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                    'if yes call restructure form 
                    If x = Windows.Forms.DialogResult.Yes Then
                        Me.optRestructure.Checked = True
                    End If
                Else
                End If
            End While
            reader.Close()
        Catch ex As Exception

        End Try
    End Sub
#End Region
#Region "get loancode"
    Private Sub _getcode(ByVal loantype As String)
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "MMS_getcode_Select", _
                                     New SqlParameter("@typename", loantype))
        Try
            While rd.Read
                Dim x As String = rd.Item(0)

            End While

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
#End Region

    Private Sub optRestructure_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optRestructure.CheckedChanged
        'MSS_LoanTracking_GetLoans' for restructure
        If optRestructure.Checked = True Then
            inEditMode = False
            getclassfication() = "New" 'Possible Changes Here
            frmLoan_ApplicationComputation.getloanClass = "New"

            'Call Load_LoanTypes_Restructure()
            Call Load_LoanTypes_ForNewLoans(GetEmployeeID(), "Approved")

            cmdDone.Enabled = True
        End If
    End Sub
    Private Sub optNew_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optNew.CheckedChanged
        If optNew.Checked = True Then
            inEditMode = False
            getclassfication() = "New"
            frmLoan_ApplicationComputation.getloanClass = "New"
            Call Load_LoanTypes_ForNewLoans(GetEmployeeID(), "Approved")

            txtRestructure.Text = ""
            cboTerms.Items.Clear()

            cmdDone.Enabled = True
        End If
    End Sub

    Private Sub Load_LoanTypes_ForNewLoans(ByVal employeeNo As String, ByVal loanStatus As String)
        cboLoanType.Items.Clear()
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "MSS_Loans_GetNewLoanTypes", _
                    New SqlParameter("@EmployeeNo", employeeNo), _
                    New SqlParameter("@status", loanStatus))
            While rd.Read
                cboLoanType.Items.Add(rd.Item("fcLoanTypeName")).ToString()
            End While
        End Using

        If cboLoanType.Items.Count = 0 Then
            MessageBox.Show("You cannot enter any more new loans, you can TRY restructuring instead.", "Loan Application", MessageBoxButtons.OK, MessageBoxIcon.Information)
            optNew.Checked = False
            'optRestructure.Checked = True
            Exit Sub 'added by enteng
        End If
    End Sub

    Private Sub CheckIfLoanCannotBerestructured(ByVal loanNo As String)
        If optRestructure.Checked = True Then
            Dim remark, percentPaid, percentAllowed As String


            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "MSS_Loans_IsAllowedToRestructure", _
                            New SqlParameter("@loanNo", loanNo))
                    If rd.Read() Then
                        remark = rd.Item("IsAllowedToRestruct").ToString()
                        percentAllowed = rd.Item("Allowed Percent").ToString()
                        percentPaid = rd.Item("Percent Paid").ToString()

                        If remark = "False" Then

                            MessageBox.Show("Loan No.: " & loanNo & " is " & percentPaid & "% paid." & vbNewLine _
                                                & "You need atleast " & percentAllowed & "% to restructure this.", _
                                                "Restructure Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            cmdDone.Enabled = False

                        Else
                            MessageBox.Show("Loan No.: " & loanNo & " is  " & percentPaid & "%  paid.", _
                                    "Restructure Granted", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                            cmdDone.Enabled = True

                        End If

                    End If
                End Using
            Catch ex As Exception

            End Try
        End If

    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Dim gcon As New Clsappconfiguration
        Dim searchno As String
        lstNumbers.Items.Clear()
        'lstname.Items.Clear()
        searchno = "select fcEmployeeNo from dbo.CIMS_m_Member_Employment " & _
                    "where fcEmployeeNo LIKE '%" & txtSearch.Text & "%'"
        'searchname = "select fcLastName + ',' + fcFirstName from dbo.CIMS_m_Member " & _
        '"where fcLastName LIKE '%" & txtSearch.Text & "%'"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, searchno)
                While rd.Read
                    lstNumbers.Items.Add(rd.Item(0).ToString)
                End While
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtSearchName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSearchName.KeyPress
        If e.KeyChar = Chr(13) Then
            Call SearchMember(Me.txtSearchName.Text)
        End If
    End Sub

#Region "Get Salary type to all member"
    Private Sub GetSalaryTypeallMember(ByVal Empid As String)
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "MSS_MembersInfo_GetEmploymentInfo", _
                                     New SqlParameter("@EmployeeNo", Empid))
        Try
            While rd.Read
                Dim xth As String = rd.Item(10)
                Me.lblRate.Text = "Salary Rate ( " + xth + " )"
                txtRateMonthly.Text = rd.Item("Salary").ToString
            End While
            rd.Close()
        Catch ex As Exception

        End Try
    End Sub
#End Region
    Private Sub lstname_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstname.SelectedIndexChanged
        Dim x, result As String
        x = lstname.SelectedItem.ToString
        Dim y As String() = x.Split(New Char() {","})
        result = y(0)
        frmloan_Penalties.getlastname() = result
        frmLoan_ApplicationComputation.getEmpinfo = result
        GetEmployeeID() = result
        panelStep1.Enabled = True
        panelstep2.Enabled = False
        panelStep3.Enabled = False
        Call GetSalaryTypeallMember(result)
        ClearFields()
    End Sub

    Private Function NumbersOnly(ByVal pstrChar As Char, ByVal oTextBox As TextBox) As Boolean
        'validate the entry for a textbox limiti // ng it to only numeric values and the dec // imal point 
        If (Convert.ToString(pstrChar) = "." And InStr(oTextBox.Text, ".")) Then Return True
        'accept only one instance of the decimal point 
        If Convert.ToString(pstrChar) <> "." And pstrChar <> vbBack Then Return IIf(IsNumeric(pstrChar), False, True)
        'check if numeric is returned End If Return False 'for backspace 
    End Function

    Private Sub txtPrincipalAmount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPrincipalAmount.KeyPress
        e.Handled = NumbersOnly(e.KeyChar, txtPrincipalAmount)
    End Sub

    Private Sub txtRateMonthly_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRateMonthly.KeyPress
        e.Handled = NumbersOnly(e.KeyChar, txtRateMonthly)
    End Sub
    Private Sub txtSSSLoan_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSSSLoan.KeyPress
        e.Handled = NumbersOnly(e.KeyChar, txtSSSLoan)
    End Sub
    Private Sub txtPagibigLoan_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPagibigLoan.KeyPress
        e.Handled = NumbersOnly(e.KeyChar, txtPagibigLoan)
    End Sub
    Private Sub txtPhilHealth_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPhilHealth.KeyPress
        e.Handled = NumbersOnly(e.KeyChar, txtPhilHealth)
    End Sub
    Private Sub txtCompanyLoan_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCompanyLoan.KeyPress
        e.Handled = NumbersOnly(e.KeyChar, txtCompanyLoan)
    End Sub
    Private Sub txtOtherNonRecurring_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtOtherNonRecurring.KeyPress
        e.Handled = NumbersOnly(e.KeyChar, txtOtherNonRecurring)
    End Sub

    Private Sub txtCapacityToPay_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCapacityToPay.KeyPress
        e.Handled = NumbersOnly(e.KeyChar, txtCapacityToPay)
    End Sub
    Private Sub txtNote_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMaxLoanAmount.KeyPress
        e.Handled = NumbersOnly(e.KeyChar, txtMaxLoanAmount)
    End Sub

#End Region


    Private Sub btnshowpenalties_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnshowpenalties.Click
        frmloan_Penalties.ShowDialog()
    End Sub

    Private Sub txtPrincipalAmount_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtPrincipalAmount.Validating
        'Call ValidatePrincipalAmount()
    End Sub
#Region "Validate Principal Amount"
    Private Sub ValidatePrincipalAmount()
        Dim thou, result2 As Double
        Dim x As String
        If Me.txtPrincipalAmount.Text = "" Then
            Me.txtPrincipalAmount.Text = "0.00"
        End If
        thou = Double.Parse(Me.txtPrincipalAmount.Text)
        x = thou / 100
        Dim y As String() = x.Split(New Char() {"."})
        Dim z As String = y(0)
        result2 = Double.Parse(z) * 100
        Me.txtPrincipalAmount.Text = result2
    End Sub
#End Region

    Private Sub btnshowcompute1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub cboChooseLoanType_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanType.TextChanged
        getloadcode() = Me.cboLoanType.Text
        _getcode(loancode)
    End Sub

    Private Sub btncompute1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncompute1.Click
        '8/15/14
        If cboModeofPayment.Text = "" Then
            frmMsgBox.txtMessage.Text = "Unable to proceed,Please select mode of payment!"
            frmMsgBox.ShowDialog()
            Exit Sub ' break
        End If

        'added 8/7/14 Vince
        If inEditMode = True Then
            frmLoan_ApplicationComputation.getEmpinfo() = GetEmployeeID()
        End If

        LoanCategory = xSelectedIntMethod

        If Me.txtPrincipalAmount.Text = "" Then
            Me.txtPrincipalAmount.Text = "0.00"
        End If

        Dim maxLoanAmount As Decimal = CDec(txtMaxLoanAmount.Text)
        Dim maxPrincipal As Decimal = CDec(txtPrincipalAmount.Text)

        If LoanCategory = "" Then
            MsgBox("Please Select Interest Method to continue.", vbInformation, "...")
            Exit Sub
        Else
            If maxPrincipal > maxLoanAmount Then
                If MessageBox.Show("Principal is greater than Maximum Loanable Amount. Proceed?", "Principal", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Call ComputeLoanApplication()
                Else
                    txtPrincipalAmount.Focus()
                    txtPrincipalAmount.SelectAll()
                End If
            Else
                Call ComputeLoanApplication()
            End If
        End If

    End Sub

    Private Sub ComputeLoanApplication()
        If Me.txtRestructure.Text = "" Then
            frmLoan_ApplicationComputation.getloanRestructuredno() = ""
        Else
            frmLoan_ApplicationComputation.getloanRestructuredno() = Me.txtRestructure.Text
        End If
        If Me.TxtCoopLoan.Text = "" Then
            frmLoan_ApplicationComputation.getloanPreterminated() = ""
        Else
            frmLoan_ApplicationComputation.getloanPreterminated() = Me.TxtCoopLoan.Text
        End If
        frmLoan_ApplicationComputation.getloanPrincipal() = Me.txtPrincipalAmount.Text
        frmLoan_ApplicationComputation.getloadAppdate() = Me.dtpLoanApplicationDate.Text
        frmLoan_ApplicationComputation.ShowDialog()
        frmLoan_ApplicationComputation.txtIntRefundRestruc.Text = ""

    End Sub

    Private Sub cmdSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        Call SearchMember(Me.txtSearchName.Text)
    End Sub
#Region "Get EMP idnumber"
    Private Sub getEmpIdnumber(ByVal id As String)
        Dim gcon As New Clsappconfiguration
        Dim searchno As String
        lstNumbers.Items.Clear()
        'lstname.Items.Clear()
        searchno = "select fcEmployeeNo from dbo.CIMS_m_Member_Employment " & _
                    "where fcEmployeeNo LIKE '%" & id & "%'"
        'searchname = "select fcLastName + ',' + fcFirstName from dbo.CIMS_m_Member " & _
        '"where fcLastName LIKE '%" & txtSearch.Text & "%'"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, searchno)
                While rd.Read
                    lstNumbers.Items.Add(rd.Item(0).ToString)
                End While
            End Using
        Catch ex As Exception

        End Try
    End Sub
#End Region
#Region "Get Emp lastname"
    Private Sub getEmplastname(ByVal lastname As String)
        Dim gcon As New Clsappconfiguration
        lstname.Items.Clear()
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "MSS_MembersInfo_SearchName", New SqlParameter("@membername", txtSearchName.Text))
            While rd.Read
                lstname.Items.Add(rd.Item(0).ToString)
            End While
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub

#End Region
#Region "Search Member"
    Private Sub SearchMember(ByVal searchname As String)
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList_SearchName", _
                                                         New SqlParameter("@SEARCH", searchname))
        With Me.lstMembers
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("IDnumber", 70, HorizontalAlignment.Left)
            .Columns.Add("Member's Fullname", 200, HorizontalAlignment.Left)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        .SubItems.Add(rd.Item(1))
                    End With
                End While
                rd.Close()
                gcon.sqlconn.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Load Member list")
            End Try
        End With

        'If Me.optNumber.Checked = True Then
        '    Call getEmpIdnumber(Me.txtSearch.Text.Trim)
        'End If
        'If Me.optName.Checked = True Then
        '    Call getEmplastname(Me.txtSearch.Text.Trim)
        'End If
    End Sub
#End Region

    Private Sub cboTerms_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTerms.SelectedIndexChanged
        frmLoan_ApplicationComputation.getloanterm() = Decimal.Parse(Me.cboTerms.Text)
    End Sub

    Private Sub txtPrincipalAmount_LostFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPrincipalAmount.LostFocus
        If txtPrincipalAmount.Text = "" Then
            txtPrincipalAmount.Text = "0.00"
        End If

        txtPrincipalAmount.Text = Format(CDec(txtPrincipalAmount.Text), "##,##0.00")

        frmLoan_ApplicationComputation.getloanPrincipal() = Double.Parse(Me.txtPrincipalAmount.Text)
    End Sub

    Private Sub txtRateMonthly_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRateMonthly.LostFocus
        If txtRateMonthly.Text = "" Then
            txtRateMonthly.Text = "0.00"
        End If

        Try
            txtRateMonthly.Text = Format(CDec(txtRateMonthly.Text), "##,##0.00")
        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtSSSLoan_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSSSLoan.LostFocus
        If txtSSSLoan.Text = "" Then
            txtSSSLoan.Text = "0.00"
        End If

        Try
            txtSSSLoan.Text = Format(CDec(txtSSSLoan.Text), "##,##0.00")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtPagibigLoan_LostFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPagibigLoan.LostFocus
        If Me.txtPagibigLoan.Text = "" Then
            Me.txtPagibigLoan.Text = "0.00"
        End If

        Try
            txtPagibigLoan.Text = Format(CDec(txtPagibigLoan.Text), "##,##0.00")
        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtPhilHealth_LostFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPhilHealth.LostFocus
        If Me.txtPhilHealth.Text = "" Then
            Me.txtPhilHealth.Text = "0.00"
        End If

        Try
            txtPhilHealth.Text = Format(CDec(txtPhilHealth.Text), "##,##0.00")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtCompanyLoan_LostFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCompanyLoan.LostFocus
        If txtCompanyLoan.Text = "" Then
            txtCompanyLoan.Text = "0.00"
        End If

        Try
            txtCompanyLoan.Text = Format(CDec(txtCompanyLoan.Text), "##,##0.00")
        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtOtherNonRecurring_LostFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtOtherNonRecurring.LostFocus
        If txtOtherNonRecurring.Text = "" Then
            txtOtherNonRecurring.Text = "0.00"
        End If

        Try
            txtOtherNonRecurring.Text = Format(CDec(txtOtherNonRecurring.Text), "##,##0.00")
        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtContribution_LostFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtContribution.LostFocus
        If Me.txtContribution.Text = "" Then
            Me.txtContribution.Text = "0.00"
        End If

        Try
            txtContribution.Text = Format(CDec(txtContribution.Text), "##,##0.00")
        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtCapacityToPay_LostFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCapacityToPay.LostFocus
        If txtCapacityToPay.Text = "" Then
            txtCapacityToPay.Text = "0.00"
        End If

        Try
            txtCapacityToPay.Text = Format(CDec(txtCapacityToPay.Text), "##,##0.00")
        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtNote_LostFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMaxLoanAmount.LostFocus
        If txtMaxLoanAmount.Text = "" Then
            txtMaxLoanAmount.Text = "0.00"
        End If

        Try
            txtMaxLoanAmount.Text = Format(CDec(txtMaxLoanAmount.Text), "##,##0.00")
        Catch ex As Exception

        End Try

    End Sub

    Private Sub chkPenalties_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPenalties.CheckedChanged
        If Me.chkPenalties.Checked = True Then
            frmLoan_ApplicationComputation.getloadincludePen() = 1
        Else
            frmLoan_ApplicationComputation.getloadincludePen() = 0
        End If
    End Sub

    Private Sub TxtCoopLoan_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtCoopLoan.TextChanged
        frmLoan_ApplicationComputation.getloanPreterminated() = Me.TxtCoopLoan.Text
    End Sub

    Private Sub lstMembers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstMembers.Click
        inEditMode = False
        loanStatus = "On-Process"
        SelectMemberPrepareForLoan()
        PrepareColumns()
        cboProject.Text = "Select Project"
    End Sub

    Private Sub SelectMemberPrepareForLoan()
        GetEmployeeID() = Me.lstMembers.SelectedItems(0).Text
        'GetEmployeeID() 
        frmLoan_Processing.onprocess = GetEmployeeID()  'Get on-process loan for continuing

        frmloan_Penalties.getempno() = GetEmployeeID()

        frmLoan_ApplicationComputation.getEmpinfo() = GetEmployeeID()

        Call ClearFields()
        Call SetupPanel()
        Call GetSalaryTypeallMember(Me.lstMembers.SelectedItems(0).Text)
        If optNew.Checked = True Then
            Call Load_LoanTypes_ForNewLoans(GetEmployeeID(), "Approved")
        ElseIf optRestructure.Checked = True Then
            Call Load_LoanTypes_Restructure()
        End If

        'Form Header
        Dim memberName As String = " ( " + Me.lstMembers.SelectedItems(0).Text.ToUpper + " : " + Me.lstMembers.SelectedItems(0).SubItems(1).Text.ToUpper + " )"
        Text = "New Loan" + memberName
        lblStepOne.Text = "Step 1 - " + memberName

        Call ResetMaximumLoanableAmount()
        GetContribution()
    End Sub

    Private Sub SetupPanel()
        panelStep1.Enabled = True
        panelstep2.Enabled = False
        panelStep3.Enabled = False
    End Sub


    Private Sub txtContribution_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtContribution.TextChanged
        If txtContribution.Text = "" Then
            txtContribution.Text = "0.00"
        End If

        Try
            txtContribution.Text = Format(CDec(txtContribution.Text), "##,##0.00")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtCapacityToPay_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCapacityToPay.TextChanged
        If txtCapacityToPay.Text = "" Then
            txtCapacityToPay.Text = "0.00"
        End If

        Try
            txtCapacityToPay.Text = Format(CDec(txtCapacityToPay.Text), "##,##0.00")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtNote_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMaxLoanAmount.TextChanged
        If txtMaxLoanAmount.Text = "" Then
            txtMaxLoanAmount.Text = "0.00"
        End If

        Try
            txtMaxLoanAmount.Text = Format(CDec(txtMaxLoanAmount.Text), "##,##0.00")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtRestructure_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRestructure.TextChanged
        frmLoan_ApplicationComputation.getloanRestructuredno() = txtRestructure.Text
    End Sub


    Private Sub lstMembers_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lstMembers.KeyDown
        SelectMemberPrepareForLoan()
    End Sub

    Private Sub lstMembers_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lstMembers.KeyUp
        SelectMemberPrepareForLoan()
    End Sub

    Private Sub btnCheckLoanNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckLoanNo.Click
        frmBrowseLoanNo.StartPosition = FormStartPosition.CenterScreen
        frmBrowseLoanNo.ShowDialog()
    End Sub

    Private Sub btnComaker_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnComaker.Click
        frmAddCoMaker.StartPosition = FormStartPosition.CenterScreen
        frmAddCoMaker.ShowDialog()
    End Sub

#Region "Comaker"
    'Added by Vincent Nacar
    '6/18/2014
    Public xCtr As Integer = 0

    Private Sub PrepareColumns()
        xCtr = 0
        dgvComakerList.Rows.Clear()
        dgvComakerList.Columns.Clear()
        Dim EmpNo As New DataGridViewTextBoxColumn
        Dim ClientName As New DataGridViewTextBoxColumn
        Dim DocNo As New DataGridViewTextBoxColumn
        Dim AcctName As New DataGridViewTextBoxColumn
        Dim Status As New DataGridViewTextBoxColumn
        Dim Amount As New DataGridViewTextBoxColumn
        With EmpNo
            .HeaderText = "Client ID"
            .Name = "EmpNo"
            .DataPropertyName = "EmpNo"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With ClientName
            .HeaderText = "Name"
            .Name = "ClientName"
            .DataPropertyName = "ClientName"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With DocNo
            .HeaderText = "Doc. Ref"
            .Name = "DocNo"
            .DataPropertyName = "DocNo"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With AcctName
            .HeaderText = "Acct. Ref"
            .Name = "AcctName"
            .DataPropertyName = "AcctName"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With Status
            .HeaderText = "Status"
            .Name = "Status"
            .DataPropertyName = "Status"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With Amount
            .HeaderText = "Amount"
            .Name = "Amount"
            .DataPropertyName = "Amount"
        End With
        With dgvComakerList
            .Columns.Clear()
            .Columns.Add(EmpNo)
            .Columns.Add(ClientName)
            .Columns.Add(DocNo)
            .Columns.Add(AcctName)
            .Columns.Add(Status)
            .Columns.Add(Amount)
            .Columns("Amount").DefaultCellStyle.Format = "##,##0.00"
        End With
    End Sub

    Public Sub _AddComaker(ByVal empid As String,
                           ByVal cname As String,
                           ByVal docno As String,
                           ByVal acctname As String,
                           ByVal stat As String,
                           ByVal amt As String)
        Try
            With dgvComakerList
                .Rows.Add()
                .Item("EmpNo", xCtr).Value = empid
                .Item("ClientName", xCtr).Value = cname
                .Item("DocNo", xCtr).Value = docno
                .Item("AcctName", xCtr).Value = acctname
                .Item("Status", xCtr).Value = stat
                .Item("Amount", xCtr).Value = amt
                xCtr += 1
            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnDeleteRow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteRow.Click
        Try
            For Each row As DataGridViewRow In dgvComakerList.SelectedRows
                dgvComakerList.Rows.Remove(row)
            Next
            xCtr -= 1
        Catch ex As Exception
            xCtr = 0
        End Try
    End Sub

#End Region

    Private Sub dgvComakerList_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvComakerList.CellEnter
        dgvComakerList.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        dgvComakerList.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvComakerList.Columns(5).DefaultCellStyle.Format = "N2"
    End Sub

    Private Sub dgvComakerList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvComakerList.DoubleClick
        dgvComakerList.SelectionMode = DataGridViewSelectionMode.CellSelect
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        If MsgBox("Are you sure?", vbYesNo, "Exit Loan Application") = vbYes Then
            Me.Close()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub txtSearchName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchName.TextChanged
        Call SearchMember(Me.txtSearchName.Text)
    End Sub

    Private Sub GetLoanDetails_ByClient(ByVal employeeno As String)
        Try
            frmExistingLoan.empno = employeeno
            frmExistingLoan.mode = "byclient"
            frmExistingLoan.Text = Me.lstMembers.SelectedItems(0).Text.ToUpper + " : " + Me.lstMembers.SelectedItems(0).SubItems(1).Text.ToUpper + " - " + "Existing Loans"
            frmExistingLoan.StartPosition = FormStartPosition.CenterScreen
            frmExistingLoan.ShowDialog()
        Catch ex As Exception
        End Try
    End Sub

    Public Sub LoadAll_LoanTypes() 'updated 8/6/2014
        cboLoanType.Items.Clear()
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "Get_AllLoan_Types")
            While rd.Read
                cboLoanType.Items.Add(rd.Item("fcLoanTypeName")).ToString()
            End While
        End Using
    End Sub

    Public Sub LoadExistingLoan_Comakers()
        PrepareColumns()
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(gCon.cnstring, "_Get_ExistingLoan_Comaker",
                                          New SqlParameter("@LoanNo", txtLoanNo.Text))
            While rd.Read
                With dgvComakerList
                    .Rows.Add()
                    .Rows(xCtr).Cells(0).Value = rd(0)
                    .Rows(xCtr).Cells(1).Value = rd(1)
                    .Rows(xCtr).Cells(2).Value = rd(2)
                    .Rows(xCtr).Cells(3).Value = rd(3)
                    .Rows(xCtr).Cells(4).Value = rd(4)
                    .Rows(xCtr).Cells(5).Value = rd(5)
                End With
                xCtr += 1
            End While
        Catch ex As Exception
        End Try
    End Sub

    Public CarryForwardPrincipal As String

    Private Sub rdCarryForward_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdCarryForward.Click
        If rdCarryForward.Checked = True Then
            'getclassfication() = "New"
            'frmLoan_ApplicationComputation.getloanClass = "New"
            Call LoadAll_LoanTypes()
            txtRestructure.Text = ""
            cboTerms.Items.Clear()
            cmdDone.Enabled = True

            frmLoan_Processing.onprocess = GetEmployeeID()
            Call GetLoanDetails_ByClient(GetEmployeeID())
        End If
    End Sub

    'Private Sub rdDeducted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Select Case rdDeducted.Checked
    '        Case True
    '            cboMonthDeduction.Enabled = True
    '        Case False
    '            cboMonthDeduction.Enabled = False
    '    End Select
    'End Sub

    'Private Sub rdDeclining_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    rdDeducted.Checked = False
    '    rdAddon.Checked = False
    'End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        frmExistingLoan.Text = "Client Loans"
        frmExistingLoan.mode = "all"
        frmExistingLoan.StartPosition = FormStartPosition.CenterScreen
        frmExistingLoan.ShowDialog()
    End Sub

    Public Sub _GEtLoanDetails_ForEditing(ByVal loanno As String)
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(gCon.cnstring, "_Select_OtherLoanDetails_ForEditing",
                                          New SqlParameter("@LoanNo", loanno))
            While rd.Read
                dtpLoanApplicationDate.Value = rd(0)
                If rd(1) = "Cheque" Then
                    frmLoan_ApplicationComputation.chkCheck.Checked = True
                End If
                If rd(1) = "ATM" Then
                    frmLoan_ApplicationComputation.chkATM.Checked = True
                End If
                If rd(1) = "Cash" Then
                    frmLoan_ApplicationComputation.chkCash.Checked = True
                End If

                'Select Case rd(2)
                '    Case "Straight Add-On"
                '        rdStraight.Checked = True
                '        rdAddon.Checked = True
                '    Case "Straight Deducted"
                '        rdStraight.Checked = True
                '        rdDeducted.Checked = True
                '    Case "Diminishing Declining"
                '        rdDim.Checked = True
                '        rdDeclining.Checked = True
                '    Case "Diminishing"
                '        rdDim.Checked = True
                'End Select
            End While
        Catch ex As Exception

        End Try
    End Sub

    Public Sub ClearFields_AfterLoan()
        Try

        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Me.Refresh()
        ClearFields()
    End Sub

    'Private Sub rdDim_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    rdAddon.Checked = False
    '    rdDeducted.Checked = False
    '    rdCustom.Checked = False
    'End Sub

    Public loanStatus As String = ""

    Private Sub cboModeofPayment_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboModeofPayment.SelectedValueChanged
        Try
            Select Case cboModeofPayment.Text
                Case "DAILY"
                    txtnoofpayment.Text = cboTerms.Text * 30
                Case "WEEKLY"
                    txtnoofpayment.Text = cboTerms.Text * 4
                Case "SEMI-MONTHLY"
                    txtnoofpayment.Text = cboTerms.Text * 2
                Case "MONTHLY"
                    txtnoofpayment.Text = cboTerms.Text
                Case "QUARTERLY"
                    txtnoofpayment.Text = cboTerms.Text / 3
                Case "ANNUALLY"
                    txtnoofpayment.Text = cboTerms.Text / 12
                Case "LUMP SUM"
                    txtnoofpayment.Text = 1
            End Select
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Please Select Terms!"
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        frmReport_LoanApplication.LoadREport2(txtLoanNo.Text)
        frmReport_LoanApplication.WindowState = FormWindowState.Maximized
        frmReport_LoanApplication.ShowDialog()
    End Sub

    Private Sub btnCustomIntMethod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmIntMethodSetting.StartPosition = FormStartPosition.CenterScreen
        frmIntMethodSetting.ShowDialog()
    End Sub

    'Private Sub rdCustom_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    rdStraight.Checked = False
    '    rdDeducted.Checked = False
    '    rdDim.Checked = False
    '    rdAddon.Checked = False
    'End Sub

    'Private Sub rdFormula_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    rdStraight.Checked = False
    '    rdDeducted.Checked = False
    '    rdDim.Checked = False
    '    rdAddon.Checked = False
    '    rdCustom.Checked = False
    '    frmAmortFormula.StartPosition = FormStartPosition.CenterScreen
    '    frmAmortFormula.ShowDialog()
    'End Sub

    Private Sub rdRestructure_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdRestructure.Click
        If rdRestructure.Checked = True Then
            'getclassfication() = "New"
            'frmLoan_ApplicationComputation.getloanClass = "New"
            Call LoadAll_LoanTypes()
            txtRestructure.Text = ""
            cboTerms.Items.Clear()
            cmdDone.Enabled = True

            frmLoan_Processing.onprocess = GetEmployeeID()
            Call GetLoanDetails_ByClient(GetEmployeeID())
            inEditMode = True
        End If
    End Sub

#Region "Interest Method Load 3/4/15"
    Public xSelectedIntMethod As String
    Public xIntMode As String

    Private Sub _Prep_StandardIntCol()
        dgvStandardIntMethod.Rows.Clear()
        dgvStandardIntMethod.Columns.Clear()
        Dim desc As New DataGridViewTextBoxColumn
        With desc
            .Name = "desc"
            .HeaderText = ""
            .ReadOnly = True
        End With
        With dgvStandardIntMethod
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .Font = New Font("Verdana", 9, FontStyle.Regular)
            .Columns.Add(desc)
        End With
    End Sub
    Private Sub _LoadStandard_IntMethod()
        _Prep_StandardIntCol()
        With dgvStandardIntMethod
            .Rows.Add("Straight")
            .Rows.Add("Straight Add-On")
            .Rows.Add("Straight Deducted")
            .Rows.Add("Diminishing")
            .Rows.Add("Ascending/Declining")
            .ClearSelection()
        End With
    End Sub
    Private Sub _Prep_CustomizedIntCol()
        dgvCustomizedIntMethod.Rows.Clear()
        dgvCustomizedIntMethod.Columns.Clear()
        Dim desc As New DataGridViewTextBoxColumn
        With desc
            .Name = "desc"
            .HeaderText = ""
            .ReadOnly = True
        End With
        With dgvCustomizedIntMethod
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .Font = New Font("Verdana", 9, FontStyle.Regular)
            .Columns.Add(desc)
        End With
    End Sub
    Private Sub _LoadCustomized_Intmethod()
        _Prep_CustomizedIntCol()
        With dgvCustomizedIntMethod
            .Rows.Add("_Formula_loan_Computation_V5")
            .Rows.Add("_Formula_loan_v8_300k")
            .Rows.Add("_Formula_loan_300k_NEW")
            .ClearSelection()
        End With
    End Sub

    Private Sub _GetStandardIntMethod() Handles dgvStandardIntMethod.Click
        xIntMode = "Standard"
        xSelectedIntMethod = dgvStandardIntMethod.CurrentRow.Cells(0).Value.ToString
    End Sub
    Private Sub _GetCustomizedIntMethod() Handles dgvCustomizedIntMethod.Click
        xIntMode = "Custom"
        xSelectedIntMethod = dgvCustomizedIntMethod.CurrentRow.Cells(0).Value.ToString
    End Sub

    Private Sub _xClearSelection() Handles TabControl1.Click
        dgvStandardIntMethod.ClearSelection()
        dgvCustomizedIntMethod.ClearSelection()
        xIntMode = ""
        xSelectedIntMethod = ""
    End Sub

#End Region

    Private Sub btnAccounts_Click(sender As System.Object, e As System.EventArgs) Handles btnAccounts.Click
        frmViewAcctFilter.vID = GetEmployeeID
        frmViewAcctFilter.StartPosition = FormStartPosition.CenterScreen
        frmViewAcctFilter.ShowDialog()
    End Sub

    Private Sub txtLoanNo_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtLoanNo.TextChanged

    End Sub
End Class

