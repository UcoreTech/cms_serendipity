Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports WMPs_Money_Figure_Convert_to_Words
Public Class frmLoan_Processing
#Region "Public Variables"
    Private gcon As New Clsappconfiguration
    Private Empinfo As String
    Private amt As String

    Private editMode As Boolean = False
    Public onprocess As String = ""

#End Region
#Region "Public Property"
    Public Property getEmpinfo() As String
        Get
            Return Empinfo
        End Get
        Set(ByVal value As String)
            Empinfo = value
        End Set
    End Property
    Public Property getamt() As String
        Get
            Return amt
        End Get
        Set(ByVal value As String)
            amt = value
        End Set
    End Property

    Private loanNumber As String
    Public Property GetLoanNo() As String
        Get
            Return loanNumber
        End Get
        Set(ByVal value As String)
            loanNumber = value
        End Set
    End Property

    Private loanID As String
    Public Property GetLoanID() As String
        Get
            Return loanID
        End Get
        Set(ByVal value As String)
            loanID = value
        End Set
    End Property


#End Region
    Private Sub cmdCheck_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCheck.Click
        If IsLoanTypeCurrent(getEmpinfo(), GetLoanNo()) = False Then
            'CIMS_t_MemberLoans_Reqts per member
            'MSS_Loans_AddEdit_MemberLoanRequirements
            'MSS_Loans_Retrieve_LoanRequirements_PerMember
            frmMasterfile_NewMemberRequirements.getloanNumber() = Me.txtLoanNumber.Text
            frmMasterfile_NewMemberRequirements.getloantype() = Me.lblExpressLoan.Text
            frmMasterfile_NewMemberRequirements.Show()
        Else
            MessageBox.Show("The member has a current loan with this loan type already. You cannot proceed.", "Loan Processing Validation", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub frmExistingLoans_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Call Load_MemberNumber()
        Call Load_MemberName()
        Call GetloanType()
        Call GetLoanStatus()
        Call LoadStatusonlistview()
        tabLoanDetails.TabPages.Remove(maxLoanAmt)
        tabLoanDetails.TabPages.Remove(amortization)

        txtSearchLastname.Text = onprocess
        'Me.optNumber.Checked = True
    End Sub
#Region "Get Loan Number based on EmpID"
    Private Sub GetloanNumber(ByVal EmpID As String)
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "MSS_LoanTracking_GetLoans", _
                                    New SqlParameter("@EmployeeNo", EmpID))
        Try
            Me.CboLoanNumber.Items.Clear()
            While rd.Read
                Me.CboLoanNumber.Items.Add(rd.Item(3))
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Get Loan Number")
        End Try
    End Sub
#End Region
#Region "Get Loan Status"
    Private Sub GetLoanStatus()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CIMS_getloanStatus")
        Me.CboLoanStatus.Items.Clear()
        While rd.Read
            Me.CboLoanStatus.Items.Add(rd.Item(0))
        End While
        rd.Close()
        gcon.sqlconn.Close()
    End Sub
#End Region
#Region "Get Loan Type"
    Private Sub GetloanType()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CIMS_getloanType")
        Me.CboLoanType.Items.Clear()
        While rd.Read
            Me.CboLoanType.Items.Add(rd.Item(0))
        End While
        rd.Close()
        gcon.sqlconn.Close()
    End Sub
#End Region
#Region "Load Member number"
    Private Sub Load_MemberNumber()
        Dim gCon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList")
        While rd.Read
            lstNumber.Items.Add(rd.Item("Employee No")).ToString()
        End While
        rd.Close()
        gCon.sqlconn.Close()

    End Sub
#End Region
#Region "Load Member fullname"
    Private Sub Load_MemberName()
        Dim gCon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList")
        With Me.lstExistingLoans
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("IDnumber", 70, HorizontalAlignment.Left)
            .Columns.Add("Member's Fullname", 200, HorizontalAlignment.Left)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        .SubItems.Add(rd.Item(1))
                    End With
                End While
                rd.Close()
                gCon.sqlconn.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Load Member list")
            End Try
        End With

        'While rd.Read
        '    lstNames.Items.Add(rd.Item("Full Name")).ToString()
        'End While
        'rd.Close()
        'gCon.sqlconn.Close()
    End Sub
#End Region
#Region "View Loan Summary Using Employee Number"
    Public Sub ViewLoanSummary(ByVal Empinfo As String)
        GrdLoanSummary.Columns.Clear()
        GrdLoanSummary.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "MSS_LoanTracking_GetLoans", _
                                          New SqlParameter("@EmployeeNo", Empinfo))
            With GrdLoanSummary
                .DataSource = ds.Tables(0)
                .Columns(7).Visible = False
                .Columns(8).Visible = False
                .Columns(9).Visible = False
                .Columns(10).Visible = False
                .Columns(11).Visible = False
                .Columns(12).Visible = False
                .Columns(13).Visible = False
                .Columns(14).Visible = False
                .Columns(15).Visible = False
                .Columns(16).Visible = False
                .Columns(17).Visible = False
                .Columns(18).Visible = False
                .Columns(19).Visible = False
                .Columns(20).Visible = False
                .Columns(21).Visible = False
                .Columns(22).Visible = False
                .Columns(23).Visible = False
                .Columns(24).Visible = False
                .Columns(25).Visible = False
                .Columns(26).Visible = False
            End With
            _Gen_ViewButton()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
#End Region

#Region "Added by Vince 10/4/14"
    Private Sub _Gen_ViewButton()
        Dim btnView As New DataGridViewButtonColumn
        With btnView
            .Name = "btnView"
            .Text = "View Details"
            .HeaderText = ""
            .UseColumnTextForButtonValue = True
            .DefaultCellStyle.ForeColor = Color.Black
        End With
        With GrdLoanSummary
            .Columns.Add(btnView)
        End With
    End Sub

    Private Sub GrdLoanSummary_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GrdLoanSummary.CellClick
        'If e.ColumnIndex = 27 Then
        '    frmReport_LoanDetails.LoadREport(getEmpinfo, GrdLoanSummary.SelectedRows(0).Cells(3).Value)
        '    frmReport_LoanDetails.WindowState = FormWindowState.Maximized
        '    frmReport_LoanDetails.StartPosition = FormStartPosition.CenterScreen
        '    frmReport_LoanDetails.ShowDialog()
        'End If
    End Sub

#End Region

#Region "View Loan Summary Using Employee GUID"
    Private Sub ViewloanSummaryGUID(ByVal GUID As String)
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("MSS_LoanTracking_GetLoans1", gcon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        gcon.sqlconn.Open()

        With cmd.Parameters
            .Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = GUID
        End With

        ad.SelectCommand = cmd
        ad.Fill(ds, "CIMS_m_Member")

        Try
            Me.GrdLoanSummary.DataSource = ds
            Me.GrdLoanSummary.DataMember = "CIMS_m_Member"

            cmd.ExecuteNonQuery()
            gcon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
#End Region
#Region "filtered loan Summary"
    Private Sub FilteredloanSummary(ByVal Empid As String, ByVal loanStatus As String, ByVal loanType As String, ByVal loanno As String, _
                                    ByVal datefrom As Date, ByVal dateto As Date)
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("MSS_LoanTracking_GetLoans_Filtered", gcon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        gcon.sqlconn.Open()

        With cmd.Parameters
            .Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = Empid
            .Add("@status", SqlDbType.VarChar, 50).Value = loanStatus
            .Add("@loanType", SqlDbType.VarChar, 50).Value = loanType
            .Add("@loanNo", SqlDbType.VarChar, 50).Value = loanno
            .Add("@dateFrom", SqlDbType.SmallDateTime).Value = datefrom
            .Add("@dateTo", SqlDbType.SmallDateTime).Value = dateto
        End With

        ad.SelectCommand = cmd
        ad.Fill(ds, "CIMS_m_Member")

        Try
            Me.GrdLoanSummary.DataSource = ds
            Me.GrdLoanSummary.DataMember = "CIMS_m_Member"
            Me.GrdLoanSummary.Columns(29).Visible = False
            Me.GrdLoanSummary.Columns(30).Visible = False
            cmd.ExecuteNonQuery()
            gcon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
#End Region
#Region "Load status on listview"
    Private Sub LoadStatusonlistview()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CIMS_getloanStatus")
        With Me.ListStatus
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Loan Status", 300, HorizontalAlignment.Left)

            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))

                    End With
                End While
                rd.Close()
                gcon.sqlconn.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "list of status")
            End Try
        End With
    End Sub
#End Region
    'Private Sub optNumber_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optNumber.CheckedChanged
    '    Me.txtSearchnumber.Visible = True
    '    Me.lstNumber.Visible = True
    '    Me.txtSearchLastname.Visible = False
    '    Me.lstNames.Visible = False

    'End Sub

    'Private Sub optName_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optName.CheckedChanged
    '    Me.txtSearchLastname.Visible = True
    '    Me.lstNames.Visible = True
    '    Me.txtSearchnumber.Visible = False
    '    Me.lstNumber.Visible = False
    'End Sub

    Private Sub lstNumber_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstNumber.SelectedIndexChanged
        getEmpinfo() = Me.lstNumber.SelectedItem.ToString
        FrmLoan_ForwardRequirements.getEmpinfo() = Me.lstNumber.SelectedItem.ToString
        FrmLoan_ForwardRequirements.getEmpinfo() = Me.GrdLoanSummary.CurrentRow.Cells(31).Value
        Call ViewLoanSummary(Empinfo) '(Me.lstNumber.SelectedItem.ToString)
    End Sub

    Private Sub GrdLoanSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles GrdLoanSummary.Click
        Try
            getEmpinfo() = GrdLoanSummary.CurrentRow.Cells("EmpNo").Value.ToString()
            GetLoanNo() = GrdLoanSummary.CurrentRow.Cells("Loan No").Value.ToString()
            GetLoanID() = GrdLoanSummary.CurrentRow.Cells("fk_Member_Loan").Value.ToString()

            Call _Prep_LessCol()
            Call GetLoanDetails()
            Call LoadComaker(GetLoanNo())
            Call _Load_Less_Details(GetLoanNo())
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "Grid Loan Summary", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub
#Region "Loan Details"

    Private Sub CheckLoanDetailStatus()
        If Me.txtStatus.Text = "On-Process" Then
            Me.cmdCheck.Enabled = True
            Me.btnview.Enabled = True
            ViewRequirementToolStripMenuItem.Enabled = True
            RequirementsForApprovalToolStripMenuItem.Enabled = True
        Else
            Me.cmdCheck.Enabled = False
            Me.btnview.Enabled = False
            ViewRequirementToolStripMenuItem.Enabled = False
            RequirementsForApprovalToolStripMenuItem.Enabled = False
        End If
    End Sub
    Private Sub GetLoanDetails()
        Dim loanNo As String = Me.GrdLoanSummary.CurrentRow.Cells(3).Value
        Me.GetLoanNo() = loanNo
        frmLoan_ReportOptions.GetLoanNo() = loanNo

        Using rd As SqlDataReader = ModifiedSqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "MSS_LoanTracking_GetLoans_PerLoan", _
                        New SqlParameter("@EmployeeNo", getEmpinfo()), _
                        New SqlParameter("@loanNo", loanNo))

            If rd.Read() Then
                txtmembername.Text = rd.Item(0).ToString()
                txtStatus.Text = rd.Item(1).ToString()
                txtLoanAppDate.Text = rd.Item(2).ToString()
                txtdDate.Text = rd.Item(2).ToString()
                txtLoanNumber.Text = rd.Item(3).ToString()
                txtDLoanNo.Text = rd.Item(3).ToString()
                lblExpressLoan.Text = rd.Item(4).ToString()
                txtClassification.Text = rd.Item(5).ToString()
                txtPrincipal.Text = Format(CDec(rd.Item(6).ToString()), "##,##0.00")
                txtDPrincipal.Text = Format(CDec(rd.Item(6).ToString()), "##,##0.00")
                txtDInt.Text = Format(CDec(rd.Item(8).ToString()), "##,##0.00")
                txtInterest.Text = Format(CDec(rd.Item(8).ToString()), "##,##0.00")

                txtServiceFee.Text = Format(CDec(rd.Item(11).ToString()), "##,##0.00")
                txtCapitalShare.Text = Format(CDec(rd.Item(13).ToString()), "##,##0.00")
                txtAmortization.Text = Format(CDec(rd.Item(14).ToString()), "##,##0.00")

                If txtStatus.Text <> "On-Process" Then
                    dteAmortStart.Value = rd.Item(15).ToString()
                    dteAmortEnd.Value = rd.Item(16).ToString()
                End If

                txtPenalty.Text = Format(CDec(rd.Item(18).ToString()), "##,##0.00")
                txtInterestRefund.Text = rd.Item(19).ToString()
                txtNetProceeds.Text = Format(CDec(rd.Item(20).ToString()), "##,##0.00")
                txtTerms.Text = rd.Item(21).ToString()
                txtDterms.Text = rd.Item(21).ToString()
                txtBalance.Text = Format(CDec(rd.Item(22).ToString()), "##,##0.00")
                txtPayment.Text = Format(CDec(rd.Item(23).ToString()), "##,##0.00")
                txtSalaryMonthly.Text = rd.Item(24).ToString()
                txtSSSloan.Text = rd.Item(25).ToString()
                txtpagibigloan.Text = rd.Item(26).ToString()
                txtphihealthloan.Text = rd.Item(27).ToString()
                txtcompanyloan.Text = rd.Item(28).ToString()
                txtothers.Text = rd.Item(29).ToString()
            End If

            Call CheckLoanDetailStatus()
        End Using
    End Sub
#End Region
    Private Sub txtSearchnumber_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchnumber.TextChanged
        Dim gcon As New Clsappconfiguration
        Dim searchno As String
        lstNumber.Items.Clear()

        searchno = "select fcEmployeeNo from dbo.CIMS_m_Member_Employment " & _
                    "where fcEmployeeNo LIKE '%" & txtSearchnumber.Text & "%'"


        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.Text, searchno)
            While rd.Read
                lstNumber.Items.Add(rd.Item(0).ToString)
            End While
            rd.Close()
            gcon.sqlconn.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtSearchLastname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSearchLastname.KeyPress
        If e.KeyChar = Chr(13) Then
            Call Searchmember(Me.txtSearchLastname.Text)
        End If
    End Sub

#Region "Get EMP idnumber"
    Private Sub getEmpIdnumber(ByVal id As String)
        Dim gcon As New Clsappconfiguration
        Dim searchno As String
        lstNumber.Items.Clear()

        searchno = "select fcEmployeeNo from dbo.CIMS_m_Member_Employment " & _
                    "where fcEmployeeNo LIKE '%" & id & "%'"

        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.Text, searchno)
            While rd.Read
                lstNumber.Items.Add(rd.Item(0).ToString)
            End While
            rd.Close()
            gcon.sqlconn.Close()
        Catch ex As Exception

        End Try
    End Sub
#End Region
#Region "Get Emp lastname"
    Private Sub getEmplastname(ByVal lastname As String)
        Dim gcon As New Clsappconfiguration
        lstNames.Items.Clear()
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "MSS_MembersInfo_SearchName", New SqlParameter("@membername", lastname.Trim))
            While rd.Read
                lstNames.Items.Add(rd.Item(0).ToString)
            End While
            rd.Close()
            gcon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub

#End Region
#Region "Search member"
    Private Sub Searchmember(ByVal searchname As String)
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList_SearchName", _
                                                          New SqlParameter("@SEARCH", searchname))
        With Me.lstExistingLoans
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("IDnumber", 70, HorizontalAlignment.Left)
            .Columns.Add("Member's Fullname", 200, HorizontalAlignment.Left)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        .SubItems.Add(rd.Item(1))
                    End With
                End While
                rd.Close()
                gcon.sqlconn.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Load Member list")
            End Try
        End With
        'If Me.optNumber.Checked = True Then
        '    Call getEmpIdnumber(Me.txtSearchnumber.Text.Trim)
        'End If
        'If Me.optName.Checked = True Then
        '    Call getEmplastname(Me.txtSearchLastname.Text.Trim)
        'End If
    End Sub
#End Region

    Private Sub cmdSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        Call Searchmember(Me.txtSearchLastname.Text)
    End Sub

    Private Sub lstNames_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstNames.SelectedIndexChanged
        Dim x, result As String
        x = lstNames.SelectedItem.ToString
        Dim y As String() = x.Split(New Char() {","})
        result = y(0)
        getEmpinfo() = result
        'Call getpkemployeeguid(result)
        Call ViewLoanSummary(result)
    End Sub
#Region "Get PKEmployee GUID"
    Private Sub getpkemployeeguid(ByVal lastname As String)
        Dim gcon As New Clsappconfiguration
        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "MMS_Get_PKemployee", _
                                     New SqlParameter("@fcLastName", lastname))
        Try
            While (reader.Read)
                Dim guid As String = reader.Item(0).ToString.Trim
                Call ViewloanSummaryGUID(guid)
            End While
            reader.Close()
            gcon.sqlconn.Close()
        Catch ex As Exception

        End Try
    End Sub
#End Region

    Private Sub btnview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnview.Click
        If IsLoanTypeCurrent(getEmpinfo(), GetLoanNo()) = False Then
            FrmLoan_ForwardRequirements.getloanNo() = Me.txtLoanNumber.Text
            FrmLoan_ForwardRequirements.getEmpinfo() = Empinfo
            FrmLoan_ForwardRequirements.getloantype() = Me.lblExpressLoan.Text
            FrmLoan_ForwardRequirements.Show()
        Else
            MessageBox.Show("The member has a current loan with this loan type already. You cannot proceed.", "Loan Processing Validation", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub lstExistingLoans_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstExistingLoans.Click
        Call ViewLoansPerMember()
    End Sub

    Private Sub ViewLoansPerMember()
        Try
            getEmpinfo() = Me.lstExistingLoans.SelectedItems(0).Text
            FrmLoan_ForwardRequirements.getEmpinfo() = Me.lstExistingLoans.SelectedItems(0).Text
            Call ViewLoanSummary(Me.lstExistingLoans.SelectedItems(0).Text)
            Call GetloanNumber(Me.lstExistingLoans.SelectedItems(0).Text)
            Me.Text = "Existing Loans" + " " + "( " + Me.lstExistingLoans.SelectedItems(0).Text.ToUpper + " : " + Me.lstExistingLoans.SelectedItems(0).SubItems(1).Text.ToUpper + " )"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub laonDTto_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles laonDTto.ValueChanged
        Try
            Call FilteredloanSummary(Me.lstExistingLoans.SelectedItems(0).Text, "", "", "", Format(Me.DTLoan.Value, "MM/dd/yyyy"), Format(Me.laonDTto.Value, "MM/dd/yyyy"))
        Catch ex As Exception
            MessageBox.Show("Select a Member before you filter", "Filtering", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

    End Sub

    Private Sub CboLoanStatus_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboLoanStatus.TextChanged
        Try
            Call FilteredloanSummary(Me.lstExistingLoans.SelectedItems(0).Text, Me.CboLoanStatus.Text, "", "", Format(Now, "MM/dd/yyyy"), Format(Now, "MM/dd/yyyy"))
        Catch ex As Exception
            MessageBox.Show("Select a Member before you filter", "Filtering", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub CboLoanType_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboLoanType.TextChanged

        Try
            Call FilteredloanSummary(Me.lstExistingLoans.SelectedItems(0).Text, "", Me.CboLoanType.Text, "", Format(Now, "MM/dd/yyyy"), Format(Now, "MM/dd/yyyy"))
        Catch ex As Exception
            MessageBox.Show("Select a Member before you filter", "Filtering", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub CboLoanNumber_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboLoanNumber.TextChanged

        Try
            'frmLoanReports.GetLoanNo() = Me.CboLoanNumber.Text
            'getvaluePledgeformReports(Me.CboLoanNumber.Text)

            Call FilteredloanSummary(Me.lstExistingLoans.SelectedItems(0).Text, "", "", Me.CboLoanNumber.Text, Format(Now, "MM/dd/yyyy"), Format(Now, "MM/dd/yyyy"))
        Catch ex As Exception
            MessageBox.Show("Select a Member before you filter", "Filtering", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
#Region "Get value for Pledge form reports"
    Private Sub getvaluePledgeformReports(ByVal loanNo As String)

        Dim amtwords As New WPMs
        Dim mycon As New Clsappconfiguration

        Using rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "MSS_LoanTracking_GetLoans_Number", _
                                    New SqlParameter("@loanNo", loanNo))
            Try
                While rd.Read
                    getamt() = Format(rd.Item("Balance"), "#,#00.00")
                End While

                frmLoan_ReportOptions.getAmtWords() = amtwords.gGenerateCurrencyInWords(amt)

            Catch ex As Exception
                MessageBox.Show(ex.Message, "getvaluePledgeformReports")
            End Try

        End Using

    End Sub
#End Region
    Private Sub ListStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListStatus.Click
        Call LoadAllLoanStatus(Me.ListStatus.SelectedItems(0).Text)
    End Sub

    Private Sub ListStatus_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ListStatus.KeyDown
        Call LoadAllLoanStatus(Me.ListStatus.SelectedItems(0).Text)
    End Sub

    Private Sub ListStatus_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ListStatus.KeyUp
        Call LoadAllLoanStatus(Me.ListStatus.SelectedItems(0).Text)
    End Sub
#Region "Get all member loan status"
    Private Sub LoadAllLoanStatus(ByVal status As String)
        GrdLoanSummary.Columns.Clear()
        GrdLoanSummary.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None

        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("MSS_LoanTracking_GetLoans_PerStatus", gcon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        gcon.sqlconn.Open()

        With cmd.Parameters
            .Add("@status", SqlDbType.VarChar, 50).Value = status
        End With

        ad.SelectCommand = cmd
        ad.Fill(ds, "CIMS_m_Member")

        Try
            Me.GrdLoanSummary.DataSource = ds
            Me.GrdLoanSummary.DataMember = "CIMS_m_Member"
            Me.GrdLoanSummary.Columns(30).Visible = False 'fk_Member_Loan
            Me.GrdLoanSummary.Columns(31).Visible = False 'empid

            cmd.ExecuteNonQuery()
            gcon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
#End Region

    Private Sub GrdLoanSummary_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GrdLoanSummary.DoubleClick
        ' LoadPaymentProperties()
        'frmLoan_PaymentHistory.ShowDialog()
    End Sub

    Private Sub GrdLoanSummary_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles GrdLoanSummary.KeyDown
        Call GetLoanDetails()
    End Sub

    Private Sub GrdLoanSummary_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles GrdLoanSummary.KeyUp
        Call GetLoanDetails()
    End Sub

    Private Sub Btnprint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnprint.Click

        If GetLoanNo() <> "" Then
            frmLoan_ReportOptions.GetLoanNo() = Me.GetLoanNo()
            frmLoan_ReportOptions.ShowDialog()
        Else
            MessageBox.Show("Please select a loan first.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

    End Sub

    Private Sub GrdLoanSummary_RowHeaderMouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles GrdLoanSummary.RowHeaderMouseDoubleClick
        Dim fkLoanPayment As String
    End Sub

    Private Sub LoadPaymentProperties()
        With Me.GrdLoanSummary.CurrentRow
            frmLoan_PaymentHistory.GetSetFkLoanPayment() = .Cells(25).Value.ToString
            frmLoan_PaymentHistory.GetSetSupplierName() = .Cells(0).Value.ToString
            frmLoan_PaymentHistory.GetSetLoanNo() = .Cells(3).Value.ToString
            frmLoan_PaymentHistory.GetSetLoanType() = .Cells(4).Value.ToString
            frmLoan_PaymentHistory.GetSetBalance() = .Cells(22).Value.ToString
            frmLoan_PaymentHistory.GetSetTotalPayment() = .Cells(23).Value.ToString
        End With
    End Sub

    Private Sub btnEditAmort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditAmort.Click
        TextBoxEditMode(txtAmortization)
    End Sub

    Private Sub TextBoxEditMode(ByVal textbox As TextBox)
        editMode = True

        GrdLoanSummary.Enabled = False
        lstExistingLoans.Enabled = False

        textbox.BackColor = Color.White
        textbox.ReadOnly = False
        textbox.Focus()
    End Sub
    Private Sub TextBoxViewOnlyMode(ByVal textbox As TextBox)
        editMode = False

        GrdLoanSummary.Enabled = True
        lstExistingLoans.Enabled = True

        textbox.BackColor = Color.Gainsboro
        textbox.ReadOnly = True
    End Sub
    Private Sub ChangeAmortizationValue(ByVal loanNo As Int32, ByVal newAmortAmount As Decimal)
        Try
            ModifiedSqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_t_Loans_AmortChange", _
                        New SqlParameter("@loanNo", loanNo), _
                        New SqlParameter("@newAmortAmount", newAmortAmount))
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ValidateChange()
        If MessageBox.Show("This change will have an impact on your data. Proceed?", _
                      "Change Amortization", MessageBoxButtons.YesNo, _
                              MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

            Dim newAmortNumber As Decimal
            newAmortNumber = IIf(txtAmortization.Text = "", Decimal.Parse("0"), Decimal.Parse(txtAmortization.Text))

            Call ChangeAmortizationValue(Int32.Parse(txtLoanNumber.Text), newAmortNumber)
            Call GetLoanDetails()
            Call TextBoxViewOnlyMode(txtAmortization)
        Else
            TextBoxViewOnlyMode(txtAmortization)
            GetLoanDetails()
        End If
    End Sub
    Private Sub txtAmortization_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAmortization.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            If editMode = True Then
                ValidateChange()
            End If
        Else
            KeypressNumberOnly(txtAmortization, e)
        End If
    End Sub
    Private Sub KeypressNumberOnly(ByVal textbox As TextBox, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        'If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
        'If e.KeyChar = Chr(8) Then e.Handled = False
        'If e.KeyChar = Chr(13) Then textbox.Focus()


        ' If not the characters 0-9, PERIOD, DELETE, or BACKSPACE.
        If Not (e.KeyChar = Chr(48) Or e.KeyChar = Chr(49) Or _
          e.KeyChar = Chr(50) Or e.KeyChar = Chr(51) Or _
          e.KeyChar = Chr(52) Or e.KeyChar = Chr(53) Or _
          e.KeyChar = Chr(54) Or e.KeyChar = Chr(55) Or _
          e.KeyChar = Chr(56) Or e.KeyChar = Chr(57) Or _
          e.KeyChar = Chr(46) Or e.KeyChar = Chr(127) Or _
          e.KeyChar = Chr(8)) Then

            ' Stop the character from being entered into the
            ' control since it is an invalid key.
            e.Handled = True
        End If


    End Sub

    Private Function IsLoanTypeCurrent(ByVal empNo As String, ByVal loanNo As String) As Boolean
        Dim isCurrent As Boolean = False

        Try
            Dim stored As String = "CIMS_t_Loans_CheckIfLoanIsCurrent"
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, stored, _
                    New SqlParameter("@employeeNo", empNo), _
                    New SqlParameter("@loanNo", loanNo))

                isCurrent = rd.HasRows

            End Using
        Catch ex As Exception
            isCurrent = False
        End Try

        Return isCurrent
    End Function
    Private Sub ChangeLoanNo()
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Admin_ChangeLoanNo", _
                    New SqlParameter("@loanID", GetLoanID()), _
                    New SqlParameter("@newLoanNo", txtLoanNumber.Text))
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub EnableChangeLoanNo()
        txtLoanNumber.Focus()
        txtLoanNumber.ReadOnly = False
        txtLoanNumber.BackColor = Color.White
    End Sub
    Private Sub DisableChangeLoanNo()
        txtLoanNumber.ReadOnly = True
        btnChangeLoanNo.Focus()
        txtLoanNumber.BackColor = Color.Gainsboro
    End Sub


    Private Sub lstExistingLoans_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lstExistingLoans.KeyUp
        Call ViewLoansPerMember()
    End Sub

    Private Sub lstExistingLoans_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lstExistingLoans.KeyDown
        Call ViewLoansPerMember()
    End Sub

    Private Sub btnChangeLoanNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChangeLoanNo.Click
        Call EnableChangeLoanNo()
    End Sub

    Private Sub txtStatus_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtStatus.TextChanged
        If txtStatus.Text = "On-Process" Or txtStatus.Text = "Completed" Or txtStatus.Text = "" Then
            btnChangeLoanNo.Enabled = True
        Else
            btnChangeLoanNo.Enabled = False
        End If
    End Sub

    Private Sub txtLoanNumber_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtLoanNumber.KeyPress
        If txtLoanNumber.ReadOnly = False Then
            If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
                If MessageBox.Show("Do you want to change to this loan No?", "Change Loan No", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then

                    Call ChangeLoanNo()
                    'Updates Loan No Selected 
                    GetLoanNo() = txtLoanNumber.Text
                    Call LoadLoansAfterChangeLoanNo()


                    Call DisableChangeLoanNo()
                    MessageBox.Show("The loan number was changed to " & GetLoanNo(), "Change Loan No", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    txtLoanNumber.Text = GetLoanNo()
                    Call DisableChangeLoanNo()
                End If
            End If
        End If

    End Sub

    Private Sub txtLoanNumber_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLoanNumber.Leave
        Call DisableChangeLoanNo()
    End Sub

    Private Sub LoadLoansAfterChangeLoanNo()
        Try
            Call ViewLoansPerMember()
        Catch ex As Exception
            Call LoadAllLoanStatus(Me.ListStatus.SelectedItems(0).Text)
        End Try
    End Sub

    Private Sub btnValidateLoans_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnValidateLoans.Click
        frmAdmin_LoanDelete.Show()
    End Sub

    Private Sub btnChangeDateStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChangeDateStart.Click
        If btnChangeDateStart.Text = "Change Start Date" Then
            editMode = False
            btnChangeDateStart.Text = "Save"
            DateEditMode(dteAmortStart)
        ElseIf btnChangeDateStart.Text = "Save" Then
            DateEditMode(dteAmortStart)
            If editMode = True Then
                ValidateAmortDateStart()
            End If
            btnChangeDateStart.Text = "Change Start Date"
        End If
    End Sub

    Private Sub DateEditMode(ByVal datetimepicker As DateTimePicker)                    '(ByVal textbox As TextBox)
        editMode = True

        GrdLoanSummary.Enabled = False
        lstExistingLoans.Enabled = False

        datetimepicker.BackColor = Color.White
        datetimepicker.Enabled = True
        datetimepicker.Focus()
    End Sub

    Private Sub ValidateAmortDateStart()
        If MessageBox.Show("This change will have an impact on your data. Proceed?", _
                        "Change Amortization Start Date", MessageBoxButtons.YesNo, _
                                MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

            Dim newdate As Date
            newdate = Date.Parse(dteAmortStart.Value)

            Call ChangeAmortizationDate(Int32.Parse(txtLoanNumber.Text), newdate)
            Call GetLoanDetails()
            Call ViewOnlyMode()
        Else
            Call ViewOnlyMode()
            Call GetLoanDetails()
        End If
    End Sub

    Private Sub ValidateDateEnd()
        If MessageBox.Show("This change will have an impact on your data. Proceed?", _
                        "Change Amortization", MessageBoxButtons.YesNo, _
                                MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

            Dim newdate As Date
            newdate = Date.Parse(dteAmortEnd.Value)

            Call ChangeAmortizationDateEnd(Int32.Parse(txtLoanNumber.Text), newdate)
            Call GetLoanDetails()
            Call ViewOnlyMode()
        Else
            Call ViewOnlyMode()
            Call GetLoanDetails()
        End If
    End Sub

    Private Sub ChangeAmortizationDate(ByVal loanNo As Int32, ByVal newAmortDate As Date)
        Try
            ModifiedSqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_t_Loans_AmortDateStartChange", _
                        New SqlParameter("@loanNo", loanNo), _
                        New SqlParameter("@newAmortDateStart", newAmortDate))
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ChangeAmortizationDateEnd(ByVal loanNo As Int32, ByVal newAmortDate As Date)
        Try
            ModifiedSqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_t_Loans_AmortDateEndChange", _
                        New SqlParameter("@loanNo", loanNo), _
                        New SqlParameter("@newAmortDateEnd", newAmortDate))
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ViewOnlyMode()
        editMode = False

        GrdLoanSummary.Enabled = True
        lstExistingLoans.Enabled = True
        dteAmortStart.Enabled = False
        dteAmortEnd.Enabled = False
    End Sub

    Private Sub btnChangeDateEnd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChangeDateEnd.Click
        If btnChangeDateEnd.Text = "Change End Date" Then
            editMode = False
            btnChangeDateEnd.Text = "Save"
            DateEditMode(dteAmortEnd)
        ElseIf btnChangeDateEnd.Text = "Save" Then
            DateEditMode(dteAmortEnd)
            If editMode = True Then
                ValidateDateEnd()
            End If
            btnChangeDateEnd.Text = "Change End Date"
        End If
    End Sub

    'Comaker Details
    'Added by Vincent Nacar
    '6/18/2014

    Private Sub LoadComaker(ByVal loanNo As String)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(gcon.cnstring, "_LoanProcessing_Select_Comakers",
                                       New SqlParameter("@LoanNo", loanNo))
        dgvComaker.DataSource = ds.Tables(0)
    End Sub

    Private Sub btnPrintStatement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintStatement.Click
        'frmReport_LoanDetails.LoadREport(getEmpinfo(), GetLoanNo())
        'frmReport_LoanDetails.WindowState = FormWindowState.Maximized
        'frmReport_LoanDetails.StartPosition = FormStartPosition.CenterScreen
        'frmReport_LoanDetails.ShowDialog()
    End Sub

    Private Sub ViewRequirementToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewRequirementToolStripMenuItem.Click
        If IsLoanTypeCurrent(getEmpinfo(), GetLoanNo()) = False Then
            'CIMS_t_MemberLoans_Reqts per member
            'MSS_Loans_AddEdit_MemberLoanRequirements
            'MSS_Loans_Retrieve_LoanRequirements_PerMember
            frmMasterfile_NewMemberRequirements.getloanNumber() = Me.txtLoanNumber.Text
            frmMasterfile_NewMemberRequirements.getloantype() = Me.lblExpressLoan.Text
            frmMasterfile_NewMemberRequirements.ShowDialog()
        Else
            MessageBox.Show("The member has a current loan with this loan type already. You cannot proceed.", "Loan Processing Validation", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub RequirementsForApprovalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RequirementsForApprovalToolStripMenuItem.Click
        If IsLoanTypeCurrent(getEmpinfo(), GetLoanNo()) = False Then
            FrmLoan_ForwardRequirements.getloanNo() = Me.txtLoanNumber.Text
            FrmLoan_ForwardRequirements.getEmpinfo() = Empinfo
            FrmLoan_ForwardRequirements.getloantype() = Me.lblExpressLoan.Text
            FrmLoan_ForwardRequirements.ShowDialog()
        Else
            MessageBox.Show("The member has a current loan with this loan type already. You cannot proceed.", "Loan Processing Validation", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub DeleteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteToolStripMenuItem.Click
        If MsgBox("Warning, This loan might be active or currently have a transaction this may affect the system. Are you sure you want to delete this loan?", vbYesNo, "Confirmation") = vbYes Then
            deleteLoan(Me.txtLoanNumber.Text)
            Call ViewLoanSummary(getEmpinfo())
            Call _Prep_LessCol()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub deleteLoan(ByVal loanno As String)
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, "_Delete_ApprovalHistory",
                            New SqlParameter("@LoanNo", loanno))
            SqlHelper.ExecuteNonQuery(gcon.cnstring, "_Delete_LoanRqts",
                            New SqlParameter("@LoanNo", loanno))
            SqlHelper.ExecuteNonQuery(gcon.cnstring, "_DELETE_LoanMaster",
                           New SqlParameter("@LoanNo", loanno))
        Catch ex As Exception
            Throw
            frmMsgBox.txtMessage.Text = "Unable to delete this loan. It has transaction."
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub LoanAgreementToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoanAgreementToolStripMenuItem.Click
        frmReport_LoanApplication.LoadREport(txtLoanNumber.Text)
        frmReport_LoanApplication.WindowState = FormWindowState.Maximized
        frmReport_LoanApplication.ShowDialog()
    End Sub

    Private Sub LoanApplicationFormToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoanApplicationFormToolStripMenuItem.Click
        frmReport_LoanApplication.LoadREport2(txtLoanNumber.Text)
        frmReport_LoanApplication.WindowState = FormWindowState.Maximized
        frmReport_LoanApplication.ShowDialog()
    End Sub

    Private Sub txtSearchLastname_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchLastname.TextChanged
        Call Searchmember(Me.txtSearchLastname.Text)
    End Sub

#Region "Added by Vinz 10/2/14"

    Private Sub _Prep_LessCol()
        Try
            dgvDeductions.Columns.Clear()
            dgvDeductions.Rows.Clear()
            dgvDeductions.DataSource = Nothing
            Dim acctRef As New DataGridViewTextBoxColumn
            Dim code As New DataGridViewTextBoxColumn
            Dim accttitle As New DataGridViewTextBoxColumn
            Dim amount As New DataGridViewTextBoxColumn
            With acctRef
                .Name = "acctRef"
                .HeaderText = "Account Ref."
            End With
            With code
                .Name = "code"
                .HeaderText = "Account Code"
            End With
            With accttitle
                .Name = "accttitle"
                .HeaderText = "Account Title"
            End With
            With amount
                .Name = "amount"
                .HeaderText = "Amount"
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With
            With dgvDeductions
                .Columns.Add(acctRef)
                .Columns.Add(code)
                .Columns.Add(accttitle)
                .Columns.Add(amount)
            End With
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "_Prep_LessCol")
        End Try
        _Clr_text()
    End Sub
    Private Sub _Load_Less_Details(ByVal loanno As String)
        Try
            Dim i As Integer = 0
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, "_Generate_Loantype_Charges",
                               New SqlParameter("@LoanNo", loanno))
            While rd.Read
                With dgvDeductions
                    .Rows.Add()
                    .Item("acctRef", i).Value = rd("Acct. Ref").ToString
                    .Item("code", i).Value = rd("Code").ToString
                    .Item("accttitle", i).Value = rd("Description").ToString
                    .Item("amount", i).Value = rd("Credit").ToString
                    i += 1
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "_Load_Less_Details")
        End Try
    End Sub
    Private Sub dgvDeductions_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDeductions.CellValueChanged
        _Format_Cols()
    End Sub
    Private Sub _Format_Cols()
        Dim netpro As Decimal
        For i As Integer = 0 To dgvDeductions.RowCount - 1
            With dgvDeductions
                netpro = netpro + .Item("amount", i).Value
                .Item("amount", i).Value = Format(CDec(.Item("amount", i).Value), "##,##0.00")
            End With
        Next
        txtDNetProceeds.Text = Format(CDec(CDec(txtDPrincipal.Text) - netpro), "##,##0.00")
        txtDPrincipal.Text = Format(CDec(txtDPrincipal.Text), "##,##0.00")
    End Sub
    Private Sub _Clr_text()
        txtdDate.Text = "-/-/-"
        txtDInt.Text = "0.00"
        txtDLoanNo.Text = ""
        txtDNetProceeds.Text = "0.00"
        txtDPrincipal.Text = "0.00"
        txtDterms.Text = "0.00"
    End Sub

#End Region

    '#Region "PAyment History"

    '    Private Sub _GetPAyHistory(ByVal loanno As String)
    '        Try
    '            Dim ds As DataSet
    '            ds = SqlHelper.ExecuteDataset(gcon.cnstring, "_LoanProcessing_GetPaymentHistory",
    '                                           New SqlParameter("@LoanNo", loanno))
    '            dgvPaymentHistory.DataSource = ds.Tables(0)
    '            dgvPaymentHistory.Columns(4).DefaultCellStyle.Format = "##,##0.00"
    '            dgvPaymentHistory.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
    '        Catch ex As Exception
    '            Throw
    '        End Try
    '    End Sub
    '    Private Sub _GetPAyTotals()
    '        Try
    '            Dim xtot As Decimal = 0
    '            For i As Integer = 0 To dgvPaymentHistory.RowCount - 1
    '                xtot = xtot + CDec(dgvPaymentHistory.Rows(i).Cells(4).Value.ToString)
    '            Next
    '            totAmountPaid.Text = Format(xtot, "##,##0.00")
    '            txtPayBalance.Text = Format(CDec(txtDPrincipal.Text) - CDec(totAmountPaid.Text), "##,##0.00")
    '        Catch ex As Exception
    '            Throw
    '        End Try
    '    End Sub
    '#End Region

End Class