﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data


Public Class frmAdmin_LoanDelete
    Private gcon As New Clsappconfiguration
    Private pkLoan As String
    Private loanno As Integer
    Private empno As String

    Public Property GetpkLoan() As String
        Get
            Return pkLoan
        End Get
        Set(ByVal value As String)
            pkLoan = value
        End Set
    End Property

    Public Property GetLoanNo() As Integer
        Get
            Return loanno
        End Get
        Set(ByVal value As Integer)
            loanno = value
        End Set
    End Property
    Public Property GetEmpNo() As String
        Get
            Return empno
        End Get
        Set(ByVal value As String)
            empno = value
        End Set
    End Property

    Private Sub DeleteLoan(ByVal pk_Members_Loan As String, ByVal admin As String, ByVal empno As String, ByVal reason As String, ByVal compName As String)   'ByVal date_delete As Date, 
        Try
            ModifiedSqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Admin_DeleteLoans_V2", _
                        New SqlParameter("@pk_Members_Loan", pk_Members_Loan), _
                        New SqlParameter("@admin", admin), _
                        New SqlParameter("@empno", empno), _
                        New SqlParameter("@reason", reason), _
                        New SqlParameter("@compName", compName))
        Catch ex As Exception
            '   MessageBox.Show("Loan number does not exist", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            MessageBox.Show(ex.Message.ToString)
            'txtLoanNo.SelectAll()
            'txtLoanNo.Focus()

        End Try

    End Sub

#Region "Get Loan Number based on EmpID"

    Public Sub ViewLoan()

        Try
            Dim gcon As New Clsappconfiguration
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Admin_GetLoansForDeletion")
            With grdLoanDisplay
                .DataSource = ds.Tables(0)
                .Columns(6).Visible = False

            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
#End Region

    Private Sub frmAdmin_LoanDelete_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Call ViewLoan()
        btnDelete.Enabled = False
    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()

    End Sub

    Private Sub grdLoanDisplay_Click(sender As System.Object, e As System.EventArgs) Handles grdLoanDisplay.Click

        GetpkLoan() = grdLoanDisplay.CurrentRow.Cells("pk_Members_Loan").Value.ToString()
        GetEmpNo() = grdLoanDisplay.CurrentRow.Cells("Employee No.").Value.ToString()

        txtReason.Text = ""
        If txtReason.Text = "" Then
            btnDelete.Enabled = False
        End If


    End Sub

    Private Sub txtReason_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtReason.KeyPress
        btnDelete.Enabled = True
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If MessageBox.Show("This is irreversible, are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then

            Try

                Call DeleteLoan(GetpkLoan(), frmMain.getusername(), GetEmpNo(), txtReason.Text, frmMain.StComputername.Text)
                Call ViewLoan()

            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click

    End Sub
End Class