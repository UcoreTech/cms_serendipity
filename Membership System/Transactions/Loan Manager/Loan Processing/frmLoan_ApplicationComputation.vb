Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmLoan_ApplicationComputation
#Region "Public Variables"
    Public empinfo As String
    Public loanClass As String
    Public loantype As String
    Public loanterm As Integer
    Public loadAppdate As String
    Public loanPrincipal As String
    Public loanPreterminated As String
    Public loadincludePen As Integer
    Public loanRestructuredno As String
    Public LoanRecievable As Decimal
    Public ServicefeePercent As Integer
    Public CapitalSharePercent As Integer
    Public balance As Decimal
    Public loanInterest As Integer
    Public mode As String

    Public xmode As String
#End Region
#Region "Var"
    Public RemainingNetpay As Decimal
    Public MaxloanCapacity As Decimal
    Public CapitalContribution As Decimal
    Public PolicyRate As Decimal
    Public MaxloanCapital As Decimal
    Public PaymentMode As String
    Public TotalCoopLoans As Decimal
    Public pretermbal As Decimal
    Public totalintrefundamt As Decimal
    Public netbalRestructureLoan As Decimal
    Public balpreterminatedLoan As Decimal
    Public totalpreterbalamt As Decimal
#End Region
#Region "Property for Var"
    Public Property getRemainingNetpay() As Decimal
        Get
            Return RemainingNetpay
        End Get
        Set(ByVal value As Decimal)
            RemainingNetpay = value
        End Set
    End Property
    Public Property getMaxloanCapacity() As Decimal
        Get
            Return MaxloanCapacity
        End Get
        Set(ByVal value As Decimal)
            MaxloanCapacity = value
        End Set
    End Property
    Public Property getCapitalContribution() As Decimal
        Get
            Return CapitalContribution
        End Get
        Set(ByVal value As Decimal)
            CapitalContribution = value
        End Set
    End Property
    Public Property getNetBalanceofRestrucLoan() As Decimal
        Get
            Return netbalRestructureLoan
        End Get
        Set(ByVal value As Decimal)
            netbalRestructureLoan = value
        End Set
    End Property
    Public Property getBalanceofPreterminatedLoan() As Decimal
        Get
            Return balpreterminatedLoan
        End Get
        Set(ByVal value As Decimal)
            balpreterminatedLoan = value
        End Set
    End Property


    Public Property getPolicyRate() As Decimal
        Get
            Return PolicyRate
        End Get
        Set(ByVal value As Decimal)
            PolicyRate = value
        End Set
    End Property
    Public Property getMaxloanCapital() As Decimal
        Get
            Return MaxloanCapital
        End Get
        Set(ByVal value As Decimal)
            MaxloanCapital = value
        End Set
    End Property
    Public Property getPaymentMode() As String
        Get
            Return PaymentMode
        End Get
        Set(ByVal value As String)
            PaymentMode = value
        End Set
    End Property
    Public Property getMode() As String
        Get
            Return mode
        End Get
        Set(ByVal value As String)
            mode = value
        End Set
    End Property
    Public Property getTotalCoopLoans() As Decimal
        Get
            Return TotalCoopLoans
        End Get
        Set(ByVal value As Decimal)
            TotalCoopLoans = value
        End Set
    End Property
    Public Property gettotalintrefundamt() As Decimal
        Get
            Return totalintrefundamt
        End Get
        Set(ByVal value As Decimal)
            totalintrefundamt = value
        End Set
    End Property

    Private amortPerPayroll As Decimal
    Public Property GetAmortPerPayroll() As Decimal
        Get
            Return amortPerPayroll
        End Get
        Set(ByVal value As Decimal)
            amortPerPayroll = value
        End Set
    End Property

#End Region
#Region "Get Values of each Variables"
    Public Property getEmpinfo() As String
        Get
            Return empinfo
        End Get
        Set(ByVal value As String)
            empinfo = value
        End Set
    End Property
    Public Property getloanClass() As String
        Get
            Return loanClass
        End Get
        Set(ByVal value As String)
            loanClass = value
        End Set
    End Property
    Public Property getloantype() As String
        Get
            Return loantype
        End Get
        Set(ByVal value As String)
            loantype = value
        End Set
    End Property
    Public Property getloanterm() As Integer
        Get
            Return loanterm
        End Get
        Set(ByVal value As Integer)
            loanterm = value
        End Set
    End Property
    Public Property getloadAppdate() As String
        Get
            Return loadAppdate
        End Get
        Set(ByVal value As String)
            loadAppdate = value
        End Set
    End Property
    Public Property getloanPrincipal() As String
        Get
            Return loanPrincipal
        End Get
        Set(ByVal value As String)
            loanPrincipal = value
        End Set
    End Property
    Public Property getloanPreterminated() As String
        Get
            Return loanPreterminated
        End Get
        Set(ByVal value As String)
            loanPreterminated = value
        End Set
    End Property
    Public Property getloadincludePen() As Integer
        Get
            Return loadincludePen
        End Get
        Set(ByVal value As Integer)
            loadincludePen = value
        End Set
    End Property
    Public Property getloanRestructuredno() As String
        Get
            Return loanRestructuredno
        End Get
        Set(ByVal value As String)
            loanRestructuredno = value
        End Set
    End Property
    Public Property getloanRecievable() As Decimal
        Get
            Return LoanRecievable
        End Get
        Set(ByVal value As Decimal)
            LoanRecievable = value
        End Set
    End Property
    Public Property getServicefeePercent() As Integer
        Get
            Return ServicefeePercent
        End Get
        Set(ByVal value As Integer)
            ServicefeePercent = value
        End Set
    End Property
    Public Property getCapitalSharePercent() As Integer
        Get
            Return CapitalSharePercent
        End Get
        Set(ByVal value As Integer)
            CapitalSharePercent = value
        End Set
    End Property
    Public Property getbalance() As Decimal
        Get
            Return balance
        End Get
        Set(ByVal value As Decimal)
            balance = value
        End Set
    End Property
    Public Property getloanInterest() As Integer
        Get
            Return loanInterest
        End Get
        Set(ByVal value As Integer)
            loanInterest = value
        End Set
    End Property

    Public Property getpretermbal() As Decimal
        Get
            Return pretermbal
        End Get
        Set(ByVal value As Decimal)
            pretermbal = value
        End Set
    End Property

    Public Property gettotalpreterbalamt() As Decimal
        Get
            Return totalpreterbalamt
        End Get
        Set(ByVal value As Decimal)
            totalpreterbalamt = value
        End Set

    End Property

#End Region
    Private gcon As New Clsappconfiguration()
    Private originalNetproceeds As Decimal
    Private Function NumbersOnly(ByVal pstrChar As Char, ByVal oTextBox As TextBox) As Boolean
        'validate the entry for a textbox limiti // ng it to only numeric values and the dec // imal point 
        If (Convert.ToString(pstrChar) = "." And InStr(oTextBox.Text, ".")) Then Return True
        'accept only one instance of the decimal point 
        If Convert.ToString(pstrChar) <> "." And pstrChar <> vbBack Then Return IIf(IsNumeric(pstrChar), False, True)
        'check if numeric is returned End If Return False 'for backspace 
    End Function

    Private Sub _refresh_Cols()
        With dgvList
            .Refresh()
            .DataSource = Nothing
            .Rows.Clear()
            .Columns.Clear()
        End With
        dgvList.Controls.Clear()
    End Sub

    Private Sub frmCoop_LoanAppComputation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _refresh_Cols() 'test
        txtTotUnearned.Text = "0.00"
        chkStandard.Checked = True ' default setting
        ConfigureCntrls_CarryFrward()

        Call ViewApplicationLoanComputation(empinfo, loanClass, loantype, loanterm, loadAppdate, CDec(loanPrincipal), loanPreterminated, loadincludePen, loanRestructuredno)

        'Added 8/6/2014 Vince
        lblInterest.Text = GetInterest().ToString

        Call GetEmployeeInfo(empinfo)

        txtAmorDateEnd.Value = Today
        txtAmorDateStart.Value = frmLoan_ApplicationMain.dtpLoanApplicationDate.Value 'get first date
        Call GenerateAmortDate()
        Call GenerateButton_forDeduct()
        'test
        'If frmLoan_ApplicationMain.rdCustom.Checked = True Then
        '    xmode = "Custom"
        '    _Gen_AmortCol()
        '    '_IsFirstLoad()
        '    cust_principal = CDec(lblPrincipal.Text)
        '    cust_term = CInt(frmLoan_ApplicationMain.txtnoofpayment.Text)
        '    _IntMethod(CInt(frmLoan_ApplicationMain.txtnoofpayment.Text))
        'Else
        If frmLoan_ApplicationMain.xIntMode = "Custom" Then 'test
            xmode = "Custom"
            _Exec_Amort_Formula(frmLoan_ApplicationMain.txtPrincipalAmount.Text, CDec(txtAnnulInterest.Text), lblTerms.Text, txtAmorDateStart.Text, xmode)
            GetAmortizationTotals()
        Else
            Call ExecAmortizationSchedule()
        End If
        _NormalizeVal_GotowithEgg() 'test
        '_GetAmortizationCols() '3/9/15

        txtrestrucdiff.Text = "0.00"
        txtrestrucbalanceafterchange.Text = "0.00"
        Calculate_NetProceeds()
        lblloantype.Text = _GetLoanType()
        Call _Load_Deductions(CDec(txtPrincipaldetails.Text), lblloantype.Text, empinfo)

        Call _GenerateColor_forDeductedInterest()
        dgvList.Columns(5).HeaderText = "Other Payment"
        'Call _LoadAdvanceInterest(frmLoan_ApplicationMain.txtLoanNo.Text)
    End Sub

    Private Sub _IsFirstLoad()
        Try
            With dgvList
                .Rows.Add()
                .Item("payno", cIndex).Value = "0"
                .Item("dt", cIndex).Value = txtAmorDateStart.Value.ToString("MMMM dd yyyy")
                .Item("prin", cIndex).Value = "0.00"
                .Item("inter", cIndex).Value = "0.00"
                .Item("serv", cIndex).Value = "0.00"
                .Item("amort", cIndex).Value = "0.00"
                .Item("balnc", cIndex).Value = lblPrincipal.Text
                cIndex += 1
            End With
        Catch ex As Exception
        End Try
    End Sub

    Private Function _GetLoanType() As String
        Try
            Return frmLoan_ApplicationMain.cboLoanType.Text
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Sub FirstSave()
        If isInEditMode() = True Then
            'code here
            Try
                Call CreditComAppInsert2(Me.txtEmpNo.Text, GetLoanStatus(), Me.lblType.Text, Me.lblApplicationDate.Text, frmLoan_ApplicationMain.txtLoanNo.Text, Me.lblClassification.Text, Me.lblPrincipal.Text, loanInterest, Me.lblInterest.Text, _
                               LoanRecievable, ServicefeePercent, Me.txtservicefeedetails.Text, CapitalSharePercent, 0.0, Me.txtMonthlyAmor.Text, Me.txtAmorDateStart.Text, Me.txtAmorDateEnd.Text, _
                               12, Me.txtTotalPenalties.Text, Me.txtTotalIntRefunds.Text, Me.txtNetProceeds.Text, Me.lblTerms.Text, lblPrincipal.Text, 0.0, loanPreterminated, frmLoan_ApplicationMain.salary01, 0.0, _
                               0.0, 0.0, 0.0, 0.0, 0.0, MaxloanCapacity, CapitalContribution, PolicyRate, 0.0, _
                               PaymentMode, 0.0, txtIntRefundRestruc.Text, txtMissedPayments.Text)
            Catch ex As Exception


            End Try

            Exit Sub
        End If

        Try
            If Me.chkCheck.Checked = True Or Me.chkATM.Checked = True Then
                Call CreditComAppInsert2(Me.txtEmpNo.Text, "On-Process", Me.lblType.Text, Me.lblApplicationDate.Text, frmLoan_ApplicationMain.txtLoanNo.Text, Me.lblClassification.Text, Me.lblPrincipal.Text, loanInterest, Me.lblInterest.Text, _
                                                   LoanRecievable, ServicefeePercent, Me.txtservicefeedetails.Text, CapitalSharePercent, 0.0, Me.txtMonthlyAmor.Text, Me.txtAmorDateStart.Text, Me.txtAmorDateEnd.Text, _
                                                   12, Me.txtTotalPenalties.Text, Me.txtTotalIntRefunds.Text, Me.txtNetProceeds.Text, Me.lblTerms.Text, lblPrincipal.Text, 0.0, loanPreterminated, frmLoan_ApplicationMain.salary01, 0.0, _
                                                   0.0, 0.0, 0.0, 0.0, 0.0, MaxloanCapacity, CapitalContribution, PolicyRate, 0.0, _
                                                   PaymentMode, 0.0, txtIntRefundRestruc.Text, txtMissedPayments.Text)

                Call SaveAmortSchedule()
                Call SaveCoMakers()
                Call UpdateDocNumber()
                frmLoan_ApplicationMain.inEditMode = True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub

    Private Sub GenerateButton_forDeduct()
        dgvList.Columns.Clear()
        Dim btndeduct As New DataGridViewCheckBoxColumn
        With btndeduct
            .Name = "btndeduct"
            .HeaderText = "Deduct"
        End With
        dgvList.Columns.Add(btndeduct)
    End Sub

    Private Sub Calculate_NetProceeds()
        txtTotalInterestINcome.Text = txtTotInterest.Text
        Dim principal As Decimal = CDec(txtPrincipaldetails.Text)
        Dim serviceFee As Decimal = CDec(txtservicefeedetails.Text)
        Dim netPreterminated As Decimal = CDec(txtNetPreterminatedloan.Text)
        Dim penalty As Decimal = CDec(txtTotalPenalties.Text)
        Dim missedPayment As Decimal = CDec(txtMissedPayments.Text)
        Dim interestincome As Decimal = CDec(txtTotalInterestINcome.Text)
        Dim netproceeds As Decimal = principal - (interestincome + serviceFee + netPreterminated + penalty + missedPayment)
        txtNetProceeds.Text = String.Format("{0:n2}", netproceeds)
    End Sub

    Private Function payMode() As String
        Try
            Return frmLoan_ApplicationMain.cboModeofPayment.Text
        Catch ex As Exception
            Return "0"
        End Try
    End Function

    Public Sub GetAmortizationTotals() 'updated 12-11-14
        Try
            ' align number grid textboxes
            With dgvList
                .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With

            Dim totprincipal As Decimal
            Dim totinterest As Decimal
            Dim totservicefee As Decimal
            Dim totamortization As Decimal

            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList
                    totprincipal = totprincipal + CDec(.Rows(i).Cells(3).Value.ToString)
                    totinterest = totinterest + CDec(.Rows(i).Cells(4).Value.ToString)
                    totservicefee = totservicefee + CDec(.Rows(i).Cells(5).Value.ToString)
                    totamortization = totamortization + CDec(.Rows(i).Cells(6).Value.ToString)
                End With
            Next

            txtTotPrincipal.Text = Format(totprincipal, "##,##0.00")
            txtTotInterest.Text = Format(totinterest, "##,##0.00")
            txtTotServiceFee.Text = Format(totservicefee, "##,##0.00")
            txtTotAmortization.Text = Format(totamortization, "##,##0.00")
        Catch ex As Exception
            frmMsgBox.Text = "Error - Calculating Amortization"
            frmMsgBox.txtMessage.Text = ex.Message + "  - Select 'Mode of Payment' and 'Interest Method' then click Compute."
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub GenerateAmortDate()
        'Generate Date Granted,First Payment , Maturity
        'Added by: Vincent Nacar 8/5/2014
        '****************************************************************************
        Dim mode As String = frmLoan_ApplicationMain.cboModeofPayment.Text
        frmLoan_ApplicationMain.dtpLoanApplicationDate.Value = txtAmorDateStart.Value
        Dim t As Integer = frmLoan_ApplicationMain.txtnoofpayment.Text
        Dim fpay As Date = txtAmorDateStart.Value
        Dim weekctr As Integer = 7
        Dim weekdays As Date
        'updated 8/15/14
        Select Case mode
            Case "DAILY"
                dtFirstPayment.Value = DateAdd(DateInterval.Day, 1, txtAmorDateStart.Value)
                txtAmorDateEnd.Value = fpay.AddDays(t)
            Case "WEEKLY"
                dtFirstPayment.Value = DateAdd(DateInterval.Day, 7, txtAmorDateStart.Value)
                For i As Integer = 0 To t - 1
                    txtAmorDateEnd.Value = DateAdd(DateInterval.Day, 7, txtAmorDateEnd.Value)
                Next
            Case "SEMI-MONTHLY"
                dtFirstPayment.Value = DateAdd(DateInterval.Day, 15, txtAmorDateStart.Value)
                For i As Integer = 0 To t - 1
                    txtAmorDateEnd.Value = DateAdd(DateInterval.Day, 15, txtAmorDateEnd.Value)
                Next
            Case "MONTHLY"
                dtFirstPayment.Value = DateAdd(DateInterval.Month, 1, txtAmorDateStart.Value)
                txtAmorDateEnd.Value = fpay.AddMonths(t)
            Case "QUARTERLY"
                dtFirstPayment.Value = DateAdd(DateInterval.Month, 3, txtAmorDateStart.Value)
                For i As Integer = 0 To t - 1
                    txtAmorDateEnd.Value = DateAdd(DateInterval.Month, 3, txtAmorDateEnd.Value)
                Next
            Case "ANNUALLY"
                dtFirstPayment.Value = DateAdd(DateInterval.Year, 1, txtAmorDateStart.Value)
                txtAmorDateEnd.Value = fpay.AddYears(t)
            Case "LUMP SUM"
                dtFirstPayment.Value = DateAdd(DateInterval.Year, 1, txtAmorDateStart.Value)
                txtAmorDateEnd.Value = dtFirstPayment.Value
        End Select
        '****************************************************************************
    End Sub

    Private Function InterestType() 'added 8/7/2014 Vince
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(gcon.cnstring, "_GetInterestType_ByLoanType",
                                         New SqlParameter("@LoanType", frmLoan_ApplicationMain.cboLoanType.Text))
            While rd.Read
                Return rd(0)
            End While
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Dim xConEditMode As Boolean = False
    Private Function isInEditMode() 'added 8/6/2014 Vince
        Try
            Return frmLoan_ApplicationMain.inEditMode
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function GetInterest()
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(gcon.cnstring, "_SelectInterest_ByLoanType",
                                         New SqlParameter("@LoanType", frmLoan_ApplicationMain.cboLoanType.Text))
            While rd.Read
                If rd(1) = "PER YEAR" Then
                    txtEffectiveRate.Text = Calculate_EffectiveRate(rd(0))
                    txtAnnulInterest.Text = rd(0)
                    Return rd(0) / 12
                Else
                    txtEffectiveRate.Text = Calculate_EffectiveRate(rd(0) * 12)
                    txtAnnulInterest.Text = rd(0) * 12
                    Return rd(0)
                End If
            End While
        Catch ex As Exception
            Return 0
        End Try
    End Function
    'Module: Calculate Effective Rate
    'Created by: Vincent Nacar
    'Date : 8/5/2014
    Private Function Calculate_EffectiveRate(ByVal interest As String)
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Calculate_EffectiveRate",
                                          New SqlParameter("@Interest", interest))
            While rd.Read
                Return rd(0) * 100
            End While
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Public Sub ConfigureCntrls_CarryFrward()
        If frmLoan_ApplicationMain.rdCarryForward.Checked = True Then
            cmdSubmit.Enabled = False
            btnPost.Enabled = True
        Else
            cmdSubmit.Enabled = True
            btnPost.Enabled = False
        End If
    End Sub


#Region "update manual adjustment of int. refund of preterminated loans"
    Public Sub UpdateManualAdjPreterminatedLoan(ByVal preterminatedLoan As String, ByVal intrefundamt As Decimal, ByVal pretermbal As String)

        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "MSS_Loans_UpdateInterestRefundsAdj", _
                                        New SqlParameter("@preterminatedLoans", preterminatedLoan), _
                                         New SqlParameter("@intrefundamt", intrefundamt), _
                                        New SqlParameter("@preterm_bal", pretermbal))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Update interest refund")
        Finally
            mycon.sqlconn.Close()
        End Try

    End Sub
#End Region

   

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub
#Region "View Employee info by ID and lastname only"
    Private Sub GetEmployeeInfo(ByVal EmpInfos As String)
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "MSS_get_computeloanEmpinfo", _
                                     New SqlParameter("@EmployeeNo", EmpInfos))
        Try
            While (rd.Read)
                Me.txtEmpNo.Text = rd.Item(1)
                Me.TxtEmpName.Text = rd.Item(0)
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
#End Region
#Region "View Application Loan Computation"
    Private Sub ViewApplicationLoanComputation(ByVal EmpInfo As String, ByVal Classification As String, ByVal Type As String, ByVal Terms As Integer, _
                                              ByVal Applicationdate As String, ByVal Principal As Decimal, ByVal PreterminatedLoans As String, ByVal IncPernalties As Integer, _
                                              ByVal restructuredloans As String)

        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "MSS_Loans_ComputeLoanApplication_Testing", _
                                     New SqlParameter("@employeeNo", EmpInfo), _
                                     New SqlParameter("@loanClassification", Classification), _
                                     New SqlParameter("@loanType", Type), _
                                     New SqlParameter("@terms", Terms), _
                                     New SqlParameter("@loanApplicationDate", Applicationdate), _
                                     New SqlParameter("@principal", Principal), _
                                     New SqlParameter("@preterminatedLoans", PreterminatedLoans), _
                                     New SqlParameter("@includePenalties", IncPernalties), _
                                     New SqlParameter("@restructuredLoanNo", restructuredloans), _
                                     New SqlParameter("@ctype", frmLoan_ApplicationMain.LoanCategory), _
                                     New SqlParameter("@mode", IIf(Me.getMode = "", DBNull.Value, Me.getMode)))
        Try
            While rd.Read
                lblApplicationDate.Text = rd("Application Date")
                lblPrincipal.Text = Format(CDec(rd("Principal")), "##,##0.00")
                lblClassification.Text = rd("Loan Classification")
                lblType.Text = rd("Loan Type")
                lblTerms.Text = rd("Terms")
                txtPrincipaldetails.Text = rd("Principal")
                lblServiceFee.Text = Format(CDec(rd("Service Fee")), "##,##0.00")
                txtservicefeedetails.Text = rd("Service Fee")
                txtTotalIntRefunds.Text = rd("Interest Refunds")
                txtNetPreterminatedloan.Text = rd("Net Preterminated Loans")
                txtTotalPenalties.Text = rd("Total Unpaid Penalties")
                originalNetproceeds = rd("Net Proceeds")
                getServicefeePercent() = rd("Service Fee Percent")
                getbalance() = rd("Balance")
                txtIntRefundRestruc.Text = rd.Item("Interest Refund Amount of Restructure")
            End While
            rd.Close()
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error in <Loan Application Computation> Please Report this bug immediately."
            frmMsgBox.ShowDialog()
        End Try
    End Sub
#End Region

#Region "Credit Committee Approval "
    Private Sub CreditComAppInsert(ByVal Empinfo As String, ByVal loanStatus As String, ByVal loantype As String, ByVal appDate As String, _
                                   ByVal loanNo As String, ByVal classification As String, ByVal principal As Decimal, ByVal interestPercent As Integer, _
                                   ByVal InterestAmount As Decimal, ByVal recievable As Decimal, ByVal ServicfeePercent As Decimal, ByVal ServicefeeAmt As Decimal, _
                                   ByVal CapitalsharePerct As Integer, ByVal CapitalshareAmt As Decimal, ByVal Amortization As Decimal, ByVal amorstartdate As String, _
                                   ByVal Amorenddate As String, ByVal PenaltyPercent As Integer, ByVal PenaltyAmt As Decimal, ByVal InterestRefund As Decimal, _
                                   ByVal NetProceeds As Decimal, ByVal TermsMonths As Decimal, ByVal balance As Decimal, ByVal AmountPaid As Decimal, ByVal Preterminate As String, _
                                   ByVal SalaryRate As Decimal, ByVal sss As Decimal, ByVal pagibig As Decimal, ByVal philhealth As Decimal, ByVal comploan As Decimal, _
                                   ByVal others As Decimal, ByVal remainingNetPay As Decimal, ByVal maxLoan_capacity As Decimal, ByVal capitalContribution As Decimal, _
                                   ByVal policyRate As Decimal, ByVal maxLoan_Capital As Decimal, ByVal fcPaymentMode As String, ByVal totalCoopLoans As Decimal, ByVal intrefund_restruc As Decimal, _
                                   Optional ByVal missedPayments As Decimal = 0)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            '##################### Update Existing Loans #########################################
            'Added by: Vincent Nacar
            'Date : 7/25/2014
            If frmLoan_ApplicationMain.rdCarryForward.Checked = True Or isInEditMode() = True Then
                SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "_Update_ExistingLoans", _
                          New SqlParameter("@employeeNo", Empinfo), _
                          New SqlParameter("@status", loanStatus), _
                          New SqlParameter("@loanType", loantype), _
                          New SqlParameter("@applicationDate", appDate), _
                          New SqlParameter("@loanNo", loanNo), _
                          New SqlParameter("@classification", classification), _
                          New SqlParameter("@principal", principal), _
                          New SqlParameter("@interestPercent", interestPercent), _
                          New SqlParameter("@interestAmount", InterestAmount), _
                          New SqlParameter("@fdReceivable", recievable), _
                          New SqlParameter("@serviceFeePercent", ServicfeePercent), _
                          New SqlParameter("@serviceFeeAmount", ServicefeeAmt), _
                          New SqlParameter("@capitalSharePercent", CapitalsharePerct), _
                          New SqlParameter("@capitalShareAmount", CapitalshareAmt), _
                          New SqlParameter("@Amortization", Amortization), _
                          New SqlParameter("@AmortStart", txtAmorDateStart.Value), _
                          New SqlParameter("@AmortEnd", txtAmorDateEnd.Value), _
                          New SqlParameter("@penaltyPercent", PenaltyPercent), _
                          New SqlParameter("@penaltyAmount", PenaltyAmt), _
                          New SqlParameter("@InterestRefund", InterestRefund), _
                          New SqlParameter("@netProceeds", NetProceeds), _
                          New SqlParameter("@termsInMonths", TermsMonths), _
                          New SqlParameter("@balance", balance), _
                          New SqlParameter("@amountPaid", AmountPaid), _
                          New SqlParameter("@preterminatedLoans", Preterminate), _
                          New SqlParameter("@salary", SalaryRate), _
                          New SqlParameter("@sss", sss), _
                          New SqlParameter("@pagibig", pagibig), _
                          New SqlParameter("@philhealth", philhealth), _
                          New SqlParameter("@companyLoan", comploan), _
                          New SqlParameter("@others", others), _
                          New SqlParameter("@remainingNetPay", remainingNetPay), _
                          New SqlParameter("@maxLoan_capacity", maxLoan_capacity), _
                          New SqlParameter("@capitalContribution", capitalContribution), _
                          New SqlParameter("@policyRate", policyRate), _
                          New SqlParameter("@maxLoan_Capital", maxLoan_Capital), _
                          New SqlParameter("@fcPaymentMode", fcPaymentMode), _
                          New SqlParameter("@totalCoopLoans", totalCoopLoans), _
                          New SqlParameter("@amortPerPayroll", GetAmortPerPayroll()), _
                          New SqlParameter("@missedPayments", missedPayments), _
                          New SqlParameter("@intrefund_restruc", intrefund_restruc), _
                          New SqlParameter("@Company", frmLoan_ApplicationMain.cboProject.Text))

                If isInEditMode() = True Then

                    DeleteAmortization_ThenUpdate(loanNo)
                    Call SaveAmortSchedule()
                    Call SaveCoMakers()
                    Call UpdateDocNumber()
                    Call ModeOfPayment_Saved()

                    Dim t As DialogResult
                    t = MessageBox.Show("Your Transaction Successfully Saved!", "Successfull", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    If t = Windows.Forms.DialogResult.OK Then
                        Me.Close()
                        Exit Sub
                    End If


                Else
                    Call SaveAmortSchedule()
                    Call SaveCoMakers()
                    Call UpdateDocNumber()
                    Call ModeOfPayment_Saved()

                    Dim y As DialogResult
                    y = MessageBox.Show("Your Transaction Successfully Posted", "Post", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    If y = Windows.Forms.DialogResult.OK Then
                        Me.Close()
                        Exit Sub
                    End If
                End If
            End If
            '#####################################################################################

            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "MSS_Loans_AddMemberLoans_v2", _
                                      New SqlParameter("@employeeNo", Empinfo), _
                                      New SqlParameter("@status", loanStatus), _
                                      New SqlParameter("@loanType", loantype), _
                                      New SqlParameter("@applicationDate", appDate), _
                                      New SqlParameter("@loanNo", loanNo), _
                                      New SqlParameter("@classification", classification), _
                                      New SqlParameter("@principal", principal), _
                                      New SqlParameter("@interestPercent", interestPercent), _
                                      New SqlParameter("@interestAmount", InterestAmount), _
                                      New SqlParameter("@fdReceivable", recievable), _
                                      New SqlParameter("@serviceFeePercent", ServicfeePercent), _
                                      New SqlParameter("@serviceFeeAmount", ServicefeeAmt), _
                                      New SqlParameter("@capitalSharePercent", CapitalsharePerct), _
                                      New SqlParameter("@capitalShareAmount", CapitalshareAmt), _
                                      New SqlParameter("@Amortization", Amortization), _
                                      New SqlParameter("@AmortStart", amorstartdate), _
                                      New SqlParameter("@AmortEnd", Amorenddate), _
                                      New SqlParameter("@penaltyPercent", PenaltyPercent), _
                                      New SqlParameter("@penaltyAmount", PenaltyAmt), _
                                      New SqlParameter("@InterestRefund", InterestRefund), _
                                      New SqlParameter("@netProceeds", NetProceeds), _
                                      New SqlParameter("@termsInMonths", TermsMonths), _
                                      New SqlParameter("@balance", balance), _
                                      New SqlParameter("@amountPaid", AmountPaid), _
                                      New SqlParameter("@preterminatedLoans", Preterminate), _
                                      New SqlParameter("@salary", SalaryRate), _
                                      New SqlParameter("@sss", sss), _
                                      New SqlParameter("@pagibig", pagibig), _
                                      New SqlParameter("@philhealth", philhealth), _
                                      New SqlParameter("@companyLoan", comploan), _
                                      New SqlParameter("@others", others), _
                                      New SqlParameter("@remainingNetPay", remainingNetPay), _
                                      New SqlParameter("@maxLoan_capacity", maxLoan_capacity), _
                                      New SqlParameter("@capitalContribution", capitalContribution), _
                                      New SqlParameter("@policyRate", policyRate), _
                                      New SqlParameter("@maxLoan_Capital", maxLoan_Capital), _
                                      New SqlParameter("@fcPaymentMode", fcPaymentMode), _
                                      New SqlParameter("@totalCoopLoans", totalCoopLoans), _
                                      New SqlParameter("@amortPerPayroll", GetAmortPerPayroll()), _
                                      New SqlParameter("@missedPayments", missedPayments), _
                                      New SqlParameter("@intrefund_restruc", intrefund_restruc), _
                                      New SqlParameter("@Company", frmLoan_ApplicationMain.cboProject.Text))

            trans.Commit()

            Dim x As DialogResult
            x = MessageBox.Show("Your Transaction Successfully Submitted", "Submit", MessageBoxButtons.OK, MessageBoxIcon.Information)
            If x = Windows.Forms.DialogResult.OK Then
                Me.Close()
            End If

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "MSS_Loans_AddMemberLoans")
        Finally
            gcon.sqlconn.Close()
        End Try

        'MSS_Loans_AddMemberLoans

    End Sub


    Private Sub CreditComAppInsert2(ByVal Empinfo As String, ByVal loanStatus As String, ByVal loantype As String, ByVal appDate As String, _
                               ByVal loanNo As String, ByVal classification As String, ByVal principal As Decimal, ByVal interestPercent As Integer, _
                               ByVal InterestAmount As Decimal, ByVal recievable As Decimal, ByVal ServicfeePercent As Decimal, ByVal ServicefeeAmt As Decimal, _
                               ByVal CapitalsharePerct As Integer, ByVal CapitalshareAmt As Decimal, ByVal Amortization As Decimal, ByVal amorstartdate As String, _
                               ByVal Amorenddate As String, ByVal PenaltyPercent As Integer, ByVal PenaltyAmt As Decimal, ByVal InterestRefund As Decimal, _
                               ByVal NetProceeds As Decimal, ByVal TermsMonths As Decimal, ByVal balance As Decimal, ByVal AmountPaid As Decimal, ByVal Preterminate As String, _
                               ByVal SalaryRate As Decimal, ByVal sss As Decimal, ByVal pagibig As Decimal, ByVal philhealth As Decimal, ByVal comploan As Decimal, _
                               ByVal others As Decimal, ByVal remainingNetPay As Decimal, ByVal maxLoan_capacity As Decimal, ByVal capitalContribution As Decimal, _
                               ByVal policyRate As Decimal, ByVal maxLoan_Capital As Decimal, ByVal fcPaymentMode As String, ByVal totalCoopLoans As Decimal, ByVal intrefund_restruc As Decimal, _
                               Optional ByVal missedPayments As Decimal = 0)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            '##################### Update Existing Loans #########################################
            'Added by: Vincent Nacar
            'Date : 7/25/2014
            If frmLoan_ApplicationMain.rdCarryForward.Checked = True Or isInEditMode() = True Then
                SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "_Update_ExistingLoans", _
                          New SqlParameter("@employeeNo", Empinfo), _
                          New SqlParameter("@status", loanStatus), _
                          New SqlParameter("@loanType", loantype), _
                          New SqlParameter("@applicationDate", appDate), _
                          New SqlParameter("@loanNo", loanNo), _
                          New SqlParameter("@classification", classification), _
                          New SqlParameter("@principal", principal), _
                          New SqlParameter("@interestPercent", interestPercent), _
                          New SqlParameter("@interestAmount", InterestAmount), _
                          New SqlParameter("@fdReceivable", recievable), _
                          New SqlParameter("@serviceFeePercent", ServicfeePercent), _
                          New SqlParameter("@serviceFeeAmount", ServicefeeAmt), _
                          New SqlParameter("@capitalSharePercent", CapitalsharePerct), _
                          New SqlParameter("@capitalShareAmount", CapitalshareAmt), _
                          New SqlParameter("@Amortization", Amortization), _
                          New SqlParameter("@AmortStart", txtAmorDateStart.Value), _
                          New SqlParameter("@AmortEnd", txtAmorDateEnd.Value), _
                          New SqlParameter("@penaltyPercent", PenaltyPercent), _
                          New SqlParameter("@penaltyAmount", PenaltyAmt), _
                          New SqlParameter("@InterestRefund", InterestRefund), _
                          New SqlParameter("@netProceeds", NetProceeds), _
                          New SqlParameter("@termsInMonths", TermsMonths), _
                          New SqlParameter("@balance", balance), _
                          New SqlParameter("@amountPaid", AmountPaid), _
                          New SqlParameter("@preterminatedLoans", Preterminate), _
                          New SqlParameter("@salary", SalaryRate), _
                          New SqlParameter("@sss", sss), _
                          New SqlParameter("@pagibig", pagibig), _
                          New SqlParameter("@philhealth", philhealth), _
                          New SqlParameter("@companyLoan", comploan), _
                          New SqlParameter("@others", others), _
                          New SqlParameter("@remainingNetPay", remainingNetPay), _
                          New SqlParameter("@maxLoan_capacity", maxLoan_capacity), _
                          New SqlParameter("@capitalContribution", capitalContribution), _
                          New SqlParameter("@policyRate", policyRate), _
                          New SqlParameter("@maxLoan_Capital", maxLoan_Capital), _
                          New SqlParameter("@fcPaymentMode", fcPaymentMode), _
                          New SqlParameter("@totalCoopLoans", totalCoopLoans), _
                          New SqlParameter("@amortPerPayroll", GetAmortPerPayroll()), _
                          New SqlParameter("@missedPayments", missedPayments), _
                          New SqlParameter("@intrefund_restruc", intrefund_restruc), _
                          New SqlParameter("@Company", frmLoan_ApplicationMain.cboProject.Text))

                If isInEditMode() = True Then

                    DeleteAmortization_ThenUpdate(loanNo)
                    Call SaveAmortSchedule()
                    Call SaveCoMakers()
                    Call UpdateDocNumber()
                    Call ModeOfPayment_Saved()

                    Exit Sub

                Else
                    Call SaveAmortSchedule()
                    Call SaveCoMakers()
                    Call UpdateDocNumber()
                    Call ModeOfPayment_Saved()

                    Exit Sub
                End If
            End If
            '#####################################################################################

            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "MSS_Loans_AddMemberLoans_v2", _
                                      New SqlParameter("@employeeNo", Empinfo), _
                                      New SqlParameter("@status", loanStatus), _
                                      New SqlParameter("@loanType", loantype), _
                                      New SqlParameter("@applicationDate", appDate), _
                                      New SqlParameter("@loanNo", loanNo), _
                                      New SqlParameter("@classification", classification), _
                                      New SqlParameter("@principal", principal), _
                                      New SqlParameter("@interestPercent", interestPercent), _
                                      New SqlParameter("@interestAmount", InterestAmount), _
                                      New SqlParameter("@fdReceivable", recievable), _
                                      New SqlParameter("@serviceFeePercent", ServicfeePercent), _
                                      New SqlParameter("@serviceFeeAmount", ServicefeeAmt), _
                                      New SqlParameter("@capitalSharePercent", CapitalsharePerct), _
                                      New SqlParameter("@capitalShareAmount", CapitalshareAmt), _
                                      New SqlParameter("@Amortization", Amortization), _
                                      New SqlParameter("@AmortStart", amorstartdate), _
                                      New SqlParameter("@AmortEnd", Amorenddate), _
                                      New SqlParameter("@penaltyPercent", PenaltyPercent), _
                                      New SqlParameter("@penaltyAmount", PenaltyAmt), _
                                      New SqlParameter("@InterestRefund", InterestRefund), _
                                      New SqlParameter("@netProceeds", NetProceeds), _
                                      New SqlParameter("@termsInMonths", TermsMonths), _
                                      New SqlParameter("@balance", balance), _
                                      New SqlParameter("@amountPaid", AmountPaid), _
                                      New SqlParameter("@preterminatedLoans", Preterminate), _
                                      New SqlParameter("@salary", SalaryRate), _
                                      New SqlParameter("@sss", sss), _
                                      New SqlParameter("@pagibig", pagibig), _
                                      New SqlParameter("@philhealth", philhealth), _
                                      New SqlParameter("@companyLoan", comploan), _
                                      New SqlParameter("@others", others), _
                                      New SqlParameter("@remainingNetPay", remainingNetPay), _
                                      New SqlParameter("@maxLoan_capacity", maxLoan_capacity), _
                                      New SqlParameter("@capitalContribution", capitalContribution), _
                                      New SqlParameter("@policyRate", policyRate), _
                                      New SqlParameter("@maxLoan_Capital", maxLoan_Capital), _
                                      New SqlParameter("@fcPaymentMode", fcPaymentMode), _
                                      New SqlParameter("@totalCoopLoans", totalCoopLoans), _
                                      New SqlParameter("@amortPerPayroll", GetAmortPerPayroll()), _
                                      New SqlParameter("@missedPayments", missedPayments), _
                                      New SqlParameter("@intrefund_restruc", intrefund_restruc), _
                                      New SqlParameter("@Company", frmLoan_ApplicationMain.cboProject.Text))

            trans.Commit()


        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "MSS_Loans_AddMemberLoans")
        Finally
            gcon.sqlconn.Close()
        End Try

        'MSS_Loans_AddMemberLoans

    End Sub
#End Region

    Private Sub DeleteAmortization_ThenUpdate(ByVal loanno As String)
        Try
            If loanno = "" Then
                loanno = frmLoan_ApplicationMain.txtLoanNo.Text
            End If

            SqlHelper.ExecuteNonQuery(gcon.cnstring, "_DeleteAmortization_Schedule_ForUpdating",
                                       New SqlParameter("@LoanNo", loanno))
        Catch ex As Exception

        End Try
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        _refresh_Cols()
        xConEditMode = False
        frmLoan_ApplicationMain.inEditMode = False
        Me.Close()
    End Sub

    'Update status added 8/7/2014 Vince
    Public Function GetLoanStatus()
        Try
            If frmLoan_ApplicationMain.loanStatus = "" Then
                Dim rd As SqlDataReader
                rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Get_LoanStatus_byLoanNo",
                                              New SqlParameter("@LoanNo", frmLoan_ApplicationMain.txtLoanNo.Text))
                While rd.Read
                    Return rd(0)
                End While
            Else
                Return frmLoan_ApplicationMain.loanStatus
            End If
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Private Sub cmdSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSubmit.Click
        If isInEditMode() = True Then
            'code here
            If MsgBox("Are you sure you want to save this loan?", vbYesNo, "Confirmation") = vbYes Then
                Try
                    Call CreditComAppInsert(Me.txtEmpNo.Text, GetLoanStatus(), Me.lblType.Text, Me.lblApplicationDate.Text, frmLoan_ApplicationMain.txtLoanNo.Text, Me.lblClassification.Text, Me.lblPrincipal.Text, loanInterest, Me.lblInterest.Text, _
                                   LoanRecievable, ServicefeePercent, Me.txtservicefeedetails.Text, CapitalSharePercent, 0.0, Me.txtMonthlyAmor.Text, Me.txtAmorDateStart.Text, Me.txtAmorDateEnd.Text, _
                                   12, Me.txtTotalPenalties.Text, Me.txtTotalIntRefunds.Text, Me.txtNetProceeds.Text, Me.lblTerms.Text, lblPrincipal.Text, 0.0, loanPreterminated, frmLoan_ApplicationMain.salary01, 0.0, _
                                   0.0, 0.0, 0.0, 0.0, 0.0, MaxloanCapacity, CapitalContribution, PolicyRate, 0.0, _
                                   PaymentMode, 0.0, txtIntRefundRestruc.Text, txtMissedPayments.Text)

                    'Call SaveAmortSchedule()
                    'Call SaveCoMakers()
                    'Call UpdateDocNumber()
                    'Call UpdatedAdjInterestRefund()
                    frmLoan_ApplicationMain.panelStep1.Enabled = False
                    frmLoan_ApplicationMain.panelstep2.Enabled = False
                    frmLoan_ApplicationMain.panelStep3.Enabled = False
                    frmLoan_ApplicationMain.dgvComakerList.Columns.Clear()
                Catch ex As Exception

                End Try
            End If

            Exit Sub
            ' Me.Close()
            frmLoan_ApplicationMain.inEditMode = False
        End If

        Dim x As New DialogResult
        Try
            x = MessageBox.Show("Are you sure you want to submit this for processing?", "Submit", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If x = Windows.Forms.DialogResult.Yes Then
                If Me.chkCheck.Checked = True Or Me.chkATM.Checked = True Or Me.chkCash.Checked = True Then
                    Call CreditComAppInsert(Me.txtEmpNo.Text, "On-Process", Me.lblType.Text, Me.lblApplicationDate.Text, frmLoan_ApplicationMain.txtLoanNo.Text, Me.lblClassification.Text, Me.lblPrincipal.Text, loanInterest, Me.lblInterest.Text, _
                                                       LoanRecievable, ServicefeePercent, Me.txtservicefeedetails.Text, CapitalSharePercent, 0.0, Me.txtMonthlyAmor.Text, Me.txtAmorDateStart.Text, Me.txtAmorDateEnd.Text, _
                                                       12, Me.txtTotalPenalties.Text, Me.txtTotalIntRefunds.Text, Me.txtNetProceeds.Text, Me.lblTerms.Text, lblPrincipal.Text, 0.0, loanPreterminated, frmLoan_ApplicationMain.salary01, 0.0, _
                                                       0.0, 0.0, 0.0, 0.0, 0.0, MaxloanCapacity, CapitalContribution, PolicyRate, 0.0, _
                                                       PaymentMode, 0.0, txtIntRefundRestruc.Text, txtMissedPayments.Text)

                    Call SaveAmortSchedule()
                    Call SaveCoMakers()
                    Call UpdateDocNumber()
                    Call ModeOfPayment_Saved()

                    frmLoan_ApplicationMain.panelStep1.Enabled = False
                    frmLoan_ApplicationMain.panelstep2.Enabled = False
                    frmLoan_ApplicationMain.panelStep3.Enabled = False
                    frmLoan_ApplicationMain.dgvComakerList.Columns.Clear()
                Else
                    MessageBox.Show("Unable to proceed, Please Select one payment mode", "Payment mode", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            Else

            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub UpdateDocNumber()
        SqlHelper.ExecuteNonQuery(gcon.cnstring, "_Update_DocNumber_Used",
                                   New SqlParameter("@DocNumber", frmLoan_ApplicationMain.txtLoanNo.Text),
                                   New SqlParameter("@DateUsed", Me.lblApplicationDate.Text))
    End Sub

    Private Sub txtMonthlyAmor_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMonthlyAmor.TextChanged
        If Me.txtMonthlyAmor.Text = "" Then
            Me.txtMonthlyAmor.Text = "0.00"
        Else
            Dim amt As Decimal = CType(Me.txtMonthlyAmor.Text, Decimal)
            Me.txtMonthlyAmor.Text = String.Format("{0:n2}", amt)
        End If
    End Sub
    Private Sub txtPrincipaldetails_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPrincipaldetails.TextChanged
        If Me.txtPrincipaldetails.Text = "" Then
            Me.txtPrincipaldetails.Text = "0.00"
        Else
            Dim amt As Decimal = CType(Me.txtPrincipaldetails.Text, Decimal)
            Me.txtPrincipaldetails.Text = String.Format("{0:n2}", amt)
        End If
    End Sub

    Private Sub txtservicefeedetails_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtservicefeedetails.TextChanged
        If Me.txtservicefeedetails.Text = "" Then
            Me.txtservicefeedetails.Text = "0.00"
        Else
            Dim amt As Decimal = CType(Me.txtservicefeedetails.Text, Decimal)
            Me.txtservicefeedetails.Text = String.Format("{0:n2}", amt)
        End If
    End Sub

    Private Sub txtNetPreterminatedloan_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNetPreterminatedloan.TextChanged
        If Me.txtNetPreterminatedloan.Text = "" Then
            Me.txtNetPreterminatedloan.Text = "0.00"
        Else

            Dim amt As Decimal = CType(Me.txtNetPreterminatedloan.Text, Decimal)
            Me.txtNetPreterminatedloan.Text = String.Format("{0:n2}", amt)


        End If
    End Sub

    'Disabled
    'Private Sub txtTotalPenalties_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTotalPenalties.TextChanged
    '    If Me.txtTotalPenalties.Text = "" Then
    '        Me.txtTotalPenalties.Text = "0.00"
    '    Else
    '        Dim amt As Decimal = CType(Me.txtTotalPenalties.Text, Decimal)
    '        Me.txtTotalPenalties.Text = String.Format("{0:n2}", amt)
    '    End If
    'End Sub

    Private Sub txtNetProceeds_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNetProceeds.TextChanged
        If Me.txtNetProceeds.Text = "" Then
            Me.txtNetProceeds.Text = "0.00"
        Else
            Dim amt As Decimal = CType(Me.txtNetProceeds.Text, Decimal)
            Me.txtNetProceeds.Text = String.Format("{0:n2}", amt)
        End If
    End Sub

    Private Sub txtPrincipalAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPrincipalAmt.TextChanged
        If Me.txtPrincipalAmt.Text = "" Then
            Me.txtPrincipalAmt.Text = "0.00"
        Else
            Dim amt As Decimal = CType(Me.txtPrincipalAmt.Text, Decimal)
            Me.txtPrincipalAmt.Text = String.Format("{0:n2}", amt)
        End If
    End Sub

    Private Sub txtInterestAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInterestAmt.TextChanged
        If Me.txtInterestAmt.Text = "" Then
            Me.txtInterestAmt.Text = "0.00"
        Else
            Dim amt As Decimal = CType(Me.txtInterestAmt.Text, Decimal)
            Me.txtInterestAmt.Text = String.Format("{0:n2}", amt)
        End If
    End Sub

    Private Sub txtServicefee_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtServicefee.TextChanged
        If Me.txtServicefee.Text = "" Then
            Me.txtServicefee.Text = "0.00"
        Else
            Dim amt As Decimal = CType(Me.txtServicefee.Text, Decimal)
            Me.txtServicefee.Text = String.Format("{0:n2}", amt)

        End If
    End Sub

    Private Sub txtCapitalShare_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCapitalShare.TextChanged
        If Me.txtCapitalShare.Text = "" Then
            Me.txtCapitalShare.Text = "0.00"
        Else
            Dim amt As Decimal = CType(Me.txtCapitalShare.Text, Decimal)
            Me.txtCapitalShare.Text = String.Format("{0:n2}", amt)
        End If
    End Sub


    Private Sub chkCheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCheck.CheckedChanged
        If Me.chkCheck.Checked = True Then
            getPaymentMode() = "Cheque"
            Me.chkATM.Enabled = False
            Me.chkATM.Checked = False
            Me.chkCash.Enabled = False
            Me.chkCash.Checked = False
        Else
            Me.chkCash.Enabled = True
            Me.chkATM.Enabled = True
        End If
    End Sub

    Private Sub chkATM_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkATM.CheckedChanged
        If Me.chkATM.Checked = True Then
            getPaymentMode() = "ATM"
            Me.chkCheck.Enabled = False
            Me.chkCheck.Checked = False
            Me.chkCash.Enabled = False
            Me.chkCash.Checked = False
        Else
            Me.chkCheck.Enabled = True
            Me.chkCash.Enabled = True
        End If
    End Sub

    Private Sub ExecAmortizationSchedule()
        xmode = frmLoan_ApplicationMain.xSelectedIntMethod

        If chkDeductinPayroll.Checked = True Then
            'schedule based in payroll
            LoadAmort_DeductedinPayroll(frmLoan_ApplicationMain.txtPrincipalAmount.Text, lblInterest.Text, lblTerms.Text, txtAmorDateStart.Text, xmode)
        Else
            ' load regular amortization
            LoadAmortDetails(frmLoan_ApplicationMain.txtPrincipalAmount.Text, lblInterest.Text, lblTerms.Text, txtAmorDateStart.Text, xmode)
        End If

        GetAmortizationTotals()
    End Sub

    'added 8/7/2014 Vince ### Deducted in Payroll amortization
    ' - Under Constraction -
    Private Sub LoadAmort_DeductedinPayroll(ByVal LoanAmt As String, ByVal interest As String, ByVal terms As String, ByVal datestart As String, ByVal mode As String)
        Dim noofpayment As Integer = CType(frmLoan_ApplicationMain.txtnoofpayment.Text, Integer)
        Dim interestrate As Decimal
        Select Case frmLoan_ApplicationMain.cboModeofPayment.Text
            Case "DAILY"
                interestrate = CDec(txtAnnulInterest.Text / 360)
            Case "WEEKLY"
                interestrate = CDec(txtAnnulInterest.Text / 48)
            Case "SEMI-MONTHLY"
                interestrate = CDec(txtAnnulInterest.Text / 24)
            Case "MONTHLY"
                interestrate = CDec(txtAnnulInterest.Text / 12)
            Case "QUARTERLY"
                interestrate = CDec(txtAnnulInterest.Text / 3)
            Case "ANNUALLY"
                interestrate = CDec(txtAnnulInterest.Text / 1)
            Case "LUMP SUM"
                interestrate = CDec(txtAnnulInterest.Text)
        End Select

        Select Case mode
            Case "Straight Add-On"
                Try
                    Dim ds As DataSet
                    ds = SqlHelper.ExecuteDataset(gcon.cnstring, "Calculate_Amortization_Schedule_Straight_AddOn_SemiMonthly",
                                                   New SqlParameter("@PrincipalAmount", LoanAmt),
                                                   New SqlParameter("@InterestRate", interestrate),
                                                   New SqlParameter("@Terms", noofpayment),
                                                   New SqlParameter("@DateGranted", txtAmorDateStart.Text),
                                                   New SqlParameter("@FirstPayment", dtFirstPayment.Value))
                    dgvList.DataSource = ds.Tables(0)
                    dgvList.Columns(0).Width = 50
                Catch ex As Exception
                End Try
            Case "Straight Deducted"
                Try
                    Dim ds As DataSet
                    ds = SqlHelper.ExecuteDataset(gcon.cnstring, "Calculate_Amortization_Schedule_Straight_Deducted_Semimonthly",
                                                   New SqlParameter("@PrincipalAmount", LoanAmt),
                                                   New SqlParameter("@InterestRate", interestrate),
                                                   New SqlParameter("@Terms", noofpayment),
                                                   New SqlParameter("@DateGranted", txtAmorDateStart.Text),
                                                   New SqlParameter("@FirstPayment", dtFirstPayment.Value))
                    dgvList.DataSource = ds.Tables(0)
                    dgvList.Columns(0).Width = 50
                Catch ex As Exception
                End Try
            Case "Diminishing Declining"
                Try
                    Dim ds As DataSet
                    ds = SqlHelper.ExecuteDataset(gcon.cnstring, "Calculate_Amortization_Schedule_Diminishing_Semimonthly",
                                                   New SqlParameter("@PrincipalAmount", LoanAmt),
                                                   New SqlParameter("@InterestRate", interestrate),
                                                   New SqlParameter("@Terms", noofpayment),
                                                   New SqlParameter("@DateGranted", txtAmorDateStart.Text),
                                                   New SqlParameter("@FirstPayment", dtFirstPayment.Value))
                    dgvList.DataSource = ds.Tables(0)
                    dgvList.Columns(0).Width = 50
                Catch ex As Exception
                End Try
            Case "Diminishing"
                Try
                    Dim ds As DataSet
                    ds = SqlHelper.ExecuteDataset(gcon.cnstring, "Calculate_Amortization_Schedule_Diminishing_Semimonthly",
                                                   New SqlParameter("@PrincipalAmount", LoanAmt),
                                                   New SqlParameter("@InterestRate", interestrate),
                                                   New SqlParameter("@Terms", noofpayment),
                                                   New SqlParameter("@DateGranted", txtAmorDateStart.Text),
                                                   New SqlParameter("@FirstPayment", dtFirstPayment.Value))
                    dgvList.DataSource = ds.Tables(0)
                    dgvList.Columns(0).Width = 50
                Catch ex As Exception
                End Try
        End Select
    End Sub
    '##### Regular Amortization #####
    Private Sub LoadAmortDetails(ByVal LoanAmt As String, ByVal interest As String, ByVal terms As String, ByVal datestart As String, ByVal mode As String)
        Dim noofpayment As Integer = CType(frmLoan_ApplicationMain.txtnoofpayment.Text, Integer)
        Dim interestrate As Decimal
        Select Case frmLoan_ApplicationMain.cboModeofPayment.Text
            Case "DAILY"
                interestrate = CDec(txtAnnulInterest.Text / 360)
            Case "WEEKLY"
                interestrate = CDec(txtAnnulInterest.Text / 48)
            Case "SEMI-MONTHLY"
                interestrate = CDec(txtAnnulInterest.Text / 24)
            Case "MONTHLY"
                interestrate = CDec(txtAnnulInterest.Text / 12)
            Case "QUARTERLY"
                interestrate = CDec(txtAnnulInterest.Text / 3)
            Case "ANNUALLY"
                interestrate = CDec(txtAnnulInterest.Text / 1)
            Case "LUMP SUM"
                interestrate = CDec(txtAnnulInterest.Text)
        End Select

        Select Case mode
            Case "Ascending/Declining" '3/10/2015
                Try
                    Dim ds As DataSet
                    ds = SqlHelper.ExecuteDataset(gcon.cnstring, "Calculate_Amortization_Schedule_AscendingDeclining",
                                                   New SqlParameter("@PrincipalAmount", LoanAmt),
                                                   New SqlParameter("@InterestRate", interestrate),
                                                   New SqlParameter("@Terms", noofpayment),
                                                   New SqlParameter("@DateGranted", txtAmorDateStart.Text),
                                                   New SqlParameter("@FirstPayment", dtFirstPayment.Value),
                                                   New SqlParameter("@xmode", payMode()))
                    dgvList.DataSource = ds.Tables(0)
                    dgvList.Columns(0).Width = 50
                Catch ex As Exception
                End Try
            Case "Straight"
                Try
                    Dim ds As DataSet
                    ds = SqlHelper.ExecuteDataset(gcon.cnstring, "Calculate_Amortization_Schedule_Straight_AddOn",
                                                   New SqlParameter("@PrincipalAmount", LoanAmt),
                                                   New SqlParameter("@InterestRate", interestrate),
                                                   New SqlParameter("@Terms", noofpayment),
                                                   New SqlParameter("@DateGranted", txtAmorDateStart.Text),
                                                   New SqlParameter("@FirstPayment", dtFirstPayment.Value),
                                                   New SqlParameter("@xmode", payMode()))
                    dgvList.DataSource = ds.Tables(0)
                    dgvList.Columns(0).Width = 50
                Catch ex As Exception
                End Try
            Case "Straight Add-On"
                Try
                    Dim ds As DataSet
                    ds = SqlHelper.ExecuteDataset(gcon.cnstring, "Calculate_Amortization_Schedule_Straight_AddOn",
                                                   New SqlParameter("@PrincipalAmount", LoanAmt),
                                                   New SqlParameter("@InterestRate", interestrate),
                                                   New SqlParameter("@Terms", noofpayment),
                                                   New SqlParameter("@DateGranted", txtAmorDateStart.Text),
                                                   New SqlParameter("@FirstPayment", dtFirstPayment.Value),
                                                   New SqlParameter("@xmode", payMode()))
                    dgvList.DataSource = ds.Tables(0)
                    dgvList.Columns(0).Width = 50
                Catch ex As Exception
                End Try
            Case "Straight Deducted"
                Try
                    Dim ds As DataSet
                    ds = SqlHelper.ExecuteDataset(gcon.cnstring, "Calculate_Amortization_Schedule_Straight_Deducted",
                                                   New SqlParameter("@PrincipalAmount", LoanAmt),
                                                   New SqlParameter("@InterestRate", interestrate),
                                                   New SqlParameter("@Terms", noofpayment),
                                                   New SqlParameter("@DateGranted", txtAmorDateStart.Text),
                                                   New SqlParameter("@FirstPayment", dtFirstPayment.Value),
                                                   New SqlParameter("@xmode", payMode()))
                    dgvList.DataSource = ds.Tables(0)
                    dgvList.Columns(0).Width = 50
                Catch ex As Exception
                End Try
            Case "Diminishing Declining"
                Try
                    Dim ds As DataSet
                    ds = SqlHelper.ExecuteDataset(gcon.cnstring, "Calculate_Amortization_Schedule_DiminishingDeclining",
                                                   New SqlParameter("@PrincipalAmount", LoanAmt),
                                                   New SqlParameter("@InterestRate", interestrate),
                                                   New SqlParameter("@Terms", noofpayment),
                                                   New SqlParameter("@DateGranted", txtAmorDateStart.Text),
                                                   New SqlParameter("@FirstPayment", dtFirstPayment.Value),
                                                   New SqlParameter("@xmode", payMode()))
                    dgvList.DataSource = ds.Tables(0)
                    dgvList.Columns(0).Width = 50
                Catch ex As Exception
                End Try
            Case "Diminishing"
                Try
                    Dim ds As DataSet
                    ds = SqlHelper.ExecuteDataset(gcon.cnstring, "Calculate_Amortization_Schedule_Diminishing",
                                                   New SqlParameter("@PrincipalAmount", LoanAmt),
                                                   New SqlParameter("@InterestRate", interestrate),
                                                   New SqlParameter("@Terms", noofpayment),
                                                   New SqlParameter("@DateGranted", txtAmorDateStart.Text),
                                                   New SqlParameter("@FirstPayment", dtFirstPayment.Value),
                                                   New SqlParameter("@xmode", payMode()))
                    dgvList.DataSource = ds.Tables(0)
                    dgvList.Columns(0).Width = 50
                Catch ex As Exception
                End Try
        End Select

    End Sub

    Private Sub SaveAmortSchedule() 'by Vincent Nacar
        For i As Integer = 0 To dgvList.RowCount - 1
            With dgvList
                Dim loanNo As String = frmLoan_ApplicationMain.txtLoanNo.Text
                Dim payNo As String = .Item(1, i).Value
                Dim fcDate As String = .Item(2, i).Value
                Dim xprincipal As String = .Item(3, i).Value
                Dim xinterest As String = .Item(4, i).Value
                Dim xservicefee As String = .Item(5, i).Value
                Dim xamortization As String = .Item(6, i).Value
                Dim balance As String = .Item(7, i).Value
                Dim method As String = xmode
                Dim status As String
                If .Item(0, i).Value = True Then
                    status = "Paid" 'deduct interest in advance
                Else
                    status = "Unpaid"
                End If

                SqlHelper.ExecuteNonQuery(gcon.cnstring, "_Insert_AmortizationSchedule",
                                           New SqlParameter("@pk_KeyID", System.Guid.NewGuid),
                                           New SqlParameter("@fcLoanNo", loanNo),
                                           New SqlParameter("@fdPayNo", payNo),
                                           New SqlParameter("@fcDate", fcDate),
                                           New SqlParameter("@fdPrincipal", xprincipal),
                                           New SqlParameter("@fdInterest", xinterest),
                                           New SqlParameter("@fdServiceFee", xservicefee),
                                           New SqlParameter("@fdAmortization", xamortization),
                                           New SqlParameter("@fdBalance", balance),
                                           New SqlParameter("@fcMethod", method),
                                           New SqlParameter("@fcStatus", status))
            End With
        Next
    End Sub

    Private Sub SaveCoMakers() 'by Vincent Nacar
        For i As Integer = 0 To frmLoan_ApplicationMain.dgvComakerList.RowCount - 1
            With frmLoan_ApplicationMain.dgvComakerList
                Dim maker As String = Me.txtEmpNo.Text
                Dim loanNo As String = frmLoan_ApplicationMain.txtLoanNo.Text
                Dim EmpNo As String = .Item(0, i).Value
                Dim EmpName As String = .Item(1, i).Value
                Dim DocNum As String = .Item(2, i).Value
                Dim AcctName As String = .Item(3, i).Value
                Dim Status As String = .Item(4, i).Value
                Dim Amount As String = .Item(5, i).Value

                SqlHelper.ExecuteNonQuery(gcon.cnstring, "_Insert_Comakers",
                                           New SqlParameter("@fcMakerID", maker),
                                           New SqlParameter("@fcLoanNo", loanNo),
                                           New SqlParameter("@fcEmployeeNo", EmpNo),
                                           New SqlParameter("@fcFullname", EmpName),
                                           New SqlParameter("@fcDocNo", DocNum),
                                           New SqlParameter("@fcAccountName", AcctName),
                                           New SqlParameter("@fcStatus", Status),
                                           New SqlParameter("@fcAmount", Amount))
            End With
        Next
    End Sub

    Private Sub btnPost_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Dim x As New DialogResult
        Try
            x = MessageBox.Show("Are you sure you want to Post this transaction?", "Posting", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If x = Windows.Forms.DialogResult.Yes Then
                getPaymentMode() = "Cheque"
                'If Me.chkCheck.Checked = True Or Me.chkATM.Checked = True Then

                'Save Check voucher Entry
                'Added by Vincent Nacar
                '7/28/14
                'frmChkVoucherEntry.empno = txtEmpNo.Text
                'frmChkVoucherEntry.LoanNo = frmLoan_ApplicationMain.txtLoanNo.Text
                'frmChkVoucherEntry.StartPosition = FormStartPosition.CenterScreen
                'frmChkVoucherEntry.ShowDialog()
                '##################################################################

                Call CreditComAppInsert(Me.txtEmpNo.Text, "Released", Me.lblType.Text, Me.lblApplicationDate.Text, frmLoan_ApplicationMain.txtLoanNo.Text, Me.lblClassification.Text, Me.lblPrincipal.Text, loanInterest, Me.lblInterest.Text, _
                                                   LoanRecievable, ServicefeePercent, Me.txtservicefeedetails.Text, CapitalSharePercent, 0.0, Me.txtMonthlyAmor.Text, Me.txtAmorDateStart.Text, Me.txtAmorDateEnd.Text, _
                                                   12, Me.txtTotalPenalties.Text, Me.txtTotalIntRefunds.Text, Me.txtNetProceeds.Text, Me.lblTerms.Text, lblPrincipal.Text, 0.0, loanPreterminated, frmLoan_ApplicationMain.salary01, 0.0, _
                                                   0.0, 0.0, 0.0, 0.0, 0.0, MaxloanCapacity, CapitalContribution, PolicyRate, 0.0, _
                                                   PaymentMode, 0.0, txtIntRefundRestruc.Text, txtMissedPayments.Text)

                'Call SaveAmortSchedule()
                'Call SaveCoMakers()
                'Call UpdateDocNumber()
                'Call UpdatedAdjInterestRefund()
                frmLoan_ApplicationMain.panelStep1.Enabled = False
                frmLoan_ApplicationMain.panelstep2.Enabled = False
                frmLoan_ApplicationMain.panelStep3.Enabled = False
                frmLoan_ApplicationMain.dgvComakerList.Columns.Clear()
                'Else
                ' MessageBox.Show("Unable to proceed, Please Select one payment mode", "Payment mode", MessageBoxButtons.OK, MessageBoxIcon.Information)
                'End If
            Else

            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub btnRefreshAmort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshAmort.Click
        'GenerateAmortDate()
        GenerateAmortDateEND()
        ExecAmortizationSchedule()
        _NormalizeVal_GotowithEgg() 'test
        dgvList.Columns(5).HeaderText = "Other Payment"
    End Sub

#Region "Generate Date End 12-1-14"
    Private Sub GenerateAmortDateEND()
        Dim mode As String = frmLoan_ApplicationMain.cboModeofPayment.Text
        frmLoan_ApplicationMain.dtpLoanApplicationDate.Value = txtAmorDateStart.Value
        Dim t As Integer = frmLoan_ApplicationMain.txtnoofpayment.Text
        Dim fpay As Date = dtFirstPayment.Value
        Dim weekctr As Integer = 7
        Dim weekdays As Date
        'updated 8/15/14
        Select Case mode
            Case "DAILY"
                txtAmorDateEnd.Value = fpay.AddDays(t)
            Case "WEEKLY"
                txtAmorDateEnd.Value = DateAdd(DateInterval.Day, 7, dtFirstPayment.Value)
            Case "SEMI-MONTHLY"
                txtAmorDateEnd.Value = DateAdd(DateInterval.Day, 15, dtFirstPayment.Value)
            Case "MONTHLY"
                txtAmorDateEnd.Value = fpay.AddMonths(t)
            Case "QUARTERLY"
                txtAmorDateEnd.Value = DateAdd(DateInterval.Month, 3, dtFirstPayment.Value)
            Case "ANNUALLY"
                txtAmorDateEnd.Value = fpay.AddYears(t)
            Case "LUMP SUM"
                txtAmorDateEnd.Value = dtFirstPayment.Value
        End Select
    End Sub
#End Region

    Private Sub dgvList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.Click
        _EXEC_AdvanceInterest()
        GetAmortizationTotals()
    End Sub

    Private Sub _EXEC_AdvanceInterest()
        If dgvList.SelectedRows(0).Cells(1).Value = "0" Then
            If dgvList.SelectedRows(0).Cells(0).Value = True Then
                For i As Integer = 0 To dgvList.RowCount - 1
                    With dgvList
                        .Rows(i).Cells(0).Value = False
                        .Rows(i).DefaultCellStyle.BackColor = Color.White
                        .Rows(i).Cells(6).Value = Format((CDec(.Rows(i).Cells(6).Value) + CDec(.Rows(i).Cells(4).Value)), "##,##0.00")
                    End With
                Next
            Else
                For i As Integer = 0 To dgvList.RowCount - 1
                    With dgvList
                        .Rows(i).Cells(0).Value = True
                        .Rows(i).DefaultCellStyle.BackColor = Color.SkyBlue
                        .Rows(i).Cells(6).Value = Format(CDec(.Rows(i).Cells(3).Value), "##,##0.00")
                    End With
                Next
            End If
            Calculate_Total_UnearnedInterest()
            Exit Sub
        End If
        If dgvList.SelectedRows(0).Cells(0).Value = True Then
            dgvList.SelectedRows(0).Cells(0).Value = False
            dgvList.SelectedRows(0).DefaultCellStyle.BackColor = Color.White
            With dgvList
                .SelectedRows(0).Cells(6).Value = Format((CDec(.SelectedRows(0).Cells(6).Value) + CDec(.SelectedRows(0).Cells(4).Value)), "##,##0.00")
            End With
        Else
            dgvList.SelectedRows(0).Cells(0).Value = True
            dgvList.SelectedRows(0).DefaultCellStyle.BackColor = Color.SkyBlue
            With dgvList
                .SelectedRows(0).Cells(6).Value = Format(CDec(.SelectedRows(0).Cells(3).Value), "##,##0.00")
            End With
        End If
        Calculate_Total_UnearnedInterest()
    End Sub

    Private Sub Calculate_Total_UnearnedInterest() 'added by vincent Nacar 8/15/14
        Try
            Dim totunearned As Decimal
            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList
                    If .Rows(i).Cells(0).Value = True Then
                        totunearned = totunearned + CDec(.Rows(i).Cells(4).Value.ToString)
                    End If
                End With
            Next
            txtTotUnearned.Text = Format(totunearned, "##,##0.00")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub chkCash_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCash.CheckedChanged
        If Me.chkCash.Checked = True Then
            getPaymentMode() = "Cash"
            Me.chkCheck.Enabled = False
            Me.chkCheck.Checked = False
            Me.chkATM.Enabled = False
            Me.chkATM.Checked = False
        Else
            Me.chkCheck.Enabled = True
            Me.chkATM.Enabled = True
        End If
    End Sub

#Region "Printing"

    Private Sub btnPrintForm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintForm.Click
        Try
            If chkATM.Checked = False And chkCheck.Checked = False And chkCash.Checked = False Then
                MsgBox("Select Payment Type to continue.", vbInformation, "Oops.")
            Else
                FirstSave()
                'frmReport_LoanApplication.LoadREport2(frmLoan_ApplicationMain.txtLoanNo.Text)
                'frmReport_LoanApplication.WindowState = FormWindowState.Maximized
                'frmReport_LoanApplication.ShowDialog()
                'frmLoanFilT.StartPosition = FormStartPosition.CenterScreen
                'frmLoanFilT.ShowDialog()
                frmReport_LoanApplication.LoadREport_Cash(frmLoan_ApplicationMain.txtLoanNo.Text, frmLoan_ApplicationMain.cboProject.Text)
                frmReport_LoanApplication.WindowState = FormWindowState.Maximized
                frmReport_LoanApplication.ShowDialog()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnPrinAgreement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrinAgreement.Click
        Try
            If chkATM.Checked = False And chkCheck.Checked = False And chkCash.Checked = False Then
                MsgBox("Select Payment Type to continue.", vbInformation, "Oops.")
            Else
                FirstSave()
                frmReport_LoanApplication.LoadREport(frmLoan_ApplicationMain.txtLoanNo.Text)
                frmReport_LoanApplication.WindowState = FormWindowState.Maximized
                frmReport_LoanApplication.ShowDialog()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnPrintDisclousure_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintDisclousure.Click
        Try
            If chkATM.Checked = False And chkCheck.Checked = False And chkCash.Checked = False Then
                MsgBox("Select Payment Type to continue.", vbInformation, "Oops.")
            Else
                FirstSave()
                frmReport_LoanDetails.LoadREport(txtEmpNo.Text, frmLoan_ApplicationMain.txtLoanNo.Text, frmLoan_ApplicationMain.cboProject.Text)
                frmReport_LoanDetails.WindowState = FormWindowState.Maximized
                frmReport_LoanDetails.StartPosition = FormStartPosition.CenterScreen
                frmReport_LoanDetails.ShowDialog()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

#End Region

#Region "Added by Vince 10/3/14 Computation of Net Proceeds"
    Private Sub _Prep_Deductions_Col()
        dgvDeductions.DataSource = Nothing
        dgvDeductions.Columns.Clear()
        dgvDeductions.Rows.Clear()
        Dim code As New DataGridViewTextBoxColumn
        Dim accttitle As New DataGridViewTextBoxColumn
        Dim amount As New DataGridViewTextBoxColumn
        With code
            .Name = "code"
            .HeaderText = "Code"
            .Visible = False
        End With
        With accttitle
            .Name = "accttitle"
            .HeaderText = "Acct. Title"
        End With
        With amount
            .Name = "amount"
            .HeaderText = "Amount"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With dgvDeductions
            .Columns.Add(code)
            .Columns.Add(accttitle)
            .Columns.Add(amount)
        End With
    End Sub

    Private Sub _Load_Deductions(ByVal principal As Decimal, ByVal loantype As String, ByVal idno As String)
        Try
            _Prep_Deductions_Col()
            Dim i As Integer = 0
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, "_Generate_Loantype_Charges_2",
                                                               New SqlParameter("@Principal", principal),
                                                               New SqlParameter("@LoanType", loantype),
                                                               New SqlParameter("@IDNo", idno),
                                                               New SqlParameter("@CompanyName", frmLoan_ApplicationMain.cboProject.Text))
            While rd.Read
                With dgvDeductions
                    .Rows.Add()
                    .Item("code", i).Value = rd(0)
                    .Item("accttitle", i).Value = rd(1)
                    .Item("amount", i).Value = rd(2)
                    .Item("code", i).Tag = rd(3) 'Acct Type
                    .Item("accttitle", i).Tag = rd(4) 'Acct Ref.
                    i += 1
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "_Loan_Deductions")
        End Try
    End Sub

    Private Sub dgvDeductions_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDeductions.CellValueChanged
        Dim deductions As Decimal = 0
        For i As Integer = 0 To dgvDeductions.RowCount - 1
            With dgvDeductions
                deductions = deductions + .Item("amount", i).Value
                .Item("amount", i).Value = Format(CDec(.Item("amount", i).Value), "##,##0.00")
            End With
        Next
        txtNetProceeds.Text = Format(CDec(txtPrincipaldetails.Text) - deductions, "##,##0.00")
    End Sub

    Private Sub _GetAmount_Interest_Unearned() Handles dgvList.CellValueChanged
        Dim _interest As Decimal = 0
        Dim _Advance As Decimal = 0
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList
                    If .Rows(i).Cells(0).Value = True Then
                        _Advance = _Advance + .Item(4, i).Value
                    Else
                        _interest = _interest + .Item(4, i).Value
                    End If
                End With
            Next
            For i As Integer = 0 To dgvDeductions.RowCount - 1
                With dgvDeductions
                    If .Item("code", i).Tag = "Interest" Then
                        .Item("amount", i).Value = Format(_interest, "##,##0.00")
                    End If
                    If .Item("code", i).Tag = "Unearned Interest" Then
                        .Item("amount", i).Value = Format(_Advance, "##,##0.00")
                    End If
                End With
            Next
        Catch ex As Exception
            MsgBox(gcon.cnstring, vbInformation, "_GetAmount_Interest_Unearned")
        End Try
    End Sub


    Private Sub _Save_LoanCharges(ByVal loanno As String, ByVal acctref As String,
                                  ByVal code As String, ByVal accttitle As String,
                                  ByVal amt As Decimal)
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, "_Loan_Charges_InsertUpdate",
                                       New SqlParameter("@fcLoanRef", loanno),
                                       New SqlParameter("@fcAcctRef", acctref),
                                       New SqlParameter("@fcCode", code),
                                       New SqlParameter("@fcAcctTitle", accttitle),
                                       New SqlParameter("@fdAmount", amt))
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "_Save_LoanCharges")
        End Try
    End Sub
    Private Sub _save_LoanCharges_exec() Handles cmdSubmit.Click
        For i As Integer = 0 To dgvDeductions.RowCount - 1
            With dgvDeductions
                _Save_LoanCharges(frmLoan_ApplicationMain.txtLoanNo.Text, _
                                  .Item("accttitle", i).Tag, _
                                  .Item("code", i).Value, _
                                  .Item("accttitle", i).Value, _
                                  .Item("amount", i).Value)
            End With
        Next
    End Sub

#End Region

#Region "Check all advance interest"

    Private Sub _GenerateColor_forDeductedInterest()
        If frmLoan_ApplicationMain.xSelectedIntMethod = "Straight Deducted" Then
            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList
                    If .Rows(i).Cells(3).Value = .Rows(i).Cells(6).Value Then
                        .Rows(i).Cells(0).Value = True
                        .Rows(i).DefaultCellStyle.BackColor = Color.SkyBlue
                    End If
                End With
            Next
        End If
    End Sub

#End Region

#Region "Load Advance Interest"
    Private Sub _LoadAdvanceInterest(ByVal loanno As String)
        Try
            Dim totadv As Decimal = 0
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, "_LoadAdvanceInterest",
                                                               New SqlParameter("@LoanNo", loanno))

            While rd.Read
                For i As Integer = 0 To dgvList.RowCount - 1
                    With dgvList
                        If .Item(1, i).Value = rd(0) Then
                            If rd(1) = "Paid" Then
                                .Item(0, i).Value = True
                                .Rows(i).DefaultCellStyle.BackColor = Color.SkyBlue
                                totadv = totadv + rd(2)
                            End If
                        End If
                    End With
                Next
            End While
            txtTotUnearned.Text = Format(totadv, "##,##0.00")
        Catch ex As Exception
            MsgBox(ex.Message, vbInformation, "_LoadAdvanceInterest")
        End Try
    End Sub
#End Region

#Region "Generate Amortization Using Customize Interest Method Added by Vince 10/16/14"

    'read the int method setup
    Private Sub _IntMethod(ByVal terms As Integer)
        Try
            Dim yr As Integer = terms / 12
            Dim intmethod As String = ""
            Dim ctr As Integer = 1
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, "_IntMethod_Load")
            While rd.Read
                If rd(4) = True Then
                    If ctr = rd(0) Then
                        If ctr <= yr Then
                            If rd(1) = True Then
                                intmethod = "Straight Add-On"
                                _GenerateCustom_Amortization(intmethod, rd(5) * 12)
                            End If
                            If rd(2) = True Then
                                intmethod = "Straight Deducted"
                                _GenerateCustom_Amortization(intmethod, rd(5) * 12)
                            End If
                            If rd(3) = True Then
                                intmethod = "Diminishing"
                                _GenerateCustom_Amortization(intmethod, rd(5) * 12)
                            End If
                            ctr += 1
                        Else
                            Exit Sub
                        End If
                    End If
                End If
            End While
        Catch ex As Exception
            Throw
        End Try
    End Sub
    'then apply per year and get balance
    Dim cust_principal As Decimal
    Dim cust_term As Integer
    Dim cIndex As Integer = 0
    Private Sub _GenerateCustom_Amortization(ByVal intmethod As String, ByVal interest As Decimal)
        Try
            Exec_Custom_Amortization(cust_principal, interest, lblTerms.Text, txtAmorDateStart.Text, intmethod)
        Catch ex As Exception
            Throw
        End Try
    End Sub
    '#Remarks divide terms into no of years
    '#Add Pay No : 0 and Balance
    '######################################
    Private Sub Exec_Custom_Amortization(ByVal LoanAmt As String, ByVal interest As Decimal, ByVal terms As String, ByVal datestart As String, ByVal mode As String)
        Dim ctr As Integer = 0
        Dim noofpayment As Integer = cust_term
        Dim interestrate As Decimal
        Select Case frmLoan_ApplicationMain.cboModeofPayment.Text
            Case "DAILY"
                interestrate = CDec(interest / 360)
            Case "WEEKLY"
                interestrate = CDec(interest / 48)
            Case "SEMI-MONTHLY"
                interestrate = CDec(interest / 24)
            Case "MONTHLY"
                interestrate = CDec(interest / 12)
            Case "QUARTERLY"
                interestrate = CDec(interest / 3)
            Case "ANNUALLY"
                interestrate = CDec(interest / 1)
            Case "LUMP SUM"
                interestrate = CDec(interest)
        End Select

        Select Case mode
            Case "Straight Add-On"
                Try
                    Dim ds As SqlDataReader
                    ds = SqlHelper.ExecuteReader(gcon.cnstring, "Calculate_Amortization_Schedule_Straight_AddOn",
                                                   New SqlParameter("@PrincipalAmount", LoanAmt),
                                                   New SqlParameter("@InterestRate", interestrate),
                                                   New SqlParameter("@Terms", noofpayment),
                                                   New SqlParameter("@DateGranted", txtAmorDateStart.Text),
                                                   New SqlParameter("@FirstPayment", dtFirstPayment.Value),
                                                   New SqlParameter("@xmode", payMode()))
                    dgvList.Columns(0).Width = 50
                    With dgvList
                        While ds.Read
                            If cIndex = 0 Then
                                If ds(0) = 0 Then
                                    .Rows.Add()
                                    .Rows(cIndex).Cells(1).Value = cIndex
                                    .Rows(cIndex).Cells(2).Value = ds(1)
                                    .Rows(cIndex).Cells(3).Value = "0.00"
                                    .Rows(cIndex).Cells(4).Value = "0.00"
                                    .Rows(cIndex).Cells(5).Value = "0.00"
                                    .Rows(cIndex).Cells(6).Value = "0.00"
                                    .Rows(cIndex).Cells(7).Value = ds(6)
                                    cIndex += 1
                                End If
                            End If
                            If ds(0) = 13 Then
                                cust_principal = ds(6)
                                cust_term -= 12
                                '_IntMethod(cust_term)
                                Exit Sub
                            End If
                            If ds(0) <> 0 Then
                                .Rows.Add()
                                .Rows(cIndex).Cells(1).Value = cIndex
                                .Rows(cIndex).Cells(2).Value = ds(1)
                                .Rows(cIndex).Cells(3).Value = ds(2)
                                .Rows(cIndex).Cells(4).Value = ds(3)
                                .Rows(cIndex).Cells(5).Value = ds(4)
                                .Rows(cIndex).Cells(6).Value = ds(5)
                                .Rows(cIndex).Cells(7).Value = ds(6)
                                cIndex += 1
                            End If
                        End While
                    End With
                Catch ex As Exception
                End Try
            Case "Straight Deducted"
                Try
                    Dim ds As SqlDataReader
                    ds = SqlHelper.ExecuteReader(gcon.cnstring, "Calculate_Amortization_Schedule_Straight_Deducted",
                                                   New SqlParameter("@PrincipalAmount", LoanAmt),
                                                   New SqlParameter("@InterestRate", interestrate),
                                                   New SqlParameter("@Terms", noofpayment),
                                                   New SqlParameter("@DateGranted", txtAmorDateStart.Text),
                                                   New SqlParameter("@FirstPayment", dtFirstPayment.Value),
                                                   New SqlParameter("@xmode", payMode()))
                    dgvList.Columns(0).Width = 50
                    With dgvList
                        While ds.Read
                            If cIndex = 0 Then
                                If ds(0) = 0 Then
                                    .Rows.Add()
                                    .Rows(cIndex).Cells(1).Value = cIndex
                                    .Rows(cIndex).Cells(2).Value = ds(1)
                                    .Rows(cIndex).Cells(3).Value = "0.00"
                                    .Rows(cIndex).Cells(4).Value = "0.00"
                                    .Rows(cIndex).Cells(5).Value = "0.00"
                                    .Rows(cIndex).Cells(6).Value = "0.00"
                                    .Rows(cIndex).Cells(7).Value = ds(6)
                                    cIndex += 1
                                End If
                            End If
                            If ds(0) = 13 Then
                                cust_principal = ds(6)
                                cust_term -= 12
                                '_IntMethod(cust_term)
                                Exit Sub
                            End If
                            If ds(0) <> 0 Then
                                .Rows.Add()
                                .Rows(cIndex).Cells(1).Value = cIndex
                                .Rows(cIndex).Cells(2).Value = ds(1)
                                .Rows(cIndex).Cells(3).Value = ds(2)
                                .Rows(cIndex).Cells(4).Value = ds(3)
                                .Rows(cIndex).Cells(5).Value = ds(4)
                                .Rows(cIndex).Cells(6).Value = ds(5)
                                .Rows(cIndex).Cells(7).Value = ds(6)
                                cIndex += 1
                            End If
                        End While
                    End With
                Catch ex As Exception
                End Try
            Case "Diminishing Declining"
                Try
                    Dim ds As SqlDataReader
                    ds = SqlHelper.ExecuteReader(gcon.cnstring, "Calculate_Amortization_Schedule_DiminishingDeclining",
                                                   New SqlParameter("@PrincipalAmount", LoanAmt),
                                                   New SqlParameter("@InterestRate", interestrate),
                                                   New SqlParameter("@Terms", noofpayment),
                                                   New SqlParameter("@DateGranted", txtAmorDateStart.Text),
                                                   New SqlParameter("@FirstPayment", dtFirstPayment.Value),
                                                   New SqlParameter("@xmode", payMode()))
                    dgvList.Columns(0).Width = 50
                    With dgvList
                        While ds.Read
                            If cIndex = 0 Then
                                If ds(0) = 0 Then
                                    .Rows.Add()
                                    .Rows(cIndex).Cells(1).Value = cIndex
                                    .Rows(cIndex).Cells(2).Value = ds(1)
                                    .Rows(cIndex).Cells(3).Value = "0.00"
                                    .Rows(cIndex).Cells(4).Value = "0.00"
                                    .Rows(cIndex).Cells(5).Value = "0.00"
                                    .Rows(cIndex).Cells(6).Value = "0.00"
                                    .Rows(cIndex).Cells(7).Value = ds(6)
                                    cIndex += 1
                                End If
                            End If
                            If ds(0) = 13 Then
                                cust_principal = ds(6)
                                cust_term -= 12
                                '_IntMethod(cust_term)
                                Exit Sub
                            End If
                            If ds(0) <> 0 Then
                                .Rows.Add()
                                .Rows(cIndex).Cells(1).Value = cIndex
                                .Rows(cIndex).Cells(2).Value = ds(1)
                                .Rows(cIndex).Cells(3).Value = ds(2)
                                .Rows(cIndex).Cells(4).Value = ds(3)
                                .Rows(cIndex).Cells(5).Value = ds(4)
                                .Rows(cIndex).Cells(6).Value = ds(5)
                                .Rows(cIndex).Cells(7).Value = ds(6)
                                cIndex += 1
                            End If
                        End While
                    End With
                Catch ex As Exception
                End Try
            Case "Diminishing"
                Try
                    Dim ds As SqlDataReader
                    ds = SqlHelper.ExecuteReader(gcon.cnstring, "Calculate_Amortization_Schedule_Diminishing",
                                                   New SqlParameter("@PrincipalAmount", LoanAmt),
                                                   New SqlParameter("@InterestRate", interestrate),
                                                   New SqlParameter("@Terms", noofpayment),
                                                   New SqlParameter("@DateGranted", txtAmorDateStart.Text),
                                                   New SqlParameter("@FirstPayment", dtFirstPayment.Value),
                                                   New SqlParameter("@xmode", payMode()))
                    dgvList.Columns(0).Width = 50
                    With dgvList
                        While ds.Read
                            If cIndex = 0 Then
                                If ds(0) = 0 Then
                                    .Rows.Add()
                                    .Rows(cIndex).Cells(1).Value = cIndex
                                    .Rows(cIndex).Cells(2).Value = ds(1)
                                    .Rows(cIndex).Cells(3).Value = "0.00"
                                    .Rows(cIndex).Cells(4).Value = "0.00"
                                    .Rows(cIndex).Cells(5).Value = "0.00"
                                    .Rows(cIndex).Cells(6).Value = "0.00"
                                    .Rows(cIndex).Cells(7).Value = ds(6)
                                    cIndex += 1
                                End If
                            End If
                            If ds(0) = 13 Then
                                cust_principal = ds(6)
                                cust_term -= 12
                                '_IntMethod(cust_term)
                                Exit Sub
                            End If
                            If ds(0) <> 0 Then
                                .Rows.Add()
                                .Rows(cIndex).Cells(1).Value = cIndex
                                .Rows(cIndex).Cells(2).Value = ds(1)
                                .Rows(cIndex).Cells(3).Value = ds(2)
                                .Rows(cIndex).Cells(4).Value = ds(3)
                                .Rows(cIndex).Cells(5).Value = ds(4)
                                .Rows(cIndex).Cells(6).Value = ds(5)
                                .Rows(cIndex).Cells(7).Value = ds(6)
                                cIndex += 1
                            End If
                        End While
                    End With
                Catch ex As Exception
                End Try
        End Select

        GetAmortizationTotals()
    End Sub

#End Region

#Region "generate amort col"
    Private Sub _Gen_AmortCol()
        Try
            dgvList.DataSource = Nothing
            cIndex = 0
            dgvList.Rows.Clear()
            dgvList.Columns.Clear()
            GenerateButton_forDeduct()
            Dim payno As New DataGridViewTextBoxColumn
            Dim dt As New DataGridViewTextBoxColumn
            Dim prin As New DataGridViewTextBoxColumn
            Dim inter As New DataGridViewTextBoxColumn
            Dim serv As New DataGridViewTextBoxColumn
            Dim amort As New DataGridViewTextBoxColumn
            Dim balnc As New DataGridViewTextBoxColumn
            With payno
                .Name = "payno"
                .HeaderText = "Pay No."
                .Width = "50"
            End With
            With dt
                .Name = "dt"
                .HeaderText = "Date"
            End With
            With prin
                .Name = "prin"
                .HeaderText = "Principal"
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With
            With inter
                .Name = "inter"
                .HeaderText = "Interest"
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With
            With serv
                .Name = "serv"
                .HeaderText = "Service Fee"
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With
            With amort
                .Name = "amort"
                .HeaderText = "Amortization"
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With
            With balnc
                .Name = "balnc"
                .HeaderText = "Balance"
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With
            With dgvList
                .Columns.Add(payno)
                .Columns.Add(dt)
                .Columns.Add(prin)
                .Columns.Add(inter)
                .Columns.Add(serv)
                .Columns.Add(amort)
                .Columns.Add(balnc)
            End With
        Catch ex As Exception
        End Try
    End Sub
#End Region

    Private Sub ModeOfPayment_Saved()
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, "_ModeOfPayment_InsertUpdate",
                                       New SqlParameter("@fnLoanNo", frmLoan_ApplicationMain.txtLoanNo.Text),
                                       New SqlParameter("@fcMode", frmLoan_ApplicationMain.cboModeofPayment.Text))
        Catch ex As Exception
            Throw
        End Try
    End Sub

#Region "Generate amort using custom FORMULA"

    Private Sub _Exec_Amort_Formula(ByVal LoanAmt As String, ByVal interest As Decimal, ByVal terms As String, ByVal datestart As String, ByVal mode As String)
        Dim noofpayment As Integer = CInt(frmLoan_ApplicationMain.txtnoofpayment.Text)
        Dim interestrate As Decimal = interest
        'Select Case frmLoan_ApplicationMain.cboModeofPayment.Text
        '    Case "DAILY"
        '        interestrate = CDec(interest / 360)
        '    Case "WEEKLY"
        '        interestrate = CDec(interest / 48)
        '    Case "SEMI-MONTHLY"
        '        interestrate = CDec(interest / 24)
        '    Case "MONTHLY"
        '        interestrate = CDec(interest / 12)
        '    Case "QUARTERLY"
        '        interestrate = CDec(interest / 3)
        '    Case "ANNUALLY"
        '        interestrate = CDec(interest / 1)
        '    Case "LUMP SUM"
        '        interestrate = CDec(interest)
        'End Select

        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(gcon.cnstring, frmLoan_ApplicationMain.xSelectedIntMethod,
                                           New SqlParameter("@PrincipalAmount", LoanAmt),
                                           New SqlParameter("@InterestRate", interestrate),
                                           New SqlParameter("@Terms", noofpayment),
                                           New SqlParameter("@DateGranted", txtAmorDateStart.Text),
                                           New SqlParameter("@FirstPayment", dtFirstPayment.Value),
                                           New SqlParameter("@xmode", payMode()))
            dgvList.DataSource = ds.Tables(0)
            dgvList.Columns(0).Width = 50
        Catch ex As Exception

        End Try
    End Sub

#End Region

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        frmAmortOtherPayment.mmode = frmLoan_ApplicationMain.cboModeofPayment.Text
        frmAmortOtherPayment.noofPayment = frmLoan_ApplicationMain.txtnoofpayment.Text
        frmAmortOtherPayment.StartPosition = FormStartPosition.CenterScreen
        frmAmortOtherPayment.ShowDialog()
    End Sub

#Region "Adjust some points in amort. - Vince 1-6-14"

    Private Sub _NormalizeVal_GotowithEgg()
        Try
            'Dim n_Prin As Decimal = 0
            Dim P_Diff As Decimal = 0
            Dim I_Diff As Decimal = 0

            Dim Int_Rate As Decimal = 0 'CDec(txtAnnulInterest.Text) / 100
            Select Case frmLoan_ApplicationMain.cboModeofPayment.Text
                Case "DAILY"
                    Int_Rate = CDec(txtAnnulInterest.Text / 360)
                Case "WEEKLY"
                    Int_Rate = CDec(txtAnnulInterest.Text / 48)
                Case "SEMI-MONTHLY"
                    Int_Rate = CDec(txtAnnulInterest.Text / 24)
                Case "MONTHLY"
                    Int_Rate = CDec(txtAnnulInterest.Text / 12)
                Case "QUARTERLY"
                    Int_Rate = CDec(txtAnnulInterest.Text / 3)
                Case "ANNUALLY"
                    Int_Rate = CDec(txtAnnulInterest.Text / 1)
                Case "LUMP SUM"
                    Int_Rate = CDec(txtAnnulInterest.Text)
            End Select
            Int_Rate = CDec(Int_Rate) / 100

            Dim n_Int As Decimal = (CDec(lblPrincipal.Text) * Int_Rate) * CDec(frmLoan_ApplicationMain.txtnoofpayment.Text)

            If frmLoan_ApplicationMain.xIntMode = "Standard" Then
                If frmLoan_ApplicationMain.xSelectedIntMethod <> "Ascending/Declining" Then
                    If frmLoan_ApplicationMain.xSelectedIntMethod = "Diminishing" Then
                        'Principal --------------------------------------
                        If txtTotPrincipal.Text <> lblPrincipal.Text Then
                            If CDec(txtTotPrincipal.Text) > CDec(lblPrincipal.Text) Then
                                P_Diff = CDec(txtTotPrincipal.Text) - CDec(lblPrincipal.Text)
                                dgvList.Item(3, CInt(frmLoan_ApplicationMain.txtnoofpayment.Text)).Value = Format(CDec(dgvList.Item(3, CInt(frmLoan_ApplicationMain.txtnoofpayment.Text)).Value - P_Diff), "##,##0.00")
                            End If
                            If CDec(lblPrincipal.Text) > CDec(txtTotPrincipal.Text) Then
                                P_Diff = CDec(lblPrincipal.Text) - CDec(txtTotPrincipal.Text)
                                dgvList.Item(3, CInt(frmLoan_ApplicationMain.txtnoofpayment.Text)).Value = Format(CDec(dgvList.Item(3, CInt(frmLoan_ApplicationMain.txtnoofpayment.Text)).Value + P_Diff), "##,##0.00")
                            End If
                        End If
                        'Interest ----------------------------------------
                    Else
                        'Principal --------------------------------------
                        If txtTotPrincipal.Text <> lblPrincipal.Text Then
                            If CDec(txtTotPrincipal.Text) > CDec(lblPrincipal.Text) Then
                                P_Diff = CDec(txtTotPrincipal.Text) - CDec(lblPrincipal.Text)
                                dgvList.Item(3, CInt(frmLoan_ApplicationMain.txtnoofpayment.Text)).Value = Format(CDec(dgvList.Item(3, CInt(frmLoan_ApplicationMain.txtnoofpayment.Text)).Value - P_Diff), "##,##0.00")
                            End If
                            If CDec(lblPrincipal.Text) > CDec(txtTotPrincipal.Text) Then
                                P_Diff = CDec(lblPrincipal.Text) - CDec(txtTotPrincipal.Text)
                                dgvList.Item(3, CInt(frmLoan_ApplicationMain.txtnoofpayment.Text)).Value = Format(CDec(dgvList.Item(3, CInt(frmLoan_ApplicationMain.txtnoofpayment.Text)).Value + P_Diff), "##,##0.00")
                            End If
                        End If
                        'Interest ------------------------------------------
                        If txtTotInterest.Text <> n_Int Then
                            If CDec(txtTotInterest.Text) > n_Int Then
                                I_Diff = CDec(txtTotInterest.Text) - n_Int
                                dgvList.Item(4, CInt(frmLoan_ApplicationMain.txtnoofpayment.Text)).Value = Format(CDec(dgvList.Item(4, CInt(frmLoan_ApplicationMain.txtnoofpayment.Text)).Value - I_Diff), "##,##0.00")
                            End If
                            If n_Int > CDec(txtTotInterest.Text) Then
                                I_Diff = n_Int - CDec(txtTotInterest.Text)
                                dgvList.Item(4, CInt(frmLoan_ApplicationMain.txtnoofpayment.Text)).Value = Format(CDec(dgvList.Item(4, CInt(frmLoan_ApplicationMain.txtnoofpayment.Text)).Value + I_Diff), "##,##0.00")
                            End If
                        End If
                    End If
                End If
            End If
            _Retotal_Amort()
            GetAmortizationTotals()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub _Retotal_Amort()
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList
                    If .Rows(i).Cells(1).Value <> 0 Then
                        .Rows(i).Cells(6).Value = Format(CDec(.Rows(i).Cells(3).Value) + CDec(.Rows(i).Cells(4).Value) + CDec(.Rows(i).Cells(5).Value), "##,##0.00")
                    End If
                End With
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

#End Region

#Region "Auto generate Amortization Columns 3/9/2015"

    Private Sub _GetAmortizationCols()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, "_Loans_GetAmortizationColumns")
        If rd.HasRows Then
            While rd.Read
                dgvList.Columns.Add(rd(0), rd(0))
                dgvList.Columns(rd(0).ToString).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End While
            _Compute_NewColumns()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub _Compute_NewColumns()
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, "_Loans_GetAmortizationDefaultVal",
                                                               New SqlParameter("@EmployeeNo", txtEmpNo.Text))

            If rd.HasRows Then
                While rd.Read
                    For i As Integer = 0 To dgvList.RowCount - 1
                        With dgvList
                            .Item(CStr(rd(0)), i).Value = Format(CDec(rd(1) / CDec(frmLoan_ApplicationMain.txtnoofpayment.Text)), "##,##0.00")
                        End With
                    Next
                End While
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

#End Region
End Class