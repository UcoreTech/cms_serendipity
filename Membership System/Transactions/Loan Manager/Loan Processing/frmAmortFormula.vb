﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmAmortFormula

    Private Sub frmAmortFormula_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _PrepColFormula()
        _AddFormula()
    End Sub

    Private Sub _PrepColFormula()
        dgvFormula.Columns.Clear()
        dgvFormula.Rows.Clear()
        Dim chk As New DataGridViewCheckBoxColumn
        Dim desc As New DataGridViewTextBoxColumn
        With chk
            .Name = "chk"
            .HeaderText = "Select"
            .Width = 50
        End With
        With desc
            .Name = "desc"
            .HeaderText = "Description"
            .Width = 300
            .ReadOnly = True
        End With
        With dgvFormula
            .Columns.Add(chk)
            .Columns.Add(desc)
        End With
    End Sub

    Private Sub _AddFormula()
        With dgvFormula
            .Rows.Add()
            .Item("desc", 0).Value = "_Formula_loan_Computation_V5"
            .Rows.Add()
            .Item("desc", 1).Value = "_Formula_loan_v8_300k"
            .Rows.Add()
            .Item("desc", 2).Value = "_Formula_loan_300k_NEW"
        End With
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        ' frmLoan_ApplicationMain.rdFormula.Tag = dgvFormula.CurrentRow.Cells(1).Value.ToString
        frmEditor_Amortization.lblSelFormula.Text = dgvFormula.CurrentRow.Cells(1).Value.ToString
        Me.Close()
    End Sub
End Class