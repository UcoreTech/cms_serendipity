﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmBrowseCheck

    Private con As New Clsappconfiguration
    Public doctype As String

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub LoadCheck(ByVal search As String)
        dgvList.Columns.Clear()
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(con.cnstring, "_LoadCHV_2",
                                      New SqlParameter("@search", search))
        dgvList.DataSource = ds.Tables(0)
        dgvList.Columns(0).HeaderText = "Document No."
    End Sub
    Private Sub LoadCash(ByVal search As String)
        dgvList.Columns.Clear()
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(con.cnstring, "_Load_Cash_Voucher_No",
                                      New SqlParameter("@search", search))
        dgvList.DataSource = ds.Tables(0)
        dgvList.Columns(0).HeaderText = "Document No."
    End Sub
    Private Sub LoadJournal(ByVal search As String)
        dgvList.Columns.Clear()
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(con.cnstring, "_Load_Journal_Voucher_No",
                                      New SqlParameter("@search", search))
        dgvList.DataSource = ds.Tables(0)
        dgvList.Columns(0).HeaderText = "Document No."
    End Sub

    Private Sub frmBrowseCheck_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtSearch.Text = ""
        ActiveControl = txtSearch
        If doctype = "CHV" Then
            LoadCheck("")
        End If
        If doctype = "CV" Then
            LoadCash("")
        End If
        If doctype = "JV" Then
            LoadJournal("")
        End If
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        frmChkVoucherEntry.cbDocNum.Text = dgvList.SelectedRows(0).Cells(0).Value.ToString
        Me.Close()
    End Sub

    Private Sub dgvList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.DoubleClick
        frmChkVoucherEntry.cbDocNum.Text = dgvList.SelectedRows(0).Cells(0).Value.ToString
        Me.Close()
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        If doctype = "CHV" Then
            LoadCheck(txtSearch.Text)
        End If
        If doctype = "CV" Then
            LoadCash(txtSearch.Text)
        End If
        If doctype = "JV" Then
            LoadJournal(txtSearch.Text)
        End If
    End Sub

End Class