﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmComakerHistory

    Private con As New Clsappconfiguration
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frmComakerHistory_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Public Sub ViewCMHistory(ByVal idno As String,
                            ByVal docno As String)
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_Comaker_History_List",
                                           New SqlParameter("@fcEmployeeNo", idno),
                                           New SqlParameter("@fcDocNo", docno))
            dgvList.DataSource = ds.Tables(0)
            dgvList.Columns(2).DefaultCellStyle.Format = "##,##0.00"
        Catch ex As Exception
            Throw
        End Try
    End Sub

End Class