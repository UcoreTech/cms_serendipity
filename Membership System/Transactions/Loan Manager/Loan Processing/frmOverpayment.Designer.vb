﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOverpayment
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.grdOverpmt = New System.Windows.Forms.DataGridView()
        Me.chkRefund = New System.Windows.Forms.CheckBox()
        Me.chkCapShare = New System.Windows.Forms.CheckBox()
        Me.chkExistingLoan = New System.Windows.Forms.CheckBox()
        Me.chkPenalty = New System.Windows.Forms.CheckBox()
        Me.grdExistingLoan = New System.Windows.Forms.DataGridView()
        Me.btnCreditOverpmt = New System.Windows.Forms.Button()
        CType(Me.grdOverpmt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdExistingLoan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grdOverpmt
        '
        Me.grdOverpmt.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.grdOverpmt.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.grdOverpmt.BackgroundColor = System.Drawing.SystemColors.Window
        Me.grdOverpmt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdOverpmt.Location = New System.Drawing.Point(13, 13)
        Me.grdOverpmt.Name = "grdOverpmt"
        Me.grdOverpmt.RowHeadersVisible = False
        Me.grdOverpmt.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdOverpmt.Size = New System.Drawing.Size(748, 197)
        Me.grdOverpmt.TabIndex = 0
        '
        'chkRefund
        '
        Me.chkRefund.AutoSize = True
        Me.chkRefund.Enabled = False
        Me.chkRefund.Location = New System.Drawing.Point(13, 299)
        Me.chkRefund.Name = "chkRefund"
        Me.chkRefund.Size = New System.Drawing.Size(61, 17)
        Me.chkRefund.TabIndex = 10
        Me.chkRefund.Text = "Refund"
        Me.chkRefund.UseVisualStyleBackColor = True
        '
        'chkCapShare
        '
        Me.chkCapShare.AutoSize = True
        Me.chkCapShare.Enabled = False
        Me.chkCapShare.Location = New System.Drawing.Point(13, 253)
        Me.chkCapShare.Name = "chkCapShare"
        Me.chkCapShare.Size = New System.Drawing.Size(131, 17)
        Me.chkCapShare.TabIndex = 9
        Me.chkCapShare.Text = "Credit to Share Capital"
        Me.chkCapShare.UseVisualStyleBackColor = True
        '
        'chkExistingLoan
        '
        Me.chkExistingLoan.AutoSize = True
        Me.chkExistingLoan.Enabled = False
        Me.chkExistingLoan.Location = New System.Drawing.Point(13, 230)
        Me.chkExistingLoan.Name = "chkExistingLoan"
        Me.chkExistingLoan.Size = New System.Drawing.Size(131, 17)
        Me.chkExistingLoan.TabIndex = 8
        Me.chkExistingLoan.Text = "Credit to Existing Loan"
        Me.chkExistingLoan.UseVisualStyleBackColor = True
        '
        'chkPenalty
        '
        Me.chkPenalty.AutoSize = True
        Me.chkPenalty.Enabled = False
        Me.chkPenalty.Location = New System.Drawing.Point(13, 276)
        Me.chkPenalty.Name = "chkPenalty"
        Me.chkPenalty.Size = New System.Drawing.Size(103, 17)
        Me.chkPenalty.TabIndex = 11
        Me.chkPenalty.Text = "Credit to Penalty"
        Me.chkPenalty.UseVisualStyleBackColor = True
        '
        'grdExistingLoan
        '
        Me.grdExistingLoan.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.grdExistingLoan.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.grdExistingLoan.BackgroundColor = System.Drawing.SystemColors.Window
        Me.grdExistingLoan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdExistingLoan.Enabled = False
        Me.grdExistingLoan.Location = New System.Drawing.Point(176, 216)
        Me.grdExistingLoan.Name = "grdExistingLoan"
        Me.grdExistingLoan.ReadOnly = True
        Me.grdExistingLoan.RowHeadersVisible = False
        Me.grdExistingLoan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdExistingLoan.Size = New System.Drawing.Size(585, 163)
        Me.grdExistingLoan.TabIndex = 12
        '
        'btnCreditOverpmt
        '
        Me.btnCreditOverpmt.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCreditOverpmt.Enabled = False
        Me.btnCreditOverpmt.Location = New System.Drawing.Point(13, 347)
        Me.btnCreditOverpmt.Name = "btnCreditOverpmt"
        Me.btnCreditOverpmt.Size = New System.Drawing.Size(143, 32)
        Me.btnCreditOverpmt.TabIndex = 13
        Me.btnCreditOverpmt.Text = "Process Over payment"
        Me.btnCreditOverpmt.UseVisualStyleBackColor = True
        '
        'frmOverpayment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(773, 391)
        Me.Controls.Add(Me.btnCreditOverpmt)
        Me.Controls.Add(Me.grdExistingLoan)
        Me.Controls.Add(Me.chkPenalty)
        Me.Controls.Add(Me.chkRefund)
        Me.Controls.Add(Me.chkCapShare)
        Me.Controls.Add(Me.chkExistingLoan)
        Me.Controls.Add(Me.grdOverpmt)
        Me.Name = "frmOverpayment"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmOverpayment"
        Me.TopMost = True
        CType(Me.grdOverpmt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdExistingLoan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grdOverpmt As System.Windows.Forms.DataGridView
    Friend WithEvents chkRefund As System.Windows.Forms.CheckBox
    Friend WithEvents chkCapShare As System.Windows.Forms.CheckBox
    Friend WithEvents chkExistingLoan As System.Windows.Forms.CheckBox
    Friend WithEvents chkPenalty As System.Windows.Forms.CheckBox
    Friend WithEvents grdExistingLoan As System.Windows.Forms.DataGridView
    Friend WithEvents btnCreditOverpmt As System.Windows.Forms.Button
End Class
