<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoan_ChangeAmortization
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtAmortPerMonth = New System.Windows.Forms.TextBox()
        Me.txtSalaryRate = New System.Windows.Forms.TextBox()
        Me.txtAmortPerPayday = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'txtAmortPerMonth
        '
        Me.txtAmortPerMonth.Location = New System.Drawing.Point(45, 12)
        Me.txtAmortPerMonth.Name = "txtAmortPerMonth"
        Me.txtAmortPerMonth.Size = New System.Drawing.Size(89, 20)
        Me.txtAmortPerMonth.TabIndex = 0
        '
        'txtSalaryRate
        '
        Me.txtSalaryRate.Location = New System.Drawing.Point(140, 12)
        Me.txtSalaryRate.Name = "txtSalaryRate"
        Me.txtSalaryRate.Size = New System.Drawing.Size(84, 20)
        Me.txtSalaryRate.TabIndex = 1
        '
        'txtAmortPerPayday
        '
        Me.txtAmortPerPayday.Location = New System.Drawing.Point(32, 38)
        Me.txtAmortPerPayday.Name = "txtAmortPerPayday"
        Me.txtAmortPerPayday.Size = New System.Drawing.Size(205, 20)
        Me.txtAmortPerPayday.TabIndex = 2
        '
        'frmLoan_ChangeAmortization
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(263, 82)
        Me.Controls.Add(Me.txtAmortPerPayday)
        Me.Controls.Add(Me.txtSalaryRate)
        Me.Controls.Add(Me.txtAmortPerMonth)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmLoan_ChangeAmortization"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Change Amortization"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtAmortPerMonth As System.Windows.Forms.TextBox
    Friend WithEvents txtSalaryRate As System.Windows.Forms.TextBox
    Friend WithEvents txtAmortPerPayday As System.Windows.Forms.TextBox
End Class
