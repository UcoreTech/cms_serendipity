Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class FrmLoan_ForwardRequirements
#Region "Public Variables"
    Public Empinfo As String
    Public loantype As String
    Public LoanNo As String
#End Region
#Region "Public Property"
    Public Property getEmpinfo() As String
        Get
            Return Empinfo
        End Get
        Set(ByVal value As String)
            Empinfo = value
        End Set
    End Property
    Public Property getloantype() As String
        Get
            Return loantype
        End Get
        Set(ByVal value As String)
            loantype = value
        End Set
    End Property
    Public Property getloanNo() As String
        Get
            Return LoanNo
        End Get
        Set(ByVal value As String)
            LoanNo = value
        End Set
    End Property
#End Region
    Private Sub FrmRequirementsforApproval_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call ViewSelectedRequirements(LoanNo)
    End Sub
#Region "View Selected Requirements Per Member"
    Private Sub ViewSelectedRequirements(ByVal loannumber As String)
        Dim gcon As New Clsappconfiguration
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("MSS_Loans_Retrieve_LoanRequirements_PerMember", gcon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        gcon.sqlconn.Open()
        With cmd.Parameters
            .Add("@loanNo", SqlDbType.VarChar).Value = loannumber
        End With
        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "CIMS_t_MemberLoans_Reqts")

            Me.grdRequirements.DataSource = ds
            Me.grdRequirements.DataMember = "CIMS_t_MemberLoans_Reqts"
            Me.grdRequirements.Columns(0).Visible = False
            Me.grdRequirements.Columns(1).Width = 300
            Me.grdRequirements.Columns(2).Visible = False

            cmd.ExecuteNonQuery()
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "View Requirements for Approval")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Update for if approved"
    Private Sub TaggedCompleted(ByVal guid As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "MSS_Loans_AddEdit_MemberLoanRequirements1", _
                                      New SqlParameter("@pk_LoanRequirements", guid), _
                                      New SqlParameter("@fbApproved", 1))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Update Member Requirements")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Update Loan Status to Completed"
    Private Sub UpdateloanStatus(ByVal loanNo As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "MSS_Loans_ChangeLoanStatus", _
                                      New SqlParameter("@loanNo", loanNo), _
                                      New SqlParameter("@status", "Completed"))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "UpdateloanStatus")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Me.Close()
    End Sub

    Private Sub cmdUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        'MSS_Loans_ChangeLoanStatus //update status to completed

        getEmpinfo() = frmLoan_Processing.GrdLoanSummary.CurrentRow.Cells(0).Value

        Dim i As Integer
        Dim msg As New DialogResult
        msg = MessageBox.Show("Are you sure you want to forward this to the Approver for approval", "Forward", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If msg = Windows.Forms.DialogResult.Yes Then
            For i = 0 To Me.grdRequirements.Rows.Count - 1
                Call TaggedCompleted(Me.grdRequirements.Item(0, i).Value)
            Next
            Call UpdateloanStatus(LoanNo)
            Call frmLoan_Processing.ViewLoanSummary(Empinfo)
            Me.Close()
        Else

        End If
    End Sub
End Class