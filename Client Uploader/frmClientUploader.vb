﻿Imports System.Data.SqlClient
Imports Microsoft.VisualBasic.FileSystem
Imports System.Data
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data

'Module : Client Uploader
'Programmer : Vincent Nacar
'Minimum Upload Speed : 0.1 Sec
'Maximum Upload Speed : 60 Sec
'-------------------------------------------------------

Public Class frmClientUploader
    Dim filenym As String
    Dim fpath As String
    Dim itemCount As Long
    Dim con As New Clsappconfiguration

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Try
            OpenFileDialog1.ShowDialog()
            filenym = OpenFileDialog1.FileName
            fpath = Path.GetFullPath(filenym)
            txtPath.Text = fpath

            AddSelectButton()
            Import()
            Check_ErrorRows()
        Catch ex As Exception
            ' frmMsgBox.txtMessage.Text = "Error :" + ex.ToString
            frmMsgBox.txtMessage.Text = "User Cancelled!"
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub AddSelectButton()
        Dim btnselect As New DataGridViewCheckBoxColumn
        With btnselect
            .Name = "btnSelect"
            .HeaderText = "OK"
        End With
        dgvList.Columns.Add(btnselect)
    End Sub

    Private Sub Check_ErrorRows() 'Added 8/9/14 Vince
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                If dgvList.Item("BirthDate", i).Value Is DBNull.Value Or dgvList.Item("DateHired", i).Value Is DBNull.Value Or dgvList.Item("BODApproval", i).Value Is DBNull.Value Or dgvList.Item("MembershipDate", i).Value Is DBNull.Value Then
                    dgvList.Rows(i).DefaultCellStyle.BackColor = Color.Red
                    dgvList.Rows(i).Cells(0).Value = False
                Else
                    dgvList.Rows(i).DefaultCellStyle.BackColor = Color.SkyBlue
                    dgvList.Rows(i).Cells(0).Value = True
                End If
            Next
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Import()
        Dim MyConnection As System.Data.OleDb.OleDbConnection
        Dim DtSet As System.Data.DataSet
        Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + txtPath.Text.ToString + "';Extended Properties=Excel 8.0;")
        MyCommand = New System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", MyConnection)
        MyCommand.TableMappings.Add("Table", "Net-informations.com")
        DtSet = New System.Data.DataSet
        MyCommand.Fill(DtSet)
        dgvList.DataSource = DtSet.Tables(0)
        MyConnection.Close()
    End Sub

    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        itemCount = 0
        Try
            For xCtr As Integer = 0 To dgvList.RowCount - 1
                With dgvList
                    Dim empno As String = .Item("ClientNo", xCtr).Value.ToString
                    Dim lastname As String = .Item("LastName", xCtr).Value.ToString
                    Dim firstname As String = .Item("FirstName", xCtr).Value.ToString
                    Dim middlename As String = .Item("MiddleName", xCtr).Value.ToString
                    Dim CivilStatus As String = .Item("CivilStatus", xCtr).Value.ToString
                    Dim Gender As String = .Item("Gender", xCtr).Value.ToString
                    Dim Address As String = .Item("Address", xCtr).Value.ToString
                    Dim BirthDate As Date = .Item("BirthDate", xCtr).Value
                    Dim ContactNo As String = .Item("ContactNo", xCtr).Value.ToString
                    Dim EmailAddress As String = .Item("EmailAddress", xCtr).Value.ToString
                    Dim group As String = .Item("Group", xCtr).Value.ToString
                    Dim subgroup As String = .Item("SubGroup", xCtr).Value.ToString 'Added 8/5/2014 Vince
                    Dim Category As String = .Item("Category", xCtr).Value.ToString
                    Dim type As String = .Item("Type", xCtr).Value.ToString
                    Dim status As String = .Item("Status", xCtr).Value.ToString
                    Dim rank As String = .Item("Rank", xCtr).Value.ToString
                    Dim dateHired As Date = .Item("DateHired", xCtr).Value 'Added 8/5/2014 Vince
                    'Added 7/21/14 by Vincent Nacar ###########################
                    Dim BODApproval As Date = .Item("BODApproval", xCtr).Value
                    Dim MembershipDate As Date = .Item("MembershipDate", xCtr).Value
                    Dim TIN As String = .Item("TIN", xCtr).Value.ToString
                    Dim SSS As String = .Item("SSS", xCtr).Value.ToString
                    Dim PHIC As String = .Item("PHIC", xCtr).Value.ToString
                    Dim HDMF As String = .Item("HDMF", xCtr).Value.ToString
                    Dim provincialAddress As String = .Item("ProvincialAddress", xCtr).Value.ToString 'Added 8/5/2014 Vince

                    SqlHelper.ExecuteNonQuery(con.cnstring, "Client_Data_Upload",
                                             New SqlParameter("@ClientNo", empno),
                                             New SqlParameter("@Lastname", lastname),
                                             New SqlParameter("@Firstname", firstname),
                                             New SqlParameter("@MiddleName", middlename),
                                             New SqlParameter("@CivilStatus", CivilStatus),
                                             New SqlParameter("@Gender", Gender),
                                             New SqlParameter("@Address", Address),
                                             New SqlParameter("@Birthdate", BirthDate),
                                             New SqlParameter("@ContactNo", ContactNo),
                                             New SqlParameter("@EmailAddress", EmailAddress),
                                             New SqlParameter("@fcGroup", group),
                                             New SqlParameter("@fcSubgroup", subgroup),
                                             New SqlParameter("@fcCategory", Category),
                                             New SqlParameter("@fcType", type),
                                             New SqlParameter("@fcStatus", status),
                                             New SqlParameter("@fcRank", rank),
                                             New SqlParameter("@Date", dateHired),
                                             New SqlParameter("@BoardApproval", BODApproval),
                                             New SqlParameter("@MembershipDate", MembershipDate),
                                             New SqlParameter("@TIN", TIN),
                                             New SqlParameter("@SSS", SSS),
                                             New SqlParameter("@PHIC", PHIC),
                                             New SqlParameter("@HDMF", HDMF),
                                             New SqlParameter("@ProvincialAddress", provincialAddress))
                End With

                itemCount += 1
            Next

            CleanDummyFiles()

        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Upload Failed in Client No:" + dgvList.Rows(itemCount).Cells(1).Value.ToString + " " + ex.Message
            frmMsgBox.ShowDialog()
            dgvList.Columns.Clear()
            CleanDummyFiles()
        End Try

        frmMsgBox.txtMessage.Text = "Upload Complete , Total No. of Item(s):" + itemCount.ToString
        frmMsgBox.ShowDialog()
    End Sub

    Private Sub frmClientUploader_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub CleanDummyFiles()
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Clean_ClientMember_Uselessfiles")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        dgvList.Columns.Clear()
    End Sub

End Class