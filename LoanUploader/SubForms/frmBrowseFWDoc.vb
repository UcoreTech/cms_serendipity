﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmBrowseFWDoc

    Private con As New Clsappconfiguration
    Private Sub frmBrowseFWDoc_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _SearchDoc("")
    End Sub
    Private Sub _SearchDoc(ByVal key As String)
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(con.cnstring, "_ForwardBalance_Select_Doc",
                                                          New SqlParameter("@key", key))
            dgvList.DataSource = ds.Tables(0)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub _GetSelected()
        Try
            frmLoanForwardBalanceUploader.txtDocNumber.Text = dgvList.SelectedRows(0).Cells(0).Value.ToString
            Me.Close()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        If e.KeyCode = Keys.Enter Then
            _GetSelected()
        End If
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        _SearchDoc(txtSearch.Text)
    End Sub

    Private Sub dgvList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.DoubleClick
        _GetSelected()
    End Sub

    Private Sub dgvList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvList.KeyDown
        If e.KeyCode = Keys.Enter Then
            _GetSelected()
        End If
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        _GetSelected()
    End Sub

End Class