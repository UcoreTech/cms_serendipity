﻿Imports System.Data.SqlClient
Imports Microsoft.VisualBasic.FileSystem
Imports System.Data
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.OleDb

'Module : Loan Forward Balance Uploader
'Programmer : Vincent Nacar
'Minimum Upload Speed : 0.1 Sec
'Maximum Upload Speed : 60 Sec
'-------------------------------------------------------

Public Class frmLoanForwardBalanceUploader
    Dim filenym As String
    Dim fpath As String
    Dim itemCount As Long
    Dim con As New Clsappconfiguration
    Dim cIndex As Integer

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Try
            OpenFileDialog1.ShowDialog()
            filenym = OpenFileDialog1.FileName
            fpath = Path.GetFullPath(filenym)
            txtPath.Text = fpath

            Import()
        Catch ex As Exception
            ' frmMsgBox.txtMessage.Text = "Error :" + ex.ToString
            frmMsgBox.txtMessage.Text = "User Cancelled!"
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub Import()
        Dim MyConnection As System.Data.OleDb.OleDbConnection
        MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + txtPath.Text.ToString + "';Extended Properties=Excel 8.0;")
        MyConnection.Open()
        Dim dbCommand As New OleDbCommand("select * from [Sheet1$]", MyConnection)
        Dim rd As OleDbDataReader
        rd = dbCommand.ExecuteReader
        _BootLoader()
        cIndex = 0
        ttot = 0
        While rd.Read
            With dgvList
                .Rows.Add()
                _GetClientInfo(rd(0).ToString)
                .Item("loanref", cIndex).Value = rd(0)
                .Item("forwardbalance", cIndex).Value = rd(1)
                cIndex += 1
            End With
        End While
        MyConnection.Close()
    End Sub
    Private Sub _GetClientInfo(ByVal loanno As String)
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_ForwardBalance_GetClientInfo",
                                                               New SqlParameter("@LoanNo", loanno))
            While rd.Read
                With dgvList
                    .Item("idno", cIndex).Value = rd(0)
                    .Item("cname", cIndex).Value = rd(1)
                    .Item("code", cIndex).Value = rd(2)
                    .Item("accttitle", cIndex).Value = rd(3)
                End With
            End While
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub _Search_ExistingFB(ByVal docnum As String)
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_ForwardBalance_Get_Details",
                                                               New SqlParameter("@DocNum", docnum))
            _BootLoader()
            cIndex = 0
            ttot = 0
            While rd.Read
                With dgvList
                    .Rows.Add()
                    _GetClientInfo(rd(0).ToString)
                    .Item("loanref", cIndex).Value = rd(0)
                    .Item("forwardbalance", cIndex).Value = rd(1)
                    dtDate.Value = CDate(rd(2))
                    txtParticulars.Text = rd(3).ToString
                    chkPosted.Checked = CBool(rd(4))
                    cIndex += 1
                End With
            End While
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If txtDocNumber.Text <> "" Then
            _SaveForward_Balance()
            UpdateDocNumber(txtDocNumber.Text, dtDate.Text)
            frmMsgBox.txtMessage.Text = "Save Success"
            frmMsgBox.ShowDialog()
        Else
            frmMsgBox.txtMessage.Text = "Select document number to continue."
            frmMsgBox.ShowDialog()
        End If
        'save forward balance
    End Sub

    Private Sub UpdateDocNumber(ByVal docnum As String, ByVal dt As String)
        SqlHelper.ExecuteNonQuery(con.cnstring, "_Update_DocNumber_Used",
                                   New SqlParameter("@DocNumber", docnum),
                                   New SqlParameter("@DateUsed", dt))
    End Sub

    Private Sub _Delete_DataFB(ByVal docnum As String)
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_ForwardBalance_Delete",
                                       New SqlParameter("@DocNumber", docnum))
            _BootLoader()
            txtPath.Text = ""
            txtParticulars.Text = ""
            txtTotals.Text = "0.00"
            MsgBox("Delete Success.", vbInformation, "...")
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub _SaveForward_Balance()
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList
                    SqlHelper.ExecuteNonQuery(con.cnstring, "_ForwardBalance_InsertUpdate",
                                               New SqlParameter("@fcLoanRef", .Item("loanref", i).Value.ToString),
                                               New SqlParameter("@fdForwardBalance", .Item("forwardbalance", i).Value.ToString),
                                               New SqlParameter("@fcDocNumber", txtDocNumber.Text),
                                               New SqlParameter("@dtDate", dtDate.Text),
                                               New SqlParameter("@fcParticulars", txtParticulars.Text),
                                               New SqlParameter("@fbPosted", chkPosted.Checked))
                End With
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
    'post and unpost
    Private Sub _Post_ForwardBalance()
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList
                    SqlHelper.ExecuteNonQuery(con.cnstring, "_Upload_LoanForwardBalance",
                                            New SqlParameter("@Date", dtDate.Text),
                                            New SqlParameter("@IDNo", .Item("idno", i).Value.ToString),
                                            New SqlParameter("@DocNumber", txtDocNumber.Text),
                                            New SqlParameter("@Particulars", txtParticulars.Text),
                                            New SqlParameter("@LoanNo", .Item("loanref", i).Value.ToString),
                                            New SqlParameter("@ForwardBalance", .Item("forwardbalance", i).Value))

                End With
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub _Unpost_ForwardBalance()
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList
                    SqlHelper.ExecuteNonQuery(con.cnstring, "_Unpost_ForwardBalance",
                                               New SqlParameter("@DocNumber", txtDocNumber.Text),
                                               New SqlParameter("@LoanNo", .Item("loanref", i).Value.ToString),
                                               New SqlParameter("@ForwardBalance", .Item("forwardbalance", i).Value))
                End With
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub frmLoanForwardBalanceUploader_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

#Region "prep col"
    Private Sub _BootLoader()
        dgvList.DataSource = Nothing
        dgvList.Rows.Clear()
        dgvList.Columns.Clear()
        Dim idno As New DataGridViewTextBoxColumn
        Dim cname As New DataGridViewTextBoxColumn
        Dim loanref As New DataGridViewTextBoxColumn
        Dim code As New DataGridViewTextBoxColumn
        Dim accttitle As New DataGridViewTextBoxColumn
        Dim forwardbalance As New DataGridViewTextBoxColumn
        With idno
            .Name = "idno"
            .HeaderText = "ID No."
            .ReadOnly = True
        End With
        With cname
            .Name = "cname"
            .HeaderText = "Name"
            .ReadOnly = True
        End With
        With loanref
            .Name = "loanref"
            .HeaderText = "Loan Ref."
            .ReadOnly = True
        End With
        With code
            .Name = "code"
            .HeaderText = "Code"
            .ReadOnly = True
        End With
        With accttitle
            .Name = "accttitle"
            .HeaderText = "Acct. Title"
            .ReadOnly = True
        End With
        With forwardbalance
            .Name = "forwardbalance"
            .HeaderText = "Forward Balance"
            .ReadOnly = False
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With dgvList
            .Columns.Add(idno)
            .Columns.Add(cname)
            .Columns.Add(loanref)
            .Columns.Add(code)
            .Columns.Add(accttitle)
            .Columns.Add(forwardbalance)
        End With
    End Sub
#End Region

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        _BootLoader()
        txtDocNumber.Text = ""
        txtTotals.Text = "0.00"
        txtPath.Text = ""
        txtParticulars.Text = ""
    End Sub

    Dim ttot As Decimal = 0
    Private Function _GetTotals(ByVal amt As Decimal) As String
        ttot = ttot + amt
        Return Format(ttot, "##,##0.00")
    End Function

    Private Sub dgvList_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvList.CellValueChanged
        ttot = 0
        For i As Integer = 0 To dgvList.RowCount - 1
            With dgvList
                txtTotals.Text = _GetTotals(CDec(.Item("forwardbalance", i).Value)).ToString
                .Item("forwardbalance", i).Value = Format(CDec(.Item("forwardbalance", i).Value), "##,##0.00")
            End With
        Next
    End Sub

    Private Sub btnBrowseDoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseDoc.Click
        frmBrowseFWDoc.StartPosition = FormStartPosition.CenterScreen
        frmBrowseFWDoc.ShowDialog()
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        frmSearchExistingFB.StartPosition = FormStartPosition.CenterScreen
        frmSearchExistingFB.ShowDialog()
    End Sub

    Private Sub txtDocNumber_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDocNumber.TextChanged
        _Search_ExistingFB(txtDocNumber.Text)
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If MsgBox("Are you sure?", vbYesNo + vbQuestion, "Delete") = vbYes Then
            _Delete_DataFB(txtDocNumber.Text)
        Else
            Exit Sub
        End If
    End Sub

    Private Sub btnAddLoan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddLoan.Click
        frmAddLoan.StartPosition = FormStartPosition.CenterScreen
        frmAddLoan.ShowDialog()
    End Sub

    Public Sub addLoan(ByVal loanno As String)
        With dgvList
            .Rows.Add()
            _GetClientInfo(loanno)
            .Item("loanref", cIndex).Value = loanno
            .Item("forwardbalance", cIndex).Value = "0.00"
            cIndex += 1
        End With
    End Sub

    Private Sub btnRemoveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveItem.Click
        Try
            dgvList.Rows.Remove(dgvList.CurrentRow)
            cIndex -= 1
        Catch ex As Exception
            cIndex = 0
        End Try
    End Sub

    Private Sub chkPosted_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPosted.Click
        If chkPosted.Checked = True Then
            If MsgBox("Are you sure you want to post this document?", vbYesNo, "Posting") = vbYes Then
                chkPosted.Checked = True
                'post
                _Post_ForwardBalance()
                Exit Sub
            Else
                chkPosted.Checked = False
            End If
        Else
            If MsgBox("Are you sure you want to unpost this document?", vbYesNo, "Unposting") = vbYes Then
                chkPosted.Checked = False
                'unpost
                _Unpost_ForwardBalance()
                Exit Sub
            Else
                chkPosted.Checked = True
            End If
        End If
    End Sub

End Class