﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmDocIssuer

    Private con As New Clsappconfiguration

    Private Sub frmDocIssuer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Function totalLoan_without_DocNum() As Integer
        Try
            Dim x As Integer
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(con.cnstring, "_CheckLoan_Without_DocumentIssued")
            While rd.Read
                x += 1
            End While
            Return x
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Private Sub btnCheck_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheck.Click
        lblchecked.Text = totalLoan_without_DocNum().ToString
    End Sub

    Private Sub btnIssued_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIssued.Click
        If IssuingDoc() Then
            Me.Text = "Success"
        End If
    End Sub

    Private Function IssuingDoc() As Boolean
        Try
            Dim docnumber As String
            Dim fxcokey As New Guid(txtKey.Text)
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(con.cnstring, "_CheckLoan_Without_DocumentIssued")
            Me.Text = "Loading....."
            While rd.Read
                docnumber = rd(0).ToString
                SqlHelper.ExecuteNonQuery(con.cnstring, "_IssuedDocumentNumber",
                                           New SqlParameter("@DocNumber", docnumber),
                                           New SqlParameter("@Date", dtDate.Value),
                                           New SqlParameter("@fkCoid", fxcokey))
            End While
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

End Class