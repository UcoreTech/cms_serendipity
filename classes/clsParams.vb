Public Class clsParams
    Private m_strParamName As String
    Private m_strParamValue As Object
    Private m_strParamType As Integer

    'Public Property Let ParamType(ByVal sValue As ParamDataType)
    '      m_strParamType = sValue
    '    End Property

    Public Property ParamType() As Integer
        Get
            Return m_strParamType
        End Get
        Set(ByVal value As Integer)
            m_strParamType = value
        End Set
    End Property


    'Public Property Let ParamName(ByVal sValue As String)
    '      m_strParamName = Trim(sValue)
    '    End Property

    Public Property ParamName() As String
        Get
            Return Trim(m_strParamName)
        End Get
        Set(ByVal value As String)
            m_strParamName = value
        End Set
    End Property

    'Public Property Let ParamValue(ByVal sValue As Variant)
    '      m_strParamValue = sValue
    '    End Property

    Public Property ParamValue() As Object
        Get
            Return m_strParamValue
        End Get
        Set(ByVal value As Object)
            m_strParamValue = value
        End Set
    End Property

End Class
