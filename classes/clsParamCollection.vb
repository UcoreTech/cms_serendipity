Public Class clsParamCollection
    Private cParamCol As New Collection

    Public Sub AddParameter(ByVal ParamName As String, ByVal ParamValue As Object, ByVal DataType As Integer)
        Dim cParam As clsParams
        cParam = New clsParams
        With cParam
            .ParamName = ParamName
            .ParamType = DataType
            .ParamValue = ParamValue
        End With
        cParamCol.Add(cParam)
        cParam = Nothing
    End Sub

    Public Function ParameterCount() As Integer
        Return cParamCol.Count
    End Function
       
    'ParameterCount = cParamCol.Count


    Public Sub ClearParameter()
        Dim nItem As Integer
        For nItem = 1 To cParamCol.Count
            cParamCol.Remove(1)
        Next
    End Sub

    Public Function ParameterGetValueName(ByVal ParamName As String) As Object
        Dim nItem As Integer
        For nItem = 1 To cParamCol.Count
            If UCase(cParamCol.Item(nItem).ParamName) = UCase(ParamName) Then
                Return cParamCol.Item(nItem).ParamValue
                Exit For
            End If
        Next
    End Function

    Public Function ParameterGetValueIndex(ByVal Index As Integer) As Object
        ParameterGetValueIndex = cParamCol.Item(Index).ParamValue
    End Function

    Public Sub ParameterSetValue(ByVal ParamName As String, ByVal sValue As Object)
        Dim nItem As Integer
        For nItem = 1 To cParamCol.Count
            If UCase(cParamCol.Item(nItem).ParamName) = UCase(ParamName) Then
                cParamCol.Item(nItem).ParamValue = sValue
                Exit For
            End If
        Next
    End Sub

    Public Function ParameterGetName(ByVal Index As Integer) As String
        If cParamCol.Count >= Index Then
            Return cParamCol.Item(Index).ParamName
        End If
    End Function

    Public Function ParameterGetType(ByVal Index As Integer) As Integer
        If cParamCol.Count >= Index Then
            Return cParamCol.Item(Index).ParamType
        End If
    End Function

    'Private Sub Class_Initialize()
    '    cParamCol = New Collection
    '    ClearParameter()
    'End Sub

    Public Sub New()
        cParamCol = New Collection
        ClearParameter()
    End Sub
End Class
