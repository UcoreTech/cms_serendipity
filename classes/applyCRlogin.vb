Option Strict Off
Option Explicit On
Imports System.Text
Imports System.Security.Cryptography
Imports System.Data.SqlClient.SqlConnection
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.Shared
Public Class applyCRlogin
    Public dbName As String
    Public serverName As String
    Public userID As String
    Public passWord As String
    Public Sub ApplyInfo(ByRef _oRpt As CrystalDecisions.CrystalReports.Engine.ReportDocument)

        Dim oCRDb As CrystalDecisions.CrystalReports.Engine.Database = _oRpt.Database
        Dim oCRTables As CrystalDecisions.CrystalReports.Engine.Tables = oCRDb.Tables
        Dim oCRTable As CrystalDecisions.CrystalReports.Engine.Table
        Dim oCRTableLogonInfo As CrystalDecisions.Shared.TableLogOnInfo
        Dim oCRConnectionInfo As New CrystalDecisions.Shared.ConnectionInfo()
        oCRConnectionInfo.DatabaseName = dbName
        oCRConnectionInfo.ServerName = serverName
        oCRConnectionInfo.UserID = userID
        oCRConnectionInfo.Password = passWord
        For Each oCRTable In oCRTables
            oCRTableLogonInfo = oCRTable.LogOnInfo
            oCRTableLogonInfo.ConnectionInfo = oCRConnectionInfo
            oCRTable.ApplyLogOnInfo(oCRTableLogonInfo)
        Next
    End Sub
End Class
