﻿Imports System.Data
Imports System.Data.SqlClient

Public Class NewSQLHelper
    Private gTimeOut As Integer = 7200

    Dim appRdr As New System.Configuration.AppSettingsReader
    Private ConString As String = "Data Source=" & appRdr.GetValue("Server", GetType(String)) & _
                                    ";Initial Catalog=" & appRdr.GetValue("Database", GetType(String)) & _
                                    ";Persist Security Info=True;User ID=" & appRdr.GetValue("Username", GetType(String)) & _
                                    ";Password=" & appRdr.GetValue("Password", GetType(String))

    Public Function ExecuteDataset(ByVal Cmd As String) As DataSet
        Dim gCon As New SqlConnection(ConString)
        Dim SqlCmd As New SqlCommand(Cmd, gCon)
        Dim SQLAdapt As New SqlDataAdapter(SqlCmd)
        Dim DSResult As New DataSet

        SqlCmd.CommandTimeout = gTimeOut
        SqlCmd.CommandType = CommandType.Text

        If SqlCmd.Connection.State <> ConnectionState.Closed Then
            SqlCmd.Connection.Close()
        End If

        SqlCmd.Connection.Open()
        SQLAdapt.Fill(DSResult)
        SqlCmd.Connection.Close()

        Return DSResult
    End Function
    Public Function ExecuteDataset(ByVal StoreProc As String, ParamArray Parameters() As SqlParameter) As DataSet
        Dim gCon As New SqlConnection(ConString)
        Dim SqlCmd As New SqlCommand(StoreProc, gCon)
        Dim SQLAdapt As New SqlDataAdapter(SqlCmd)
        Dim DSResult As New DataSet

        SqlCmd.CommandTimeout = gTimeOut
        SqlCmd.CommandType = CommandType.StoredProcedure

        For Each Param As SqlParameter In Parameters
            SqlCmd.Parameters.Add(Param)
        Next

        If SqlCmd.Connection.State <> ConnectionState.Closed Then
            SqlCmd.Connection.Close()
        End If

        SqlCmd.Connection.Open()
        SQLAdapt.Fill(DSResult)
        SqlCmd.Connection.Close()

        Return DSResult
    End Function

    Public Sub ExecuteNonQuery(ByVal Command As String)
        Dim SQLCmd As New SqlCommand
        Dim gCon As New SqlConnection(ConString)

        SQLCmd.CommandTimeout = gTimeOut
        SQLCmd.CommandType = CommandType.Text
        SQLCmd.CommandText = Command
        SQLCmd.Connection = gCon

        If SQLCmd.Connection.State <> ConnectionState.Closed Then
            SQLCmd.Connection.Close()
        End If

        SQLCmd.Connection.Open()
        SQLCmd.ExecuteNonQuery()
        SQLCmd.Connection.Close()
    End Sub
    Public Sub ExecuteNonQuery(ByVal StoreProc As String, ParamArray Parameters() As SqlParameter)
        Dim SQLCmd As New SqlCommand
        Dim gCon As New SqlConnection(ConString)

        SQLCmd.CommandTimeout = gTimeOut
        SQLCmd.CommandType = CommandType.StoredProcedure
        SQLCmd.CommandText = StoreProc
        SQLCmd.Connection = gCon

        For Each Param As SqlParameter In Parameters
            SQLCmd.Parameters.Add(Param)
        Next

        If SQLCmd.Connection.State <> ConnectionState.Closed Then
            SQLCmd.Connection.Close()
        End If

        SQLCmd.Connection.Open()
        SQLCmd.ExecuteNonQuery()
        SQLCmd.Connection.Close()
    End Sub
    
    Public Function ExecuteReader(ByVal StoreProc As String, ParamArray Parameters As SqlParameter()) As SqlDataReader
        Dim gCon As New SqlConnection(ConString)
        Dim SQLCmd As New SqlCommand(StoreProc, gCon)

        SQLCmd.CommandTimeout = gTimeOut
        SQLCmd.CommandType = CommandType.StoredProcedure

        For Each Param As SqlParameter In Parameters
            SQLCmd.Parameters.Add(Param)
        Next

        If SQLCmd.Connection.State <> ConnectionState.Closed Then
            SQLCmd.Connection.Close()
        End If

        SQLCmd.Connection.Open()
        Return SQLCmd.ExecuteReader
    End Function
    Public Function ExecuteReader(ByVal Command As String) As SqlDataReader
        Dim gCon As New SqlConnection(ConString)
        Dim SQLCmd As New SqlCommand(Command, gCon)

        SQLCmd.CommandTimeout = gTimeOut
        SQLCmd.CommandType = CommandType.Text

        If SQLCmd.Connection.State <> ConnectionState.Closed Then
            SQLCmd.Connection.Close()
        End If

        SQLCmd.Connection.Open()
        Return SQLCmd.ExecuteReader
    End Function
End Class
