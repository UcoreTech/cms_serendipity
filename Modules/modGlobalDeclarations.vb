Imports System.Configuration
Imports System.Threading
Module modGlobalDeclarations
    Public dupIDnumber As String
    Public deptcode As String
    Public cnstring As String
    Public m_ProgressbarCaption As String = "Please Wait..."

    Public filename As String = ""

    <DebuggerStepThrough()> Public Sub ShowProgressBar()
        Dim Caption As String = "Loading"

        If m_ProgressbarCaption.Length = 0 Then
            Caption = Caption
        End If

        Dim fStatus As New frmSplashForm(m_ProgressbarCaption)
        fStatus.ProgressBar1.Value = 0
        fStatus.Show()
        fStatus.Refresh()
        While fStatus.ProgressBar1.Value <> 100
            If fStatus.ProgressBar1.Value < 99 Then
                fStatus.ProgressBar1.Value += 1
            Else
                fStatus.ProgressBar1.Value = 0
            End If
            'Thread.CurrentThread.Sleep(10)

            Thread.Sleep(1)
        End While

        fStatus.Hide()
        fStatus.Dispose()

        m_ProgressbarCaption = ""
    End Sub
End Module
