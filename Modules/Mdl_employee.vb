Imports System.Data.SqlClient.SqlConnection
Imports System.Data.SqlClient
Imports System.Drawing.Point

Imports System.Drawing.Color
Imports System.Data
Imports System.IO
Imports System.Drawing.Imaging
Module Mdl_employee

    Public gblkeyemployee, gblloginname, usergroup As String
    Public gblusername, encrypt_pass, status As String
    Public dttotal, iRow As Integer
    Public tagging, varfxkeyemployee, varidnumber, vardropdown, varnewdrop, vartype As String
    Public Sub load_employee_position()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration

        Dim str As String = "select description from dbo.per_employee_position"
        Dim cmd As New SqlCommand(str, myconnection.sqlconn)
        myconnection.sqlconn.Open()

        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                Employee_Selection_Report.cboposition.Items.Add(myreader(0))
            End While
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
    Public Sub Load_employee_type()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        'Dim myconnection As New SqlConnection(My.Settings.CNString)

        Dim str As String = "select typedescription from dbo.per_employee_type"
        Dim cmd As New SqlCommand(str, myconnection.sqlconn)
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                Employee_Selection_Report.cboemployeetype.Items.Add(myreader(0))
            End While
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
    Public Sub Load_Employee_SalaryGrade()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration

        Dim str As String = "select salarycode from dbo.per_employee_SalaryGrade"
        Dim cmd As New SqlCommand(str, myconnection.sqlconn)
        myconnection.sqlconn.Open()

        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                Employee_Selection_Report.cbosalarygrade.Items.Add(myreader(0))
            End While
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
    Public Sub Load_employee_id_foremployeeprofile()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        Dim str As String = "select IDnumber from dbo.temp_masterfile"
        Dim cmd As New SqlCommand(str, myconnection.sqlconn)
        myconnection.sqlconn.Open()

        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                Employee_Selection_Report.cboemployeeprofileid.Items.Add(myreader(0))
            End While
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
    Public Sub Load_employeeID_for_training()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        'Dim myconnection As New SqlConnection(My.Settings.CNString)

        Dim str As String = "select IDnumber from dbo.temp_masterfile"
        Dim cmd As New SqlCommand(str, myconnection.sqlconn)
        myconnection.sqlconn.Open()

        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                Employee_Selection_Report.cbotraining.Items.Add(myreader(0))
            End While
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
    Public Sub load_employeeID_for_training_name()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        'Dim myconnection As New SqlConnection(My.Settings.CNString)

        Dim mydatareader As SqlDataReader
        Dim mydataset As New DataSet
        Dim cmd As New SqlCommand
        cmd.CommandText = "USP_LOAD_TRAINING_EMPLOYEE_NAME"
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = myconnection.sqlconn

        cmd.Parameters.Add("@idnumber", SqlDbType.VarChar, 50, ParameterDirection.Input).Value = Employee_Selection_Report.cbotraining.Text
        myconnection.sqlconn.Open()

        mydatareader = cmd.ExecuteReader
        Try
            While mydatareader.Read()
                Employee_Selection_Report.txttraining.Text = mydatareader.Item(0)
            End While
        Finally
            myconnection.sqlconn.Close()
            mydatareader.Close()
        End Try
    End Sub
    Public Sub LOAD_EMPLOYEEID_PROFILE()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        'Dim myconnection As New SqlConnection(My.Settings.CNString)

        Dim mydatareader As SqlDataReader
        Dim mydataset As New DataSet
        Dim cmd As New SqlCommand
        cmd.CommandText = "USP_LOAD_NAME_EMPLOYEE_MASTERFILE_EQUAL"
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = myconnection.sqlconn

        cmd.Parameters.Add("@idnumber", SqlDbType.VarChar, 50, ParameterDirection.Input).Value = Employee_Selection_Report.cboemployeeprofileid.Text
        myconnection.sqlconn.Open()

        mydatareader = cmd.ExecuteReader
        Try
            While mydatareader.Read()
                Employee_Selection_Report.txtfullname.Text = mydatareader.Item(0)
            End While
        Finally
            myconnection.sqlconn.Close()
            mydatareader.Close()
        End Try
    End Sub
    Public Sub LOAD_TRNF_CERTIFICATION_NAME()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        'Dim myconnection As New SqlConnection(My.Settings.CNString)

        Dim mydatareader As SqlDataReader
        Dim mydataset As New DataSet
        Dim cmd As New SqlCommand
        cmd.CommandText = "USP_CERTIFICATION_NAME_IDNUMBER_EQUAL"
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = myconnection.sqlconn

        cmd.Parameters.Add("@idnumber", SqlDbType.VarChar, 50, ParameterDirection.Input).Value = Employee_Selection_Report.cbocertificate.Text
        myconnection.sqlconn.Open()

        mydatareader = cmd.ExecuteReader
        Try
            While mydatareader.Read()
                Employee_Selection_Report.TXTFULLNAMECERTIFICATE.Text = mydatareader.Item(0)
            End While
        Finally
            myconnection.sqlconn.Close()
            mydatareader.Close()
        End Try

    End Sub
    Public Sub load_TRNF_Certification_dynamic()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        'Dim myconnection As New SqlConnection(My.Settings.CNString)

        Dim str As String = "select IDnumber from dbo.TRNF_Certification"
        Dim cmd As New SqlCommand(str, myconnection.sqlconn)
        myconnection.sqlconn.Open()

        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                Employee_Selection_Report.cbocertificate.Items.Add(myreader(0))
            End While
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#Region "Insert ES"
    Public Sub InsertES(ByVal fxkeyemployee As String, ByVal idnumber As String, ByVal empstatus As String, ByVal effectdate As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_employee_tagging_insert", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@option", SqlDbType.Int).Value = 1
                .Add("@fxkeyemployee", SqlDbType.Char, 10).Value = fxkeyemployee
                .Add("@idnumber", SqlDbType.VarChar, 50).Value = idnumber
                .Add("@NewEmpStatus", SqlDbType.VarChar, 50).Value = empstatus
                .Add("@EffectNewEmpStatus", SqlDbType.SmallDateTime).Value = effectdate
                '.Add("@keyposition", SqlDbType.Char, 5).Value = ""
                '.Add("@year", SqlDbType.Char, 5).Value = ""
                'type
                .Add("@NewEmpType", SqlDbType.VarChar, 50).Value = ""
                .Add("@EffectNewEmpType", SqlDbType.SmallDateTime).Value = "1/1/1900"
                'position
                '.Add("@NewEmpPosition", SqlDbType.VarChar, 50).Value = ""
                .Add("@EffectEmpPosition", SqlDbType.SmallDateTime).Value = "1/1/1900"
                'SG
                .Add("@NewSalaryGrade", SqlDbType.VarChar, 50).Value = ""
                .Add("@EffectSalaryGrade", SqlDbType.SmallDateTime).Value = "1/1/1900"
                'BP
                .Add("@NewBasicPay", SqlDbType.VarChar, 50).Value = ""
                .Add("@EffectNewBasicPay", SqlDbType.SmallDateTime).Value = "1/1/1900"

            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Employee Status", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
#Region "Check and Update ES"
    Public Sub checkESDate()
        'and fxkeyemployee=" & "'" & fxkeyemployee & "'"
        Dim sdate1 As Date
        Dim sdate2 As Date = Format(Now, "M/dd/yyyy")
        Dim fxkey As String
        Dim mycon As New Clsappconfiguration
        Dim str As String = "select fxkeyemployee,EffectNewEmpStatus from Per_Employee_Tagging where BitNewEmpStatus = 1"
        Dim cmd As New SqlCommand(str, mycon.sqlconn)
        cmd.CommandType = CommandType.Text
        mycon.sqlconn.Open()
        Dim reader As SqlDataReader = cmd.ExecuteReader
        Try
            While reader.Read
                sdate1 = Format(reader.Item("EffectNewEmpStatus"), "M/dd/yyyy")
                fxkey = RTrim(reader.Item("fxkeyemployee"))
                If sdate1 <= sdate2 Then
                    Call UpdateES(RTrim(fxkey), sdate1)
                Else
                End If
            End While
            reader.Close()
            mycon.sqlconn.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Employee Status", MessageBoxButtons.OK)
        End Try
    End Sub
    Public Sub UpdateES(ByVal fxkeyemployee As String, ByVal dates As Date)
        Dim mycon As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_Personnel_tagging_update_EmpStatus", mycon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            With cmd.Parameters
                .Add("@fxkeyemployee", SqlDbType.Char, 10).Value = RTrim(fxkeyemployee)
                .Add("@dateregular", SqlDbType.DateTime).Value = dates
            End With
            mycon.sqlconn.Open()
            cmd.ExecuteNonQuery()
            mycon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Update EmpStatus", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
End Module
