Imports System.Data.SqlClient.SqlConnection
Imports System.Data.SqlClient
Imports System.Drawing.Point

Imports System.Drawing.Color
Imports System.Data
Imports System.IO
Imports System.Drawing.Imaging
Module module_applicant
    Public Sub disable_buttons()
        'for education
        Form2.btnnew_educ.Enabled = False
        Form2.btnedit1.Enabled = False
        Form2.btndelete_educ.Enabled = False
        '==============================

        'for work experience
        Form2.btnapp_newworkexperience.Enabled = False
        Form2.btnapp_editworkexperience.Enabled = False
        Form2.Btnapp_deleteworkexperience.Enabled = False
        '==========================================

        'for training records
        Form2.btnapp_newtraining.Enabled = False
        Form2.btnapp_edittraining.Enabled = False
        Form2.btnapp_deletetrainig.Enabled = False
        '====================================

        'for family members
        Form2.btnapp_newfamilyrelations.Enabled = False
        Form2.btnapp_editfamilyrelations.Enabled = False
        Form2.btnapp_deletefamilyrelations.Enabled = False
        '=================================================

        'for references
        Form2.btnapp_newreference.Enabled = False
        Form2.Btnapp_editreference.Enabled = False
        Form2.btnapp_deletereference.Enabled = False
        'for result exams
        Form2.btnAddResult.Enabled = False
        Form2.btnEditResult.Enabled = False
        Form2.btnDeleteResult.Enabled = False
        'others 4
        Form2.btngetCV.Enabled = False
        Form2.btnshowCV.Enabled = False
    End Sub
    Public Sub inable_buttons()
        'for education
        Form2.btnnew_educ.Enabled = True
        Form2.btnedit1.Enabled = True
        Form2.btndelete_educ.Enabled = True
        '==============================

        'for work experience
        Form2.btnapp_newworkexperience.Enabled = True
        Form2.btnapp_editworkexperience.Enabled = True
        Form2.Btnapp_deleteworkexperience.Enabled = True
        '==========================================

        'for training records
        Form2.btnapp_newtraining.Enabled = True
        Form2.btnapp_edittraining.Enabled = True
        Form2.btnapp_deletetrainig.Enabled = True
        '====================================

        'for family members
        Form2.btnapp_newfamilyrelations.Enabled = True
        Form2.btnapp_editfamilyrelations.Enabled = True
        Form2.btnapp_deletefamilyrelations.Enabled = True
        '=================================================

        'for references
        Form2.btnapp_newreference.Enabled = True
        Form2.Btnapp_editreference.Enabled = True
        Form2.btnapp_deletereference.Enabled = True
        'for result exams
        Form2.btnAddResult.Enabled = True
        Form2.btnEditResult.Enabled = True
        Form2.btnDeleteResult.Enabled = True
        'others 4
        Form2.btngetCV.Enabled = True
        Form2.btnshowCV.Enabled = True
    End Sub
    Public Sub visible_falsebuttons()
        Form2.btndelete_applicant.Enabled = False
        Form2.btnnext.Visible = False
        Form2.btnprevious.Visible = False
        Form2.btnfind.Visible = False
        Form2.btnhired.Visible = False
    End Sub
    Public Sub visible_truebuttons()
        Form2.btndelete_applicant.Enabled = True
        Form2.btnnext.Visible = True
        Form2.btnprevious.Visible = True
        Form2.btnfind.Visible = True
        Form2.btnhired.Visible = True
    End Sub
    Public Sub load_incrementid_requisition_manpower()
        'Dim appRdr As New System.Configuration.AppSettingsReader
        'Dim myconnection As New Clsappconfiguration
        ''Dim myconnection As New SqlConnection(My.Settings.CNString)
        'Dim cmd As New SqlCommand("USP_INCREMENT_ID_NUMBER_SELECT", myconnection.sqlconn)
        'cmd.CommandType = CommandType.StoredProcedure
        'myconnection.sqlconn.Open()

        'Dim myreader As SqlDataReader
        'myreader = cmd.ExecuteReader
        'Try
        '    While myreader.Read
        '        Requisition_of_Manpower.txtid_increment.Text = myreader(0)
        '    End While
        'Finally

        'End Try
    End Sub
    Public Sub message()
        MessageBox.Show("You already exceed the maximum length", "Maximum Length", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
End Module
