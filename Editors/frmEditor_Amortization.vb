﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmEditor_Amortization

    Private con As New Clsappconfiguration
    Private mIndex As Integer = 0

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frmEditor_Amortization_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _LoadLoanTypes()
        GenerateButton_forSelect()
        _loadLoans("")
    End Sub

    Private Sub _LoadLoanTypes()
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_Editor_LOadLoanTypes")
            cboLoanTypes.Items.Add("All")
            While rd.Read
                With cboLoanTypes
                    .Items.Add(rd(0).ToString)
                End With
            End While
            cboLoanTypes.Text = "All"
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub GenerateButton_forDeduct()
        dgvAmortization.Columns.Clear()
        Dim btndeduct As New DataGridViewCheckBoxColumn
        With btndeduct
            .Name = "btndeduct"
            .HeaderText = "Deduct"
        End With
        dgvAmortization.Columns.Add(btndeduct)
    End Sub
    Private Sub GetAmortizationTotals()
        Try
            Dim totprincipal As Decimal
            Dim totinterest As Decimal
            Dim totservicefee As Decimal
            Dim totamortization As Decimal

            For i As Integer = 0 To dgvAmortization.RowCount - 1
                With dgvAmortization
                    totprincipal = totprincipal + CDec(.Rows(i).Cells(3).Value.ToString)
                    totinterest = totinterest + CDec(.Rows(i).Cells(4).Value.ToString)
                    totservicefee = totservicefee + CDec(.Rows(i).Cells(5).Value.ToString)
                    totamortization = totamortization + CDec(.Rows(i).Cells(6).Value.ToString)
                End With
            Next

            txtTotPrincipal.Text = Format(totprincipal, "##,##0.00")
            txtTotInterest.Text = Format(totinterest, "##,##0.00")
            txtTotServiceFee.Text = Format(totservicefee, "##,##0.00")
            txtTotAmortization.Text = Format(totamortization, "##,##0.00")
        Catch ex As Exception
            frmMsgBox.Text = "Error - Calculating Amortization"
            frmMsgBox.txtMessage.Text = ex.Message + "  - Select 'Mode of Payment' and 'Interest Method' then click Compute."
            frmMsgBox.ShowDialog()
        End Try
    End Sub
    Private Sub _FormatValues()
        ' align number grid textboxes
        With dgvAmortization
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).DefaultCellStyle.Format = "##,##0.00"
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(4).DefaultCellStyle.Format = "##,##0.00"
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(5).DefaultCellStyle.Format = "##,##0.00"
            .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(6).DefaultCellStyle.Format = "##,##0.00"
            .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(7).DefaultCellStyle.Format = "##,##0.00"
            .Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
    End Sub
    Private Sub _EXEC_AdvanceInterest()
        If dgvAmortization.SelectedRows(0).Cells(1).Value = "0" Then
            If dgvAmortization.SelectedRows(0).Cells(0).Value = True Then
                For i As Integer = 0 To dgvAmortization.RowCount - 1
                    With dgvAmortization
                        .Rows(i).Cells(0).Value = False
                        .Rows(i).DefaultCellStyle.BackColor = Color.White
                        .Rows(i).Cells(6).Value = CDec(.Rows(i).Cells(6).Value) + CDec(.Rows(i).Cells(4).Value)
                    End With
                Next
            Else
                For i As Integer = 0 To dgvAmortization.RowCount - 1
                    With dgvAmortization
                        .Rows(i).Cells(0).Value = True
                        .Rows(i).DefaultCellStyle.BackColor = Color.SkyBlue
                        .Rows(i).Cells(6).Value = CDec(.Rows(i).Cells(3).Value)
                    End With
                Next
            End If
            Calculate_Total_UnearnedInterest()
            Exit Sub
        End If
        If dgvAmortization.SelectedRows(0).Cells(0).Value = True Then
            dgvAmortization.SelectedRows(0).Cells(0).Value = False
            dgvAmortization.SelectedRows(0).DefaultCellStyle.BackColor = Color.White
            With dgvAmortization
                .SelectedRows(0).Cells(6).Value = CDec(.SelectedRows(0).Cells(6).Value) + CDec(.SelectedRows(0).Cells(4).Value)
            End With
        Else
            dgvAmortization.SelectedRows(0).Cells(0).Value = True
            dgvAmortization.SelectedRows(0).DefaultCellStyle.BackColor = Color.SkyBlue
            With dgvAmortization
                .SelectedRows(0).Cells(6).Value = CDec(.SelectedRows(0).Cells(3).Value)
            End With
        End If
        Calculate_Total_UnearnedInterest()
    End Sub
    Private Sub Calculate_Total_UnearnedInterest() 'added by vincent Nacar 11/7/14
        Try
            Dim totunearned As Decimal
            For i As Integer = 0 To dgvAmortization.RowCount - 1
                With dgvAmortization
                    If .Rows(i).Cells(0).Value = True Then
                        totunearned = totunearned + CDec(.Rows(i).Cells(4).Value.ToString)
                    End If
                End With
            Next
            txtTotUnearned.Text = Format(totunearned, "##,##0.00")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub _loadLoans(ByVal key As String)
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_Editor_LoadLoans",
                                           New SqlParameter("@key", key),
                                           New SqlParameter("@LoanType", cboLoanTypes.Text))
            dgvLoans.DataSource = ds.Tables(0)
            dgvLoans.Columns(5).DefaultCellStyle.Format = "##,##0.00"
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub _loadLoans_Condition(ByVal condition As String, ByVal cvalue As String)
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_Editor_LoadLoans_Conditional",
                                           New SqlParameter("@Condition", condition),
                                           New SqlParameter("@Value", cvalue))
            dgvLoans.DataSource = ds.Tables(0)
            dgvLoans.Columns(5).DefaultCellStyle.Format = "##,##0.00"
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub _LoadLoans_WithoutAmort() Handles btnShowLoans.Click
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_Editor_ShowLoans_WithoutAmort")
            dgvLoans.DataSource = ds.Tables(0)
            dgvLoans.Columns(5).DefaultCellStyle.Format = "##,##0.00"
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub GenerateButton_forSelect()
        dgvLoans.Columns.Clear()
        Dim btndeduct As New DataGridViewCheckBoxColumn
        With btndeduct
            .Name = "btnSelect"
            .HeaderText = "Select"
        End With
        dgvLoans.Columns.Add(btndeduct)
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        _loadLoans(txtSearch.Text)
    End Sub

    Private Sub dgvLoans_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLoans.CellClick
        If dgvLoans.CurrentRow.Cells(0).Value = True Then
            dgvLoans.CurrentRow.Cells(0).Value = False
        Else
            dgvLoans.CurrentRow.Cells(0).Value = True
        End If
    End Sub

    Private Sub dgvLoans_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLoans.Click
        dgvAmortization.DataSource = Nothing
        dgvAmortization.Rows.Clear()
        dgvAmortization.Columns.Clear()
        'GenerateButton_forDeduct()
        GenerateAmortizationColumns()
        _GetCurrentAmort(dgvLoans.CurrentRow.Cells(1).Value.ToString)
        _Gen_NoofPayment_Grid(dgvLoans.CurrentRow.Cells(7).Value.ToString, CInt(dgvLoans.CurrentRow.Cells(6).Value))
        _FormatValues()
        GetAmortizationTotals()
    End Sub

    Private Sub _GetCurrentAmort(ByVal key As String)
        Try
            Dim ds As SqlDataReader
            ds = SqlHelper.ExecuteReader(con.cnstring, "_Editor_LoadAmortization",
                                           New SqlParameter("@key", key))
            While ds.Read
                With dgvAmortization
                    .Rows.Add()
                    .Item("payno", mIndex).Value = ds(0)
                    .Item("paydate", mIndex).Value = ds(1)
                    .Item("loanref", mIndex).Value = key
                    .Item("principal", mIndex).Value = ds(2)
                    .Item("interest", mIndex).Value = ds(3)
                    .Item("servicefee", mIndex).Value = ds(4)
                    .Item("amortization", mIndex).Value = ds(5)
                    .Item("balance", mIndex).Value = ds(6)
                End With
                mIndex += 1
            End While
        Catch ex As Exception

        End Try
    End Sub

    Private Sub _Gen_NoofPayment(ByVal terms As Integer)
        Try
            Select Case cboModeofPayment.Text
                Case "DAILY"
                    txtNoofPayment.Text = CStr(terms * 30)
                Case "WEEKLY"
                    txtNoofPayment.Text = CStr(terms * 4)
                Case "SEMI-MONTHLY"
                    txtNoofPayment.Text = CStr(terms * 2)
                Case "MONTHLY"
                    txtNoofPayment.Text = CStr(terms)
                Case "QUARTERLY"
                    txtNoofPayment.Text = CStr(terms / 4)
                Case "ANNUALLY"
                    txtNoofPayment.Text = CStr(terms / 12)
                Case "LUMP SUM"
                    txtNoofPayment.Text = CStr(1)
            End Select
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Please Select Terms!"
            frmMsgBox.ShowDialog()
        End Try
    End Sub
    Private Sub _Gen_NoofPayment_Grid(ByVal md As String, ByVal terms As Integer)
        Try
            cboModeofPayment.Text = md
            Select Case cboModeofPayment.Text
                Case "DAILY"
                    txtNoofPayment.Text = CStr(terms * 30)
                Case "WEEKLY"
                    txtNoofPayment.Text = CStr(terms * 4)
                Case "SEMI-MONTHLY"
                    txtNoofPayment.Text = CStr(terms * 2)
                Case "MONTHLY"
                    txtNoofPayment.Text = CStr(terms)
                Case "QUARTERLY"
                    txtNoofPayment.Text = CStr(terms / 4)
                Case "ANNUALLY"
                    txtNoofPayment.Text = CStr(terms / 12)
                Case "LUMP SUM"
                    txtNoofPayment.Text = CStr(1)
            End Select
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Please Select Terms!"
            frmMsgBox.ShowDialog()
        End Try
    End Sub
    Private Function _Gen_NoofPayment_ret(ByVal terms As Integer) As String
        Try
            Select Case cboModeofPayment.Text
                Case "DAILY"
                    Return CStr(terms * 30)
                Case "WEEKLY"
                    Return CStr(terms * 4)
                Case "SEMI-MONTHLY"
                    Return CStr(terms * 2)
                Case "MONTHLY"
                    Return CStr(terms)
                Case "QUARTERLY"
                    Return CStr(terms / 4)
                Case "ANNUALLY"
                    Return CStr(terms / 12)
                Case "LUMP SUM"
                    Return CStr(1)
            End Select
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Please Select Terms!"
            frmMsgBox.ShowDialog()
        End Try
    End Function

    Private Sub cboModeofPayment_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboModeofPayment.SelectedValueChanged
        _Gen_NoofPayment(CInt(dgvLoans.CurrentRow.Cells(6).Value))
    End Sub

    Private Sub genMaxLoad_Loans()
        ProgressBar1.Maximum = 0
        For i As Integer = 0 To dgvLoans.RowCount - 1
            ProgressBar1.Maximum += 1
        Next
    End Sub

    Private Sub btnGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        GenerateAmortizationColumns()
        genMaxLoad_Loans()
        If rdFormula.Checked = True Then
            ProgressBar1.Visible = True
            _Generate_Amortization_usingFormula()
            ProgressBar1.Visible = False
            ProgressBar1.Value = 0
            GetAmortizationTotals()
        Else
            ProgressBar1.Visible = True
            _Generate_Amortization_usingSTANDARD()
            ProgressBar1.Visible = False
            ProgressBar1.Value = 0
            GetAmortizationTotals()
        End If
    End Sub

    Private Sub _Generate_Amortization_usingFormula()
        For i As Integer = 0 To dgvLoans.RowCount - 1
            ProgressBar1.Value += 1
            With dgvLoans
                If .Rows(i).Cells(0).Value = True Then
                    _Exec_Amort_Formula(.Rows(i).Cells(5).Value, .Rows(i).Cells(8).Value, _Gen_NoofPayment_ret(.Rows(i).Cells(6).Value), .Rows(i).Cells(10).Value, DateAdd(DateInterval.Month, 1, .Rows(i).Cells(10).Value), cboModeofPayment.Text, .Rows(i).Cells(1).Value)
                End If
            End With
        Next
        _ApplyAuto_Deduct()
    End Sub

    Private Sub _Exec_Amort_Formula(ByVal LoanAmt As String, ByVal interest As Decimal, ByVal terms As String, ByVal granted As Date, ByVal datestart As Date, ByVal mode As String, ByVal loanref As String)
        Dim noofpayment As Integer = CInt(terms)
        Dim interestrate As Decimal = interest
        Try
            Dim ds As SqlDataReader
            ds = SqlHelper.ExecuteReader(con.cnstring, rdFormula.Tag,
                                           New SqlParameter("@PrincipalAmount", LoanAmt),
                                           New SqlParameter("@InterestRate", interestrate),
                                           New SqlParameter("@Terms", noofpayment),
                                           New SqlParameter("@DateGranted", granted),
                                           New SqlParameter("@FirstPayment", datestart),
                                           New SqlParameter("@xmode", mode))
            While ds.Read
                With dgvAmortization
                    .Rows.Add()
                    .Item("payno", mIndex).Value = ds(0)
                    .Item("paydate", mIndex).Value = ds(1)
                    .Item("loanref", mIndex).Value = loanref
                    .Item("principal", mIndex).Value = ds(2)
                    .Item("interest", mIndex).Value = ds(3)
                    .Item("servicefee", mIndex).Value = ds(4)
                    .Item("amortization", mIndex).Value = ds(5)
                    .Item("balance", mIndex).Value = ds(6)
                End With
                mIndex += 1
            End While
        Catch ex As Exception
        End Try
    End Sub

    Private Sub _ApplyAuto_Deduct()
        Dim advance As Decimal = 0
        Try
            If lblDesc.Text = "1 Year Advanced Interest" Then
                For i As Integer = 0 To dgvAmortization.RowCount - 1
                    With dgvAmortization
                        If .Item("payno", i).Value >= 1 And .Item("payno", i).Value <= 12 Then
                            .Item("payno", i).Tag = "Paid"
                            advance = advance + .Item("interest", i).Value
                            .Item("amortization", i).Value = .Item("principal", i).Value
                        Else
                            .Item("payno", i).Tag = "Unpaid"
                        End If
                    End With
                Next
                txtTotUnearned.Text = Format(CDec(advance), "##,##0.00")
                Exit Sub
            End If
            If lblDesc.Text = "2 Years Advanced Interest" Then
                For i As Integer = 0 To dgvAmortization.RowCount - 1
                    With dgvAmortization
                        If .Item("payno", i).Value >= 1 And .Item("payno", i).Value <= 24 Then
                            .Item("payno", i).Tag = "Paid"
                            advance = advance + .Item("interest", i).Value
                            .Item("amortization", i).Value = .Item("principal", i).Value
                        Else
                            .Item("payno", i).Tag = "Unpaid"
                        End If
                    End With
                Next
                txtTotUnearned.Text = Format(CDec(advance), "##,##0.00")
                Exit Sub
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub rdDim_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdDim.Click
        rdAddon.Checked = False
        rdDeducted.Checked = False
        rdCustomized.Checked = False
        rdFormula.Checked = False
    End Sub
    Private Sub rdCustomized_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdCustomized.Click
        rdAddon.Checked = False
        rdDeducted.Checked = False
        rdDim.Checked = False
        rdFormula.Checked = False
        rdStraight.Checked = False
    End Sub

    Private Sub rdFormula_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdFormula.Click
        rdAddon.Checked = False
        rdDeducted.Checked = False
        rdCustomized.Checked = False
        rdDim.Checked = False
        rdStraight.Checked = False
        frmAmortFormula.StartPosition = FormStartPosition.CenterScreen
        frmAmortFormula.Show()
    End Sub

    Private Sub dgvAmortization_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvAmortization.Click
        If rdFormula.Checked = False Then
            '_EXEC_AdvanceInterest()
            GetAmortizationTotals()
        Else
            GetAmortizationTotals()
        End If
    End Sub

    Private Sub cboLoanTypes_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanTypes.SelectedValueChanged
        _loadLoans(txtSearch.Text)
    End Sub

    Private Sub btnClearAmort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearAmort.Click
        mIndex = 0
        dgvAmortization.DataSource = Nothing
        dgvAmortization.Rows.Clear()
        dgvAmortization.Columns.Clear()
        txtTotAmortization.Text = "0.00"
        txtTotPrincipal.Text = "0.00"
        txtTotInterest.Text = "0.00"
        txtTotServiceFee.Text = "0.00"
        txtTotUnearned.Text = "0.00"
    End Sub

    Private Sub GenerateAmortizationColumns()
        mIndex = 0
        dgvAmortization.DataSource = Nothing
        dgvAmortization.Rows.Clear()
        dgvAmortization.Columns.Clear()
        'GenerateButton_forDeduct()
        Dim payno As New DataGridViewTextBoxColumn
        Dim paydate As New DataGridViewTextBoxColumn
        Dim loanref As New DataGridViewTextBoxColumn
        Dim principal As New DataGridViewTextBoxColumn
        Dim interest As New DataGridViewTextBoxColumn
        Dim servicefee As New DataGridViewTextBoxColumn
        Dim amortization As New DataGridViewTextBoxColumn
        Dim balance As New DataGridViewTextBoxColumn
        With payno
            .Name = "payno"
            .HeaderText = "No."
        End With
        With paydate
            .Name = "paydate"
            .HeaderText = "Date"
        End With
        With loanref
            .Name = "loanref"
            .HeaderText = "Loan Ref."
        End With
        With principal
            .Name = "principal"
            .HeaderText = "Principal"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        End With
        With interest
            .Name = "interest"
            .HeaderText = "Interest"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        End With
        With servicefee
            .Name = "servicefee"
            .HeaderText = "Other Payments"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        End With
        With amortization
            .Name = "amortization"
            .HeaderText = "Amortization"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        End With
        With balance
            .Name = "balance"
            .HeaderText = "Balance"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        End With
        With dgvAmortization
            .Columns.Add(loanref)
            .Columns.Add(payno)
            .Columns.Add(paydate)
            .Columns.Add(principal)
            .Columns.Add(interest)
            .Columns.Add(servicefee)
            .Columns.Add(amortization)
            .Columns.Add(balance)
        End With
    End Sub

    Private Sub lblSelFormula_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblSelFormula.TextChanged
        If lblSelFormula.Text = "_Formula_loan_Computation_V5" Then
            lblDesc.Text = "1 Year Advanced Interest"
        End If
        If lblSelFormula.Text = "_Formula_loan_v8_300k" Then
            lblDesc.Text = "2 Years Advanced Interest"
        End If
    End Sub

    Private Sub genMaxLoad_Amort()
        ProgressBar1.Maximum = 0
        For i As Integer = 0 To dgvAmortization.RowCount - 1
            ProgressBar1.Maximum += 1
        Next
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If MsgBox("Are you sure you want to Save CHANGES?", vbYesNo, "Saving Confirmation") = vbYes Then
            genMaxLoad_Amort()
            ProgressBar1.Visible = True
            For i As Integer = 0 To dgvLoans.RowCount - 1
                With dgvLoans
                    If .Rows(i).Cells(0).Value = True Then
                        DeleteAmortization_ThenUpdate(.Rows(i).Cells(1).Value.ToString)
                        ModeOfPayment_Saved(.Rows(i).Cells(1).Value.ToString)
                    End If
                End With
            Next
            If rdStraight.Checked = True And rdAddon.Checked = True Then
                SaveAmortSchedule("Straight Add-On")
            End If
            If rdStraight.Checked = True And rdDeducted.Checked = True Then
                SaveAmortSchedule("Straight Deducted")
            End If
            If rdDim.Checked = True Then
                SaveAmortSchedule("Diminishing")
            End If
            If rdFormula.Checked = True Then
                SaveAmortSchedule("Custom")
            End If
            If chkManual.Checked = True Then
                SaveAmortSchedule("Manual")
            End If
            ProgressBar1.Visible = False
            ProgressBar1.Value = 0
        Else
            Exit Sub
        End If
    End Sub

    Private Sub ModeOfPayment_Saved(ByVal loanno As String)
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_ModeOfPayment_InsertUpdate",
                                       New SqlParameter("@fnLoanNo", loanno),
                                       New SqlParameter("@fcMode", cboModeofPayment.Text))
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub DeleteAmortization_ThenUpdate(ByVal loanno As String)
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_DeleteAmortization_Schedule_ForUpdating",
                                       New SqlParameter("@LoanNo", loanno))
        Catch ex As Exception
            frmMsgBox.Text = "Error - Delete Amortization"
            frmMsgBox.txtMessage.Text = ex.Message
            frmMsgBox.ShowDialog()
        End Try
    End Sub
    Private Sub SaveAmortSchedule(ByVal xmethod As String) 'by Vincent Nacar
        Try
            For i As Integer = 0 To dgvAmortization.RowCount - 1
                ProgressBar1.Value += 1
                With dgvAmortization
                    Dim loanNo As String = .Item("loanref", i).Value
                    Dim payNo As String = .Item("payno", i).Value
                    Dim fcDate As String = .Item("paydate", i).Value
                    Dim xprincipal As String = .Item("principal", i).Value
                    Dim xinterest As String = .Item("interest", i).Value
                    Dim xservicefee As String = .Item("servicefee", i).Value
                    Dim xamortization As String = .Item("amortization", i).Value
                    Dim balance As String = .Item("balance", i).Value
                    Dim method As String = xmethod
                    Dim status As String = .Item("payno", i).Tag
                    SqlHelper.ExecuteNonQuery(con.cnstring, "_Insert_AmortizationSchedule",
                                               New SqlParameter("@pk_KeyID", System.Guid.NewGuid),
                                               New SqlParameter("@fcLoanNo", loanNo),
                                               New SqlParameter("@fdPayNo", payNo),
                                               New SqlParameter("@fcDate", fcDate),
                                               New SqlParameter("@fdPrincipal", xprincipal),
                                               New SqlParameter("@fdInterest", xinterest),
                                               New SqlParameter("@fdServiceFee", xservicefee),
                                               New SqlParameter("@fdAmortization", xamortization),
                                               New SqlParameter("@fdBalance", balance),
                                               New SqlParameter("@fcMethod", method),
                                               New SqlParameter("@fcStatus", status))
                End With
            Next
        Catch ex As Exception
            frmMsgBox.Text = "Error - Saving Amortization"
            frmMsgBox.txtMessage.Text = ex.Message
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub _Generate_Amortization_usingSTANDARD()
        For i As Integer = 0 To dgvLoans.RowCount - 1
            ProgressBar1.Value += 1
            With dgvLoans
                If .Rows(i).Cells(0).Value = True Then
                    _ExecNormalAmort(.Rows(i).Cells(5).Value, .Rows(i).Cells(8).Value, _Gen_NoofPayment_ret(.Rows(i).Cells(6).Value), .Rows(i).Cells(10).Value, DateAdd(DateInterval.Month, 1, .Rows(i).Cells(10).Value), cboModeofPayment.Text, .Rows(i).Cells(1).Value)
                End If
            End With
        Next
        '_ApplyAuto_Deduct()
    End Sub
    Private Sub _ExecNormalAmort(ByVal LoanAmt As String, ByVal interest As Decimal, ByVal terms As String, ByVal granted As Date, ByVal datestart As Date, ByVal mode As String, ByVal loanref As String)

        Dim interestrate As Decimal = 0
        Select Case mode
            Case "DAILY"
                interestrate = CDec(interest / 360)
            Case "WEEKLY"
                interestrate = CDec(interest / 48)
            Case "SEMI-MONTHLY"
                interestrate = CDec(interest / 24)
            Case "MONTHLY"
                interestrate = CDec(interest / 12)
            Case "QUARTERLY"
                interestrate = CDec(interest / 3)
            Case "ANNUALLY"
                interestrate = CDec(interest / 1)
            Case "LUMP SUM"
                interestrate = CDec(interest)
        End Select

        If rdStraight.Checked = True And rdAddon.Checked = True Then
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "Calculate_Amortization_Schedule_Straight_AddOn",
                       New SqlParameter("@PrincipalAmount", LoanAmt),
                       New SqlParameter("@InterestRate", interestrate),
                       New SqlParameter("@Terms", terms),
                       New SqlParameter("@DateGranted", granted),
                       New SqlParameter("@FirstPayment", datestart),
                       New SqlParameter("@xmode", mode))
            While rd.Read
                With dgvAmortization
                    .Rows.Add()
                    .Item("payno", mIndex).Value = rd(0)
                    .Item("paydate", mIndex).Value = rd(1)
                    .Item("loanref", mIndex).Value = loanref
                    .Item("principal", mIndex).Value = rd(2)
                    .Item("interest", mIndex).Value = rd(3)
                    .Item("servicefee", mIndex).Value = rd(4)
                    .Item("amortization", mIndex).Value = rd(5)
                    .Item("balance", mIndex).Value = rd(6)
                End With
                mIndex += 1
            End While
        End If
        If rdStraight.Checked = True And rdDeducted.Checked = True Then
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "Calculate_Amortization_Schedule_Straight_Deducted",
                       New SqlParameter("@PrincipalAmount", LoanAmt),
                       New SqlParameter("@InterestRate", interestrate),
                       New SqlParameter("@Terms", terms),
                       New SqlParameter("@DateGranted", granted),
                       New SqlParameter("@FirstPayment", datestart),
                       New SqlParameter("@xmode", mode))
            While rd.Read
                With dgvAmortization
                    .Rows.Add()
                    .Item("payno", mIndex).Value = rd(0)
                    .Item("paydate", mIndex).Value = rd(1)
                    .Item("loanref", mIndex).Value = loanref
                    .Item("principal", mIndex).Value = rd(2)
                    .Item("interest", mIndex).Value = rd(3)
                    .Item("servicefee", mIndex).Value = rd(4)
                    .Item("amortization", mIndex).Value = rd(5)
                    .Item("balance", mIndex).Value = rd(6)
                End With
                mIndex += 1
            End While
        End If
        If rdDim.Checked = True Then
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "Calculate_Amortization_Schedule_Diminishing",
                       New SqlParameter("@PrincipalAmount", LoanAmt),
                       New SqlParameter("@InterestRate", interestrate),
                       New SqlParameter("@Terms", terms),
                       New SqlParameter("@DateGranted", granted),
                       New SqlParameter("@FirstPayment", datestart),
                       New SqlParameter("@xmode", mode))
            While rd.Read
                With dgvAmortization
                    .Rows.Add()
                    .Item("payno", mIndex).Value = rd(0)
                    .Item("paydate", mIndex).Value = rd(1)
                    .Item("loanref", mIndex).Value = loanref
                    .Item("principal", mIndex).Value = rd(2)
                    .Item("interest", mIndex).Value = rd(3)
                    .Item("servicefee", mIndex).Value = rd(4)
                    .Item("amortization", mIndex).Value = rd(5)
                    .Item("balance", mIndex).Value = rd(6)
                End With
                mIndex += 1
            End While
        End If
    End Sub

    Private Sub btnFind_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFind.Click
        _loadLoans_Condition(cboCondition.Text, txtConditionalValue.Text)
    End Sub

#Region "Generate Row for Manual Amort"

    Private Sub btnGenRow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenRow.Click
        GenerateRow(txtNumbRow.Value.ToString)
    End Sub

    Private Sub GenerateRow(ByVal num As String)
        GenerateAmortizationColumns()
        dgvAmortization.ReadOnly = False
        For i As Integer = 0 To num - 1
            With dgvAmortization
                .Rows.Add()
                .Item("payno", i).Value = i
                .Item("paydate", i).Value = "1/1/1900"
                .Item("loanref", i).Value = " "
                .Item("principal", i).Value = "0.00"
                .Item("interest", i).Value = "0.00"
                .Item("servicefee", i).Value = "0.00"
                .Item("amortization", i).Value = "0.00"
                .Item("balance", i).Value = "0.00"
            End With
        Next
    End Sub

#End Region

    Private Function auto_Total(ByVal num1 As Decimal, ByVal num2 As Decimal, ByVal num3 As Decimal) As Decimal
        Try
            Return num1 + num2 + num3
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub AutoSumToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AutoSumToolStripMenuItem.Click
        With dgvAmortization.CurrentRow
            .Cells(6).Value = auto_Total(.Cells(3).Value, .Cells(4).Value, .Cells(5).Value)
        End With
        _AutoComputeBalance()
    End Sub

#Region "Copy/paste options"

    Dim col0, col1, col2, col3, col4, col5, col6, col7 As String

    Private Sub CopyToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CopyToolStripMenuItem.Click
        With dgvAmortization.CurrentRow
            col0 = .Cells(0).Value
            col1 = .Cells(1).Value
            col2 = .Cells(2).Value
            col3 = .Cells(3).Value
            col4 = .Cells(4).Value
            col5 = .Cells(5).Value
            col6 = .Cells(6).Value
            col7 = .Cells(7).Value
        End With
    End Sub

    Private Sub PasteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PasteToolStripMenuItem.Click
        With dgvAmortization.CurrentRow
            .Cells(0).Value = col0
            .Cells(1).Value = col1
            .Cells(2).Value = col2
            .Cells(3).Value = col3
            .Cells(4).Value = col4
            .Cells(5).Value = col5
            .Cells(6).Value = col6
            .Cells(7).Value = col7
        End With
    End Sub

#End Region

#Region "Auto compute Balance 12-1-14"
    Private Sub _AutoComputeBalance()
        Try
            Dim curbal As Decimal = dgvAmortization.Rows(0).Cells(7).Value
            For i As Integer = 0 To dgvAmortization.RowCount - 1
                If i > 0 Then
                    With dgvAmortization
                        curbal = curbal - .Rows(i).Cells(6).Value
                        .Rows(i).Cells(7).Value = curbal
                    End With
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

    Private Sub dgvAmortization_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAmortization.CellClick 
        If e.ColumnIndex = 2 Then
            dtpDate.Visible = True
            ActiveControl = dtpDate
        End If
    End Sub

    Private Sub dtpDate_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dtpDate.KeyDown
        If e.KeyCode = Keys.Escape Then
            dtpDate.Visible = False
            ActiveControl = dgvAmortization
        End If
        If e.KeyCode = Keys.Enter Then
            dgvAmortization.CurrentRow.Cells(2).Value = dtpDate.SelectionRange.Start.ToString("MM/dd/yyyy")
            dtpDate.Visible = False
            ActiveControl = dgvAmortization
        End If
    End Sub

End Class