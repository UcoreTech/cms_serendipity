﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditor_Amortization
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditor_Amortization))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnShowLoans = New System.Windows.Forms.Button()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.btnFind = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtConditionalValue = New System.Windows.Forms.TextBox()
        Me.cboCondition = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cboLoanTypes = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnGenerate = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtSearch = New System.Windows.Forms.TextBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.chkManual = New System.Windows.Forms.CheckBox()
        Me.lblSelFormula = New System.Windows.Forms.Label()
        Me.rdFormula = New System.Windows.Forms.RadioButton()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.rdCustomized = New System.Windows.Forms.RadioButton()
        Me.rdDeducted = New System.Windows.Forms.RadioButton()
        Me.rdAddon = New System.Windows.Forms.RadioButton()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.rdDim = New System.Windows.Forms.RadioButton()
        Me.rdStraight = New System.Windows.Forms.RadioButton()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.txtNoofPayment = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cboModeofPayment = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvLoans = New System.Windows.Forms.DataGridView()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.dtpDate = New System.Windows.Forms.MonthCalendar()
        Me.txtNumbRow = New System.Windows.Forms.NumericUpDown()
        Me.btnGenRow = New System.Windows.Forms.Button()
        Me.lblDesc = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.txtTotUnearned = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtTotAmortization = New System.Windows.Forms.TextBox()
        Me.txtTotServiceFee = New System.Windows.Forms.TextBox()
        Me.txtTotInterest = New System.Windows.Forms.TextBox()
        Me.txtTotPrincipal = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dgvAmortization = New System.Windows.Forms.DataGridView()
        Me.btnClearAmort = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.AutoSumToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CopyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PasteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel1.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.dgvLoans, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.txtNumbRow, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel7.SuspendLayout()
        CType(Me.dgvAmortization, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btnShowLoans)
        Me.Panel1.Controls.Add(Me.Panel6)
        Me.Panel1.Controls.Add(Me.btnGenerate)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.txtSearch)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.dgvLoans)
        Me.Panel1.Location = New System.Drawing.Point(12, 34)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(803, 390)
        Me.Panel1.TabIndex = 0
        '
        'btnShowLoans
        '
        Me.btnShowLoans.Location = New System.Drawing.Point(545, 3)
        Me.btnShowLoans.Name = "btnShowLoans"
        Me.btnShowLoans.Size = New System.Drawing.Size(253, 23)
        Me.btnShowLoans.TabIndex = 8
        Me.btnShowLoans.Text = "Show all Loans without Amortization"
        Me.btnShowLoans.UseVisualStyleBackColor = True
        '
        'Panel6
        '
        Me.Panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel6.Controls.Add(Me.btnFind)
        Me.Panel6.Controls.Add(Me.Label11)
        Me.Panel6.Controls.Add(Me.txtConditionalValue)
        Me.Panel6.Controls.Add(Me.cboCondition)
        Me.Panel6.Controls.Add(Me.Label10)
        Me.Panel6.Controls.Add(Me.cboLoanTypes)
        Me.Panel6.Controls.Add(Me.Label8)
        Me.Panel6.Controls.Add(Me.Label7)
        Me.Panel6.Location = New System.Drawing.Point(14, 239)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(305, 139)
        Me.Panel6.TabIndex = 7
        '
        'btnFind
        '
        Me.btnFind.Location = New System.Drawing.Point(240, 103)
        Me.btnFind.Name = "btnFind"
        Me.btnFind.Size = New System.Drawing.Size(53, 22)
        Me.btnFind.TabIndex = 7
        Me.btnFind.Text = "Find"
        Me.btnFind.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(131, 88)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(48, 13)
        Me.Label11.TabIndex = 6
        Me.Label11.Text = "Value :"
        '
        'txtConditionalValue
        '
        Me.txtConditionalValue.Location = New System.Drawing.Point(132, 104)
        Me.txtConditionalValue.Name = "txtConditionalValue"
        Me.txtConditionalValue.Size = New System.Drawing.Size(100, 21)
        Me.txtConditionalValue.TabIndex = 5
        '
        'cboCondition
        '
        Me.cboCondition.FormattingEnabled = True
        Me.cboCondition.Items.AddRange(New Object() {"Greater Than", "Less Than", "Equal"})
        Me.cboCondition.Location = New System.Drawing.Point(7, 104)
        Me.cboCondition.Name = "cboCondition"
        Me.cboCondition.Size = New System.Drawing.Size(121, 21)
        Me.cboCondition.TabIndex = 4
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(6, 85)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(70, 13)
        Me.Label10.TabIndex = 3
        Me.Label10.Text = "Condition :"
        '
        'cboLoanTypes
        '
        Me.cboLoanTypes.FormattingEnabled = True
        Me.cboLoanTypes.Location = New System.Drawing.Point(7, 43)
        Me.cboLoanTypes.Name = "cboLoanTypes"
        Me.cboLoanTypes.Size = New System.Drawing.Size(225, 21)
        Me.cboLoanTypes.TabIndex = 2
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 27)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(75, 13)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Loan Type :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(4, 6)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(35, 13)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Filter"
        '
        'btnGenerate
        '
        Me.btnGenerate.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnGenerate.Location = New System.Drawing.Point(326, 344)
        Me.btnGenerate.Name = "btnGenerate"
        Me.btnGenerate.Size = New System.Drawing.Size(133, 34)
        Me.btnGenerate.TabIndex = 6
        Me.btnGenerate.Text = "Generate"
        Me.btnGenerate.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(252, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Search :"
        '
        'txtSearch
        '
        Me.txtSearch.Location = New System.Drawing.Point(314, 5)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(225, 21)
        Me.txtSearch.TabIndex = 3
        '
        'Panel4
        '
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.chkManual)
        Me.Panel4.Controls.Add(Me.lblSelFormula)
        Me.Panel4.Controls.Add(Me.rdFormula)
        Me.Panel4.Controls.Add(Me.btnEdit)
        Me.Panel4.Controls.Add(Me.rdCustomized)
        Me.Panel4.Controls.Add(Me.rdDeducted)
        Me.Panel4.Controls.Add(Me.rdAddon)
        Me.Panel4.Controls.Add(Me.Panel5)
        Me.Panel4.Controls.Add(Me.Label6)
        Me.Panel4.Location = New System.Drawing.Point(465, 238)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(324, 140)
        Me.Panel4.TabIndex = 5
        '
        'chkManual
        '
        Me.chkManual.AutoSize = True
        Me.chkManual.Location = New System.Drawing.Point(245, 87)
        Me.chkManual.Name = "chkManual"
        Me.chkManual.Size = New System.Drawing.Size(66, 17)
        Me.chkManual.TabIndex = 8
        Me.chkManual.Text = "Manual"
        Me.chkManual.UseVisualStyleBackColor = True
        '
        'lblSelFormula
        '
        Me.lblSelFormula.AutoSize = True
        Me.lblSelFormula.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelFormula.Location = New System.Drawing.Point(150, 118)
        Me.lblSelFormula.Name = "lblSelFormula"
        Me.lblSelFormula.Size = New System.Drawing.Size(13, 13)
        Me.lblSelFormula.TabIndex = 7
        Me.lblSelFormula.Text = "-"
        '
        'rdFormula
        '
        Me.rdFormula.AutoSize = True
        Me.rdFormula.Location = New System.Drawing.Point(40, 118)
        Me.rdFormula.Name = "rdFormula"
        Me.rdFormula.Size = New System.Drawing.Size(96, 17)
        Me.rdFormula.TabIndex = 6
        Me.rdFormula.TabStop = True
        Me.rdFormula.Text = "Use Formula"
        Me.rdFormula.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Location = New System.Drawing.Point(150, 81)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(41, 23)
        Me.btnEdit.TabIndex = 5
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'rdCustomized
        '
        Me.rdCustomized.AutoSize = True
        Me.rdCustomized.Location = New System.Drawing.Point(40, 87)
        Me.rdCustomized.Name = "rdCustomized"
        Me.rdCustomized.Size = New System.Drawing.Size(92, 17)
        Me.rdCustomized.TabIndex = 4
        Me.rdCustomized.TabStop = True
        Me.rdCustomized.Text = "Customized"
        Me.rdCustomized.UseVisualStyleBackColor = True
        '
        'rdDeducted
        '
        Me.rdDeducted.AutoSize = True
        Me.rdDeducted.Location = New System.Drawing.Point(179, 61)
        Me.rdDeducted.Name = "rdDeducted"
        Me.rdDeducted.Size = New System.Drawing.Size(79, 17)
        Me.rdDeducted.TabIndex = 3
        Me.rdDeducted.TabStop = True
        Me.rdDeducted.Text = "Deducted"
        Me.rdDeducted.UseVisualStyleBackColor = True
        '
        'rdAddon
        '
        Me.rdAddon.AutoSize = True
        Me.rdAddon.Location = New System.Drawing.Point(40, 61)
        Me.rdAddon.Name = "rdAddon"
        Me.rdAddon.Size = New System.Drawing.Size(68, 17)
        Me.rdAddon.TabIndex = 2
        Me.rdAddon.TabStop = True
        Me.rdAddon.Text = "Add-On"
        Me.rdAddon.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.rdDim)
        Me.Panel5.Controls.Add(Me.rdStraight)
        Me.Panel5.Location = New System.Drawing.Point(8, 28)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(303, 26)
        Me.Panel5.TabIndex = 1
        '
        'rdDim
        '
        Me.rdDim.AutoSize = True
        Me.rdDim.Location = New System.Drawing.Point(171, 3)
        Me.rdDim.Name = "rdDim"
        Me.rdDim.Size = New System.Drawing.Size(91, 17)
        Me.rdDim.TabIndex = 1
        Me.rdDim.TabStop = True
        Me.rdDim.Text = "Diminishing"
        Me.rdDim.UseVisualStyleBackColor = True
        '
        'rdStraight
        '
        Me.rdStraight.AutoSize = True
        Me.rdStraight.Location = New System.Drawing.Point(32, 3)
        Me.rdStraight.Name = "rdStraight"
        Me.rdStraight.Size = New System.Drawing.Size(70, 17)
        Me.rdStraight.TabIndex = 0
        Me.rdStraight.TabStop = True
        Me.rdStraight.Text = "Straight"
        Me.rdStraight.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(109, 6)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(97, 13)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Interest Method"
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.txtNoofPayment)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.cboModeofPayment)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Location = New System.Drawing.Point(326, 239)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(133, 99)
        Me.Panel3.TabIndex = 4
        '
        'txtNoofPayment
        '
        Me.txtNoofPayment.Location = New System.Drawing.Point(6, 67)
        Me.txtNoofPayment.Name = "txtNoofPayment"
        Me.txtNoofPayment.Size = New System.Drawing.Size(118, 21)
        Me.txtNoofPayment.TabIndex = 3
        Me.txtNoofPayment.Text = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 51)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(104, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "No. of Payment :"
        '
        'cboModeofPayment
        '
        Me.cboModeofPayment.FormattingEnabled = True
        Me.cboModeofPayment.Items.AddRange(New Object() {"DAILY", "WEEKLY", "SEMI-MONTHLY", "MONTHLY", "QUARTERLY", "ANNUALLY", "LUMP SUM"})
        Me.cboModeofPayment.Location = New System.Drawing.Point(6, 23)
        Me.cboModeofPayment.Name = "cboModeofPayment"
        Me.cboModeofPayment.Size = New System.Drawing.Size(121, 21)
        Me.cboModeofPayment.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(3, 6)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(115, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Mode of Payment :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 4)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "List of All Loans"
        '
        'dgvLoans
        '
        Me.dgvLoans.AllowUserToAddRows = False
        Me.dgvLoans.AllowUserToDeleteRows = False
        Me.dgvLoans.AllowUserToResizeColumns = False
        Me.dgvLoans.AllowUserToResizeRows = False
        Me.dgvLoans.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvLoans.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvLoans.BackgroundColor = System.Drawing.Color.White
        Me.dgvLoans.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvLoans.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLoans.Location = New System.Drawing.Point(11, 32)
        Me.dgvLoans.Name = "dgvLoans"
        Me.dgvLoans.Size = New System.Drawing.Size(778, 199)
        Me.dgvLoans.TabIndex = 0
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(326, 290)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(369, 23)
        Me.ProgressBar1.TabIndex = 7
        Me.ProgressBar1.Visible = False
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.Color.White
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.dtpDate)
        Me.Panel2.Controls.Add(Me.txtNumbRow)
        Me.Panel2.Controls.Add(Me.btnGenRow)
        Me.Panel2.Controls.Add(Me.lblDesc)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.ProgressBar1)
        Me.Panel2.Controls.Add(Me.Panel7)
        Me.Panel2.Controls.Add(Me.btnClose)
        Me.Panel2.Controls.Add(Me.btnSave)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.dgvAmortization)
        Me.Panel2.Controls.Add(Me.btnClearAmort)
        Me.Panel2.Location = New System.Drawing.Point(12, 430)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(803, 318)
        Me.Panel2.TabIndex = 1
        '
        'dtpDate
        '
        Me.dtpDate.Location = New System.Drawing.Point(155, 38)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.TabIndex = 12
        Me.dtpDate.Visible = False
        '
        'txtNumbRow
        '
        Me.txtNumbRow.Location = New System.Drawing.Point(110, 3)
        Me.txtNumbRow.Name = "txtNumbRow"
        Me.txtNumbRow.Size = New System.Drawing.Size(120, 21)
        Me.txtNumbRow.TabIndex = 11
        '
        'btnGenRow
        '
        Me.btnGenRow.Location = New System.Drawing.Point(233, 3)
        Me.btnGenRow.Name = "btnGenRow"
        Me.btnGenRow.Size = New System.Drawing.Size(33, 23)
        Me.btnGenRow.TabIndex = 10
        Me.btnGenRow.Text = "+"
        Me.btnGenRow.UseVisualStyleBackColor = True
        '
        'lblDesc
        '
        Me.lblDesc.AutoSize = True
        Me.lblDesc.Location = New System.Drawing.Point(592, 8)
        Me.lblDesc.Name = "lblDesc"
        Me.lblDesc.Size = New System.Drawing.Size(12, 13)
        Me.lblDesc.TabIndex = 9
        Me.lblDesc.Text = "-"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(506, 8)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(80, 13)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Description :"
        '
        'Panel7
        '
        Me.Panel7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel7.Controls.Add(Me.txtTotUnearned)
        Me.Panel7.Controls.Add(Me.Label30)
        Me.Panel7.Controls.Add(Me.txtTotAmortization)
        Me.Panel7.Controls.Add(Me.txtTotServiceFee)
        Me.Panel7.Controls.Add(Me.txtTotInterest)
        Me.Panel7.Controls.Add(Me.txtTotPrincipal)
        Me.Panel7.Controls.Add(Me.Label26)
        Me.Panel7.Location = New System.Drawing.Point(14, 235)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(775, 49)
        Me.Panel7.TabIndex = 5
        '
        'txtTotUnearned
        '
        Me.txtTotUnearned.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotUnearned.BackColor = System.Drawing.Color.White
        Me.txtTotUnearned.ForeColor = System.Drawing.Color.Red
        Me.txtTotUnearned.Location = New System.Drawing.Point(219, 23)
        Me.txtTotUnearned.Name = "txtTotUnearned"
        Me.txtTotUnearned.ReadOnly = True
        Me.txtTotUnearned.Size = New System.Drawing.Size(144, 21)
        Me.txtTotUnearned.TabIndex = 63
        Me.txtTotUnearned.Text = "0.00"
        Me.txtTotUnearned.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(70, 26)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(146, 13)
        Me.Label30.TabIndex = 62
        Me.Label30.Text = "Total Advance Interest :"
        '
        'txtTotAmortization
        '
        Me.txtTotAmortization.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotAmortization.Location = New System.Drawing.Point(520, 2)
        Me.txtTotAmortization.Name = "txtTotAmortization"
        Me.txtTotAmortization.ReadOnly = True
        Me.txtTotAmortization.Size = New System.Drawing.Size(142, 21)
        Me.txtTotAmortization.TabIndex = 61
        Me.txtTotAmortization.Text = "0.00"
        Me.txtTotAmortization.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotServiceFee
        '
        Me.txtTotServiceFee.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotServiceFee.Location = New System.Drawing.Point(370, 2)
        Me.txtTotServiceFee.Name = "txtTotServiceFee"
        Me.txtTotServiceFee.ReadOnly = True
        Me.txtTotServiceFee.Size = New System.Drawing.Size(142, 21)
        Me.txtTotServiceFee.TabIndex = 60
        Me.txtTotServiceFee.Text = "0.00"
        Me.txtTotServiceFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotInterest
        '
        Me.txtTotInterest.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotInterest.Location = New System.Drawing.Point(219, 2)
        Me.txtTotInterest.Name = "txtTotInterest"
        Me.txtTotInterest.ReadOnly = True
        Me.txtTotInterest.Size = New System.Drawing.Size(144, 21)
        Me.txtTotInterest.TabIndex = 59
        Me.txtTotInterest.Text = "0.00"
        Me.txtTotInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotPrincipal
        '
        Me.txtTotPrincipal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTotPrincipal.Location = New System.Drawing.Point(61, 3)
        Me.txtTotPrincipal.Name = "txtTotPrincipal"
        Me.txtTotPrincipal.ReadOnly = True
        Me.txtTotPrincipal.Size = New System.Drawing.Size(154, 21)
        Me.txtTotPrincipal.TabIndex = 58
        Me.txtTotPrincipal.Text = "0.00"
        Me.txtTotPrincipal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(5, 9)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(50, 13)
        Me.Label26.TabIndex = 57
        Me.Label26.Text = "Totals: "
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(714, 290)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnSave.Image = Global.WindowsApplication2.My.Resources.Resources.save2
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(155, 290)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 3
        Me.btnSave.Text = "Save"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(14, 8)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(97, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Generate Row :"
        '
        'dgvAmortization
        '
        Me.dgvAmortization.AllowUserToAddRows = False
        Me.dgvAmortization.AllowUserToDeleteRows = False
        Me.dgvAmortization.AllowUserToResizeColumns = False
        Me.dgvAmortization.AllowUserToResizeRows = False
        Me.dgvAmortization.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvAmortization.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvAmortization.BackgroundColor = System.Drawing.Color.White
        Me.dgvAmortization.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvAmortization.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAmortization.Location = New System.Drawing.Point(14, 27)
        Me.dgvAmortization.Name = "dgvAmortization"
        Me.dgvAmortization.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAmortization.Size = New System.Drawing.Size(775, 202)
        Me.dgvAmortization.TabIndex = 1
        '
        'btnClearAmort
        '
        Me.btnClearAmort.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnClearAmort.Image = CType(resources.GetObject("btnClearAmort.Image"), System.Drawing.Image)
        Me.btnClearAmort.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClearAmort.Location = New System.Drawing.Point(14, 290)
        Me.btnClearAmort.Name = "btnClearAmort"
        Me.btnClearAmort.Size = New System.Drawing.Size(135, 23)
        Me.btnClearAmort.TabIndex = 0
        Me.btnClearAmort.Text = "Clear Amortization"
        Me.btnClearAmort.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClearAmort.UseVisualStyleBackColor = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AutoSumToolStripMenuItem, Me.CopyToolStripMenuItem, Me.PasteToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(827, 24)
        Me.MenuStrip1.TabIndex = 2
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'AutoSumToolStripMenuItem
        '
        Me.AutoSumToolStripMenuItem.Image = CType(resources.GetObject("AutoSumToolStripMenuItem.Image"), System.Drawing.Image)
        Me.AutoSumToolStripMenuItem.Name = "AutoSumToolStripMenuItem"
        Me.AutoSumToolStripMenuItem.Size = New System.Drawing.Size(81, 20)
        Me.AutoSumToolStripMenuItem.Text = "Auto Sum"
        '
        'CopyToolStripMenuItem
        '
        Me.CopyToolStripMenuItem.Image = CType(resources.GetObject("CopyToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CopyToolStripMenuItem.Name = "CopyToolStripMenuItem"
        Me.CopyToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.CopyToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.CopyToolStripMenuItem.Text = "Copy"
        '
        'PasteToolStripMenuItem
        '
        Me.PasteToolStripMenuItem.Image = CType(resources.GetObject("PasteToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PasteToolStripMenuItem.Name = "PasteToolStripMenuItem"
        Me.PasteToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.V), System.Windows.Forms.Keys)
        Me.PasteToolStripMenuItem.Size = New System.Drawing.Size(62, 20)
        Me.PasteToolStripMenuItem.Text = "Paste"
        '
        'frmEditor_Amortization
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.ClientSize = New System.Drawing.Size(827, 758)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmEditor_Amortization"
        Me.Text = "Loan Amortization Editor"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.dgvLoans, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.txtNumbRow, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        CType(Me.dgvAmortization, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvLoans As System.Windows.Forms.DataGridView
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dgvAmortization As System.Windows.Forms.DataGridView
    Friend WithEvents btnClearAmort As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents txtNoofPayment As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cboModeofPayment As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents btnGenerate As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents rdFormula As System.Windows.Forms.RadioButton
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents rdCustomized As System.Windows.Forms.RadioButton
    Friend WithEvents rdDeducted As System.Windows.Forms.RadioButton
    Friend WithEvents rdAddon As System.Windows.Forms.RadioButton
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents rdDim As System.Windows.Forms.RadioButton
    Friend WithEvents rdStraight As System.Windows.Forms.RadioButton
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cboLoanTypes As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents txtTotUnearned As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txtTotAmortization As System.Windows.Forms.TextBox
    Friend WithEvents txtTotServiceFee As System.Windows.Forms.TextBox
    Friend WithEvents txtTotInterest As System.Windows.Forms.TextBox
    Friend WithEvents txtTotPrincipal As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents lblSelFormula As System.Windows.Forms.Label
    Friend WithEvents lblDesc As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtConditionalValue As System.Windows.Forms.TextBox
    Friend WithEvents cboCondition As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnFind As System.Windows.Forms.Button
    Friend WithEvents btnShowLoans As System.Windows.Forms.Button
    Friend WithEvents txtNumbRow As System.Windows.Forms.NumericUpDown
    Friend WithEvents btnGenRow As System.Windows.Forms.Button
    Friend WithEvents chkManual As System.Windows.Forms.CheckBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents AutoSumToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CopyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PasteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dtpDate As System.Windows.Forms.MonthCalendar
End Class
