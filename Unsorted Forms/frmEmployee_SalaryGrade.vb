Imports System.Data.SqlClient
Public Class Salary_Grade
  
#Region "Variables"
    Private salarygradeID As String
#End Region
#Region "Get Propterty"
    Private Property getsalarygradeID() As String
        Get
            Return salarygradeID
        End Get
        Set(ByVal value As String)
            salarygradeID = value
        End Set
    End Property
#End Region
    Private Sub btnnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Try
            If btnnew.Text = "New" Then
                Me.cbojobrank.Focus()
                Label1.Text = "Add Salary Grade"
                Call cleartxt()
                btnnew.Text = "Save"
                btnnew.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(72, 8)
             
                Me.btndelete.Visible = False
                Me.btnupdate.Visible = False
            ElseIf btnnew.Text = "Save" Then
                If Me.cbojobrank.Text <> "" And Me.txtsalarycode.Text <> "" Then
                    Label1.Text = "Salary Grade"
                    Call check_insertrate_validation()
                    Call cleartxt()
                    btnnew.Text = "New"
                    btnnew.Image = My.Resources.new3
                    btnclose.Text = "Close"
                    btnclose.Location = New System.Drawing.Point(237, 8)
                    btndelete.Visible = True
                    btnupdate.Visible = True
                Else
                    MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
#Region "Salary Code validation/stored proc"
    Public Sub check_insertrate_validation()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_salary_grade_code_valid_get", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@SalaryCode", SqlDbType.VarChar, 10).Value = Me.txtsalarycode.Text
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.HasRows Then
                MessageBox.Show("Data already exist, please try another one", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else
                Call add_salarygrade()
                Call load_salarygrade()
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region
    Public Sub cleartxt()
        Me.txtsalarycode.Text = ""
        Me.cbojobrank.Text = ""
        Me.txtfrom.Text = ""
        Me.txtto.Text = ""
    End Sub
#Region "Salary Grade listview get/stored proc"
    Public Sub load_salarygrade()

        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_salary_grade_get", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With Me.LvlSalaryGrade
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Job Rank", 100, HorizontalAlignment.Left)
            .Columns.Add("Salary Grade", 100, HorizontalAlignment.Left)
            .Columns.Add("Salary Range From", 100, HorizontalAlignment.Left)
            .Columns.Add("Salary Range To", 100, HorizontalAlignment.Left)
            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .subitems.add(myreader.Item(1))
                        .subitems.add(myreader.Item(2))
                        .subitems.add(myreader.Item(3))
                        .subitems.add(myreader.Item(4)) 'salaryid
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
#End Region
#Region "Salary Grade Insert/stored proc"
    Public Sub add_salarygrade()

        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_salary_grade_insert", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@job_rank", SqlDbType.VarChar, 50).Value = Me.cbojobrank.Text
        cmd.Parameters.Add("@salarycode", SqlDbType.VarChar, 10).Value = Me.txtsalarycode.Text
        cmd.Parameters.Add("@salary1", SqlDbType.VarChar, 50).Value = Me.txtfrom.Text
        cmd.Parameters.Add("@salary2", SqlDbType.VarChar, 50).Value = Me.txtto.Text

        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region

    Private Sub Salary_Grade_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Call load_salarygrade()
        Call load_jobrank()

    End Sub
    Public Sub load_jobrank()
        Dim myconnection As New Clsappconfiguration
        Dim cmdjol As String = "select job_rank from dbo.Per_Employee_JobRank"
        Dim cmd As New SqlCommand(cmdjol, myconnection.sqlconn)
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read()
                Me.cbojobrank.Items.Add(myreader(0))
            End While
        Finally
            myconnection.sqlconn.Close()
            myreader.Close()
        End Try
    End Sub
    Public Sub load_position_cbobox()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        'Dim myconnection As New SqlConnection(My.Settings.CNString)

        Dim cmdjol As String = "select description from dbo.Tapplicant_list_Position"
        Dim cmd As New SqlCommand(cmdjol, myconnection.sqlconn)
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read()
                Me.cboposition.Items.Add(myreader(0))
            End While
        Finally
            myconnection.sqlconn.Close()
            myreader.Close()
        End Try
    End Sub
    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If btnupdate.Text = "Edit" Then
                Me.cbojobrank.Focus()
                Label1.Text = "Edit Salary Grade"
                Me.cbojobrank.Text = Me.LvlSalaryGrade.SelectedItems(0).Text
                Me.cbojobrank.Tag = Me.LvlSalaryGrade.SelectedItems(0).Text
                Me.txtsalarycode.Text = Me.LvlSalaryGrade.SelectedItems(0).SubItems(1).Text
                Me.txtsalarycode.Tag = Me.LvlSalaryGrade.SelectedItems(0).SubItems(1).Text
                Me.txtfrom.Text = Me.LvlSalaryGrade.SelectedItems(0).SubItems(2).Text
                Me.txtto.Text = Me.LvlSalaryGrade.SelectedItems(0).SubItems(3).Text
                getsalarygradeID = Me.LvlSalaryGrade.SelectedItems(0).SubItems(4).Text
                Me.btnnew.Enabled = False
                Me.btndelete.Visible = False
                Me.btnupdate.Text = "Update"
                Me.btnupdate.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(140, 8)

            ElseIf btnupdate.Text = "Update" Then
                If Me.cbojobrank.Text <> "" And Me.txtsalarycode.Text <> "" Then
                    If Me.cbojobrank.Text = cbojobrank.Tag And Me.txtsalarycode.Text = Me.txtsalarycode.Tag Then
                        Label1.Text = "Salary Grade"
                        Call update_salarygrade()
                        Call load_salarygrade()
                        Call cleartxt()
                        btnupdate.Text = "Edit"
                        Me.btnupdate.Image = My.Resources.edit1
                        btnclose.Text = "Cancel"
                        btnclose.Location = New System.Drawing.Point(237, 8)
                        btnclose.Text = "Close"
                        Me.btnnew.Enabled = True
                        Me.btndelete.Visible = True
                    ElseIf cbojobrank.Text <> cbojobrank.Tag Then
                        Label1.Text = "Salary Grade"

                        Call update_salarygrade()
                        Call load_salarygrade()
                        Call cleartxt()

                        btnupdate.Text = "Edit"
                        Me.btnupdate.Image = My.Resources.edit1
                        btnclose.Text = "Cancel"
                        btnclose.Location = New System.Drawing.Point(237, 8)
                        btnclose.Text = "Close"
                        Me.btnnew.Enabled = True
                        Me.btndelete.Visible = True
                    ElseIf Me.txtsalarycode.Text <> Me.txtsalarycode.Tag Then
                        Label1.Text = "Salary Grade"
                        Call CHECK_CODE_DUPLICATE()
                        Call cleartxt()
                        btnupdate.Text = "Edit"
                        Me.btnupdate.Image = My.Resources.edit1
                        btnclose.Text = "Cancel"
                        btnclose.Location = New System.Drawing.Point(237, 8)
                        btnclose.Text = "Close"
                        Me.btnnew.Enabled = True
                        Me.btndelete.Visible = True
                    End If
                Else
                    MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
    Public Sub CHECK_RANK_DUPLICATE()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("USP_SALARYGRADE_CHECK_CODE", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@Job_rank", SqlDbType.Char, 30).Value = Me.cbojobrank.Text
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.HasRows Then
                MessageBox.Show("Data is already exist , Please try another one", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else
                Call update_salarygrade()
                Call load_salarygrade()
            End If
        Catch ex As Exception

        End Try
    End Sub
#Region "Salary code validation get1/stored proc"
    Public Sub CHECK_CODE_DUPLICATE()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_salary_grade_code_valid1_get", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@salarycode", SqlDbType.VarChar, 10).Value = Me.txtsalarycode.Text
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.HasRows Then
                MessageBox.Show("Data is already exist , Please try another one", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else
                Call update_salarygrade()
                Call load_salarygrade()
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region
#Region "Salary Grade Update/stored proc"
    Public Sub update_salarygrade()

        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_salary_grade_update", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@salaryid", SqlDbType.Int).Value = salarygradeID
        cmd.Parameters.Add("@job_rank", SqlDbType.VarChar, 50).Value = Me.cbojobrank.Text
        cmd.Parameters.Add("@salarycode", SqlDbType.VarChar, 10).Value = Me.txtsalarycode.Text
        cmd.Parameters.Add("@salary1", SqlDbType.VarChar, 50).Value = Me.txtfrom.Text
        cmd.Parameters.Add("@salary2", SqlDbType.VarChar, 50).Value = Me.txtto.Text

        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        getsalarygradeID = Me.LvlSalaryGrade.SelectedItems(0).SubItems(4).Text
        Dim x As New DialogResult
        x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        If x = System.Windows.Forms.DialogResult.OK Then
            'If Me.grdsalarygrade.SelectedRows.Count > 0 AndAlso _
            '          Not Me.grdsalarygrade.SelectedRows(0).Index = _
            '          Me.grdsalarygrade.Rows.Count - 1 Then
            Call delete_salarygrade()
            Call load_salarygrade()
            '    Me.grdsalarygrade.Rows.RemoveAt( _
            '        Me.grdsalarygrade.SelectedRows(0).Index)
            'End If
        ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
        End If
    End Sub
#Region "Salary Grage Delete/Stored proc"
    Public Sub delete_salarygrade()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_salary_grade_delete", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@SALARYID", SqlDbType.Int).Value = salarygradeID

        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Cancel" Then
            Label1.Text = "Salary Grade"

            btnnew.Text = "New"
            btnnew.Image = My.Resources.new3
            btnupdate.Text = "Edit"
            Me.btnupdate.Image = My.Resources.edit1
            btnnew.Enabled = True
            btndelete.Visible = True
            btnupdate.Visible = True
            btnclose.Text = "Close"
            btnclose.Location = New System.Drawing.Point(237, 8)
        ElseIf btnclose.Text = "Close" Then
            Me.Close()
        End If
    End Sub

    Private Sub grdsalarygrade_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        'Me.cboposition.Text = grdsalarygrade.CurrentRow.Cells(1).Value
        'Me.txtjobrank.Text = grdsalarygrade.CurrentRow.Cells(2).Value
        'btnupdate.Visible = True
        'btnupdate.Location = New System.Drawing.Point(2, 232)
        'grdsalarygrade.Hide()
        'btnclose.Text = "Cancel"
        'btnclose.Location = New System.Drawing.Point(92, 232)
        'Me.txtsalarycode.Text = grdsalarygrade.CurrentRow.Cells(3).Value
        'Me.txtfrom.Text = grdsalarygrade.CurrentRow.Cells(4).Value
        'Me.txtto.Text = grdsalarygrade.CurrentRow.Cells(5).Value
    End Sub

    Private Sub txtfrom_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtfrom.KeyPress
        e.Handled = Not txtfrom_Validate(e.KeyChar)
    End Sub

    Public Function txtfrom_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = ".") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")
            'tphone.Focus()
            Return False
        Else
            Return True
        End If
    End Function
    Private Sub txtfrom_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtfrom.Validating
        If txtfrom.Text = "" Then
            'MessageBox.Show("Please supply the blank space", "eHRMS")
            'tphone.Focus()
        Else

            If Not IsNumeric(txtfrom.Text) Then
                ErrorProvider1.SetError(txtfrom, "Please write correct number format")
                'tphone.Focus()
                ErrorProvider1.SetError(txtfrom, "")
            Else
                txtfrom.Text = Format(Decimal.Parse(txtfrom.Text), "#########.00")

            End If

        End If
    End Sub

    Private Sub txtto_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtto.KeyPress
        e.Handled = Not txtto_Validate(e.KeyChar)
    End Sub
    Private Sub txtto_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtto.Validating
        If txtto.Text = "" Then
            'MessageBox.Show("Please supply the blank space", "eHRMS")
            'tphone.Focus()
        Else

            If Not IsNumeric(txtto.Text) Then
                ErrorProvider1.SetError(txtto, "Please write correct number format")
                'tphone.Focus()
                ErrorProvider1.SetError(txtto, "")
            Else
                txtto.Text = Format(Decimal.Parse(txtto.Text), "#########.00")

            End If

        End If
    End Sub
    Public Function txtto_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = ".") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")
            'tphone.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub cbojobrank_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cbojobrank.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub txtsalarycode_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtsalarycode.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub



    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label2.Click

    End Sub

    Private Sub cbojobrank_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbojobrank.SelectedIndexChanged

    End Sub
End Class
