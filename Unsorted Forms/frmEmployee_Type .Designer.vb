<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Employee_Position
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Employee_Position))
        Me.tcode = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnemp_position = New System.Windows.Forms.Button()
        Me.btnemp_deleteposition = New System.Windows.Forms.Button()
        Me.btnemp_editposition = New System.Windows.Forms.Button()
        Me.btnemp_addposition = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tdescription = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.LvlEmployeeType = New System.Windows.Forms.ListView()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.PanePanel2 = New WindowsApplication2.PanePanel()
        Me.PanePanel1.SuspendLayout()
        Me.PanePanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'tcode
        '
        Me.tcode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.tcode, "tcode")
        Me.tcode.Name = "tcode"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'btnemp_position
        '
        Me.btnemp_position.Image = Global.WindowsApplication2.My.Resources.Resources.eventlogError
        resources.ApplyResources(Me.btnemp_position, "btnemp_position")
        Me.btnemp_position.Name = "btnemp_position"
        Me.btnemp_position.UseVisualStyleBackColor = True
        '
        'btnemp_deleteposition
        '
        resources.ApplyResources(Me.btnemp_deleteposition, "btnemp_deleteposition")
        Me.btnemp_deleteposition.Name = "btnemp_deleteposition"
        Me.btnemp_deleteposition.UseVisualStyleBackColor = True
        '
        'btnemp_editposition
        '
        resources.ApplyResources(Me.btnemp_editposition, "btnemp_editposition")
        Me.btnemp_editposition.Name = "btnemp_editposition"
        Me.btnemp_editposition.UseVisualStyleBackColor = True
        '
        'btnemp_addposition
        '
        resources.ApplyResources(Me.btnemp_addposition, "btnemp_addposition")
        Me.btnemp_addposition.Name = "btnemp_addposition"
        Me.btnemp_addposition.UseVisualStyleBackColor = True
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Name = "Label1"
        '
        'tdescription
        '
        Me.tdescription.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        resources.ApplyResources(Me.tdescription, "tdescription")
        Me.tdescription.Name = "tdescription"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'LvlEmployeeType
        '
        resources.ApplyResources(Me.LvlEmployeeType, "LvlEmployeeType")
        Me.LvlEmployeeType.Name = "LvlEmployeeType"
        Me.LvlEmployeeType.UseCompatibleStateImageBehavior = False
        '
        'PanePanel1
        '
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.Label1)
        resources.ApplyResources(Me.PanePanel1, "PanePanel1")
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel1.Name = "PanePanel1"
        '
        'PanePanel2
        '
        Me.PanePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel2.Controls.Add(Me.btnemp_deleteposition)
        Me.PanePanel2.Controls.Add(Me.btnemp_position)
        Me.PanePanel2.Controls.Add(Me.btnemp_addposition)
        Me.PanePanel2.Controls.Add(Me.btnemp_editposition)
        resources.ApplyResources(Me.PanePanel2, "PanePanel2")
        Me.PanePanel2.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel2.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel2.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel2.Name = "PanePanel2"
        '
        'Employee_Position
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        resources.ApplyResources(Me, "$this")
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.PanePanel2)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.LvlEmployeeType)
        Me.Controls.Add(Me.tdescription)
        Me.Controls.Add(Me.tcode)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Employee_Position"
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.PanePanel2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tcode As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnemp_addposition As System.Windows.Forms.Button
    Friend WithEvents btnemp_editposition As System.Windows.Forms.Button
    Friend WithEvents btnemp_deleteposition As System.Windows.Forms.Button
    Friend WithEvents btnemp_position As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tdescription As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents LvlEmployeeType As System.Windows.Forms.ListView
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents PanePanel2 As WindowsApplication2.PanePanel
    'Friend WithEvents DataSet1 As WindowsApplication2.DataSet1
End Class
