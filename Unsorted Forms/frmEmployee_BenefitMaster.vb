Imports System.Data.SqlClient
Public Class frmEmployee_BenefitMaster

    Public stype(5000) As String
    Public sdescription(5000) As String
    Public sidnumber(5000) As String
#Region "variable"
    Private recid As String
#End Region
#Region "Get Property"
    Private Property getrecid() As String
        Get
            Return recid
        End Get
        Set(ByVal value As String)
            recid = value
        End Set
    End Property
#End Region
    Private Sub btnnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Try
            If btnnew.Text = "New" Then
                Me.txttype.Focus()
                Label1.Text = "Add Benefit Master"
                Call cleartxt()
                btndelete.Visible = False
                btnupdate.Visible = False
                btnnew.Text = "Save"
                btnnew.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(71, 7)
            ElseIf btnnew.Text = "Save" Then
                If Me.txttype.Text <> "" And Me.txtdescription.Text <> "" Then
                    Label1.Text = "Benefit Master"
                    Call check_insertbenefitmaster_validation()
                    Call cleartxt()
                    Me.txttype.Focus()
                    btnnew.Text = "New"
                    btnnew.Image = My.Resources.new3
                    btnclose.Text = "Close"
                    btnclose.Location = New System.Drawing.Point(215, 7)
                    btndelete.Visible = True
                    btnupdate.Visible = True
                Else
                    MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
#Region "Benefit Master code desc validation/storedproc"
    Public Sub check_insertbenefitmaster_validation()

        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_benefit_master_codedesc_valid_get", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@Type", SqlDbType.VarChar, 50).Value = Me.txttype.Text
        cmd.Parameters.Add("@Description", SqlDbType.VarChar, 50).Value = Me.txtdescription.Text
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.HasRows Then
                MessageBox.Show("Data already exist, please try another one", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else
                Call INSERT_BENEFITMASTERFILE()
                Call load_benefitmasterfile()
            End If
            myreader.Close()
            myconnection.sqlconn.Close()
        Catch ex As Exception
        End Try
    End Sub
#End Region
#Region "Benefit Master Code validation/stored proc"
    Public Sub check_updatebenefitmaster_validation()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_benefit_master_code_valid_get", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@Type", SqlDbType.VarChar, 50).Value = Me.txttype.Text
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.HasRows Then
                MessageBox.Show("That Benefit code " + " ' " + Me.txttype.Text + " ' " + " is already used , Please try another one", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else
                Call Update_benefitmasterfile()
                Call load_benefitmasterfile()
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region
#Region "Benefit Master Description validation/stored proc"
    Public Sub check_updatebenefitdescription_validation()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_benefit_master_desc_valid_get", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@description", SqlDbType.VarChar, 50).Value = Me.txtdescription.Text
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.HasRows Then
                MessageBox.Show("That Benefit Description " + " ' " + Me.txtdescription.Text + " ' " + " is already used , Please try another one", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else
                Call Update_benefitmasterfile()
                Call load_benefitmasterfile()
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region
    Public Sub cleartxt()
        Me.txttype.Text = ""
        Me.txtdescription.Text = ""
    End Sub
#Region "Benefit Master Insert/stored proc"
    Public Sub INSERT_BENEFITMASTERFILE()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_benefit_master_insert", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@type", SqlDbType.VarChar, 50).Value = Me.txttype.Text
        cmd.Parameters.Add("@description", SqlDbType.VarChar, 50).Value = Me.txtdescription.Text
        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region
#Region "Benefit Master listview Get/stored proc"
    Public Sub load_benefitmasterfile()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_benefitmaster_get", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With Me.lvlBenefitMaster
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Benefit Code", 100, HorizontalAlignment.Left)
            .Columns.Add("Benefit Description", 200, HorizontalAlignment.Left)
            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .subitems.add(myreader.Item(1))
                        .subitems.add(myreader.Item(2))
                        .subitems.add(myreader.Item(3)) 'recid
                        .subitems.add(myreader.Item(4)) 'fxkey
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
#End Region

    Private Sub Benefit_Master_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call load_benefitmasterfile()
        'load_mtcgcombobox()
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If btnupdate.Text = "Edit" Then
                Me.txttype.Focus()
                Label1.Text = "Edit Benefit Master"
                Me.btnnew.Enabled = False
                Me.txttype.Text = Me.lvlBenefitMaster.SelectedItems(0).Text
                Me.txttype.Tag = Me.lvlBenefitMaster.SelectedItems(0).Text
                Me.txtdescription.Text = Me.lvlBenefitMaster.SelectedItems(0).SubItems(1).Text
                Me.txtdescription.Tag = Me.lvlBenefitMaster.SelectedItems(0).SubItems(1).Text
                getrecid = Me.lvlBenefitMaster.SelectedItems(0).SubItems(3).Text
                btnupdate.Text = "Update"
                btnupdate.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(138, 7)
                btndelete.Visible = False

            ElseIf btnupdate.Text = "Update" Then
                If Me.txttype.Text <> "" And Me.txtdescription.Text <> "" Then
                    If Me.txttype.Text = Me.txttype.Tag And Me.txtdescription.Text = Me.txtdescription.Tag Then
                        Label1.Text = "Benefit Master"
                        Call Update_benefitmasterfile()
                        Call load_benefitmasterfile()
                        Call cleartxt()
                        btndelete.Visible = True
                        btnupdate.Text = "Edit"
                        btnupdate.Image = My.Resources.edit1
                        btnclose.Text = "Cancel"
                        btnclose.Location = New System.Drawing.Point(215, 7)
                        btnclose.Text = "Close"
                        Me.btnnew.Enabled = True
                        Me.btndelete.Visible = True
                    ElseIf Me.txttype.Text <> Me.txttype.Tag Then
                        Label1.Text = "Benefit Master"
                        Call check_updatebenefitmaster_validation()
                        Call cleartxt()
                        btndelete.Visible = True
                        btnupdate.Text = "Edit"
                        btnupdate.Image = My.Resources.edit1
                        btnclose.Text = "Cancel"
                        btnclose.Location = New System.Drawing.Point(215, 7)
                        btnclose.Text = "Close"
                        Me.btnnew.Enabled = True
                        Me.btndelete.Visible = True
                    ElseIf Me.txtdescription.Text <> Me.txtdescription.Tag Then
                        Label1.Text = "Benefit Master"
                        Call check_updatebenefitdescription_validation()
                        Call cleartxt()
                        btndelete.Visible = True
                        btnupdate.Text = "Edit"
                        btnupdate.Image = My.Resources.edit1
                        btnclose.Text = "Cancel"
                        btnclose.Location = New System.Drawing.Point(215, 7)
                        btnclose.Text = "Close"
                        Me.btnnew.Enabled = True
                        Me.btndelete.Visible = True
                    End If
                Else
                    MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
#Region "Benefit Master Update/stored proc"
    Public Sub Update_benefitmasterfile()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_benefit_master_update", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@recid", SqlDbType.Int, 4).Value = recid
        cmd.Parameters.Add("@type", SqlDbType.VarChar, 50).Value = Me.txttype.Text
        cmd.Parameters.Add("@description", SqlDbType.VarChar, 50).Value = Me.txtdescription.Text
        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        getrecid = Me.lvlBenefitMaster.SelectedItems(0).SubItems(3).Text
        Dim x As New DialogResult
        x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        If x = System.Windows.Forms.DialogResult.OK Then
            'If Me.grdbenefitmaster.SelectedRows.Count > 0 AndAlso _
            '    Not Me.grdbenefitmaster.SelectedRows(0).Index = _
            '     Me.grdbenefitmaster.Rows.Count - 1 Then

            Call delete_benefitmasterfile()
            Call load_benefitmasterfile()
            '    Me.grdbenefitmaster.Rows.RemoveAt( _
            '        Me.grdbenefitmaster.SelectedRows(0).Index)
            'End If
        ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
        End If
    End Sub
#Region "Benefit Master Delete/sotred proc"
    Public Sub delete_benefitmasterfile()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_benefit_master_delete", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@recid", SqlDbType.Int, 4).Value = recid
        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Cancel" Then
            Label1.Text = "Benefit Master"

            btnnew.Text = "New"
            btnnew.Image = My.Resources.new3
            btnupdate.Text = "Edit"
            btnupdate.Image = My.Resources.edit1
            btnclose.Text = "Close"
            btnclose.Location = New System.Drawing.Point(215, 7)
            btndelete.Visible = True
            btnupdate.Visible = True
            btnnew.Enabled = True
        ElseIf btnclose.Text = "Close" Then
            Me.Close()
        End If
    End Sub

  


    Private Sub txttype_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txttype.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub txtdescription_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtdescription.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Public Sub load_mtcgcombobox()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        'Dim myconnection As New SqlConnection(My.Settings.CNString)
        Dim cmd As New SqlCommand
        Dim myadapter As New SqlDataAdapter
        Dim mydataset As New DataSet
        Dim data As New DataTable
        cmd.CommandText = "USP_LOAD_BENEFITMASTERFILE"
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = myconnection.sqlconn
        myconnection.sqlconn.Open()

        myadapter.SelectCommand = cmd
        myadapter.Fill(mydataset, "Temp_Benefit_Master")


        ''Me.samplebox.DataSource = mydataset
        ''Me.samplebox.DisplayMember = "Temp_Benefit_Master"

        'Me.samplebox.Items.Clear()
        'Me.samplebox.LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
        'Me.samplebox.SourceDataString = New String(3) {"recid", "type", "description", "Temp_Benefit_Master"}
        'Me.samplebox.SourceDataTable = data

        myconnection.sqlconn.Close()

    End Sub

    Private Sub grdbenefitmaster_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)

    End Sub
End Class