﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmAmortOtherPayment

    Private con As New Clsappconfiguration
    Dim i As Integer

    Public mmode As String = ""
    Public noofPayment As String = ""

    Public Sub _AddAcct(ByVal code As String, ByVal title As String)
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_AMORTIZATION_SetupOtherPayments",
                                       New SqlParameter("@fcAccountCode", code),
                                       New SqlParameter("@fcAccountTitle", title))
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub _LoadAcct()
        Try
            _setupCol()
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_AMORTIZATION_SelectOtherPayments")
            With dgvPayment
                While rd.Read
                    .Rows.Add()
                    .Item(0, i).Value = rd(0)
                    .Item(1, i).Value = "0.00"
                    .Item(2, i).Value = mmode
                    .Item(3, i).Value = noofPayment
                    .Item(4, i).Value = "0.00"
                    .Item(0, i).Tag = rd(1)
                    i += 1
                End While
            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub _setupCol()
        i = 0
        dgvPayment.DataSource = Nothing
        dgvPayment.Rows.Clear()
        dgvPayment.Columns.Clear()
        Dim row1 As New DataGridViewTextBoxColumn
        Dim amt As New DataGridViewTextBoxColumn
        Dim xmode As New DataGridViewTextBoxColumn
        Dim payno As New DataGridViewTextBoxColumn
        Dim totpay As New DataGridViewTextBoxColumn
        With row1
            .Name = "row1"
            .HeaderText = "Other Payments"
            .ReadOnly = True
            .Width = 250
        End With
        With amt
            .Name = "amt"
            .HeaderText = "Amount"
            .ReadOnly = False
        End With
        With xmode
            .Name = "xmode"
            .HeaderText = "Mode of Payment"
            .ReadOnly = True
        End With
        With payno
            .Name = "payno"
            .HeaderText = "No. of Payment"
            .ReadOnly = True
        End With
        With totpay
            .Name = "totpay"
            .HeaderText = "Total Payment"
            .ReadOnly = False
        End With
        With dgvPayment
            .Columns.Add(row1)
            .Columns.Add(amt)
            .Columns.Add(xmode)
            .Columns.Add(payno)
            .Columns.Add(totpay)
        End With
    End Sub

    Private Sub frmAmortOtherPayment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _LoadAcct()
    End Sub

    Private Sub SetupPaymentsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SetupPaymentsToolStripMenuItem.Click
        frmOPSetup.StartPosition = FormStartPosition.CenterScreen
        frmOPSetup.ShowDialog()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub dgvPayment_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPayment.CellValidated
        If e.ColumnIndex = 1 Then
            With dgvPayment.CurrentRow
                .Cells(1).Value = Format(CDec(.Cells(1).Value.ToString), "##,##0.00")
                .Cells(4).Value = Format(CDec(.Cells(1).Value.ToString) * CDec(.Cells(3).Value.ToString), "##,##0.00")
            End With
            _GetTotal()
            _GetAmortizedPay()
        End If
        If e.ColumnIndex = 4 Then
            With dgvPayment.CurrentRow
                .Cells(4).Value = Format(CDec(.Cells(4).Value.ToString), "##,##0.00")
                .Cells(1).Value = Format(CDec(.Cells(4).Value.ToString) / CDec(.Cells(3).Value.ToString), "##,##0.00")
            End With
            _GetTotal()
            _GetAmortizedPay()
        End If
    End Sub

    Private Sub _GetTotal()
        Dim totpay As Decimal = 0
        For i As Integer = 0 To dgvPayment.RowCount - 1
            With dgvPayment
                totpay = totpay + CDec(.Rows(i).Cells(4).Value.ToString)
            End With
        Next
        txtTotalPAyment.Text = Format(totpay, "##,##0.00")
    End Sub
    Private Sub _GetAmortizedPay()
        Dim amort As Decimal = 0
        For i As Integer = 0 To dgvPayment.RowCount - 1
            With dgvPayment
                amort = amort + CDec(.Rows(i).Cells(1).Value.ToString)
            End With
        Next
        txtAmortizedPay.Text = Format(amort, "##,##0.00")
    End Sub

    Private Sub btnApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApply.Click
        For i As Integer = 0 To frmLoan_ApplicationComputation.dgvList.RowCount - 1
            With frmLoan_ApplicationComputation.dgvList
                If .Rows(i).Cells(1).Value <> 0 Then
                    .Rows(i).Cells(5).Value = Format(CDec(txtAmortizedPay.Text), "##,##0.00")
                End If
            End With
        Next
        _Retotal_Amort()
        ' frmLoan_ApplicationComputation.GetAmortizationTotals()
        Me.Close()
    End Sub

    Private Sub _Retotal_Amort()
        Try
            For i As Integer = 0 To frmLoan_ApplicationComputation.dgvList.RowCount - 1
                With frmLoan_ApplicationComputation.dgvList
                    If .Rows(i).Cells(1).Value <> 0 Then
                        .Rows(i).Cells(6).Value = Format(CDec(.Rows(i).Cells(3).Value) + CDec(.Rows(i).Cells(4).Value) + CDec(.Rows(i).Cells(5).Value), "##,##0.00")
                    End If
                End With
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub RemoveAccountToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RemoveAccountToolStripMenuItem.Click
        If MsgBox("Are you sure ?", vbYesNo, "Remove Account") = vbYes Then
            Try
                SqlHelper.ExecuteNonQuery(con.cnstring, "_AMORTIZATION_RemoveAcct",
                                          New SqlParameter("@fcAccountCode", dgvPayment.CurrentRow.Cells(0).Tag.ToString))

                _LoadAcct()
            Catch ex As Exception
                Throw
            End Try
        Else
            Exit Sub
        End If
    End Sub

End Class