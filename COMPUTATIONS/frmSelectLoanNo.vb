﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmSelectLoanNo

    Private con As New Clsappconfiguration
    Public empid As String

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub _LoadLoans(ByVal key As String)
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_REBATE_Select_Loan",
                                           New SqlParameter("@idno", empid),
                                           New SqlParameter("@key", key))
            dgvLoan.DataSource = ds.Tables(0)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        _LoadLoans(txtSearch.Text)
    End Sub

    Private Sub frmSelectLoanNo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _LoadLoans("")
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Try
            With dgvLoan.CurrentRow
                frmComputeRebate.txtLoanNo.Text = .Cells(0).Value.ToString
                frmComputeRebate.txtNameofBorrower.Text = .Cells(1).Value.ToString
                frmComputeRebate.txtLoanType.Text = .Cells(2).Value.ToString
                frmComputeRebate.txtTerms.Text = .Cells(3).Value.ToString
                frmComputeRebate.txtPrincipalAmt.Text = .Cells(4).Value.ToString
                frmComputeRebate.txtAdvanceInt.Text = .Cells(5).Value.ToString
                frmComputeRebate.txtBalance.Text = .Cells(6).Value.ToString
                frmComputeRebate.txtPaidMonths.Text = .Cells(7).Value.ToString
                frmComputeRebate.txtUnpaidMonths.Text = .Cells(8).Value.ToString
                frmComputeRebate.txtAdvIntMonths.Text = .Cells(9).Value.ToString
                frmComputeRebate.txtRate.Text = .Cells(10).Value.ToString
            End With
            frmComputeRebate._compute_rebates()
            Me.Close()
        Catch ex As Exception
            Throw
        End Try
    End Sub

End Class