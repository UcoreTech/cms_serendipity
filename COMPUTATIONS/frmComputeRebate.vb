﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmComputeRebate

    Private con As New Clsappconfiguration
    'Public intmonths As String
    Private rebatemonths As Integer
    'Public intrate As Decimal
    Public acctCode As String
    Public acctTitle As String

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnFind_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFind.Click
        frmSelectLoanNo.StartPosition = FormStartPosition.CenterScreen
        frmSelectLoanNo.ShowDialog()
    End Sub

    Public Sub _compute_rebates()
        Try
            rebatemonths = CInt(txtAdvIntMonths.Text) - CInt(txtPaidMonths.Text)
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_REBATE_GetRebateAmount",
                                                               New SqlParameter("@RebateMonths", rebatemonths),
                                                               New SqlParameter("@LoanNo", txtLoanNo.Text))
            While rd.Read
                txtRebate.Text = rd(0).ToString
            End While
            txtAmountDue.Text = CDec(txtBalance.Text) - CDec(txtRebate.Text)
            _Particulars_Default()
            _ConverTNumeric()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub _Particulars_Default()
        txtPArticulars.Text = "Full Payment of cash loan (with Rebates) : RefNo " + txtLoanNo.Text
    End Sub

    Private Sub _ConverTNumeric()
        txtPrincipalAmt.Text = Format(CDec(txtPrincipalAmt.Text), "##,##0.00")
        txtAdvanceInt.Text = Format(CDec(txtAdvanceInt.Text), "##,##0.00")
        txtBalance.Text = Format(CDec(txtBalance.Text), "##,##0.00")
        txtRebate.Text = Format(CDec(txtRebate.Text), "##,##0.00")
        txtAmountDue.Text = Format(CDec(txtAmountDue.Text), "##,##0.00")
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtRebateAcct.Text = "" Then
            MsgBox("Please Select Account for Rebate to Continue.", vbInformation, "...")
            Exit Sub
        End If
        Try
            If MsgBox("Are you sure?", vbYesNo, "Save Rebate") = vbYes Then
                SqlHelper.ExecuteNonQuery(con.cnstring, "_REBATE_Insert",
                                           New SqlParameter("@fnLoanNoBORROWER", frmLoan_Processing.txtDLoanNo.Text),
                                           New SqlParameter("@fnLoanNoREBATE", txtLoanNo.Text),
                                           New SqlParameter("@fcDate", Date.Now),
                                           New SqlParameter("@fcParticular", txtPArticulars.Text),
                                           New SqlParameter("@fdAmountDue", txtAmountDue.Text),
                                           New SqlParameter("@fdPrincipal", txtPrincipalAmt.Text),
                                           New SqlParameter("@fdRate", txtRate.Text),
                                           New SqlParameter("@fdTerm", txtTerms.Text),
                                           New SqlParameter("@fnUnpaidMonths", txtUnpaidMonths.Text),
                                           New SqlParameter("@fdOSBalance", txtBalance.Text),
                                           New SqlParameter("@fdRebate", txtRebate.Text),
                                           New SqlParameter("@fcAcctCode", acctCode),
                                           New SqlParameter("@fcAcctTitle", acctTitle))
                frmMsgBox.txtMessage.Text = "Saving Success!"
                frmMsgBox.ShowDialog()
                'frmLoan_Processing._LoadRebates()
                Me.Close()
            Else
                Exit Sub
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub frmComputeRebate_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnChange_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChange.Click
        frmRebateAcct.StartPosition = FormStartPosition.CenterScreen
        frmRebateAcct.ShowDialog()
    End Sub

    Private Sub btnCompute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCompute.Click
        _compute_rebates()
    End Sub

End Class