﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmComputationSelectAcct

    Private con As New Clsappconfiguration
    Public xmode As String

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub frmComputationSelectAcct_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _SelectDate("")
    End Sub
    Private Sub _SelectDate(ByVal key As String)
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_ComputationTable_SearchAcct",
                                           New SqlParameter("@key", key))
            dgvList.DataSource = ds.Tables(0)
            dgvList.Columns(1).Visible = False
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        _SelectDate(TextBox1.Text)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Select Case xmode
            Case "CS"
                frmComputationTableLoan.txtCSAccount.Text = dgvList.CurrentRow.Cells(0).Value.ToString
                frmComputationTableLoan.txtCSAccount.Tag = dgvList.CurrentRow.Cells(1).Value.ToString
            Case "CTP"
                frmComputationTableLoan.txtCTPAccount.Text = dgvList.CurrentRow.Cells(0).Value.ToString
                frmComputationTableLoan.txtCTPAccount.Tag = dgvList.CurrentRow.Cells(1).Value.ToString
        End Select
        Me.Close()
    End Sub
End Class